/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.opinion;

import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.Comparator;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.view.XmlResourceOrderByComparator;
import com.tansuosoft.discoverx.model.Opinion;
import com.tansuosoft.discoverx.model.OpinionType;
import com.tansuosoft.discoverx.model.ParticipantTree;

/**
 * 系统默认使用的意见排序比较实现类。
 * 
 * <p>
 * 排序方式如下：<br/>
 * <ol>
 * <li>先按意见类型顺序排序</li>
 * <li>然后按填写意见用户顺序排序</li>
 * <li>接着按日期倒叙排序</li>
 * <li>最后按unid排序</li>
 * </ol>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class OpinionsComparator implements Comparator<Opinion> {
	private RuleBasedCollator collator = null;
	private ParticipantTreeProvider participantTreeProvider = null;

	/**
	 * 缺省构造器。
	 */
	public OpinionsComparator() {
		collator = (RuleBasedCollator) Collator.getInstance(java.util.Locale.CHINA);
		participantTreeProvider = ParticipantTreeProvider.getInstance();
	}

	/**
	 * 重载compare：实现排序功能。
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Opinion o1, Opinion o2) {
		int result = 0;
		if (o1 == null && o2 != null) return -1;
		if (o2 == null && o1 != null) return 1;
		if (o1 == null && o2 == null) return 0;
		// 先按意见类型顺序排序
		OpinionType ot1 = o1.getOpinionTypeResource();
		OpinionType ot2 = o2.getOpinionTypeResource();
		result = new XmlResourceOrderByComparator().compare(ot1, ot2);
		// 然后按填写意见用户顺序排序
		if (result == 0) {
			ParticipantTree pt1 = participantTreeProvider.getParticipantTree(o1.getModifier());
			ParticipantTree pt2 = participantTreeProvider.getParticipantTree(o2.getModifier());
			if (pt1 == null || pt2 == null) {
				if (pt1 == null && pt2 != null) result = -1;
				if (pt2 == null && pt1 != null) result = 1;
				if (pt1 == null && pt2 == null) result = 0;
			} else {
				result = pt1.getSort() - pt2.getSort();
			}
		}
		// 然后按日期倒叙排序
		if (result == 0) {
			String dt1 = o1.getModified();
			String dt2 = o2.getModified();
			collator.compare(dt1, dt2);
		}
		// 最后按unid排序
		if (result == 0) {
			String unid1 = o1.getUNID();
			String unid2 = o2.getUNID();
			collator.compare(unid1, unid2);
		}
		return result;
	}
}

