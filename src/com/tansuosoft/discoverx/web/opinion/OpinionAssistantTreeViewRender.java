/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.opinion;

import java.util.List;

import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.model.OpinionAssistant;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出意见助手TreeView的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OpinionAssistantTreeViewRender implements HtmlRender {
	/**
	 * 缺省构造器。
	 */
	public OpinionAssistantTreeViewRender() {
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		sb.append("id:").append("'").append("tvr_opinionassistant").append("'");
		sb.append(",").append("label:").append("'").append("意见助手").append("'");

		sb.append(",").append("data:{"); // data begin
		sb.append("}"); // data end

		sb.append(",").append("children:").append("["); // children begin

		int index = 0;
		Session s = (jspContext == null ? null : jspContext.getUserSession());
		List<String> udfOpinions = Profile.getInstance(s).getOpinions();
		sb.append("{"); // tvi parent begin
		sb.append("id:").append("'").append("tvi_myopinions").append("'");
		sb.append(",").append("label:").append("'").append("我的常用意见").append("'");
		sb.append(",").append("data:{"); // data begin
		sb.append("}"); // data end
		sb.append(",").append("children:").append("["); // children begin
		if (udfOpinions == null || udfOpinions.isEmpty()) {
			sb.append("{id:'tvi_noopa',error:true,label:'").append("[无记录]").append("'}");
		} else {
			for (String x : udfOpinions) {
				if (index > 0) sb.append(",");
				sb.append("{"); // tvi child begin
				sb.append("id:").append("'").append("tvi_myopinions").append(index).append("'");
				sb.append(",").append("label:").append("'").append(x).append("'");
				sb.append(",").append("desc:").append("'").append(x).append("'");
				sb.append(",").append("data:{"); // data begin
				sb.append("}"); // data end
				sb.append("}"); // tvi child end
				index++;
			}
		}
		sb.append("]"); // children end
		sb.append("}"); // tvi parent end

		// 从意见助手中读取
		OpinionAssistant opa = ResourceContext.getInstance().getSingleResource(OpinionAssistant.class);
		if (opa == null) {
			sb.append("{id:'tvi_noopa',error:true,label:'").append("[未配置意见填写助手]").append("'}");
		} else {
			List<String> strList = null;
			strList = opa.getImperatives();
			if (strList != null && strList.size() > 0) {
				sb.append(",");
				sb.append("{"); // tvi parent begin
				sb.append("id:").append("'").append("tvi_imperatives").append("'");
				sb.append(",").append("label:").append("'").append("祈使词").append("'");
				sb.append(",").append("data:{"); // data begin
				sb.append("}"); // data end
				sb.append(",").append("children:").append("["); // children begin
				index = 0;
				for (String x : strList) {
					if (index > 0) sb.append(",");
					sb.append("{"); // tvi child begin
					sb.append("id:").append("'").append("tvi_imperatives").append(index).append("'");
					sb.append(",").append("label:").append("'").append(x).append("'");
					sb.append(",").append("desc:").append("'").append(x).append("'");
					sb.append(",").append("data:{"); // data begin
					sb.append("}"); // data end
					sb.append("}"); // tvi child end
					index++;
				}
				sb.append("]"); // children end
				sb.append("}"); // tvi parent end
			}

			strList = opa.getEntitles();
			if (strList != null && strList.size() > 0) {
				sb.append(",");
				sb.append("{"); // tvi parent begin
				sb.append("id:").append("'").append("tvi_entitles").append("'");
				sb.append(",").append("label:").append("'").append("称呼").append("'");
				sb.append(",").append("data:{"); // data begin
				sb.append("}"); // data end
				sb.append(",").append("children:").append("["); // children begin
				index = 0;
				for (String x : strList) {
					if (index > 0) sb.append(",");
					sb.append("{"); // tvi child begin
					sb.append("id:").append("'").append("tvi_entitles").append(index).append("'");
					sb.append(",").append("label:").append("'").append(x).append("'");
					sb.append(",").append("desc:").append("'").append(x).append("'");
					sb.append(",").append("data:{"); // data begin
					sb.append("}"); // data end
					sb.append("}"); // tvi child end
					index++;
				}
				sb.append("]"); // children end
				sb.append("}"); // tvi parent end
			}

			strList = opa.getActions();
			if (strList != null && strList.size() > 0) {
				sb.append(",");
				sb.append("{"); // tvi parent begin
				sb.append("id:").append("'").append("tvi_actions").append("'");
				sb.append(",").append("label:").append("'").append("动作").append("'");
				sb.append(",").append("data:{"); // data begin
				sb.append("}"); // data end
				sb.append(",").append("children:").append("["); // children begin
				index = 0;
				for (String x : strList) {
					if (index > 0) sb.append(",");
					sb.append("{"); // tvi child begin
					sb.append("id:").append("'").append("tvi_actions").append(index).append("'");
					sb.append(",").append("label:").append("'").append(x).append("'");
					sb.append(",").append("desc:").append("'").append(x).append("'");
					sb.append(",").append("data:{"); // data begin
					sb.append("}"); // data end
					sb.append("}"); // tvi child end
					index++;
				}
				sb.append("]"); // children end
				sb.append("}"); // tvi parent end
			}

			strList = opa.getOpinions();
			if (strList != null && strList.size() > 0) {
				sb.append(",");
				sb.append("{"); // tvi parent begin
				sb.append("id:").append("'").append("tvi_opinions").append("'");
				sb.append(",").append("label:").append("'").append("系统常用意见").append("'");
				sb.append(",").append("data:{"); // data begin
				sb.append("}"); // data end
				sb.append(",").append("children:").append("["); // children begin
				index = 0;
				for (String x : strList) {
					if (index > 0) sb.append(",");
					sb.append("{"); // tvi child begin
					sb.append("id:").append("'").append("tvi_opinions").append(index).append("'");
					sb.append(",").append("label:").append("'").append(x).append("'");
					sb.append(",").append("desc:").append("'").append(x).append("'");
					sb.append(",").append("data:{"); // data begin
					sb.append("}"); // data end
					sb.append("}"); // tvi child end
					index++;
				}
				sb.append("]"); // children end
				sb.append("}"); // tvi parent end
			}
		}
		sb.append("]"); // children end

		return sb.toString();
	}
}

