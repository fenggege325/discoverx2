/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.opinion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceComparator;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.impl.OpinionInserter;
import com.tansuosoft.discoverx.dao.impl.OpinionRemover;
import com.tansuosoft.discoverx.dao.impl.OpinionUpdater;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Opinion;
import com.tansuosoft.discoverx.model.OpinionType;
import com.tansuosoft.discoverx.model.OpinionView;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.PersistenceState;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.Signature;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 封装意见相关常用操作的工具类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OpinionHelper extends EventSourceImpl {
	private Document m_parent = null;
	private Session m_session = null;

	/**
	 * 接收意见所属文档资源和用户自定义会话参数的构造器。
	 * 
	 * @param parent Document，所属文档资源，必须。
	 * @param session 用户自定义会话，可选。
	 */
	public OpinionHelper(Document parent, Session session) {
		this.m_parent = parent;
		if (this.m_parent == null) throw new java.lang.IllegalArgumentException("所属文档资源必须提供！");
		this.m_session = session;
	}

	/**
	 * 根据意见内容等信息获取一个与当前文档、当前用户、当前流程环节绑定的意见对象。
	 * 
	 * @param body
	 * @param opinionTypeUNID
	 * @param signature
	 * @param opinionView
	 * @return
	 */
	public Opinion newOpinion(String body, String opinionTypeUNID, Signature signature, OpinionView opinionView) {
		Opinion result = new Opinion(this.m_parent.getUNID());
		List<Opinion> list = this.m_parent.getOpinions();
		result.setSort(list == null ? 0 : list.size());
		User user = Session.getUser(m_session);
		String fn = user.getName();
		result.setBody(body);
		result.setPassed(true);
		result.setOpinionView(opinionView);
		WorkflowRuntime wfr = null;
		Workflow wf = null;
		Activity act = null;
		String state = null;
		WFData current = null;
		try {
			wfr = WorkflowRuntime.getInstance(m_parent, m_session);
			wf = wfr.getCurrentWorkflow();
			act = wfr.getCurrentActivity();
			state = String.format("%1$s/%2$s", wf == null ? "" : wf.getUNID(), act == null ? "" : act.getUNID());
			result.setState(state);
			current = wfr.getCurrentWFData(WFDataLevel.Approver);
		} finally {
			wfr.shutdown();
		}
		boolean isAgent = false;
		String agentFullName = null;
		if (current != null && current.checkSubLevel(WFDataSubLevel.Agent)) {
			isAgent = true;
			ParticipantTree ptAgent = ParticipantTreeProvider.getInstance().getParticipantTree(current.getData());
			agentFullName = (ptAgent == null ? "[未知代理人]" : ptAgent.getName());
			ParticipantTree ptClient = ParticipantTreeProvider.getInstance().getParticipantTree(current.getParentData());
			fn = (ptClient == null ? "[未知委托人]" : ptClient.getName());
		}
		result.setCreator(fn);
		result.setModifier(fn);
		result.setName(ParticipantHelper.getFormatValue(user, "ou0\\cn"));
		result.setAgent(isAgent ? agentFullName : null);
		if (opinionTypeUNID != null && opinionTypeUNID.length() > 0) result.setOpinionType(opinionTypeUNID);
		switch (signature.getIntValue()) {
		case 1:
		default:
			result.setSignature(null);
		}
		return result;
	}

	/**
	 * 检查文档当前流程状态下当前用户是否有填写意见。
	 * 
	 * @return
	 */
	public boolean hasOpinion() {
		return (getOpinion() != null);
	}

	private Opinion m_current = null;

	/**
	 * 根据{@link Opinion}对象的UNID获取对应{@link Opinion}对象。
	 * 
	 * @param opinionUNID
	 * @return
	 */
	public Opinion getOpinion(String opinionUNID) {
		if (opinionUNID == null || opinionUNID.length() == 0) return null;
		List<Opinion> opinions = this.m_parent.getOpinions();
		if (opinions == null || opinions.isEmpty()) return null;
		for (Opinion x : opinions) {
			if (x != null && opinionUNID.equalsIgnoreCase(x.getUNID())) { return x; }
		}
		return null;
	}

	/**
	 * 返回文档当前流程状态下的当前用户未完成的意见。
	 * 
	 * @return
	 */
	public Opinion getOpinion() {
		if (m_current != null) return m_current;
		List<Opinion> opinions = this.m_parent.getOpinions();
		if (opinions == null || opinions.isEmpty()) return null;
		Collections.sort(opinions, new ResourceComparator());
		User user = Session.getUser(m_session);
		String fn = user.getName();
		WorkflowRuntime wfr = WorkflowRuntime.getInstance(m_parent, m_session);
		Workflow wf = wfr.getCurrentWorkflow();
		Activity act = wfr.getCurrentActivity();
		String state = String.format("%1$s/%2$s", wf == null ? "" : wf.getUNID(), act == null ? "" : act.getUNID());

		String startDT = null;
		String endDT = DateTime.getNowDTString();
		List<WFData> wfdata = wfr.getContextWFData();
		for (WFData x : wfdata) {
			if (x != null && x.getData() == user.getSecurityCode()) {
				startDT = x.getCreated();
			}
		}
		if (startDT == null) startDT = "";
		String created = null;
		String stateo = null;
		for (Opinion x : opinions) {
			if (x == null) continue;
			created = x.getCreated();
			stateo = x.getState();
			if (x.getModifier().equalsIgnoreCase(fn) && startDT.compareToIgnoreCase(created) <= 0 && endDT.compareToIgnoreCase(created) >= 0 && (state.equalsIgnoreCase(stateo) || state.endsWith(stateo))) {
				m_current = x;
			}
		}
		wfr.shutdown();
		return m_current;
	}

	/**
	 * 保存或删除指定的审批意见到数据库并更改内存中的文档包含的意见。
	 * 
	 * <p>
	 * 将触发意见资源的保存前/删除前和保存后/删除后事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父文档资源。
	 * </p>
	 * 
	 * @param opinion
	 * @param persistenceState
	 * @return
	 */
	public boolean persistenceOpinion(Opinion opinion, PersistenceState persistenceState) {
		try {
			if (opinion == null || persistenceState == PersistenceState.Raw) return false;
			ResourceEventArgs e = new ResourceEventArgs(opinion, this.m_session);
			// 添加事件处理程序。
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(opinion, null);
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s,%3$s,%4$s", EventHandler.EHT_SAVE, EventHandler.EHT_SAVED, EventHandler.EHT_DELETE, EventHandler.EHT_DELETED), eventHandlerInfoList);

			DBRequest dbr = null;
			switch (persistenceState.getIntValue()) {
			case 1: // New
				this.callEventHandler(this.m_parent, EventHandler.EHT_SAVE, e); // 保存开始事件。
				opinion.setPUNID(m_parent.getUNID());
				dbr = new OpinionInserter();
				dbr.setResource(opinion);
				break;
			case 2: // Delete
				this.callEventHandler(this.m_parent, EventHandler.EHT_DELETE, e); // 删除开始事件。
				dbr = new OpinionRemover();
				dbr.setParameter(DBRequest.UNID_PARAM_NAME, opinion.getUNID());
				break;
			case 3: // Update
				this.callEventHandler(this.m_parent, EventHandler.EHT_SAVE, e); // 保存开始事件。
				dbr = new OpinionUpdater();
				dbr.setResource(opinion);
				break;
			default:
				break;
			}
			if (dbr != null) dbr.sendRequest();
			if (dbr.getResultLong() > 0) {
				switch (persistenceState.getIntValue()) {
				case 1: // New
					if (m_parent.getOpinions() == null) m_parent.setOpinions(new ArrayList<Opinion>());
					m_parent.getOpinions().add(0, opinion);
					this.callEventHandler(this.m_parent, EventHandler.EHT_SAVED, e); // 保存完成事件。
					break;
				case 2: // Delete
					if (m_parent.getOpinions() != null) {
						int idx = 0;
						for (Opinion x : m_parent.getOpinions()) {
							if (x != null && opinion.getUNID().equalsIgnoreCase(x.getUNID())) {
								m_parent.getOpinions().remove(idx);
								this.callEventHandler(this.m_parent, EventHandler.EHT_DELETED, e); // 删除完成事件。
								break;
							}
							idx++;
						}
					}
					break;
				case 3: // Update
					if (m_parent.getOpinions() != null) {
						int idx = 0;
						for (Opinion x : m_parent.getOpinions()) {
							if (x != null && opinion.getUNID().equalsIgnoreCase(x.getUNID())) {
								m_parent.getOpinions().set(idx, opinion);
								this.callEventHandler(this.m_parent, EventHandler.EHT_SAVED, e); // 保存完成事件。
								break;
							}
							idx++;
						}
					}
					break;
				default:
					break;
				}
				return true;
			}
			return false;
		} catch (Exception ex) {
			throw (ex instanceof RuntimeException ? (RuntimeException) ex : new RuntimeException(ex));
		}
	}

	/**
	 * 获取容器资源包含的所有审批意见（或评论、答复）中，类型为opnTypeId的所有审批意见列表。
	 * 
	 * @param opnTypeId 表示要返回的审批意见对应的审批意见类型资源的别名、UNID或名称之一。
	 * @return 如果找到至少一个匹配的结果则返回包含其结果的列表，否则返回null。
	 */
	public List<Opinion> getOpinionsByType(String opnTypeId) {
		if (opnTypeId == null || opnTypeId.length() == 0) return null;
		List<Opinion> list = (this.m_parent == null ? null : this.m_parent.getOpinions());
		if (list == null) return null;
		List<Opinion> result = null;
		OpinionType ot = null;
		for (Opinion x : list) {
			if (x == null) continue;
			ot = x.getOpinionTypeResource();
			if (ot == null) continue;
			if ((opnTypeId.startsWith("act") && opnTypeId.equalsIgnoreCase(ot.getAlias())) || opnTypeId.equalsIgnoreCase(ot.getUNID()) || opnTypeId.equalsIgnoreCase(ot.getName())) {
				if (result == null) result = new ArrayList<Opinion>();
				result.add(x);
			}
		}
		return result;
	}

	/**
	 * 获取容器资源包含的所有审批意见（或评论、答复）中，类型为opnTypeId的第一个审批意见对象。
	 * 
	 * @param opnTypeId 表示要返回的审批意见对应的审批意见类型资源的别名、UNID或名称之一。
	 * @return 如果找到至少一个匹配的结果则返回之，否则返回null。
	 */
	public Opinion getFirstOpinionByType(String opnTypeId) {
		List<Opinion> result = getOpinionsByType(opnTypeId);
		return (result != null && result.size() > 0 ? result.get(0) : null);
	}
}

