/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.opinion;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.impl.SecurityDeserializer;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Opinion;
import com.tansuosoft.discoverx.model.OpinionView;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.GenericTriplet;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 输出文档包含的所有意见内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OpinionsRender implements HtmlRender {
	private Document m_document = null;
	private DocumentDisplayInfo m_DocumentDisplayInfo = null;
	private Comparator<Opinion> m_comparator = null;

	private static final String NO_OPINION_RESULT = "{error:true,message:'暂无相关内容！'}";

	/**
	 * 接收文档资源、文档显示信息、意见排序对象的构造器。
	 * 
	 * @param document {@link Document}，如果为null，则尝试从{@link Session#getLastDocument()}中获取。
	 * @param documentDisplayInfo {@link DocumentDisplayInfo}，可以为null
	 * @param comparator 如果为null则默认使用{@link OpinionsComparator}来排序输出的意见。
	 */
	public OpinionsRender(Document document, DocumentDisplayInfo documentDisplayInfo, Comparator<Opinion> comparator) {
		if (document == null) throw new java.lang.IllegalArgumentException("必须提供文档资源！");
		m_document = document;
		m_DocumentDisplayInfo = documentDisplayInfo;
		m_comparator = comparator;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		StringBuilder json = new StringBuilder();
		WorkflowRuntime wfr = null;

		try {
			Session session = jspContext.getUserSession();
			if (this.m_document == null && session != null) this.m_document = session.getLastDocument();
			if (this.m_document == null) throw new Exception("无法获取当前文档！");
			List<Opinion> opinions = m_document.getOpinions();
			if (opinions == null || opinions.isEmpty()) return NO_OPINION_RESULT;
			if (m_DocumentDisplayInfo != null) m_DocumentDisplayInfo.setHasOpinion(true);
			wfr = WorkflowRuntime.getInstance(this.m_document, session);
			Workflow wf = wfr.getCurrentWorkflow();
			Activity act = wfr.getCurrentActivity();
			String state = String.format("%1$s/%2$s", wf == null ? "" : wf.getUNID(), act == null ? "" : act.getUNID());

			List<WFData> wfdata = wfr.getWFData();
			// 保存流程参与者信息的对象
			GenericTriplet<String, String, Integer> triplet = null; // 第一个值为开始时间，第二个值为完成时间，第三个值为安全编码
			// 流程参与者全名与流程参与者信息一一对应的映射。
			HashMap<String, GenericTriplet<String, String, Integer>> map = new HashMap<String, GenericTriplet<String, String, Integer>>();
			String fn = null;
			ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
			ParticipantTree pt = null;
			for (WFData x : wfdata) {
				if (x == null) continue;
				pt = ptp.getParticipantTree(x.getData());
				fn = (pt == null ? x.getData() + "" : pt.getName());
				if (!map.containsKey(fn)) map.put(fn, new GenericTriplet<String, String, Integer>(x.getCreated(), x.getAccomplished(), x.getData()));
			}

			User user = Session.getUser(session);
			String body = null;
			String agent = null;
			String modified = null;
			String signature = null;
			Collections.sort(opinions, (m_comparator == null ? new OpinionsComparator() : m_comparator));
			String opinionTypeName = null;
			String lastOpinionTypeName = null;
			OpinionView ov = OpinionView.ViewByDocumentViewer;
			boolean viewable = false;
			int opinionIndex = 0;
			int opinionTypeNameIndex = 0;
			for (Opinion x : opinions) {
				if (x == null) continue;
				ov = x.getOpinionView();
				modified = x.getModified();
				if (modified == null) modified = "";
				viewable = true;
				if (ov == OpinionView.ViewByDocumentViewerOnDone) {
					// 如果意见是在当前环节下填写的（当前环节下的意见才判断是否完成，如果环节已经完成，就不用判断了）
					if (state.equalsIgnoreCase(x.getState()) || state.endsWith(x.getState())) {
						// 如果用户没有完成审批那么不能查看
						triplet = map.get(x.getModifier());
						if (triplet != null && StringUtil.isBlank(triplet.getSecond()) && !x.getCreator().equalsIgnoreCase(user.getName())) viewable = false;
					}
				} else if (ov == OpinionView.ViewBySubsequentParticipants) {
					triplet = map.get(user.getName());
					if (triplet == null) triplet = map.get(user.getSecurityCode() + "");
					if (triplet != null) {
						// 如果当前用户在参与者列表中的且其参与日期小于意见日期，那么不显示。
						// 注：对于没在参与者中可以查看文档的用户，可以直接查看意见而不受此选项影响
						if (modified.compareToIgnoreCase(triplet.getFirst()) > 0) viewable = false;
					}
				} else if (ov == OpinionView.Custom) {
					Security s = x.getSecurity();
					if (s == null) {
						DBRequest dbr = new SecurityDeserializer();
						dbr.setParameter(DBRequest.PUNID_PARAM_NAME, x.getUNID());
						dbr.sendRequest();
						s = dbr.getResult(Security.class);
						if (s == null) s = new Security(x.getUNID());
						x.setSecurity(s);
					}
					viewable = SecurityHelper.authorize(session, x, SecurityLevel.View);
				}
				if (!viewable) continue;
				body = x.getBody();
				if (body == null) body = "";
				opinionTypeName = x.getOpinionTypeName();
				if (opinionTypeName == null || opinionTypeName.length() == 0) opinionTypeName = "[未分类意见]";
				signature = x.getSignature();
				agent = x.getAgent();

				if (!opinionTypeName.equalsIgnoreCase(lastOpinionTypeName)) {
					if (opinionIndex > 0) {
						sb.append(json.toString());
						json.delete(0, json.length());
						sb.append("]"); // opinions end
						sb.append("}"); // opinionTypeName end
						if (opinionTypeNameIndex > 0) sb.append(",");
					}
					sb.append("\r\n{"); // opinionTypeName begin
					sb.append("opinionTypeName:").append("'").append(opinionTypeName).append("'");
					sb.append(",").append("\r\nopinions:["); // opinions begin
					opinionTypeNameIndex++;
				}

				json.append(json.length() > 0 ? "," : "");
				json.append("{");// opinion begin
				body = StringUtil.encode4Json(body);
				if (body != null) body = body.replace("\r\n", "<br/>").replace("\r", "<br/>").replace("\n", "<br/>").replace("<", "&lt;").replace(">", "&gt;");
				json.append("body:").append("'").append(body).append("'");
				json.append(",").append("modifier:").append("'").append(StringUtil.encode4Json(x.getModifier())).append("'");
				json.append(",").append("modified:").append("'").append(modified).append("'");
				json.append(",").append("id:").append("'").append(x.getUNID()).append("'");
				json.append(",").append("sort:").append(x.getSort());
				json.append(",").append("name:'").append(StringUtil.encode4Json(StringUtil.getValueString(x.getName(), ""))).append("'");
				json.append(",").append("description:'").append(StringUtil.encode4Json(StringUtil.getValueString(x.getDescription(), ""))).append("'");
				json.append(",").append("category:'").append(StringUtil.encode4Json(StringUtil.getValueString(x.getCategory(), ""))).append("'");
				json.append(",").append("alias:'").append(StringUtil.encode4Json(StringUtil.getValueString(x.getAlias(), ""))).append("'");
				if (agent != null && agent.length() > 0) json.append(",").append("agent:").append("'").append(StringUtil.encode4Json(agent)).append("'");
				if (signature != null && signature.length() > 0) json.append(",").append("signature:").append(StringUtil.encode4Json(signature));
				json.append("}");// opinion end
				opinionIndex++;
				lastOpinionTypeName = opinionTypeName;
			}
			if (opinionIndex == 0) return NO_OPINION_RESULT;
			sb.append(json.toString());
			sb.append("]"); // opinions end
			sb.append("}"); // opinionTypeName end
		} catch (Exception ex) {
			sb = new StringBuilder();
			sb.append("{error:true,message:'").append(StringUtil.encode4Json(ex.getMessage())).append("'}");
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		return sb.toString();
	}
}

