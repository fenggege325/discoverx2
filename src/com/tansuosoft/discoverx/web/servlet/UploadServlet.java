/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.VerbotenUploadFileTypeConfig;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.CRUD;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.accessory.AccessoryContainerHelper;
import com.tansuosoft.discoverx.web.accessory.AccessoryForm;

/**
 * 表示接收以流方式提交的附加文件内容数据并将其保存到服务器上传目录的Servlet类。
 * 
 * <p>
 * 对于附加文件的请求参数信息请参考{@link com.tansuosoft.discoverx.web.accessory.AccessoryForm}，文件内容保存成功后，将转发到“dispatch?fx=accessory”。<br/>
 * 如果是其它用途的上传，需提供以下参数：<br/>
 * <ul>
 * <li>for：表示使用上传的文件的目标转发url地址，必须；</li>
 * <li>fn：上传文件的文件名（不能包含绝对路径），必须。</li>
 * </ul>
 * 基于其它用途上传的内容保存在临时附加文件路径下，使用完毕后建议删除之，只有超级管理员才能上传其它用途的文件到系统。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class UploadServlet extends HttpServlet {

	/**
	 * 序列化版本号。
	 */
	private static final long serialVersionUID = 5502334992632999237L;

	/**
	 * 重载doPost:把request中的附加文件内容保存到服务器中。
	 * 
	 * <p>
	 * 出错则重定向到json错误页。
	 * </p>
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FileOutputStream fo = null;
		ServletInputStream is = null;

		Session session = SessionContext.getSession(request);
		String sizeExceededErrorMsg = "文件大小超过系统最大允许上传的文件大小限制！";
		try {
			int cl = request.getContentLength();
			int maxUploadSize = CommonConfig.getInstance().getMaxUploadSize();
			String forUrl = request.getParameter("for");
			boolean isForAccessory = (forUrl == null || forUrl.length() == 0);

			String absolutePath = null; // 附加文件绝对路径
			String filePath = null; // 附加文件绝对路径和文件名
			if (isForAccessory) {
				if (cl > maxUploadSize) { throw new RuntimeException(sizeExceededErrorMsg); }

				AccessoryForm form = new AccessoryForm();
				form.fillWebRequestForm(request);

				String fileName = null;
				String punid = form.getParentUNID();
				String pdir = form.getParentDirectory();
				int action = form.getAction();
				boolean isAdd = (action == CRUD.Create.getIntValue());

				Resource parent = null;
				if (pdir.equalsIgnoreCase(ResourceDescriptorConfig.DOCUMENT_DIRECTORY)) parent = DocumentBuffer.getInstance().getDocument(punid);
				if (parent == null) parent = ResourceContext.getInstance().getResource(punid, pdir);
				if (parent == null) throw new Exception("无法获取附加文件资源的容器资源！");

				boolean authorized = SecurityHelper.authorize(session, parent, (isAdd ? SecurityLevel.Add : SecurityLevel.Modify));
				if (!authorized) throw new Exception("对不起，您没有" + (isAdd ? "新增" : "修改") + "文件的权限！");
				if (isAdd) {
					fileName = form.getFileName();
				} else {
					AccessoryContainerHelper helper = new AccessoryContainerHelper(parent, session);
					Accessory accessory = helper.getAccessory(form.getUNID());
					if (accessory == null) throw new RuntimeException("找不到资源“" + parent.getName() + "”中“" + form.getUNID() + "”对应的附加文件！");
					fileName = accessory.getFileName();
				}
				if (fileName != null && !VerbotenUploadFileTypeConfig.getInstance().check(fileName, true)) {
					String extName = FileHelper.getExtName(fileName, "");
					throw new Exception("不允许上传“" + extName + "”类型的文件！");
				}
				absolutePath = AccessoryPathHelper.getAbsoluteAccessoryServerPath(parent);
				filePath = absolutePath + fileName;
			} else {
				// if (!SecurityHelper.checkUserRole(Session.getUser(session), Role.ROLE_ADMIN_SC)) throw new Exception("只有超级管理员才能上传文件！");
				User u = Session.getUser(session);
				if (session == null || (u != null && u.isAnonymous())) throw new Exception("只有登录用户才能上传文件！");
				String fn = request.getParameter("fn");
				if (fn == null || fn.length() == 0) fn = String.format("tmp_%1$s_%2$d", Session.getUser(session).getAlias(), Thread.currentThread().getId());
				boolean hasPath = (fn.indexOf('\\') > 0 || fn.indexOf('/') > 0);
				if (hasPath) fn = fn.replace('\\', File.separatorChar).replace('/', File.separatorChar);
				if (fn.startsWith(File.separator)) fn = fn.substring(1);
				absolutePath = (hasPath ? CommonConfig.getInstance().getInstallationPath() : AccessoryPathHelper.getAbsoluteTempAccessoryServerPath());
				filePath = absolutePath + fn;
			}

			// 创建目标路径
			File dir = new File(absolutePath);
			dir.mkdirs();

			// 写入文件
			long received = 0L;
			File f = new File(filePath);
			fo = new FileOutputStream(f, false); // 文件输出流对象
			is = request.getInputStream(); // 提交的内容
			int bytesLen = 8192;
			byte[] bss = new byte[bytesLen];
			int i = -1; // 每次实际读取的字节内容
			MessageDigest sha1 = MessageDigest.getInstance("SHA1");
			while ((i = is.read(bss, 0, bytesLen)) > 0) {
				fo.write(bss, 0, i);
				sha1.update(bss, 0, i);
				received += i;
				if (isForAccessory && received > maxUploadSize) {
					try {
						fo.flush();
						fo.close();
						if (!f.delete()) f.deleteOnExit();
					} catch (Exception ex) {
					} finally {
					}
					throw new RuntimeException(sizeExceededErrorMsg);
				}
			}
			fo.flush();
			String hash = StringUtil.toHexString(sha1.digest());
			if (isForAccessory) {
				request.getRequestDispatcher("dispatch?fx=Accessory&hash=" + hash).forward(request, response);
			} else {
				request.getRequestDispatcher(forUrl + (forUrl.indexOf('?') > 0 ? "&hash=" : "?hash=") + hash).forward(request, response);
			}
		} catch (Exception ex) {
			HttpSession httpSession = (request == null ? null : request.getSession());
			Session.setSessionParam(session, httpSession, Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, new Exception(ex.getMessage()));
			PathContext pathContext = new PathContext(request);
			response.sendRedirect(pathContext.parseUrl(UrlConfig.getInstance().getUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE)));
		} finally {
			if (fo != null) fo.close();
			if (is != null) is.close();
		}
	}

	/**
	 * 处理GET请求：重定向到json错误页。
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Session session = SessionContext.getSession(request);
		HttpSession httpSession = (request == null ? null : request.getSession());
		Session.setSessionParam(session, httpSession, Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, new Exception("请使用POST方式发送请求并提供请求内容！"));
		PathContext pathContext = new PathContext(request);
		response.sendRedirect(pathContext.parseUrl(UrlConfig.getInstance().getUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE)));
	}
}

