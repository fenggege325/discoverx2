/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.function.CommonDispatchForm;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.function.OperationEvaluator;
import com.tansuosoft.discoverx.bll.function.OperationParser;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Function;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UriUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.PathContext;

/**
 * 表示接收系统功能扩展函数调用http请求并处理之，然后转发处理结果的功能转发器Servlet类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DispatchServlet extends HttpServlet {

	/**
	 * 序列化版本号。
	 */
	private static final long serialVersionUID = -1673725662231910593L;

	private static final String AT = "@";
	private static final String P = "()";

	/**
	 * 处理POST请求。
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession httpSession = request.getSession();
		PathContext pathContext = new PathContext(request);

		CommonDispatchForm dispatchForm = new CommonDispatchForm();
		dispatchForm.fillWebRequestForm(request);
		Session session = SessionContext.getSession(request);

		String function = dispatchForm.getFunction();
		if (StringUtil.isUNID(function)) {
			Function f = ResourceContext.getInstance().getResource(function, Function.class);
			if (f != null) function = AT + f.getAlias() + P;
		} else {
			if (!function.startsWith(AT)) function = AT + function;
			if (!function.endsWith(P.substring(1))) function = function + P;
		}

		OperationResult operationResult = null;

		Operation operation = null;
		try {
			OperationEvaluator evaluator = new OperationEvaluator(function, session, request, null, this);
			OperationParser parser = evaluator.getOperationParser();
			String uiPath = parser.getUiPath(null);
			operation = parser.getOperation();
			// 显示操作界面
			if (uiPath != null && uiPath.trim().length() > 0 && !operation.isPostback()) {
				Session.setSessionParam(session, httpSession, Session.LASTOPERATION_PARAM_NAME_IN_HTTPSESSION, parser);
				String requestUrl = operation.getSourceRequestUrl();
				operation.setHttpRequest(null);
				response.sendRedirect(requestUrl);
				return;
			}
			operationResult = evaluator.evalResult();
			if (operationResult == null) {
				throw new FunctionException("无法获取有效执行结果信息！");
			} else {
				String resultUrl = pathContext.parseUrl(operationResult.getResultUrl());
				// 转发或重定向
				if (operationResult.getIsForward()) {
					RequestDispatcher requestDispatcher = request.getRequestDispatcher(resultUrl);
					requestDispatcher.forward(request, response);
				} else {
					response.sendRedirect(resultUrl);
				}
			}
		} catch (Exception e) {
			delPostOperationFunctionCookie(request, response);
			boolean isFunctionException = (e instanceof FunctionException);
			Throwable t = (isFunctionException ? ((FunctionException) e).getRootThrowable() : e);
			Session.setSessionParam(session, httpSession, Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, t);
			if (CommonConfig.getInstance().getDebugable()) {
				if (isFunctionException) FileLogger.debug("执行操作时出现异常：" + t.getMessage());
				else FileLogger.error(e);
			}
			FileLogger.debug(encMsg4Log("触发操作执行异常的请求地址：" + request.getRequestURL().toString() + "?" + UriUtil.decodeURIComponent(request.getQueryString())));
			response.sendRedirect(pathContext.parseUrl(UrlConfig.getInstance().getUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE)));
		}
	}

	protected static String encMsg4Log(String raw) {
		if (raw == null || raw.isEmpty()) return raw;
		return raw.replace("%", "%%");
	}

	/**
	 * 处理GET请求。
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * 出现异常时，用于删除cookie，这个cookie保存了操作执行成功后要自动执行的js函数的名称。
	 * 
	 * @param request
	 * @param response
	 */
	protected void delPostOperationFunctionCookie(HttpServletRequest request, HttpServletResponse response) {
		Cookie cookie = new Cookie("POFC", "");
		cookie.setMaxAge(0); // cookie有效时间
		cookie.setPath(request.getContextPath()); // 在根路径下创建
		response.addCookie(cookie);
	}
}

