/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.servlet;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.LoginEventArgs;
import com.tansuosoft.discoverx.bll.security.Authentication;
import com.tansuosoft.discoverx.bll.security.AuthenticationProvider;
import com.tansuosoft.discoverx.bll.security.BasicCredential;
import com.tansuosoft.discoverx.bll.security.Identity;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.Crypt;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.Escape;
import com.tansuosoft.discoverx.util.HttpUtil;
import com.tansuosoft.discoverx.util.InfoLogLevelException;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.Browser;
import com.tansuosoft.discoverx.web.PathContext;

/**
 * 表示处理用户登录的Servlet类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public final class LoginServlet extends HttpServlet {
	/**
	 * 获取登录后重定向url地址的请求参数名：“redirect”。
	 */
	public static final String REDIRECT_PARAMETER_NAME = "redirect";

	/**
	 * 获取登录后转发url地址的请求参数名：“forward”。
	 */
	public static final String FORWARD_PARAMETER_NAME = "forward";

	/**
	 * 办公助手在线状态值。
	 */
	public static final int TSIM_PRESENCE_ONLINE = 40012;

	private static final String TRYLOGINCOUNT_PARAM_NAME = "__TRYLOGINCOUNT";
	/**
	 * 序列化版本号。
	 */
	private static final long serialVersionUID = -5932836312282816873L;
	private static final String SAVED_PWD_PREFIX = "c:";
	private static final String SAVE_PWD_COOKIE_NAME = "alk";

	/**
	 * 处理POST请求。 request参数设置:"uid"表示用户登录帐号,"password"表示登录密码。<br/>
	 * “redirect”表示成功登录后跳转的url(不提供则默认为用户当前使用的门户框架首页)。<br/>
	 * 若登录失败,则将错误信息保存在http会话中名为“{@link com.tansuosoft.discoverx.model.Session#LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION}”的参数中。
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession httpsession = request.getSession();
		boolean tsimflag = checkTSIM(request);
		String referer = request.getHeader("Referer");
		String loginUrl = (tsimflag ? "tsim/login.jsp" : (referer == null || referer.length() == 0) ? "login.jsp" : referer);
		try {
			PathContext pathContext = new PathContext(request);
			String webAppRoot = pathContext.getWebAppRoot();

			// 登录尝试处理
			int maxTryLoginTimeout = CommonConfig.getInstance().getMaxTryLoginTimeout();
			int maxTryLoginCount = CommonConfig.getInstance().getMaxTryLoginCount();
			Object tryLoginCountValueObj = httpsession.getAttribute(TRYLOGINCOUNT_PARAM_NAME);
			int tryLoginCount = StringUtil.getValueInt(tryLoginCountValueObj == null ? "0" : tryLoginCountValueObj.toString(), 0);
			if (tryLoginCount >= maxTryLoginCount && maxTryLoginTimeout * 60 * 1000 >= (System.currentTimeMillis() - httpsession.getLastAccessedTime())) {
				String message = "超过系统允许的登录尝试次数!";
				httpsession.setAttribute(Session.LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION, message);
				response.sendRedirect(loginUrl); // 返回登录页面
				return;
			} else {
				httpsession.setAttribute(TRYLOGINCOUNT_PARAM_NAME, tryLoginCount + 1);
			}

			// 执行登录过程
			String userAgent = HttpUtil.getClient(request);
			String remoteAddr = HttpUtil.getRemoteAddr(request);
			String addr = (tsimflag ? HttpUtil.getClientIP(request) : null);

			String account = null;
			String password = null;
			String savedPwd = null;
			boolean savePwd = StringUtil.getValueBool(request.getParameter("savepwd"), false);
			String sid = request.getParameter("sid");
			if (sid == null || sid.trim().length() == 0) {
				account = request.getParameter("uid");
				password = request.getParameter("password");
				if (password != null && password.startsWith(SAVED_PWD_PREFIX)) {
					if (savePwd) savedPwd = password;
					Crypt c = new Crypt();
					password = c.decryptFromHexString(password.substring(2));
				} else if (password != null && savePwd) {
					Crypt c = new Crypt();
					savedPwd = SAVED_PWD_PREFIX + c.encryptToHexString(password);
				}
			} else {
				StringPair sp = unwrapCredentialPair(sid);
				if (sp == null) throw new InfoLogLevelException("没有提供有效用户登录凭据信息！");
				account = sp.getKey();
				password = sp.getValue();
			}
			if (account == null || account.trim().length() == 0) throw new InfoLogLevelException("没有提供有效登录帐号！");
			if (password == null || password.length() == 0) throw new InfoLogLevelException("没有提供有效登录密码！");

			BasicCredential baseCredential = new BasicCredential();
			baseCredential.setPassword(StringUtil.getMD5HashString(password.trim()));
			password = null;
			baseCredential.setAccount(account.trim());
			baseCredential.setRemoteAddress(remoteAddr);
			baseCredential.setRemoteAgent(userAgent);

			Authentication auth = null;
			auth = AuthenticationProvider.getAuthentication();
			Identity identity = auth.authenticate(baseCredential);

			if (identity == null || !identity.getAuthenticated() || identity.getUser() == null) {
				String message = "登录失败，请提供正确的帐号和密码！";
				httpsession.setAttribute(Session.LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION, message);
				response.sendRedirect(loginUrl); // 返回登录页面
				return;
			} else {
				httpsession.removeAttribute(TRYLOGINCOUNT_PARAM_NAME);

				String redirectUrl = null;
				String redirectUrlFromRequest = request.getParameter(REDIRECT_PARAMETER_NAME);
				String forwardUrl = request.getParameter(FORWARD_PARAMETER_NAME);
				if (!StringUtil.isNullOrEmpty(redirectUrlFromRequest)) redirectUrl = PathContext.combineUrl(webAppRoot, redirectUrlFromRequest);
				if (redirectUrl == null || redirectUrl.trim().length() == 0) redirectUrl = pathContext.getFramePath() + "index.jsp";

				// 处理用户自定义会话
				User user = identity.getUser();
				Session oldSession = Session.getSession(user.getUNID());
				if (oldSession == null) { // 如果原来没有登录过。
					// NOOP
				} else { // 否则说明原来已经登录过。
					String lastAddr = StringUtil.getValueString(oldSession.getLoginAddress(), "");
					String lastAgent = StringUtil.getValueString(oldSession.getLoginClient(), "");
					if (!CommonConfig.getInstance().getAllowMultipleLogin() && !tsimflag && !remoteAddr.startsWith("127.0.0.1") && lastAddr.length() > 0 && lastAddr.indexOf(remoteAddr) < 0) { throw new Exception("“" + ParticipantHelper.getFormatValue(user, "ou0\\cn") + "”已经从“" + lastAddr.replace(Session.ADDRESS_AND_CLIENT_SEP, ';') + "”通过“" + lastAgent.replace(Session.ADDRESS_AND_CLIENT_SEP, ';') + "”登录！"); }
					if (CommonConfig.getInstance().getDebugable()) System.out.println(String.format("%1$s:继续用户“%2$s”的会话", DateTime.getNowDTString(), ParticipantHelper.getFormatValue(user, "ou0\\cn")));
				}
				Session session = Session.newSession(user, true);
				if (session == null) throw new RuntimeException("无法创建用户会话，请联系提供商！");
				if (tsimflag) session.setTsimLoginInfo(addr, TSIM_PRESENCE_ONLINE, true); // 设置小助手相关信息
				session.appendLoginInfo(userAgent, remoteAddr); // 设置客户端程序、地址等
				session.setHttpRequest(request);

				// 将用户自定义会话存入httpsession
				httpsession.setAttribute(Session.USERSESSION_PARAM_NAME_IN_HTTPSESSION, session);

				// 记住密码处理
				Cookie cookie = new Cookie(SAVE_PWD_COOKIE_NAME, Escape.escape(savedPwd != null && savedPwd.length() > 0 ? savedPwd : ""));
				cookie.setPath(request.getContextPath()); // 在根路径下创建
				if (savedPwd != null && savedPwd.length() > 0) cookie.setMaxAge(3600 * 24 * 365); // cookie有效时间：一年
				else cookie.setMaxAge(0); // cookie有效时间：一年
				response.addCookie(cookie);
				// 登录完成事件
				EventSourceImpl eventSource = new EventSourceImpl();
				EventRegister.registerEventHandler(eventSource, EventHandler.EHT_ONLOGIN, EventHandlerInfoProvider.getProvider(EventCategory.Login).provide(null, null));
				eventSource.callEventHandler(this, EventHandler.EHT_ONLOGIN, new LoginEventArgs(request, response, baseCredential, session));

				// 如果没有所有者名称或系统没激活且为第一次登录那么重定向到激活页面
				String belong = CommonConfig.getInstance().getBelong();
				if (belong == null || belong.length() == 0) {
					redirectUrl = "register.jsp";
					response.sendRedirect(redirectUrl);
					return;
				}

				// 正常登录成功的转发或重定向
				if (forwardUrl != null && forwardUrl.length() > 0) {
					RequestDispatcher requestDispatcher = request.getRequestDispatcher(forwardUrl);
					requestDispatcher.forward(request, response);
				} else {
					response.sendRedirect(redirectUrl);
				}
				return;
			}
		} catch (Exception ex) {
			if (ex instanceof NullPointerException) FileLogger.error(ex);
			httpsession.setAttribute(Session.LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION, ex.getMessage());// 返回登录异常消息
			response.sendRedirect(loginUrl); // 返回登录页面
			return;
		}
	}

	/**
	 * 处理GET请求。
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * 是否小助手登录。
	 * 
	 * @param request
	 * @return boolean
	 */
	public static boolean checkTSIM(HttpServletRequest request) {
		if (request == null) return false;
		try {
			boolean result = StringUtil.getValueBool(request.getParameter("tsimflag"), false);
			if (!result) result = Browser.getBrowser(request) == Browser.TSIM;
			return result;
		} catch (NullPointerException ex) {
		}
		return false;
	}

	/**
	 * 解包并获取sid对应的结果中的账号和密码信息。
	 * 
	 * @param sid
	 * @return 返回账号(key)和密码(value)对应的{@link StringPair}。
	 */
	protected static StringPair unwrapCredentialPair(String sid) {
		if (sid == null || sid.length() <= 16) return null;
		StringPair sp = null;
		String keyHex = sid.substring(0, 16);
		String encryptedHex = sid.substring(16);

		byte keyBs[] = StringUtil.fromHexString(keyHex);
		if (keyBs.length != 8) throw new RuntimeException("账号密码信息异常，错误码：0！");
		byte bs[] = new byte[8];

		for (int i = 4; i < 8; i++) {
			keyBs[i] ^= keyBs[i - 4];
		}
		int idx = 7;
		for (int i = 0; i < 8; i++) {
			bs[idx--] = keyBs[i];
		}

		long keyLong = ByteBuffer.wrap(bs).getLong();
		String keyStr = keyLong + "";
		int keyStrLen = keyStr.length();
		Crypt c = null;
		String decrypted = null;
		try {
			c = new Crypt(keyStr.substring(keyStrLen - 8));
			byte[] bsDecrypted = c.decrypt(StringUtil.fromHexString(encryptedHex));
			decrypted = new String(bsDecrypted, "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("账号密码信息异常，错误码：1！");
		} catch (Exception e) {
			throw new RuntimeException("账号密码信息异常，错误码：9！");
		}

		if (decrypted != null && decrypted.length() > 0 && decrypted.indexOf(":") > 0) {
			String key = StringUtil.stringLeft(decrypted, ":");
			String val = StringUtil.stringRight(decrypted, ":");
			sp = new StringPair(key, val);
		}

		return sp;
	}
}

