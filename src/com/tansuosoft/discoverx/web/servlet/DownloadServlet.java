/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.MIMEContentTypeConfig;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.ThrowableUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.Browser;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.accessory.AccessoryContainerHelper;
import com.tansuosoft.discoverx.web.accessory.AccessoryForm;
import com.tansuosoft.discoverx.web.accessory.FileOutput;

/**
 * 表示根据附加文件unid和其所属资源unid，下载附加文件内容的{@link HttpServlet}。
 * 
 * <p>
 * 如果通过名为“fn”的请求参数提供了有效一个文件名，那么从临时附加文件目录读取对应文件内容并提供给用户下栽。<br/>
 * 临时文件下栽完毕后将被删除。
 * </p>
 * <p>
 * 将触发附加文件资源的打开前（在实际文件内容输出前触发）和打开后（在实际文件内容输出后触发）事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父资源。 <br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DownloadServlet extends HttpServlet {

	/**
	 * 序列化版本号。
	 */
	private static final long serialVersionUID = 5502334992632999237L;

	/**
	 * 重载doPost:下载附加文件内容。
	 * 
	 * <p>
	 * 出错则重定向到json错误页。
	 * </p>
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fullPath = null;
		boolean isTemp = false;
		Session s = null;
		try {
			AccessoryForm form = new AccessoryForm();
			form.fillWebRequestForm(request);
			s = SessionContext.getSession(request);
			User u = Session.getUser(s);
			Resource parent = null;
			Accessory accessory = null;
			String pdir = form.getParentDirectory();
			if (pdir == null || pdir.length() == 0) pdir = ResourceDescriptorConfig.DOCUMENT_DIRECTORY;
			String punid = form.getParentUNID();
			String unid = form.getUNID();
			if (punid == null || punid.length() == 0 || unid == null || unid.length() == 0) {
				String fn = request.getParameter("fn");
				if (fn != null && fn.length() > 0) {
					String tempFolder = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath();
					fullPath = tempFolder + fn;
					GenericPair<Resource, Accessory> pair = AccessoryContainerHelper.getTempFileDownloadInfo(fullPath, u.getSecurityCode());
					if (pair != null) {
						parent = pair.getKey();
						accessory = pair.getValue();
						isTemp = true;
					} else {
						fullPath = null;
					}
				} else {
					throw new RuntimeException("没有提供必要的参数！");
				}
			} else {
				if (parent == null && pdir.equalsIgnoreCase(ResourceDescriptorConfig.DOCUMENT_DIRECTORY)) parent = DocumentBuffer.getInstance().getDocument(punid);
				if (parent == null) parent = ResourceContext.getInstance().getResource(punid, pdir);
				if (parent == null) throw new RuntimeException("无法获取附加文件资源的容器资源！");
				AccessoryContainerHelper containerHelper = new AccessoryContainerHelper(parent, s);
				accessory = containerHelper.getAccessory(unid);
				if (accessory == null) throw new RuntimeException("找不到资源“" + parent.getName() + "”中“" + unid + "”对应的附加文件！");
				fullPath = AccessoryPathHelper.getAbsoluteAccessoryServerPath(parent) + accessory.getFileName();
			}
			boolean authorized = SecurityHelper.authorize(s, accessory, parent, 3, SecurityLevel.View);
			if (!authorized) throw new RuntimeException("对不起，您没有下载此文件的权限！");
			if (fullPath == null || fullPath.length() == 0) throw new RuntimeException("下载文件路径未知！");
			// 添加事件处理程序。
			EventSourceImpl esi = new EventSourceImpl();
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(accessory, null);
			EventRegister.registerEventHandler(esi, String.format("%1$s,%2$s", EventHandler.EHT_OPEN, EventHandler.EHT_OPENED), eventHandlerInfoList);
			ResourceEventArgs e = new ResourceEventArgs(accessory, s);
			esi.callEventHandler(parent, EventHandler.EHT_OPEN, e); // 打开事件。
			FileOutput outputImpl = null;
			String outputType = MIMEContentTypeConfig.getInstance().checkOutput(fullPath);
			if (outputType != null && outputType.length() > 0 && !"string".equalsIgnoreCase(outputType)) {
				outputImpl = Instance.newInstance(outputType, FileOutput.class);
			}
			if (outputImpl != null) {
				if (outputImpl.output(fullPath, response)) esi.callEventHandler(parent, EventHandler.EHT_OPENED, e); // 打开完成事件。
			} else {
				if (FileOutput.outputToResponse(fullPath, response)) esi.callEventHandler(parent, EventHandler.EHT_OPENED, e); // 打开完成事件。
			}
		} catch (Exception ex) {
			Session.setSessionParam(s, request.getSession(), Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, ex);
			PathContext p = new PathContext(request);
			if (!(ex instanceof RuntimeException || ex.getClass().getName().equals(Exception.class.getName()))) {
				FileLogger.error(ex);
			} else {
				FileLogger.debug("%1$s 请求地址：%2$s", ThrowableUtil.getRootThrowable(ex).getMessage(), StringUtil.getValueString(request.getQueryString(), ""));
			}
			Browser b = Browser.getBrowser(request);
			try {
				// 如果是客户端，则输出json
				if (b == Browser.TSHTTP || b == Browser.AJAX || b == Browser.TSIM) {
					String url = p.parseUrl(UrlConfig.getInstance().getUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE));
					response.sendRedirect(url);
				} else {
					// 否则输出提示框
					String url = p.parseUrl(UrlConfig.getInstance().getUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE));
					response.sendRedirect(url);
				}
			} catch (Exception e) {
				FileLogger.error(e);
			}
		} finally {
			if (isTemp) {
				File f = new File(fullPath);
				if (f.exists() && !f.delete()) f.deleteOnExit();
			}
		}
	}

	/**
	 * 处理GET请求:下载附加文件内容。
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}

