/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.UserActivityEventArgs;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.HttpUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.servlet.LoginServlet;

/**
 * Http会话相关事件处理程序实现类。
 * 
 * @author coca@tensosoft.com
 */
public class SessionListener extends EventSourceImpl implements javax.servlet.http.HttpSessionAttributeListener, HttpSessionListener {
	/**
	 * 缺省构造器。
	 */
	public SessionListener() {
	}

	/**
	 * Http会话中增加属性时触发的事件：根据要求触发用户登录事件。
	 * 
	 * @see javax.servlet.http.HttpSessionAttributeListener#attributeAdded(HttpSessionBindingEvent)
	 */
	public void attributeAdded(HttpSessionBindingEvent arg0) {
		String name = arg0.getName();
		if (Session.USERSESSION_PARAM_NAME_IN_HTTPSESSION.equalsIgnoreCase(name)) {
			Session s = (Session) arg0.getValue();
			if (s == null) return;
			Session.newSession(s.getUser());
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.User).provide(null, null);
			EventRegister.registerEventHandler(this, EventHandler.EHT_USERLOGIN, eventHandlerInfoList);
			UserActivityEventArgs e = new UserActivityEventArgs(s);
			this.callEventHandler(EventHandler.EHT_USERLOGIN, e);

			DateTime dt = new DateTime();
			HttpServletRequest request = s.getHttpRequest();
			String address = StringUtil.getValueString((request == null ? s.getLoginAddress() : HttpUtil.getRemoteAddr(request)), HttpUtil.UNKNOWN_REMOTE_ADDRESS);
			Browser b = (request == null ? Browser.getBrowser(s.getLoginClient()) : Browser.getBrowser(HttpUtil.getClient(request)));
			String agent = StringUtil.getValueString(Browser.getBrowserVersionName(b), HttpUtil.UNKNOWN_CLIENT);
			CommonConfig cc = CommonConfig.getInstance();
			String un = ParticipantHelper.getFormatValue(Session.getUser(s), "ou0\\cn").replace(cc.getBelong() + "\\", "");
			if (cc.getDebugable()) {
				String str = String.format("%1$s:“%2$s”通过“%3$s”从“%4$s”登录“%5$s”。", dt.toString(), un, agent, address, cc.getSystemNameAbbr());
				System.out.println(str);
			}
		}
	}

	/**
	 * Http会话中删除属性时触发的事件：根据要求触发用户注销事件。
	 * 
	 * @see javax.servlet.http.HttpSessionAttributeListener#attributeRemoved(HttpSessionBindingEvent)
	 */
	public void attributeRemoved(HttpSessionBindingEvent arg0) {
		String name = arg0.getName();
		// 用户注销
		if (Session.USERSESSION_PARAM_NAME_IN_HTTPSESSION.equalsIgnoreCase(name)) {
			Session s = (Session) arg0.getValue();
			if (s == null) return;
			Profile.clear(s);
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.User).provide(null, null);
			EventRegister.registerEventHandler(this, EventHandler.EHT_USERLOGOUT, eventHandlerInfoList);
			UserActivityEventArgs e = new UserActivityEventArgs(s);
			this.callEventHandler(EventHandler.EHT_USERLOGOUT, e);

			DateTime dt = new DateTime();

			HttpServletRequest request = s.getHttpRequest();
			String address = StringUtil.getValueString(HttpUtil.getRemoteAddr(request), HttpUtil.UNKNOWN_REMOTE_ADDRESS);
			String agent = StringUtil.getValueString(HttpUtil.getClient(request), HttpUtil.UNKNOWN_CLIENT);
			s.removeLoginInfo(agent, address);
			if (LoginServlet.checkTSIM(request)) s.setTsimLoginInfo(s.getTsimAddress(), 0, false);
			User u = Session.getUser(s);
			Document doc = s.getLastDocument();
			if (doc != null) DocumentBuffer.getInstance().removeHolder(doc.getUNID(), u.getName());
			CommonConfig cc = CommonConfig.getInstance();
			String un = ParticipantHelper.getFormatValue(u, "ou0\\cn").replace(cc.getBelong() + "\\", "");
			if (cc.getDebugable()) {
				String str = null;
				if (HttpUtil.UNKNOWN_REMOTE_ADDRESS.equalsIgnoreCase(address) && HttpUtil.UNKNOWN_CLIENT.equalsIgnoreCase(agent)) {
					str = String.format("%1$s:“%2$s”对应的登录会话被“%3$s”回收。", dt.toString(), un, cc.getSystemNameAbbr());
				} else {
					str = String.format("%1$s:“%2$s”通过“%3$s”从“%4$s”注销“%5$s”。", dt.toString(), un, Browser.getBrowserVersionName(Browser.getBrowser(agent)), address, cc.getSystemNameAbbr());
				}
				System.out.println(str);
			}
			Session.removeSession(s.getSessionUNID());
		}
	}

	/**
	 * Http会话中修改属性值时触发的事件：不做任何操作。
	 * 
	 * @see javax.servlet.http.HttpSessionAttributeListener#attributeReplaced(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void attributeReplaced(HttpSessionBindingEvent arg0) {

	}

	/**
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		HttpSession s = se.getSession();
		Session.getAllHttpSession().put(s.getId(), s);
	}

	/**
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSession s = se.getSession();
		Session.getAllHttpSession().remove(s.getId());
	}
}

