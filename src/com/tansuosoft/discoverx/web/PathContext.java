/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * Web路径相关的工具类，用于获取各类Web资源的路径。
 * 
 * @author coca@tansuosoft.cn
 */
public class PathContext {
	private HttpServletRequest m_request = null; // 用于获取相关路径信息。
	private Portal m_portal = null; // 当前门户信息。

	/**
	 * 缺省构造器。
	 */
	protected PathContext() {

	}

	/**
	 * 接收Http请求的构造器，建议优先使用。
	 */
	public PathContext(HttpServletRequest request) {
		this.m_request = request;
		Session s = SessionContext.getSession(this.m_request);
		User u = Session.getUser(s);
		m_portal = Profile.getInstance(u).getUsePortal();
	}

	/**
	 * 获取当前web应用程序url根路径。 如“http://localhost/discoverx/”这个应用的返回结果为“/discoverx/”。
	 * 
	 * @return String
	 */
	public String getWebAppRoot() {
		return this.m_request == null ? null : this.m_request.getContextPath().concat("/");
	}

	/**
	 * 获取当前web应用程序通用资源url路径， 如“http://localhost/discoverx/”这个应用程序的返回结果为“/discoverx/common/”。
	 * 
	 * @return String
	 */
	public String getCommonPath() {
		return this.m_request == null ? null : this.m_request.getContextPath() + "/common/";
	}

	/**
	 * 获取当前web应用程序通用图像资源url路径， 如“http://localhost/discoverx/”这个应用程序的返回结果为“/discoverx/common/images/”
	 * 
	 * @return String
	 */
	public String getCommonImagesPath() {
		return this.m_request == null ? null : this.m_request.getContextPath() + "/common/images/";
	}

	/**
	 * 获取当前web应用程序通用图标资源url路径， 如“http://localhost/discoverx/”这个应用程序的返回结果为“/discoverx/common/icons/”
	 * 
	 * @return String
	 */
	public String getCommonIconsPath() {
		return this.m_request == null ? null : this.m_request.getContextPath() + "/common/icons/";
	}

	/**
	 * 获取当前web应用程序脚本资源url路径， 如“http://localhost/discoverx/”这个应用程序的返回结果为“/discoverx/common/js/”。
	 * 
	 * @return String
	 */
	public String getCommonJSPath() {
		return this.m_request == null ? null : this.m_request.getContextPath() + "/common/js/";
	}

	/**
	 * 获取门户框架路径。
	 * 
	 * @return String
	 */
	public String getFramePath() {
		String webAppRoot = this.getWebAppRoot();
		String frameName = (m_portal == null ? Portal.DEFAULT_FRAME_NAME : m_portal.getFrame());
		if (StringUtil.isBlank(frameName)) frameName = Portal.DEFAULT_FRAME_NAME;
		return webAppRoot + frameName + "/";
	}

	/**
	 * 获取门户框架JS路径。
	 * 
	 * @return String
	 */
	public String getFrameJSPath() {
		String framePath = this.getFramePath();
		return framePath + "js/";
	}

	/**
	 * 获取门户主题路径。
	 * 
	 * @return String
	 */
	public String getThemePath() {
		String framePath = this.getFramePath();
		String userThemePath = (m_portal == null ? Portal.DEFAULT_THEME_NAME : m_portal.getTheme());
		if (StringUtil.isBlank(userThemePath)) userThemePath = Portal.DEFAULT_THEME_NAME;
		return framePath + userThemePath + "/";
	}

	/**
	 * 获取门户主题图片路径。
	 * 
	 * @return String
	 */
	public String getThemeImagesPath() {
		String themePath = this.getThemePath();
		return themePath + "images/";
	}

	/**
	 * 获取消息路径。
	 * 
	 * @return
	 */
	public String getMessagePath() {
		String base = this.getWebAppRoot();
		return base + "message/";
	}

	/**
	 * 合并firstPart、secondPart所指定的url路径，并返回合并结果。
	 * 
	 * @param firstPart
	 * @param secondPart
	 * @return String
	 */
	public static String combineUrl(String firstPart, String secondPart) {
		String first = firstPart == null ? "" : firstPart;
		String second = secondPart == null ? "" : secondPart;
		first = first.replace('\\', '/');
		second = second.replace('\\', '/');
		String result = (first + second).replace("//", "/");
		return result;
	}

	/**
	 * 合并Web请求主机url地址和secondPart所指定的url路径，并返回合并结果。 比如http://localhost/为Web请求主机url地址，传入的secondPart为“/discoverx/common/images/”， 则返回“http://localhost/discoverx/common/images/”。
	 * 
	 * @param secondPart
	 * @return String
	 */
	public String combineUrl(String secondPart) {
		if (this.m_request == null) return null;
		String first = m_request.getScheme() + "://" + m_request.getServerName() + (m_request.getServerPort() == 80 ? "" : ":" + m_request.getServerPort()) + "/";
		return combineUrl(first, secondPart);
	}

	/**
	 * 返回门户首页额外引用的JS。
	 * 
	 * <p>
	 * 返回的js文件的url地址将在门户首页的body底部被自动引用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getPortalLevel1Js() {
		String url = (m_portal == null ? null : m_portal.getPortalLevel1Js());
		return parseUrl(url);
	}

	/**
	 * 返回门户二级页额外引用的JS。
	 * 
	 * <p>
	 * 返回的js文件的url地址将在门户二级页面的body底部被自动引用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getPortalLevel2Js() {
		String url = (m_portal == null ? null : m_portal.getPortalLevel2Js());
		return parseUrl(url);
	}

	/**
	 * 返回门户首页额外引用的CSS。
	 * 
	 * <p>
	 * 返回的css文件的url地址将在门户首页的head区被自动引用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getPortalLevel1Css() {
		String url = (m_portal == null ? null : m_portal.getPortalLevel1Css());
		return parseUrl(url);
	}

	/**
	 * 返回门户二级页额外引用的CSS。
	 * 
	 * <p>
	 * 返回的css文件的url地址将在二级页面的head区被自动引用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getPortalLevel2Css() {
		String url = (m_portal == null ? null : m_portal.getPortalLevel2Css());
		return parseUrl(url);
	}

	/**
	 * 解析路径（把rawUrl中路径变量替换为具体路径值）。
	 * 
	 * <p>
	 * 支持的变量信息如下：
	 * <ul>
	 * <li>“~”：表示{@link PathContext#getWebAppRoot()}的结果。</li>
	 * <li>“frame”：表示{@link PathContext#getFramePath()}的结果。</li>
	 * <li>“theme”：表示{@link PathContext#getThemePath()}的结果。</li>
	 * <li>“common”：表示{@link PathContext#getCommonPath()}的结果。</li>
	 * <li>“message”：表示{@link PathContext#getMessagePath()}的结果。</li>
	 * </ul>
	 * </p>
	 * 
	 * @param rawUrl
	 * @return 返回解析好的路径。
	 */
	public String parseUrl(String rawUrl) {
		if (rawUrl == null || rawUrl.length() == 0) return rawUrl;
		String result = null;
		String root = this.getWebAppRoot();
		String frame = this.getFramePath();
		String theme = this.getThemePath();
		String common = this.getCommonPath();
		String message = this.getMessagePath();
		result = rawUrl.replace("~", root).replace("{frame}", frame).replace("{theme}", theme).replace("{common}", common).replace("{message}", message);
		return result;
	}

}

