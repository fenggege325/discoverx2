/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.view.FormViewTable;
import com.tansuosoft.discoverx.common.Agent;
import com.tansuosoft.discoverx.common.AgentConfig;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.EventHandlerConfig;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.HttpContext;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.Path;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.app.message.UrgeContext;
import com.tansuosoft.discoverx.web.ui.admin.UpdateSystem;

/**
 * web应用程序相关事件处理程序实现类。
 * 
 * @author coca@tensosoft.com
 */
public class ContextListener implements javax.servlet.ServletContextListener, ServletContextAttributeListener {
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	/**
	 * 缺省构造器。
	 */
	public ContextListener() {
	}

	/**
	 * 重载contextDestroyed
	 * 
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		fireEvent(EventHandler.EHT_ONSYSTEMSHUTDOWN);
		try {
			scheduler.shutdownNow();
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		FormViewTable.stopMonitor();
		SessionContext.shutdown();
		HttpContext.shutdown();
		UrgeContext.getInstance().shutdown();
		DBRequest.shutdown();
		System.out.println(DateTime.getNowDTString() + " " + arg0.getServletContext().getServletContextName() + "终止。");
		FileLogger.close();
		if (updaterProcess != null) {
			updaterProcess.destroy();
			updaterProcess = null;
		}
	}

	/**
	 * 重载contextInitialized
	 * 
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		String p = Path.WEBAPP_ROOT;
		boolean hasValidatePath = true;
		if (p == null || p.length() == 0) hasValidatePath = false;
		if (hasValidatePath) hasValidatePath = Path.checkPathExists(p);
		if (!hasValidatePath) Path.WEBAPP_ROOT = arg0.getServletContext().getRealPath("/");
		if (!Path.checkPathExists(Path.WEBAPP_ROOT)) {
			final String installationLocationParamName = "installationLocation";
			String paramValue = arg0.getServletContext().getInitParameter(installationLocationParamName);
			if (paramValue != null && paramValue.length() > 0 && Path.checkPathExists(paramValue)) Path.WEBAPP_ROOT = paramValue;
		}
		if (!Path.checkPathExists(Path.WEBAPP_ROOT)) { throw new RuntimeException("由于无法获取安装路径系统将无法正常使用，请联系管理员！"); }
		File logsDir = new File(Path.WEBAPP_ROOT + "logs");
		if (!logsDir.exists()) logsDir.mkdir();
		CommonConfig.getInstance().init(arg0.getServletContext());
		System.out.println(DateTime.getNowDTString() + " " + CommonConfig.getInstance().getSystemNameAbbr() + "(" + Path.WEBAPP_ROOT + ")启动...");
		UrgeContext.getInstance();
		// 视图表同步监控
		FormViewTable.startMonitor();
		// 定时代理初始化
		try {
			StringBuilder logs = new StringBuilder();
			Agent[] agents = AgentConfig.getInstance().getAllAgents();
			if (agents != null && agents.length > 0) {
				int aidx = 0;
				String desc = null;
				String title = null;
				long period = 0L;
				long delay = 0L;
				for (Agent a : agents) {
					if (a == null) continue;
					AgentWrapper aw = new AgentWrapper(a);
					period = aw.getPeriod();
					delay = aw.getInitialDelay();
					if (period == 0L) {
						scheduler.schedule(aw, delay, TimeUnit.SECONDS);
					} else {
						if (aw.getAbsolute()) {
							scheduler.scheduleAtFixedRate(aw, delay, period, TimeUnit.SECONDS);
						} else {
							scheduler.scheduleWithFixedDelay(aw, delay, period, TimeUnit.SECONDS);
						}
					}
					desc = aw.getDescription();
					desc = (desc != null && desc.length() > 0 ? "(" + desc + ")" : "");
					title = String.format("%1$s[延迟:%2$d秒,间隔:%3$d秒]", aw.getName(), delay, period);
					logs.append("\r\n初始化" + CommonConfig.getInstance().getSystemNameAbbr() + "的第" + (++aidx) + "个定时任务:" + title + desc + "...");
				}
			}
			if (logs.length() > 0) FileLogger.debug(logs.toString());
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		if (CommonConfig.getInstance().getValueBool("global.autoStartUpdater", true)) {
			updaterThread = new Thread(startUpdater);
			updaterThread.start();
		}
		// 第一次运行时执行的代码
		File firstRun = new File(CommonConfig.getInstance().getInstallationPath() + "WEB-INF" + File.separator + "classes" + File.separator + "firstrun");
		if (!firstRun.exists()) {
			FileOutputStream fs = null;
			try {
				fs = new FileOutputStream(firstRun);
				fs.write("这个文件存在则表示已经执行过第一次启动事件。".getBytes("GBK"));
				fs.flush();
				fs.close();
			} catch (Exception e) {
				FileLogger.error(e);
			}
			fireEvent(EventHandler.EHT_ONSYSTEMINIT);
		}// if end
		fireEvent(EventHandler.EHT_ONSYSTEMSTARTED);
	}

	/**
	 * 触发指eventType定类型的事件执行。
	 * 
	 * @param eventType
	 */
	protected void fireEvent(String eventType) {
		if (eventType == null || eventType.isEmpty()) return;
		List<EventHandlerInfo> ehis = EventHandlerConfig.getInstance().getEventHandlers();
		if (ehis == null || ehis.isEmpty()) return;
		for (EventHandlerInfo ehi : ehis) {
			if (ehi != null && eventType.equalsIgnoreCase(ehi.getEventHandlerType()) && !StringUtil.isBlank(ehi.getEventHandler())) {
				EventHandler eh = Instance.newInstance(ehi.getEventHandler(), EventHandler.class);
				if (eh == null) return;
				try {
					FileLogger.debug("触发\"%1$s\"类型的事件处理程序:%2$s", eventType, eh.getClass().getName());
					eh.handle(this, new EventArgs());
				} catch (Exception e) {
					FileLogger.error(e);
				}
			}// if end
		}// for end
	}

	protected static Process updaterProcess = null;
	protected static Thread updaterThread = null;
	Runnable startUpdater = new Runnable() {
		@Override
		public void run() {
			try {
				String updaterJar = System.getProperty("java.home") + File.separator + "lib" + File.separator + "ext" + File.separator + "tswebappupdater.jar";
				File updaterJarFile = new File(updaterJar);
				if (!updaterJarFile.exists() || !updaterJarFile.isFile()) {
					updaterJar = System.getProperty("java.home") + File.separator + "jre" + File.separator + "lib" + File.separator + "ext" + File.separator + "tswebappupdater.jar";
					updaterJarFile = new File(updaterJar);
				}
				if (!updaterJarFile.exists() || !updaterJarFile.isFile()) throw new Exception("找不到更新程序:" + updaterJar + "。");
				String webUpdateDir = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath() + UpdateSystem.WEBUPDATE_DIR;
				File webUpdate = new File(webUpdateDir);
				webUpdate.mkdirs();
				String cmd = String.format("java -jar \"%1$s\" \"%2$s\"", updaterJar, webUpdateDir);
				updaterProcess = Runtime.getRuntime().exec(cmd);
				System.out.println(DateTime.getNowDTString() + " 启动" + CommonConfig.getInstance().getSystemNameAbbr() + "系统更新进程:" + cmd);
				while (updaterProcess != null) {
					BufferedReader is = new BufferedReader(new InputStreamReader(updaterProcess.getInputStream(), "gbk"));
					BufferedReader err = new BufferedReader(new InputStreamReader(updaterProcess.getErrorStream(), "gbk"));
					String line = null;
					while ((line = is.readLine()) != null) {
						System.out.println(line);
					}
					while ((line = err.readLine()) != null) {
						System.out.println(line);
					}
					Thread.sleep(300);
				}
			} catch (Exception ex) {
				FileLogger.error(ex);
			}
		}
	};

	/**
	 * @see javax.servlet.ServletContextAttributeListener#attributeAdded(javax.servlet.ServletContextAttributeEvent)
	 */
	@Override
	public void attributeAdded(ServletContextAttributeEvent scab) {

	}

	/**
	 * @see javax.servlet.ServletContextAttributeListener#attributeRemoved(javax.servlet.ServletContextAttributeEvent)
	 */
	@Override
	public void attributeRemoved(ServletContextAttributeEvent scab) {

	}

	/**
	 * @see javax.servlet.ServletContextAttributeListener#attributeReplaced(javax.servlet.ServletContextAttributeEvent)
	 */
	@Override
	public void attributeReplaced(ServletContextAttributeEvent scab) {

	}
}
