/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.util.EnumBase;
import com.tansuosoft.discoverx.util.HttpUtil;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 表示访问服务器的浏览器或客户端类型枚举。
 * 
 * @author coca@tansuosoft.cn
 */
public enum Browser implements EnumBase {
	/**
	 * IE浏览器（1）。
	 */
	MSIE(1),
	/**
	 * Chrome 浏览器（2）。
	 */
	CHROME(2),
	/**
	 * Firefox 浏览器（3）。
	 */
	FIREFOX(3),
	/**
	 * Safari 浏览器（4）。
	 */
	SAFARI(4),
	/**
	 * Opera 浏览器（5）。
	 */
	OPERA(5),
	/**
	 * 办公助手（6）。
	 */
	TSIM(6),
	/**
	 * 客户端组件（7）。
	 */
	TSHTTP(7),
	/**
	 * 客户端组件（8）。
	 */
	AJAX(8),
	/**
	 * 其它类型客户端（0）。
	 */
	OTHERS(0);

	protected static final String[] UA_SHORT_NAMES = { "Others", "MSIE", "Chrome", "Firefox", "Safari", "Opera", "TSIM", "TSHTTP", "AJAX" };
	protected static final String[] UA_ENUM_NAMES = { "OTHERS", "MSIE ", "CHROME/", "FIREFOX/", "SAFARI/", "OPERA/", "TSIM/", "TSHTTP/", "AJAX/" };
	private final int m_intValue; // 对应的int值。
	private int m_version = 0; // 主版本号

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	Browser(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 获取主版本号。
	 * 
	 * @return int 返回浏览器对应的主版本号。
	 */
	public int getVersion() {
		return m_version;
	}

	/**
	 * 检查当前{@link Browser}对象是否指定版本。
	 * 
	 * @param version
	 * @return boolean
	 */
	public boolean checkVersion(int version) {
		return (m_version == version);
	}

	/**
	 * 检查当前{@link Browser}对象是否在指定的版本区间内。
	 * 
	 * @param versionStart 版本区间中的小版本号(包括此版本号)。
	 * @param versionEnd 版本区间中的大版本号(包括此版本号)。
	 * @return boolean
	 */
	public boolean checkVersion(int versionStart, int versionEnd) {
		return (m_version >= versionStart && m_version <= versionEnd);
	}

	/**
	 * 检查指定{@link Browser}对象是否指定浏览器的指定版本。
	 * 
	 * @param check
	 * @param version 如果为0，则只检查浏览器而不检查版本。
	 * @return boolean
	 */
	public boolean check(Browser check, int version) {
		return (m_intValue == check.m_intValue && (version > 0 ? m_version == version : true));
	}

	/**
	 * 检查指定{@link Browser}对象是否指定浏览器且在指定的版本区间。
	 * 
	 * @param check
	 * @param versionStart 版本区间中的小版本号(包括此版本号)。
	 * @param versionEnd 版本区间中的大版本号(包括此版本号)。
	 * @return boolean
	 */
	public boolean check(Browser check, int versionStart, int versionEnd) {
		return (m_intValue == check.m_intValue && (versionStart > 0 ? m_version >= versionStart : true) && (versionEnd > 0 ? m_version <= versionEnd : true));
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return Browser
	 */
	public Browser parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (Browser s : Browser.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return Browser.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return Browser
	 */
	public static Browser parse(int v) {
		for (Browser s : Browser.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

	/**
	 * 根据传入的http请求信息获取浏览器类型和版本信息。
	 * 
	 * @param request
	 * @return Browser
	 */
	public static Browser getBrowser(HttpServletRequest request) {
		if (request == null) return Browser.OTHERS;
		String ua = request.getHeader("User-Agent");
		return getBrowser(ua);
	}

	static final String RV = "RV:";
	static final String VERSION = "VERSION/";
	static final String TRIDENT = "TRIDENT/";

	/**
	 * 根据传入的浏览器UserAgent信息获取浏览器类型和版本信息。
	 * 
	 * @param userAgent
	 * @return Browser
	 */
	public static Browser getBrowser(String userAgent) {
		String ua = (userAgent == null ? "" : userAgent.toUpperCase());
		Browser b = Browser.OTHERS;
		int pos = 0;
		int end = 0;
		boolean isIeGte11 = (ua.indexOf(TRIDENT) > 0 && ua.indexOf(RV) > 0);
		if ((pos = ua.indexOf("TSIM/")) >= 0) {
			b = Browser.TSIM;
			end = ua.indexOf('.', pos);
			if (end < 0) end = ua.indexOf(' ', pos);
		} else if ((pos = ua.indexOf("TSHTTP/")) >= 0) {
			b = Browser.TSHTTP;
		} else if ((pos = ua.indexOf("AJAX/")) >= 0) {
			b = Browser.AJAX;
		} else if ((pos = ua.indexOf("MSIE ")) > 0 || isIeGte11) {
			if (isIeGte11) pos = ua.indexOf(RV);
			b = Browser.MSIE;
		} else if ((pos = ua.indexOf("FIREFOX/")) > 0) {
			b = Browser.FIREFOX;
		} else if ((pos = ua.indexOf("CHROME/")) > 0) {
			b = Browser.CHROME;
		} else if ((pos = ua.indexOf("SAFARI/")) > 0 && ua.indexOf("CHROME/") < 0) {
			b = Browser.SAFARI;
			pos = ua.indexOf(VERSION);
		} else if ((pos = ua.indexOf("OPERA/")) >= 0) {
			b = Browser.OPERA;
		} else {
			b = Browser.OTHERS;
		}
		if (pos >= 0) {
			end = ua.indexOf('.', pos);
			if (end < 0) end = ua.indexOf(' ', pos);
			if (end > 0) {
				String n = (b == Browser.SAFARI ? VERSION : (isIeGte11 ? RV : UA_ENUM_NAMES[b.getIntValue()]));
				b.m_version = StringUtil.getValueInt(ua.substring(pos + n.length(), end), 0);
			}
		}
		return b;
	}

	/**
	 * 根据浏览器类型获取客户端浏览器名称。
	 * 
	 * @param b
	 * @return String
	 */
	public static String getBrowserShortName(Browser b) {
		String v = null;
		if (b == null) v = UA_SHORT_NAMES[0];
		v = UA_SHORT_NAMES[b.getIntValue()];
		return (v.equalsIgnoreCase(UA_SHORT_NAMES[0]) ? HttpUtil.UNKNOWN_CLIENT : v);
	}

	/**
	 * 根据浏览器类型获取客户端浏览器的短名称和主版本号。
	 * 
	 * @param b
	 * @return String
	 */
	public static String getBrowserVersionName(Browser b) {
		String v = null;
		if (b == null) v = UA_SHORT_NAMES[0];
		v = UA_SHORT_NAMES[b.getIntValue()];
		return (v.equalsIgnoreCase(UA_SHORT_NAMES[0]) ? HttpUtil.UNKNOWN_CLIENT : v + " v" + b.m_version);
	}

	/**
	 * 测试代码。
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		String uas[] = { "TSHTTP/2.0", "TSIM/2.0", "ajax/1.0", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.163 Safari/535.1", "Mozilla/5.0 (Windows NT 6.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1", "Mozilla/5.0 (Windows NT 6.1; rv:7.0.1) Gecko/20100101 Firefox/5.0.1", "Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 6.1; WOW64; Trident/5.0)", "Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.13221/25.623; U; en) Presto/2.5.25 Version/10.54", "maxthon/12" };
		for (String ua : uas) {
			Browser b = Browser.getBrowser(ua);
			System.out.println(Browser.getBrowserShortName(b) + ",version=" + b.getVersion());
			if (b.check(Browser.MSIE, 6)) System.out.println("ie version between 6-9!");
		}
	}
}

