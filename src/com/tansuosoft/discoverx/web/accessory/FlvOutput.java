/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.accessory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

/**
 * 输出flv格式动画内容的{@link FileOutput}实现类。
 * 
 * @author coca@tensosoft.com
 */
public class FlvOutput extends FileOutput {

	/**
	 * @see com.tansuosoft.discoverx.web.accessory.FileOutput#output(java.lang.String, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public boolean output(String filePath, HttpServletResponse response) {
		OutputStream output = null;
		try {
			String fn = filePath.substring(filePath.lastIndexOf(File.separatorChar) + 1);
			File file = new File(filePath);
			length = file.length();
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("video/x-flv");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + new String(fn.getBytes("GBK"), "ISO8859-1") + "\"");
			response.setHeader("Content-Length", length + "");

			flvStream = new BufferedInputStream(new FileInputStream(filePath));
			output = response.getOutputStream();
			stream(output, 0);
			return false;
		}// end try
		catch (Exception e) {
			throw new RuntimeException(e);
		}// catch end
		finally {
			try {
				if (output != null) {
					output.flush();
					output.close();
				}
				if (flvStream != null) flvStream.close();
			} catch (IOException e) {
			}
		}
	}

	protected static final int HEADER_LENGTH = 13;
	protected static final byte[] SIGNATURE = { 'F', 'L', 'V', 0x01 };
	protected long length;
	protected InputStream flvStream;

	protected byte[] readHeader(InputStream in) throws IOException {
		byte[] header = new byte[HEADER_LENGTH];
		// read header
		int len = HEADER_LENGTH;
		while (len > 0)
			len -= in.read(header, HEADER_LENGTH - len, len);
		// validate it
		for (int i = 0; i < SIGNATURE.length; i++)
			if (header[i] != SIGNATURE[i]) throw new IOException("invalid FLV signature");
		return header;
	}

	public void stream(OutputStream out, long pos) throws IOException, InterruptedIOException {
		if (pos <= 0) pos = HEADER_LENGTH;
		long len = length - pos;
		byte[] buf = new byte[16384];
		int packetSize = Integer.MAX_VALUE;
		try {
			// write FLV header
			byte[] header = readHeader(flvStream);
			out.write(header, 0, HEADER_LENGTH);
			// skip to start position
			pos -= HEADER_LENGTH; // header has already been read from stream
			while (pos > 0)
				pos -= flvStream.skip(pos);
			// stream the data
			while (len > 0) {
				long packetStartTime = System.currentTimeMillis();
				// write full packet
				int remaining = (int) Math.min(len, packetSize);
				int count = 0;
				while ((count = flvStream.read(buf, 0, Math.min(buf.length, remaining))) > 0) {
					try {
						out.write(buf, 0, count);
					} catch (IOException ioe) {
						throw new InterruptedException("streaming interrupted");
					}
					remaining -= count;
					len -= count;
				}
				long remainingTime = packetStartTime + 1000 - System.currentTimeMillis();
				if (remainingTime > 0) Thread.sleep(remainingTime);
			}
		} catch (InterruptedException ie) {
			InterruptedIOException iioe = new InterruptedIOException("streaming interrupted");
			iioe.bytesTransferred = (int) (length - len);
			throw iioe;
		} finally {
			try {
				flvStream.close();
			} catch (IOException ignore) {
			}
			try {
				out.close();
			} catch (IOException ignore) {

			}
		}// finally end
	}
}

