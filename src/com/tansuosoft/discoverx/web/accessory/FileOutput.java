/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.accessory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.MIMEContentTypeConfig;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 输出文件内容字节流到{@link javax.servlet.http.HttpServletResponse}指定的输出流的类。
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class FileOutput {

	/**
	 * 输出parent所包含的由resource指定的附加文件资源对应的文件内容到{@link javax.servlet.http.HttpServletResponse}。
	 * 
	 * @param resource
	 * @param parent
	 * @param response
	 */
	public static void outputToResponse(Resource resource, Resource parent, HttpServletResponse response) {
		try {
			if (resource == null || !(resource instanceof Accessory)) throw new RuntimeException("未知附加文件资源！");
			Accessory accessory = (Accessory) resource;
			String fn = accessory.getFileName();
			String fullPath = AccessoryPathHelper.getAbsoluteAccessoryServerPath(parent) + fn;
			outputToResponse(fullPath, response);
		}// end try
		catch (Exception e) {
			AJAXResponse ajaxReq = new AJAXResponse();
			response.setContentType("text/xml");
			ajaxReq.setMessage(e.getMessage());
			try {
				response.getWriter().print(ajaxReq.toXml());
			} catch (IOException ex) {
				FileLogger.error(ex);
			}
		}// catch end
	}// func end

	/**
	 * 输出filePath指定的路径对应的文件的内容到{@link javax.servlet.http.HttpServletResponse}。
	 * 
	 * @param filePath
	 * @param response
	 * @return boolean 如果文件全部内容写入response的输出缓存区，则返回true，否则返回false。
	 */
	public static boolean outputToResponse(String filePath, HttpServletResponse response) {
		FileInputStream is = null;
		ServletOutputStream output = null;
		String relativePath = null;
		try {
			if (filePath == null || filePath.length() == 0) throw new Exception("未知文件！");
			relativePath = filePath.replace(CommonConfig.getInstance().getAccessoryPath(), "");
			if (relativePath.length() == filePath.length()) {
				int pos = filePath.lastIndexOf(File.separatorChar);
				if (pos > 0 && pos < (filePath.length() - 1)) relativePath = filePath.substring(pos + 1);
			}
			String fn = filePath.substring(filePath.lastIndexOf(File.separatorChar) + 1);
			File file = new File(filePath);
			boolean isExist = file.exists();
			if (isExist) {
				long fl = file.length();
				if (fl == 0) throw new Exception("文件“" + relativePath + "”无内容！");
				output = response.getOutputStream();
				response.setContentType(MIMEContentTypeConfig.getInstance().check(filePath, MIMEContentTypeConfig.DEFAULT_MIME_CONTENTTYPE));
				response.setHeader("Content-Disposition", "inline;filename=" + new String(fn.getBytes("GBK"), "ISO8859-1"));
				response.setContentLength((int) fl);
				long sum = 0;
				int count, n = 8192;
				byte buffer[] = new byte[n];
				is = new FileInputStream(file);
				while ((count = is.read(buffer, 0, n)) != -1) {
					sum += count;
					output.write(buffer, 0, count);
				}
				return (sum == fl);
			} else {
				throw new Exception("找不到文件：" + relativePath + "！");
			}
		}// end try
		catch (Exception e) {
			String clazzName = e.getClass().getName();
			if ("org.apache.catalina.connector.ClientAbortException".equals(clazzName)) {
				FileLogger.debug("客户端取消了对文件“%1$s”的下载。", relativePath);
				return true;
			}
			throw new RuntimeException(e);
		}// catch end
		finally {
			try {
				if (is != null) is.close();
				if (output != null) {
					output.flush();
					output.close();
				}
			} catch (IOException e) {
			}
		}
	}// func end

	/**
	 * 删除fullPath指定的临时附加文件内容。
	 * 
	 * @param fullPath
	 * @return 删除成功则返回true，否则返回false（非临时附加文件或者暂时无法删除时）。
	 */
	public static boolean deleteTempFile(String fullPath) {
		if (fullPath == null || fullPath.length() == 0) return false;
		if (fullPath.startsWith(AccessoryPathHelper.getAbsoluteTempAccessoryServerPath())) {
			File f = new File(fullPath);
			if (f.delete()) return true;
			f.deleteOnExit();
			return false;
		}
		return false;
	}

	/**
	 * 输出filePath指定的路径对应的文件的内容到{@link javax.servlet.http.HttpServletResponse}。
	 * 
	 * <p>
	 * 有特殊输出格式要求的流可以继承此类并重载此输出方法。
	 * </p>
	 * 
	 * @param filePath
	 * @param response
	 * @return 成功则返回true，否则返回false.
	 */
	public abstract boolean output(String filePath, HttpServletResponse response);
}

