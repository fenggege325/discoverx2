/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.accessory;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.function.CommonForm;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.CRUD;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 通过http请求提交的附加文件处理信息类。
 * 
 * @see com.tansuosoft.discoverx.model.CRUD <P>
 * @author coca@tansuosoft.cn
 * 
 */
public class AccessoryForm extends CommonForm {
	/**
	 * 缺省构造器。
	 */
	public AccessoryForm() {
	}

	private String m_UNID = null; // 附加文件UNID。
	private String m_parentUNID = null; // 附加文件所属资源UNID，可选。
	private String m_parentDirectory = null; // 附加文件所属资源目录，可选。
	private String m_template = null; // 样式模板UNID，可选。
	private String m_codeTemplate = null; // 代码模板UNID，可选。
	private String m_name = null; // 名称，可选。
	private String m_fileName = null; // 文件名，可选。
	private int m_action = CRUD.Read.getIntValue(); // 附加文件操作标记，默认为查看。
	private String m_accessoryType = null; // 附加文件UNID，必须。
	private String m_alias = null; // 别名，可选
	private String m_description = null; // 描述，可选。
	private int m_size = 0; // 附加文件内容大小。

	/**
	 * 返回附加文件UNID。
	 * 
	 * <p>
	 * 从名为“target”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置附加文件UNID必。
	 * 
	 * <p>
	 * 从名为“target”的http请求参数中获取。
	 * </p>
	 * 
	 * @param UNID String
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回附加文件所属资源UNID，。
	 * 
	 * <p>
	 * 从名为“punid”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getParentUNID() {
		return this.m_parentUNID;
	}

	/**
	 * 设置附加文件所属资源UNID，可选。
	 * 
	 * <p>
	 * 从名为“punid”的http请求参数中获取。
	 * </p>
	 * 
	 * @param parentUNID String
	 */
	public void setParentUNID(String parentUNID) {
		this.m_parentUNID = parentUNID;
	}

	/**
	 * 返回附加文件所属资源目录，可选。
	 * 
	 * <p>
	 * 从名为“pdir”的http请求参数中获取，默认为文档资源的目录名。
	 * </p>
	 * 
	 * @return String
	 */
	public String getParentDirectory() {
		return this.m_parentDirectory;
	}

	/**
	 * 设置附加文件所属资源目录，可选。
	 * 
	 * <p>
	 * 从名为“pdir”的http请求参数中获取。
	 * </p>
	 * 
	 * @param parentDirectory String
	 */
	public void setParentDirectory(String parentDirectory) {
		this.m_parentDirectory = parentDirectory;
	}

	/**
	 * 返回所属样式模板附加文件资源UNID。
	 * 
	 * <p>
	 * 从名为“template”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTemplate() {
		return this.m_template;
	}

	/**
	 * 设置所属样式模板附加文件资源UNID，可选。
	 * 
	 * <p>
	 * 从名为“template”的http请求参数中获取。
	 * </p>
	 * 
	 * @param template String
	 */
	public void setTemplate(String template) {
		this.m_template = template;
	}

	/**
	 * 返回所属代码模板附加文件资源UNID。
	 * 
	 * <p>
	 * 从名为“code”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getCodeTemplate() {
		return this.m_codeTemplate;
	}

	/**
	 * 设置所属代码模板附加文件资源UNID，可选。
	 * 
	 * <p>
	 * 从名为“code”的http请求参数中获取。
	 * </p>
	 * 
	 * @param codeTemplate String
	 */
	public void setCodeTemplate(String codeTemplate) {
		this.m_codeTemplate = codeTemplate;
	}

	/**
	 * 返回名称，可选。
	 * 
	 * <p>
	 * 从名为“name”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置名称，可选。
	 * 
	 * <p>
	 * 从名为“name”的http请求参数中获取。
	 * </p>
	 * 
	 * @param name String
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回文件名，可选。
	 * 
	 * <p>
	 * 从名为“fn”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getFileName() {
		return this.m_fileName;
	}

	/**
	 * 设置文件名，可选。
	 * 
	 * <p>
	 * 从名为“fn”的http请求参数中获取。
	 * </p>
	 * 
	 * @param fileName String
	 */
	public void setFileName(String fileName) {
		this.m_fileName = fileName;
	}

	/**
	 * 返回附加文件操作标记，默认为{@link CRUD#Read#getIntValue()}。
	 * 
	 * <p>
	 * 从名为“action”的http请求参数中获取。
	 * </p>
	 * <p>
	 * 1-4分别表示{@link CRUD}中的{@link CRUD#Create}、{@link CRUD#Read}、{@link CRUD#Update}、{@link CRUD#Delete}；<br/>
	 * 5-8分别表示：修改名称、修改类型、修改说明、获取系统默认模板文件md5哈希值；<br/>
	 * 10表示获取指定pdir和punid对应的资源包含的附加文件通过{@link AccessoryListViewRender#render(com.tansuosoft.discoverx.web.JSPContext)}输出的内容；<br/>
	 * 11表示检查是否已经有指定文件名的附加文件； <br/>
	 * 12表示检查是否已经存在指定类型的附加文件；<br/>
	 * 13表示检查要上传的文件内容大小是否超过系统定义的最大允许上传文件内容长度。
	 * </p>
	 * 
	 * @return int
	 */
	public int getAction() {
		return this.m_action;
	}

	/**
	 * 设置附加文件操作标记。
	 * 
	 * <p>
	 * 从名为“action”的http请求参数中获取。
	 * </p>
	 * 
	 * @param action int
	 */
	public void setAction(int action) {
		this.m_action = action;
	}

	/**
	 * 返回附加文件类型资源UNID，可选，默认为附件类型。
	 * 
	 * <p>
	 * 从名为“act”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getAccessoryType() {
		return this.m_accessoryType;
	}

	/**
	 * 设置附加文件类型资源UNID。
	 * 
	 * <p>
	 * 从名为“act”的http请求参数中获取。
	 * </p>
	 * 
	 * @param accessoryType String
	 */
	public void setAccessoryType(String accessoryType) {
		this.m_accessoryType = accessoryType;
	}

	/**
	 * 返回别名。
	 * 
	 * <p>
	 * 对于图片类型的字段，系统通过此属性获取或设置其所属的字段的字段名（{@link com.tansuosoft.discoverx.model.Item#getItemName()}）。
	 * </p>
	 * <p>
	 * 从名为“alias”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getAlias() {
		return this.m_alias;
	}

	/**
	 * 设置别名，可选。
	 * 
	 * <p>
	 * 对于图片类型的字段，系统通过此属性获取或设置其所属的字段的字段名（{@link com.tansuosoft.discoverx.model.Item#getItemName()}）。
	 * </p>
	 * <p>
	 * 从名为“alias”的http请求参数中获取。
	 * </p>
	 * 
	 * @param alias String
	 */
	public void setAlias(String alias) {
		this.m_alias = alias;
	}

	/**
	 * 返回描述，可选。
	 * 
	 * <p>
	 * 从名为“desc”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置描述，可选。
	 * 
	 * <p>
	 * 从名为“desc”的http请求参数中获取。
	 * </p>
	 * 
	 * @param description String
	 */
	public void setDescription(String description) {
		this.m_description = description;
	}

	/**
	 * 返回附加文件内容大小。
	 * 
	 * <p>
	 * 从名为“size”的http请求参数中获取。
	 * </p>
	 * 
	 * @return int
	 */
	public int getSize() {
		return this.m_size;
	}

	/**
	 * 设置附加文件内容大小。
	 * 
	 * <p>
	 * 从名为“size”的http请求参数中获取。只有{@link #getAction()}返回13时才有意义。
	 * </p>
	 * 
	 * @param size int
	 */
	public void setSize(int size) {
		this.m_size = size;
	}

	/**
	 * 用HttpServletRequest中的相关参数值填充属性值。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.CommonForm#fillWebRequestForm(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public CommonForm fillWebRequestForm(HttpServletRequest request) {
		if (request == null) return null;
		this.m_UNID = request.getParameter("target");
		this.m_name = request.getParameter("name");
		this.m_fileName = request.getParameter("fn");
		this.m_parentDirectory = request.getParameter("pdir");
		if (this.m_parentDirectory == null || this.m_parentDirectory.length() == 0) this.m_parentDirectory = ResourceDescriptorConfig.DOCUMENT_DIRECTORY;
		this.m_parentUNID = request.getParameter("punid");
		this.m_template = request.getParameter("template");
		this.m_codeTemplate = request.getParameter("code");
		this.m_action = StringUtil.getValueInt(request.getParameter("action"), CRUD.Read.getIntValue());
		this.m_accessoryType = request.getParameter("act");
		if ((m_accessoryType != null && m_accessoryType.startsWith("act")) || !StringUtil.isUNID(m_accessoryType)) {
			m_accessoryType = ResourceAliasContext.getInstance().getUNIDByAlias(AccessoryType.class, m_accessoryType);
		}
		this.m_alias = request.getParameter("alias");
		this.m_description = request.getParameter("desc");
		this.m_size = StringUtil.getValueInt(request.getParameter("size"), 0);
		return this;
	}
}

