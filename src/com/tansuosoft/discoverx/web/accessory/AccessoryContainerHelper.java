/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.accessory;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.ResourceInserter;
import com.tansuosoft.discoverx.bll.ResourceRemover;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.resource.DBResourceInserter;
import com.tansuosoft.discoverx.bll.resource.DBResourceRemover;
import com.tansuosoft.discoverx.bll.resource.XmlResourceUpdater;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.impl.AccessoryInserter;
import com.tansuosoft.discoverx.dao.impl.DocumentFieldUpdater;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.UNIDProvider;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 封装附加文件容器资源常用操作的工具类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class AccessoryContainerHelper extends EventSourceImpl {
	private Resource m_container = null;
	private ResourceDescriptor m_descriptor = null;
	private Session session = null;

	/**
	 * 接收包含附加文件的父资源对象实例和自定义会话的构造器。
	 * 
	 * @param container Resource，必须。
	 * @param session 用户自定义会话，可选。
	 */
	public AccessoryContainerHelper(Resource container, Session session) {
		if (container == null) throw new IllegalArgumentException("必须提供有效容器资源！");
		this.session = session;
		this.m_container = container;
		if (this.m_container != null) {
			this.m_descriptor = ResourceDescriptorConfig.getInstance().getResourceDescriptor(this.m_container.getClass());
			if (this.m_descriptor == null) throw new RuntimeException("无法找到容器资源的描述！");
			if (!this.m_descriptor.getAccessoryContainer()) throw new RuntimeException("“" + m_descriptor.getName() + "”类型的资源不能包含附加文件！");
		}
	}

	/**
	 * 追加一个附加文件资源到容器。
	 * 
	 * <p>
	 * 如果父资源保存于数据库中，那么将触发附加文件资源的保存前和保存后事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父资源。<br/>
	 * 如果父资源保存于xml中，那么将触发附加文件所属父资源的保存前和保存后事件。
	 * </p>
	 * 
	 * @param accessory Accessory 要追加的Accessory对象实例，必须。
	 * @return Accessory 追加成功则返回被追加的Accessory对象实例，否则返回null。
	 * @throws ResourceException 资源异常
	 */
	public Accessory addAccessory(Accessory accessory) throws ResourceException {
		if (accessory == null) throw new IllegalArgumentException("必须提供有效附加文件资源！");

		List<Accessory> list = this.m_container.getAccessories();
		if (list == null) {
			list = new ArrayList<Accessory>();
			m_container.setAccessories(list);
		}

		if (!this.m_descriptor.getXmlStore()) {
			ResourceEventArgs e = new ResourceEventArgs(accessory, session);
			// 添加事件处理程序。
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(accessory, null);
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_SAVE, EventHandler.EHT_SAVED), eventHandlerInfoList);
			this.callEventHandler(m_container, EventHandler.EHT_SAVE, e); // 保存开始事件。

			ResourceInserter inserter = new DBResourceInserter();
			if (inserter.insert(accessory, session) != null) {
				list.add(0, accessory);

				// 保存的如果是文档资源的图片字段对应的附件则同步写入对应的图片字段值
				if (m_container instanceof Document) {
					Document doc = (Document) m_container;
					String fldName = accessory.getAlias();
					if (doc.hasItem(fldName)) {
						String unidv = doc.getItemValue(fldName);
						if (!accessory.getUNID().equalsIgnoreCase(unidv)) {
							DocumentFieldUpdater dbr = new DocumentFieldUpdater();
							dbr.setResource(doc);
							dbr.setParameter(DocumentFieldUpdater.FIELD_NAME_PARAM_NAME, fldName);
							dbr.setParameter(DocumentFieldUpdater.FIELD_VALUE_PARAM_NAME, accessory.getUNID());
							dbr.sendRequest();
							if (dbr.getResultLong() <= 0) throw new RuntimeException("系统无法将选择的图片保存到图片字段，请联系系统管理员！");
						}
					}
				}

				this.callEventHandler(m_container, EventHandler.EHT_SAVED, e); // 保存完成事件。
			}
		} else {
			list.add(0, accessory);
			XmlResourceUpdater updater = new XmlResourceUpdater();
			updater.update(m_container, session);
		}
		return accessory;
	}// func end

	/**
	 * 从容器中删除一个指定附加文件。
	 * 
	 * <p>
	 * 如果父资源保存于数据库中，那么将触发附加文件资源的删除前和删除后事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父资源。 <br/>
	 * 如果父资源保存于xml中，那么将触发附加文件所属父资源的保存前和保存后事件。
	 * </p>
	 * 
	 * @param accessory 删除的{@link Accessory}对象实例，必须
	 * @return Accessory 删除成功则返回被删除的{@link Accessory}对象实例，否则返回null。
	 * @throws ResourceException 资源异常
	 */
	public Accessory removeAccessory(Accessory accessory) throws ResourceException {
		if (accessory == null) throw new IllegalArgumentException("必须提供有效附加文件资源！");
		String fn = AccessoryPathHelper.getAbsoluteAccessoryServerPath(this.m_container) + accessory.getFileName();
		File file = new File(fn);
		Accessory result = null;
		List<Accessory> list = this.m_container.getAccessories();
		if (list == null || list.isEmpty()) return null;
		if (list.remove(accessory)) result = accessory;
		if (!this.m_descriptor.getXmlStore() && result != null) {
			ResourceEventArgs e = new ResourceEventArgs(accessory, session);
			// 添加事件处理程序。
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(accessory, null);
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_DELETE, EventHandler.EHT_DELETED), eventHandlerInfoList);
			this.callEventHandler(m_container, EventHandler.EHT_DELETE, e); // 删除开始事件。

			ResourceRemover remover = new DBResourceRemover();
			accessory.setSecurity(this.m_container.getSecurity());
			if (remover.remove(accessory, session) != null) {
				this.callEventHandler(m_container, EventHandler.EHT_DELETED, e); // 删除完成事件。
			}
		} else if (result != null) {
			XmlResourceUpdater updater = new XmlResourceUpdater();
			updater.update(m_container, session);
		}
		boolean fileDeleted = true;
		if (file.exists() && file.isFile()) {
			fileDeleted = file.delete();
		} else {
			FileLogger.debug("找不到要删除的附加文件对应的物理文件：“%1$s”！", accessory.getFileName());
		}
		if (!fileDeleted) {
			file.deleteOnExit();
			FileLogger.debug("无法删除文件：“%1$s”，服务器关闭时将再次尝试！", accessory.getFileName());
		}
		return result;
	}

	/**
	 * 从容器中删除一个指定附加文件。
	 * 
	 * <p>
	 * 如果父资源保存于数据库中，那么将触发附加文件资源的删除前和删除后事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父资源。<br/>
	 * 如果父资源保存于xml中，那么将触发附加文件所属父资源的保存前和保存后事件。
	 * </p>
	 * 
	 * @param accessoryUNID String 要删除的Accessory对象UNID，必须
	 * @throws ResourceException 资源异常
	 */
	public Accessory removeAccessory(String accessoryUNID) throws ResourceException {
		if (accessoryUNID == null || accessoryUNID.length() == 0) throw new IllegalArgumentException("必须传入有效附加文件资源UNID！");
		Accessory accessory = this.getAccessory(accessoryUNID);
		if (accessory != null) return this.removeAccessory(accessory);
		return null;
	}

	/**
	 * 更新一个指定附加文件的文件内容大小到其对应数据库记录。
	 * 
	 * <p>
	 * 将同步更新附加文件对应数据库记录的修改日期和修改者。
	 * </p>
	 * <p>
	 * 如果父资源保存于数据库中，那么将触发附加文件资源的保存前和保存后事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父资源。
	 * </p>
	 * 
	 * @param accessory 要更新的其对应数据库记录的附加文件资源。
	 * @return boolean 成功则返回true，否则返回false。
	 * @throws ResourceException
	 */
	public boolean updateAccessorySize(Accessory accessory) throws ResourceException {
		if (accessory == null) throw new IllegalArgumentException("必须提供有效附加文件资源！");
		return updateAccessory(accessory, "c_size", "" + accessory.getSize());
	}

	/**
	 * 更新一个指定附加文件对应的数据库记录。
	 * 
	 * <p>
	 * 将同步更新附加文件对应数据库记录的修改日期和修改者。
	 * </p>
	 * <p>
	 * 如果父资源保存于数据库中，那么将触发附加文件资源的保存前和保存后事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父资源。
	 * </p>
	 * 
	 * @param accessory 要更新的其对应数据库记录的附加文件资源。
	 * @param columnName 要更新的属性值对应的字段名。
	 * @param columnValue 要更新的属性值对应的字段值。
	 * @return boolean 成功则返回true，否则返回false。
	 * @throws ResourceException
	 */
	public boolean updateAccessory(Accessory accessory, String columnName, String columnValue) throws ResourceException {
		if (accessory == null) throw new IllegalArgumentException("必须提供有效附加文件资源！");

		final String dt = DateTime.getNowDTString();
		String modifier = Session.getUser(session).getName();

		if (!this.m_descriptor.getXmlStore()) {
			SecurityLevel currentLevel = SecurityLevel.Modify;
			boolean authorized = SecurityHelper.authorize(this.session, accessory, this.m_container, 3, currentLevel);
			if (!authorized) throw new ResourceException("对不起，您没有更改此文件属性的权限！");
			ResourceEventArgs e = new ResourceEventArgs(accessory, session);

			// 添加事件处理程序。
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(accessory, null);
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_SAVE, EventHandler.EHT_SAVED), eventHandlerInfoList);
			this.callEventHandler(m_container, EventHandler.EHT_SAVE, e); // 保存开始事件。
			DBRequest dbr = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper result = new SQLWrapper();
					StringBuilder sql = new StringBuilder();

					String columnName = this.getParamValueString("columnName", null);
					sql.append("update t_accessory set c_modified=?,c_modifier=?").append(",").append(columnName).append("=?").append(" where c_unid=?");

					result.setSql(sql.toString());
					result.setParameterized(true);
					result.setRequestType(RequestType.NonQuery);

					return result;
				}
			};
			dbr.setParameter("columnName", columnName);
			dbr.setParameter("columnValue", columnValue);
			dbr.setParameter("unid", accessory.getUNID());
			dbr.setParameter("modifier", modifier);
			ParametersSetter parametersSetter = new ParametersSetter() {
				@Override
				public void setParameters(DBRequest request, CommandWrapper cw) {
					try {
						cw.setString(1, dt);
						cw.setString(2, request.getParamValueString("modifier", null));
						String columnName = request.getParamValueString("columnName", "");
						if (columnName.equalsIgnoreCase("c_sort") || columnName.equalsIgnoreCase("c_size")) {
							cw.setInt(3, request.getParamValueInt("columnValue", 0));
						} else {
							cw.setString(3, request.getParamValueString("columnValue", null));
						}
						cw.setString(4, request.getParamValueString("unid", null));
					} catch (SQLException e) {
						FileLogger.error(e);
					}
				}
			};

			dbr.setParametersSetter(parametersSetter);
			dbr.sendRequest();
			if (dbr.getResultLong() > 0) {
				accessory.setModified(dt);
				accessory.setModifier(modifier);
				this.callEventHandler(m_container, EventHandler.EHT_SAVE, e); // 保存完成事件。
				return true;
			}
		} else {
			XmlResourceUpdater updater = new XmlResourceUpdater();
			if (updater.update(m_container, session) != null) {
				accessory.setModified(dt);
				accessory.setModifier(modifier);
			}
		}

		return true;
	}

	/**
	 * 获取附加文件容器资源中指定unid的附加文件资源。
	 * 
	 * @param unid
	 * @return Accessory，找不到则返回null。
	 */
	public Accessory getAccessory(String unid) {
		if (unid == null || this.m_container == null) return null;
		List<Accessory> list = this.m_container.getAccessories();
		if (list == null || list.isEmpty()) return null;
		for (Accessory x : list) {
			if (x != null && unid.equalsIgnoreCase(x.getUNID())) return x;
		}
		return null;
	}

	/**
	 * 拷贝当前容器包含的所有附加文件到目标容器资源。
	 * 
	 * <p>
	 * 拷贝时将同时写入目标容器对应的所有附加文件到数据库。
	 * </p>
	 * 
	 * @param target Resource，目标容器资源，必须。
	 * @return 如果当前容器包含有效的附加文件且正确宝贝到目标容器，则返回true，否则返回false。
	 */
	public boolean copyAccessoriesTo(Resource target) {
		if (target == null) return false;
		List<Accessory> list = (this.m_container == null ? null : this.m_container.getAccessories());
		if (list == null || list.isEmpty()) return false;
		List<Accessory> targetList = new ArrayList<Accessory>(list.size());
		List<File> dels = new ArrayList<File>();
		try {
			for (Accessory x : list) {
				if (x == null) continue;
				Accessory newa = (Accessory) x.clone();
				String fn = AccessoryPathHelper.getAbsoluteAccessoryServerPath(this.m_container) + x.getFileName();
				File file = new File(fn);
				if (!file.exists() || !file.isFile()) continue;
				String dirnew = AccessoryPathHelper.getAbsoluteAccessoryServerPath(target);
				File dirnewFile = new File(dirnew);
				if (!dirnewFile.exists()) dirnewFile.mkdirs();
				String fnnew = AccessoryPathHelper.getAbsoluteAccessoryServerPath(target) + x.getFileName();
				File newfile = new File(fnnew);
				FileHelper.copyFile(file, newfile);
				dels.add(newfile);
				newa.setUNID(UNIDProvider.getUNID());
				newa.setPUNID(target.getUNID());
				targetList.add(newa);
			}// for end
			DBRequest dbr = null;
			DBRequest first = null;
			for (Accessory x : targetList) {
				dbr = new AccessoryInserter();
				dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, x);
				if (first == null) {
					first = dbr;
				} else {
					first.setNextRequest(dbr);
				}
			}
			if (first != null) {
				first.setUseTransaction(true);
				first.sendRequest();
				if (first.getResultLong() > 0) {
					target.setAccessories(targetList);
					return (targetList.size() > 0);
				}
			}
		} catch (Exception e) {
			if (targetList != null) {
				for (File x : dels) {
					if (!x.delete()) x.deleteOnExit();
				}
			}
			FileLogger.error(e);
		}
		return false;
	}

	/**
	 * 获取容器资源包含的所有附加文件中，类型为accTypeId的所有附加文件列表。
	 * 
	 * @param accTypeId 表示要返回的附加文件对应的附加文件类型资源的别名、UNID或名称之一。
	 * @return 如果找到至少一个匹配的结果则返回包含其结果的列表，否则返回null。
	 */
	public List<Accessory> getAccessoriesByType(String accTypeId) {
		if (accTypeId == null || accTypeId.length() == 0) return null;
		List<Accessory> list = (this.m_container == null ? null : this.m_container.getAccessories());
		if (list == null) return null;
		List<Accessory> result = null;
		AccessoryType at = null;
		for (Accessory x : list) {
			if (x == null) continue;
			at = x.getAccessoryTypeResource();
			if (at == null) continue;
			if ((accTypeId.startsWith("act") && accTypeId.equalsIgnoreCase(at.getAlias())) || accTypeId.equalsIgnoreCase(at.getUNID()) || accTypeId.equalsIgnoreCase(at.getName())) {
				if (result == null) result = new ArrayList<Accessory>();
				result.add(x);
			}
		}
		return result;
	}

	/**
	 * 获取容器资源包含的所有附加文件中，类型为accTypeId的第一个附加文件对象。
	 * 
	 * @param accTypeId 表示要返回的附加文件对应的附加文件类型资源的别名、UNID或名称之一。
	 * @return 如果找到至少一个匹配的结果则返回之，否则返回null。
	 */
	public Accessory getFirstAccessoryByType(String accTypeId) {
		List<Accessory> result = getAccessoriesByType(accTypeId);
		return (result != null && result.size() > 0 ? result.get(0) : null);
	}

	/**
	 * 获取下载临时文件时使用的临时父资源和临时附加文件资源对象。
	 * 
	 * @param fn 完整临时文件路径。
	 * @param sc 要下载此资源的用户对应的安全编码，为0表示所有人可下载。
	 * @return {@link GenericPair}对象，其中K为临时父资源对象，V为临时附加文件资源对象，如果fn对应文件不存在则抛出异常并返回null。
	 */
	public static GenericPair<Resource, Accessory> getTempFileDownloadInfo(String fn, int sc) {
		try {
			if (fn == null || fn.length() == 0) throw new RuntimeException("没有提供有效的文件名！");
			File f = new File(fn);
			if (!f.exists() || !f.isFile()) throw new RuntimeException("要下在的文件不存在！");

			GenericPair<Resource, Accessory> gp = null;

			Resource r = new Application();
			r.setUNID(AccessoryPathHelper.TEMP_FILE_PARENT_UNID);
			r.setName("临时附加文件父资源");

			Security s = new Security(r.getUNID());
			if (sc > 0) {
				s.setSecurityLevel(sc, SecurityLevel.View.getIntValue());
			} else {
				s.setSecurityLevel(Role.ROLE_DEFAULT_SC, SecurityLevel.View.getIntValue());
				s.setSecurityLevel(Role.ROLE_ANONYMOUS_SC, SecurityLevel.View.getIntValue());
			}
			r.setSecurity(s);

			Accessory a = new Accessory();
			a.setUNID(AccessoryPathHelper.TEMP_FILE_ACCESSORY_UNID);
			String name = f.getName();
			a.setName(name);
			a.setSecurity((Security) s.clone());

			List<Accessory> accessories = new ArrayList<Accessory>(1);
			accessories.add(a);
			r.setAccessories(accessories);
			gp = new GenericPair<Resource, Accessory>(r, a);
			return gp;
		} catch (CloneNotSupportedException e) {
			FileLogger.error(e);
		}
		return null;
	}
}

