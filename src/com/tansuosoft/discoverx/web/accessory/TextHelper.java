/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.accessory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;

/**
 * 文档正文/办理单处理相关帮助类。
 * 
 * @author coca@tansuosoft.cn
 */
public class TextHelper implements JsonSerializable {
	private Document m_document = null;
	private String m_targetAccessoryTypeAlias = null;

	/**
	 * 接收文档资源与目标附加文件类型的构造器。
	 * 
	 * @param Document
	 *          必须。
	 * @param targetAccessoryTypeAlias
	 *          目标附加文件类型资源的别名，必须，正文是“actText”、办理单是“actTransaction”。
	 */
	public TextHelper(Document document, String targetAccessoryTypeAlias) {
		this.m_document = document;
		if (this.m_document == null) throw new IllegalArgumentException("必须提供有效文档资源！");
		m_targetAccessoryTypeAlias = targetAccessoryTypeAlias;
		if (m_targetAccessoryTypeAlias == null || m_targetAccessoryTypeAlias.length() == 0) throw new IllegalArgumentException("必须提供有效附加文件类型别名！");
	}

	/**
	 * 获取有效的正文代码模板对应的附加文件列表。
	 * 
	 * @return
	 */
	public List<Accessory> getAvailableCodeTemplates() {
		List<Accessory> result = getAccessories(this.m_document, m_targetAccessoryTypeAlias + "Code");
		if (result == null || result.isEmpty()) {
			Accessory accessory = getSystemDefaultAccessoryTemplate(m_targetAccessoryTypeAlias + "Code");
			if (result == null) result = new ArrayList<Accessory>();
			result.add(accessory);
		}
		return result;
	}

	/**
	 * 获取有效的正文样式模板对应的附加文件列表。
	 * 
	 * @return
	 */
	public List<Accessory> getAvailableStyleTemplates() {
		List<Accessory> result = getAccessories(this.m_document, m_targetAccessoryTypeAlias + "Style");
		if (result == null || result.isEmpty()) {
			Accessory accessory = getSystemDefaultAccessoryTemplate(m_targetAccessoryTypeAlias + "Style");
			if (result == null) result = new ArrayList<Accessory>();
			result.add(accessory);
		}
		return result;
	}

	/**
	 * 获取document所指定文档所属模块（从最近一级模块到根模块逐级往上查找）包含的附加文件列表中，
	 * 附加文件类型别名为accessoryTypeAlias的所有附加文件列表。
	 * 
	 * @param document
	 * @param accessoryTypeAlias
	 * @return
	 */
	protected static List<Accessory> getAccessories(Document document, String accessoryTypeAlias) {
		List<Accessory> result = null;
		int appCnt = document.getApplicationCount();
		Application app = null;
		List<Accessory> accessories = null;
		AccessoryType accessoryType = null;
		boolean foundTemplate = false;
		for (int i = appCnt; i >= 1; i--) {
			app = document.getApplication(i);
			accessories = app.getAccessories();
			if (accessories == null || accessories.isEmpty()) continue;
			for (Accessory x : accessories) {
				if (x == null) continue;
				accessoryType = ResourceContext.getInstance().getResource(x.getAccessoryType(), AccessoryType.class);
				if (accessoryType == null || accessoryTypeAlias == null || !accessoryTypeAlias.equals(accessoryType.getAlias())) continue;
				if (!foundTemplate) foundTemplate = true;
				if (result == null) result = new ArrayList<Accessory>();
				result.add(x);
			}
		}
		return result;
	}

	/**
	 * 系统默认使用的模板对应附加文件资源所属上级资源的UNID。
	 */
	public static final String SYSTEM_DEFAULT_TEMPLATE_ACCESSORY_PARENT_UNID = "5A96D7FD2A0E4FC098079AE411F841F0";

	protected static final String SYSTEM_DEFAULT_TEMPLATE_ACCESSORY_PARENT_DIRECTORY = "systemDefaultTemplates";

	/**
	 * 返回目标附加文件类型对应的系统默认使用的模板附加文件资源对应的上级资源。
	 * 
	 * @param alias
	 * @return
	 */
	protected Resource getSystemDefaultTemplateAccessoryParent(String alias) {
		AccessoryType accessoryType = ResourceContext.getInstance().getResource(ResourceAliasContext.getInstance().getUNIDByAlias(AccessoryType.class, alias), AccessoryType.class);
		if (accessoryType == null) throw new RuntimeException("无法获取“" + alias + "”对应的附件文件类型！");

		Resource parent = new Resource() {
			/**
			 * 序列化id。
			 */
			private static final long serialVersionUID = 7671052303092708985L;

			/**
			 * 重载getDirectory：总是返回：“systemDefaultTemplates”。
			 * 
			 * @see com.tansuosoft.discoverx.model.Resource#getDirectory()
			 */
			@Override
			public String getDirectory() {
				return SYSTEM_DEFAULT_TEMPLATE_ACCESSORY_PARENT_DIRECTORY;
			}
		};
		parent.setName("系统缺省模板附加文件上级资源" + accessoryType.getName());
		parent.setAlias(alias);
		parent.setUNID(SYSTEM_DEFAULT_TEMPLATE_ACCESSORY_PARENT_UNID);
		parent.setSecurity(new Security(parent.getUNID()));
		parent.getSecurity().setSecurityLevel(Role.ROLE_ANONYMOUS_SC, SecurityLevel.View.getIntValue());
		return parent;
	}

	/**
	 * 返回目标附加文件类型对应的系统默认使用的模板对应的附加文件资源。
	 * 
	 * @param alias
	 * @return
	 */
	protected Accessory getSystemDefaultAccessoryTemplate(String alias) {
		AccessoryType accessoryType = ResourceContext.getInstance().getResource(ResourceAliasContext.getInstance().getUNIDByAlias(AccessoryType.class, alias), AccessoryType.class);
		if (accessoryType == null) throw new RuntimeException("无法获取“" + alias + "”对应的附件文件类型！");
		Accessory accessory = new Accessory(SYSTEM_DEFAULT_TEMPLATE_ACCESSORY_PARENT_UNID);
		accessory.setName("系统缺省" + accessoryType.getName());
		String path = AccessoryPathHelper.getAbsoluteAccessoryServerPath(getSystemDefaultTemplateAccessoryParent(alias));
		File file = new File(path);
		if (!file.exists() || !file.isDirectory()) return null;
		String[] files = file.list();
		boolean foundFile = false;
		for (String x : files) {
			if (x.startsWith("default")) {
				accessory.setFileName(x);
				foundFile = true;
				break;
			}
		}
		accessory.setUNID(StringUtil.getMD5HashString(alias));
		accessory.setAccessoryType(ResourceAliasContext.getInstance().getUNIDByAlias(AccessoryType.class, alias));
		return (foundFile ? accessory : null);
	}

	/**
	 * 获取模板附加文件列表对应的json脚本。
	 * 
	 * @param accessories
	 * @param isTarget
	 * @param type
	 * @return
	 */
	protected static String getAccessoriesJsonResult(List<Accessory> accessories, boolean isTarget, String type) {
		int index = 0;
		StringBuilder sb = new StringBuilder();

		if (accessories != null) {
			for (Accessory x : accessories) {
				if (x == null) continue;
				sb.append(index == 0 ? "" : ",").append("\r\n");
				sb.append("{");
				sb.append("name:").append("'").append(x.getName()).append("'");
				sb.append(",").append("description:").append("'").append(StringUtil.encode4Json(x.getDescription())).append("'");
				sb.append(",").append("accessoryType:").append("'").append(x.getAccessoryType()).append("'");
				sb.append(",").append("UNID:").append("'").append(x.getUNID()).append("'");
				sb.append(",").append("fileName:").append("'").append(x.getFileName()).append("'");
				sb.append(",").append("PUNID:").append("'").append(x.getPUNID()).append("'");
				if (!x.getParentDirectory().equalsIgnoreCase(ResourceDescriptorConfig.DOCUMENT_DIRECTORY)) sb.append(",").append("parentDirectory:").append("'").append(x.getParentDirectory()).append("'");
				sb.append(",").append("type:").append("'").append(type).append("'");
				sb.append(",").append("fileExt:").append("'").append(FileHelper.getExtName(x.getFileName(), "")).append("'");
				if (isTarget) {
					sb.append(",").append("styleTemplate:").append("'").append(x.getStyleTemplate()).append("'");
					sb.append(",").append("codeTemplate:").append("'").append(x.getCodeTemplate()).append("'");
				}
				sb.append("}");
				index++;
			}
		}

		return sb.toString();
	}

	/**
	 * 重载toJson：构造并返回指定类型的所有附加文件处理时的Json初始化脚本。
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	@Override
	public String toJson() {
		String targetAccessoryTypeUNID = ResourceAliasContext.getInstance().getUNIDByAlias(AccessoryType.class, this.m_targetAccessoryTypeAlias);
		AccessoryType targetAccessoryType = ResourceContext.getInstance().getResource(targetAccessoryTypeUNID, AccessoryType.class);

		StringBuilder sb = new StringBuilder();
		sb.append("parent:").append("'").append(m_document.getUNID()).append("'");

		int appCnt = m_document.getApplicationCount();
		Application app = null;
		int textAccessoryCount = 0;
		for (int i = appCnt; i > 0; i--) {
			app = m_document.getApplication(i);
			if (app.getTextAccessoryCount() >= 0) {
				textAccessoryCount = app.getTextAccessoryCount();
				break;
			}
		}

		sb.append(",").append("availableStyleTemplets:").append("[");
		sb.append(getAccessoriesJsonResult(this.getAvailableStyleTemplates(), false, "style"));
		sb.append("]");

		sb.append(",").append("availableCodeTemplets:").append("[");
		sb.append(getAccessoriesJsonResult(this.getAvailableCodeTemplates(), false, "code"));
		sb.append("]");

		List<Accessory> list = m_document.getAccessories();
		List<Accessory> targetList = new ArrayList<Accessory>();
		if (list != null && list.size() > 0) {
			for (Accessory x : list) {
				if (x != null && targetAccessoryTypeUNID != null && targetAccessoryTypeUNID.equalsIgnoreCase(x.getAccessoryType())) targetList.add(x);
			}
		}

		String type = m_targetAccessoryTypeAlias.substring(3).toLowerCase();
		sb.append(",").append("existTexts:").append("[");
		sb.append(getAccessoriesJsonResult(targetList, true, type));
		sb.append("]");
		sb.append(",").append("type:").append("'").append(type).append("'");
		sb.append(",").append("targetAlias:").append("'").append(m_targetAccessoryTypeAlias).append("'");
		sb.append(",").append("targetName:").append("'").append(targetAccessoryType.getName()).append("'");
		sb.append(",").append("targetUNID:").append("'").append(targetAccessoryTypeUNID).append("'");
		if ("actText".equalsIgnoreCase(m_targetAccessoryTypeAlias)) {
			sb.append(",").append("readonly:").append("false");
			sb.append(",").append("maxCount:").append(textAccessoryCount);
		} else {
			sb.append(",").append("readonly:").append("true");
			sb.append(",").append("maxCount:").append(1);
		}

		return sb.toString();
	}

	/**
	 * 返回systemDefaultTemplateAccessoryUNID指定的系统默认模板附加文件的路径。
	 * 
	 * @param systemDefaultTemplateAccessoryUNID
	 * @return
	 */
	public static String getSystemDefaultTemplatePath(String systemDefaultTemplateAccessoryUNID) {
		String path = null;
		List<Resource> list = XmlResourceLister.getResources(AccessoryType.class);
		if (list == null) return null;

		for (Resource r : list) {
			if (r == null || !(r instanceof AccessoryType)) continue;
			AccessoryType accessoryType = (AccessoryType) r;
			String unid = StringUtil.getMD5HashString(accessoryType.getAlias());
			if (unid != null && unid.equalsIgnoreCase(systemDefaultTemplateAccessoryUNID)) {
				path = CommonConfig.getInstance().getAccessoryPath() + SYSTEM_DEFAULT_TEMPLATE_ACCESSORY_PARENT_DIRECTORY + java.io.File.separator + accessoryType.getAlias() + java.io.File.separator;
				break;
			}
		}

		String fileName = null;
		if (path != null) {
			File file = new File(path);
			if (!file.exists() || !file.isDirectory()) return null;
			String[] files = file.list();
			for (String x : files) {
				if (x.startsWith("default")) {
					fileName = x;
					break;
				}
			}
		}

		return (fileName == null ? null : path + fileName);

	}

	/**
	 * 返回systemDefaultTemplateAccessoryUNID指定的系统默认模板文件的MD5。
	 * 
	 * @param systemDefaultTemplateAccessoryUNID
	 * @return
	 */
	public static String getSystemDefaultTemplateMD5(String systemDefaultTemplateAccessoryUNID) {
		String path = null;
		List<Resource> list = XmlResourceLister.getResources(AccessoryType.class);
		if (list == null) return null;

		for (Resource r : list) {
			if (r == null || !(r instanceof AccessoryType)) continue;
			AccessoryType accessoryType = (AccessoryType) r;
			String unid = StringUtil.getMD5HashString(accessoryType.getAlias());
			if (unid != null && unid.equalsIgnoreCase(systemDefaultTemplateAccessoryUNID)) {
				path = CommonConfig.getInstance().getAccessoryPath() + SYSTEM_DEFAULT_TEMPLATE_ACCESSORY_PARENT_DIRECTORY + java.io.File.separator + accessoryType.getAlias() + java.io.File.separator;
				break;
			}
		}

		String fileName = null;
		if (path != null) {
			File file = new File(path);
			if (!file.exists() || !file.isDirectory()) return null;
			String[] files = file.list();
			for (String x : files) {
				if (x.startsWith("md5.txt")) {
					fileName = x;
					break;
				}
			}
		}

		String fullPath = path + fileName;
		String md5 = null;
		FileReader fr = null;
		try {
			fr = new FileReader(fullPath);
			char chars[] = new char[32];
			fr.read(chars);
			md5 = new String(chars);
		} catch (FileNotFoundException e) {
			FileLogger.debug("文件“%s”不存在！", fullPath);
		} catch (IOException e) {
			FileLogger.debug("文件“%s”访问异常！", fullPath);
		} finally {
			try {
				if (fr != null) fr.close();
			} catch (IOException e) {
				FileLogger.error(e);
			}
		}
		return md5;
	}
}

