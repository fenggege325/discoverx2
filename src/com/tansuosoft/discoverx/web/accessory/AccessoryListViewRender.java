/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.accessory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.bll.view.XmlResourceOrderByComparator;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.ConfigEntry;
import com.tansuosoft.discoverx.common.VerbotenUploadFileTypeConfig;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.Browser;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.document.ItemsParser;
import com.tansuosoft.discoverx.web.ui.toolbar.ToolbarRender;

/**
 * 输出附加文件客户端ListView代码的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AccessoryListViewRender implements HtmlRender {
	private String m_id = null; // 页面唯一的附加文件容器id
	private Resource m_parent = null;
	private int m_width = 0;
	private int m_height = 0;
	private ToolbarRender m_toolbarRender = null;
	private boolean m_readonly = false;
	private List<AccessoryType> m_acts = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param parent 所属容器资源。
	 * @param width 最大宽度（像素）。
	 * @param height 高度（像素）。
	 * @param readonly 是否只读
	 * @param toolbarRender 对应工具栏{@link HtmlRender}。
	 * @param accessoryTypes 可包含的附加文件类型，如果为null且容器资源非文档资源，则自动设置可包含的附件文件类型为系统所有附加文件类型中{@link AccessoryType#getRestrictToConfig()}属性为false的附加文件列表。
	 */
	public AccessoryListViewRender(Resource parent, int width, int height, boolean readonly, ToolbarRender toolbarRender, List<AccessoryType> accessoryTypes) {
		this.m_parent = parent;
		if (m_parent == null) throw new IllegalArgumentException("必须提供有效附加文件容器资源！");
		if (parent instanceof Document) {
			Document doc = (Document) parent;
			ItemsParser itemsParser = ItemsParser.getInstance(doc.getForm());
			if (itemsParser.hasAccessory()) {
				Item item = itemsParser.getAccessoryItem();
				this.m_id = item.getItemName();
			} else {
				this.m_id = "dynamic_accessories";
			}
		} else {
			this.m_id = "accessories";
		}

		this.m_width = width;
		this.m_height = height;
		this.m_toolbarRender = toolbarRender;
		this.m_readonly = readonly;
		this.m_acts = accessoryTypes;
		if ((this.m_acts == null || this.m_acts.isEmpty()) && !(this.m_parent instanceof Document)) {
			this.m_acts = new ArrayList<AccessoryType>();
			List<Resource> ats = XmlResourceLister.getResources(AccessoryType.class);
			if (ats == null || ats.isEmpty()) return;
			for (Resource r : ats) {
				if (r == null || !(r instanceof AccessoryType)) continue;
				AccessoryType at = (AccessoryType) r;
				if (!at.getRestrictToConfig()) continue;
				this.m_acts.add(at);
			}
		}
		if (this.m_acts != null && this.m_acts.size() > 1) Collections.sort(this.m_acts, new XmlResourceOrderByComparator());
	}

	/**
	 * 接收所属容器资源和大小的构造器。
	 * 
	 * @param resource
	 * @param width
	 * @param height
	 * @return
	 */
	public AccessoryListViewRender(Resource resource, int width, int height) {
		this(resource, width, height, false, null, null);
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		List<com.tansuosoft.discoverx.model.Accessory> accs = this.m_parent.getAccessories();
		if (accs == null) accs = new ArrayList<com.tansuosoft.discoverx.model.Accessory>();
		StringBuilder sb = new StringBuilder();

		sb.append("id:").append("'lv_").append(m_id).append("'");
		sb.append(",").append("width:").append(this.m_width); // - 2
		sb.append(",").append("height:").append(this.m_height);

		// header
		sb.append(",\r\n").append("header:").append("{"); // header begin
		sb.append("id:'lvh_").append(m_id).append("'").append(",itemType: 1");
		sb.append(",columns:["); // columns begin
		int index = 0;
		int subIndex = 0;
		String titles[] = { "名称", "类型", "上传者", "日期", "大小(KB)", "说明" };
		String props[] = { "name", "accessorytypename", "creator", "modified", "size", "description" };
		String values[] = null;
		String extName = null;

		for (String t : titles) {
			sb.append(index == 0 ? "" : ",");
			sb.append("{"); // lvc begin
			sb.append("id:").append("'lvhc_").append(m_id).append("_").append(props[index]).append("'");
			sb.append(",").append("label:").append("'").append(t).append("'");
			if (index == 4) sb.append(",").append("isNumberValue:true");
			if (index == 5) sb.append(",").append("sortable:false");
			sb.append(",").append("data:").append("{"); // data begin

			sb.append("}"); // data end
			sb.append("}\r\n"); // lvc end
			index++;
		}
		sb.append("]"); // columns end
		sb.append("}\r\n"); // // header end

		// listitem
		index = 0;
		boolean editable = false;
		sb.append(",").append("rows:").append("["); // rows begin
		AccessoryType accessoryType = null;
		for (com.tansuosoft.discoverx.model.Accessory x : accs) {
			if (x == null) continue;
			accessoryType = x.getAccessoryTypeResource();
			if (accessoryType == null || !accessoryType.getVisible()) continue;
			values = new String[titles.length];
			for (int i = 0; i < titles.length; i++) {
				switch (i) {
				case 0:
					values[i] = x.getName();
					break;
				case 1:
					values[i] = x.getAccessoryTypeName();
					break;
				case 2:
					values[i] = ParticipantHelper.getFormatValue(x.getModifier(), "cn");
					break;
				case 3:
					values[i] = x.getModified();
					if (values[i] != null && values[i].length() >= 19) values[i] = StringUtil.stringLeftBack(values[i], ":");
					break;
				case 4:
					int sizeInKB = x.getSize();
					values[i] = String.format("%1$d", sizeInKB);
					break;
				case 5:
					values[i] = x.getDescription();
					break;
				default:
					break;
				}
			}
			sb.append(index > 0 ? "," : "");
			sb.append("{"); // lvi begin
			sb.append("id:").append("'lvi_").append(m_id).append(index).append("'");
			extName = FileHelper.getExtName(x.getFileName(), ".unknown");
			// if (jspContext.getBrowser() == Browser.IE6 || jspContext.getBrowser() == Browser.IE7 || jspContext.getBrowser() == Browser.IE8) {
			if (jspContext.getBrowser() == Browser.MSIE) {
				sb.append(",").append("icon:'").append(extName.substring(1)).append(".png").append("'");
			} else {
				sb.append(",").append("icon:'icon_clip.gif'");
			}
			sb.append(",").append("data:").append("{"); // lvi data begin
			sb.append("UNID:'").append(x.getUNID()).append("'");
			sb.append(",").append("accessoryType:").append("'").append(x.getAccessoryType()).append("'");
			AccessoryType at = x.getAccessoryTypeResource();
			if (at != null) sb.append(",").append("accessoryTypeAlias:").append("'").append(at.getAlias()).append("'");
			sb.append(",").append("extName:").append("'").append(extName).append("'");
			sb.append(",").append("fileName:").append("'").append(x.getFileName()).append("'");
			sb.append("}"); // lvi data end

			sb.append(",\r\n").append("columns:").append("["); // columns begin
			subIndex = 0;
			for (String value : values) {
				sb.append(subIndex == 0 ? "" : ",");
				sb.append("{"); // lvc begin
				sb.append("id:").append("'lvc_").append(m_id).append("_").append(index).append("_").append(subIndex).append("'");
				sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(value)).append("'");
				if (value != null && value.length() > 18) sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(value)).append("'");
				editable = (subIndex == 0 || subIndex == 5 || (subIndex == 1 && m_acts != null && m_acts.size() > 1));
				sb.append(",").append("editable:").append(editable ? "true" : "false");
				sb.append(",").append("data:").append("{"); // data begin
				sb.append("}"); // data end
				sb.append("}\r\n"); // lvc end
				subIndex++;
			}
			sb.append("]"); // columns end

			sb.append("}\r\n"); // lvi end
			index++;
		}
		sb.append("]"); // rows end

		sb.append(",").append("data:").append("{"); // lv data begin
		sb.append("parentUNID:").append("'").append(this.m_parent.getUNID()).append("'");
		sb.append(",").append("parentDirectory:").append("'").append(this.m_parent.getDirectory()).append("'");
		sb.append(",").append("maxUploadSize:").append(CommonConfig.getInstance().getMaxUploadSize());
		if (m_toolbarRender != null) sb.append(",").append("toolbar:").append("{").append(m_toolbarRender.render(jspContext)).append("}\r\n");
		sb.append(",").append("readonly:").append(m_readonly ? "true" : "false");
		sb.append(",").append("belongId:").append("'").append(m_id).append("'");
		if (m_acts != null && m_acts.size() > 0) {
			sb.append(",").append("accessoryTypes:").append("["); // accessoryTypes begin
			index = 0;
			for (AccessoryType at : m_acts) {
				if (at == null) continue;
				sb.append(index == 0 ? "\r\n" : ",\r\n").append("{");
				sb.append("name").append(":'").append(at.getName()).append("'");
				sb.append(",").append("unid").append(":").append("'").append(at.getUNID()).append("'");
				sb.append(",").append("alias").append(":").append("'").append(at.getAlias()).append("'");
				sb.append(",").append("minCount").append(":").append(at.getMinCount());
				sb.append(",").append("maxCount").append(":").append(at.getMaxCount());
				sb.append(",").append("maxSize").append(":").append(at.getMaxSize());
				String fne = at.getFileNameExtension();
				String fnes[] = StringUtil.splitString(fne, ',');
				sb.append(",").append("fileNameExtension").append(":").append("[");
				if (fnes != null && fnes.length > 0) {
					int fneIdx = 0;
					for (String s : fnes) {
						if (s == null || s.length() == 0) continue;
						sb.append(fneIdx > 0 ? "," : "");
						sb.append("'").append(s).append("'");
						fneIdx++;
					}
				}
				sb.append("]");
				String desc = StringUtil.encode4Json(at.getDescription());
				if (desc != null && desc.length() > 0) sb.append(",").append("desc").append(":").append("'").append(desc).append("'");
				sb.append("}");
				index++;
			}
			sb.append("]\r\n"); // accessoryTypes end
		}

		sb.append(",").append("verbotenFileTypes:["); // verbotenFileTypes begin
		index = 0;
		for (ConfigEntry ce : VerbotenUploadFileTypeConfig.getInstance().getEntries()) {
			if (ce == null) continue;
			sb.append(index == 0 ? "" : ",").append("'").append(ce.getValue()).append("'");
			index++;
		}
		sb.append("]"); // verbotenFileTypes end

		sb.append("}"); // lv data end

		return sb.toString();
	}
}

