/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.filter;

/**
 * 用于记录用户自定义会话及其绑定请求信息以供后续使用的过滤器。
 * 
 * @author coca@tansuosoft.cn
 */
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;

/**
 * 用于设置用户自定义会话中相关参数的过滤器类。
 */
public class SessionContextFilter implements Filter {
	protected FilterConfig filterConfig = null;

	/**
	 * 记录用户自定义会话及其绑定请求信息。
	 * 
	 * <p>
	 * 如果当前用户为登录用户，则执行如下操作：
	 * </p>
	 * <p>
	 * <ul>
	 * <li>记录当前登录用户的自定义会话信息以供后续使用；</li>
	 * <li>把当前{@link javax.servlet.http.HttpServletRequest}对象绑定到用户自定义会话中。</li>
	 * </ul>
	 * </p>
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpSession httpSession = (request == null ? null : request.getSession());
		Session session = (Session) httpSession.getAttribute(Session.USERSESSION_PARAM_NAME_IN_HTTPSESSION);
		if (session != null) {
			User u = session.getUser();
			if (u != null && !u.isAnonymous()) {
				SessionContext.reset();
				SessionContext.storeSession(session);
				if (Session.getSession(session.getSessionUNID()) == null) {
					session = Session.newSession(session.getUser());
					httpSession.setAttribute(Session.USERSESSION_PARAM_NAME_IN_HTTPSESSION, session);
				}
				session.setHttpRequest(request);
			}
		}
		filterChain.doFilter(servletRequest, servletResponse);
	}

	public void destroy() {
		filterConfig = null;
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
}

