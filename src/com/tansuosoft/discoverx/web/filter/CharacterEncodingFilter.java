/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.filter;

/**
 * 用于设置 HTTP 请求字符编码的过滤器，通过过滤器参数encoding指明使用何种字符编码。
 * 
 * @author coca@tansuosoft.cn
 */
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 用于设置 HTTP 请求字符编码的过滤器类。
 * 
 * <p>
 * 具体请求内容的字符编码通过过滤器配置中名为“encoding”的参数提供。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class CharacterEncodingFilter implements Filter {
	protected FilterConfig filterConfig = null;
	protected String encoding = null;

	/**
	 * 重载doFilter
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		if (servletRequest.getCharacterEncoding() == null) {
			if (encoding != null && encoding.length() > 0) {
				servletRequest.setCharacterEncoding(encoding);
			}
		}
		filterChain.doFilter(servletRequest, servletResponse);
	}

	/**
	 * 重载destroy
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		filterConfig = null;
		encoding = null;
	}

	/**
	 * 重载init
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
		this.encoding = StringUtil.getValueString(filterConfig.getInitParameter("encoding"), "gbk");
	}
}

