/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.filter;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.common.ConfigEntry;
import com.tansuosoft.discoverx.common.UrlVerifyConfig;

/**
 * 判断用户当前访问的http请求对应的url是要求登录，如果要求且没登录则则重定向到登录地址。
 * 
 * @author coca@tansuosoft.cn
 */
public class UrlVerifyFilter implements Filter {

	/**
	 * 如果用户未登录，则重定向到登录页面的地址（login.jsp?redirect=）。
	 */
	private static final String redirectURL = "/login.jsp?redirect=";
	private static Pattern[] urlPatterns = null; // 所有需登录的才能访问的url地址匹配的正则表达式数组。

	/**
	 * 重载doFilter
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		boolean isLogin = (SessionContext.getSession(request) != null); // 是否登录。
		// 如果已经登录，那么不用判断。
		if (isLogin) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}

		if (urlPatterns != null && urlPatterns.length > 0) {
			// 没登录则循环判断是否访问的url是要求登录的，如果要求，则重定向到登录地址。
			String servletPath = null; // 当前请求地址。
			String qs = request.getQueryString();
			for (Pattern pattern : urlPatterns) {
				servletPath = request.getServletPath();
				if (pattern.matcher(servletPath).find()) {
					// 未登录则跳转到登录页面并指示登录后转到原先的地址。
					if (qs != null) servletPath = servletPath + "?" + qs;
					response.sendRedirect(request.getContextPath() + redirectURL + servletPath);
					return;
				}
			}
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	/**
	 * 重载destroy
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		urlPatterns = null;
	}

	/**
	 * 
	 * 重载init
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
		// 读取配置的需要登录才能访问的地址。
		if (urlPatterns == null) {
			UrlVerifyConfig urlVerifyConfig = UrlVerifyConfig.getInstance();
			List<ConfigEntry> verifies = urlVerifyConfig.getEntries();
			if (verifies != null) {
				urlPatterns = new Pattern[verifies.size()];
				int idx = 0;
				for (ConfigEntry entry : verifies) {
					urlPatterns[idx++] = Pattern.compile(entry.getValue(), Pattern.CASE_INSENSITIVE);
				}
			}
		}
	}

}

