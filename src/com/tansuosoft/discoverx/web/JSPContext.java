/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.ErrorData;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.el.ExpressionEvaluator;
import javax.servlet.jsp.el.VariableResolver;
import javax.servlet.jsp.tagext.BodyContent;

import com.tansuosoft.discoverx.bll.ResourceOpener;
import com.tansuosoft.discoverx.bll.ResourceOpenerProvider;
import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.function.OperationParser;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.bll.view.FormViewTable;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.DBException;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.JsonSerializer;

/**
 * 封装JSP页面上下文以提供常用功能的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class JSPContext {
	public static final String AVAIL_WIDTH_COOKIE_NAME = "availWidth";
	private ResourceContext m_resourceContext = ResourceContext.getInstance();
	private PageContext m_pageContext = null;
	private HttpServletRequest m_request = null;
	private HttpServletResponse m_response = null;
	private Session m_userSession = null;
	private Resource resource = null; // 最近一次请求处理之后后端服务器返回的资源，配合getLastResource使用。
	private User m_user = null;

	/**
	 * 接收当前jsp页面的javax.servlet.jsp.PageContext对象实例的构造器。
	 * 
	 * @param pageContext
	 */
	public JSPContext(PageContext pageContext) {
		this.m_pageContext = pageContext;
		if (this.m_pageContext != null) {
			this.m_request = (HttpServletRequest) this.m_pageContext.getRequest();
			this.m_response = (HttpServletResponse) this.m_pageContext.getResponse();
		}
		try {
			FormViewTable.startMonitor();
		} catch (Exception ex) {
			Session s = this.getUserSession();
			Session.setSessionParam(s, (m_request == null ? null : m_request.getSession()), Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, ex);
			this.redirect(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
			return;
		}
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @param arg0
	 * @return
	 * @see javax.servlet.jsp.JspContext#findAttribute(java.lang.String)
	 */
	public Object findAttribute(String arg0) {
		return m_pageContext.findAttribute(arg0);
	}

	/**
	 * 调用{@link javax.servlet.jsp.PageContext}同名方法。
	 * 
	 * @param arg0
	 * @throws ServletException
	 * @throws IOException
	 * @see javax.servlet.jsp.PageContext#forward(java.lang.String)
	 */
	public void forward(String arg0) throws ServletException, IOException {
		m_pageContext.forward(arg0);
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @param arg0
	 * @param arg1
	 * @return
	 * @see javax.servlet.jsp.JspContext#getAttribute(java.lang.String, int)
	 */
	public Object getAttribute(String arg0, int arg1) {
		return m_pageContext.getAttribute(arg0, arg1);
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @param arg0
	 * @return
	 * @see javax.servlet.jsp.JspContext#getAttribute(java.lang.String)
	 */
	public Object getAttribute(String arg0) {
		return m_pageContext.getAttribute(arg0);
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @param arg0
	 * @return Iterator<String>
	 * @see javax.servlet.jsp.JspContext#getAttributeNamesInScope(int)
	 */
	@SuppressWarnings("unchecked")
	public Iterator<String> getAttributeNamesInScope(int arg0) {
		Enumeration<String> enumeration = m_pageContext.getAttributeNamesInScope(arg0);
		Iterator<String> itr = null;
		ArrayList<String> al = new ArrayList<String>();
		while (enumeration.hasMoreElements()) {
			al.add(enumeration.nextElement());
		}
		if (al != null && !al.isEmpty()) {
			itr = al.iterator();
		}
		return itr;
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @param arg0
	 * @return
	 * @see javax.servlet.jsp.JspContext#getAttributesScope(java.lang.String)
	 */
	public int getAttributesScope(String arg0) {
		return m_pageContext.getAttributesScope(arg0);
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.PageContext#getErrorData()
	 */
	public ErrorData getErrorData() {
		return m_pageContext.getErrorData();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.PageContext#getException()
	 */
	public Exception getException() {
		return m_pageContext.getException();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.JspContext#getExpressionEvaluator()
	 */
	public ExpressionEvaluator getExpressionEvaluator() {
		return m_pageContext.getExpressionEvaluator();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.JspContext#getOut()
	 */
	public JspWriter getOut() {
		return m_pageContext.getOut();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.PageContext#getPage()
	 */
	public Object getPage() {
		return m_pageContext.getPage();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.PageContext#getRequest()
	 */
	public ServletRequest getRequest() {
		return m_pageContext.getRequest();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.PageContext#getResponse()
	 */
	public ServletResponse getResponse() {
		return m_pageContext.getResponse();
	}

	/**
	 * @return
	 * @see javax.servlet.jsp.PageContext#getServletConfig()
	 */
	public ServletConfig getServletConfig() {
		return m_pageContext.getServletConfig();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.PageContext#getServletContext()
	 */
	public ServletContext getServletContext() {
		return m_pageContext.getServletContext();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.PageContext#getSession()
	 */
	public HttpSession getSession() {
		return m_pageContext.getSession();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.JspContext#getVariableResolver()
	 */
	public VariableResolver getVariableResolver() {
		return m_pageContext.getVariableResolver();
	}

	/**
	 * 调用{@link javax.servlet.jsp.PageContext}同名方法。
	 * 
	 * @param arg0
	 * @throws ServletException
	 * @throws IOException
	 * @see javax.servlet.jsp.PageContext#handlePageException(java.lang.Exception)
	 */
	public void handlePageException(Exception arg0) throws ServletException, IOException {
		m_pageContext.handlePageException(arg0);
	}

	/**
	 * 调用{@link javax.servlet.jsp.PageContext}同名方法。
	 * 
	 * @param arg0
	 * @throws ServletException
	 * @throws IOException
	 * @see javax.servlet.jsp.PageContext#handlePageException(java.lang.Throwable)
	 */
	public void handlePageException(Throwable arg0) throws ServletException, IOException {
		m_pageContext.handlePageException(arg0);
	}

	/**
	 * 调用{@link javax.servlet.jsp.PageContext}同名方法。
	 * 
	 * @param arg0
	 * @param arg1
	 * @throws ServletException
	 * @throws IOException
	 * @see javax.servlet.jsp.PageContext#include(java.lang.String, boolean)
	 */
	public void include(String arg0, boolean arg1) throws ServletException, IOException {
		m_pageContext.include(arg0, arg1);
	}

	/**
	 * 调用{@link javax.servlet.jsp.PageContext}同名方法。
	 * 
	 * @param arg0
	 * @throws ServletException
	 * @throws IOException
	 * @see javax.servlet.jsp.PageContext#include(java.lang.String)
	 */
	public void include(String arg0) throws ServletException, IOException {
		m_pageContext.include(arg0);
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.JspContext#popBody()
	 */
	public JspWriter popBody() {
		return m_pageContext.popBody();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @return
	 * @see javax.servlet.jsp.PageContext#pushBody()
	 */
	public BodyContent pushBody() {
		return m_pageContext.pushBody();
	}

	/**
	 * 返回{@link javax.servlet.jsp.PageContext}同名方法的结果。
	 * 
	 * @param writer
	 * @return
	 * @see javax.servlet.jsp.JspContext#pushBody(java.io.Writer)
	 */
	public JspWriter pushBody(Writer writer) {
		return m_pageContext.pushBody(writer);
	}

	/**
	 * 调用{@link javax.servlet.jsp.PageContext}同名方法。
	 * 
	 * @param arg0
	 * @param arg1
	 * @see javax.servlet.jsp.JspContext#removeAttribute(java.lang.String, int)
	 */
	public void removeAttribute(String arg0, int arg1) {
		m_pageContext.removeAttribute(arg0, arg1);
	}

	/**
	 * 调用{@link javax.servlet.jsp.PageContext}同名方法。
	 * 
	 * @param arg0
	 * @see javax.servlet.jsp.JspContext#removeAttribute(java.lang.String)
	 */
	public void removeAttribute(String arg0) {
		m_pageContext.removeAttribute(arg0);
	}

	/**
	 * 调用{@link javax.servlet.jsp.PageContext}同名方法。
	 * 
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @see javax.servlet.jsp.JspContext#setAttribute(java.lang.String, java.lang.Object, int)
	 */
	public void setAttribute(String arg0, Object arg1, int arg2) {
		m_pageContext.setAttribute(arg0, arg1, arg2);
	}

	/**
	 * 调用{@link javax.servlet.jsp.PageContext}同名方法。
	 * 
	 * @param arg0
	 * @param arg1
	 * @see javax.servlet.jsp.JspContext#setAttribute(java.lang.String, java.lang.Object)
	 */
	public void setAttribute(String arg0, Object arg1) {
		m_pageContext.setAttribute(arg0, arg1);
	}

	/**
	 * 获取路径信息。
	 * 
	 * @return {@link PathContext}
	 */
	public PathContext getPathContext() {
		return new PathContext(this.m_request);
	}

	private Portal m_portal = null;

	/**
	 * 获取当前用户门户信息。
	 * 
	 * @return {@link Portal}
	 */
	public Portal getPortal() {
		if (m_portal == null) m_portal = Profile.getInstance(this.getLoginUser()).getUsePortal();
		return m_portal;
	}

	/**
	 * 从当前HTTP会话中获取当前用户自定义会话信息。
	 * 
	 * @return {@link com.tansuosoft.discoverx.model.Session}
	 */
	public Session getUserSession() {
		if (this.m_userSession == null) {
			this.m_userSession = SessionContext.getSession(this.m_request);
		}
		return this.m_userSession;
	}

	/**
	 * 获取当前所有登录用户自定义会话的个数。
	 * 
	 * @return
	 */
	public int getUserSessionCount() {
		return Session.getSessionCount();
	}

	/**
	 * 获取登录的用户信息。
	 * 
	 * @return Participant 如果没有登录，则返回匿名用户对象。
	 */
	public User getLoginUser() {
		if (this.m_user == null) {
			this.m_user = Session.getUser(getUserSession());
		}
		return this.m_user;
	}

	/**
	 * 判断当前请求是否登录用户发送的请求。
	 * <p>
	 * 可在页面中用此方法判断用户是否登录。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean hasLogin() {
		return (this.getUserSession() != null);
	}

	/**
	 * 返回obj对象的toString方法，如果obj为null，则返回defaultString值。
	 * 
	 * @param obj
	 * @param defaultString
	 * @return String
	 */
	public String getString(Object obj, String defaultString) {
		if (obj == null) return defaultString;
		return StringUtil.convertToString(obj, defaultString);
	}

	/**
	 * 返回obj对象的toString方法，如果obj为null，则返回空字符串。
	 * 
	 * @param obj
	 * @param defaultString
	 * @return String
	 */
	public String getString(Object obj) {
		return this.getString(obj, "");
	}

	/**
	 * 获取发送当前请求的浏览器类型和版本
	 * 
	 * @return Browser
	 */
	public Browser getBrowser() {
		HttpServletRequest request = (HttpServletRequest) this.getRequest();
		return Browser.getBrowser(request);
	}

	/**
	 * 根据资源类信息获取资源描述信息。
	 * 
	 * @param cls Class<?> 资源类信息。
	 * @return ResourceDescriptor
	 */
	public ResourceDescriptor getResourceDescriptor(Class<?> cls) {
		ResourceDescriptorConfig cfg = ResourceDescriptorConfig.getInstance();
		return cfg.getResourceDescriptor(cls);
	}

	/**
	 * 根据资源UNID和资源类信息从{@link ResourceContext}中获取资源实例。
	 * 
	 * @param unid
	 * @param clazz
	 * @return Resource 找不到资源时可能返回null。
	 */
	public Resource getResource(String unid, Class<?> clazz) {
		return this.m_resourceContext.getResource(unid, clazz);
	}

	/**
	 * 根据资源UNID和资源类信息获取资源名称，如果找不到则返回defaultReturn。
	 * 
	 * @param unid
	 * @param clazz
	 * @param defaultReturn
	 * @return
	 */
	public String getResourceName(String unid, Class<?> clazz, String defaultReturn) {
		if (unid == null || unid.length() == 0) return defaultReturn;
		Resource r = this.getResource(unid, clazz);
		if (r == null) return defaultReturn;
		return r.getName();
	}

	/**
	 * 打开unid和cls指定的资源。
	 * 
	 * @param unid
	 * @param cls
	 * @return Resource
	 */
	public Resource openResource(String unid, Class<?> cls) {
		if (unid == null || unid.length() == 0 || cls == null) return null;
		ResourceOpener opener = ResourceOpenerProvider.getResourceOpener(cls);
		if (opener == null) return null;
		Session session = this.getUserSession();
		try {
			return opener.open(unid, cls, session);
		} catch (ResourceException e) {
			Session.setSessionParam(session, this.getSession(), Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, e);
			User u = this.getLoginUser();
			FileLogger.debug("用户“%1$s”尝试打开“%2$s://%3$s”对应的资源时失败！", (u == null ? "匿名用户" : u.getName()), StringUtil.toCamelCase(cls.getSimpleName()), unid);
		}
		return null;
	}

	/**
	 * 将r指定的资源序列化为json格式。
	 * 
	 * @param r 目标资源
	 * @param defaultIfNull 如果r为null，则返回这个值。
	 * @return String
	 */
	public static String toJson(Resource r, String defaultIfNull) {
		if (r == null) return defaultIfNull;
		JsonSerializer jsonSerializer = new JsonSerializer();
		StringWriter writer = new StringWriter();
		jsonSerializer.serialize(r, writer);
		return writer.toString();
	}

	/**
	 * 重定向到urlConfigName指定名称对应的url地址。
	 * 
	 * <p>
	 * 需在{@link UrlConfig}中配置urlConfigName指定名称对应的url地址相关信息。
	 * </p>
	 * 
	 * @param urlConfigName String
	 */
	public void redirect(String urlConfigName) {
		if (this.m_response == null || this.m_request == null) return;
		PathContext pathContext = new PathContext(this.m_request);
		UrlConfig urlConfig = UrlConfig.getInstance();
		String errorUrl = pathContext.parseUrl(urlConfig.getUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		try {
			if (urlConfigName == null || urlConfigName.length() == 0) {
				Session.setSessionParam(m_userSession, m_request.getSession(), Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, new Exception("未指定有效地址名称！"));
				this.m_response.sendRedirect(errorUrl);
				return;
			}
			String url = pathContext.parseUrl(urlConfig.getUrl(urlConfigName));
			if (url == null || url.length() == 0) {
				Session.setSessionParam(m_userSession, m_request.getSession(), Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, new Exception("无法获取“" + urlConfigName + "”对应的地址！"));
				this.m_response.sendRedirect(errorUrl);
				return;
			}
			this.m_response.sendRedirect(url);
			return;
		} catch (IOException ex) {
			FileLogger.error(ex);
		}
	}

	/**
	 * 获取当前请求对应的jsp文件名。
	 * 
	 * @return String
	 */
	public String getJSPFile() {
		if (this.m_request == null) return "";
		String JSPFile = this.m_request.getServletPath();
		if (JSPFile.startsWith("/")) JSPFile = JSPFile.substring(1);
		int pos = JSPFile.lastIndexOf("/");
		if (pos > 0) JSPFile = JSPFile.substring(pos + 1);
		return JSPFile;
	}

	/**
	 * 如果存在与jsp同名的.css文件，那么输出html样式信息&lt;link href="主题路径下的css文件"...&gt;，否则输出空字符串。
	 * 
	 * @return String
	 */
	public String getJSPFileCssFile() {
		String cssName = this.getJSPFile().replace(".jsp", "") + ".css";
		if (cssName == null || cssName.length() == 0) return "";
		String path = com.tansuosoft.discoverx.common.CommonConfig.getInstance().getInstallationPath();
		if (path == null || path.length() == 0) return "";
		String themePath = new PathContext(this.m_request).getThemePath();
		if (themePath == null || themePath.length() == 0) return "";
		String themePathLocal = new String(themePath);
		int pos = themePathLocal.indexOf("/", 1);
		if (pos > 0) themePathLocal = themePathLocal.substring(pos + 1);
		themePathLocal = themePathLocal.replace('/', File.separatorChar);
		String cssPath = String.format("%1$s%2$s%3$s", path, themePathLocal, cssName);
		File f = new File(cssPath);
		if (f.exists()) return String.format("<link rel=\"stylesheet\" href=\"%1$s%2$s\" type=\"text/css\"/>", themePath, cssName);
		return "";
	}

	/**
	 * 输出basePath下的cssName指定的样式表html样式节点，格式为：&lt;link rel="stylesheet" type="text/css" href="{basePath}{cssName}"/&gt;
	 * 
	 * @param cssName
	 * @param basePath
	 * @return String
	 */
	public String outputCss(String cssName, String basePath) {
		if (cssName == null || cssName.length() == 0) return "";
		return String.format("<link rel=\"stylesheet\" type=\"text/css\" href=\"%1$s%2$s\"/>\r\n", basePath, cssName);
	}

	/**
	 * 输出basePath下的jsName指定的script节点，格式为：&lt;script type="text/javascript" language="javascript" src="{basePath}{cssName}"&gt&lt;script&gt;
	 * 
	 * @param jsName
	 * @param basePath
	 * @return String
	 */
	public String outputJs(String jsName, String basePath) {
		if (jsName == null || jsName.length() == 0) return "";
		return String.format("<script type=\"text/javascript\" src=\"%1$s%2$s\"></script>\r\n", basePath, jsName);
	}

	private List<String> m_csslist = null;
	private List<String> m_jslist = null;

	/**
	 * 输出basePath下的cssName指定的样式表html link节点。
	 * 
	 * @param cssName
	 * @param basePath
	 * @param merge 如果merge为true，返回空字符串以待后续合并输出(此时需调用{@link #mergeCss(String[])}才能真正输出)，如果为false则直接返回{@link #outputCss(String, String)}的结果。
	 * @return String
	 */
	public String outputCss(String cssName, String basePath, boolean merge) {
		if (cssName == null || cssName.length() == 0) return "";
		String r = basePath + cssName;
		if (merge) {
			if (m_csslist == null) m_csslist = new ArrayList<String>();
			m_csslist.add(r);
			return "";
		} else {
			return outputCss(cssName, basePath);
		}
	}

	/**
	 * 输出basePath下的jsName指定的script节点。
	 * 
	 * @param jsName
	 * @param basePath
	 * @param merge 如果merge为true，返回空字符串以待后续合并输出(此时需调用{@link #mergeJs(String[])}才能真正输出)，如果为false则直接返回{@link #outputJs(String, String)}的结果。
	 * @return String
	 */
	public String outputJs(String jsName, String basePath, boolean merge) {
		if (jsName == null || jsName.length() == 0) return "";
		String r = basePath + jsName;
		if (merge) {
			if (m_jslist == null) m_jslist = new ArrayList<String>();
			m_jslist.add(r);
			return "";
		} else {
			return outputJs(jsName, basePath);
		}
	}

	/**
	 * 合并输出所有css文件为一个引用文件。
	 * 
	 * <p>
	 * 只能在所有需要合并的css都追加了之后调用一次。所有待合并的css文件必须都存在于同一个web应用程序的相同路径下。
	 * </p>
	 * 
	 * @param encoding 表示输入输出文件的编码，如“utf-8”、“gbk等”，如果不提供则使用http请求中的编码(即{@link javax.servlet.ServletRequest#getCharacterEncoding()}的结果)。
	 * @param css 包含所有css文件的数组，每个值为一个引用css的url路径，如果提供则次序在通过{@link #outputCss(String, String, boolean)}(merge为true)添加的结果之后。
	 * 
	 * @return String
	 */
	public String mergeCss(String encoding, String[] css) {
		if (css != null && css.length > 0) {
			for (String s : css) {
				if (s == null) continue;
				if (m_csslist == null) m_csslist = new ArrayList<String>();
				m_csslist.add(s);
			}
		}
		return this.outputCss(merge(m_csslist, ".css", encoding), "");
	}

	/**
	 * 合并输出所有js文件为一个引用文件。
	 * 
	 * <p>
	 * 只能在所有需要合并的js都追加了之后调用一次。所有待合并的js文件必须都存在于同一个web应用程序根路径下。
	 * </p>
	 * 
	 * @param encoding 表示输入输出文件的编码，如“utf-8”、“gbk等”，如果不提供则使用http请求中的编码(即{@link javax.servlet.ServletRequest#getCharacterEncoding()}的结果)。
	 * @param js 包含所有js文件的数组，每个值为一个引用js的url路径，如果提供则次序在通过{@link #outputJs(String, String, boolean)}(merge为true)添加的结果之后。
	 * 
	 * @return String
	 */
	public String mergeJs(String encoding, String[] js) {
		if (js != null && js.length > 0) {
			for (String s : js) {
				if (s == null) continue;
				if (m_jslist == null) m_jslist = new ArrayList<String>();
				m_jslist.add(s);
			}
		}
		return this.outputJs(merge(m_jslist, ".js", encoding), "");
	}

	/**
	 * 合并输出所有css文件为一个引用文件。
	 * 
	 * <p>
	 * 只能在所有需要合并的css都追加了之后调用一次。所有待合并的css文件必须都存在于同一个web应用程序的相同路径下。
	 * </p>
	 * <p>
	 * 使用http请求中的编码(即{@link javax.servlet.ServletRequest#getCharacterEncoding()}的结果)作为输入输出文件的编码。
	 * </p>
	 * 
	 * @param css 包含所有css文件的数组，每个值为一个引用css的url路径，如果提供则次序在通过{@link #outputCss(String, String, boolean)}(merge为true)添加的结果之后。
	 * 
	 * @return String
	 */
	public String mergeCss(String[] css) {
		return mergeCss(null, css);
	}

	/**
	 * 合并输出所有js文件为一个引用文件。
	 * 
	 * <p>
	 * 只能在所有需要合并的js都追加了之后调用一次。所有待合并的js文件必须都存在于同一个web应用程序根路径下。
	 * </p>
	 * <p>
	 * 使用http请求中的编码(即{@link javax.servlet.ServletRequest#getCharacterEncoding()}的结果)作为输入输出文件的编码。
	 * </p>
	 * 
	 * @param js 包含所有js文件的数组，每个值为一个引用js的url路径，如果提供则次序在通过{@link #outputJs(String, String, boolean)}(merge为true)添加的结果之后。
	 * 
	 * @return String
	 */
	public String mergeJs(String[] js) {
		return mergeJs(null, js);
	}

	/**
	 * 合并输出所有文本文件为一个引用文件。
	 * 
	 * @param urls
	 * @param ext
	 * @param enc
	 * @return String
	 */
	private String merge(List<String> urls, String ext, String enc) {
		if (urls == null || urls.isEmpty()) return "";
		String result = null;
		try {
			String encoding = (StringUtil.isBlank(enc) ? this.getRequest().getCharacterEncoding() : enc);
			String jspfile = this.m_request.getServletPath().replace('/', '_').replace('\\', '_').replace('.', '_') + ext;
			String approot = this.getPathContext().getWebAppRoot();
			int protocolEndPos = -1;
			int approotPos = -1;
			String installPath = CommonConfig.getInstance().getInstallationPath();
			String fp = null;
			boolean isCss = (".css".equalsIgnoreCase(ext));

			String pathPrefix = "res";
			if (isCss) {
				String s = urls.get(0);
				protocolEndPos = s.indexOf("://");
				if (protocolEndPos < 0) protocolEndPos = 0;
				approotPos = s.indexOf(approot, (protocolEndPos == 0 ? 0 : protocolEndPos + 3));
				pathPrefix = s.substring(approotPos + approot.length()).replace('/', File.separatorChar).replace('\\', File.separatorChar);
				if (pathPrefix == null) pathPrefix = "";
				pathPrefix = StringUtil.stringLeftBack(pathPrefix, File.separator);
			}

			String outputDir = installPath + pathPrefix + File.separator;
			File resdir = new File(outputDir);
			resdir.mkdirs();
			String outfp = outputDir + jspfile;
			result = approot + pathPrefix.replace('\\', '/') + "/" + jspfile;

			File f = new File(outfp);
			if (f.exists()) {
				DateTime now = new DateTime();
				DateTime dt = new DateTime(f.lastModified());
				if (now.getDate().equalsIgnoreCase(dt.getDate())) return result;
			}
			if (f.exists() && !f.delete()) return result;
			PrintWriter outputWriter = new PrintWriter(new FileWriter(outfp, false));
			for (String s : urls) {
				if (s == null || s.length() == 0) continue;
				protocolEndPos = s.indexOf("://");
				if (protocolEndPos < 0) protocolEndPos = 0;
				approotPos = s.indexOf(approot, (protocolEndPos == 0 ? 0 : protocolEndPos + 3));
				if (approotPos < 0) {
					FileLogger.debug("资源“%1$s”无法合并输出！", s);
					continue;
				}
				fp = (installPath + s.substring(approotPos + approot.length())).replace('/', File.separatorChar).replace('\\', File.separatorChar);
				f = new File(fp);
				if (!f.exists() || !f.isFile()) {
					FileLogger.debug("无法找到资源“%1$s”对应的文件！", s);
					continue;
				}
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), encoding));
				String line = null;
				while ((line = br.readLine()) != null) {
					outputWriter.println(line);
				}
				br.close();
			}// for end
			outputWriter.close();
		} catch (IOException ex) {
			FileLogger.error(ex);
			return "";
		}
		return result;
	}

	/**
	 * 关闭页面输出缓存。
	 * <p>
	 * 根据需要使用
	 * </p>
	 */
	public void setNoCache() {
		if (this.m_response == null) return;
		// this.m_response.setHeader("Pragma", "No-cache"); // 用于http/1.0
		this.m_response.setHeader("Cache-Control", "no-cache");
		this.m_response.setDateHeader("Expires", 0);
	}

	/**
	 * 控制页面输出缓存。
	 * 
	 * @param period 缓存周期，单位为妙，如果为0，则表示缓存一年不过期。
	 */
	public void setCache(int period) {
		if (this.m_response == null) return;
		if (period <= 0) period = 60 * 60 * 24 * 365;
		this.m_response.setHeader("Cache-Control", "max-age=" + period + ", must-revalidate");
		DateTime dt = new DateTime();
		dt.adjustSecond(period);
		this.m_response.setDateHeader("Expires", dt.getTimeMillis());
	}

	/**
	 * 获取最近一次请求处理之后后端服务器返回的资源。
	 * 
	 * <p>
	 * 此方法先尝试从用户自定义会话中获取，如果没有，则从http会话中获取。
	 * </p>
	 * <p>
	 * 此方不会从用户自定义会话或http会话中删除获取到的资源。
	 * </p>
	 * 
	 * @return Resource 返回获取的资源，如果找不到则返回null。
	 */
	public Resource getLastResource() {
		if (this.resource != null) return this.resource;
		Session session = this.getUserSession();
		HttpSession httpSession = this.getSession();
		Object obj = Session.getSessionParam(session, httpSession, Session.LASTRESOURCE_PARAM_NAME_IN_HTTPSESSION, null, false);
		if (obj != null && obj instanceof Resource) this.resource = (Resource) obj;
		return this.resource;
	}

	/**
	 * 获取最近一次请求处理之后后端服务器返回的文档资源。
	 * 
	 * <p>
	 * 此方法先尝试从用户自定义会话中获取，如果没有，则从http会话中获取。
	 * </p>
	 * <p>
	 * 此方不会从用户自定义会话或http会话中删除获取到的文档。
	 * </p>
	 * 
	 * @return Document 返回获取的文档资源，如果找不到则返回null。
	 */
	public Document getLastDocument() throws ResourceException {
		if (this.resource != null && this.resource instanceof Document) return (Document) this.resource;
		Session session = this.getUserSession();
		HttpSession httpSession = this.getSession();
		Object obj = Session.getSessionParam(session, httpSession, Session.LASTDOCUMENT_PARAM_NAME_IN_HTTPSESSION, null, false);
		if (obj != null && obj instanceof Document) this.resource = (Document) obj;
		return (Document) this.resource;
	}

	/**
	 * 先从当前用户自定义会话({@link com.tansuosoft.discoverx.model.Session})中获取最近一次处理结果对应的消息文本。<br/>
	 * 如果不存在，则从http会话({@link javax.servlet.http.HttpSession})中获取，如果都取不到则返回defaultMessage。
	 * 
	 * <p>
	 * 此方法从用户自定义会话或http会话中获取完信息后，如果removeAfterFetch为true则会删除用户自定义会话或http会话中获取到的参数结果。
	 * </p>
	 * 
	 * @param defaultMessage String
	 * @param removeAfterFetch boolean
	 * @return String
	 */
	public String getLastMessage(String defaultMessage, boolean removeAfterFetch) {
		String message = defaultMessage;
		Object obj = null;
		Session userSession = this.getUserSession();
		HttpSession session = this.getSession();
		obj = Session.getSessionParam(userSession, session, Session.LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION, null, removeAfterFetch);
		if (obj != null && obj instanceof String) message = (String) obj;
		return message;
	}

	/**
	 * 先从当前用户自定义会话({@link com.tansuosoft.discoverx.model.Session})中获取最近一次处理结果对应的{@link Exception}。<br/>
	 * 如果不存在，则从http会话({@link javax.servlet.http.HttpSession})中获取，如果都取不到则返回null。
	 * 
	 * <p>
	 * 此方法从用户自定义会话或http会话中获取完信息后，如果removeAfterFetch为true则会删除用户自定义会话或http会话中获取到的参数结果。
	 * </p>
	 * 
	 * @param removeAfterFetch boolean
	 * @return Exception
	 */
	public Exception getLastError(boolean removeAfterFetch) {
		Exception result = null;
		Object obj = null;
		Session userSession = this.getUserSession();
		HttpSession session = this.getSession();
		obj = Session.getSessionParam(userSession, session, Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, null, removeAfterFetch);
		if (obj != null && obj instanceof Exception) result = (Exception) obj;
		return result;
	}

	/**
	 * 先从当前用户自定义会话({@link com.tansuosoft.discoverx.model.Session})中获取最近一次处理结果对应的{@link AJAXResponse}。<br/>
	 * 如果不存在，则从http会话({@link javax.servlet.http.HttpSession})中获取，如果都取不到则返回null。
	 * 
	 * <p>
	 * 此方法从用户自定义会话或http会话中获取完信息后，如果removeAfterFetch为true则会删除用户自定义会话或http会话中获取到的参数结果。
	 * </p>
	 * 
	 * @param removeAfterFetch boolean
	 * @return AJAXResponse
	 */
	public AJAXResponse getLastAJAXResponse(boolean removeAfterFetch) {
		AJAXResponse result = null;
		Object obj = null;
		Session userSession = this.getUserSession();
		HttpSession session = this.getSession();
		obj = Session.getSessionParam(userSession, session, Session.LASTAJAXRESPONSE_PARAM_NAME_IN_HTTPSESSION, null, removeAfterFetch);
		if (obj != null && obj instanceof AJAXResponse) result = (AJAXResponse) obj;
		return result;
	}

	/**
	 * 先从当前用户自定义会话({@link com.tansuosoft.discoverx.model.Session})中获取最近一次处理结果对应的操作信息，如果不存在，则从http会话({@link javax.servlet.http.HttpSession})中获取，如果都取不到则返回null。
	 * 
	 * <p>
	 * 此方法从用户自定义会话或http会话中获取完信息后，如果removeAfterFetch为true则会删除用户自定义会话或http会话中获取到的参数结果。
	 * </p>
	 * 
	 * @param removeAfterFetch boolean
	 * @return {@link OperationParser} 返回操作的解析对象。
	 */
	public OperationParser getLastOperation(boolean removeAfterFetch) {
		OperationParser result = null;
		Object obj = null;
		Session userSession = this.getUserSession();
		HttpSession session = this.getSession();
		obj = Session.getSessionParam(userSession, session, Session.LASTOPERATION_PARAM_NAME_IN_HTTPSESSION, null, removeAfterFetch);
		if (obj != null && obj instanceof OperationParser) result = (OperationParser) obj;
		return result;
	}

	/**
	 * 先从当前用户自定义会话({@link com.tansuosoft.discoverx.model.Session})中获取最近一次处理结果对应的操作结果信息（{@link OperationResult}）。<br/>
	 * 如果不存在，则从http会话({@link javax.servlet.http.HttpSession})中获取，如果都取不到则返回null。
	 * 
	 * <p>
	 * 此方法从用户自定义会话或http会话中获取完信息后，如果removeAfterFetch为true则会删除用户自定义会话或http会话中获取到的参数结果。
	 * </p>
	 * 
	 * @param removeAfterFetch boolean
	 * @return OperationResult
	 */
	public OperationResult getLastOperationResult(boolean removeAfterFetch) {
		OperationResult result = null;
		Object obj = null;
		Session userSession = this.getUserSession();
		HttpSession session = this.getSession();
		obj = Session.getSessionParam(userSession, session, Session.LASTOPERATIONRESULT_PARAM_NAME_IN_HTTPSESSION, null, removeAfterFetch);
		if (obj != null) result = (OperationResult) obj;
		return result;
	}

	/**
	 * 先从当前用户自定义会话({@link com.tansuosoft.discoverx.model.Session})中获取最近一次请求的有效Url信息，如果不存在，则从http会话({@link javax.servlet.http.HttpSession})中获取，如果都取不到则返回null。
	 * 
	 * <p>
	 * 此方法从用户自定义会话或http会话中获取完信息后，如果removeAfterFetch为true则会删除用户自定义会话或http会话中获取到的参数结果。
	 * </p>
	 * 
	 * @param removeAfterFetch boolean
	 * @return String
	 */
	public String getLastRequestUrl(boolean removeAfterFetch) {
		String result = null;
		Object obj = null;
		Session userSession = this.getUserSession();
		HttpSession session = this.getSession();
		obj = Session.getSessionParam(userSession, session, Session.LASTREQUESTURL_PARAM_NAME_IN_HTTPSESSION, null, removeAfterFetch);
		if (obj != null) result = obj.toString();
		return result;
	}

	/**
	 * 设置消息。
	 * 
	 * <p>
	 * 如果有自定义会话，那么设置到其LastMessage属性中，否则设置到HttpSession的名为Session.LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION的参数中。
	 * </p>
	 * 
	 * @param message String 消息内容，可以为null。
	 */
	public void setMessage(String message) {
		Session session = this.getUserSession();
		HttpSession httpSession = this.getSession();
		Session.setSessionParam(session, httpSession, Session.LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION, message);
	}

	/**
	 * 返回select控件的option对应的html文本。
	 * 
	 * <p>
	 * 如果value和check.toString()的结果相同，则返回格式为：
	 * 
	 * <pre>
	 * &lt;option value="{value}" selected="true"&gt;{text}&lt;/option&gt;
	 * 
	 * <pre>
	 * 
	 * 否则返回：
	 * 
	 * <pre>
	 * &lt;option value="{value}"&gt;{text}&lt;/option&gt;
	 * 
	 * <pre>
	 * </p>
	 * 
	 * @param value
	 * @param check
	 * @param text
	 * @return
	 */
	public String getOption(String value, Object check, String text) {
		if (value == null || text == null) return "";
		String str = (check == null ? "" : check.toString());
		StringBuilder sb = new StringBuilder();
		sb.append("<option value=\"").append(value).append("\"").append((str.equalsIgnoreCase(value)) ? " selected=\"true\"" : "").append(">").append(text).append("</option>");
		return sb.toString();
	}

	/**
	 * 如果check和v.toString()的结果相同，则返回“ checked="true"”，否则返回空字符串。
	 * 
	 * @param check
	 * @param v
	 * @return
	 */
	public String getChecked(String check, Object v) {
		if (v == null) return "";
		String str = v.toString();
		if (str.equalsIgnoreCase(check)) { return " checked=\"true\""; }
		return "";
	}

	/**
	 * 从名为“availWidth”的cookie中获取availWidth的结果。
	 * 
	 * @param defaultReturn int 如果找不到则返回此默认结果。
	 * @return
	 */
	public int getAvailWidth(int defaultReturn) {
		Cookie[] cookies = this.m_request.getCookies();
		if (cookies == null || cookies.length == 0) return defaultReturn;
		String cookieValue = null;
		for (Cookie x : cookies) {
			if (AVAIL_WIDTH_COOKIE_NAME.equalsIgnoreCase(x.getName())) {
				cookieValue = x.getValue();
			}
		}
		if (cookieValue == null || cookieValue.length() == 0) return defaultReturn;
		return StringUtil.getValueInt(cookieValue, defaultReturn);
	}

	/**
	 * 剥离消息中的java错误类型前缀。
	 * 
	 * @param msg
	 * @return
	 */
	public String stripExPrefx(String msg) {
		if (msg == null || msg.isEmpty()) return msg;
		int pos = 0;
		String exPrefixes[] = { RuntimeException.class.getName(), Exception.class.getName(), DBException.class.getName(), ResourceException.class.getName() };
		for (String prefix : exPrefixes) {
			if ((pos = msg.lastIndexOf(prefix + ":")) >= 0) {
				msg = msg.substring(pos + prefix.length() + 1);
			}
		}
		return msg;
	}
}
