/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;

import com.tansuosoft.discoverx.common.event.EventSourceImpl;

/**
 * {@link HttpSession}中的属性变更事件处理程序实现类。
 * 
 * @deprecated
 * @author coca@tansuosoft.cn
 */
@Deprecated
public class HttpSessionAttributeListener extends EventSourceImpl implements javax.servlet.http.HttpSessionAttributeListener {
	private SessionListener m_delegate = null;

	/**
	 * 缺省构造器。
	 */
	public HttpSessionAttributeListener() {
		m_delegate = new SessionListener();
	}

	/**
	 * Http会话中增加属性时触发的事件：根据要求触发用户登录事件。
	 * 
	 * @see javax.servlet.http.HttpSessionAttributeListener#attributeAdded(HttpSessionBindingEvent)
	 */
	public void attributeAdded(HttpSessionBindingEvent arg0) {
		m_delegate.attributeAdded(arg0);
	}

	/**
	 * Http会话中删除属性时触发的事件：根据要求触发用户注销事件。
	 * 
	 * @see javax.servlet.http.HttpSessionAttributeListener#attributeRemoved(HttpSessionBindingEvent)
	 */
	public void attributeRemoved(HttpSessionBindingEvent arg0) {
		m_delegate.attributeRemoved(arg0);
	}

	/**
	 * Http会话中修改属性值时触发的事件：不做任何操作。
	 * 
	 * @see javax.servlet.http.HttpSessionAttributeListener#attributeReplaced(javax.servlet.http.HttpSessionBindingEvent)
	 */
	public void attributeReplaced(HttpSessionBindingEvent arg0) {
		m_delegate.attributeReplaced(arg0);
	}
}
