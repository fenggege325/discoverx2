/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

/**
 * {@link ServletContext}初始化和终止时执行的事件处理程序实现类。
 * 
 * @deprecated
 * @author coca@tansuosoft.cn
 */
@Deprecated
public class ServletContextListener implements javax.servlet.ServletContextListener {
	private ContextListener m_delegate = null;

	/**
	 * 缺省构造器。
	 */
	public ServletContextListener() {
		m_delegate = new ContextListener();
	}

	/**
	 * 重载contextDestroyed
	 * 
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		m_delegate.contextDestroyed(arg0);
	}

	/**
	 * 重载contextInitialized
	 * 
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		m_delegate.contextInitialized(arg0);
	}
}
