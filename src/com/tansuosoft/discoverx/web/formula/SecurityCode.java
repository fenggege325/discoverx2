/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.ParticipantTree;

/**
 * 返回安全编码的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>seed</td>
 * <td>种子编码，如果不提供，则为当前用户的安全编码。</td>
 * </tr>
 * <tr>
 * <td>level</td>
 * <td>级别，如果为0（默认），则表示返回种子安全编码，如果为1，则返回上一级安全编码，以此类推。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>int</td>
 * <td>安全编码数字，找不到则返回-1。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 2, alias = "SC", name = "获取安全编码的公式", desc = "获取安全编码的公式")
public class SecurityCode extends Function {
	/**
	 * 重载:实现功能。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		int seed = this.getParameterInt("seed", this.getUser().getSecurityCode());
		int level = this.getParameterInt("level", 0);
		if (level == 0) return seed;
		ParticipantTree pt = null;
		for (int i = 0; i < level; i++) {
			pt = ParticipantTreeProvider.getInstance().getParticipantTree(pt == null ? seed : pt.getSecurityCode());
		}
		return (pt == null ? -1 : pt.getSecurityCode());
	}

}

