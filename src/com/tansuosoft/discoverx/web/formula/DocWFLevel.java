/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import java.util.List;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.User;

/**
 * 获取当前用户在当前文档流程状态中所处状态值的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td colspan="2">不接收参数!</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>int</td>
 * <td>用户所处文档流程的状态值对应的int结果,如果没有状态值,则返回0。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DocWFLevel extends Function {
	/**
	 * 缺省构造器。
	 */
	public DocWFLevel() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = this.getDocument();
		if (doc == null) return 0;
		User user = this.getUser();
		if (user == null) return 0;
		Security security = doc.getSecurity();
		if (security == null) return 0;
		List<SecurityEntry> ses = security.getSecurityEntries();
		if (ses == null || ses.isEmpty()) return 0;
		for (SecurityEntry x : ses) {
			if (x != null && user.getSecurityCode() == x.getSecurityCode()) return x.getWorkflowLevel();
		}
		return 0;
	}

}

