/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 返回自增长序列号的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>key</td>
 * <td>字增长序号的关键字，每次调用指定一个关键字时才会正常自动增长，如果不提供，则系统使用同一个默认关键字。</td>
 * </tr>
 * <tr>
 * <td>set</td>
 * <td>是否为设置值，默认为false。</td>
 * </tr>
 * <tr>
 * <td>v</td>
 * <td>如果为设置值，则表示要设置的值（此时必须提供），否则表示初始值（初始值默认为0，即表示从1开始）。</td>
 * </tr>
 * <tr>
 * <td>preview</td>
 * <td>是否只取预览值，而不是取实际值，默认为false。预览值与实际值的区别在于，实际值会记录取到的值，这样下一次调用就又返回新值了。</td>
 * </tr>
 * <tr>
 * <td>length</td>
 * <td>序号总长度（默认为0，表示返回实际获取的结果而不补头尾）。如果提供了长度且获取的结果长度小于提供的长度是，不足的长度用prefix或suffix指定参数值补足。</td>
 * </tr>
 * <tr>
 * <td>step</td>
 * <td>(在默认每次加一后取回的值上)额外追加步长值，默认为0。</td>
 * </tr>
 * <tr>
 * <td>prefix</td>
 * <td>长度不足是前面填充的字符，默认为“0”，prefix和suffix只能指定一个。</td>
 * </tr>
 * <tr>
 * <td>suffix</td>
 * <td>长度不足时后面填充的字符，默认无填充。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>String</td>
 * <td>生成的序列号，出现异常则返回null。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 2, alias = "Sequence", name = "获取自增长序列号", desc = "获取自增长序列号的公式")
public class Sequence extends Function {
	private static final String DEFAULT_KEY = "formula_default_key";

	/**
	 * 重载:实现功能。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			String key = this.getParameterString("key", DEFAULT_KEY);
			boolean set = this.getParameterBoolean("set", false);
			boolean preview = this.getParameterBoolean("preview", false);
			long v = this.getParameterLong("v", 0L);
			long step = this.getParameterLong("step", 0L);
			long result = 0L;
			SequenceProvider sp = SequenceProvider.getInstance(key, v);
			if (set) {
				sp.setSequence(v);
				result = v;
			} else {
				result = (preview ? (sp.getPreviewSequence() + 1) : sp.getSequence()) + step;
			}
			int length = this.getParameterInt("length", 0);
			String prefix = this.getParameterString("prefix", null);
			String suffix = this.getParameterString("suffix", null);
			if (StringUtil.isBlank(prefix) && StringUtil.isBlank(suffix)) prefix = "0";
			StringBuilder sb = new StringBuilder();
			sb.append(result);
			int rawLen = sb.length();
			if (!StringUtil.isBlank(prefix)) {
				for (int i = 0; i < (length - rawLen); i++) {
					sb.insert(0, prefix);
				}
			} else if (!StringUtil.isBlank(suffix)) {
				for (int i = 0; i < (length - rawLen); i++) {
					sb.append(suffix);
				}
			}
			return sb.toString();
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		return null;
	}
}

