/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DAOConfig;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 返回与当前用户同一个部门的用户列表
 * <p>
 * 参数指定部门级数level:=1/2/3/4…默认为1
 * </p>
 * <p>
 * sql所需参数"dept"
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SameDeptUsers extends Function {

	@Override
	public Object call() throws FunctionException {
		User user = this.getUser();
		if (user == null) return null;
		String level = getParameterString("level", "1");
		String format = "ou" + level;
		String dept = ParticipantHelper.getFormatValue(user, format);// 获取指定级数的部门名称

		String cfgName = "DeptUserRequest";
		com.tansuosoft.discoverx.dao.DAOConfig dbConfig = DAOConfig.getInstance();
		String className = dbConfig.getValue(cfgName);
		DBRequest dbRequest = (DBRequest) Instance.newInstance(className);
		this.setParameter("dept", dept); // 传递参数dept
		if (dbRequest != null) dbRequest.sendRequest();
		return dbRequest.getResult();
	}

}

