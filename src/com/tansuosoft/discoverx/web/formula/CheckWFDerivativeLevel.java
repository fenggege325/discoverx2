/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import java.util.List;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataDerivativeLevel;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 检查当前用户在当前文档当前流程状态中是否指定派生级别类型的用户的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>level</td>
 * <td>{@link WFDataDerivativeLevel}枚举中的有效级别对应的数字，可选，默认为{@link WFDataDerivativeLevel#Sender}</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>boolean</td>
 * <td>返回true/false指示当前用户是否指定派生级别的流程权限类型。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class CheckWFDerivativeLevel extends Function {
	/**
	 * 缺省构造器。
	 */
	public CheckWFDerivativeLevel() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = this.getDocument();
		if (doc == null) return false;
		User user = this.getUser();
		if (user == null) return false;
		int securityCode = user.getSecurityCode();
		WorkflowRuntime wfr = null;
		try {
			wfr = WorkflowRuntime.getInstance(doc, this.getSession());
			if (wfr == null) return false;
			WFDataDerivativeLevel targetLevel = this.getParameterEnum("level", WFDataDerivativeLevel.class, WFDataDerivativeLevel.Sender);
			List<WFData> list = wfr.getContextWFData(WFDataLevel.Approver);
			if (list == null || list.isEmpty()) return false;
			for (WFData x : list) {
				if (x != null && x.getParentData() == securityCode && (x.checkSubLevel(WFDataSubLevel.Normal) || x.checkSubLevel(WFDataSubLevel.Equivalent)) && targetLevel == WFDataDerivativeLevel.Sender) return true;
				if (x != null && x.getParentData() == securityCode && x.checkSubLevel(WFDataSubLevel.Agent) && targetLevel == WFDataDerivativeLevel.Client) return true;
				if (x != null && x.getParentData() == securityCode && x.checkSubLevel(WFDataSubLevel.Forward) && targetLevel == WFDataDerivativeLevel.Forwarder) return true;
				if (x != null && x.getParentData() == securityCode && x.checkSubLevel(WFDataSubLevel.Dispatch) && targetLevel == WFDataDerivativeLevel.Dispatcher) return true;
			}
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		return false;
	}
}

