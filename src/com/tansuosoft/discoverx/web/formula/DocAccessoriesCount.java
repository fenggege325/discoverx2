/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import java.util.List;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Resource;

/**
 * 获取当前文档指定类型附加文件的个数。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>act</td>
 * <td>表示附加文件类型({@link AccessoryType})资源的UNID或别名，用于指示返回文档所包含的这一类型的附加文件的个数。可选，如果不提供，则返回文档包含的所有附加文件的个数。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>int</td>
 * <td>文档指定附加文件的个数。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DocAccessoriesCount extends Function {

	/**
	 * 缺省构造器。
	 */
	public DocAccessoriesCount() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = this.getDocument();
		if (doc == null) return 0;
		String cfgat = this.getParameterString("act", null);
		List<Accessory> list = doc.getAccessories();
		if (list == null || list.isEmpty()) return 0;
		if (cfgat == null || cfgat.length() == 0) return list.size();
		int result = 0;
		for (Accessory x : list) {
			if (x != null && cfgat.equalsIgnoreCase(x.getAccessoryType())) {
				result++;
			} else if (x != null) {
				Resource r = x.getAccessoryTypeResource();
				if (r != null && cfgat.equalsIgnoreCase(r.getAlias())) result++;
			}
		}
		return result;
	}

}

