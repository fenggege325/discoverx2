/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 返回当前用户会话所在的单位(根组织信息)的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>format</td>
 * <td>返回的格式，如id为单位UNID，sc为单位安全编码等，可参考{@link com.tansuosoft.discoverx.model.User#LEGALLY_NAME_PART_FORMATS}中的说明，不提供则默认为"id"。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>String</td>
 * <td>以文本值返回，如果找不到当前环节则返回空串。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tensosoft.com
 * 
 */
@FunctionAttributes(type = 2, alias = "ContextOrg", name = "返回当前用户会话所在的单位", desc = "返回当前用户会话所在的单位(根组织信息)的公式。")
public class ContextOrg extends Function {

	/**
	 * 重载：实现功能。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			ParticipantTree ptroot = ParticipantTreeProvider.getInstance().getRoot();
			if (ptroot == null) return "";
			String format = this.getParameterString("format", com.tansuosoft.discoverx.model.User.LEGALLY_NAME_PART_FORMATS[7]);
			return ParticipantHelper.getFormatValue(ptroot, format);
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		return "";
	}
}

