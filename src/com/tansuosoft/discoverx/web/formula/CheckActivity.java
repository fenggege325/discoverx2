/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import java.util.List;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 检查当前文档是否已经经历过指定的流程环节的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>check</td>
 * <td>待检查的环节信息，多值用半角分号分隔，每个环节信息可以是环节名称、环节别名、环节unid等；如果指定了多个环节信息则必须所有指定的环节都经历过才能返回true。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>boolean</td>
 * <td>返回true/false。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tensosoft.cn
 */
@FunctionAttributes(type = 2, alias = "checkActivity", name = "检查流程历史环节", desc = "检查文档是否经历了指定环节。")
public class CheckActivity extends Function {
	/**
	 * 缺省构造器。
	 */
	public CheckActivity() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = this.getDocument();
		if (doc == null) return false;

		WorkflowRuntime wfr = null;
		try {
			String pv = this.getParameterString("check", null);
			if (pv == null || pv.length() == 0) return false;
			pv = pv.replace(',', ';');
			String pvs[] = StringUtil.splitString(pv, ';');
			wfr = WorkflowRuntime.getInstance(doc, this.getSession());
			List<Activity> acts = wfr.getTraversalActivities();
			if (acts == null || acts.isEmpty()) return false;
			for (String x : pvs) {
				boolean ret = false;
				for (Activity y : acts) {
					if (y != null && (x.equalsIgnoreCase(y.getName()) || x.equalsIgnoreCase(y.getAlias()) || x.equalsIgnoreCase(y.getUNID()))) {
						ret = true;
						break;
					}
				}
				if (!ret) return false;
			}
			return true;
		} finally {
			if (wfr != null) wfr.shutdown();
		}
	}
}

