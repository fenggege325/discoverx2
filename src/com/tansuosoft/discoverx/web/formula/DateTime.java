/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.common.exception.FunctionException;

/**
 * 按格式返回当前日期时间。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>format</td>
 * <td>指定返回的日期时间格式，默认为“yyyy-MM-dd HH:mm:ss”。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>String</td>
 * <td>日期时间结果</td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * 示例：<br/>
 * <list>
 * <li>@DateTime()或@DateTime(format:="yyyy-MM-dd HH:mm:ss")：返回当时的详细日期时间，如“2009-10-10 15:58:20”。</li>
 * <li>@DateTime(format:="yyyy-MM-dd")：返回当天日期，如“2009-10-10”。</li>
 * <li>@DateTime(format:="hh:mm:ss")：返回当前时间，如“13:00:00”。</li>
 * <li>@DateTime(format:="yyyy")：返回当前年份，如“2009”。</li>
 * <li>@DateTime(format:="MM")：返回当前月份，如“10”。</li>
 * </list>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DateTime extends Function {
	/**
	 * 缺省构造器。
	 */
	public DateTime() {
		super();
	}

	/**
	 * 重载call：返回指定格式的日期时间。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		String format = this.getParameterString("format", null);
		com.tansuosoft.discoverx.util.DateTime dt = new com.tansuosoft.discoverx.util.DateTime();
		if (format == null || format.length() == 0) {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		return dt.toString(format);
	}

}

