/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.ParticipantHelper;

/**
 * 按指定格式返回当前用户信息的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>format</td>
 * <td>指定用户名的返回格式或返回用户名的哪一部分，可参考“{@link com.tansuosoft.discoverx.model.User#LEGALLY_NAME_PART_FORMATS}”中的说明以提供有效的参数值。<br/>
 * 如果format没有提供，则返回用户全名（即用户的完整层次名）</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>String</td>
 * <td>format指定的用户信息结果</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @see com.tansuosoft.discoverx.model.User#LEGALLY_NAME_PART_FORMATS
 * @author coca@tansuosoft.cn
 */
public class User extends Function {
	/**
	 * 缺省构造器。
	 */
	public User() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		String format = getParameterString("format", null);
		com.tansuosoft.discoverx.model.User user = this.getUser();
		if (user == null) return null;
		String name = user.getName();
		if (format == null || format.length() == 0) { return name; }
		return ParticipantHelper.getFormatValue(user, format);
	}

}

