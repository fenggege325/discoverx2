/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 检查当前用户是否具有指定角色的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>roles</td>
 * <td>表示角色编码，多个请用逗号分隔。</td>
 * </tr>
 * <tr>
 * <td>mc</td>
 * <td>表示组合方式（MultipleCombination），如果“<strong>roles</strong>”中指定了多个角色，那么此参数指示这些角色是同时具有还是具有其中一个就可以，默认（不提供）或值为“and”则表示必须同时具有这些角色才返回true，值为“or”表示只要具有其中一个角色则返回true。</td>
 * </tr>
 * <tr>
 * <td>author</td>
 * <td>表示获取结果时的基准用户是否为当前文档作者，如果(默认)不提供或值为false则表示为当前登录用户，如果为true则表示当前文档发起人。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>boolean</td>
 * <td>是否具有指定角色</td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * 如果具有参数指定的角色，则返回true，否则返回false。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class CheckRole extends Function {

	/**
	 * 缺省构造器。
	 */
	public CheckRole() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		boolean author = getParameterBoolean("author", false);
		User u = null;
		if (!author) {
			u = this.getUser();
		} else {
			Document doc = this.getDocument();
			u = (doc == null ? null : ResourceHelper.getAuthor(doc));
		}
		if (u == null) {
			FileLogger.debug("找不到目标用户！");
			return false;
		}
		String cfgRoles = this.getParameterString("roles", null);
		if (cfgRoles == null || cfgRoles.length() == 0) return false;
		List<String> roles = new ArrayList<String>();
		int currentIndex = 0;
		int nextSeparator;
		while ((nextSeparator = cfgRoles.indexOf(',', currentIndex)) != -1) {
			roles.add(cfgRoles.substring(currentIndex, nextSeparator));
			currentIndex = nextSeparator + 1;
		}
		roles.add(cfgRoles.substring(currentIndex));
		if (roles.isEmpty()) return false;
		boolean result = false;
		if (roles.size() > 1) {
			String cfgMC = this.getParameterString("mc", null);
			boolean isAnd = true;
			if (cfgMC == null || cfgMC.length() == 0 || cfgMC.trim().equalsIgnoreCase("and")) {
				isAnd = true;
			} else if (cfgMC != null && cfgMC.equalsIgnoreCase("or")) {
				isAnd = false;
			}
			boolean singleResult = false;
			result = (isAnd ? true : false);
			for (String x : roles) {
				singleResult = SecurityHelper.checkUserRole(u, StringUtil.getValueInt(x, 0));
				if (!isAnd && singleResult) {
					result = true;
					break;
				} else {
					result = (result && singleResult);
				}
			}
		} else {
			result = SecurityHelper.checkUserRole(u, StringUtil.getValueInt(roles.get(0), 0));
		}
		return result;
	}
}

