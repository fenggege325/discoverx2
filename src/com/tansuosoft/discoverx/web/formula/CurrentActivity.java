/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 返回当前文档当前环节信息的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>p</td>
 * <td>可选，表示要返回的环节信息对应的属性代码(如果不提供则返回环节名称)，可用的环节属性代码如下：<br/>
 * <ul>
 * <li>0：环节名称；</li>
 * <li>1：环节（排）序号（返回其数字对应的文本）；</li>
 * <li>2：环节别名；</li>
 * <li>3：环节说明；</li>
 * <li>4：环节类型（返回{@link com.tansuosoft.discoverx.workflow.ActivityType}枚举的字符串表现形式）；</li>
 * <li>5：环节审批方式（返回其数字对应的文本）。</li>
 * </ul>
 * </td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>String</td>
 * <td>文档当前环节信息，如果找不到当前环节则返回空串。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class CurrentActivity extends Function {
	static final String EMPTY = "";

	/**
	 * 重载。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		WorkflowRuntime wfr = null;
		try {
			Document doc = this.getDocument();
			if (doc == null) return EMPTY;
			wfr = WorkflowRuntime.getInstance(doc, this.getSession());
			Activity act = wfr.getCurrentActivity();
			if (act == null) return EMPTY;
			int p = this.getParameterInt("p", 0);
			switch (p) {
			case 1:
				return act.getSort() + "";
			case 2:
				return act.getAlias();
			case 3:
				return act.getDescription();
			case 4:
				return act.getActivityType().toString();
			case 5:
				return act.getParticipation() + "";
			case 0:
			default:
				return act.getName();
			}
		} catch (Exception ex) {
			FileLogger.error(ex);
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		return EMPTY;
	}

}

