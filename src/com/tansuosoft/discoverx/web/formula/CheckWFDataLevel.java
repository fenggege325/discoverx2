/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.workflow.WFDataLevel;

/**
 * 检查当前用户在当前文档当前流程状态中是否指定流程状态类型的用户的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>level</td>
 * <td>{@link WFDataLevel}枚举中的有效级别对应的数字，可选，默认为{@link WFDataLevel#Approver}</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>boolean</td>
 * <td>返回true/false指示当前用户是否指定流程状态类型的用户。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author Simon@tensosoft.cn
 */
public class CheckWFDataLevel extends Function {
	/**
	 * 缺省构造器。
	 */
	public CheckWFDataLevel() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = this.getDocument();
		if (doc == null) return false;
		Security s = doc.getSecurity();
		if (s == null) return false;
		User user = this.getUser();
		if (user == null) return false;
		int securityCode = user.getSecurityCode();
		WFDataLevel level = this.getParameterEnum("level", WFDataLevel.class, WFDataLevel.Approver);
		if (s.checkWorkflowLevel(securityCode, level.getIntValue())) return true;
		return false;
	}
}

