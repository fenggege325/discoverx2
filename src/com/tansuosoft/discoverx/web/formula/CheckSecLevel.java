/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.formula;

import java.util.List;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityLevel;

/**
 * 检查当前用户对当前文档是否具有指定级别操作权限的公式。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>level</td>
 * <td>{@link com.tansuosoft.discoverx.model.SecurityLevel}枚举中的定义的值对应的数字，如果不提供，则默认为{@link com.tansuosoft.discoverx.model.SecurityLevel#Author}，表示检查是否作者。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>boolean</td>
 * <td>返回true/false。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author Simon@tensosoft.cn
 */
public class CheckSecLevel extends Function {
	/**
	 * 缺省构造器。
	 */
	public CheckSecLevel() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = this.getDocument();
		if (doc == null) return false;
		SecurityLevel level = this.getParameterEnum("level", SecurityLevel.class, SecurityLevel.Author);
		Security s = doc.getSecurity();
		if (s == null) return false;
		List<Integer> scs = SecurityHelper.getUserSecurityCodes(getUser());
		if (scs == null || scs.isEmpty()) return false;
		for (int x : scs) {
			if (s.checkSecurityLevel(x, level)) return true;
		}
		return false;
	}
}

