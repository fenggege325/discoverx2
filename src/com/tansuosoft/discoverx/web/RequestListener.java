/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import javax.servlet.ServletRequestEvent;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.util.HttpContext;

/**
 * Http请求相关事件处理程序实现类。
 * 
 * @author coca@tensosoft.com
 */
public class RequestListener implements javax.servlet.ServletRequestListener {
	/**
	 * 缺省构造器。
	 */
	public RequestListener() {
	}

	/**
	 * 重载：清理{@link HttpContext}和{@link SessionContext}中保存的线程本地变量。
	 * 
	 * @see javax.servlet.ServletRequestListener#requestDestroyed(javax.servlet.ServletRequestEvent)
	 */
	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		SessionContext.reset();
		HttpContext.reset();
	}

	/**
	 * 重载：无操作
	 * 
	 * @see javax.servlet.ServletRequestListener#requestInitialized(javax.servlet.ServletRequestEvent)
	 */
	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		// NOOP
	}
}

