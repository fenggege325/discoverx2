/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import javax.servlet.ServletRequestEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.util.HttpContext;

/**
 * {@link javax.servlet.ServletRequest}初始化和终止事件处理程序实现类。
 * 
 * @deprecated
 * @author coca@tensosoft.com
 */
@Deprecated
public class ServletRequestListener implements javax.servlet.ServletRequestListener, HttpSessionListener {
	private RequestListener m_delegate = null;
	private SessionListener m_sldelegate = null;

	/**
	 * 缺省构造器。
	 */
	public ServletRequestListener() {
		m_delegate = new RequestListener();
		m_sldelegate = new SessionListener();
	}

	/**
	 * 重载：清理{@link HttpContext}和{@link SessionContext}中保存的线程本地变量。
	 * 
	 * @see javax.servlet.ServletRequestListener#requestDestroyed(javax.servlet.ServletRequestEvent)
	 */
	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		m_delegate.requestDestroyed(sre);
	}

	/**
	 * 重载：无操作
	 * 
	 * @see javax.servlet.ServletRequestListener#requestInitialized(javax.servlet.ServletRequestEvent)
	 */
	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		m_delegate.requestInitialized(sre);
	}

	/**
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		m_sldelegate.sessionCreated(se);
	}

	/**
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		m_sldelegate.sessionDestroyed(se);
	}

}
