/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.bll.ApplicationProfile;
import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.bll.view.ViewHelper;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.MenuTarget;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.ReferenceComparatorDefault;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.json.ResourceJsonObjectRender;

/**
 * 输出应用程序类型的菜单项大纲客户端呈现内容的{@link MenuOutlineRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ApplicationMenuOutlineRender extends MenuOutlineRender {
	/**
	 * 应用程序大纲相关的系统内置应用程序额外参数名，此参数名对应的参数值表示在应用程序内置视图之前输出的TreeView内容提供类完整类名或json内容本身。
	 * 
	 * <p>
	 * 配置的全限定类名应该是父类为{@link MenuOutlineRender}的具体实现类的类名。<br/>
	 * 实现类通过{@link MenuOutlineRender#renderSpecial(JSPContext)}实现输出树（TreeView）格式的Json内容。<br/>
	 * 输出格式形如：{id: ?,label:?,data:{menuTarget:?,target:?,directory:?...}...}或<br/>
	 * {id: ?,label:?,data:{menuTarget:?,target:?,directory:?...}...},{id: ?,label:?,data:{menuTarget:?,target:?,directory:?...},children:[...]...}等<br/>
	 * 也可以直接配置为类似输出格式的树(ListView)的json内容作为子节点树。<br/>
	 * 请注意输出结果合并时的总体格式以确保输出的内容符合ListView的json规则。其它应用程序大纲相关的系统内置应用程序额外参数对应的参数值均顺应此处定义的规则。
	 * </p>
	 */
	protected static final String PREVIEWMENUOUTLINERENDER_PARAMNAME = "previewmenuoutlinerender";
	/**
	 * 应用程序大纲相关的系统内置应用程序额外参数名，此参数名对应的参数值表示在应用程序内置视图之后输出的TreeView内容提供类完整类名或json内容本身。
	 * 
	 * @see ApplicationMenuOutlineRender#PREVIEWMENUOUTLINERENDER_PARAMNAME
	 */
	protected static final String POSTVIEWMENUOUTLINERENDER_PARAMNAME = "postviewmenuoutlinerender";
	/**
	 * 应用程序大纲相关的系统内置应用程序额外参数名，此参数名对应的参数值表示在应用程序子应用程序之后输出的TreeView内容提供类完整类名或json内容本身。
	 * 
	 * @see ApplicationMenuOutlineRender#PREVIEWMENUOUTLINERENDER_PARAMNAME
	 */
	protected static final String POSTSUBAPPMENUOUTLINERENDER_PARAMNAME = "postsubappmenuoutlinerender";
	/**
	 * 应用程序大纲相关的系统内置应用程序额外参数名，此参数名对应的参数值表示在应用程序包含的下级菜单之后输出的TreeView内容提供类完整类名或json内容本身。
	 * 
	 * @see ApplicationMenuOutlineRender#PREVIEWMENUOUTLINERENDER_PARAMNAME
	 */
	protected static final String POSTSUBMENUOUTLINERENDER_PARAMNAME = "postsubmenuoutlinerender";

	/**
	 * 接收必要参数的构造器。
	 * 
	 * <p>
	 * 参考{@link MenuOutlineRender#MenuOutlineRender(Menu, MenuForm, int)}
	 * </p>
	 * 
	 * @param targetMenu
	 * @param menuForm
	 * @param index
	 */
	public ApplicationMenuOutlineRender(Menu targetMenu, MenuForm menuForm, int index) {
		super(targetMenu, menuForm, index);
	}

	/**
	 * 重载：输出应用程序类型的菜单项大纲客户端呈现内容。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.menu.MenuOutlineRender#renderSpecial(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	protected String renderSpecial(JSPContext jspContext) {
		if (!MenuRender.isApplication(m_menu)) return "";
		return renderTreeView(m_menu, jspContext);
	}

	/**
	 * 重载。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.menu.MenuOutlineRender#renderTreeView(com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	protected String renderTreeView(Resource r, JSPContext jspContext) {
		User u = jspContext.getLoginUser();
		if (r == null || !MenuRender.checkUsage(u, r)) return "";
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append(new ResourceJsonObjectRender(r).render(jspContext));
		sb.append(",").append("data:").append("{").append(buildData(r, jspContext)).append("}");

		Application app = null;
		if (r instanceof Menu) {
			app = ResourceContext.getInstance().getResource(((Menu) r).getTarget(), Application.class);
			if (app == null) {
				String appunid = ResourceAliasContext.getInstance().getUNIDByAlias(m_menu.getTargetDirectory(), m_menu.getTarget());
				app = ResourceContext.getInstance().getResource(appunid, Application.class);
			}
		} else if (r instanceof Application) {
			app = (Application) r;
		}

		if (app == null || !MenuRender.checkUsage(u, app)) return "";
		sb.append(",").append("children:").append("[\r\n"); // children begin

		boolean hasChildren = false;

		// 从应用的额外参数中获取可能存在的视图大纲之前输出的内容
		String extraOutline = getAppExtraOutline(app, PREVIEWMENUOUTLINERENDER_PARAMNAME, jspContext);
		if (extraOutline != null && extraOutline.length() > 0) {
			sb.append(extraOutline);
			hasChildren = true;
		}

		// 包含的下级视图
		List<Reference> list = app.getViews();
		int viewIdx = 0;
		if (list != null && list.size() > 0) {
			Collections.sort(list, new ReferenceComparatorDefault());
			View view = null;
			for (Reference ref : list) {
				if (ref == null || ref.getUnid() == null) continue;
				view = ResourceContext.getInstance().getResource(ref.getUnid(), View.class);
				if (view == null || !view.getApplicationOutline() || !MenuRender.checkUsage(u, view)) continue;
				sb.append(viewIdx == 0 ? (hasChildren ? "\r\n," : "") : ",\r\n");
				sb.append("{");
				sb.append(new ResourceJsonObjectRender(view).render(jspContext));
				sb.append(",").append("data:{");
				sb.append(buildData(view, jspContext));
				sb.append(",").append("builtin:true/*是否应用内置视图*/");
				sb.append("}");
				if (ViewHelper.hasCategoryColumn(view)) {
					sb.append(",").append(buildAjaxWaitChild());
				}
				sb.append("}\r\n");
				viewIdx++;
				if (!hasChildren) hasChildren = true;
			}
		}

		// 从应用的额外参数中获取可能存在的视图大纲之后输出的内容
		extraOutline = getAppExtraOutline(app, POSTVIEWMENUOUTLINERENDER_PARAMNAME, jspContext);
		if (extraOutline != null && extraOutline.length() > 0) {
			sb.append(extraOutline);
			if (!hasChildren) hasChildren = true;
		}

		// 包含的下级应用程序
		list = app.getSubApplications();
		int appIdx = 0;
		if (list != null && list.size() > 0) {
			Collections.sort(list, new ReferenceComparatorDefault());
			Application sub = null;
			for (Reference ref : list) {
				if (ref == null || ref.getUnid() == null) continue;
				sub = ResourceContext.getInstance().getResource(ref.getUnid(), Application.class);
				if (sub == null || !sub.hasOutlineView()) continue;
				sb.append(appIdx == 0 ? (!hasChildren ? "" : ",") : ",");
				sb.append(renderTreeView(sub, jspContext));
				appIdx++;
				if (!hasChildren) hasChildren = true;
			}
		}

		// 从应用的额外参数中获取可能存在的子模块大纲之后输出的内容
		extraOutline = getAppExtraOutline(app, POSTSUBAPPMENUOUTLINERENDER_PARAMNAME, jspContext);
		if (extraOutline != null && extraOutline.length() > 0) {
			sb.append(extraOutline);
			if (!hasChildren) hasChildren = true;
		}

		// 处理应用程序可能包含的下级菜单条目
		if (r instanceof Menu) {
			List<Resource> children = r.getChildren();
			if (children != null && children.size() > 0) {
				int idx = 0;
				MenuOutlineRender menuOutlineRender = null;
				for (Resource x : children) {
					if (x == null || !MenuRender.checkUsage(u, x)) continue;
					if ((x instanceof Menu)) { // 在输出目标资源树时，只有菜单项资源本身需要被重新处理
						menuOutlineRender = MenuRender.getMenuOutlineRender((Menu) x, this.m_menuForm, -1);
						if (menuOutlineRender != null) sb.append(idx == 0 ? (hasChildren ? ",\r\n" : "") : ",").append(menuOutlineRender.render(jspContext));
					} else {// 其它资源直接递归输出资源树。
						sb.append(idx == 0 ? (hasChildren ? ",\r\n" : "") : ",").append(new DefaultMenuOutlineRender((Menu) r, this.m_menuForm, -1).renderTreeView(x, jspContext));
					}
					idx++;
					if (!hasChildren) hasChildren = true;
				}
			}
		}

		// 从应用的额外参数中获取可能存在的子模块大纲之后输出的内容
		extraOutline = getAppExtraOutline(app, POSTSUBMENUOUTLINERENDER_PARAMNAME, jspContext);
		if (extraOutline != null && extraOutline.length() > 0) {
			sb.append(extraOutline);
			if (!hasChildren) hasChildren = true;
		}

		// 配置文件
		Document profile = ApplicationProfile.getInstance(app).getProfile();
		boolean outputProfileItemFlag = (profile != null && SecurityHelper.authorize(u, profile, SecurityLevel.Modify));
		if (outputProfileItemFlag) {
			sb.append(hasChildren ? ",\r\n" : "").append("{");
			sb.append("id:").append("'").append(profile.getUNID()).append("'");
			sb.append(",").append("label:").append("'").append(app.getName()).append("配置").append("'");
			sb.append(",").append("desc:").append("'").append("查看或编辑“").append(app.getName()).append("”的配置文件。").append("'");
			sb.append(",").append("data:{");
			sb.append("target:").append("'profile.jsp?unid=").append(profile.getUNID()).append("'");
			sb.append(",").append("menuTarget:").append(MenuTarget.Href);
			sb.append(",").append("targetDirectory:").append("'_right'");
			sb.append("}");
			sb.append("}");
		}

		sb.append("]"); // children end
		sb.append("}\r\n");
		return sb.toString();
	}// func end

	/**
	 * 从app中配置的相关额外参数中获取额外大纲项输出结果。
	 * 
	 * @param app
	 * @param paramName
	 * @param jspContext
	 * @return String 如果没有配置结果则返回空字符串。
	 */
	protected String getAppExtraOutline(Application app, String paramName, JSPContext jspContext) {
		String v = app.getParamValueString(paramName, null);
		if (v == null || v.length() == 0) return "";
		if (v.indexOf('{') >= 0 || v.indexOf(',') >= 0) return v;
		MenuOutlineRender render = Instance.newInstance(v, MenuOutlineRender.class);
		if (render != null) {
			String renderResult = render.renderSpecial(jspContext);
			if (renderResult != null && renderResult.length() > 0) return renderResult;
		}
		return "";
	}
}
