/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.MenuTarget;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出一级菜单（{@link Menu}）包含下级大纲作为一整个treeview的客户端呈现内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class MenuRenderSingleTree implements HtmlRender {
	/**
	 * 缺省构造器。
	 */
	public MenuRenderSingleTree() {
	}

	private Menu m_menuItemLv1 = null;
	private MenuForm mf = null;

	/**
	 * 重载render：输出一级菜单（{@link Menu}）包含下级大纲客户端呈现内容。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		mf = new MenuForm();
		mf.fillWebRequestForm((HttpServletRequest) jspContext.getRequest());
		Menu root = mf.getRoot();
		if (root == null) return "";
		m_menuItemLv1 = mf.getLevelMenu(1);
		if (m_menuItemLv1 == null) return "";
		mf.popLevel();
		StringBuilder sb = new StringBuilder();
		MenuOutlineRender menuOutlineRender = getMenuOutlineRender(m_menuItemLv1, mf, 0);
		sb.append(menuOutlineRender.render(jspContext));
		return sb.toString();
	}

	/**
	 * 根据menu指定的菜单资源获取其{@link MenuOutlineRender}对象。
	 * 
	 * @param menu {@link Menu}
	 * @param mf {@link MenuForm}对象。
	 * @param idx menu在其上级包含的所有下级菜单列表中的序号
	 * @return
	 */
	public static MenuOutlineRender getMenuOutlineRender(Menu menu, MenuForm mf, int idx) {
		MenuOutlineRender result = null;
		if (MenuRenderSingleTree.isApplication(menu)) {
			result = new ApplicationMenuOutlineRender(menu, mf, idx);
		} else if (menu != null && menu.getMenuTarget() == MenuTarget.TreeViewProvider) {
			result = Instance.newInstance(menu.getTarget(), MenuOutlineRender.class);
			if (result != null) result.init(menu, mf, idx);
		} else {
			result = new DefaultMenuOutlineRender(menu, mf, idx);
		}
		return result;
	}

	/**
	 * 返回菜单项的数据源类型是否为有效的资源类型。
	 * 
	 * @param menu
	 * @return
	 */
	public static boolean isResource(Menu menu) {
		if (menu == null) return false;
		return (menu.getMenuTarget() == MenuTarget.Resource && !StringUtil.isBlank(menu.getTarget()));
	}

	/**
	 * 返回菜单项的数据源类型是否为有效的应用程序类型的资源。
	 * 
	 * @param menu
	 * @return
	 */
	public static boolean isApplication(Menu menu) {
		if (menu == null) return false;
		return (menu.getMenuTarget() == MenuTarget.Resource && !StringUtil.isBlank(menu.getTarget()) && (StringUtil.isBlank(menu.getTargetDirectory()) || "application".equalsIgnoreCase(menu.getTargetDirectory())));
	}

	/**
	 * 返回菜单项的数据源类型是否为视图类型的资源。
	 * 
	 * @param menu
	 * @return
	 */
	public static boolean isView(Menu menu) {
		if (menu == null) return false;
		return (menu.getMenuTarget() == MenuTarget.Resource && !StringUtil.isBlank(menu.getTarget()) && "view".equalsIgnoreCase(menu.getTargetDirectory()));
	}

}

