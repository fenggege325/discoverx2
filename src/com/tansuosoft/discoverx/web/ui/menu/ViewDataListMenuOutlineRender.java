/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.view.ViewEntry;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.bll.view.ViewQueryProvider;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.json.ResourceJsonObjectRender;

/**
 * 输出视图数据菜单项大纲客户端呈现内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewDataListMenuOutlineRender extends MenuOutlineRender {
	public ViewDataListMenuOutlineRender() {
		super();
	}

	/**
	 * 接收必要参数的构造器。
	 * 
	 * <p>
	 * 参考{@link MenuOutlineRender#MenuOutlineRender(Menu, MenuForm, int)}
	 * </p>
	 * 
	 * @param targetMenu {@link Menu}，从其名为“view”的额外参数中获取视图unid/别名，此视图用于提取数据列表。
	 * @param menuForm
	 * @param index
	 */
	public ViewDataListMenuOutlineRender(Menu targetMenu, MenuForm menuForm, int index) {
		super(targetMenu, menuForm, index);
	}

	/**
	 * 重载：输出菜单项大纲客户端呈现内容。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.menu.MenuOutlineRender#renderSpecial(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	protected String renderSpecial(JSPContext jspContext) {
		if (m_menu == null) return "";
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append(new ResourceJsonObjectRender(m_menu).render(jspContext));
		sb.append(",").append("data:").append("{").append(buildData(m_menu, jspContext)).append("}");

		sb.append(",").append("children:").append("[");

		String viewid = m_menu.getParamValueString("view", null);
		String viewdata = null;
		if (viewid != null && viewid.length() > 0) {
			View view = ResourceContext.getInstance().getResource(viewid, View.class);
			if (view == null) {
				String viewunid = ResourceAliasContext.getInstance().getUNIDByAlias(View.class, viewid);
				view = ResourceContext.getInstance().getResource(viewunid, View.class);
			}
			if (view != null) {
				ViewQuery vq = ViewQueryProvider.getViewQuery(view, jspContext.getUserSession());
				if (vq != null) {
					ViewEntryCallback vec = new ViewEntryCallback() {
						private StringBuilder sb = new StringBuilder();

						@Override
						public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
							if (sb.length() > 0) sb.append(",");
							sb.append("{");
							sb.append("id:").append("'").append(viewEntry.getUnid()).append("'");
							List<String> list = viewEntry.getValues();
							String v = ((list == null || list.isEmpty()) ? "无标题" : list.get(0));
							sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(v)).append("'");
							sb.append(",").append("data:").append("{");
							sb.append("viewData:true");
							View view = query.getViewParser().getView();
							sb.append(",").append("viewType:").append(view.getViewType());
							sb.append(",").append("table:").append("'").append(view.getTable()).append("'");
							if (list != null && list.size() > 1) {
								sb.append(",").append("values:").append("{");
								int idx = 0;
								for (int i = 1; i < list.size(); i++) {
									String s = list.get(i);
									if (s == null) s = "";
									sb.append(idx == 0 ? "" : ",").append("'").append(StringUtil.encode4Json(s)).append("'");
									idx++;
								}
								sb.append("}"); // values end
							}
							sb.append("}"); // data end
							sb.append("}"); // tv end
						}

						@Override
						public Object getResult() {
							return sb.toString();
						}
					};
					vq.setPagination(null);
					vq.addViewEntryCallback(vec);
					vq.getViewEntries();
					viewdata = (String) vec.getResult();
				}
			}
		}
		if (viewdata != null) sb.append(viewdata);
		sb.append("]");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * 重载：总是返回空字符串。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.menu.MenuOutlineRender#renderTreeView(com.tansuosoft.discoverx.model.Resource,
	 *      com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	protected String renderTreeView(Resource r, JSPContext jspContext) {
		return "";
	}
}

