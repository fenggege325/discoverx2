/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.function.CommonForm;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 控制菜单大纲显示的{@link CommonForm}实现类。
 * 
 * <p>
 * 参数说明：<br/>
 * <ul>
 * <li>menu:根{@link Menu}对象的UNID。</li>
 * <li>menupath:选中的根{@link Menu}对象包含的下级{@link Menu}对象的索引路径，比如“0”表示根下的第一个下级，“1”表示第二个下级，而“0\1”表示根下第一个下级的第二个下级，可以支持多层。</li>
 * <li>apppath:选中应用程序的下级应用程序的索引路径，在path的末端{@link Menu}对象指向应用程序（{@link Application1}）时，表示应用程序包含的下级应用程序的索引路径。</li>
 * </ul>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class MenuForm extends CommonForm {
	/**
	 * 缺省构造器。
	 */
	public MenuForm() {
	}

	private List<String> m_menus = null; // 菜单资源UNID
	private List<Menu> m_menuList = null; // 菜单资源
	private String m_apppath = null; // 选中应用程序的下级应用程序的索引路径

	/**
	 * 返回菜单大纲包含的根菜单({@link Menu})。
	 * 
	 * @return String
	 */
	public Menu getRoot() {
		return getLevelMenu(0);
	}

	/**
	 * 返回菜单路径的最大层数。
	 * 
	 * <p>
	 * 包括根节点。
	 * </p>
	 * 
	 * @return
	 */
	public int getMaxLevel() {
		return (this.m_menus == null ? 0 : this.m_menus.size());
	}

	/**
	 * 返回菜单路径中指定层对应的{@link Menu}对象的UNID结果。
	 * 
	 * @param level int，从0开始到最大层数减一(0-({@link MenuForm#getMaxLevel()})-1)。
	 * @return
	 */
	public String getLevelUNID(int level) {
		if (this.m_menus == null || this.m_menus.isEmpty() || level >= this.m_menus.size() || level < 0) return null;
		return this.m_menus.get(level);
	}

	/**
	 * 返回菜单路径中指定层对应的{@link Menu}对象。
	 * 
	 * @param level int，从0开始到最大层数减一(0-({@link MenuForm#getMaxLevel()})-1)。
	 * @return
	 */
	public Menu getLevelMenu(int level) {
		if (this.m_menuList == null || this.m_menuList.isEmpty() || level >= this.m_menuList.size() || level < 0) return null;
		return this.m_menuList.get(level);
	}

	/**
	 * 获取选中应用程序的下级应用程序或视图的索引路径。
	 * 
	 * <p>
	 * 从名为“apppath”的http请求参数中获取。
	 * </p>
	 * 
	 * @return
	 */
	public String getAppPath() {
		return this.m_apppath;
	}

	/**
	 * 将层次从尾部减少一层。
	 * 
	 * @return 返回被减去的菜单UNID和对应菜单资源。
	 */
	public Map<String, Menu> popLevel() {
		if (m_menuList == null || m_menus == null || m_menus.isEmpty() || m_menus.size() != m_menuList.size()) return null;
		Menu m = m_menuList.remove(m_menuList.size() - 1);
		String unid = m_menus.remove(m_menus.size() - 1);
		Map<String, Menu> map = new HashMap<String, Menu>(1);
		map.put(unid, m);
		return map;
	}

	/**
	 * 用http请求参数填充相关信息。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.CommonForm#fillWebRequestForm(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public CommonForm fillWebRequestForm(HttpServletRequest request) {
		if (request == null) return null;
		String rootMenuUNID = request.getParameter("menu");
		String path = request.getParameter("menupath");
		if (path != null && path.length() > 0 && (rootMenuUNID == null || rootMenuUNID.length() == 0)) {
			Session session = SessionContext.getSession(request);
			Portal portal = Profile.getInstance(session).getPortal();
			if (portal == null) portal = ResourceContext.getInstance().getDefaultResource(Portal.class);
			rootMenuUNID = (portal == null ? null : portal.getMenu());
			if (rootMenuUNID != null && rootMenuUNID.startsWith("menu")) rootMenuUNID = ResourceAliasContext.getInstance().getUNIDByAlias(Menu.class, rootMenuUNID);
		}
		Menu root = ResourceContext.getInstance().getResource(rootMenuUNID, Menu.class);
		if (root != null && path != null && path.length() > 0) {
			if (m_menuList == null) m_menuList = new ArrayList<Menu>();
			m_menuList.add(root);

			if (m_menus == null) m_menus = new ArrayList<String>();
			m_menus.add(rootMenuUNID);

			List<Resource> list = root.getChildren();
			if (path.indexOf('\\') < 0) path += "\\0"; // 默认展开显示二级菜单下的第一个菜单项内容
			if (list != null && list.size() > 0 && path != null && path.length() > 0) {
				String[] pathes = StringUtil.splitString(path, '\\');
				int idx = 0;
				Resource r = null;
				for (String p : pathes) {
					idx = StringUtil.getValueInt(p, -1);
					if (idx < 0 || (list != null && idx >= list.size())) continue;
					r = (list == null ? null : list.get(idx));
					if (r == null || !(r instanceof Menu)) {
						r = new Menu();
						r.setName("[没有包含有效下级内容]");
					}
					if (m_menus == null) m_menus = new ArrayList<String>();
					m_menus.add(r.getUNID());

					if (m_menuList == null) m_menuList = new ArrayList<Menu>();
					m_menuList.add((Menu) r);

					list = r.getChildren();
				}// for end
			}// if end
		}// if (root !=null) end

		m_apppath = request.getParameter("apppath");

		return this;
	}
}

