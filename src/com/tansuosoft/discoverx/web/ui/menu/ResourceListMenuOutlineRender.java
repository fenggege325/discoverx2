/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import java.util.List;

import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.json.ResourceJsonObjectRender;

/**
 * 输出资源列表菜单项大纲客户端呈现内容的{@link HtmlRender}实现类。
 * 
 * <p>
 * 只能获取保存于xml的资源的列表。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceListMenuOutlineRender extends MenuOutlineRender {
	public ResourceListMenuOutlineRender() {
		super();
	}

	/**
	 * 接收必要参数的构造器。
	 * 
	 * <p>
	 * 参考{@link MenuOutlineRender#MenuOutlineRender(Menu, MenuForm, int)}
	 * </p>
	 * 
	 * @param targetMenu {@link Menu}，从其名为“directory”的额外参数中获取资源目录名，此目录名用于提取所有属于此目录的资源列表。
	 * @param menuForm
	 * @param index
	 */
	public ResourceListMenuOutlineRender(Menu targetMenu, MenuForm menuForm, int index) {
		super(targetMenu, menuForm, index);
	}

	/**
	 * 重载：输出菜单项大纲客户端呈现内容。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.menu.MenuOutlineRender#renderSpecial(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	protected String renderSpecial(JSPContext jspContext) {
		if (m_menu == null) return "";
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append(new ResourceJsonObjectRender(m_menu).render(jspContext));
		sb.append(",").append("data:").append("{").append(buildData(m_menu, jspContext)).append("}");

		sb.append(",").append("children:").append("[");
		String directory = m_menu.getParamValueString("directory", null);
		if (directory != null && directory.length() > 0) {
			List<Resource> list = XmlResourceLister.getResources(directory);
			if (list != null && list.size() > 0) {
				int idx = 0;
				for (Resource x : list) {
					if (idx > 0) sb.append(",");
					sb.append("{");
					sb.append(new ResourceJsonObjectRender(x).render(jspContext));
					sb.append(",").append("data:").append("{").append(buildData(x, jspContext)).append("}");
					sb.append("}");
					idx++;
				}
			}
		}
		sb.append("]");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * 重载：总是返回空字符串。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.menu.MenuOutlineRender#renderTreeView(com.tansuosoft.discoverx.model.Resource,
	 *      com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	protected String renderTreeView(Resource r, JSPContext jspContext) {
		return "";
	}
}

