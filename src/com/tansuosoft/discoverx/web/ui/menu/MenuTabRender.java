/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.MenuTarget;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.ResourceTreeHelper;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.json.ResourceDataJsonObjectRender;
import com.tansuosoft.discoverx.web.ui.json.ResourceJsonObjectRender;

/**
 * 菜单大纲第一级内容标签页呈现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class MenuTabRender implements HtmlRender {
	private String m_selectedMenuUnid = null;

	/**
	 * 缺省构造器。
	 */
	public MenuTabRender() {
	}

	/**
	 * 重载render：输出当前用户对应门户菜单第一级内容标签页(Tab)数据结构的json代码。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		MenuForm mf = new MenuForm();
		mf.fillWebRequestForm((HttpServletRequest) jspContext.getRequest());
		Menu menu = mf.getRoot();
		String menuUnid = (menu == null ? null : menu.getUNID());
		this.m_selectedMenuUnid = mf.getLevelUNID(1);
		if (menuUnid == null || menuUnid.length() == 0) {
			Portal portal = jspContext.getPortal();
			menuUnid = portal.getMenu();
			menu = ResourceContext.getInstance().getResource(menuUnid, Menu.class);
		}
		if (menu == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(this.buildTab(menu, jspContext));
		return sb.toString();
	}

	/**
	 * 根据门户菜单UIND输出该菜单第一级内容标签页(Tab)数据结构的json代码。
	 * 
	 * @param jspContext
	 * @param unid
	 * @return String
	 */
	public String renderbyUind(JSPContext jspContext, String unid) {
		MenuForm mf = new MenuForm();
		mf.fillWebRequestForm((HttpServletRequest) jspContext.getRequest());
		Menu menu = mf.getRoot();
		String menuUnid = (menu == null ? null : menu.getUNID());
		this.m_selectedMenuUnid = mf.getLevelUNID(1);
		if (menuUnid == null || menuUnid.length() == 0) {
			menuUnid = unid;
			menu = ResourceContext.getInstance().getResource(menuUnid, Menu.class);
		}
		if (menu == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(this.buildTab(menu, jspContext));
		return sb.toString();
	}

	/**
	 * 根据门户菜单UIND输出该菜单第一级内容标签页(Tab)数据结构的json代码。
	 * 
	 * @param jspContext
	 * @param path
	 * @return String
	 */
	public String renderbyPath(JSPContext jspContext, String path) {
		if (path == null || path.length() == 0) return null;
		String ids[] = StringUtil.splitString(path, '\\');
		if (ids == null || ids.length == 0) return null;
		Menu root = ResourceContext.getInstance().getResource(ids[0], Menu.class);
		String lastUNID = (ids.length == 1 ? null : ids[ids.length - 1]);
		Menu chosen = (Menu) (lastUNID == null ? root : ResourceTreeHelper.getChildByUNID(root, lastUNID));
		if (chosen == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(this.buildTab(chosen, jspContext));
		return sb.toString();
	}

	/**
	 * 构造TreeView所对应的json对象。
	 * 
	 * @param menu
	 * @param jspContext
	 * @return
	 */
	protected String buildTab(Menu menu, JSPContext jspContext) {
		if (menu == null) return null;
		StringBuilder sb = new StringBuilder();
		sb.append(new ResourceJsonObjectRender(menu).render(jspContext));
		List<Resource> list = menu.getChildren();
		if (list != null && list.size() > 0) {
			sb.append(",").append("items:").append("[");
			sb.append("{");
			sb.append("id:'tabi_homepage',label:'首页'");
			if (this.m_selectedMenuUnid == null || this.m_selectedMenuUnid.length() == 0) {
				sb.append(",").append("selected:true");
				sb.append(",").append("desc:'“").append(CommonConfig.getInstance().getSystemName()).append("”门户首页'");
			} else {
				sb.append(",").append("desc:'转至“").append(CommonConfig.getInstance().getSystemName()).append("”门户首页'");
			}
			sb.append(",").append("data:").append("{"); // data begin
			sb.append("menuTarget:").append(MenuTarget.Href);
			sb.append(",").append("target:").append("'").append(StringUtil.encode4Json(jspContext.getPathContext().getFramePath())).append("index.jsp'");
			sb.append("}"); // data end
			sb.append("}");
			Menu m = null;
			User u = jspContext.getLoginUser();
			int idx = 0;
			for (Resource x : list) {
				if (x == null || !(x instanceof Menu)) continue;
				boolean visible = MenuRender.checkUsage(u, x);
				m = (Menu) x;
				sb.append(",").append("{");
				sb.append("id:").append("'tabi_").append(idx++).append("'");
				sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(x.getName())).append("'");
				if (!visible) sb.append(",").append("visible:false");
				String desc = x.getDescription();
				if (desc == null || desc.trim().length() == 0) desc = x.getName();
				if (x.getUNID().equalsIgnoreCase(this.m_selectedMenuUnid)) {
					sb.append(",").append("selected:true");
					sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(desc)).append("'");
				} else {
					sb.append(",").append("desc:").append("'转至“").append(StringUtil.encode4Json(desc)).append("”'");
				}
				sb.append(",").append("data:").append("{"); // data begin
				sb.append("menuTarget:").append(m.getMenuTarget());
				sb.append(",").append("target:").append("'").append(StringUtil.encode4Json(m.getTarget())).append("'");
				sb.append(",").append("targetDirectory:").append("'").append(StringUtil.encode4Json(m.getTargetDirectory())).append("'");
				sb.append(",").append(new ResourceDataJsonObjectRender(x).render(jspContext));
				sb.append("}"); // data end
				sb.append("}");
			}
			sb.append("]");
		}
		return sb.toString();
	}
}

