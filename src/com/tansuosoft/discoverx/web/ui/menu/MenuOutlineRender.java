/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.MenuTarget;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.json.ResourceDataJsonObjectRender;
import com.tansuosoft.discoverx.web.ui.selector.ParticipantTreeViewProvider;

/**
 * 输出菜单项大纲（{@link Menu}）客户端呈现内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class MenuOutlineRender implements HtmlRender {
	/**
	 * 当前要输出其内容的目标菜单项资源。
	 */
	protected Menu m_menu = null;
	/**
	 * 包含控制菜单显示的参数信息。
	 */
	protected MenuForm m_menuForm = null;
	/**
	 * 目标菜单项在其所属上级资源中的索引位置。
	 */
	protected int m_index = 0;
	/**
	 * 是否选中标记。
	 */
	protected boolean m_selected = false;

	/**
	 * 缺省构造器。
	 */
	public MenuOutlineRender() {
	}

	/**
	 * 接收目标菜单{@link Menu}对象、{@link MenuForm}对象、目标菜单项所在索引位置的构造器。
	 * 
	 * @param targetMenu 必须。
	 * @param menuForm 必须。
	 * @param index targetMenu对应菜单项资源的在其父资源所属子资源列表中的位置序号，如果小于0则表示直接返回{@link #renderSpecial(JSPContext)}的结果。
	 */
	public MenuOutlineRender(Menu targetMenu, MenuForm menuForm, int index) {
		init(targetMenu, menuForm, index);
	}

	/**
	 * 接收目标菜单{@link Menu}对象、{@link MenuForm}对象、目标菜单项所在索引位置的初始化器。
	 * 
	 * @param targetMenu 必须。
	 * @param menuForm 必须。
	 * @param index targetMenu对应菜单项资源的在其父资源所属子资源列表中的位置序号，如果小于0则表示直接返回{@link #renderSpecial(JSPContext)}的结果。
	 */
	protected void init(Menu targetMenu, MenuForm menuForm, int index) {
		m_menu = targetMenu;
		if (m_menu == null) throw new IllegalArgumentException("必须提供有效菜单资源！");
		m_menuForm = menuForm;
		if (m_menuForm == null) throw new IllegalArgumentException("必须提供有效菜单大纲呈现信息！");
		m_index = index;
		this.m_selected = (m_menu.getUNID().equalsIgnoreCase(this.m_menuForm.getLevelUNID(m_menuForm.getMaxLevel() - 1)));
	}

	/**
	 * 重载render：输出最终结果。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (m_index < 0) return renderSpecial(jspContext);
		StringBuilder sb = new StringBuilder();
		sb.append(renderSpecial(jspContext));
		return sb.toString();
	}

	/**
	 * 基于特定目标类型的菜单{@link Menu}资源输出客户端呈现内容的重载方法。
	 * 
	 * @param jspContext
	 * @return
	 */
	protected abstract String renderSpecial(JSPContext jspContext);

	/**
	 * 输出客户端TreeView初始化所需的json文本。
	 * 
	 * @param r 为要为其输出json内容的资源对象。
	 * @param jspContext
	 * @return
	 */
	protected abstract String renderTreeView(Resource r, JSPContext jspContext);

	/**
	 * 构造额外数据所对应的json对象。
	 * 
	 * @param r
	 * @param jspContext
	 * @return
	 */
	protected String buildData(Resource r, JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		sb.append(new ResourceDataJsonObjectRender(r).render(jspContext));
		if (r instanceof Menu) {
			Menu menu = (Menu) r;
			String target = StringUtil.getValueString(menu.getTarget(), "");
			if (target.length() > 0 && menu.getMenuTarget() == MenuTarget.Href) {
				PathContext pc = jspContext.getPathContext();
				target = StringUtil.encode4Json(pc.parseUrl(target));
			}
			sb.append(",").append("target:").append("'").append(target).append("'");
			sb.append(",").append("menuTarget:").append(menu.getMenuTarget());
			String td = menu.getTargetDirectory();
			if ((td == null || td.length() == 0) && menu.getMenuTarget() == MenuTarget.Resource) {
				td = "application";
			}
			if ((td == null || td.length() == 0) && menu.getMenuTarget() == MenuTarget.Href) {
				td = "_right";
			}
			if (td == null) td = "";
			sb.append(",").append("targetDirectory:").append("'").append(td).append("'");
			sb.append(",").append("subMenu:true");
		} else {
			sb.append(",").append("target:").append("'").append(r.getUNID()).append("'");
			sb.append(",").append("menuTarget:").append(MenuTarget.Resource);
			sb.append(",").append("targetDirectory:").append("'").append(r.getDirectory()).append("'");
		}
		if (r.getUNID().equalsIgnoreCase(m_menu.getTarget()) || (r.getAlias() != null && r.getAlias().equalsIgnoreCase(m_menu.getTarget()))) {
			sb.append(",").append("rootMenuTarget:").append(m_menu.getMenuTarget());
			sb.append(",").append("rootTarget:").append("'").append(m_menu.getTarget()).append("'");
		}
		if (r instanceof View) {
			View view = (View) r;
			String display = view.getDisplay();
			if (display != null && display.length() > 0 && display.indexOf(':') >= 0) {
				sb.append(",").append("display:").append("'").append(jspContext.getPathContext().parseUrl(display)).append("'");
			}
			Application app = ResourceContext.getInstance().getResource(view.getPUNID(), Application.class);
			if (app != null) {
				sb.append(",").append("appUnid:").append("'").append(app.getUNID()).append("'");
				sb.append(",").append("appAlias:").append("'").append(StringUtil.encode4Json(app.getAlias())).append("'");
				sb.append(",").append("appName:").append("'").append(StringUtil.encode4Json(app.getName())).append("'");
			}
		}
		return sb.toString();
	}

	/**
	 * 输出TreeView子节点为动态内容时的子树占位符和ajaxChildren为真的标记。
	 * 
	 * @return String
	 */
	protected String buildAjaxWaitChild() {
		StringBuilder sb = new StringBuilder();
		sb.append("\r\nchildren:[");
		sb.append("{");
		sb.append("id:").append("'").append("ajaxplaceholderid_").append(System.currentTimeMillis()).append("'");
		sb.append(",").append("label:").append("'").append(ParticipantTreeViewProvider.TV_ASYNC_LABEL).append("'");
		sb.append(",").append("selectable:false");
		sb.append("}");
		sb.append("]");
		sb.append(",\r\n").append("ajaxChildren:true");
		return sb.toString();
	}
}

