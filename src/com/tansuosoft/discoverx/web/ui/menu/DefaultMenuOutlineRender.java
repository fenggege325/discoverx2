/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.view.ViewHelper;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.json.ResourceJsonObjectRender;

/**
 * 系统默认输出菜单项大纲客户端呈现内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DefaultMenuOutlineRender extends MenuOutlineRender {

	/**
	 * 接收必要参数的构造器。
	 * 
	 * <p>
	 * 参考{@link MenuOutlineRender#MenuOutlineRender(Menu, MenuForm, int)}
	 * </p>
	 * 
	 * @param targetMenu
	 * @param menuForm
	 * @param index
	 */
	public DefaultMenuOutlineRender(Menu targetMenu, MenuForm menuForm, int index) {
		super(targetMenu, menuForm, index);
	}

	/**
	 * 重载：输出菜单项大纲客户端呈现内容。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.menu.MenuOutlineRender#renderSpecial(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	protected String renderSpecial(JSPContext jspContext) {
		if (MenuRender.isApplication(m_menu)) {
			// 如果菜单项指向应用程序资源
			return new ApplicationMenuOutlineRender(this.m_menu, this.m_menuForm, this.m_index).render(jspContext);
		} else if (MenuRender.isResource(m_menu) && m_menu.getRenderResourceTree()) {
			// 如果菜单项指向的是独立的资源且菜单项配置为输出目标资源树
			Resource r = ResourceContext.getInstance().getResource(m_menu.getTarget(), m_menu.getTargetDirectory());
			if (r == null) {
				String unid = ResourceAliasContext.getInstance().getUNIDByAlias(m_menu.getTargetDirectory(), m_menu.getTarget());
				r = ResourceContext.getInstance().getResource(unid, m_menu.getTargetDirectory());
			}
			// 如果没取到目标资源，则输出菜单项本身资源树，否则输出目标资源树。
			return renderTreeView((r == null ? m_menu : r), jspContext);
		}
		// 否则输出菜单项资源树。
		return renderTreeView(m_menu, jspContext);
	}

	/**
	 * 重载。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.menu.MenuOutlineRender#renderTreeView(com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	protected String renderTreeView(Resource r, JSPContext jspContext) {
		User u = jspContext.getLoginUser();
		if (r == null || !MenuRender.checkUsage(u, r)) return "";
		StringBuilder sb = new StringBuilder();
		sb.append("\r\n{");
		sb.append(new ResourceJsonObjectRender(r).render(jspContext));
		sb.append(",").append("data:").append("{").append(buildData(r, jspContext)).append("}");
		if (r instanceof View) {
			View view = (View) r;
			if (ViewHelper.hasCategoryColumn(view)) {
				sb.append(",").append(buildAjaxWaitChild());
			}
		} else {
			sb.append(",").append("children:").append("[");
			List<Resource> list = r.getChildren();
			if (list != null && list.size() > 0) {
				int idx = 0;
				MenuOutlineRender menuOutlineRender = null;
				for (Resource x : list) {
					if (x == null || !MenuRender.checkUsage(u, x)) continue;
					if ((x instanceof Menu)) { // 在输出目标资源树时，只有菜单项资源本身需要被重新处理
						menuOutlineRender = MenuRender.getMenuOutlineRender((Menu) x, this.m_menuForm, -1);
						if (menuOutlineRender != null) sb.append(idx == 0 ? "" : ",").append(menuOutlineRender.render(jspContext));
					} else { // 其它资源直接递归输出资源树。
						sb.append(idx == 0 ? "" : ",").append(renderTreeView(x, jspContext));
					}
					idx++;
				}
			}
			sb.append("]"); // children end
		}
		sb.append("}");
		return sb.toString();
	}
}

