/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.view.ViewCategoryEntry;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.bll.view.ViewQueryProvider;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.tree.StringComparator;
import com.tansuosoft.discoverx.util.tree.Tree;
import com.tansuosoft.discoverx.util.tree.TreeBuilder;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.view.ViewColumnDictionaryRender;

/**
 * 输出视图分类信息TreeView格式json内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tensosoft.com
 */
public class ViewCategoryTreeViewRender implements HtmlRender {
	private View view = null;

	/**
	 * 接收视图unid或视图别名的构造器。
	 * 
	 * @param viewId
	 */
	public ViewCategoryTreeViewRender(String viewId) {
		if (viewId == null || viewId.length() == 0) return;
		String unid = viewId;
		if (viewId.startsWith("view")) {
			unid = ResourceAliasContext.getInstance().getUNIDByAlias(View.class, viewId);
		}
		view = ResourceContext.getInstance().getResource(unid, View.class);
	}

	/**
	 * 分类项中的多值分隔符。
	 * 
	 * <p>
	 * 如果分类项中有这些字符，则会被作为多个分类项的分隔符，找到一个分隔符后，就不会再查找其它分隔符了，因此某一条分类项的分隔符只能存在以下分隔符中的一个而不能混合使用。
	 * </p>
	 */
	protected static final char SEPS[] = { ' ', ';', ',', '|', '~', '`' };

	/**
	 * 重载：输出视图分类数据对应的TreeView格式json内容。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		try {
			if (view == null) throw new Exception("视图未知！");
			Session s = jspContext.getUserSession();
			ViewQuery vq = ViewQueryProvider.getViewQuery(view, s);
			List<ViewColumn> vcs = view.getColumns();
			if (vcs == null) throw new Exception("视图没有配置有效列信息！");
			List<ViewColumn> vccs = new ArrayList<ViewColumn>();
			Map<Integer, Integer> idxMap = new HashMap<Integer, Integer>(); // 用于根据第几个分类列查询分类列在视图列中的序号
			int idxKey = 0, idxValue = 0;
			for (ViewColumn x : vcs) {
				if (x != null && x.getCategorized()) {
					vccs.add(x);
					idxMap.put(idxKey, idxValue);
					idxKey++;
				}
				idxValue++;
			}
			List<ViewCategoryEntry> vces = vq.getCategories();
			if (vces == null || vces.isEmpty()) throw new Exception("暂无数据！");

			List<String> vals = null;
			int validViewCategoryEntryCount = 0;
			for (ViewCategoryEntry x : vces) {
				if (x == null) continue;
				vals = x.getValues();
				if (vals == null || vals.isEmpty()) continue;
				validViewCategoryEntryCount++;
			}
			ViewColumn vc = null;
			Set<String> tmpVals = null;
			String[] mvals = null;
			int vcidx = 0;
			String v = null;
			String vcdrr = null;
			for (ViewCategoryEntry x : vces) {
				if (x == null) continue;
				vals = x.getValues();
				if (vals == null || vals.isEmpty()) continue;
				vc = vccs.get(x.getIndex());
				vcidx = idxMap.get(x.getIndex());
				tmpVals = new TreeSet<String>(new StringComparator());
				// 处理多个分类值。
				for (int i = 0; i < vals.size(); i++) {
					v = vals.get(i);
					if (v == null || v.length() == 0) continue;
					v = v.replace('/', '\\');
					mvals = null;
					for (char c : SEPS) {
						if (v.indexOf(c) > 0) {
							mvals = StringUtil.splitString(v, c);
							break;
						}
					}
					if (mvals == null) {
						tmpVals.add(v);
					} else {
						for (String val : mvals) {
							tmpVals.add(val);
						}
					}
				}
				vals = new ArrayList<String>(tmpVals);
				if (validViewCategoryEntryCount > 1) {
					sb.append(",\r\n");
					sb.append("{");
					sb.append("id:'tvi_vce_").append(view.getAlias()).append("_").append(x.getIndex()).append("'");
					sb.append(",label:'按").append(vc.getTitle()).append("分类'");
					sb.append(",desc:'按").append(vc.getTitle()).append("分类'");
					sb.append(",data:{menuTarget:11}");
					vcdrr = ViewColumnDictionaryRender.getViewColumnDictionaryRender(vc).render(jspContext);
					if (vcdrr != null && vcdrr.length() > 0) sb.append(",dict:").append(vcdrr);
					sb.append(",children:[");
				}
				TreeBuilder treeBuilder = new TreeBuilder();
				for (int i = 0; i < vals.size(); i++) {
					v = vals.get(i);
					if (StringUtil.isBlank(v)) continue;
					treeBuilder.add(new com.tansuosoft.discoverx.util.tree.Tree(v));
				}
				Tree trees[] = treeBuilder.values();
				if (trees == null || trees.length == 0) throw new Exception("暂无数据！");
				for (int i = 0; i < trees.length; i++) {
					if (i > 0) sb.append(",");
					sb.append(buildTVI(trees[i], vcidx));
					if (i == 0 && validViewCategoryEntryCount <= 1) {
						vcdrr = ViewColumnDictionaryRender.getViewColumnDictionaryRender(vc).render(jspContext);
						if (vcdrr != null && vcdrr.length() > 0) {
							sb.deleteCharAt(sb.length() - 1);
							sb.append(",dict:").append(vcdrr);
							sb.append("}");
						}
					}
				}// for i end

				if (validViewCategoryEntryCount > 1) {
					sb.append("]");
					sb.append("}");
				}
			}
			return sb.toString();
		} catch (Exception ex) {
			sb = new StringBuilder();
			sb.append("{id:'tvError").append(System.currentTimeMillis()).append("',error:true,label:'").append(StringUtil.encode4Json(ex.getMessage())).append("}");
		}
		return null;
	}

	private int m_tviIdx = 0;

	/**
	 * 输出TreeView对应的Json文本。
	 * 
	 * @param t
	 * @param vcidx
	 * @return
	 */
	protected String buildTVI(Tree t, int vcidx) {
		StringBuilder sb = new StringBuilder();
		String[] vals = t.getValues();
		sb.append("{");
		sb.append("id:'tvi_vcev_").append(view.getAlias()).append("_").append(m_tviIdx++).append("'");
		String v = StringUtil.encode4Json(vals[vals.length - 1]);
		sb.append(",label:'").append(v).append("'");
		sb.append(",desc:'").append(v).append("'");
		sb.append(",data:{"); // data begin
		sb.append("menuTarget:12");
		sb.append(",viewColumnIndex:").append(vcidx);
		sb.append("}"); // data end

		sb.append(",children:["); // children begin
		List<Tree> children = t.getChildren();
		if (children != null && children.size() > 0) {
			for (int i = 0; i < children.size(); i++) {
				sb.append(i > 0 ? "," : "").append("\r\n");
				sb.append(buildTVI(children.get(i), vcidx));
			}
		}
		sb.append("]"); // children end

		sb.append("}"); // tv end
		return sb.toString();
	}
}

