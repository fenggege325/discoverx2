/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.menu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.MenuTarget;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出一级菜单（{@link Menu}）包含下级大纲（每个一级菜单包含的二级菜单作为独立treeview）客户端呈现内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class MenuRender implements HtmlRender {
	/**
	 * 缺省构造器。
	 */
	public MenuRender() {
	}

	private Menu m_menuItemLv1 = null;
	private MenuForm mf = null;

	protected static final String TVR_ID = "tvr_menuoutline";

	/**
	 * 重载render：输出一级菜单（{@link Menu}）包含下级大纲客户端呈现内容。
	 * 
	 * <p>
	 * 以treeview格式json文本方式输出选中一级子菜单下包含的下级菜单项的内容。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		mf = new MenuForm();
		mf.fillWebRequestForm((HttpServletRequest) jspContext.getRequest());
		Menu root = mf.getRoot();
		if (root == null) return "{id:'" + TVR_ID + "',label:'找不到菜单配置',error:true}";
		m_menuItemLv1 = mf.getLevelMenu(1);
		if (m_menuItemLv1 == null) return "{id:'" + TVR_ID + "',label:'找不到下级菜单',error:true}";
		List<Resource> children = m_menuItemLv1.getChildren();
		if (children == null || children.isEmpty()) return "{id:'" + TVR_ID + "',label:'" + m_menuItemLv1.getName() + "下无内容',error:true}";
		MenuOutlineRender menuOutlineRender = null;
		StringBuilder sb = new StringBuilder();
		Menu menuItem = null;
		int idx = 0;
		String r = null;
		for (Resource x : children) {
			if (x == null || !(x instanceof Menu) || !checkUsage(jspContext.getLoginUser(), x)) {
				idx++;
				continue;
			}
			menuItem = (Menu) x;
			menuOutlineRender = getMenuOutlineRender(menuItem, mf, idx);
			if (menuOutlineRender == null) {
				idx++;
				continue;
			}
			r = menuOutlineRender.render(jspContext);
			if (r != null && r.length() > 0) {
				if (idx > 0) sb.append(",");
				sb.append(r);
			}
			idx++;
		}
		if (sb.length() == 0) return "{id:'" + TVR_ID + "',label:'暂无内容',error:true}";
		if (sb.length() > 0) {
			sb.insert(0, "{id:'" + TVR_ID + "',label:'" + m_menuItemLv1.getName() + "',children:[");
			sb.append("]}");
		}

		return sb.toString();
	}

	/**
	 * 获取当前请求对应的{@link MenuForm}对象。
	 * 
	 * @return
	 */
	public MenuForm getMenuForm() {
		return mf;
	}

	/**
	 * 根据menu指定的菜单资源获取其{@link MenuOutlineRender}对象。
	 * 
	 * @param menu {@link Menu}
	 * @param mf {@link MenuForm}对象。
	 * @param idx menu在其上级包含的所有下级菜单列表中的序号
	 * @return
	 */
	public static MenuOutlineRender getMenuOutlineRender(Menu menu, MenuForm mf, int idx) {
		MenuOutlineRender result = null;
		if (MenuRender.isApplication(menu)) {
			result = new ApplicationMenuOutlineRender(menu, mf, idx);
		} else if (menu != null && menu.getMenuTarget() == MenuTarget.TreeViewProvider) {
			result = Instance.newInstance(menu.getTarget(), MenuOutlineRender.class);
			if (result != null) result.init(menu, mf, idx);
		} else {
			result = new DefaultMenuOutlineRender(menu, mf, idx);
		}
		return result;
	}

	/**
	 * 返回菜单项的数据源类型是否为有效的资源类型。
	 * 
	 * @param menu
	 * @return
	 */
	public static boolean isResource(Menu menu) {
		if (menu == null) return false;
		return (menu.getMenuTarget() == MenuTarget.Resource && !StringUtil.isBlank(menu.getTarget()));
	}

	/**
	 * 返回菜单项的数据源类型是否为有效的应用程序类型的资源。
	 * 
	 * @param menu
	 * @return
	 */
	public static boolean isApplication(Menu menu) {
		if (menu == null) return false;
		return (menu.getMenuTarget() == MenuTarget.Resource && !StringUtil.isBlank(menu.getTarget()) && (StringUtil.isBlank(menu.getTargetDirectory()) || "application".equalsIgnoreCase(menu.getTargetDirectory())));
	}

	/**
	 * 返回菜单项的数据源类型是否为视图类型的资源。
	 * 
	 * @param menu
	 * @return
	 */
	public static boolean isView(Menu menu) {
		if (menu == null) return false;
		return (menu.getMenuTarget() == MenuTarget.Resource && !StringUtil.isBlank(menu.getTarget()) && "view".equalsIgnoreCase(menu.getTargetDirectory()));
	}

	/**
	 * 检查指定用户自定义会话对应的用户是否对指定资源具有使用权限。
	 * 
	 * <p>
	 * 即检查指定资源能否呈现在大纲项中供用户点击。
	 * </p>
	 * 
	 * @param s
	 * @param r
	 * @return
	 */
	public static boolean checkUsage(Session s, Resource r) {
		return SecurityHelper.authorize(s, r, SecurityLevel.Usage);
	}

	/**
	 * 检查指定用户是否对指定资源具有使用权限。
	 * 
	 * <p>
	 * 即检查指定资源能否呈现在大纲项中供用户点击。
	 * </p>
	 * 
	 * @param u
	 * @param r
	 * @return
	 */
	public static boolean checkUsage(User u, Resource r) {
		return SecurityHelper.authorize(u, r, SecurityLevel.Usage);
	}

	private Menu m_dummy = null;
	private Map<String, Integer> m_singleIndexes = null;

	/**
	 * 追加单一的菜单项资源（不包含下级的菜单资源）以便集中呈现。
	 * 
	 * @param r
	 * @param idx 单一菜单项资源的在其父资源所属子资源列表中的位置序号。
	 */
	protected void addSingle(Resource r, int idx) {
		if (r == null) return;
		if (m_dummy == null) {
			m_dummy = new Menu();
			m_dummy.setName(m_menuItemLv1.getName());
			m_dummy.setAlias("dummy_" + m_menuItemLv1.getAlias());
			m_singleIndexes = new HashMap<String, Integer>();
		}
		m_singleIndexes.put(r.getUNID(), idx);
		m_dummy.addChild(r);
	}

	/**
	 * 输出包含的所有单一的菜单项资源（不包含下级的菜单资源）的客户端呈现内容。
	 * 
	 * @param jspContext
	 * 
	 * @return
	 */
	protected String renderSingle(JSPContext jspContext) {
		if (m_dummy == null) return "";
		List<Resource> list = m_dummy.getChildren();
		if (list == null || list.isEmpty()) return "";
		MenuOutlineRender menuOutlineRender = new DefaultMenuOutlineRender(m_dummy, mf, -1);
		return menuOutlineRender.render(jspContext);
	}
}

