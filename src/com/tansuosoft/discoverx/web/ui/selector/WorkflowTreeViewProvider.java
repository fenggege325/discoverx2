/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.selector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.document.DocumentOpener;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.workflow.WFDataParticipantsProvider;
import com.tansuosoft.discoverx.workflow.WorkflowGroupsProvider;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 输出流程参与者选择框TreeView数据源的{@link TreeViewProvider}实现类。
 * 
 * <p>
 * 绑定的{@link SelectorForm}的属性格式说明：<br/>
 * <ul>
 * <li>{@link SelectorForm#getDatasource()}的格式为wf://{文档unid}/{流程实例}/{环节unid}。</li>
 * <li>{@link SelectorForm#getFilter()}用于获取参与者范围安全编码，使用半角逗号分隔多个安全编码，如果安全编码前有“+”标记，则表示默认选中。</li>
 * <li>{@link SelectorForm#getLabel()}的格式为获取TreeView中label结果的标记，默认为“cn”。</li>
 * <li>{@link SelectorForm#getValue()}的值固定为“sc”。</li>
 * </ul>
 * </p>
 * <p>
 * {@link SelectorForm#getLabel()}与{@link SelectorForm#getValue()}支持的标记请参考{@link com.tansuosoft.discoverx.model.User#LEGALLY_NAME_PART_FORMATS}中的说明。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowTreeViewProvider extends TreeViewProvider {
	protected static final String NAME = "人员范围";
	protected static final String UNID = "9A7AF4D149D04A31980E11A4EB8395B1";
	protected static final String ALIAS = "wfproot";

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		String labelFormat = this.selectorForm.getLabel();
		if (labelFormat == null || labelFormat.length() == 0) this.selectorForm.setLabel("cn");
		this.selectorForm.setValue("sc");
		m_title = "选择流程参与者";
		String ds = this.selectorForm.getDatasource();
		ds = StringUtil.stringRight(ds, PREFIX_DELIMITER);
		String delimiter = "/";
		String docUnid = StringUtil.stringLeft(ds, delimiter);
		WorkflowRuntime wfr = null;
		try {
			Map<Integer, Boolean> selectedQueryMap = new HashMap<Integer, Boolean>(); // 是否默认选中查询Map
			List<Integer> sclist = new ArrayList<Integer>();
			String strSCs = this.selectorForm.getFilter();
			if (strSCs != null && strSCs.length() > 0) {
				String[] scs = StringUtil.splitString(strSCs, ',');
				int securityCode = 0;
				boolean selected = false;
				for (String sc : scs) {
					if (sc == null || sc.length() == 0) continue;
					selected = (sc.startsWith("+") ? true : false);
					securityCode = StringUtil.getValueInt((selected ? sc.substring(1) : sc), -1);
					if (securityCode > 0) {
						selectedQueryMap.put(securityCode, selected);
						sclist.add(securityCode);
					}
				}
			}

			// 没有指定范围则使用默认用户选选择所有用户
			if (sclist == null || sclist.isEmpty()) {
				ParticipantTreeViewProvider p = new ParticipantTreeViewProvider();
				p.selectorForm = this.selectorForm;
				p.selectorForm.setDatasource("p://");
				p.selectorForm.setFilter(null);
				return p.render(jspContext);
			}

			Session session = jspContext.getUserSession();
			Document document = (Document) (new DocumentOpener()).open(docUnid, Document.class, session);
			if (document == null) throw new RuntimeException("无法打开“" + docUnid + "”对应的文档！");
			String strInstance = StringUtil.stringRight(ds, delimiter);
			int instance = 0;
			if (strInstance != null && strInstance.length() > 0) instance = StringUtil.getValueInt(strInstance, -1);
			wfr = WorkflowRuntime.getInstance(document, session, instance);

			Group group = null;
			Map<String, String> parameters = null;

			ParticipantTree root = new ParticipantTree(ParticipantTree.WORKFLOW_ROOT_CODE, NAME, UNID, ALIAS, ParticipantType.Root, false);
			ParticipantTree ptx = null;
			ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
			List<Participant> children = null;
			Boolean querySelected = null;
			boolean selected = false;
			int ptMask = 0;
			// 获取每一个预设参与者内容并输出TreeView
			for (int sc : sclist) {
				if (sc <= 0) continue;
				querySelected = selectedQueryMap.get(sc);
				selected = (querySelected == null ? false : querySelected.booleanValue());
				ptx = ptp.getParticipantTree(sc);
				if (ptx == null) {
					group = WorkflowGroupsProvider.getGroup(sc);
					if (group != null) ptx = new ParticipantTree(sc, group.getName(), group.getUNID(), group.getAlias(), ParticipantType.Group, selected);
					else throw new Exception("无法获取配置的流程参与者范围信息，以下为可能的原因：1.被删除了；2.因系统更新导致安全编码变动。请联系系统管理员尝试解决此问题。");
				} else {
					ptx = new ParticipantTree(sc, ptx.getName(), ptx.getUNID(), ptx.getAlias(), ptx.getParticipantType(), selected);
				}
				children = null;
				if (ptx.getParticipantType() == ParticipantType.Group || ptx.getParticipantType() == ParticipantType.Role) {
					if (group != null) {
						children = WFDataParticipantsProvider.getParticipants(wfr, group, parameters);
					} else {
						children = ptp.getParticipants(ptx, session);
					}
					if (children != null && children.size() > 0) {
						for (Participant x : children) {
							if (x == null) continue;
							ptx.addChild(new ParticipantTree(x));
						}
					}
					if (ptx.getChildren() == null || ptx.getChildren().size() == 0) continue;
				} else {
					List<ParticipantTree> pts = ptx.getChildren();
					if (pts != null && pts.size() > 0) {
						ptx.setChildren(new ArrayList<ParticipantTree>(pts.size()));
						ptx.getChildren().addAll(pts);
					}
				}
				ptMask |= ptx.getParticipantType().getIntValue();
				root.addChild(ptx);
			}
			// 只有一个部门、群组或角色时直接作为根。
			List<ParticipantTree> list = root.getChildren();
			if (list != null && list.size() == 1) {
				if (ptMask == ParticipantType.Organization.getIntValue()) {
					root = list.get(0);
				} else if (ptMask == ParticipantType.Group.getIntValue() || ptMask == ParticipantType.Role.getIntValue()) {
					ParticipantTree subRoot = list.get(0);
					if (subRoot != null && subRoot.getChildren() != null && subRoot.getChildren().size() > 0) root = subRoot.getChildren().get(0);
				}
			}
			if (list == null || list.isEmpty()) throw new Exception("没有包含任何有效用户，请联系管理员确认流程配置是否正常！");
			return ParticipantTreeViewProvider.buildTreeView(root, jspContext, selectorForm, true, root.getSelected(), false, 0);
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				throw (RuntimeException) e;
			} else {
				throw new RuntimeException(e);
			}
		} finally {
			if (wfr != null) wfr.shutdown();
		}
	}
}

