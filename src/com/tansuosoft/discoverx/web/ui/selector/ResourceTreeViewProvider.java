/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.selector;

import java.lang.reflect.Method;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.ResourceTreeHelper;
import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.json.ResourceDataJsonObjectRender;

/**
 * 输出资源树选择框TreeView数据源的{@link TreeViewProvider}实现类。
 * 
 * <p>
 * 绑定的{@link SelectorForm}的属性格式说明：<br/>
 * <ul>
 * <li>{@link SelectorForm#getDatasource()}的格式为“{资源目录}://{资源UNID|资源别名}”</li>
 * <li>{@link SelectorForm#getFilter()}的格式为下级分支资源UNID、别名或下级分支层次名</li>
 * <li>{@link SelectorForm#getLabel()}的格式为获取TreeView中label结果的资源所具有的有效属性名称，如“name”，默认为资源名称。</li>
 * <li>{@link SelectorForm#getValue()}的格式为获取TreeView中value结果的资源所具有的有效属性名称，如“UNID”、“alias”，默认为资源名称。</li>
 * </ul>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceTreeViewProvider extends TreeViewProvider {
	private int idx = 0;
	private Resource m_resource = null;

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		try {
			String ds = this.selectorForm.getDatasource();
			if (ds == null || ds.length() == 0) throw new RuntimeException("没有提供有效数据源！");
			String directory = StringUtil.stringLeft(ds, PREFIX_DELIMITER);
			String unid = StringUtil.stringRight(ds, PREFIX_DELIMITER);
			if (unid == null || directory == null) throw new RuntimeException("提供的资源信息无法识别：“" + ds + "”！");
			if (!StringUtil.isUNID(unid)) unid = ResourceAliasContext.getInstance().getUNIDByAlias(directory, unid);
			m_resource = ResourceContext.getInstance().getResource(unid, directory);
			if (m_resource == null) unid = ResourceAliasContext.getInstance().getUNIDByAlias(directory, unid);
			if (unid != null && unid.length() > 0) m_resource = ResourceContext.getInstance().getResource(unid, directory);
			if (m_resource == null) throw new RuntimeException("找不到“" + ds + "”对应的资源！");
			m_title = String.format("选择%1$s", m_resource.getName());
			String filter = this.selectorForm.getFilter();
			Resource sub = null;
			if (filter != null && filter.length() > 0) {
				m_resource.setPath("", "\\");
				if (StringUtil.isUNID(filter)) {
					sub = ResourceTreeHelper.getChildByUNID(m_resource, filter);
				} else if (filter.indexOf('\\') > 0) {
					sub = ResourceTreeHelper.getChildByPath(m_resource, filter);
				} else {
					sub = ResourceTreeHelper.getChildByAlias(m_resource, filter);
				}
				if (sub == null) throw new RuntimeException("找不到“" + m_resource.getName() + "”中“" + filter + "”对应的下级资源！");
			}
			if (sub != null) sub.setPath("", "\\");
			if (m_resource != null) m_resource.setPath("", "\\");
			sb.append(buildTreeView((sub != null ? sub : m_resource), jspContext));
		} catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
		return sb.toString();
	}

	private Method m_labelMethod = null;
	private Method m_valueMethod = null;
	private int m_methodGet = 0;

	/**
	 * 根据绑定的{@link SelectorForm}获取用户返回label结果的方法。
	 * 
	 * @param resource
	 * @return
	 */
	protected Method getLabelMethod(Resource resource) {
		if ((m_methodGet & 1) == 1) return m_labelMethod;
		String propName = this.selectorForm.getLabel();
		if (propName != null && propName.length() != 0) {
			m_labelMethod = ObjectUtil.getMethod(resource, "get" + StringUtil.toPascalCase(propName));
		}
		m_methodGet = 1;
		return m_labelMethod;
	}

	/**
	 * 根据绑定的{@link SelectorForm}获取用户返回value结果的方法。
	 * 
	 * @param resource
	 * @return
	 */
	protected Method getValueMethod(Resource resource) {
		if ((m_methodGet & 2) == 2) return m_valueMethod;
		String propName = this.selectorForm.getValue();
		if (propName != null && propName.length() != 0) {
			m_valueMethod = ObjectUtil.getMethod(resource, "get" + StringUtil.toPascalCase(propName));
		}
		m_methodGet += 2;
		return m_valueMethod;
	}

	/**
	 * 构造TreeView所对应的json对象。
	 * 
	 * @param resource
	 * @param jspContext
	 * @return
	 */
	protected String buildTreeView(Resource resource, JSPContext jspContext) {
		if (resource == null) return null;

		boolean selectable = resource.getSelectable();

		StringBuilder sb = new StringBuilder();

		String unid = resource.getUNID();
		// sb.append("id:").append("'").append(unid).append("'");
		sb.append("id:").append("'tvi_").append(idx++).append("'");

		Method labelMethod = getLabelMethod(resource);
		String label = null;
		if (labelMethod == null) {
			label = resource.getName();
		} else {
			label = StringUtil.convertToString(ObjectUtil.getMethodResult(resource, labelMethod), resource.getName());
		}

		Method valueMethod = getValueMethod(resource);
		String value = null;
		if (valueMethod == null) {
			value = resource.getName();
		} else {
			value = StringUtil.convertToString(ObjectUtil.getMethodResult(resource, valueMethod), null);
			if (value == null) value = resource.getName();
		}
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(label)).append("'");
		sb.append(",").append("value:").append("'").append(StringUtil.encode4Json(value)).append("'");
		String desc = resource.getDescription();
		if (desc != null && desc.length() > 0) sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(desc)).append("'");
		if (!selectable) sb.append(",").append("selectable:").append("false");
		sb.append(",").append("data:").append("{");
		sb.append(buildData(resource, jspContext));
		sb.append(",").append("unid:'").append(unid).append("'");
		sb.append("}");
		List<Resource> list = resource.getChildren();
		if (list != null && list.size() > 0) {
			if (selectable && !this.selectorForm.getBranchSelectable()) this.selectorForm.setBranchSelectable(true);
			int idx = 0;
			sb.append(",").append("children:").append("[");
			for (Resource x : list) {
				if (x == null) continue;
				sb.append(idx == 0 ? "" : ",").append("\r\n{").append(buildTreeView(x, jspContext)).append("}");
				idx++;
			}
			sb.append("]");
		} else if (resource.getUNID().equalsIgnoreCase(m_resource.getUNID())) { throw new RuntimeException("“" + resource.getName() + "”下没有包含可供选择的分支！"); }
		return sb.toString();
	}

	/**
	 * 构造TreeView中data所对应的json对象。
	 * 
	 * @param resource
	 * @param jspContext
	 * @return
	 */
	protected String buildData(Resource resource, JSPContext jspContext) {
		return (new ResourceDataJsonObjectRender(resource)).render(jspContext);
	}
}

