/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.selector;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.workflow.WorkflowGroupsProvider;

/**
 * 输出系统参与者选择框TreeView数据源的{@link TreeViewProvider}实现类。
 * 
 * <p>
 * 绑定的{@link SelectorForm}的属性格式说明：<br/>
 * <ul>
 * <li>{@link SelectorForm#getDatasource()} 的值必须为“p://{branch}”，其中“{branch}”可以为“user、unit、group、role”、“all”、“wf”中的某一个，分别表示选择“用户、部门、群组、角色、所有参与者、带内置流程群组的所有参与者”，如果{branch}不提供，默认为用户。</li>
 * <li>{@link SelectorForm#getFilter()}的值可以为某个分支的安全编码，表示获取某个分支下内容。</li>
 * <li>{@link SelectorForm#getLabel()}的格式为获取TreeView中label结果的标记，默认为“cn”。</li>
 * <li>{@link SelectorForm#getValue()}的格式为获取TreeView中value结果的标记，默认为“sc”。</li>
 * </ul>
 * </p>
 * <p>
 * {@link SelectorForm#getLabel()}与{@link SelectorForm#getValue()}支持的标记请参考{@link com.tansuosoft.discoverx.model.User#LEGALLY_NAME_PART_FORMATS}中的说明。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ParticipantTreeViewProvider extends TreeViewProvider {
	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		String ds = this.selectorForm.getDatasource();
		String labelFormat = this.selectorForm.getLabel();
		if (labelFormat == null || labelFormat.length() == 0) selectorForm.setLabel("cn");
		String valueFormat = this.selectorForm.getValue();
		if (valueFormat == null || valueFormat.length() == 0) selectorForm.setValue("sc");

		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree root = ptp.getRoot();

		boolean includePersons = false; // 是否包含用户
		boolean workflowBuiltinGroupsFlag = false; // 是否需要输出内置流程群组标记
		String title = "";
		if (ds.equalsIgnoreCase(PARTICIPANT_PREFIX) || ds.endsWith("user")) {
			this.selectorForm.setBranchSelectable(false);
			m_title = "选择用户";
			title = "用户";
			includePersons = true;
		} else if (ds.endsWith("unit") || ds.endsWith("org") || ds.endsWith("organization") || ds.endsWith("dept")) {
			this.selectorForm.setBranchSelectable(true);
			root = ptp.getAllSubOrg();
			m_title = "选择部门";
			title = "部门";
		} else if (ds.endsWith("group")) {
			this.selectorForm.setBranchSelectable(true);
			root = ptp.getParticipantTree(ParticipantTree.GROUP_ROOT_CODE);
			m_title = "选择群组";
			title = "群组";
		} else if (ds.endsWith("role")) {
			this.selectorForm.setBranchSelectable(true);
			root = ptp.getParticipantTree(ParticipantTree.ROLE_ROOT_CODE);
			m_title = "选择角色";
			title = "角色";
		} else if (ds.endsWith("all") || ds.endsWith("wf")) {
			workflowBuiltinGroupsFlag = (ds.endsWith("wf"));
			this.selectorForm.setBranchSelectable(true);
			root = ptp.getRoot();
			m_title = "选择参与者";
			title = "参与者";
			includePersons = true;
		} else {
			throw new RuntimeException("未知数据源：“" + ds + "”！");
		}
		ParticipantTree sub = null;
		String filter = this.selectorForm.getFilter();
		if (filter != null && filter.length() > 0) {
			int sc = StringUtil.getValueInt(filter, Integer.MIN_VALUE);
			if (sc != ParticipantTree.ROOT_CODE && sc != 0) {
				sub = ParticipantTreeProvider.getInstance().getParticipantTree(sc);
				if (sub == null) {
					Group g = WorkflowGroupsProvider.getGroup(sc);
					throw new RuntimeException("没有找到“" + (g == null ? sc + "" : g.getName()) + "”对应的" + title + "！");
				}
				if (sub.getParticipantType() == ParticipantType.Group || sub.getParticipantType() == ParticipantType.Role) {
					List<Participant> persons = ptp.getParticipants(sub, jspContext.getUserSession());
					if (persons != null && persons.size() > 0) {
						for (Participant x : persons) {
							if (x != null && x instanceof ParticipantTree) sub.addChild((ParticipantTree) x);
						}
					}
				}
			}
		}
		String result = null;
		if (sub != null) {
			result = buildTreeView(sub, jspContext, this.selectorForm, includePersons, false, workflowBuiltinGroupsFlag, 0);
			if (sub.getParticipantType() == ParticipantType.Group || sub.getParticipantType() == ParticipantType.Role) sub.setChildren(null);
		} else if (root != null) {
			result = buildTreeView(root, jspContext, this.selectorForm, includePersons, false, workflowBuiltinGroupsFlag, 0);
		} else {
			throw new RuntimeException("没有找到匹配的" + title + "！");
		}
		return result;
	}

	public static final String TV_ASYNC_LABEL = "请稍候，正在获取包含的下级分支...";

	/**
	 * 构造参与者对应的treeview初始化json代码以供客户端使用。
	 * 
	 * @param pt 参与者({@link ParticipantTree})
	 * @param jspContext {@link JSPContext}
	 * @param selectorForm 选择框相关属性({@link SelectorForm})
	 * @param includePersons 是否包含用户标记
	 * @param selected 是否选中标记
	 * @param includeWorkflowGroups 是否包含流程群组标记
	 * @param ptidx 参与者id索引。
	 * @return
	 */
	protected static String buildTreeView(ParticipantTree pt, JSPContext jspContext, SelectorForm selectorForm, boolean includePersons, boolean selected, boolean includeWorkflowGroups, int ptidx) {
		String labelFormat = selectorForm.getLabel();
		String valueFormat = selectorForm.getValue();
		boolean branchSelectable = selectorForm.getBranchSelectable();
		if (pt == null) return "";
		if (!includePersons && pt.getParticipantType() == ParticipantType.Person) return "";
		StringBuilder sb = new StringBuilder();
		if (ptidx == 0) uniqueNum.set(0);
		sb.append("id:").append("'pt_").append(uniqueNum.get()).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(ParticipantHelper.getFormatValue(pt, labelFormat))).append("'");
		sb.append(",").append("value:").append("'").append(StringUtil.encode4Json(ParticipantHelper.getFormatValue(pt, valueFormat))).append("'");
		if (pt.getSelected() || selected) sb.append(",").append("selected:").append("true");
		if (pt.getParticipantType() != ParticipantType.Person) {
			boolean isRoleRoot = (ParticipantTree.ROLE_ROOT_UNID.equalsIgnoreCase(pt.getUNID()));
			boolean isGroupRoot = (ParticipantTree.GROUP_ROOT_UNID.equalsIgnoreCase(pt.getUNID()));
			sb.append(",").append("selectable:").append(branchSelectable ? (isRoleRoot || isGroupRoot ? "false" : "true") : "false");
		}
		sb.append(",").append("data:").append("{");
		sb.append("alias:").append("'").append(pt.getAlias()).append("'");
		sb.append(",").append("unid:").append("'").append(pt.getUNID()).append("'");
		sb.append(",").append("participantType:").append(pt.getParticipantType());
		sb.append(",").append("level:").append(pt.getLevel());
		sb.append(",").append("securityCode:").append(pt.getSecurityCode());
		sb.append(",").append("name:").append("'").append(StringUtil.encode4Json(pt.getName())).append("'");
		sb.append(",").append("sort:").append(pt.getSort());
		sb.append("}");
		if (pt.getParticipantType() != ParticipantType.Person) {
			sb.append(",").append("children:[");
			List<ParticipantTree> list = pt.getChildren();

			// 处理自动包括的内置流程群组
			boolean wfBuiltinGroupFlag = (includeWorkflowGroups && pt.getSecurityCode() == ParticipantTree.GROUP_ROOT_CODE);
			if (wfBuiltinGroupFlag) {
				List<Group> wfBuiltinGroups = WorkflowGroupsProvider.getGroups();
				if (wfBuiltinGroups != null) {
					List<ParticipantTree> wfbuiltInGroupPts = new ArrayList<ParticipantTree>(wfBuiltinGroups.size());
					for (Group g : wfBuiltinGroups) {
						if (g == null) continue;
						wfbuiltInGroupPts.add(new ParticipantTree(g.getSecurityCode(), g.getName(), g.getUNID(), g.getAlias(), ParticipantType.Group, 3, null, g.getSort(), pt));
					}
					if (wfbuiltInGroupPts != null && wfbuiltInGroupPts.size() > 0) {
						List<ParticipantTree> newList = new ArrayList<ParticipantTree>(wfbuiltInGroupPts);
						if (list != null && list.size() > 0) newList.addAll(list);
						list = newList;
					}
				}
			}

			// 循环处理子条目
			boolean ajaxChildren = false;
			if (list != null && list.size() > 0) {
				int idx = 0;
				for (ParticipantTree x : list) {
					if (!includePersons && x.getParticipantType() == ParticipantType.Person) continue;
					sb.append(idx == 0 ? "" : ",");
					sb.append("\r\n{");
					sb.append(buildTreeView(x, jspContext, selectorForm, includePersons, (pt.getSelected() || selected), includeWorkflowGroups, 1));
					sb.append("}");
					idx++;
				}
			} else if (!branchSelectable) { // 如果不包含显式的下级分支且不能选择本身，那么在客户端用户单击此条目时动态获取此条目下的动态下级内容
				ajaxChildren = true;
				sb.append("{");
				sb.append("id:").append("'").append("waitforfetchdata").append(System.currentTimeMillis()).append("'");
				sb.append(",").append("label:").append("'").append(TV_ASYNC_LABEL).append("'");
				sb.append(",").append("selectable:false");
				sb.append("}");
			}
			sb.append("]");
			if (ajaxChildren) sb.append(",").append("ajaxChildren:true");
		}
		return sb.toString();
	}

	private static final ThreadLocal<Integer> uniqueNum = new ThreadLocal<Integer>() {
		private int seq = 0;

		@Override
		protected Integer initialValue() {
			seq = 0;
			return 0;
		}

		/**
		 * 重载：
		 * 
		 * @see java.lang.ThreadLocal#get()
		 */
		@Override
		public Integer get() {
			return seq++;
		}

		/**
		 * 重载：
		 * 
		 * @see java.lang.ThreadLocal#set(java.lang.Object)
		 */
		@Override
		public void set(Integer value) {
			seq = value;
		}

	};
}

