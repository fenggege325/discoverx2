/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.selector;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.CommonForm;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;

/**
 * 表示选择框相关属性的{@link CommonForm}实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class SelectorForm extends CommonForm implements JsonSerializable {

	/**
	 * 缺省构造器。
	 */
	public SelectorForm() {

	}

	private String m_datasource = null; // 选择/编辑框数据源，必须。
	private String m_label = null; // 选择/编辑框选择项的标题信息，可选。
	private String m_value = null; // 选择/编辑框选择项的值信息，可选。
	private String m_target = null; // 选择/编辑框选择结果值的接收者信息，可选。
	private String m_labelOk = null; // 选择/编辑框确定后处理选择结果标题时执行的代码。
	private String m_valueOk = null; // 选择/编辑框确定后处理选择结果值时执行的代码。
	private String m_labelTarget = null; // 选择/编辑框选择显示值的接收者信息，可选。
	private String m_filter = null; // 选择/编辑框待选择条目过滤信息
	private boolean m_multiple = false; // 选择结果是否可以选择多值，默认为false。
	private String m_delimiter = ","; // 选择结果可以选择多值时的分隔符，默认为“,”（半角逗号）。
	private Map<String, String> m_complements = null; // 选择/编辑框额外参数名-参数值集合。
	private boolean m_branchSelectable = false; // 分支是否可以选择。

	/**
	 * 返回选择/编辑框数据源，必须。
	 * <p>
	 * 从名为“x”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDatasource() {
		return this.m_datasource;
	}

	/**
	 * 设置选择/编辑框数据源，必须。
	 * 
	 * <p>
	 * 从名为“x”的http请求参数中获取。
	 * </p>
	 * 
	 * @param datasource String
	 */
	public void setDatasource(String datasource) {
		this.m_datasource = datasource;
	}

	/**
	 * 返回选择/编辑框选择项的显示值格式，可选。
	 * 
	 * <p>
	 * 从名为“l”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getLabel() {
		return this.m_label;
	}

	/**
	 * 设置选择/编辑框选择项的显示值格式，可选。
	 * 
	 * <p>
	 * 从名为“l”的http请求参数中获取。
	 * </p>
	 * 
	 * @param label String
	 */
	public void setLabel(String label) {
		this.m_label = label;
	}

	/**
	 * 返回选择/编辑框选择项的结果值格式，可选。
	 * 
	 * <p>
	 * 从名为“v”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getValue() {
		return this.m_value;
	}

	/**
	 * 设置选择/编辑框选择项的结果值格式，可选。
	 * 
	 * <p>
	 * 从名为“v”的http请求参数中获取。
	 * </p>
	 * 
	 * @param value String
	 */
	public void setValue(String value) {
		this.m_value = value;
	}

	/**
	 * 返回选择/编辑框选择结果值的接收者信息，可选。
	 * 
	 * <p>
	 * 从名为“t”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTarget() {
		return this.m_target;
	}

	/**
	 * 设置选择/编辑框选择结果值的接收者信息，可选。
	 * 
	 * <p>
	 * 从名为“t”的http请求参数中获取。
	 * </p>
	 * <p>
	 * 通常是选择结果值要填充的目标浏览器字段的id。
	 * </p>
	 * 
	 * @param target String
	 */
	public void setTarget(String target) {
		this.m_target = target;
	}

	/**
	 * 返回选择/编辑框选择显示值的接收者信息，可选。
	 * 
	 * <p>
	 * 从名为“tt”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getLabelTarget() {
		return this.m_labelTarget;
	}

	/**
	 * 设置选择/编辑框选择显示值的接收者信息，可选。
	 * 
	 * <p>
	 * 从名为“tt”的http请求参数中获取。
	 * </p>
	 * <p>
	 * 通常是选择显示值要填充的目标浏览器字段的id。
	 * </p>
	 * 
	 * @param labelTarget String
	 */
	public void setLabelTarget(String labelTarget) {
		this.m_labelTarget = labelTarget;
	}

	/**
	 * 返回选择/编辑框待选择条目过滤信息，
	 * 
	 * <p>
	 * 从名为“f”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getFilter() {
		return this.m_filter;
	}

	/**
	 * 设置选择/编辑框待选择条目过滤信息，
	 * 
	 * <p>
	 * 从名为“f”的http请求参数中获取。
	 * </p>
	 * 
	 * @param filter String
	 */
	public void setFilter(String filter) {
		this.m_filter = filter;
	}

	/**
	 * 返回选择结果是否可以选择多值，默认为false。
	 * 
	 * <p>
	 * 从名为“m”的http请求参数中获取。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getMultiple() {
		return this.m_multiple;
	}

	/**
	 * 设置选择结果是否可以选择多值，默认为false。
	 * 
	 * <p>
	 * 从名为“m”的http请求参数中获取。
	 * </p>
	 * 
	 * @param multiple boolean
	 */
	public void setMultiple(boolean multiple) {
		this.m_multiple = multiple;
	}

	/**
	 * 返回选择结果可以选择多值时的分隔符，默认为“,”（半角逗号）。
	 * 
	 * <p>
	 * 从名为“d”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDelimiter() {
		return this.m_delimiter;
	}

	/**
	 * 设置选择结果可以选择多值时的分隔符，默认为“,”（半角逗号）。
	 * 
	 * <p>
	 * 从名为“d”的http请求参数中获取。
	 * </p>
	 * 
	 * @param delima String
	 */
	public void setDelimiter(String delima) {
		this.m_delimiter = delima;
	}

	/**
	 * 返回分支是否可以选择。
	 * 
	 * <p>
	 * 分支是指包含下级分枝节点（也叫文件夹或目录节点），区别于根节点和叶节点（也叫文件节点）。
	 * </p>
	 * <p>
	 * 默认为false，表示不能选择分支。
	 * </p>
	 * <p>
	 * 此属性仅影响参与者数据源({@link ParticipantTreeViewProvider})和资源数据源({@link ResourceTreeViewProvider})的行为。
	 * </p>
	 * <p>
	 * 从名为“s”的http请求参数中获取。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getBranchSelectable() {
		return this.m_branchSelectable;
	}

	/**
	 * 设置分支是否可以选择。
	 * 
	 * <p>
	 * 从名为“s”的http请求参数中获取。
	 * </p>
	 * <p>
	 * 在参与者数据源({@link ParticipantTreeViewProvider})中，如果类型为选择非用户参与者则此属性会被自动设置为true；
	 * </p>
	 * <p>
	 * 在资源数据源({@link ResourceTreeViewProvider})中，如果资源本身可被选择(即{@link com.tansuosoft.discoverx.model.Resource#getSelectable()}返回true)且资源包含下级资源则此属性会被自动设置为true。
	 * </p>
	 * 
	 * @param branchSelectable boolean
	 */
	public void setBranchSelectable(boolean branchSelectable) {
		this.m_branchSelectable = branchSelectable;
	}

	/**
	 * 返回选择/编辑框确定后处理选择结果标题时执行的js函数名。
	 * 
	 * <p>
	 * 从名为“lo”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getLabelOk() {
		return this.m_labelOk;
	}

	/**
	 * 设置选择/编辑框确定后处理选择结果标题时执行的js函数名。
	 * 
	 * <p>
	 * 可选，从名为“lo”的http请求参数中获取。
	 * </p>
	 * 
	 * @param labelOk String
	 */
	public void setLabelOk(String labelOk) {
		this.m_labelOk = labelOk;
	}

	/**
	 * 返回选择/编辑框确定后处理选择结果值时执行的js函数名。
	 * 
	 * <p>
	 * 从名为“vo”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getValueOk() {
		return this.m_valueOk;
	}

	/**
	 * 设置选择/编辑框确定后处理选择结果值时执行的js函数名。
	 * 
	 * <p>
	 * 可选，从名为“vo”的http请求参数中获取。
	 * </p>
	 * 
	 * @param valueOk String
	 */
	public void setValueOk(String valueOk) {
		this.m_valueOk = valueOk;
	}

	/**
	 * 返回选择/编辑框额外参数名-参数值集合。
	 * 
	 * <p>
	 * 用于从前台（http请求）向后台或者从后台向前台（js/json等）传递额外参数。<br/>
	 * 其内容和方向由使用者自行定义和解释。
	 * </p>
	 * 
	 * @return Map&lt;String, String&gt;
	 */
	public Map<String, String> getComplements() {
		return this.m_complements;
	}

	/**
	 * 设置选择/编辑框额外参数名-参数值集合。
	 * 
	 * <p>
	 * 如果是通过http请求向后台提交的参数，则从内置http请求参数名范围（{@link SelectorForm#BuiltInParameters}）之外的请求参数名中获取。<br/>
	 * 如果是后台提供的，则需由调用者填充这些额外参数。
	 * </p>
	 * 
	 * @param complements Map&lt;String, String&gt;
	 */
	public void setComplements(Map<String, String> complements) {
		this.m_complements = complements;
	}

	/**
	 * 内置已定义参数名数组。
	 */
	protected static final String[] BuiltInParameters = { "x", "f", "t", "l", "v", "m", "d", "s", "tt", "lo", "vo" };

	/**
	 * 
	 * 重载fillWebRequestForm
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.CommonForm#fillWebRequestForm(javax.servlet.http.HttpServletRequest)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CommonForm fillWebRequestForm(HttpServletRequest request) {
		if (request == null) return this;
		this.m_datasource = request.getParameter("x");
		this.m_filter = request.getParameter("f");
		this.m_target = request.getParameter("t");
		this.m_labelTarget = request.getParameter("tt");
		this.m_label = request.getParameter("l");
		this.m_value = request.getParameter("v");
		this.m_labelOk = request.getParameter("lo");
		this.m_valueOk = request.getParameter("vo");
		this.m_multiple = StringUtil.getValueBool(request.getParameter("m"), false);
		this.m_delimiter = StringUtil.convertToString(request.getParameter("d"), this.m_delimiter);
		this.m_branchSelectable = StringUtil.getValueBool(request.getParameter("s"), false);
		Enumeration<String> enums = request.getParameterNames();
		if (enums != null) {
			String name = null;
			boolean isComplement = false;
			while (enums.hasMoreElements()) {
				name = enums.nextElement();
				isComplement = true;
				for (int i = 0; i < BuiltInParameters.length; i++) {
					if (BuiltInParameters[i].equalsIgnoreCase(name)) {
						isComplement = false;
						break;
					}
				}// for end
				if (isComplement) {
					if (this.m_complements == null) this.m_complements = new HashMap<String, String>();
					this.m_complements.put(name, request.getParameter(name));
				}// if end
			}// while end
		}// if end
		return this;
	}

	protected static final String JsonPropNames[] = { "datasource", "filter", "target", "multiple", "delimiter", "valueOk", "value", "labelOkId", "labelOk", "label", "branchSelectable" };

	/**
	 * 从json字符串中解析并返回对应的{@link SelectorForm}对象。
	 * 
	 * <p>
	 * json字符串形如：“{datasource:'p://user',target:'{字段名}',label:'cn',value:'fn',multiple:true,labelOk:functionPtr,...}”。<br/>
	 * 其中datasource为必须，target不提供时默认会设置为字段名，其余属性可选。
	 * </p>
	 * 
	 * @param json
	 * @return
	 */
	public static SelectorForm fromJson(String json) {
		if (json == null || json.length() == 0) return null;
		char[] cs = json.toCharArray();
		StringBuilder str = new StringBuilder(new String(cs));
		SelectorForm x = new SelectorForm();
		x.m_complements = new HashMap<String, String>();
		int pos = 0;
		char c = 0;
		int l = cs.length;
		String v = null;
		String s = null;
		for (int i = 0; i < JsonPropNames.length; i++) {
			s = JsonPropNames[i];
			pos = str.indexOf(s);
			if (pos < 0) continue;
			v = null;
			for (int j = pos + s.length(); j < l; j++) {
				c = str.charAt(j);
				if (c == ' ' || c == '\t') continue;
				if (c == ':' && j < (l - 1)) { // 冒号
					for (int m = j + 1; m < l; m++) {
						c = str.charAt(m);
						if (c == ' ' || c == '\t') continue;
						if (c == '\'' || c == '"') { // 在单引号或双引号中的字符串值
							for (int n = m + 1; n < l; n++) {
								c = str.charAt(n);
								if ((c == '\'' || c == '"') && n > 0 && str.charAt(n - 1) != '\\') {
									v = str.substring(m + 1, n);
									str.delete(pos, n + 1);
									break;
								}
							}
						} else { // 不在单引号或双引号中的字符串值
							for (int n = m + 1; n < l; n++) {
								c = str.charAt(n);
								if (c == ',' || c == ' ' || c == '\t' || c == '}') {
									v = str.substring(m, n);
									str.delete(pos, n + 1);
									break;
								}
							}
						}// else end
						if (v != null) break;
					}// for m
				}// 冒号
				if (v != null) break;
			}// for j
			if (v != null) {
				switch (i) { // "datasource", "filter", "target", "multiple", "delimiter", "valueOk", "value", "labelOkId", "labelOk", "label", "branchSelectable"
				case 0:
					x.setDatasource(v);
					break;
				case 1:
					x.setFilter(v);
					break;
				case 2:
					x.setTarget(v);
					break;
				case 3:
					x.setMultiple(StringUtil.getValueBool(v, false));
					break;
				case 4:
					x.setDelimiter(v);
					break;
				case 5:
					x.setValueOk(v);
					break;
				case 6:
					x.setValue(v);
					break;
				case 7:
					x.setLabelTarget(v);
					break;
				case 8:
					x.setLabelOk(v);
					break;
				case 9:
					x.setLabel(v);
					break;
				case 10:
					x.setBranchSelectable(StringUtil.getValueBool(v, false));
					break;
				default:
					break;
				}
			}
		}

		return x;
	}

	// public static void main(String[] args) {
	// String json = "{datasource:'v://viewAllOfGoods',value:'cn',label:'1',labelOk:setSelResult,dd:1}";
	// SelectorForm x = SelectorForm.fromJson(json);
	// System.out.println(x.toJson());
	// }

	/**
	 * 重载toJson：输出JSON对象字符串。
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	public String toJson() {
		StringBuffer sb = new StringBuffer();
		boolean isObjDS = (m_datasource != null && m_datasource.length() > 0 && m_datasource.indexOf(TreeViewProvider.PREFIX_DELIMITER) < 0);
		sb.append("datasource:").append(isObjDS ? "" : "'").append(this.m_datasource).append(isObjDS ? "" : "'");
		sb.append(",").append("multiple:").append(this.m_multiple ? "true" : "false");
		sb.append(",").append("delimiter:").append("'").append(this.m_delimiter).append("'");
		if (this.m_filter != null && this.m_filter.length() > 0) sb.append(",").append("filter:").append("'").append(this.m_filter).append("'");
		if (this.m_target != null && this.m_target.length() > 0) sb.append(",").append("target:").append("'").append(this.m_target).append("'");
		if (this.m_labelTarget != null && this.m_labelTarget.length() > 0) sb.append(",").append("labelTarget:").append("'").append(this.m_labelTarget).append("'");
		if (this.m_label != null && this.m_label.length() > 0) sb.append(",").append("label:").append("'").append(this.m_label).append("'");
		if (this.m_value != null && this.m_value.length() > 0) sb.append(",").append("value:").append("'").append(this.m_value).append("'");
		if (this.m_labelOk != null && this.m_labelOk.length() > 0) sb.append(",").append("labelOk:").append(this.m_labelOk);
		if (this.m_valueOk != null && this.m_valueOk.length() > 0) sb.append(",").append("valueOk:").append(this.m_valueOk);
		sb.append(",").append("branchSelectable:").append(this.m_branchSelectable ? "true" : "false");
		if (this.m_complements != null && this.m_complements.size() > 0) {
			sb.append(",").append("complements:{");
			int idx = 0;
			for (String key : this.m_complements.keySet()) {
				String val = this.m_complements.get(key);
				if (val == null || val.length() == 0) continue;
				sb.append(idx > 0 ? "," : "").append(key).append(":").append("'").append(StringUtil.encode4Json(val)).append("'");
				idx++;
			}
			sb.append("}");
		}
		return sb.toString();
	}
}

