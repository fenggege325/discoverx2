/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.selector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.view.ViewEntry;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.bll.view.ViewQueryProvider;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.tree.StringComparator;
import com.tansuosoft.discoverx.util.tree.Tree;
import com.tansuosoft.discoverx.util.tree.TreeBuilder;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.receiver.ViewReceiver;
import com.tansuosoft.discoverx.web.ui.view.ViewFilterParser;

/**
 * 输出视图内容选择框TreeView数据源的{@link TreeViewProvider}实现类。
 * 
 * <p>
 * 绑定的{@link SelectorForm}的属性格式说明：<br/>
 * <ul>
 * <li>{@link SelectorForm#getDatasource()}的格式为：v://{视图UNID|视图别名}</li>
 * <li>{@link SelectorForm#getFilter()}为符合格式（参考：{@link com.tansuosoft.discoverx.web.ui.view.ViewFilterParser}中的格式说明）的视图过滤信息。</li>
 * <li>{@link SelectorForm#getLabel()}的格式为获取TreeView中label结果的视图列序号（从1开始，0表示UNID为结果），默认为1。</li>
 * <li>{@link SelectorForm#getValue()}的格式为获取TreeView中value结果的视图列序号（从1开始，0表示UNID为结果），如“0”、“2”等，默认为资源UNID。</li>
 * </ul>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewTreeViewProvider extends TreeViewProvider {
	private View m_view = null;
	private int m_labelIndex = 0;
	private int m_valueIndex = 0;
	private int idx = 0;
	private static final String NOCATEGORY = "最最最最最最最最最最";
	private static final String NOCATEGORYTITLE = "[无分类]";
	private static final char MULTIPLE_CATEGORY_DELIMITER = ' ';
	private static final char MULTIPLE_LEVEL_CATEGORY_DELIMITER = '\\';
	private static final String MULTIPLE_LEVEL_CATEGORY_DELIMITER_TEXT = new String(new char[] { MULTIPLE_LEVEL_CATEGORY_DELIMITER });
	protected static final String DCPH = ViewReceiver.DCPH;
	private final static ResourceContext RC = ResourceContext.getInstance();
	private final static String APPDIR = "application";
	private int m_categoryCount = 0;
	private Map<Integer, String> m_categoryParent = new HashMap<Integer, String>();

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		String viewId = StringUtil.stringRight(this.selectorForm.getDatasource(), PREFIX_DELIMITER);
		if (viewId == null || viewId.length() == 0) throw new RuntimeException("未提供有效视图信息！");
		String viewUnid = (viewId.startsWith("view") ? ResourceAliasContext.getInstance().getUNIDByAlias(View.class, viewId) : viewId);
		m_view = ResourceContext.getInstance().getResource(viewUnid, View.class);
		if (viewUnid != null && viewUnid.length() > 0) m_view = ResourceContext.getInstance().getResource(viewUnid, View.class);
		if (m_view == null) throw new RuntimeException("找不到“" + viewId + "”对应的视图！");
		m_title = String.format("从%1$s中选择", m_view.getName());
		this.m_labelIndex = StringUtil.getValueInt(this.selectorForm.getLabel(), 1);
		this.m_valueIndex = StringUtil.getValueInt(this.selectorForm.getValue(), 0);
		ViewQuery viewQuery = ViewQueryProvider.getViewQuery(m_view, jspContext.getUserSession(), null);

		// 额外搜索条件
		String f = this.selectorForm.getFilter();
		if (f != null && f.length() > 0) {
			ViewFilterParser viewFilterParser = new ViewFilterParser(f, m_view);
			List<ViewCondition> vcs = viewFilterParser.parse();
			if (vcs != null) {
				for (ViewCondition x : vcs) {
					viewQuery.appendCondition(x);
				}
			}
		}
		final boolean isDBView = (m_view.getViewType() != ViewType.XMLResourceView);
		final TreeSet<String> categories = new TreeSet<String>(new StringComparator());
		final Map<String, List<Integer>> mapOfCategoryAndIdx = new HashMap<String, List<Integer>>(1000);
		final Map<Integer, String> mapOfIdxAndTV = new TreeMap<Integer, String>();
		final TreeBuilder treeBuilder = new TreeBuilder();
		final List<ViewColumn> cols = m_view.getColumns();
		if (isDBView) {
			for (int i = 0; i < cols.size(); i++) {
				if (cols.get(i) != null && cols.get(i).getCategorized()) {
					m_categoryParent.put(i, StringUtil.getValueString(cols.get(i).getTitle(), "按第" + i + "列分类"));
					m_categoryCount++;
				}
			}
			if (m_categoryCount > 1) categories.addAll(m_categoryParent.values());
		}
		ViewEntryCallback callback = new ViewEntryCallback() {
			@Override
			public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
				String unid = StringUtil.getValueString(viewEntry.getUnid(), null);
				List<String> vals = viewEntry.getValues();
				if (vals == null) return;
				String label = vals.get(m_labelIndex - 1);
				String value = (m_valueIndex == 0 ? unid : vals.get(m_valueIndex - 1));
				if (value == null) value = label;
				StringBuilder sb = new StringBuilder();
				sb.append("{");
				sb.append("id:").append("'ve_").append(idx).append("'");
				sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(label)).append("'");
				sb.append(",").append("value:").append("'").append(StringUtil.encode4Json(value)).append("'");
				sb.append(",").append("data:{");

				if (!isDBView) processCategory4XmlResourceView(idx, unid);

				ViewColumn col = null;
				int validx = 0;
				for (int i = 0; i < vals.size(); i++) {
					col = cols.get(i);
					if (col == null) continue;
					if (isDBView && col.getCategorized()) processCategory4DBView(idx, i, vals.get(i));
					if (i == (m_valueIndex - 1) || i == (m_labelIndex - 1)) continue;
					String colName = col.getColumnName();

					if (colName == null || colName.length() == 0 || DCPH.equalsIgnoreCase(colName)) colName = col.getColumnAlias();
					if (DCPH.equalsIgnoreCase(colName)) colName = "col_" + i;
					if (validx > 0) sb.append(",");
					sb.append("\r\n'").append(colName).append("':'").append(StringUtil.encode4Json(vals.get(i))).append("'");
					validx++;
				}

				sb.append("}"); // data end
				sb.append("}"); // tv end
				mapOfIdxAndTV.put(idx, sb.toString());
				idx++;
			}

			@Override
			public Object getResult() {
				boolean hasCategory = false;
				if (categories.size() > 1) {
					hasCategory = true;
					for (String c : categories) {
						treeBuilder.add(new Tree(c));
					}
				}
				StringBuilder prelude = new StringBuilder();
				prelude.append("id:").append("'tvr_").append(m_view.getAlias()).append("'");
				prelude.append(",").append("label:").append("'").append(StringUtil.encode4Json(m_view.getName())).append("'");
				prelude.append(",").append("children:").append("[\r\n");
				StringBuilder sb = new StringBuilder();
				Tree[] trees = treeBuilder.values();
				if (hasCategory && trees.length > 1) {
					if (trees != null && trees.length > 0) {
						int idx = 0;
						String tv = null;
						for (Tree t : trees) {
							tv = renderCategory(t);
							if (tv == null || tv.isEmpty()) continue;
							if (idx > 0) sb.append(",");
							sb.append(tv);
							sb.append("\r\n");
							idx++;
						}
					}
				} else { // 无分类
					String tv = null;
					for (int idx : mapOfIdxAndTV.keySet()) {
						tv = mapOfIdxAndTV.get(idx);
						if (sb.length() > 0) sb.append(",");
						sb.append(tv);
						sb.append("\r\n");
					}
				}
				if (sb.length() > 0) {
					sb.insert(0, prelude.toString());
					sb.append("]");
				} else {
					throw new RuntimeException("暂无可选条目！");
				}
				return sb.toString();
			}

			/**
			 * 输出分类树。
			 * 
			 * @param t
			 * @return
			 */
			protected String renderCategory(Tree t) {
				List<Integer> idxList = mapOfCategoryAndIdx.get(t.getValue());
				List<Tree> children = t.getChildren();
				if ((idxList == null || idxList.isEmpty()) && (children == null || children.isEmpty())) return null;

				StringBuilder sb = new StringBuilder();
				sb.append("{"); // treeview
				sb.append("id:").append("'tvi_c_").append(t.hashCode()).append("'");
				String[] vals = t.getValues();
				String val = StringUtil.encode4Json(vals[vals.length - 1]);
				if (NOCATEGORY.equals(val)) val = NOCATEGORYTITLE;
				sb.append(",").append("label:").append("'").append(val).append("'");
				sb.append(",").append("desc:").append("'").append(val).append("'");

				// 包含的应用
				sb.append(",").append("children:").append("[\r\n");
				int count = 0;
				String tv = null;
				if (children != null && children.size() > 0) {
					for (int i = 0; i < children.size(); i++) {
						tv = renderCategory(children.get(i));
						if (tv == null) continue;
						sb.append(count > 0 ? "," : "").append("\r\n");
						sb.append(tv);
						count++;
					}
				}
				if (idxList != null) {
					for (int idx : idxList) {
						tv = mapOfIdxAndTV.get(idx);
						if (tv == null) continue;
						if (count > 0) sb.append(",");
						sb.append(tv);
						count++;
					}
				}
				sb.append("]"); // children
				sb.append("}"); // treeview
				return sb.toString();
			}

			protected List<String> processCategory4DBView(int idx, int colIdx, String val) {
				if (val == null || val.isEmpty()) return null;
				List<String> result = parseCategory(val);
				if (m_categoryCount <= 1) {
					categories.addAll(result);
					for (String s : result) {
						putCategoryAndIdx(s, idx);
					}
				} else {
					String p = m_categoryParent.get(colIdx);
					result = parseCategory(val);
					List<String> newresult = new ArrayList<String>(result.size());
					String fn = null;
					for (String s : result) {
						fn = p + MULTIPLE_LEVEL_CATEGORY_DELIMITER_TEXT + s;
						newresult.add(fn);
						categories.add(fn);
						putCategoryAndIdx(fn, idx);
					}
					return newresult;
				}
				return result;
			}

			protected List<String> processCategory4XmlResourceView(int idx, String unid) {
				if (unid == null || unid.isEmpty()) return null;
				List<String> result = new ArrayList<String>();

				String calcCategory = "";
				final String tbl = m_view.getTable();
				boolean isApp = tbl.equals("application");
				boolean appAsPdirFlag = (tbl.equals("form") || tbl.equals("view") || tbl.equals("workflow") || isApp);
				Resource r = RC.getResource(unid, tbl);
				Resource rootParent = null;
				if (appAsPdirFlag && r != null) { // 如果是表单、视图、流程、应用程序等，先从其父unid取默认具有的分类信息。
					Resource p = RC.getResource(r.getPUNID(), APPDIR);
					StringBuilder sbc = new StringBuilder();
					while (p != null) {
						if (isApp) rootParent = p;
						if (sbc.length() > 0) sbc.insert(0, MULTIPLE_LEVEL_CATEGORY_DELIMITER_TEXT);
						sbc.insert(0, p.getName());
						p = RC.getResource(p.getPUNID(), APPDIR);
					}
					calcCategory = sbc.toString();
				}

				String category = (rootParent != null ? rootParent.getCategory() : r.getCategory());
				if (StringUtil.isBlank(category) || StringUtil.isUNID(category)) { // 如果category中保存的是空值或unid那么尝试找unid所对应的应用名(即资源所属应用的名称)作为分类。
					r = RC.getResource(category, APPDIR);
					if (r == null) {
						if (calcCategory.isEmpty()) category = NOCATEGORY;
						else category = calcCategory;
					} else {
						if (calcCategory.isEmpty()) category = r.getName();
						else category = r.getName() + MULTIPLE_LEVEL_CATEGORY_DELIMITER_TEXT + calcCategory;
					}
					result.add(category);
					putCategoryAndIdx(category, idx);
				} else { // 否则直接使用有效的分类
					result = parseCategory(category);
					List<String> newresult = new ArrayList<String>(result.size());
					String fn = null;
					if (calcCategory.length() > 0) calcCategory = MULTIPLE_LEVEL_CATEGORY_DELIMITER_TEXT + calcCategory;
					for (String s : result) {
						fn = s + calcCategory;
						newresult.add(fn);
						categories.add(fn);
						putCategoryAndIdx(fn, idx);
					}
					return newresult;
				}
				categories.add(category);
				return result;
			}

			protected void putCategoryAndIdx(String category, int idx) {
				List<Integer> list = mapOfCategoryAndIdx.get(category);
				if (list == null) {
					list = new ArrayList<Integer>();
					mapOfCategoryAndIdx.put(category, list);
				}
				list.add(idx);
			}
		};

		viewQuery.addViewEntryCallback(callback);
		viewQuery.getViewEntries();

		m_categoryParent.clear();
		m_categoryParent = null;
		return callback.getResult().toString();
	}

	/**
	 * 将空格分隔的多个分类项解析为分类项列表并返回。
	 * 
	 * @param raw
	 * @return List&lt;String&gt;
	 */
	protected static List<String> parseCategory(String raw) {
		List<String> result = new ArrayList<String>();
		if (raw == null || raw.isEmpty()) return result;
		String tmp = raw.replace('/', '\\');
		String[] arr = StringUtil.splitString(tmp, MULTIPLE_CATEGORY_DELIMITER);
		if (arr == null) return result;
		for (String s : arr) {
			if (s != null && s.length() > 0) result.add(s);
		}
		return result;
	}
}

