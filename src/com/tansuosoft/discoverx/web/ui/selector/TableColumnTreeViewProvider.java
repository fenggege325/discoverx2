/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.selector;

import java.util.List;

import com.tansuosoft.discoverx.bll.view.ConfigurableColumn;
import com.tansuosoft.discoverx.bll.view.ConfigurableTable;
import com.tansuosoft.discoverx.bll.view.TableColumnProvider;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 输出视图配置时选择表和字段信息对应的TreeView数据源的{@link TreeViewProvider}实现类。
 * 
 * <p>
 * 绑定的{@link SelectorForm}的属性格式说明：<br/>
 * <ul>
 * <li>{@link SelectorForm#getDatasource()}的格式为：vm://{视图UNID}，如果{视图UNID}省略，默认为空白文档视图。</li>
 * <li>{@link SelectorForm#getFilter()}{视图UNID}省略时，可以通过此方法提供额外的表单别名用来提供额外的待选字段。</li>
 * <li>{@link SelectorForm#getLabel()}被忽略。</li>
 * <li>{@link SelectorForm#getValue()}被忽略。</li>
 * </ul>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class TableColumnTreeViewProvider extends TreeViewProvider {
	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		this.m_title = "选择表名和字段名";
		String ds = this.selectorForm.getDatasource();
		if (ds == null || ds.length() == 0) throw new RuntimeException("没有提供有效数据源！");
		View view = ResourceContext.getInstance().getResource(StringUtil.stringRight(ds, PREFIX_DELIMITER), View.class);
		ViewType viewType = (view == null ? ViewType.DocumentView : view.getViewType());
		switch (viewType.getIntValue()) {
		case 0: // DocumentView
			break;
		case 1:
		case 2:
		default:
			throw new RuntimeException("不支持非文档视图的字段选择！");
		}
		if (view == null) {
			view = new View();
			view.setViewType(viewType);
		}
		String filter = this.selectorForm.getFilter();
		String table = view.getTable();
		view.setTable(filter);
		List<ConfigurableTable> configurableTables = TableColumnProvider.getTables(view);
		view.setTable(table);
		if (configurableTables == null || configurableTables.isEmpty()) throw new RuntimeException("没有配置有效的视图表和字段信息！");

		sb.append("id:").append("'tv_viewtablecolumns'");
		sb.append(",").append("label:'选择表字段'");
		sb.append(",").append("children:").append("[");

		StringBuilder sbsub = new StringBuilder();
		for (ConfigurableTable x : configurableTables) {
			buildTreeView(x, sbsub);
		}
		if (sbsub.length() == 0) throw new RuntimeException("当前视图没有可供配置时选择的表和字段信息！");

		sb.append(sbsub.toString());
		sb.append("]");

		return sb.toString();
	}

	/**
	 * 构造TreeView。
	 * 
	 * @param x
	 * @param sb
	 */
	private void buildTreeView(ConfigurableTable x, StringBuilder sb) {
		if (x == null) return;
		List<ConfigurableColumn> tableColumns = x.getTableColumns();
		if (tableColumns == null || tableColumns.isEmpty()) return;
		sb.append(sb.length() > 0 ? ",\r\n" : "\r\n").append("{");
		sb.append("id:").append("'tv_").append(StringUtil.encode4Json(x.getTableName())).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(x.getTableTitle())).append("'");
		sb.append(",").append("value:").append("'").append(StringUtil.encode4Json(x.getTableName())).append("'");
		sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(x.getTableTitle())).append("'");
		sb.append(",").append("selectable:").append("false");

		sb.append(",").append("children:").append("[");
		int idx = 0;
		for (ConfigurableColumn y : tableColumns) {
			sb.append(idx == 0 ? "\r\n" : ",\r\n").append("{");
			sb.append("id:").append("'tv_").append(StringUtil.encode4Json(y.getColumnName())).append("'");
			sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(y.getColumnTitle())).append("'");
			sb.append(",").append("value:").append("'").append(StringUtil.encode4Json(y.getColumnName())).append("'");
			sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(y.getColumnTitle())).append("'");
			if (!StringUtil.isBlank(y.getDictionary())) sb.append(",data:{").append("dictionary:").append("'").append(StringUtil.encode4Json(y.getDictionary())).append("'}");
			sb.append("}");
			idx++;
		}
		sb.append("]");
		sb.append("}");
	}
}

