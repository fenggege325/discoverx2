/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.selector;

import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出选择框所使用的js端TreeView对象数据源的基类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class TreeViewProvider implements HtmlRender {
	/**
	 * 数据源前缀分隔符："://"。
	 */
	public static final String PREFIX_DELIMITER = "://";
	/**
	 * 表示选择参与者的数据源信息前缀："p://"。
	 */
	public static final String PARTICIPANT_PREFIX = "p" + PREFIX_DELIMITER;
	/**
	 * 表示选择视图数据的数据源信息前缀："v://"。
	 */
	public static final String VIEW_PREFIX = "v" + PREFIX_DELIMITER;
	/**
	 * 表示选择流程参与者的数据源信息前缀："wf://"。
	 */
	public static final String WORKFLOW_PREFIX = "wf" + PREFIX_DELIMITER;

	/**
	 * 绑定的{@link SelectorForm}对象。
	 */
	protected SelectorForm selectorForm = null;

	/**
	 * 缺省构造器。
	 */
	protected TreeViewProvider() {
	}

	/**
	 * 根据{@link SelectorForm}获取对应的{@link TreeViewProvider}对象。
	 * 
	 * @param selectorForm
	 * @return
	 */
	public static TreeViewProvider getTreeViewProvider(SelectorForm selectorForm) {
		if (selectorForm == null) return null;
		TreeViewProvider result = null;
		String ds = StringUtil.getValueString(selectorForm.getDatasource(), "");
		if (ds.startsWith(PARTICIPANT_PREFIX)) {
			result = new ParticipantTreeViewProvider();
		} else if (ds.startsWith(VIEW_PREFIX)) {
			result = new ViewTreeViewProvider();
		} else if (ds.startsWith(WORKFLOW_PREFIX)) {
			result = new WorkflowTreeViewProvider();
		} else if (ds.startsWith("vm" + PREFIX_DELIMITER)) {
			result = new TableColumnTreeViewProvider();
		} else {
			result = new ResourceTreeViewProvider();
		}
		result.selectorForm = selectorForm;
		return result;
	}

	protected String m_title = "选择框"; // 选择框标题

	/**
	 * 获取选择框标题。
	 * 
	 * <p>
	 * 在{@link TreeViewProvider#renderForSelector(JSPContext)}调用之后才能获取到值。
	 * </p>
	 * 
	 * @return
	 */
	public String getTitle() {
		return this.m_title;
	}

	/**
	 * 输出可供选择框使用的js端TreeView对象代码。
	 * 
	 * @param jspContext
	 * @return
	 */
	public String renderForSelector(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("var tvroot=createTreeView({");
			sb.append(this.render(jspContext));
			sb.append("});");
			sb.append("\r\ntvroot.nodeType=0;");
		} catch (Exception ex) {
			boolean isNullPointer = (ex instanceof NullPointerException);
			if (isNullPointer) FileLogger.error(ex);
			sb = new StringBuilder();
			sb.append("var tvroot={error:true,label:'").append(isNullPointer ? "出现空指针异常，请联系管理员！" : StringUtil.encode4Json(ex.getMessage())).append("'};");
		}
		return sb.toString();
	}
}

