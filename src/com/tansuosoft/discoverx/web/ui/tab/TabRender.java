/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.tab;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.tansuosoft.discoverx.model.Tab;
import com.tansuosoft.discoverx.model.TabShow;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * {@link Tab}表示的json标签页json对象呈现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class TabRender implements HtmlRender {
	private List<Tab> m_tabs = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param tabs
	 */
	public TabRender(List<Tab> tabs) {
		this.m_tabs = tabs;
	}

	/**
	 * 重载render：输出工具栏(Toolbar)json对象内容。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (m_tabs == null) return "";
		StringBuilder sb = new StringBuilder();
		Comparator<Tab> c = new Comparator<Tab>() {
			@Override
			public int compare(Tab o1, Tab o2) {
				int sortDiff = o1.getSort() - o2.getSort();
				int result = (sortDiff > 0 ? 1 : (sortDiff == 0 ? 0 : -1));
				if (result == 0) { return o1.getTitle().compareTo(o2.getTitle()); }
				return result;
			}
		};
		Collections.sort(m_tabs, c);
		int idx = 0;
		String id = null;
		String onSelect = null;
		String onLeave = null;
		HashMap<String, String> map = new HashMap<String, String>(m_tabs.size());
		for (Tab x : m_tabs) {
			if (x == null) continue;
			sb.append(sb.length() == 0 ? "" : ",");
			sb.append("{");
			id = x.getId();
			if (map.containsKey(id)) {
				id = String.format("%1$s%2$s", id, idx);
			}
			map.put(id, null);
			sb.append("id:").append("'").append("tabi_").append(id).append("'");
			sb.append(",").append("label:").append("'").append(x.getTitle()).append("'");
			sb.append(",").append("desc:").append("'").append(x.getDescription()).append("'");
			if (x.getTabShow() == TabShow.ShowAll) {
				sb.append(",").append("selected:").append("true");
			} else if (x.getTabShow() == TabShow.ShowNone) {
				sb.append(",").append("visible:").append("false");
			}
			onSelect = x.getOnselect();
			if (onSelect != null && onSelect.trim().length() > 0) {
				sb.append(",").append("onselect:").append(onSelect);
			}
			onLeave = x.getOnleave();
			if (onLeave != null && onLeave.trim().length() > 0) {
				sb.append(",").append("onleave:").append(onLeave);
			}
			sb.append("}");
			idx++;
		}
		return sb.toString();
	}
}

