/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.toolbar;

import java.util.List;

import com.tansuosoft.discoverx.bll.function.BlankOperation;
import com.tansuosoft.discoverx.bll.function.OperationParser;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.document.DocumentDisplay;

/**
 * 工具栏呈现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ToolbarRender implements HtmlRender {
	private OperationProvider m_operationProvider = null;
	private Session session = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param operationProvider
	 */
	public ToolbarRender(OperationProvider operationProvider) {
		this.m_operationProvider = operationProvider;
	}

	/**
	 * 重载render：输出工具栏(Toolbar)json对象内容。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (m_operationProvider == null) return null;
		List<Operation> operations = m_operationProvider.provide(jspContext);
		session = jspContext.getUserSession();
		StringBuilder sb = new StringBuilder();
		String id = m_operationProvider.getClass().getSimpleName().toLowerCase();
		String tbbidPrefix = "tbb_" + id + "_";
		sb.append("id:").append("'tb_").append(id).append("'");
		sb.append(",").append("iconPath:").append("'").append(jspContext.getPathContext().getCommonIconsPath()).append("'");
		sb.append(",").append("data:").append("{");
		sb.append("}");
		sb.append(",").append("buttons:");
		sb.append("[");

		if (operations != null && !operations.isEmpty()) {
			int idx = 0;
			OperationParser operationParser = null;
			String icon = null;
			String js = null;
			OperationParser operationParserInvisible = jspContext.getLastOperation(false); // 当前请求的返回的操作（通常是带操作向导的操作）在工具栏按钮栏中不显示。
			if (operationParserInvisible == null) {
				// 如果操作有显示操作界面，那么在document_wizard.jsp中会设置名为“documentDisplay”的参数值，其值类型为com.tansuosoft.discoverx.web.ui.document.DocumentDisplay对象
				// 主要是通过这个documentDisplay对象获取一个虚拟的不显示的操作，用于获取其相关信息。
				Object obj = jspContext.getRequest().getAttribute("documentDisplay");
				DocumentDisplay documentDisplay = null;
				if (obj != null && obj instanceof DocumentDisplay) documentDisplay = (DocumentDisplay) obj;
				if (documentDisplay != null) operationParserInvisible = documentDisplay.getOperationParser();
			}
			if (operationParserInvisible != null) {
				Operation opri = new Operation();
				opri.setIcon(" ");
				opri.setTitle(operationParserInvisible.getFunction().getName());
				opri.setExpression(operationParserInvisible.getOperation().getExpression());
				operations.add(opri);
			}
			int lastIdx = operations.size() - 1;
			String title = null;
			for (Operation x : operations) {
				if (x == null) continue;
				operationParser = null;
				boolean isInvisible = (idx == lastIdx && operationParserInvisible != null);
				try {
					operationParser = (isInvisible ? operationParserInvisible : new OperationParser(x.getExpression(), session));
				} catch (Exception ex) {
					FileLogger.debug("操作“%1$s”无法解析：%2$s", x.getExpression(), ex.getMessage());
					operationParser = null;
				}
				if (operationParser == null) {
					idx++;
					continue;
				}
				if (!isInvisible) operationParser.setClientId(tbbidPrefix + idx);
				sb.append(idx == 0 ? "" : ",");
				sb.append("{");
				sb.append("id:").append("'").append(operationParser.getClientId()).append("'");
				title = x.getTitle();
				if (title == null || title.length() == 0) title = operationParser.getFunction().getName();
				sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(title)).append("'");
				sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(operationParser.getOperation() instanceof BlankOperation ? title : operationParser.getFunction().getDescription())).append("'");
				// String nextPath = getOperationPath(operationParser, null);
				// if (nextPath != null && nextPath.length() > 0) sb.append(",").append("nextPath:").append("'").append(StringUtil.encode4Json(nextPath)).append("'");
				icon = x.getIcon();
				if (icon == null || icon.length() == 0) icon = operationParser.getOperation().getParameterString("icon", null);
				if (icon == null || icon.length() == 0) icon = "icon_plugin.gif";
				js = operationParser.getJSFunction("operation.execute();");
				sb.append(",").append("icon:").append("'").append(icon).append("'");
				sb.append(",").append("onclick:").append("function(){").append(js).append("}");
				if (isInvisible) sb.append(",").append("visible:").append("false");
				sb.append(",").append("data:").append("{");
				sb.append("expression:").append("'").append(StringUtil.encode4Json(x.getExpression())).append("'");
				sb.append("}");
				sb.append("}\r\n");
				idx++;
			}
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * 获取操作链路径。
	 * 
	 * @param parser
	 * @param parent
	 * @return 返回操作链对应{@link com.tansuosoft.discoverx.model.Function}的unid，多个以反斜杠分隔。
	 */
	protected String getOperationPath(OperationParser parser, String parent) {
		if (parser == null) return null;
		String result = String.format("%1$s%2$s%3$s", (parent == null ? "" : parent), (parent == null || parent.length() == 0 ? "" : "\\"), parser.getFunction().getUNID());
		OperationParser next = null;
		String nextopr = null;
		try {
			nextopr = parser.getNextOperation(null);
			if (nextopr != null && nextopr.length() > 0) {
				next = new OperationParser(nextopr, session);
				if (next != null) return getOperationPath(next, result);
			}
		} catch (Exception ex) {
			FileLogger.debug("操作“%1$s”无法解析：%2$s", nextopr, ex.getMessage());
		}
		return result;
	}
}

