/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.toolbar;

import java.util.Comparator;
import java.util.List;

import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.OperationComparator;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 提供操作按钮({@link com.tansuosoft.discoverx.model.Operation})集合的基类。
 * 
 * <p>
 * 用于为相关web页面提供操作按钮列表。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class OperationProvider {
	/**
	 * 缺省构造器。
	 */
	public OperationProvider() {
	}

	private boolean m_showClose = true; // 输出的操作列表中第一个操作是否固定为关闭，默认为true。

	/**
	 * 返回是否输出关闭按钮。
	 * 
	 * <p>
	 * 默认为true，关闭按钮总是第一个按钮。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getShowClose() {
		return this.m_showClose;
	}

	/**
	 * 设置是否输出关闭按钮。
	 * 
	 * @param showClose boolean
	 */
	public void setShowClose(boolean showClose) {
		this.m_showClose = showClose;
	}

	/**
	 * 根据参数构造一个{@link Operation}实例。
	 * 
	 * @param expression String 操作表达式，必须。
	 * @param sort int 排序号，可选。
	 * @param title String 操作标题，必须。
	 * @param visibleExpression String 显示条件表达式，可选。
	 * @param icon String 图标文件名，可选。
	 * @return Operation 返回构造好的操作。
	 */
	protected static Operation createOperation(String expression, int sort, String title, String visibleExpression, String icon) {
		Operation or = new Operation();
		or.setExpression(expression);
		or.setVisibleExpression(visibleExpression);
		or.setSort(sort);
		or.setTitle(title);
		or.setIcon(icon);
		return or;
	}

	/**
	 * 获取关闭按钮对应的{@link Operation}对象。
	 * 
	 * @return Operation
	 */
	protected Operation getClose() {
		Operation operation = new Operation();
		operation.setTitle("关闭");
		operation.setExpression("@Close()");
		operation.setSort(0);
		operation.setIcon("icon_close.gif");
		return operation;
	}

	/**
	 * 获取帮助按钮对应的{@link Operation}对象。
	 * 
	 * @return Operation
	 */
	protected Operation getHelp() {
		Operation operation = new Operation();
		operation.setTitle("帮助");
		operation.setExpression("@Help()");
		operation.setSort(100);
		operation.setIcon("icon_help.gif");
		return operation;
	}

	/**
	 * 排序unsortedList中的{@link Operation}对象。
	 * 
	 * @param unsortedList
	 */
	protected void sort(List<Operation> unsortedList) {
		if (unsortedList == null || unsortedList.isEmpty()) return;
		Comparator<Operation> c = new OperationComparator();
		java.util.Collections.sort(unsortedList, c);
	}

	/**
	 * 检查指定的{@link Operation}对象是否能够显示。
	 * 
	 * @param operation
	 * @param jspContext
	 * @param resource
	 * @return
	 */
	protected boolean checkVisibleExpression(Operation operation, JSPContext jspContext, Resource resource) {
		if (operation == null) return false;
		String expression = operation.getVisibleExpression();
		if (expression == null || expression.trim().length() == 0) return true;
		ExpressionEvaluator evaluator = new ExpressionEvaluator(expression, jspContext.getUserSession(), resource);
		return evaluator.evalBoolean();
	}

	/**
	 * 为指定的{@link JSPContext}返回提供的操作按钮列表。
	 * 
	 * @param jspContext
	 * @return
	 */
	public abstract List<Operation> provide(JSPContext jspContext);
}

