/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.toolbar;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 资源配置操作提供类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceOperationProvider extends OperationProvider {
	private Resource m_resource = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param resource
	 */
	public ResourceOperationProvider(Resource resource) {
		this.m_resource = resource;
	}

	/**
	 * 重载provide
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.toolbar.OperationProvider#provide(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public List<Operation> provide(JSPContext jspContext) {
		if (this.m_resource == null) return null;
		List<Operation> result = new ArrayList<Operation>();
		result.add(this.getClose());
		boolean canSaveAndDel = true;
		if (OrganizationsContext.getInstance().isInTenantContext()) {
			if ((m_resource instanceof Group || m_resource instanceof Role) && StringUtil.isBlank(m_resource.getPUNID())) canSaveAndDel = false;
		}
		Operation save = OperationProvider.createOperation("@Save()", 1, "保存", null, "icon_save.gif");
		result.add(save);
		if (!canSaveAndDel) result.remove(result.size() - 1);

		if (ResourceHelper.exists(this.m_resource)) {
			result.add(OperationProvider.createOperation("@Delete()", 2, "作废", null, "icon_delete.gif"));
			if (!canSaveAndDel) result.remove(result.size() - 1);
			result.add(OperationProvider.createOperation("@Export()", 3, "导出", null, "icon_export.gif"));
			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(m_resource.getClass());
			if (rd != null && rd.getCache()) {
				result.add(OperationProvider.createOperation("@ReloadResource()", 4, "重新加载", null, "icon_refresh.gif"));
			}
		}

		result.add(this.getHelp()); // 帮助
		this.sort(result);
		return result;
	}
}

