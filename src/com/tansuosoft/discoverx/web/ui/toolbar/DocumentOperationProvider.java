/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.toolbar;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.DocumentType;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 文档操作提供类。
 * 
 * @author coca@tansuosoft.cn
 */

public class DocumentOperationProvider extends OperationProvider {
	private Document m_document = null;
	private DocumentDisplayInfo m_documentDisplayInfo = null;
	private List<Operation> result = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param document
	 * @param documentDisplayInfo
	 */
	public DocumentOperationProvider(Document document, DocumentDisplayInfo documentDisplayInfo) {
		m_document = document;
		m_documentDisplayInfo = documentDisplayInfo;
	}

	/**
	 * 重载provide
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.toolbar.OperationProvider#provide(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public List<Operation> provide(JSPContext jspContext) {
		if (m_document == null) return null;
		if (result != null) return result;
		if (m_document.getType() == DocumentType.Profile) {
			result = new ArrayList<Operation>(3);
			result.add(this.getClose());
			PathContext pc = jspContext.getPathContext();
			int w = StringUtil.getValueInt(jspContext.getRequest().getParameter("width"), 735);
			String resultUrl = String.format("%1$sprofile.jsp?unid=%2$s&width=%3$d", pc.getFramePath(), m_document.getUNID(), w);
			result.add(createOperation("@Save(resulturl:=\"" + resultUrl + "\")", 1, "保存", null, "icon_save.gif"));
			result.add(createOperation("@Delete()", 2, "作废", null, "icon_delete.gif"));
			this.sort(result);
			return result;
		}
		int appCnt = m_document.getApplicationCount();
		Application app = m_document.getApplication(appCnt);
		if (app == null) return null;
		boolean isNew = (m_document.checkState(DocumentState.New));

		if (isNew) {
			result = new ArrayList<Operation>();
			result.add(this.getClose());
			result.add(createOperation("@Save()", 1, "保存", null, "icon_save.gif"));
			List<Operation> newoprs = app.getNewStateOperations();
			if (newoprs != null && newoprs.size() > 0) {
				for (Operation x : newoprs) {
					if (x == null || StringUtil.isBlank(x.getExpression())) continue;
					if (this.checkVisibleExpression(x, jspContext, m_document)) {
						result.add(x);
					}
				}
			}
			this.sort(result);
			return result;
		}

		result = new ArrayList<Operation>();
		result.add(this.getClose());
		if (!this.m_documentDisplayInfo.getReadonly()) result.add(createOperation("@Save()", 1, "保存", null, "icon_save.gif"));
		List<Operation> appoprs = app.getOperations();
		if (appoprs != null && appoprs.size() > 0) {
			for (Operation x : appoprs) {
				if (x == null || StringUtil.isBlank(x.getExpression())) continue;
				if (this.checkVisibleExpression(x, jspContext, m_document)) {
					result.add(x);
				}
			}
		}
		WorkflowRuntime wfr = null;
		Activity activity = null;
		try {
			wfr = WorkflowRuntime.getInstance(this.m_document, jspContext.getUserSession());
			activity = wfr.getCurrentActivity();
		} catch (Exception e) {
			FileLogger.debug(e.getMessage());
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		if (activity != null) {
			List<Operation> wfaoprs = activity.getOperations();
			if (wfaoprs != null && wfaoprs.size() > 0) {
				String visibleExpression = null;
				User u = Session.getUser(jspContext.getUserSession());
				int sc = u.getSecurityCode();
				Security s = this.m_document.getSecurity();
				for (Operation x : wfaoprs) {
					if (x == null || StringUtil.isBlank(x.getExpression())) continue;
					visibleExpression = x.getVisibleExpression();
					// 未提供显示表达式则按当前办理人显示
					if (visibleExpression == null || visibleExpression.trim().length() == 0) {
						if (s == null) continue;
						if (!s.checkWorkflowLevel(sc, WFDataLevel.Approver.getIntValue())) continue;
						result.add(x);
					} else { // 否则检查表达式是否为true
						if (this.checkVisibleExpression(x, jspContext, m_document)) {
							result.add(x);
						}
					}
				}// for end
			}// if end
		}// if end
		this.sort(result);
		return result;
	}
}

