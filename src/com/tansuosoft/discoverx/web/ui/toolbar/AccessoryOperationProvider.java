/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.toolbar;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;

/**
 * 附加文件操作提供类。
 * 
 * @author coca@tansuosoft.cn
 */

public class AccessoryOperationProvider extends OperationProvider {
	private Resource m_container = null;
	private ItemDisplay m_itemDisplay = null;
	private DocumentDisplayInfo m_documentDisplayInfo = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param container 附加文件的容器资源，必须。
	 * @param itemDisplay 附加文件字段呈现类，可选。
	 * @param documentDisplayInfo 文档显示选项，可选。
	 * 
	 */
	public AccessoryOperationProvider(Resource container, ItemDisplay itemDisplay, DocumentDisplayInfo documentDisplayInfo) {
		m_container = container;
		m_documentDisplayInfo = documentDisplayInfo;
		m_itemDisplay = itemDisplay;
		if (m_container == null) throw new RuntimeException("初始化参数不合法！");
	}

	/**
	 * 重载provide
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.toolbar.OperationProvider#provide(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public List<Operation> provide(JSPContext jspContext) {
		if (this.m_container == null) return null;
		List<Operation> result = null;
		boolean isReadonly = (this.m_itemDisplay == null ? (this.m_documentDisplayInfo == null ? false : this.m_documentDisplayInfo.getReadonly()) : this.m_itemDisplay.getReadonly());
		int count = 0;
		if (m_container.getAccessories() != null && m_container.getAccessories().size() > 1) {
			AccessoryType at = null;
			for (Accessory x : m_container.getAccessories()) {
				if (x == null) continue;
				at = x.getAccessoryTypeResource();
				if (at != null && !at.getVisible()) continue;
				count++;
			}
		}
		result = new ArrayList<Operation>();
		if (!isReadonly) {
			result.add(createOperation("@CallJS(js:=\"accessoryActions.add();\")", 1, "添加", null, "icon_new.gif"));
			result.add(createOperation("@CallJS(js:=\"accessoryActions.update();\")", 2, "修改", null, "icon_edit.gif"));
			result.add(createOperation("@CallJS(js:=\"accessoryActions.remove();\")", 3, "删除", null, "icon_filedel.gif"));
			result.add(createOperation("@CallJS(js:=\"accessoryActions.replace();\")", 4, "替换", null, "icon_restore.gif"));
		}
		result.add(createOperation("@CallJS(js:=\"accessoryActions.open();\")", 5, "打开", null, "icon_document.gif"));
		result.add(createOperation("@CallJS(js:=\"accessoryActions.saveas();\")", 6, "另存为", null, "icon_saveas.gif"));
		if (count > 1) {
			result.add(createOperation("@CallJS(js:=\"accessoryActions.saveall();\")", 7, "保存所有", null, "icon_save.gif"));
		}
		result.add(createOperation("@CallJS(js:=\"accessoryListView.reload();\")", 8, "刷新", null, "icon_refresh.gif"));
		result.add(this.getHelp());
		this.sort(result);
		return result;
	}
}

