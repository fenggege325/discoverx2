/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.toolbar;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 视图操作提供类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewOperationProvider extends OperationProvider {
	private View m_view = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param view
	 */
	public ViewOperationProvider(View view) {
		m_view = view;
	}

	/**
	 * 重载provide
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.toolbar.OperationProvider#provide(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public List<Operation> provide(JSPContext jspContext) {
		if (m_view == null) return null;
		List<Operation> result = new ArrayList<Operation>();
		List<Operation> oprs = m_view.getOperations();
		if (oprs != null && oprs.size() > 0) {
			String expression = null;
			for (Operation x : oprs) {
				if (x == null) continue;
				expression = x.getExpression();
				if (expression == null) continue;
				expression = expression.toLowerCase();
				if (this.checkVisibleExpression(x, jspContext, m_view)) result.add(x);
			}
		}
		this.sort(result);
		return result;
	}
}

