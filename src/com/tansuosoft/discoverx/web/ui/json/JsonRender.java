/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.json;

import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出完整json对象内容。
 * 
 * <p>
 * 用于输出：“var {jsVarName}=new {jsClassName}({jsonBodyHtmlRender.render(JSPContext)})”格式的结果。<br/>
 * 或者：“var {jsVarName}={jsonBodyHtmlRender.render(JSPContext)}”（如果jsClassName不提供）。<br/>
 * 或者：“{jsonBodyHtmlRender.render(JSPContext)}”（如果jsVarName和jsClassName不提供）。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class JsonRender implements HtmlRender {

	/**
	 * 接收提供json对象内容和json对象变量名的构造器。
	 * 
	 * @param jsonBodyHtmlRender
	 * @param jsVarName
	 * @param jsClassName
	 */
	public JsonRender(HtmlRender jsonBodyHtmlRender, String jsVarName, String jsClassName) {
		this.m_jsonBodyHtmlRender = jsonBodyHtmlRender;
		this.m_jsVarName = jsVarName;
		this.m_jsClassName = jsClassName;
	}

	private HtmlRender m_jsonBodyHtmlRender = null;
	private String m_jsVarName = null;
	private String m_jsClassName = null;

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		if (this.m_jsVarName != null && this.m_jsVarName.length() > 0) {
			sb.append("var ").append(this.m_jsVarName).append("=");
		}
		boolean foundClassName = false;
		if (this.m_jsClassName != null && this.m_jsClassName.length() > 0) {
			sb.append("new ").append(this.m_jsClassName).append("(\r\n");
			foundClassName = true;
		}
		String body = null;
		if (this.m_jsonBodyHtmlRender != null) {
			body = m_jsonBodyHtmlRender.render(jspContext);
		}
		if (body.startsWith("{") || body.startsWith("\r\n{")) {
			sb.append(body);
		} else {
			sb.append("{").append(body);
		}
		if (!body.endsWith("}") && !body.endsWith("}\r\n")) {
			sb.append("}\r\n");
		}
		if (foundClassName) sb.append(")");
		// sb.append(";");
		return sb.toString();
	}// func end

}

