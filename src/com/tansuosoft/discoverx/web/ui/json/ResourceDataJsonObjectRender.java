/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.json;

import java.util.List;

import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.ParameterValueDataType;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 为{@link Resource}输出通用json对象的data属性结果的{@link HtmlRender}实现类。
 * 
 * <p>
 * 输出的json对象的格式符合TreeView、ListView等客户端json对象要求的data格式，包含了通用的alias、额外参数（
 * parameters）等属性。<br/>
 * 格式：<br/>
 * alias:'aliasvalue'[,parameters:{name:'name1',value:'value1'[,valueType:
 * {@link ParameterValueDataType}对应某一个数字值]}]<br/>
 * “[”与“]”之间的内容表示可选。<br/>
 * 如果没有额外参数，则没有输出任何内容；如果参数值类型是文本，那么“valueType”不输出。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceDataJsonObjectRender implements HtmlRender {
	private Resource resource = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param resource
	 */
	public ResourceDataJsonObjectRender(Resource resource) {
		this.resource = resource;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		sb.append("alias:").append("'").append(StringUtil.getValueString(resource.getAlias(), "")).append("'");
		List<Parameter> params = resource.getParameters();
		if (params != null && params.size() > 0) {
			StringBuilder psb = new StringBuilder();
			for (Parameter x : params) {
				if (x == null || StringUtil.isBlank(x.getValue())) continue;
				psb.append(psb.length() == 0 ? "" : ",");
				psb.append("{");
				psb.append("name:").append("'").append(x.getName()).append("'");
				psb.append(",").append("value:").append("'").append(StringUtil.encode4Json(x.getValue())).append("'");
				if (x.getValueType() != ParameterValueDataType.Text) psb.append(",").append("valueType:").append("'").append(x.getValueType()).append("'");
				psb.append("}");
			}
			if (psb.length() > 0) sb.append(",").append("parameters:[").append(psb.toString()).append("]");
		}

		return sb.toString();
	}
}

