/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.json;

import java.util.List;

import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 输出资源树TreeView对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceTreeViewRender {
	/**
	 * 缺省构造器。
	 */
	public ResourceTreeViewRender() {
	}

	/**
	 * 输出{@link Resource}对象对应的TreeView json对象。
	 * 
	 * @param res
	 * @param sb
	 */
	protected void renderTreeView(Resource res, StringBuilder sb) {
		if (res == null) return;
		boolean rootFlag = (sb.length() == 0);
		if (!rootFlag) sb.append("{"); // treeview begin
		sb.append("id:").append("'").append(res.getUNID()).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json((res.getName()))).append("'");
		sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(StringUtil.getValueString(res.getDescription(), ""))).append("'");
		int idx = 0;
		List<Resource> list = res.getChildren();
		if (list != null) {
			sb.append(",").append("children:").append("["); // children begin
			for (Resource x : list) {
				if (x == null) continue;
				sb.append(idx == 0 ? "" : ",");
				this.renderTreeView(x, sb);
				idx++;
			}
			sb.append("]"); // children end
		}

		if (!rootFlag) sb.append("}"); // treeview end

		return;
	}
}

