/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.json;

import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 输出Json格式数据的实用工具类。
 * 
 * @author coca@tensosoft.com
 * 
 */
public final class JsonRenderHelper {
	protected static final String TRUE = "true";
	protected static final String FALSE = "false";

	private int propCount = 0;
	private boolean newLine = true;
	private boolean propWithQuot = false;
	private StringBuilder sb = null;

	/**
	 * 
	 * 构造器。
	 * 
	 * @param sb 输出Json文本的目标缓冲区，必须。
	 * @param newLine 是否在每个属性输出后输出一个新行，默认为true。
	 * @param propWithQuot 是否将属性名也用单引号包裹起来，默认为false。
	 */
	public JsonRenderHelper(StringBuilder sb, boolean newLine, boolean propWithQuot) {
		this.sb = sb;
		this.newLine = newLine;
		this.propWithQuot = propWithQuot;
	}

	/**
	 * 使用newLine和propWithQuot的默认值的构造器。。
	 * 
	 * @param sb 输出Json文本的目标缓冲区，必须。
	 */
	public JsonRenderHelper(StringBuilder sb) {
		this.sb = sb;
	}

	/**
	 * 追加文本值属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, String propValue) {
		appendProp(propName).append("'").append(StringUtil.encode4Json(propValue)).append("'");
	}

	/**
	 * 追加布尔值属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, boolean propValue) {
		appendProp(propName).append(propValue ? TRUE : FALSE);
	}

	/**
	 * 追加整数值属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, int propValue) {
		appendProp(propName).append(propValue);
	}

	/**
	 * 追加长整数值属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, long propValue) {
		appendProp(propName).append(propValue);
	}

	/**
	 * 追加单精度浮点值属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, float propValue) {
		appendProp(propName).append(propValue);
	}

	/**
	 * 追加双精度浮点值属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, double propValue) {
		appendProp(propName).append(propValue);
	}

	/**
	 * 追加文本值数组属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, String[] propValue) {
		appendProp(propName);
		if (propValue == null) sb.append("[]");
		sb.append("[");
		for (int i = 0; i < propValue.length; i++) {
			if (i > 0) {
				sb.append(",");
				if (newLine) sb.append("\r\n");
			}
			sb.append("'").append(StringUtil.encode4Json(propValue[i])).append("'");
		}
		sb.append("]");
	}

	/**
	 * 追加布尔值数组属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, boolean[] propValue) {
		appendProp(propName);
		if (propValue == null) sb.append("[]");
		sb.append("[");
		for (int i = 0; i < propValue.length; i++) {
			if (i > 0) {
				sb.append(",");
				if (newLine) sb.append("\r\n");
			}
			sb.append(propValue[i] ? TRUE : FALSE);
		}
		sb.append("]");
	}

	/**
	 * 追加整数值数组属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, int[] propValue) {
		appendProp(propName);
		if (propValue == null) sb.append("[]");
		sb.append("[");
		for (int i = 0; i < propValue.length; i++) {
			if (i > 0) {
				sb.append(",");
				if (newLine) sb.append("\r\n");
			}
			sb.append(propValue[i]);
		}
		sb.append("]");
	}

	/**
	 * 追加长整数值数组数组属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, long[] propValue) {
		appendProp(propName);
		if (propValue == null) sb.append("[]");
		sb.append("[");
		for (int i = 0; i < propValue.length; i++) {
			if (i > 0) {
				sb.append(",");
				if (newLine) sb.append("\r\n");
			}
			sb.append(propValue[i]);
		}
		sb.append("]");
	}

	/**
	 * 追加单精度浮点值数组属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, float[] propValue) {
		appendProp(propName);
		if (propValue == null) sb.append("[]");
		sb.append("[");
		for (int i = 0; i < propValue.length; i++) {
			if (i > 0) {
				sb.append(",");
				if (newLine) sb.append("\r\n");
			}
			sb.append(propValue[i]);
		}
		sb.append("]");
	}

	/**
	 * 追加双精度浮点值数组属性。
	 * 
	 * @param propName
	 * @param propValue
	 */
	public void append(String propName, double[] propValue) {
		appendProp(propName);
		if (propValue == null) sb.append("[]");
		sb.append("[");
		for (int i = 0; i < propValue.length; i++) {
			if (i > 0) {
				sb.append(",");
				if (newLine) sb.append("\r\n");
			}
			sb.append(propValue[i]);
		}
		sb.append("]");
	}

	/**
	 * 重新开始一个新Json对象的属性设置。
	 */
	public void resetProp() {
		propCount = 0;
	}

	/**
	 * 追加属性分隔符。
	 */
	protected void appendSep() {
		if (propCount > 0) {
			sb.append(",");
			if (newLine) sb.append("\r\n");
		}
	}

	/**
	 * 追加属性名。
	 * 
	 * @param propName
	 * @return StringBuilder
	 */
	protected StringBuilder appendProp(String propName) {
		appendSep();
		String q = (propWithQuot ? "'" : "");
		sb.append(q).append(propName).append(q).append(":");
		propCount++;
		return sb;
	}
}

