/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.json;

import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 为{@link Resource}输出通用json对象的{@link HtmlRender}实现类。
 * 
 * <p>
 * 输出的json对象的格式符合TreeView、ListView等客户端json对象要求的格式，包含了通用的id、lable、desc等属性。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceJsonObjectRender implements HtmlRender {
	private Resource resource = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param resourcer
	 */
	public ResourceJsonObjectRender(Resource resource) {
		this.resource = resource;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (resource == null) return "";
		StringBuilder sb = new StringBuilder();
		String id = resource.getUNID();
		sb.append("id:").append("'").append(id).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(resource.getName())).append("'");
		String desc = resource.getDescription();
		if (desc != null && desc.length() > 0) sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(desc)).append("'");
		return sb.toString();
	}

}

