/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.List;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemDisplay;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * 系统默认的只读字段对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明： 以<strong>&lt;div id="{字段英文名}" readonly="true" class="readonly|readonlym(如果为多行文本框则多一个m后缀)" value="{字段保存值}"&gt;{字段显示值}&lt;/div&gt;</strong>格式输出只读字段值。<br/>
 * 前端js要获取字段值时可通过“innerHTML”属性获取，也可通过名为“value”的扩展html属性来获取，如果字段的保存值和显示值不一样的话则innerHTML为显示值，value为实际保存值。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DefaultReadonlyItemRender extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public DefaultReadonlyItemRender() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;

		HtmlElement el = new HtmlElement("div");

		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		ItemDisplay itemDisplay = item.getDisplay();
		boolean faceValue = (itemDisplay == ItemDisplay.CheckBox || itemDisplay == ItemDisplay.Radio || itemDisplay == ItemDisplay.SingleCombo || itemDisplay == ItemDisplay.MultipleCombo);
		String itemName = item.getItemName();
		String itemValue = this.getItemValue("");

		el.appendAttribute("id", itemName);
		el.appendAttribute("readonly", "true");
		el.appendAttribute("class", "readonly" + (item.getDisplay() == ItemDisplay.MultipleLineText ? "m" : ""));

		boolean foundMapValue = false;
		DSValuesProvider dsp = DSValuesProvider.getDatasourceValuesProvider(this.m_itemDisplay);
		List<StringPair> dsvs = (dsp == null ? null : dsp.getValues(this.m_itemDisplay, jspContext));
		if (dsvs != null && dsvs.size() > 0) {
			String pairValue = null;
			String pairName = null;

			for (StringPair x : dsvs) {
				if (x == null) continue;
				pairName = x.getKey();
				pairValue = x.getValue();
				if (pairName == null || pairName.length() == 0) continue;
				if (pairValue == null || pairValue.length() == 0) pairValue = pairName;
				if (pairValue.equalsIgnoreCase(itemValue)) {
					foundMapValue = true;
					if (faceValue) {
						el.setInnerHTML(pairName);
					} else {
						el.setInnerHTML(pairValue);
					}
				}
			}// for end
		}// if end
		if (!foundMapValue) el.setInnerHTML(itemValue);
		el.appendAttribute("value", StringUtil.encode4HtmlPropValue(itemValue));
		return el;
	}
}

