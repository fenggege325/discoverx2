/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.model.DataSource;
import com.tansuosoft.discoverx.model.DataSourceResource;
import com.tansuosoft.discoverx.model.Dictionary;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;

/**
 * 提供{@link DataSourceResource}类型数据源具体值的{@link DSValuesProvider}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DSResourceValuesProvider extends DSValuesProvider {

	/**
	 * 缺省构造器。
	 */
	public DSResourceValuesProvider() {
		super();
	}

	private List<StringPair> list = null;
	private Method titlePropMethod = null;
	private Method valuePropMethod = null;
	private boolean fullTree = true;
	private int level = 0;

	/**
	 * 重载getValues
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.DSValuesProvider#getValues(com.tansuosoft.discoverx.web.ui.document.ItemDisplay,
	 *      com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public List<StringPair> getValues(ItemDisplay itemDisplay, JSPContext jspContext) {
		if (itemDisplay == null) return null;
		Item item = itemDisplay.getItem();
		if (item == null) return null;
		DataSource ds = item.getDataSource();
		if (ds == null || !(ds instanceof DataSourceResource)) return null;
		DataSourceResource dsx = (DataSourceResource) ds;
		String unid = dsx.getUNID();
		if (unid.startsWith("dict")) unid = ResourceAliasContext.getInstance().getUNIDByAlias(Dictionary.class, unid);
		String directory = dsx.getDirectory();
		Resource resource = ResourceContext.getInstance().getResource(unid, directory);
		if (resource == null) {
			FileLogger.debug("找不到“%1$s”中“%2$s”指定的资源！", directory, dsx.getUNID());
			return null;
		}
		String titlePropName = dsx.getTitlePropName();
		if (titlePropName == null || titlePropName.length() == 0) {
			titlePropName = "Name";
		} else {
			titlePropName = StringUtil.toPascalCase(titlePropName);
		}
		String valuePropName = dsx.getValuePropName();
		if (valuePropName == null || valuePropName.length() == 0) {
			valuePropName = "Name";
		} else {
			valuePropName = StringUtil.toPascalCase(valuePropName);
		}

		Class<?> clazz = resource.getClass();
		try {
			titlePropMethod = clazz.getMethod("get" + titlePropName);
			valuePropMethod = clazz.getMethod("get" + valuePropName);
		} catch (SecurityException e) {
			FileLogger.error(e);
		} catch (NoSuchMethodException e) {
			FileLogger.error(e);
		}
		fullTree = dsx.getFullTree();
		list = new ArrayList<StringPair>();
		this.fillList(resource);
		return this.list;
	}

	/**
	 * 填充List&lt;StringPair&gt;结果列表。
	 * 
	 * @param r
	 */
	protected void fillList(Resource r) {
		if (r == null) return;
		List<Resource> children = r.getChildren();
		if (children == null || children.isEmpty()) return;
		level++;
		String title = null;
		String value = null;
		Object obj = null;
		for (Resource x : children) {
			if (x == null) continue;
			try {
				obj = (titlePropMethod == null ? "[错误：找不到属性]" : titlePropMethod.invoke(x));
				if (obj != null) title = obj.toString();
				if (title == null) title = "";
				StringBuilder sb = new StringBuilder();
				for (int i = 1; i < level; i++) {
					sb.append("-");
				}
				sb.append(title);
				title = sb.toString();
				obj = (valuePropMethod == null ? "" : valuePropMethod.invoke(x));
				if (obj != null) value = obj.toString();
				if (value == null) value = "";
				list.add(new StringPair(title, value));
			} catch (IllegalArgumentException e) {
				FileLogger.error(e);
			} catch (IllegalAccessException e) {
				FileLogger.error(e);
			} catch (InvocationTargetException e) {
				FileLogger.error(e);
			}
			if (fullTree) fillList(x);
		}// for end
		level--;
	}// func end
}

