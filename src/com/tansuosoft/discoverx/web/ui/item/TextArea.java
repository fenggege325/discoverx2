/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * 多行文本框对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：
 * 
 * <pre>
 * &lt;textarea id=&quot;{itemName}&quot; name=&quot;{itemName}&quot;...&gt;
 * {字段值}
 * &lt;/textarea&gt;
 * </pre>
 * 
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class TextArea extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public TextArea() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String itemValue = this.getEncodedItemValue("");

		HtmlElement htmlElement = new HtmlElement("textarea");
		htmlElement.appendAttribute("name", itemName);
		htmlElement.appendAttribute("id", itemName);
		htmlElement.appendAttribute("style", "width:" + (this.m_itemDisplay.getContentWidth() - 2) + "px;");
		int height = (this.m_itemDisplay.getContentHeight() <= 0 ? 80 : this.m_itemDisplay.getContentHeight());
		htmlElement.appendAttribute("style", "height:" + height + "px;");
		htmlElement.setInnerHTML(itemValue);
		return htmlElement;
	}
}

