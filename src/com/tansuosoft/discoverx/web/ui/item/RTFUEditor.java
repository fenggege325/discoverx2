/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * 富文本对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：使用百度UEditor1.2.5编辑器界面以编辑字段值。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class RTFUEditor extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public RTFUEditor() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String itemValue = this.getEncodedItemValue("");

		StringBuilder sb = new StringBuilder();

		int h = (this.m_itemDisplay.getContentHeight() > 0 ? this.m_itemDisplay.getContentHeight() : 320);
		int w = this.m_itemDisplay.getContentWidth();

		// 只读
		if (this.m_itemDisplay.getReadonly()) {
			HtmlElement htmlElement = new HtmlElement("div");
			htmlElement.appendAttribute("id", itemName);
			htmlElement.appendAttribute("style", "width:" + this.m_itemDisplay.getContentWidth() + "px;");
			htmlElement.appendAttribute("style", "min-height:" + h + "px;");
			htmlElement.appendAttribute("style", "_height:" + h + "px;");
			htmlElement.setInnerHTML(this.getItemValue(""));
			return htmlElement;
		}

		HtmlElement textarea = new HtmlElement("textarea");
		textarea.appendAttribute("name", itemName);
		textarea.appendAttribute("id", itemName);
		textarea.appendAttribute("style", "width:" + this.m_itemDisplay.getContentWidth() + "px;");
		textarea.appendAttribute("style", "height:" + h + "px;");
		textarea.setInnerHTML(itemValue);

		HtmlElement jsinclude1 = new HtmlElement("script");
		jsinclude1.appendAttribute("src", "../rtf_ueditor/editor_config.js");
		jsinclude1.appendAttribute("type", "text/javascript");
		jsinclude1.setInnerHTML("");

		HtmlElement jsinclude2 = new HtmlElement("script");
		jsinclude2.appendAttribute("src", "../rtf_ueditor/editor_all_min.js");
		jsinclude2.appendAttribute("type", "text/javascript");
		jsinclude2.setInnerHTML("");

		HtmlElement js = new HtmlElement("script");
		js.appendAttribute("type", "text/javascript");

		StringBuilder sbjs = new StringBuilder();
		sbjs.append("\r\n\twindow.UEDITOR_CONFIG.textarea='").append(itemName).append("';\r\n");
		if (h > 0) {
			sbjs.append("\twindow.UEDITOR_CONFIG.minFrameHeight=").append(h - 81 - 25 - 2).append(";\r\n");
			sbjs.append("\twindow.UEDITOR_CONFIG.initialFrameHeight=").append(h - 81 - 25 - 2).append(";\r\n");
		}
		if (w > 0) {
			sbjs.append("\twindow.UEDITOR_CONFIG.minFrameWidth=").append(w - 2).append(";\r\n");
			sbjs.append("\twindow.UEDITOR_CONFIG.initialFrameWidth=").append(w - 2).append(";\r\n");
		}
		sbjs.append("\tEVENT.add(window,'load',function(){\r\n");
		sbjs.append("\t\twindow.RTFEditorId='").append(itemName).append("';window.RTFEditorInstance_").append(itemName).append("=UE.getEditor('").append(itemName).append("');\r\n");
		sbjs.append("\t\tsetQueryExecute('querysaveexecute',function(){\r\n\t\t\twindow.RTFEditorInstance_").append(itemName).append(".sync();\r\n");
		sbjs.append("\t\t});\r\n");
		sbjs.append("\t});\r\n");
		js.setInnerHTML(sbjs.toString());
		sb.append(textarea.render(jspContext)).append(jsinclude1.render(jspContext)).append(jsinclude2.render(jspContext)).append(js.render(jspContext));
		HtmlElement result = new HtmlElement();
		result.setInnerHTML(sb.toString());
		return result;
	}
}

