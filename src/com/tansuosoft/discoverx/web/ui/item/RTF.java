/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.web.Browser;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * 富文本对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：使用CKEditor 3.0.2编辑器界面以编辑字段值。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class RTF extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public RTF() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String itemValue = this.getEncodedItemValue("");

		StringBuilder sb = new StringBuilder();

		// 只读
		if (this.m_itemDisplay.getReadonly()) {
			HtmlElement htmlElement = new HtmlElement("div");
			htmlElement.appendAttribute("id", itemName);
			htmlElement.appendAttribute("style", "width:" + this.m_itemDisplay.getContentWidth() + "px;");
			int height = (this.m_itemDisplay.getContentHeight() > 0 ? this.m_itemDisplay.getContentHeight() : 200);
			htmlElement.appendAttribute("style", "min-height:" + height + "px;");
			htmlElement.appendAttribute("style", "_height:" + height + "px;");
			htmlElement.setInnerHTML(this.getItemValue(""));
			return htmlElement;
		}

		HtmlElement htmlElement = new HtmlElement("textarea");
		htmlElement.appendAttribute("name", itemName);
		htmlElement.appendAttribute("id", itemName);
		htmlElement.appendAttribute("style", "width:" + this.m_itemDisplay.getContentWidth() + "px;");
		htmlElement.appendAttribute("style", "height:" + (this.m_itemDisplay.getContentHeight() > 0 ? this.m_itemDisplay.getContentHeight() : 200) + "px;");
		htmlElement.setInnerHTML(itemValue);

		HtmlElement jsinclude = new HtmlElement("script");
		jsinclude.appendAttribute("id", "ckeditor");
		jsinclude.appendAttribute("src", "../ckeditor/ckeditor.js");
		jsinclude.appendAttribute("type", "text/javascript");
		jsinclude.setInnerHTML("");

		HtmlElement js = new HtmlElement("script");
		js.appendAttribute("type", "text/javascript");
		js.appendAttribute("id", "rtfeditor_" + itemName);
		int h = (this.m_itemDisplay.getContentHeight() > 0 ? this.m_itemDisplay.getContentHeight() : 300);

		int columnCount = m_itemDisplay.getColumnCount();
		boolean isIE6 = jspContext.getBrowser().check(Browser.MSIE, 6);// jspContext.getBrowser() == Browser.IE6;
		if (isIE6 && columnCount > 1) h -= 150;
		String hx = ",{height:'" + h + "'}";
		String jsie6 = "";
		if (isIE6 && columnCount == 1) {
			jsie6 = String.format("try{var itemctrl_%1$s=document.getElementById('itemcontrol_%2$s');if(itemctrl_%3$s!=null)itemctrl_%4$s.style.height='%5$spx';}catch(e){}", itemName, itemName, itemName, itemName, h + 150);
		}
		js.setInnerHTML(jsie6 + "window.RTFEditorId='" + itemName + "';window.RTFEditorInstance_" + itemName + "=CKEDITOR.replace('" + itemName + "'" + hx + ");");

		sb.append(jsinclude.render(jspContext)).append(htmlElement.render(jspContext)).append(js.render(jspContext));
		HtmlElement result = new HtmlElement();
		result.setInnerHTML(sb.toString());
		return result;
	}
}

