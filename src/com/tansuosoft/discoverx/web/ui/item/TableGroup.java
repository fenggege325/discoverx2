/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemGroup;
import com.tansuosoft.discoverx.model.ItemGroupType;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplayFilter;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplayReadonlyFilter;
import com.tansuosoft.discoverx.web.ui.document.ItemParsed;
import com.tansuosoft.discoverx.web.ui.document.ItemsParser;
import com.tansuosoft.discoverx.web.ui.selector.SelectorForm;

/**
 * 字段组类型为{@link ItemGroupType#Table}的{@link ItemGroup}对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：客户端ListView格式的结果。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class TableGroup extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public TableGroup() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null || !(item instanceof ItemGroup)) return null;
		ItemGroup itemGroup = (ItemGroup) item;
		if (itemGroup.getGroupType() != ItemGroupType.Table) return null;

		String unid = item.getUNID();
		Form form = this.m_document.getForm();
		ItemsParser formItemsParser = ItemsParser.getInstance(form);
		Map<String, ItemsParser> groupMap = formItemsParser.getGroups();
		if (groupMap == null || groupMap.isEmpty()) {
			FileLogger.debug("无法获取表单“%1$s”对应的字段分组解析对象！", form.getName());
			return null;
		}
		ItemsParser itemsParser = groupMap.get(unid);
		if (itemsParser == null || itemsParser.getMaxRow() <= 0) {
			FileLogger.debug("无法获取“%1$s”字段组包含的字段的解析信息！", item.getName());
			return null;
		}
		List<ItemParsed> parsedItems = itemsParser.getLineItems(1);
		if (parsedItems == null || parsedItems.isEmpty()) {
			FileLogger.debug("无法获取“%1$s”字段组包含的字段信息！", item.getName());
			return null;
		}

		String itemName = item.getItemName();
		if (itemName == null || itemName.length() == 0) {
			int groupIndex = 0;
			for (ItemGroup x : form.getItemGroups()) {
				if (x != null && unid.equalsIgnoreCase(x.getUNID())) {
					itemName = "group" + groupIndex;
					break;
				}
				groupIndex++;
			}
		}

		boolean isReadonly = (this.m_itemDisplay == null ? (this.m_documentDisplayInfo == null ? false : this.m_documentDisplayInfo.getReadonly()) : this.m_itemDisplay.getReadonly());
		int lvcHeight = this.m_itemDisplay.getContentHeight();
		int lvcWidth = this.m_itemDisplay.getContentWidth() - 2;
		HtmlElement htmlElement = new HtmlElement("div");
		htmlElement.appendAttribute("id", "lvxc_" + itemName);
		htmlElement.appendAttribute("style", "min-height:" + lvcHeight + "px;");
		htmlElement.appendAttribute("style", "_height:" + lvcHeight + "px;");
		htmlElement.appendAttribute("style", "width:" + lvcWidth + "px;");
		htmlElement.appendAttribute("style", "overflow:hidden;clear:both;border:solid 1px #ccc;");
		htmlElement.setInnerHTML("");

		// javascript
		HtmlElement js = new HtmlElement("script");
		js.appendAttribute("type", "text/javascript");

		StringBuilder sb = new StringBuilder();

		sb.append("EVENT.add(window,'load',function(){\r\n"); // event add begin
		// sb.append("EVENT.add(document,'readystatechange',function(){if(document.readyState!='complete') return;\r\n"); // event add begin
		String globalVarName = "lv_" + itemName;
		sb.append("var ").append(globalVarName).append("=new TableGroupItemListView({\r\n"); // lv begin
		sb.append("id:").append("'").append(globalVarName).append("'");
		sb.append(",").append("width:").append(this.m_itemDisplay.getContentWidth() - 2);
		sb.append(",").append("height:").append(lvcHeight);
		sb.append(",").append("widthMeasurement:").append("'").append(form.getMeasurement() == SizeMeasurement.Percentage ? "%" : "px").append("'");
		sb.append(",").append("data:").append("{");
		sb.append("parentUNID:").append("'").append(this.m_document.getUNID()).append("'");
		sb.append(",").append("readonly:").append(isReadonly ? "true" : "false");
		sb.append(",").append("itemName:").append("'").append(itemName).append("'");
		sb.append(",").append("minCount:").append(itemGroup.getMinCount());
		sb.append(",").append("maxCount:").append(itemGroup.getMaxCount());
		sb.append("}");

		// header
		sb.append(",\r\n").append("header:").append("{"); // header begin
		sb.append("id:'lvh_").append(itemName).append("'").append(",itemType: 1");
		sb.append(",columns:[\r\n"); // columns begin

		Item itemx = null;
		int index = 0;
		String itemValue = null;
		String itemValues[] = null;
		String delimiter = null;
		List<String[]> values = null;
		ItemDisplay itemDisplay = null;
		List<ItemDisplay> itemDisplays = new ArrayList<ItemDisplay>(parsedItems.size());
		ItemDisplayFilter readonlyFilter = new ItemDisplayReadonlyFilter(); // 只读过滤
		// ItemDisplayFilter visibileFilter = new ItemDisplayVisibleFilter(); // 可见过滤
		for (ItemParsed x : parsedItems) {
			if (x == null) continue;
			itemx = x.getItem();
			if (itemx == null) continue;
			if (!StringUtil.isBlank(itemx.getJsValidator())) m_documentDisplayInfo.setHasValidator(true);
			if (!StringUtil.isBlank(itemx.getJsTranslator())) m_documentDisplayInfo.setHasTranslator(true);
			itemDisplay = new ItemDisplay(x);
			readonlyFilter.filter(itemDisplay, this.m_document, jspContext);
			// visibileFilter.filter(itemDisplay, m_document, jspContext);
			itemDisplays.add(itemDisplay);
			sb.append(index == 0 ? "" : ",");
			sb.append("{"); // lvhc begin
			sb.append("id:").append("'lvhc_").append(itemx.getItemName()).append("'");
			sb.append(",").append("label:").append("'").append(itemx.getName()).append(itemx.getRequired() ? "<span>*</span>" : "").append("'");
			sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(itemx.getDescription())).append("'");
			if (itemx.getWidth() > 0) sb.append(",").append("width:").append(itemx.getWidth());
			sb.append(",").append("data:").append("{"); // data begin
			sb.append("itemName:").append("'").append(itemx.getItemName()).append("'");
			sb.append(",").append("delimiter:").append("'").append(StringUtil.getValueString(itemx.getDelimiter(), Item.DEFAULT_DELIMITER)).append("'");
			String jsV = itemx.getJsValidator();
			if (jsV != null && jsV.length() > 0) sb.append(",").append("jsValidator:").append(jsV);
			String jsT = itemx.getJsTranslator();
			if (jsT != null && jsT.length() > 0) sb.append(",").append("jsTranslactor:").append(jsT);
			sb.append(",").append("type:").append(itemx.getType());
			sb.append(",").append("display:").append(itemx.getDisplay());
			sb.append(",").append("dataSourceInput:").append(itemx.getDataSourceInput());
			if (itemx.getSize() > 0) sb.append(",").append("maxLength:").append(itemx.getSize());
			List<com.tansuosoft.discoverx.model.HtmlAttribute> attrlist = itemx.getHtmlAttributes();
			if (attrlist != null && !attrlist.isEmpty()) {
				StringBuilder sbattr = new StringBuilder();
				for (com.tansuosoft.discoverx.model.HtmlAttribute attr : attrlist) {
					if (attr == null || com.tansuosoft.discoverx.model.HtmlAttribute.SELECTOR_ATTRIBUTE_NAME.equalsIgnoreCase(attr.getName())) continue;
					sbattr.append(sbattr.length() == 0 ? "" : ",").append("{n:'").append(StringUtil.encode4Json(attr.getName())).append("',v:'").append(StringUtil.encode4Json(attr.getValue())).append("'}");
				}
				if (sbattr.length() > 0) {
					sbattr.insert(0, ",htmlAttributes:[");
					sbattr.append("]");
					sb.append(sbattr.toString());
				}
			}
			if (itemx.getDisplay() == com.tansuosoft.discoverx.model.ItemDisplay.SingleCombo || itemx.getDisplay() == com.tansuosoft.discoverx.model.ItemDisplay.Radio) {
				// 直接数据源
				DSValuesProvider dsp = DSValuesProvider.getDatasourceValuesProvider(itemDisplay);
				if (dsp instanceof DSParticipantValuesProvider) dsp = null;
				List<StringPair> dsvs = (dsp == null ? null : dsp.getValues(itemDisplay, jspContext));
				if (dsvs != null && dsvs.size() > 0) {
					sb.append(",").append("dataSourceResult:[");
					int spIdx = 0;
					for (StringPair sp : dsvs) {
						if (sp == null) continue;
						String pairName = sp.getKey();
						String pairValue = sp.getValue();
						if (pairName == null || pairName.length() == 0) continue;
						if (pairValue == null || pairValue.length() == 0) pairValue = pairName;

						sb.append(spIdx == 0 ? "" : ",").append("{title:'").append(StringUtil.encode4Json(pairName)).append("',value:'").append(StringUtil.encode4Json(pairValue)).append("'}");
						spIdx++;
					}
					sb.append("]");
				}
			} else if (!isReadonly) { // 选择数据源
				SelectorForm selectorForm = ItemRenderSelectorProvider.getSelectorForm(itemDisplay, jspContext);
				if (selectorForm != null) {
					m_documentDisplayInfo.setHasSelector(true);
					selectorForm.setTarget("edit_" + selectorForm.getTarget());
					String templet = ItemRenderSelectorProvider.getSelectorJsonObject(selectorForm);
					if (templet != null && templet.length() > 0) sb.append(",").append("selector:").append(templet).append("");
				}
			}
			if (itemDisplay != null && itemDisplay.getReadonly()) sb.append(",").append("readonly:true");
			String valueStoreItemName = itemx.getValueStoreItem();
			if (valueStoreItemName != null && valueStoreItemName.length() > 0) sb.append(",").append("valueStoreItem:'").append(valueStoreItemName).append("'");
			sb.append("}"); // data end

			sb.append("}\r\n"); // lvhc end

			itemValue = this.m_document.getItemValue(itemx.getItemName());
			if (itemValue != null) {
				delimiter = itemx.getDelimiter();
				if (delimiter == null || delimiter.length() == 0) delimiter = ",";
				itemValues = (delimiter.length() == 1 ? StringUtil.splitString(itemValue, delimiter.charAt(0)) : itemValue.split(delimiter));
			} else {
				itemValues = null;
			}
			if (values == null) values = new ArrayList<String[]>(parsedItems.size());
			values.add(itemValues);

			index++;
		}
		sb.append("]"); // columns end
		sb.append("}"); // // header end

		// listitem

		sb.append(",\r\n").append("rows:").append("["); // rows begin

		int valueCount = -1;
		int subIndex = 0;
		String first[] = null;
		String str = null;

		first = values.get(0);
		if (valueCount < 0) valueCount = (first != null ? first.length : 1);
		for (int i = 0; i < valueCount; i++) { // 行
			sb.append(i > 0 ? "," : "");
			sb.append("{"); // lvi begin
			sb.append("id:").append("'lvi_").append(itemName).append("_").append(i).append("'");
			sb.append(",").append("data:").append("{"); // data begin
			sb.append("}"); // data end

			sb.append(",").append("columns:").append("[\r\n"); // columns begin
			subIndex = 0;
			for (String[] strs : values) { // 列
				itemDisplay = itemDisplays.get(subIndex);
				str = (strs != null && strs.length > i ? strs[i] : "");
				if (str == null) {
					str = "";
				} else {
					str = StringUtil.encode4Json(str);
				}
				sb.append(subIndex == 0 ? "" : ",");
				sb.append("{"); // lvc begin

				sb.append("id:").append("'lvc_").append(itemName).append("_").append(i).append("_").append(subIndex).append("'");
				sb.append(",").append("label:").append("'").append(str).append("'");
				sb.append(",").append("value:").append("'").append(str).append("'");
				sb.append(",").append("desc:").append("'").append(str).append("'");
				if (itemDisplay != null && itemDisplay.getReadonly()) sb.append(",").append("editable:false");
				sb.append(",").append("data:").append("{"); // data begin
				sb.append("}"); // data end

				sb.append("}\r\n"); // lvc end
				subIndex++;
			} // 列结束
			sb.append("]"); // columns end

			sb.append("}"); // lvi end
		} // 行结束

		sb.append("]"); // rows end

		sb.append("\r\n});"); // lv end
		sb.append("\r\nTableGroupItemListViews.push(").append(globalVarName).append(");");
		sb.append("\r\n});"); // event add end
		js.setInnerHTML(sb.toString());
		StringBuilder result = new StringBuilder();
		result.append(htmlElement.render(jspContext)).append(js.render(jspContext));
		HtmlElement ret = new HtmlElement();
		ret.setInnerHTML(result.toString());
		return ret;
	}
}

