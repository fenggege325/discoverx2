/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.DataSource;
import com.tansuosoft.discoverx.model.DataSourceParticipant;
import com.tansuosoft.discoverx.model.DataSourceResource;
import com.tansuosoft.discoverx.model.DataSourceType;
import com.tansuosoft.discoverx.model.DataSourceView;
import com.tansuosoft.discoverx.model.HtmlAttribute;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemType;
import com.tansuosoft.discoverx.model.ItemTypeDateTimeOption;
import com.tansuosoft.discoverx.model.ItemTypeLinkOption;
import com.tansuosoft.discoverx.model.ItemTypeOption;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;
import com.tansuosoft.discoverx.web.ui.selector.SelectorForm;
import com.tansuosoft.discoverx.web.ui.selector.TreeViewProvider;

/**
 * 字段编辑或输入时用于根据其绑定数据源或字段值类型显示对应的编辑/选择框信息（{@link SelectorForm}）的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemRenderSelectorProvider {
	/**
	 * 缺省构造器。
	 */
	private ItemRenderSelectorProvider() {
	}

	/**
	 * 返回一个打开选择框的热点对应的{@link HtmlElement}对象。
	 * 
	 * <p>
	 * tagName元素的“id”属性值为：“selector_{字段名}”。<br/>
	 * tagName元素的“class”属性值为：“selectorhotspot”。<br/>
	 * tagName元素的“onclick”属性值为打开选择框的js函数且根据字段数据源配置选项提供了必要的初始化参数。<br/>
	 * </p>
	 * 
	 * @param tagName 必须，表示{@link HtmlElement}对象的标记名，如“div”。
	 * @param selectorForm {@link SelectorForm}，必须。
	 * @param label 热点名称，默认为“[选择]”。
	 * @return
	 */
	public static HtmlElement getSelectorHtmlElement(String tagName, SelectorForm selectorForm, String label) {
		if (tagName == null || tagName.length() == 0 || selectorForm == null) return null;
		HtmlElement div = new HtmlElement(tagName);
		div.appendAttribute("id", "selector_" + selectorForm.getTarget());
		div.appendAttribute("class", "selectorhotspot");

		String onclick = String.format("Selector.show(%1$s);", getSelectorJsonObject(selectorForm));
		div.appendAttribute("onclick", onclick);
		div.setInnerHTML(label == null || label.length() == 0 ? "[选择]" : label);
		return div;
	}

	/**
	 * 返回一个打开选择框的Selector.show方法所使用的json对象文本，形如：“{datasource:'p://user'...}”。
	 * 
	 * @param selectorForm
	 * @return
	 */
	public static String getSelectorJsonObject(SelectorForm selectorForm) {
		if (selectorForm == null) return null;
		boolean isNumber = (StringUtil.isNumber(selectorForm.getValue()) && StringUtil.isNumber(selectorForm.getLabel()));
		String valueFormat = (isNumber ? selectorForm.getValue() : "'" + selectorForm.getValue() + "'");
		String labelFormat = (isNumber ? selectorForm.getLabel() : "'" + selectorForm.getLabel() + "'");

		Map<String, String> complements = selectorForm.getComplements();
		StringBuilder complementsParam = new StringBuilder();
		if (complements != null && complements.size() > 0) {
			for (String k : complements.keySet()) {
				complementsParam.append(complementsParam.length() > 0 ? "," : "");
				complementsParam.append(k).append(":'").append(StringUtil.encode4Json(complements.get(k))).append("'");
			}
			complementsParam.insert(0, ",complements:{");
			complementsParam.append("}");
		}
		StringBuilder okFnPtr = new StringBuilder();
		String fnPtr = selectorForm.getLabelOk();
		if (fnPtr != null && fnPtr.length() > 0) okFnPtr.append("labelOk:").append(fnPtr);
		fnPtr = selectorForm.getValueOk();
		if (fnPtr != null && fnPtr.length() > 0) okFnPtr.append(okFnPtr.length() > 0 ? "," : "").append("valueOk:").append(fnPtr);
		if (okFnPtr.length() > 0) okFnPtr.insert(0, ",");
		String rds = selectorForm.getDatasource();
		boolean isObjDS = (rds != null && rds.length() > 0 && rds.indexOf(TreeViewProvider.PREFIX_DELIMITER) < 0);
		String ds = String.format("%1$s%2$s%3$s", isObjDS ? "" : "'", rds, isObjDS ? "" : "'");
		return String.format("{datasource:%1$s,target:'%2$s',labelOkId:'%3$s',value:%4$s,label:%5$s,multiple:%6$s,delimiter:'%7$s'%8$s%9$s}", ds, selectorForm.getTarget(), StringUtil.getValueString(selectorForm.getLabelTarget(), ""), valueFormat, labelFormat, (selectorForm.getMultiple() ? "true" : "false"), (selectorForm.getMultiple() ? selectorForm.getDelimiter() : ""), okFnPtr, complementsParam.toString());
	}

	/**
	 * 为指定{@link ItemDisplay}返回其绑定数据源对应的选择框信息（{@link SelectorForm}）。
	 * 
	 * @param itemDisplay
	 * @param jspContext
	 * @return {@link SelectorForm}，如果没有有效的数据源信息或者字段只读时则返回null。
	 */
	public static SelectorForm getSelectorForm(ItemDisplay itemDisplay, JSPContext jspContext) {
		if (itemDisplay == null || itemDisplay.getReadonly()) return null;
		Item item = itemDisplay.getItem();
		SelectorForm result = null;
		ItemType itemType = item.getType();
		DataSourceType dst = item.getDataSourceType();
		DataSource dso = item.getDataSource();
		String ds = null;
		String valueFormat = null;
		String titleFormat = null;
		String labelOk = null;
		String valueOk = null;
		Map<String, String> complements = null;
		if (dso != null) {
			switch (dst.getIntValue()) {
			case 1: // Resource
				if (dso instanceof DataSourceResource) {
					DataSourceResource dsr = (DataSourceResource) dso;
					ds = dsr.getDirectory() + TreeViewProvider.PREFIX_DELIMITER + dsr.getUNID();
				}
				break;
			case 2: // View
				if (dso instanceof DataSourceView) {
					DataSourceView dsv = (DataSourceView) dso;
					ds = TreeViewProvider.VIEW_PREFIX + dsv.getUNID();
				}
				break;
			case 3: // Participant
				if (dso instanceof DataSourceParticipant) {
					DataSourceParticipant dsp = (DataSourceParticipant) dso;
					ds = TreeViewProvider.PARTICIPANT_PREFIX + ParticipantType.getAbbreviation(dsp.getParticipantType());
				}
				break;
			default:
				break;
			}
			if (ds != null) {
				valueFormat = dso.getValueFormat();
				titleFormat = dso.getTitleFormat();
			}
		}

		if (ds == null) {
			ItemTypeOption option = null;
			switch (itemType.getIntValue()) {
			case 3: // Datetime
				ds = "datetime://";
				option = item.getDataTypeOption();
				if (option != null && option instanceof ItemTypeDateTimeOption) {
					ItemTypeDateTimeOption dtOption = (ItemTypeDateTimeOption) option;
					valueFormat = dtOption.getValueFormat();
					titleFormat = dtOption.getTitleFormat();
				}
				if (valueFormat != null && valueFormat.length() > 0) {
					valueFormat = valueFormat.replace('m', 'n').replace('M', 'm').replace('H', 'h');
				}
				if (titleFormat != null && titleFormat.length() > 0) {
					titleFormat = titleFormat.replace('m', 'n').replace('M', 'm').replace('H', 'h');
				}
				if (valueFormat == null || valueFormat.length() == 0) valueFormat = "yyyy-mm-dd hh:nn:ss";
				if (titleFormat == null || titleFormat.length() == 0) titleFormat = "yyyy-mm-dd hh:nn:ss";
				break;
			case 4: // Image
				ds = "image://";
				valueFormat = "";
				titleFormat = "";
				if (complements == null) complements = new HashMap<String, String>();
				complements.put("accessoryType", AccessoryType.IMAGE_FIELD_TYPE);
				valueOk = "setImgResult";
				break;
			case 5: // Link
				ds = "link://";
				option = item.getDataTypeOption();
				if (option != null && option instanceof ItemTypeLinkOption) {
					String target = null;
					ItemTypeLinkOption linkOption = (ItemTypeLinkOption) option;
					target = linkOption.getTarget();
					if (target == null || target.length() == 0) target = "_blank";
					if (complements == null) complements = new HashMap<String, String>();
					complements.put("target", target);
				}
				valueFormat = "";
				titleFormat = "";
				labelOk = "setLinkEditorResult";
				break;
			}
		}

		String valueStoreItemName = item.getValueStoreItem();
		if (ds != null && ds.length() > 0) {
			result = new SelectorForm();
			result.setDatasource(ds);
			if (valueStoreItemName != null && valueStoreItemName.length() > 0) {
				result.setTarget(valueStoreItemName);
				result.setLabelTarget(item.getItemName());
			} else {
				result.setTarget(item.getItemName());
			}
			String linkageItemNames = item.getLinkageItem();
			if (linkageItemNames != null && linkageItemNames.length() > 0) {
				valueOk = "linkageActions.setLinkageResult";
			}
			result.setMultiple(item.allowMultipleValue());
			if (result.getMultiple()) result.setDelimiter(item.getDelimiter());
			result.setValue(valueFormat);
			result.setLabel(titleFormat);
			result.setLabelOk(labelOk);
			result.setValueOk(valueOk);
			result.setComplements(complements);
		} else {
			List<HtmlAttribute> htmlattrs = item.getHtmlAttributes();
			if (htmlattrs != null && htmlattrs.size() > 0) {
				for (HtmlAttribute ht : htmlattrs) {
					if (ht != null && HtmlAttribute.SELECTOR_ATTRIBUTE_NAME.equalsIgnoreCase(ht.getName())) {
						result = SelectorForm.fromJson(ht.getValue());
						break;
					}
				}
			}
			if (result != null && StringUtil.isBlank(result.getTarget())) {
				if (valueStoreItemName != null && valueStoreItemName.length() > 0) {
					result.setTarget(valueStoreItemName);
					result.setLabelTarget(item.getItemName());
				} else {
					result.setTarget(item.getItemName());
				}
			}
		}

		return result;
	}
}

