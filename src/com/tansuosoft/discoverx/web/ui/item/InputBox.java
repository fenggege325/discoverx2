/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.List;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemDataSourceInput;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * 单行文本框对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：
 * 
 * <pre>
 * &lt;input type=&quot;text&quot; id=&quot;{itemName}&quot; name=&quot;{itemName}&quot; value=&quot;{关键字值}&quot;/&gt;
 * </pre>
 * 
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class InputBox extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public InputBox() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String itemValue = this.getEncodedItemValue("");

		HtmlElement htmlElement = new HtmlElement("input");
		htmlElement.appendAttribute("type", "text");
		htmlElement.appendAttribute("name", itemName);
		htmlElement.appendAttribute("id", itemName);
		htmlElement.appendAttribute("value", itemValue);
		String valueStoreItemName = item.getValueStoreItem();
		if (valueStoreItemName != null && valueStoreItemName.length() > 0) htmlElement.appendAttribute("valueStoreItem", valueStoreItemName);
		htmlElement.appendAttribute("style", "width:" + (this.m_itemDisplay.getContentWidth() - 4) + "px;");
		int maxSize = item.getSize();
		if (maxSize > 0) htmlElement.appendAttribute("maxlength", maxSize + "");
		// 处理输入自动下拉提示类型的数据源
		boolean isReadonly = (this.m_itemDisplay == null ? (this.m_documentDisplayInfo == null ? false : this.m_documentDisplayInfo.getReadonly()) : this.m_itemDisplay.getReadonly());
		if (!isReadonly && ItemDataSourceInput.AutoComplete == this.m_itemDisplay.getDatasourceInput()) {
			DSValuesProvider dsvp = DSValuesProvider.getDatasourceValuesProvider(m_itemDisplay);
			List<StringPair> dss = dsvp.getValues(m_itemDisplay, jspContext);
			StringBuilder sb = new StringBuilder();
			if (dss != null && dss.size() > 0) {
				for (StringPair sp : dss) {
					if (sb.length() > 0) sb.append(",");
					sb.append("{");
					sb.append("t:'").append(StringUtil.encode4Json(sp.getKey())).append("'");
					sb.append(",").append("v:'").append(StringUtil.encode4Json(sp.getValue())).append("'");
					sb.append("}");
				}
			}
			if (sb.length() > 0) {
				htmlElement.appendAttribute("ds", sb.toString());
				htmlElement.appendAttribute("onkeyup", "autoComplete.onkeyup(this);");
				htmlElement.appendAttribute("autocomplete", "off");
			}
		}
		return htmlElement;
	}
}

