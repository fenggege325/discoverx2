/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.DataSource;
import com.tansuosoft.discoverx.model.DataSourceParticipant;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;

/**
 * 提供{@link DataSourceParticipant}类型数据源具体值的{@link DSValuesProvider}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DSParticipantValuesProvider extends DSValuesProvider {

	/**
	 * 缺省构造器。
	 */
	public DSParticipantValuesProvider() {
		super();
	}

	/**
	 * 重载getValues
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.DSValuesProvider#getValues(com.tansuosoft.discoverx.web.ui.document.ItemDisplay,
	 *      com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public List<StringPair> getValues(ItemDisplay itemDisplay, JSPContext jspContext) {
		if (itemDisplay == null) return null;
		Item item = itemDisplay.getItem();
		if (item == null) return null;
		DataSource ds = item.getDataSource();
		if (ds == null || !(ds instanceof DataSourceParticipant)) return null;
		DataSourceParticipant dsx = (DataSourceParticipant) ds;
		ParticipantType pt = dsx.getParticipantType();
		if (pt == null) return null;
		List<ParticipantTree> list = ParticipantTreeProvider.getInstance().getChildren(pt);
		if (list == null || list.isEmpty()) return null;
		String valueFormat = dsx.getValueFormat();
		if (valueFormat == null || valueFormat.length() == 0) valueFormat = User.LEGALLY_NAME_PART_FORMATS[0];
		String titleFormat = dsx.getTitleFormat();
		if (titleFormat == null || titleFormat.length() == 0) titleFormat = User.LEGALLY_NAME_PART_FORMATS[0];
		List<StringPair> result = new ArrayList<StringPair>(list.size());
		for (ParticipantTree x : list) {
			if (x == null) continue;
			result.add(new StringPair(ParticipantHelper.getFormatValue(x, titleFormat), ParticipantHelper.getFormatValue(x, valueFormat)));
		}
		return result;
	}

}

