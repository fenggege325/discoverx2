/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.Map;

import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemGroup;
import com.tansuosoft.discoverx.model.ItemGroupType;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;
import com.tansuosoft.discoverx.web.ui.document.ItemsParser;
import com.tansuosoft.discoverx.web.ui.document.ItemsRender;

/**
 * {@link ItemGroup}对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明： <br/>
 * 如果是段落型，则输出
 * 
 * <pre>
 * &lt;div id=&quot;{itemName}&quot; class=&quot;itemgroupcontainer&quot;&gt;{分组字段包含的所有字段对应的{@link ItemsRender}输出结果}&lt;/div&gt;
 * </pre>
 * 
 * <br/>
 * 如果是表格型，则输{@link TableGroup}的输出结果。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Group extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public Group() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null || !(item instanceof ItemGroup)) return null;
		ItemGroup itemGroup = (ItemGroup) item;
		String itemName = item.getItemName();

		// 获取字段组对应ItemsParser。
		String unid = item.getUNID();
		Form form = this.m_document.getForm();
		ItemsParser formItemsParser = ItemsParser.getInstance(form);
		Map<String, ItemsParser> groupMap = formItemsParser.getGroups();
		if (groupMap == null || groupMap.isEmpty()) {
			FileLogger.debug("无法获取表单“%1$s”对应的字段分组解析对象！", form.getName());
			return null;
		}
		ItemsParser itemsParser = groupMap.get(unid);
		if (itemsParser == null) {
			FileLogger.debug("无法获取“%1$s”字段组包含的字段的解析信息！", item.getName());
			return null;
		}
		ItemGroupType igt = itemGroup.getGroupType();
		m_documentDisplayInfo.setItemGroupType(igt);
		switch (igt.getIntValue()) {
		case 1: // Table
			TableGroup tableGroup = new TableGroup();
			tableGroup.m_itemDisplay = this.m_itemDisplay;
			tableGroup.m_document = this.m_document;
			tableGroup.m_documentDisplayInfo = this.m_documentDisplayInfo;
			return tableGroup.renderItem(jspContext);
		case 2: // Paragraph
			HtmlElement htmlElement = new HtmlElement("div");
			htmlElement.appendAttribute("id", itemName);
			htmlElement.appendAttribute("class", "itemgroupcontainer");
			// ItemsRender itemsRender = new ItemsRender();
			// itemsRender.init(itemsParser, this.m_document, this.m_documentDisplayInfo);
			ItemsRender itemsRender = ItemsRender.getItemsRender(itemsParser, this.m_document, this.m_documentDisplayInfo);
			itemsRender.setMaxWidth(this.m_itemDisplay.getContentWidth());
			htmlElement.setInnerHTML(itemsRender.render(jspContext));
			return htmlElement;
		default:
			return null;
		}
	}
}

