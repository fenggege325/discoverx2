/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import com.tansuosoft.discoverx.common.ItemValidator;
import com.tansuosoft.discoverx.common.ItemValidatorConfig;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出字段校验信息JSON的HTMLRender类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ValidationRender implements HtmlRender {
	private Item m_item = null;

	/**
	 * 接收Item对象的构造器。
	 */
	public ValidationRender(Item item) {
		this.m_item = item;
	}

	/**
	 * 重载render：输出字段校验信息JSON。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		if (this.m_item == null) return "";
		String jsfunc = this.m_item.getJsValidator();
		if (jsfunc == null || jsfunc.length() == 0) return "";

		ItemValidatorConfig itemValidatorConfig = ItemValidatorConfig.getInstance();
		ItemValidator x = itemValidatorConfig.getItemValidator(jsfunc);
		String func = (x == null ? jsfunc : x.getJsFunctionName());
		String errmsg = (x == null ? null : x.getErrorMessage());
		if (errmsg == null || errmsg.length() == 0) {
			errmsg = "对不起，“{caption}”值不符合要求的格式。" + (x == null ? "" : "原因:" + x.getValidatorName());
		} else {
			errmsg.replace("\\", "\\\\").replace("'", "\\'");
		}
		String param = (x == null ? null : x.getComplement());

		StringBuffer sb = new StringBuffer();
		sb.append("{");
		sb.append("name:").append("'").append(this.m_item.getItemName()).append("'"); // 字段名
		sb.append(",").append("caption:").append("'").append(this.m_item.getCaption().replace("*", "")).append("'"); // 字段说明
		sb.append(",").append("func:").append(func);
		sb.append(",").append("errmsg:").append("'").append(errmsg).append("'");
		if (param != null && param.length() > 0) sb.append(",").append("params:").append(param);
		sb.append("}");
		return sb.toString();
	}
}

