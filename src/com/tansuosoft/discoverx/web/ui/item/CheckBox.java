/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.List;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * 复选按钮对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：
 * 
 * <pre>
 * &lt;input type=&quot;checkbox&quot; id=&quot;{itemName}_{根据备选值个数自增长的序号}&quot; name=&quot;{itemName}&quot; value=&quot;{关键字值}&quot;/&gt;&lt;label for=&quot;{id}&quot;&gt;{关键字名称}&lt;/label&gt;
 * ...
 * </pre>
 * 
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class CheckBox extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public CheckBox() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String[] vals = this.m_document.getItemValues(itemName);

		DSValuesProvider dsp = DSValuesProvider.getDatasourceValuesProvider(this.m_itemDisplay);
		List<StringPair> dsvs = (dsp == null ? null : dsp.getValues(this.m_itemDisplay, jspContext));

		StringBuilder sb = new StringBuilder();// 将所有选择框输出
		StringBuilder sbjs = new StringBuilder();
		if (dsvs != null && dsvs.size() > 0) {
			String pairValue = null;
			String pairName = null;
			HtmlElement htmlElement = null;
			HtmlElement label = null;
			int idx = 0;
			String id = null;
			for (StringPair x : dsvs) {
				if (x == null) continue;
				pairName = x.getKey();
				pairValue = x.getValue();
				if (pairName == null || pairName.length() == 0) continue;
				if (pairValue == null || pairValue.length() == 0) pairValue = pairName;

				htmlElement = new HtmlElement("input");
				htmlElement.appendAttribute("type", "checkbox");
				htmlElement.appendAttribute("name", itemName);
				htmlElement.appendAttribute("class", "inputopt");
				id = itemName + "_" + idx;
				htmlElement.appendAttribute("id", id);
				htmlElement.appendAttribute("value", StringUtil.encode4HtmlPropValue(pairValue));
				String desc = item.getDescription();
				if (desc != null && desc.length() > 0) htmlElement.appendAttribute("title", desc);

				label = new HtmlElement("label");
				label.appendAttribute("class", "inputoptlabel");
				label.appendAttribute("for", id);
				if (desc != null && desc.length() > 0) label.appendAttribute("title", desc);
				label.setInnerHTML(pairName);

				if (vals != null && vals.length > 0) {
					for (String v : vals) {
						if (pairValue.equalsIgnoreCase(v)) htmlElement.appendAttribute("checked", "true");
					}
				}
				appendConfigHtmlAttributes(htmlElement);
				sb.append(htmlElement.render(jspContext).replace("\r\n", "")).append(label.render(jspContext).replace("\r\n", "")).append("&nbsp;\r\n");
				idx++;
			}
			if (idx > 0) {
				sbjs.append("<script type=\"text/javascript\">\r\n");
				sbjs.append("EVENT.add(window, 'load', function () {setQueryExecute('querysaveexecute',function(){\r\n");
				sbjs.append("try{\r\n\tvar f=FORM.get();if(f==null) return;\r\n");
				sbjs.append("\tvar el=null;var c=").append(idx).append(";var n='").append(itemName).append("';var nosel=true;\r\n");
				sbjs.append("\tfor(var i=0;i<c;i++){\r\n");
				sbjs.append("\t\tel=document.getElementById(n+'_'+i);if(el==null) continue;\r\n");
				sbjs.append("\t\tif(el.checked){nosel=false;break;}\r\n");
				sbjs.append("\t}\r\n");
				sbjs.append("\tif(nosel){\r\n");
				sbjs.append("\t\tel=document.createElement('input');\r\n");
				sbjs.append("\t\tel.setAttribute('name',n);\r\n");
				sbjs.append("\t\tel.setAttribute('value','');\r\n");
				sbjs.append("\t\tf.appendChild(el);\r\n");
				sbjs.append("\t}\r\n");
				sbjs.append("}catch(e){}\r\n");
				sbjs.append("})});\r\n");
				sbjs.append("</script>\r\n");
				sb.append(sbjs.toString());
			}
		} else {
			sb.append("没有可用选项！");
		}

		HtmlElement htmlElement = new HtmlElement();
		htmlElement.setInnerHTML(sb.toString());

		return htmlElement;
	}
}

