/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;
import com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;

/**
 * 隐藏字段对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：
 * 
 * <pre>
 * &lt;input type=&quot;hidden&quot; id=&quot;{itemName}&quot; name=&quot;{itemName}&quot; value=&quot;{itemValue}&quot;/&gt;
 * </pre>
 * 
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Hidden extends ItemRender {

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param itemDisplay
	 * @param document
	 * @param documentDisplayInfo
	 */
	public Hidden(ItemDisplay itemDisplay, Document document, DocumentDisplayInfo documentDisplayInfo) {
		this.m_itemDisplay = itemDisplay;
		this.m_document = document;
		this.m_documentDisplayInfo = documentDisplayInfo;
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String itemValue = this.getEncodedItemValue("");

		HtmlElement htmlElement = new HtmlElement("input");
		htmlElement.appendAttribute("type", "hidden");
		htmlElement.appendAttribute("name", itemName);
		htmlElement.appendAttribute("id", itemName);
		htmlElement.appendAttribute("value", itemValue);
		return htmlElement;
	}
}

