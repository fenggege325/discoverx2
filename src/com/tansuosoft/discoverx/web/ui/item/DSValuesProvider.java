/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.List;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;

/**
 * 提供字段数据源结果列表的类。
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class DSValuesProvider {
	/**
	 * 缺省构造器。
	 */
	public DSValuesProvider() {
	}

	/**
	 * 为指定字段提供数据源结果列表集合。
	 * 
	 * @param itemDisplay
	 * @param jspContext
	 * @return
	 */
	public abstract List<StringPair> getValues(ItemDisplay itemDisplay, JSPContext jspContext);

	/**
	 * 为指定{@link ItemDisplay}对象返回其对应的{@link Item}对象配置的数据源对应的{@link DSValuesProvider}对象。
	 * 
	 * @param itemDisplay
	 * @return
	 */
	public static DSValuesProvider getDatasourceValuesProvider(ItemDisplay itemDisplay) {
		if (itemDisplay == null) return null;
		Item item = itemDisplay.getItem();
		if (item == null) return null;
		return getDatasourceValuesProvider(item);
	}

	/**
	 * 为指定{@link Item}对象返回其配置的数据源对应的{@link DSValuesProvider}对象。
	 * 
	 * @param item
	 * @return
	 */
	public static DSValuesProvider getDatasourceValuesProvider(Item item) {
		DSValuesProvider result = null;
		if (item == null) return null;
		switch (item.getDataSourceType().getIntValue()) {
		case 1: // Resource
			result = new DSResourceValuesProvider();
			break;
		case 2: // View
			result = new DSViewValuesProvider();
			break;
		case 3: // Participant
			result = new DSParticipantValuesProvider();
			break;
		case 4: // Constant
			result = new DSConstantValuesProvider();
			break;
		case 100:
			String impl = item.getCustomDataSourceImplement();
			if (impl != null && impl.length() > 0) {
				result = Instance.newInstance(impl, DSValuesProvider.class);
			}
			break;
		case 0: // None
		default:
			return null;
		}
		return result;
	}
}

