/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * {@link com.tansuosoft.discoverx.model.ItemType#Image}对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：输出图片预览。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Image extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public Image() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String itemValue = this.getItemValue("");

		// boolean readonly = this.m_itemDisplay.getReadonly();
		boolean gotImg = true;
		if (itemValue == null || itemValue.length() == 0) gotImg = false;

		HtmlElement dummy = new HtmlElement("");
		HtmlElement img = new HtmlElement("img");
		img.appendAttribute("id", "img_" + itemName);
		if (gotImg) {
			img.appendAttribute("src", String.format("%1$sdownload?target=%2$s&punid=%3$s", jspContext.getPathContext().getWebAppRoot(), itemValue, this.m_document.getUNID()));
		} else {
			img.appendAttribute("src", String.format("%1$sblank.gif", jspContext.getPathContext().getThemeImagesPath()));
		}
		img.appendAttribute("alt", item.getCaption());
		if (!StringUtil.isBlank(item.getDescription())) img.appendAttribute("title", item.getDescription());
		img.appendAttribute("border", "0");
		img.appendAttribute("style", "width:" + this.m_itemDisplay.getContentWidth() + "px;");
		if (this.m_itemDisplay.getContentHeight() > 0) img.appendAttribute("style", "height:" + this.m_itemDisplay.getContentHeight() + "px;");
		appendConfigHtmlAttributes(img);

		HtmlElement hidden = new HtmlElement("input");
		hidden.appendAttribute("type", "hidden");
		hidden.appendAttribute("name", itemName);
		hidden.appendAttribute("id", itemName);
		hidden.appendAttribute("value", itemValue);

		dummy.appendSubElement(img);
		dummy.appendSubElement(hidden);
		return dummy;
	}
}

