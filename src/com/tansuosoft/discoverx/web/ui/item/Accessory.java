/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemTypeAccessoryOption;
import com.tansuosoft.discoverx.model.ItemTypeOption;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.accessory.AccessoryListViewRender;
import com.tansuosoft.discoverx.web.ui.HtmlElement;
import com.tansuosoft.discoverx.web.ui.toolbar.AccessoryOperationProvider;
import com.tansuosoft.discoverx.web.ui.toolbar.OperationProvider;
import com.tansuosoft.discoverx.web.ui.toolbar.ToolbarRender;

/**
 * 附加文件类型字段对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明： 输出包含各附加文件信息的json对象内容。其数据结构符合ListView的定义。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Accessory extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public Accessory() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		this.m_documentDisplayInfo.setHasAccessory(true);
		List<com.tansuosoft.discoverx.model.Accessory> accs = this.m_document.getAccessories();
		if (accs == null) accs = new ArrayList<com.tansuosoft.discoverx.model.Accessory>();

		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();

		OperationProvider operationProvider = new AccessoryOperationProvider(this.m_document, this.m_itemDisplay, this.m_documentDisplayInfo);
		ToolbarRender toolbarRender = new ToolbarRender(operationProvider);

		boolean isReadonly = (this.m_itemDisplay == null ? (this.m_documentDisplayInfo == null ? false : this.m_documentDisplayInfo.getReadonly()) : this.m_itemDisplay.getReadonly());

		int lvcWidth = (this.m_itemDisplay.getContentWidth() - 2);
		int lvcHeight = this.m_itemDisplay.getContentHeight() - 32;

		List<AccessoryType> ats = null;
		ItemTypeOption ito = item.getDataTypeOption();
		if (ito != null && ito instanceof ItemTypeAccessoryOption) {
			ItemTypeAccessoryOption itao = (ItemTypeAccessoryOption) ito;
			List<Reference> racts = itao.getAccessoryTypes();
			if (racts != null) {
				AccessoryType at = null;
				for (Reference r : racts) {
					if (r == null) continue;
					at = ResourceContext.getInstance().getResource(r.getUnid(), AccessoryType.class);
					if (at == null) continue;
					if (ats == null) ats = new ArrayList<AccessoryType>();
					ats.add(at);
				}
			}
		}

		// toolbar container
		HtmlElement divToolbar = new HtmlElement("div");
		divToolbar.appendAttribute("id", "tbxc_" + itemName);
		divToolbar.appendAttribute("style", "height:32px;");
		divToolbar.appendAttribute("style", "width:" + lvcWidth + "px;");
		divToolbar.appendAttribute("style", "overflow:hidden;clear:both;border:solid 1px #ccc;border-bottom-width:0px;");
		divToolbar.setInnerHTML("");

		// listview container
		HtmlElement divContainer = new HtmlElement("div");
		divContainer.appendAttribute("id", "lvxc_" + itemName);
		divContainer.appendAttribute("style", "min-height:" + lvcHeight + "px;");
		divContainer.appendAttribute("style", "_height:" + lvcHeight + "px;");
		divContainer.appendAttribute("style", "width:" + lvcWidth + "px;");
		divContainer.appendAttribute("style", "overflow:hidden;clear:both;border:solid 1px #ccc;border-top-width:0px;");
		divContainer.setInnerHTML("");

		// accessory.js reference
		HtmlElement jsinclude = new HtmlElement("script");
		jsinclude.appendAttribute("src", jspContext.getPathContext().getFrameJSPath() + "accessory.js");
		jsinclude.appendAttribute("type", "text/javascript");
		jsinclude.setInnerHTML("");

		// javascript
		HtmlElement js = new HtmlElement("script");
		js.appendAttribute("type", "text/javascript");

		StringBuilder sb = new StringBuilder();
		sb.append("EVENT.add(window,'load',function(){\r\n"); // event add begin
		sb.append("accessoryListView.init({"); // lv begin
		sb.append(new AccessoryListViewRender(this.m_document, lvcWidth, lvcHeight, isReadonly, toolbarRender, ats).render(jspContext));
		sb.append("});"); // lv end
		sb.append("});"); // event add end

		js.setInnerHTML(sb.toString());
		StringBuilder result = new StringBuilder();
		result.append(jsinclude.render(jspContext)).append(divToolbar.render(jspContext)).append(divContainer.render(jspContext)).append(js.render(jspContext));
		HtmlElement htmlElement = new HtmlElement();
		htmlElement.setInnerHTML(result.toString());
		return htmlElement;
	}
}

