/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.List;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * 单选按钮对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：
 * 
 * <pre>
 * &lt;input type=&quot;radio&quot; id=&quot;{itemName}_{自增长选向序号}&quot; name=&quot;{itemName}&quot; value=&quot;{关键字值}&quot;/&gt;&lt;label for=&quot;{id}&quot;&gt;{关键字名称}&lt;/label&gt;
 * ...
 * </pre>
 * 
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Radio extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public Radio() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String itemValue = this.getEncodedItemValue("");

		DSValuesProvider dsp = DSValuesProvider.getDatasourceValuesProvider(this.m_itemDisplay);
		List<StringPair> dsvs = (dsp == null ? null : dsp.getValues(this.m_itemDisplay, jspContext));

		StringBuilder sb = new StringBuilder();// 将所有选择框输出
		if (dsvs != null && dsvs.size() > 0) {
			String pairValue = null;
			String pairName = null;
			HtmlElement htmlElement = null;
			HtmlElement label = null;
			int idx = 0;
			String id = null;
			for (StringPair x : dsvs) {
				if (x == null) continue;
				pairName = x.getKey();
				pairValue = x.getValue();
				if (pairName == null || pairName.length() == 0) continue;
				if (pairValue == null || pairValue.length() == 0) pairValue = pairName;

				htmlElement = new HtmlElement("input");
				htmlElement.appendAttribute("type", "radio");
				htmlElement.appendAttribute("name", itemName);
				htmlElement.appendAttribute("class", "inputopt");
				id = itemName + "_" + idx;
				htmlElement.appendAttribute("id", id);
				htmlElement.appendAttribute("value", pairValue);
				String desc = item.getDescription();
				if (desc != null && desc.length() > 0) htmlElement.appendAttribute("title", desc);

				label = new HtmlElement("label");
				label.appendAttribute("class", "inputoptlabel");
				label.appendAttribute("for", id);
				if (desc != null && desc.length() > 0) label.appendAttribute("title", desc);
				label.setInnerHTML(pairName);
				if (pairValue.equalsIgnoreCase(itemValue)) {
					htmlElement.appendAttribute("checked", "true");
				}
				appendConfigHtmlAttributes(htmlElement);
				sb.append(htmlElement.render(jspContext).replace("\r\n", "")).append(label.render(jspContext).replace("\r\n", "")).append("&nbsp;\r\n");
				idx++;
			}
		}
		HtmlElement htmlElement = new HtmlElement();
		htmlElement.setInnerHTML(sb.toString());
		return htmlElement;
	}

}

