/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemTypeLinkOption;
import com.tansuosoft.discoverx.model.ItemTypeOption;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * {@link com.tansuosoft.discoverx.model.ItemType#Link}对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：可单击的链接地址。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Link extends ItemRender {
	/**
	 * 缺省构造器。
	 */
	public Link() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String itemValue = this.getEncodedItemValue("");

		String delimiter = "\r\n";
		String title = StringUtil.stringLeft(itemValue, delimiter);
		String href = StringUtil.stringRight(itemValue, delimiter);
		if (title.equalsIgnoreCase(title) && href == null) {
			char cs[] = { '|', '~', '`', ':', ';', ',' };
			int pos = -1;
			for (char c : cs) {
				pos = itemValue.indexOf(c);
				if (pos >= 0 && itemValue.indexOf(c, pos + 1) < 0) {
					String[] strs = StringUtil.splitString(itemValue, c);
					title = strs[0];
					href = strs[1];
					break;
				}
			}
		}
		boolean hasHref = (href != null && href.length() > 0);
		boolean isJsHref = (hasHref ? (href.toLowerCase().startsWith("javascript:")) : false);

		HtmlElement dummy = new HtmlElement("");
		HtmlElement hidden = new HtmlElement("input");
		hidden.appendAttribute("type", "hidden");
		hidden.appendAttribute("name", itemName);
		hidden.appendAttribute("id", itemName);
		hidden.appendAttribute("value", StringUtil.encode4HtmlPropValue(itemValue));
		dummy.appendSubElement(hidden);
		HtmlElement anchor = new HtmlElement("a");
		anchor.appendAttribute("id", "link_" + itemName);
		anchor.appendAttribute("class", "linkanchor");
		anchor.appendAttribute("href", (hasHref ? href : "javascript:undefined"));
		if (hasHref && !isJsHref) {
			ItemTypeOption ito = item.getDataTypeOption();
			ItemTypeLinkOption itlo = null;
			if (ito != null && ito instanceof ItemTypeLinkOption) {
				itlo = (ItemTypeLinkOption) ito;
			}
			String target = (itlo == null ? null : itlo.getTarget());
			if (target == null || target.length() == 0) target = "_blank";
			anchor.appendAttribute("target", target);
			anchor.appendAttribute("title", "打开链接");
		}
		anchor.setInnerHTML((title != null && title.length() > 0 ? title : item.getCaption()));
		this.appendConfigHtmlAttributes(anchor);
		dummy.appendSubElement(anchor);

		return dummy;
	}
}

