/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.List;

import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemDataSourceInput;
import com.tansuosoft.discoverx.model.ItemType;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlAttribute;
import com.tansuosoft.discoverx.web.ui.HtmlElement;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;
import com.tansuosoft.discoverx.web.ui.selector.SelectorForm;

/**
 * 字段内容显示/呈现基类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ItemRender implements HtmlRender {
	/**
	 * 缺省构造器。
	 */
	public ItemRender() {
	}

	/**
	 * 要显示/呈现的字段信息。
	 */
	protected ItemDisplay m_itemDisplay = null;
	/**
	 * 要显示的字段所属文档资源。
	 */
	protected Document m_document = null;

	/**
	 * 要显示的字段所属文档显示选项资源。
	 */
	protected DocumentDisplayInfo m_documentDisplayInfo = null;

	/**
	 * 重载render：输出最终html结果。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		HtmlElement el = this.renderItem(jspContext);
		if (el == null) return "";
		if (!StringUtil.isBlank(el.getTagName()) && !"hidden".equalsIgnoreCase(el.getAttribute("type")) && StringUtil.isBlank(el.getAttribute("title"))) {
			String desc = this.m_itemDisplay.getItem().getDescription();
			if (desc != null) el.appendAttribute("title", desc);
		}
		this.processConfigHtmlAttributes(el, jspContext);
		this.processAutoSelector(el, jspContext);

		return el.render(jspContext);
	}

	/**
	 * 处理“input”、“textarea”、“select”等标记的额外Html属性。
	 * 
	 * @param target
	 */
	protected void processConfigHtmlAttributes(HtmlElement target, JSPContext jspContext) {
		if (target == null) return;
		String tagName = target.getTagName();
		if (tagName != null && (tagName.equalsIgnoreCase("input") || tagName.equalsIgnoreCase("textarea") || tagName.equalsIgnoreCase("select"))) {
			this.appendConfigHtmlAttributes(target);
		}
	}

	/**
	 * 处理“input”、“textarea”等标记获取焦点时自动显示数据源选择框的额外html属性。
	 * 
	 * @param target
	 * @param jspContext
	 */
	protected void processAutoSelector(HtmlElement target, JSPContext jspContext) {
		if (target == null) return;
		String tagName = target.getTagName();
		if (ItemDataSourceInput.SelectorAuto == this.m_itemDisplay.getDatasourceInput()) {
			// 获取焦点时自动显示数据源选择框的处理。
			if (tagName != null && (tagName.equalsIgnoreCase("input") || tagName.equalsIgnoreCase("textarea"))) {
				SelectorForm selectorForm = ItemRenderSelectorProvider.getSelectorForm(m_itemDisplay, jspContext);
				HtmlElement selectorHotspot = ItemRenderSelectorProvider.getSelectorHtmlElement("div", selectorForm, "");
				if (selectorHotspot != null) {
					String onfocus = selectorHotspot.getAttribute("onclick");
					if (onfocus != null && onfocus.length() > 0) {
						target.appendAttribute("onfocus", onfocus);
						m_documentDisplayInfo.setHasSelector(true);
					}
				}
			}
		}
	}

	/**
	 * 为{@link JSPContext}所指页面输出字段值控件内容的{@link HtmlElement}结果。
	 * 
	 * @param jspContext
	 * @return HtmlElement
	 */
	protected abstract HtmlElement renderItem(JSPContext jspContext);

	/**
	 * 获取当前字段值，如果找不到，则返回defaultIfNull。
	 * 
	 * @param defaultIfNull
	 * @return
	 */
	protected String getItemValue(String defaultIfNull) {
		Item item = (this.m_itemDisplay == null ? null : this.m_itemDisplay.getItem());
		if (item == null) return defaultIfNull;
		String v = this.m_document.getItemValue(item.getItemName());
		if (v == null) return defaultIfNull;
		return v;
	}

	/**
	 * 获取当前字段值编码为html属性值后的结果，如果找不到，则返回defaultIfNull。
	 * 
	 * @param defaultIfNull
	 * @return
	 */
	protected String getEncodedItemValue(String defaultIfNull) {
		String v = getItemValue(defaultIfNull);
		if (v != null && v.length() > 0) return StringUtil.encode4HtmlPropValue(v);
		return v;
	}

	/**
	 * 将指定字段配置中额外包含的{@link com.tansuosoft.discoverx.model.HtmlAttribute}集合附加到{@link HtmlElement}包含的属性列表中。
	 * 
	 * @param el
	 */
	protected void appendConfigHtmlAttributes(HtmlElement el) {
		if (this.m_itemDisplay == null || el == null) return;
		Item item = m_itemDisplay.getItem();
		if (item == null) return;
		List<com.tansuosoft.discoverx.model.HtmlAttribute> list = item.getHtmlAttributes();
		if (list == null || list.isEmpty()) return;
		for (com.tansuosoft.discoverx.model.HtmlAttribute x : list) {
			if (x == null || com.tansuosoft.discoverx.model.HtmlAttribute.SELECTOR_ATTRIBUTE_NAME.equalsIgnoreCase(x.getName())) continue;
			el.appendAttribute(new HtmlAttribute(x));
		}
	}

	/**
	 * 根据{@link ItemDisplay}获取对应{@link ItemRender}。
	 * 
	 * @param itemDisplay
	 * @return {@link ItemRender}
	 */
	public static ItemRender getItemRender(ItemDisplay itemDisplay, Document document, DocumentDisplayInfo documentDisplayInfo) {
		if (itemDisplay == null) return null;
		ItemRender itemRender = null;
		Item item = itemDisplay.getItem();
		if (item == null) return null;
		com.tansuosoft.discoverx.model.ItemDisplay display = item.getDisplay();

		boolean readonly = itemDisplay.getReadonly();
		if (readonly) {
			if (display == com.tansuosoft.discoverx.model.ItemDisplay.Custom) {
				itemRender = Instance.newInstance(item.getDisplayImplement(), ItemRender.class);
			}
			if (itemRender == null) {
				switch (item.getType().getIntValue()) {
				case 4: // Image
					itemRender = new Image();
					break;
				case 5: // Link
					itemRender = new Link();
					break;
				case 101: // RTF
					itemRender = (USE_BAIDU_UEDITOR_AS_RTF_EDITOR ? new RTFUEditor() : new RTF());
					break;
				case -1:// Accessory
					itemRender = new Accessory();
					break;
				case -2: // Group
					itemRender = new Group();
					break;
				default:
					itemRender = new DefaultReadonlyItemRender();
					break;
				}
			}// if end
		}

		if (itemRender == null && display == com.tansuosoft.discoverx.model.ItemDisplay.Default) {
			ItemType itemType = item.getType();
			switch (itemType.getIntValue()) {
			case 4: // Image
				itemRender = new Image();
				break;
			case 5: // Link
				itemRender = new Link();
				break;
			case 101: // RTF
				itemRender = (USE_BAIDU_UEDITOR_AS_RTF_EDITOR ? new RTFUEditor() : new RTF());
				break;
			case -1:// Accessory
				itemRender = new Accessory();
				break;
			case -2: // Group
				itemRender = new Group();
				break;
			case 1: // Send
			case 2: // Number
			case 3: // DateTime
			default:
				itemRender = new InputBox();
				break;
			}
		}

		if (itemRender == null) {
			switch (display.getIntValue()) {
			case 1: // SingleLineText
				itemRender = new InputBox();
				break;
			case 2: // MultipleLineText
				itemRender = new TextArea();
				break;
			case 3: // SingleCombo
				itemRender = new Select();
				break;
			case 4: // MultipleCombo
				itemRender = new Select();
				break;
			case 5: // CheckBox
				itemRender = new CheckBox();
				break;
			case 6: // Radio
				itemRender = new Radio();
				break;
			case 7: // Image
				itemRender = new Image();
				break;
			case 8: // Link
				itemRender = new Link();
				break;
			case 9: // RTF
				itemRender = (USE_BAIDU_UEDITOR_AS_RTF_EDITOR ? new RTFUEditor() : new RTF());
				break;
			case 10: // Accessory
				itemRender = new Accessory();
				break;
			case 11: // Group
				itemRender = new Group();
				break;
			case 0: // Custom
				String impl = item.getDisplayImplement();
				itemRender = Instance.newInstance(impl, ItemRender.class);
				break;
			default: // Default
				itemRender = new InputBox();
				break;
			}
		}
		if (itemRender == null) {
			itemRender = new ItemRender() {
				@Override
				protected HtmlElement renderItem(JSPContext jspContext) {
					HtmlElement el = new HtmlElement("div");
					el.appendAttribute("style", "font-weight:bold;");
					el.setInnerHTML("无法呈现字段控件，请联系管理员检查表单及字段配置！");
					return el;
				}
			};
		}
		if (itemRender != null) {
			itemRender.m_itemDisplay = itemDisplay;
			itemRender.m_document = document;
			itemRender.m_documentDisplayInfo = documentDisplayInfo;
		}
		return itemRender;
	}// func end

	/**
	 * 是否使用百度UEditor编辑器作为富文本编辑器。
	 */
	protected static boolean USE_BAIDU_UEDITOR_AS_RTF_EDITOR = true;
	static {
		try {
			Class.forName("ueditor.Uploader");
		} catch (ClassNotFoundException e) {
			USE_BAIDU_UEDITOR_AS_RTF_EDITOR = false;
		} catch (Exception ex) {
			USE_BAIDU_UEDITOR_AS_RTF_EDITOR = false;
		}
	}
}

