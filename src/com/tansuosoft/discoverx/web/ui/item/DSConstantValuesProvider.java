/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.DataSource;
import com.tansuosoft.discoverx.model.DataSourceConstant;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;

/**
 * 提供{@link DataSourceConstant}类型数据源具体值的{@link DSValuesProvider}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DSConstantValuesProvider extends DSValuesProvider {

	/**
	 * 缺省构造器。
	 */
	public DSConstantValuesProvider() {
		super();
	}

	/**
	 * 重载getValues
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.DSValuesProvider#getValues(com.tansuosoft.discoverx.web.ui.document.ItemDisplay,
	 *      com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public List<StringPair> getValues(ItemDisplay itemDisplay, JSPContext jspContext) {
		if (itemDisplay == null) return null;
		Item item = itemDisplay.getItem();
		if (item == null) return null;
		DataSource ds = item.getDataSource();
		if (ds == null || !(ds instanceof DataSourceConstant)) return null;
		DataSourceConstant dsx = (DataSourceConstant) ds;
		return getValues(dsx);
	}

	/**
	 * 从{@link DataSourceConstant}对象的{@link DataSourceConstant#getConfigValue()}返回结果中解析出标题-值配对的结果列表并返回之。
	 * 
	 * @param ds DataSourceConstant
	 * @return List&lt;StringPair&gt; 返回解析后的标题-值配对的结果列表或null（异常时）。
	 */
	protected static List<StringPair> getValues(DataSourceConstant ds) {
		if (ds == null) return null;
		String configValue = ds.getConfigValue();
		if (configValue == null || configValue.length() == 0) return null;
		List<String> items = new ArrayList<String>();
		int currentIndex = 0;
		int nextSeparator;
		while ((nextSeparator = configValue.indexOf(DataSourceConstant.VALUES_DELIMITER, currentIndex)) != -1) {
			items.add(configValue.substring(currentIndex, nextSeparator));
			currentIndex = nextSeparator + 1;
		}
		items.add(configValue.substring(currentIndex));
		String title = null;
		String value = null;
		int pos = -1;
		List<StringPair> result = new ArrayList<StringPair>(items.size());
		for (String x : items) {
			if (x == null || x.length() == 0) continue;
			pos = x.indexOf(DataSourceConstant.TITLE_VALUE_DELIMITER);
			if (pos < 0) {
				title = value = x;
			} else {
				title = x.substring(0, pos);
				value = x.substring(pos + 1);
				if (value == null || value.length() == 0) value = title;
			}
			result.add(new StringPair(title, value));
		}// for end
		return result;
	}
}

