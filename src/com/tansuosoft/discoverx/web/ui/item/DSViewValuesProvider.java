/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.view.ViewEntry;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.bll.view.ViewQueryProvider;
import com.tansuosoft.discoverx.model.DataSource;
import com.tansuosoft.discoverx.model.DataSourceView;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.document.ItemDisplay;

/**
 * 提供{@link DataSourceView}类型数据源具体值的{@link DSValuesProvider}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DSViewValuesProvider extends DSValuesProvider {

	/**
	 * 缺省构造器。
	 */
	public DSViewValuesProvider() {
		super();
	}

	private int titleIndex = 1;
	private int valueIndex = 0;
	private List<StringPair> list = null;

	/**
	 * 重载getValues
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.DSValuesProvider#getValues(com.tansuosoft.discoverx.web.ui.document.ItemDisplay,
	 *      com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public List<StringPair> getValues(ItemDisplay itemDisplay, JSPContext jspContext) {
		if (itemDisplay == null) return null;
		Item item = itemDisplay.getItem();
		if (item == null) return null;
		DataSource ds = item.getDataSource();
		if (ds == null || !(ds instanceof DataSourceView)) return null;
		DataSourceView dsx = (DataSourceView) ds;

		String viewUnid = dsx.getUNID();
		View view = (View) ResourceContext.getInstance().getResource(viewUnid, View.class);
		if (view == null) {
			FileLogger.debug("找不到“%1$s”对应视图！", viewUnid);
			return null;
		}
		titleIndex = dsx.getTitleColumnIndex();
		valueIndex = dsx.getValueColumnIndex();
		ViewQuery vq = ViewQueryProvider.getViewQuery(view, jspContext.getUserSession());
		vq.setPagination(null);
		ViewEntryCallback callback = new ViewEntryCallback() {
			@Override
			public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
				List<String> values = viewEntry.getValues();
				int max = (titleIndex > valueIndex ? titleIndex : valueIndex);
				if (values == null || values.size() < max) throw new RuntimeException("视图最大列数小于" + max + "！");
				String value = (valueIndex == 0 ? viewEntry.getUnid() : values.get(valueIndex - 1));
				String title = values.get(titleIndex - 1);
				if (list == null) list = new ArrayList<StringPair>();
				list.add(new StringPair(title, value));
			}

			@Override
			public Object getResult() {
				return null;
			}
		};
		vq.addViewEntryCallback(callback);
		vq.getViewEntries();
		return this.list;
	}
}

