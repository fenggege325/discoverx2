/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.item;

import java.util.List;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;

/**
 * 下拉选择框对应的{@link ItemRender}实现类。
 * 
 * <p>
 * 输出格式说明：
 * 
 * <pre>
 * &lt;select id=&quot;{itemName}&quot; name=&quot;{itemName}&quot;&gt;
 * &lt;option value=&quot;{关键字值}&quot;&gt;{关键字名称}&lt;/option&gt;
 * ...
 * &lt;/select&gt;
 * </pre>
 * 
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Select extends ItemRender {
	protected static HtmlElement NoDataSourceOption = null;
	static {
		NoDataSourceOption = new HtmlElement("option");
		NoDataSourceOption.appendAttribute("value", "");
		NoDataSourceOption.appendAttribute("style", "color:#ccc;");
		NoDataSourceOption.appendAttribute("selected", "true");
		NoDataSourceOption.setInnerHTML("没有配置有效数据源！");
	};

	/**
	 * 缺省构造器。
	 */
	public Select() {
	}

	/**
	 * 重载renderItem
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.item.ItemRender#renderItem(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public HtmlElement renderItem(JSPContext jspContext) {
		if (this.m_itemDisplay == null || this.m_document == null) return null;
		Item item = this.m_itemDisplay.getItem();
		if (item == null) return null;
		String itemName = item.getItemName();
		String itemValue = this.getItemValue("");
		String[] itemValues = null;
		boolean multipleValue = false;
		HtmlElement selectElement = new HtmlElement("select");
		selectElement.appendAttribute("name", itemName);
		selectElement.appendAttribute("id", itemName);
		if (item.allowMultipleValue()) {
			selectElement.appendAttribute("multiple", "true");
			itemValues = m_document.getItemValues(itemName);
			multipleValue = true;
		}
		selectElement.appendAttribute("style", "width:100%;");

		DSValuesProvider dsp = DSValuesProvider.getDatasourceValuesProvider(this.m_itemDisplay);
		List<StringPair> dsvs = (dsp == null ? null : dsp.getValues(this.m_itemDisplay, jspContext));

		if (dsvs != null && dsvs.size() > 0) {
			String pairValue = null;
			String pairName = null;

			for (StringPair x : dsvs) {
				if (x == null) continue;
				pairName = x.getKey();
				pairValue = x.getValue();
				if (pairName == null || pairName.length() == 0) continue;
				if (pairValue == null || pairValue.length() == 0) pairValue = pairName;

				HtmlElement optionElement = new HtmlElement("option");
				optionElement.appendAttribute("value", StringUtil.encode4HtmlPropValue(pairValue));
				if (multipleValue) {
					if (itemValues != null) {
						for (String s : itemValues) {
							if (s != null && s.equalsIgnoreCase(pairValue)) optionElement.appendAttribute("selected", "true");
						}
					}
				} else {
					if (pairValue.equalsIgnoreCase(itemValue)) optionElement.appendAttribute("selected", "true");
				}
				optionElement.setInnerHTML(pairName);

				selectElement.appendSubElement(optionElement);
			}
		} else {
			selectElement.appendSubElement(NoDataSourceOption);
		}
		// 选择结果对应的标题同步
		String valueStoreItem = item.getValueStoreItem();
		if (valueStoreItem != null && valueStoreItem.length() > 0) {
			HtmlElement js = new HtmlElement("script");
			js.appendAttribute("type", "text/javascript");
			StringBuilder sbjs = new StringBuilder();
			String delimiter = item.getDelimiter();
			sbjs.append("\r\nEVENT.add(document.getElementById('").append(itemName).append("'),'change',function(e){");
			sbjs.append("\r\n\ttry{");
			sbjs.append("\r\n\tvar evt=(window.event||e);var t=(evt.srcElement||evt.target);");
			sbjs.append("\r\n\tvar el=document.getElementById('").append(valueStoreItem).append("');if(el==null) return;");
			sbjs.append("\r\n\tvar delimiter='").append(delimiter).append("';");
			sbjs.append("\r\n\tvar result='';");
			sbjs.append("\r\n\tfor(var i=0;i<t.options.length;i++){");
			sbjs.append("\r\n\tvar opt=t.options[i];if(!opt || !opt.selected) continue;");
			sbjs.append("\r\n\tresult+=(result.length>0?delimiter:'')+opt.text");
			sbjs.append("\r\n\t}"); // for end
			sbjs.append("\r\n\tel.value=result;");
			sbjs.append("\r\n\t}catch(e){}"); // try end
			sbjs.append("\r\n});\r\n"); // EVENT.add end
			js.setInnerHTML(sbjs.toString());
			selectElement.setOuterElement(js);
		}
		return selectElement;
	}
}

