/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 用于输出供文档日志在客户端呈现的ListView格式的json内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class LogsListViewRender implements HtmlRender {
	private Document m_document = null;
	private DocumentDisplayInfo m_documentDisplayInfo = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param document
	 * @param documentDisplayInfo
	 */
	public LogsListViewRender(Document document, DocumentDisplayInfo documentDisplayInfo) {
		super();
		this.m_document = document;
		this.m_documentDisplayInfo = documentDisplayInfo;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		try {
			Session session = jspContext.getUserSession();
			if (this.m_document == null && session != null) this.m_document = session.getLastDocument();
			if (this.m_document == null) throw new Exception("无法获取当前文档！");
			if (m_documentDisplayInfo == null) m_documentDisplayInfo = new DocumentDisplay(m_document, jspContext).getDocumentDisplayInfo();
			m_documentDisplayInfo.setHasLog(true);
			List<Log> logs = this.m_document.getLogs();
			if (logs == null) logs = new ArrayList<Log>();

			sb.append("id:").append("'lv_logs'");
			sb.append(",").append("width:").append(this.m_documentDisplayInfo.getMaxWidth() - 2);
			sb.append(",").append("height:").append(100);

			// header
			sb.append(",").append("header:").append("{"); // header begin
			sb.append("id:'lvh_logs'").append(",itemType: 1");

			int index = 0;
			int subIndex = 0;
			String titles[] = { "执行者", "日期", "动作", "目标", "结果", "备注" };
			String props[] = { "subject", "created", "verb", "object", "result", "message" };
			String values[] = null;

			sb.append(",columns:["); // columns begin
			for (String t : titles) {
				sb.append(index == 0 ? "" : ",");
				sb.append("\r\n{"); // lvc begin
				sb.append("id:").append("'lvhc_logs_").append(props[index]).append("'");
				sb.append(",").append("label:").append("'").append(t).append("'");
				sb.append(",").append("data:").append("{"); // data begin
				sb.append("}"); // data end
				sb.append("}"); // lvc end
				index++;
			}
			sb.append("]"); // columns end
			sb.append("}"); // // header end

			// listitem
			index = 0;
			sb.append(",").append("rows:").append("["); // rows begin
			for (Log x : logs) {
				if (x == null) continue;
				values = new String[titles.length];
				for (int i = 0; i < titles.length; i++) {
					switch (i) {
					case 0:
						values[i] = ParticipantHelper.getFormatValue(x.getSubject(), "cn");
						break;
					case 1:
						values[i] = x.getCreated();
						if (values[i] != null && values[i].length() >= 19) values[i] = StringUtil.stringLeftBack(values[i], ":");
						break;
					case 2:
						values[i] = x.getVerb();
						break;
					case 3:
						values[i] = x.getObject();
						break;
					case 4:
						values[i] = x.getResult();
						break;
					case 5:
						values[i] = x.getMessage();
						break;
					default:
						break;
					}
				}// for end

				sb.append(index > 0 ? "," : "");
				sb.append("\r\n{"); // lvi begin
				sb.append("id:").append("'lvi_logs").append(index).append("'");
				sb.append(",").append("data:").append("{"); // lvi data begin
				sb.append("UNID:'").append(x.getUNID()).append("'");
				sb.append("}"); // lvi data end

				sb.append(",").append("columns:").append("["); // columns begin
				subIndex = 0;
				for (String value : values) {
					sb.append(subIndex == 0 ? "" : ",");
					sb.append("\r\n{"); // lvc begin
					sb.append("id:").append("'lvc_logs_").append(index).append("_").append(subIndex).append("'");
					sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(value)).append("'");
					if (value != null && value.length() > 18) sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(value)).append("'");
					sb.append(",").append("data:").append("{"); // data begin
					sb.append("}"); // data end
					sb.append("}"); // lvc end
					subIndex++;
				}
				sb.append("]"); // columns end

				sb.append("}"); // lvi end
				index++;
			}// for end

			sb.append("]"); // rows end

			sb.append(",").append("data:").append("{"); // lv data begin
			sb.append("parentUNID:").append("'").append(this.m_document.getUNID()).append("'");
			sb.append("}"); // lv data end
		} catch (Exception ex) {
			sb = new StringBuilder();
			sb.append("error:true,message:'").append(StringUtil.encode4Json(ex.getMessage())).append("'");
		}
		return sb.toString();
	}
}

