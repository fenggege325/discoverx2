/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.web.ui.Coordinate;

/**
 * 包含实际显示时的行列等信息的{@link Item}封装类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ItemParsed {
	private Item m_item = null;

	/**
	 * 接收字段资源的构造器。
	 * 
	 * @param item Item
	 */
	public ItemParsed(Item item) {
		this.m_item = item;
	}

	/**
	 * 获取封装的字段对象。
	 * 
	 * @return Item
	 */
	public Item getItem() {
		return this.m_item;
	}

	private Coordinate m_coordinate = new Coordinate(); // 字段显示时的实际坐标位置。

	/**
	 * 返回字段显示时的实际坐标位置。
	 * 
	 * @return Coordinate 字段显示时的实际坐标位置。
	 */
	public Coordinate getCoordinate() {
		return this.m_coordinate;
	}

	/**
	 * 设置字段显示时的实际坐标位置。
	 * 
	 * @param coordinate Coordinate 字段显示时的实际坐标位置。
	 */
	public void setCoordinate(Coordinate coordinate) {
		this.m_coordinate = coordinate;
	}
}

