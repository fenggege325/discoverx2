/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import com.tansuosoft.discoverx.model.DataSourceType;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemDataSourceInput;
import com.tansuosoft.discoverx.model.ItemType;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 设置字段数据源输入方式相关属性的{@link ItemDisplayFilter}实现类。
 * 
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ItemDisplayDatasourceInputFilter extends ItemDisplayFilter {
	/**
	 * 缺省构造器。
	 */
	public ItemDisplayDatasourceInputFilter() {
	}

	/**
	 * 重载filter
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.document.ItemDisplayFilter#filter(com.tansuosoft.discoverx.web.ui.document.ItemDisplay,
	 *      com.tansuosoft.discoverx.model.Document, com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public void filter(ItemDisplay itemDisplay, Document document, JSPContext jspContext) {
		if (itemDisplay == null) return;
		itemDisplay.setDatasourceInput(null);
		Item item = itemDisplay.getItem();
		if (item == null) return;
		ItemType itemType = item.getType();
		com.tansuosoft.discoverx.model.ItemDisplay display = item.getDisplay();
		DataSourceType dataSourceType = item.getDataSourceType();
		ItemDataSourceInput dataSourceInput = item.getDataSourceInput();
		// 字段控件本身是否需要使用数据源
		boolean isDSUsedByItemControl = (display == com.tansuosoft.discoverx.model.ItemDisplay.SingleCombo || display == com.tansuosoft.discoverx.model.ItemDisplay.MultipleCombo || display == com.tansuosoft.discoverx.model.ItemDisplay.Radio || display == com.tansuosoft.discoverx.model.ItemDisplay.CheckBox);
		if (isDSUsedByItemControl) return;

		// 是否常数或自定义类型数据源。
		boolean isConstantOrCustom = (dataSourceType == DataSourceType.Constant || dataSourceType == DataSourceType.Custom);

		if (itemType == ItemType.DateTime || dataSourceInput == ItemDataSourceInput.Default) {// 系统自动处理
			if (isConstantOrCustom) { // 输入过滤下拉
				itemDisplay.setDatasourceInput(ItemDataSourceInput.AutoComplete);
			} else {
				itemDisplay.setDatasourceInput(ItemDataSourceInput.SelectorHotspot);
			}
		} else if (dataSourceInput == ItemDataSourceInput.SelectorHotspot) { // 选择框热点
			itemDisplay.setDatasourceInput(ItemDataSourceInput.SelectorHotspot);
		} else if (dataSourceInput == ItemDataSourceInput.SelectorAuto) { // 自动显示选择框
			itemDisplay.setDatasourceInput(ItemDataSourceInput.SelectorAuto);
		} else if (dataSourceInput == ItemDataSourceInput.AutoComplete) { // 根据用户输入自动过滤下拉
			itemDisplay.setDatasourceInput(ItemDataSourceInput.AutoComplete);
		}

	}// func end

}

