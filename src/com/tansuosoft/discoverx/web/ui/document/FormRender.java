/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import java.util.List;

import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.item.Hidden;
import com.tansuosoft.discoverx.web.ui.item.ItemRender;

/**
 * 文档表单的字段html输出类。
 * 
 * @author coca@tansuosoft.cn
 */
public class FormRender implements HtmlRender {
	private Document m_document = null;
	private DocumentDisplayInfo m_documentDisplayInfo = null;
	private ItemsRender itemsRender = null;
	private ItemsParser itemsParser = null;

	/**
	 * 接收必要属性构造器。
	 * 
	 * @param document
	 * @param documentDisplayInfo
	 */
	public FormRender(Document document, DocumentDisplayInfo documentDisplayInfo) {
		this.m_document = document;
		this.m_documentDisplayInfo = documentDisplayInfo;
		Form form = this.m_document.getForm();
		itemsParser = ItemsParser.getInstance(form);
		itemsRender = ItemsRender.getItemsRender(itemsParser, this.m_document, this.m_documentDisplayInfo);
	}

	/**
	 * 获取绑定的{@link ItemsRender}对象。
	 * 
	 * @return
	 */
	public ItemsRender getItemsRender() {
		return itemsRender;
	}

	/**
	 * 重载render：输出文档字段呈现html文本。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		sb.append(itemsRender.render(jspContext));
		// 字段值校验和转换
		sb.append("\r\n<script type=\"text/javascript\">\r\n");
		sb.append("window.itemsValidate=function(){");
		sb.append("\r\n\tvar r=true;");
		sb.append("\r\n\tif(window.itemValidators){");
		sb.append("\r\n\t\tfor(var i=0;i<window.itemValidators.length;i++){");
		sb.append("\r\n\t\t\tr=r && (window.itemValidators[i])();");
		sb.append("\r\n\t\t}\r\n\t}");
		sb.append("\r\n\treturn r;");
		sb.append("\r\n};");
		sb.append("\r\n</script>");
		sb.append("\r\n<script type=\"text/javascript\">\r\n");
		sb.append("window.itemsTranslate=function(){");
		sb.append("\r\n\tvar r=true;");
		sb.append("\r\n\tif(window.itemTranslators){");
		sb.append("\r\n\t\tfor(var i=0;i<window.itemTranslators.length;i++){");
		sb.append("\r\n\t\t\tr=r && (window.itemTranslators[i])();");
		sb.append("\r\n\t\t}\r\n\t}");
		sb.append("\r\n\treturn r;");
		sb.append("\r\n};");
		sb.append("\r\n</script>");

		// 控制字段
		List<Item> controlItems = itemsParser.getControlItems();
		if (controlItems != null && controlItems.size() > 0) {
			ItemRender hidden = null;
			for (Item x : controlItems) {
				if (x == null || !x.getOutputHiddenField()) continue;
				hidden = new Hidden(new ItemDisplay(new ItemParsed(x)), this.m_document, this.m_documentDisplayInfo);
				sb.append(hidden.render(jspContext));
			}
		}

		return sb.toString();
	}
}

