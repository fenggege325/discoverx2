/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import com.tansuosoft.discoverx.bll.ItemComparator;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemGroup;
import com.tansuosoft.discoverx.model.ItemGroupType;
import com.tansuosoft.discoverx.model.ItemType;
import com.tansuosoft.discoverx.web.ui.Coordinate;

/**
 * 表单显示时提供显示/呈现相关信息的解析器类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemsParser {
	private static Map<String, ItemsParser> m_instances = new HashMap<String, ItemsParser>();

	/**
	 * 接收表单资源的构造器。
	 * 
	 * @param form，可以为null。
	 */
	private ItemsParser(Form form) {
		m_form = form;
		if (m_form == null) return;
		Collection<Item> collection = new TreeSet<Item>(new ItemComparator());
		List<Item> items = form.getItems();
		if (items != null && items.size() > 0) collection.addAll(items);
		List<ItemGroup> groupItems = form.getItemGroups();
		if (groupItems != null && groupItems.size() > 0) {
			for (Item x : groupItems) {
				if (x != null) collection.add(x);
			}
		}
		parse(collection);
	}

	/**
	 * 获取表单对应解析器对象实例。
	 * 
	 * @param form
	 * @return 返回{@link ItemsParser}结果。
	 */
	public synchronized static ItemsParser getInstance(Form form) {
		if (form == null) return null;
		String unid = form.getUNID();
		ItemsParser instance = m_instances.get(unid);
		if (instance == null) {
			instance = new ItemsParser(form);
			m_instances.put(unid, instance);
		}
		return instance;
	}

	/**
	 * 重新解析表单。
	 * 
	 * @param form 需要重新解析的表单。
	 * @return ItemsParser，返回原来的{@link ItemsParser}结果。
	 */
	public static ItemsParser reload(Form form) {
		return reload(form.getUNID());
	}

	/**
	 * 重新解析表单。
	 * 
	 * @param formUNID 需要重新解析的表单的UNID。
	 * @return ItemsParser，返回原来的{@link ItemsParser}结果。
	 */
	public static ItemsParser reload(String formUNID) {
		if (formUNID == null || formUNID.length() == 0) return null;
		return m_instances.remove(formUNID);
	}

	private int m_maxColumn = 0;
	private int m_maxRow = 0;
	private int m_minColumn = 0;
	private List<List<ItemParsed>> m_parsedItems = null;
	private Map<String, ItemParsed> m_parsedControlItemGroups = null;
	private List<Item> m_controlItems = null;
	private Map<String, ItemsParser> m_parsedGroups = null;
	private boolean m_hasAccessory = false;
	private boolean m_hasRTF = false;
	private Item m_accessoryItem = null;
	private Item m_rtfItem = null;
	private Form m_form = null;

	/**
	 * 解析字段内容。
	 * 
	 * @param items 已经排序过的Item集合。
	 */
	protected void parse(java.util.Collection<Item> items) {
		if (items == null || items.isEmpty()) return;

		ItemComparator comparator = new ItemComparator();
		Map<String, Collection<Item>> groups = null;
		m_parsedItems = new ArrayList<List<ItemParsed>>();
		m_parsedControlItemGroups = new HashMap<String, ItemParsed>();
		List<ItemParsed> list = null;
		ItemParsed display = null;
		Coordinate coordinate = null;
		int rowNumber = 0;
		int colNumber = 0;
		int row = -1;
		String groupUnid = null;
		Collection<Item> groupItemList = null;
		ItemGroup itemGroup = null;
		for (Item x : items) {
			if (x == null) continue;
			// 附加文件字段独立处理
			if (x.getType() == ItemType.Accessory) {
				if (!this.m_hasAccessory) {
					this.m_hasAccessory = true;
					this.m_accessoryItem = x;
				} else {
					throw new RuntimeException("一份文档只能包含一个附加文件字段！");
				}
			}
			// RTF字段独立处理
			if (x.getType() == ItemType.RTF) {
				if (!this.m_hasRTF) {
					this.m_hasRTF = true;
					this.m_rtfItem = x;
				} else {
					throw new RuntimeException("一份文档只能包含一个富文本字段！");
				}
			}
			// 控制字段独立处理
			if (x.getControl()) {
				if (m_controlItems == null) m_controlItems = new ArrayList<Item>();
				m_controlItems.add(x);
				continue;
			}

			// 控制类型的字段组
			if (x instanceof ItemGroup) {
				ItemGroup ig = (ItemGroup) x;
				if (ig.getGroupType() == ItemGroupType.Control) {
					m_parsedControlItemGroups.put(x.getUNID(), new ItemParsed(x));
					continue;
				}
			}

			// 分组字段独立处理
			groupUnid = x.getGroup();
			if (this.m_form != null && groupUnid != null && groupUnid.length() > 0) {
				itemGroup = m_form.getItemGroupByUNID(groupUnid);
				// 如果字段所属的字段组类型为控制类型则作为普通字段处理
				// 如果字段所属的字段组不存在也作为普通字段处理
				if (itemGroup != null && itemGroup.getGroupType() != ItemGroupType.Control) {
					if (groups == null) groups = new HashMap<String, Collection<Item>>();
					groupItemList = groups.get(groupUnid);
					if (groupItemList == null) {
						groupItemList = new TreeSet<Item>(comparator);
						groups.put(groupUnid, groupItemList);
					}
					groupItemList.add(x);
					continue;
				}
			}

			display = new ItemParsed(x);
			if (x.getRow() != row) { // 不同行
				rowNumber++;
				if (list != null) m_parsedItems.add(list);
				list = new ArrayList<ItemParsed>();
				if (m_maxColumn < colNumber) m_maxColumn = colNumber;
				if (m_minColumn > colNumber) m_minColumn = colNumber;
				colNumber = 1;
			} else { // 相同行
				colNumber++;
			}
			list.add(display);
			coordinate = new Coordinate(rowNumber, colNumber);
			display.setCoordinate(coordinate);

			row = x.getRow();
		}// for end
		if (list != null) m_parsedItems.add(list);

		// 如果有分组字段
		if (groups != null) {
			ItemsParser parser = null;
			for (String unid : groups.keySet()) {
				groupItemList = groups.get(unid);
				if (m_parsedGroups == null) m_parsedGroups = new HashMap<String, ItemsParser>();
				parser = new ItemsParser(null);
				parser.parse(groupItemList);
				m_parsedGroups.put(unid, parser);
			}
		}
		m_maxRow = rowNumber;
	}

	/**
	 * 返回最大列数。
	 * 
	 * @return
	 */
	public int getMaxColumn() {
		return m_maxColumn;
	}

	/**
	 * 返回最小列数。
	 * 
	 * @return
	 */
	public int getMinColumn() {
		return m_minColumn;
	}

	/**
	 * 返回最大行数。
	 * 
	 * @return
	 */
	public int getMaxRow() {
		return m_maxRow;
	}

	/**
	 * 返回lineNumber指定行的所有{@link ItemParsed}列表集合。
	 * 
	 * <p>
	 * 行数从1开始。
	 * </p>
	 * 
	 * @param lineNumber int
	 * @return
	 */
	public List<ItemParsed> getLineItems(int lineNumber) {
		if (m_parsedItems == null) return null;
		int size = m_parsedItems.size();
		if (lineNumber > size) throw new RuntimeException("指定的行数大于最大行数！");
		if (lineNumber <= 0) throw new RuntimeException("行数必须大于0！");
		return m_parsedItems.get(lineNumber - 1);
	}

	/**
	 * 返回所有控制字段列表集合。
	 * 
	 * @return 没有控制字段则返回null。
	 */
	public List<Item> getControlItems() {
		return this.m_controlItems;
	}

	/**
	 * 返回字段分组解析对象集合。
	 * 
	 * <p>
	 * 返回的集合中，key为字段分组的UNID，value为属于key指定的字段分组的所有字段的解析对象（{@link ItemsParser}）。
	 * </p>
	 * 
	 * @return 没有分组则返回null。
	 */
	public Map<String, ItemsParser> getGroups() {
		return this.m_parsedGroups;
	}

	/**
	 * 是否有附加文件字段。
	 * 
	 * @return
	 */
	public boolean hasAccessory() {
		return this.m_hasAccessory;
	}

	/**
	 * 返回类型为{@link ItemType#Accessory}的字段。
	 * 
	 * @return 必须是{@link ItemsParser#hasAccessory()}为true才有返回值，否则返回null。
	 */
	public Item getAccessoryItem() {
		return this.m_accessoryItem;
	}

	/**
	 * 是否有RTF字段。
	 * 
	 * @return
	 */
	public boolean hasRTF() {
		return this.m_hasRTF;
	}

	/**
	 * 返回类型为{@link ItemType#RTF}的字段。
	 * 
	 * @return 必须是{@link ItemsParser#hasRTF()}为true才有返回值，否则返回null。
	 */
	public Item getRTFItem() {
		return this.m_rtfItem;
	}

	/**
	 * 返回控制类型的字段组解析结果集合。
	 * 
	 * @return Map&lt;String, ItemParsed&gt;，key为控制类型字段组的UNID，value为控制类型字段组对应的{@link ItemParsed}对象。
	 */
	public Map<String, ItemParsed> getControlItemGroups() {
		return this.m_parsedControlItemGroups;
	}
}

