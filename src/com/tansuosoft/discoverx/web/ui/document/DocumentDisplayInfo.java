/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import java.util.List;

import com.tansuosoft.discoverx.model.DocumentDefaultTabConfig;
import com.tansuosoft.discoverx.model.HtmlAttribute;
import com.tansuosoft.discoverx.model.ItemGroupType;
import com.tansuosoft.discoverx.model.Tab;

/**
 * 提供文档在浏览器中显示时相关显示选项信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentDisplayInfo {
	private int m_maxWidth = 0; // 最大显示内容区宽度（像素）。
	private String m_display = null; // 显示方式。
	private List<Tab> m_tabs = null; // Tab配置项。
	private List<HtmlAttribute> m_htmlAttributes = null; // 表单内容在文档中显示时连带输出的html属性。
	private boolean m_readonly = false; // 文档是否只读。
	private boolean m_hasAccessory = false; // 呈现时是否有附加文件控件输出。
	private boolean m_hasSelector = false; // 呈现时是否有显示选择框标记的控件输出。
	private boolean m_hasTranslator = false;// 呈现时是否有字段值转换内容。
	private boolean m_hasValidator = false;// 呈现时是否有字段值校验内容。
	private boolean m_hasLog = true; // 呈现时是否有文档日志控件输出。
	private boolean m_hasOpinion = false; // 呈现时是否有意见控件输出。
	private boolean m_hasOperationUI = false; // 呈现时是否有操作向导控件输出。
	private int m_groupType = 0;// 呈现时有哪些类型的字段组控件。

	/**
	 * 返回最大显示内容区宽度（像素）。
	 * 
	 * @return int
	 */
	public int getMaxWidth() {
		return this.m_maxWidth;
	}

	/**
	 * 设置最大显示内容区宽度（像素）。
	 * 
	 * @param maxWidth int
	 */
	public void setMaxWidth(int maxWidth) {
		this.m_maxWidth = maxWidth;
	}

	/**
	 * 返回显示方式。
	 * 
	 * <p>
	 * 参考{@link com.tansuosoft.discoverx.model.DocumentDisplay#getDisplay()}中的说明。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDisplay() {
		return this.m_display;
	}

	/**
	 * 设置显示方式。
	 * 
	 * @param display String
	 */
	public void setDisplay(String display) {
		this.m_display = display;
	}

	/**
	 * 返回文档{@link Tab}配置项。
	 * 
	 * <p>
	 * 默认为{@link DocumentDefaultTabConfig}
	 * </p>
	 * 
	 * @return List&lt;Tab&gt;
	 */
	public List<Tab> getTabs() {
		return this.m_tabs;
	}

	/**
	 * 设置文档{@link Tab}配置项。
	 * 
	 * @param tabs List&lt;Tab&gt;
	 */
	public void setTabs(List<Tab> tabs) {
		this.m_tabs = tabs;
	}

	/**
	 * 返回表单内容在文档中显示时连带输出的html属性。
	 * 
	 * @return List&lt;HtmlAttribute&gt;
	 */
	public List<HtmlAttribute> getHtmlAttributes() {
		return this.m_htmlAttributes;
	}

	/**
	 * 设置表单内容在文档中显示时连带输出的html属性。
	 * 
	 * @param htmlAttributes List&lt;HtmlAttribute&gt;
	 */
	public void setHtmlAttributes(List<HtmlAttribute> htmlAttributes) {
		this.m_htmlAttributes = htmlAttributes;
	}

	/**
	 * 返回文档是否只读。
	 * 
	 * @return boolean
	 */
	public boolean getReadonly() {
		return this.m_readonly;
	}

	/**
	 * 设置文档是否只读。
	 * 
	 * @param readonly boolean
	 */
	public void setReadonly(boolean readonly) {
		this.m_readonly = readonly;
	}

	/**
	 * 返回呈现时是否有附加文件控件输出。
	 * 
	 * @return boolean
	 */
	public boolean getHasAccessory() {
		return this.m_hasAccessory;
	}

	/**
	 * 设置呈现时是否有附加文件控件输出。
	 * 
	 * @param hasAccessory boolean
	 */
	public void setHasAccessory(boolean hasAccessory) {
		this.m_hasAccessory = hasAccessory;
	}

	/**
	 * 返回呈现时是否有显示选择框标记的控件输出。
	 * 
	 * @return boolean
	 */
	public boolean getHasSelector() {
		return this.m_hasSelector;
	}

	/**
	 * 设置呈现时是否有显示选择框标记的控件输出。
	 * 
	 * @param hasSelector boolean
	 */
	public void setHasSelector(boolean hasSelector) {
		this.m_hasSelector = hasSelector;
	}

	/**
	 * 返回呈现时是否有字段值转换内容。
	 * 
	 * @return boolean
	 */
	public boolean getHasTranslator() {
		return this.m_hasTranslator;
	}

	/**
	 * 设置呈现时是否有字段值转换内容。
	 * 
	 * @param hasTranslator boolean
	 */
	public void setHasTranslator(boolean hasTranslator) {
		this.m_hasTranslator = hasTranslator;
	}

	/**
	 * 返回呈现时是否有字段值校验内容。
	 * 
	 * @return boolean
	 */
	public boolean getHasValidator() {
		return this.m_hasValidator;
	}

	/**
	 * 设置呈现时是否有字段值校验内容。
	 * 
	 * @param hasValidator boolean
	 */
	public void setHasValidator(boolean hasValidator) {
		this.m_hasValidator = hasValidator;
	}

	/**
	 * 返回呈现时有无指定类型的字段组控件输出。
	 * 
	 * @param igt
	 * @return boolean
	 */
	public boolean getHasItemGroupType(ItemGroupType igt) {
		if (igt == null) return false;
		return ((this.m_groupType & igt.getIntValue()) == igt.getIntValue());
	}

	/**
	 * 设置呈现时有哪些类型的字段组控件信息。
	 * 
	 * @param igt ItemGroupType
	 */
	public void setItemGroupType(ItemGroupType igt) {
		if (igt == null) return;
		this.m_groupType += igt.getIntValue();
	}

	/**
	 * 返回呈现时是否有文档日志控件输出。
	 * 
	 * @return boolean
	 */
	public boolean getHasLog() {
		return this.m_hasLog;
	}

	/**
	 * 设置呈现时是否有文档日志控件输出。
	 * 
	 * @param hasLog boolean
	 */
	public void setHasLog(boolean hasLog) {
		this.m_hasLog = hasLog;
	}

	/**
	 * 返回呈现时是否有意见控件输出。
	 * 
	 * @return boolean
	 */
	public boolean getHasOpinion() {
		return this.m_hasOpinion;
	}

	/**
	 * 设置呈现时是否有意见控件输出。
	 * 
	 * @param hasOpinion boolean
	 */
	public void setHasOpinion(boolean hasOpinion) {
		this.m_hasOpinion = hasOpinion;
	}

	/**
	 * 返回呈现时是否有操作向导控件输出。
	 * 
	 * @return boolean
	 */
	public boolean getHasOperationUI() {
		return this.m_hasOperationUI;
	}

	/**
	 * 设置呈现时是否有操作向导控件输出。
	 * 
	 * @param hasOperationUI boolean
	 */
	public void setHasOperationUI(boolean hasOperationUI) {
		this.m_hasOperationUI = hasOperationUI;
	}

}

