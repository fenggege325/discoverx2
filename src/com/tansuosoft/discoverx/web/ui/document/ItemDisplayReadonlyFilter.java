/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 设置是否只读（{@link ItemDisplay#getVisible()}）的{@link ItemDisplayFilter}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemDisplayReadonlyFilter extends ItemDisplayFilter {

	/**
	 * 重载filter
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.document.ItemDisplayFilter#filter(com.tansuosoft.discoverx.web.ui.document.ItemDisplay,
	 *      com.tansuosoft.discoverx.model.Document, com.tansuosoft.discoverx.web.JSPContext)
	 */
	public void filter(ItemDisplay itemDisplay, Document document, JSPContext jspContext) {
		Item item = itemDisplay.getItem();
		if (item == null) return;
		boolean result = false;

		String readOnly = item.getReadonlyExpression();// 获取只读公式
		if (!StringUtil.isNullOrEmpty(readOnly)) {// 如果有只读公式
			ExpressionEvaluator evaluator = new ExpressionEvaluator(readOnly, jspContext.getUserSession(), document);
			result = evaluator.evalBoolean();
			itemDisplay.setReadonly(result);
		} else { // 如果没有只读公式，那么看看是否所属字段组有只读公式。
			ItemDisplay groupItemDisplay = itemDisplay.getGroupItemDisplay();
			if (groupItemDisplay != null) itemDisplay.setReadonly(groupItemDisplay.getReadonly());
		}

		DocumentDisplayInfo documentDisplayInfo = itemDisplay.getDocumentDisplayInfo();
		boolean documentisReadonly = (documentDisplayInfo == null ? false : documentDisplayInfo.getReadonly());
		if (documentisReadonly) itemDisplay.setReadonly(true);
	}
}

