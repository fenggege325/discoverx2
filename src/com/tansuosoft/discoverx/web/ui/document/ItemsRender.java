/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.model.DataSource;
import com.tansuosoft.discoverx.model.DataSourceResource;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemDataSourceInput;
import com.tansuosoft.discoverx.model.ItemType;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.Browser;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlElement;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.item.Hidden;
import com.tansuosoft.discoverx.web.ui.item.ItemRender;
import com.tansuosoft.discoverx.web.ui.item.ItemRenderSelectorProvider;
import com.tansuosoft.discoverx.web.ui.item.TranslationRender;
import com.tansuosoft.discoverx.web.ui.item.ValidationRender;
import com.tansuosoft.discoverx.web.ui.selector.SelectorForm;

/**
 * 输出字段布局和内容html的{@link HtmlRender}实现类。
 * 
 * <p>
 * 用于输出一组关联的字段，比如表单中的所有字段或者同属于一个分组的字段。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemsRender implements HtmlRender {
	private Document m_document = null;
	private DocumentDisplayInfo m_documentDisplayInfo = null;
	private ItemsParser m_itemsParser = null;
	protected static Map<String, ItemDisplay> m_groups = null;

	/**
	 * 表单字段内容左右边距值（必须是偶数）：10。
	 */
	protected static final int DEFAULT_ITEMS_MARGIN = 10;

	private int m_maxWidth = 0; // 实际显示最大可用宽度（像素）。

	/**
	 * 缺省构造器。
	 */
	public ItemsRender() {
	}

	/**
	 * 使用必要参数初始化对象。
	 * 
	 * <p>
	 * 此方法由系统内部使用。
	 * </p>
	 * 
	 * @param itemsParser 字段解析器。
	 * @param document 目标文档。
	 * @param documentDisplayInfo 目标文挡显示选项。
	 */
	public void init(ItemsParser itemsParser, Document document, DocumentDisplayInfo documentDisplayInfo) {
		this.m_itemsParser = itemsParser;
		this.m_document = document;
		this.m_documentDisplayInfo = documentDisplayInfo;
	}

	/**
	 * 获取绑定的{@link ItemsParser}对象。
	 * 
	 * @return
	 */
	public ItemsParser getItemsParser() {
		return m_itemsParser;
	}

	/**
	 * 获取字段所在行容器对应的{@link HtmlElement}。
	 * 
	 * @param lineNumber
	 * @return
	 */
	protected HtmlElement getItemsLineHtmlElement(int lineNumber) {
		HtmlElement el = new HtmlElement("div");
		el.appendAttribute("id", "itemsline_" + lineNumber);
		el.appendAttribute("class", "itemsline");
		return el;
	}

	/**
	 * 获取字段标题和（控件）内容容器对应的{@link HtmlElement}。
	 * 
	 * @param itemDisplay
	 * @return
	 */
	protected HtmlElement getItemBodyHtmlElement(ItemDisplay itemDisplay) {
		HtmlElement el = new HtmlElement("div");
		Item item = itemDisplay.getItem();
		el.appendAttribute("id", "itembody_" + item.getItemName());
		el.appendAttribute("class", "itembody");
		return el;
	}

	/**
	 * 获取字段标题容器对应的{@link HtmlElement}。
	 * 
	 * @param itemDisplay
	 * @return
	 */
	protected HtmlElement getItemTitleHtmlElement(ItemDisplay itemDisplay) {
		HtmlElement el = new HtmlElement("div");
		Item item = itemDisplay.getItem();
		el.appendAttribute("id", "itemtitle_" + item.getItemName());
		el.appendAttribute("class", "itemtitle");
		HtmlElement label = new HtmlElement("label");
		label.appendAttribute("for", item.getItemName());
		String desc = item.getDescription();
		if (desc != null && desc.length() > 0) label.appendAttribute("title", desc);
		String c = item.getCaption();
		if (c == null || c.trim().length() == 0) {
			label.setInnerHTML(c.replace(" ", "&nbsp;"));
		} else {
			label.setInnerHTML(c + (itemDisplay.getRequired() ? "<span>*</span>" : "") + ":");
		}
		el.appendSubElement(label);
		return el;
	}

	/**
	 * 获取字段内容控件容器对应的{@link HtmlElement}。
	 * 
	 * @param itemDisplay
	 * @return
	 */
	protected HtmlElement getItemControlHtmlElement(ItemDisplay itemDisplay) {
		HtmlElement el = new HtmlElement("div");
		Item item = itemDisplay.getItem();
		el.appendAttribute("id", "itemcontrol_" + item.getItemName());
		el.appendAttribute("class", "itemcontrol");
		return el;
	}

	/**
	 * 返回实际显示最大可用宽度（像素）。
	 * 
	 * <p>
	 * 默认为0，表示从{@link DocumentDisplayInfo#getMaxWidth()}中获取最大可用宽度，如果返回大于零的值，则优先使用返回的值。
	 * </p>
	 * 
	 * @return int
	 */
	public int getMaxWidth() {
		return this.m_maxWidth;
	}

	/**
	 * 设置实际显示最大可用宽度（像素）。
	 * 
	 * @param maxWidth int
	 */
	public void setMaxWidth(int maxWidth) {
		this.m_maxWidth = maxWidth;
	}

	/**
	 * 重载render：输出文档字段呈现html文本。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		Form form = this.m_document.getForm();

		StringBuilder sb = new StringBuilder();
		StringBuilder sbl = new StringBuilder();
		StringBuilder sbv = new StringBuilder();
		StringBuilder sbt = new StringBuilder();

		int formConfigCaptionWidth = form.getItemCaptionWidth();
		if (formConfigCaptionWidth <= 0) formConfigCaptionWidth = Form.DEFAULT_ITEMCAPTION_WIDTH;
		int lineColumn = 0; // 某一行中的列数
		int maxRow = m_itemsParser.getMaxRow(); // 最大行数
		int maxWidth = getMaxWidth();
		if (maxWidth == 0) maxWidth = m_documentDisplayInfo.getMaxWidth() - DEFAULT_ITEMS_MARGIN; // 最大宽度
		int width = 0; // 单个字段宽度
		int captionWidth = 0; // 单个字段标签宽度
		int controlWidth = 0; // 单个字段内容/控件宽度
		int configWidth = 0; // 配置的总宽度值
		int configCaptionWidth = 0; // 配置的标题宽度值
		int sumWidth = 0; // 用于累积宽度
		int height = 0; // 行高

		ItemRender itemRender = null; // 字段呈现实现对象
		ItemDisplay itemDisplay = null; // 字段内容呈现信息对象
		HtmlElement lineElement = null; // 字段所在行html容器
		HtmlElement itemBodyElement = null; // 字段本身html容器
		HtmlElement itemTitleElement = null; // 字段标题html容器
		HtmlElement itemControlElement = null; // 字段控件内容html容器
		List<ItemParsed> lineItems = null; // 某一行的所有字段信息
		Item item = null; // 字段资源对象
		ItemParsed itemParsed = null; // 解析后的字段对象
		boolean isLast = false; // 是否某一行包含的字段列中最后一个字段
		boolean isPercentage = (form.getMeasurement() == SizeMeasurement.Percentage); // 宽度是否以百分比为单位
		boolean showIndependentTitle = false; // 是否显示独立字段标题行
		boolean visible = true;
		String itemGroup = null; // 字段所属分组unid
		Map<String, ItemsParser> groupsMap = m_itemsParser.getGroups();
		Map<String, ItemParsed> controlGroupsMap = m_itemsParser.getControlItemGroups();
		for (int i = 1; i <= maxRow; i++) { // 按行顺序循环
			lineElement = this.getItemsLineHtmlElement(i);
			lineElement.appendAttribute("style", "width:" + maxWidth + "px;");
			lineItems = m_itemsParser.getLineItems(i);
			lineColumn = lineItems.size();
			sumWidth = 0;
			for (int j = 0; j < lineColumn; j++) { // 按列顺序循环
				isLast = (j == (lineColumn - 1));
				height = 0;
				itemParsed = lineItems.get(j);
				item = itemParsed.getItem();
				itemDisplay = new ItemDisplay(itemParsed);
				itemDisplay.setDocumentDisplayInfo(this.m_documentDisplayInfo);
				itemDisplay.setRowNumber(i);
				itemDisplay.setColumnCount(lineColumn);
				itemDisplay.setColumnNumber(j + 1);
				// 分组字段
				if (item.getType() == ItemType.Group && groupsMap != null) {
					if (m_groups == null) m_groups = new HashMap<String, ItemDisplay>(groupsMap.size());
					m_groups.put(item.getUNID(), itemDisplay);
				}
				itemGroup = item.getGroup();
				if (itemGroup != null && itemGroup.length() > 0) {
					ItemDisplay gid = (m_groups == null ? null : m_groups.get(itemGroup));
					if (gid == null && controlGroupsMap != null) {
						ItemParsed gip = controlGroupsMap.get(itemGroup);
						if (gip != null) {
							gid = new ItemDisplay(gip);
							itemDisplayFilter(gid, m_document, jspContext);
						}
					}
					itemDisplay.setGroupItemDisplay(gid);
				}
				// 呈现属性过滤
				itemDisplayFilter(itemDisplay, m_document, jspContext);

				visible = itemDisplay.getVisible();

				showIndependentTitle = (item.getCaptionWidth() >= 0 && item.getCaptionHeight() > 0);
				// 高度
				if (height < item.getHeight()) height = item.getHeight();
				if (height > 0) itemDisplay.setContentHeight(height);

				// 宽度计算
				configWidth = item.getWidth();
				configCaptionWidth = item.getCaptionWidth();
				if (isPercentage) { // 单位为百分比
					if (configWidth == 0) { // 自动计算宽度
						width = (maxWidth / lineColumn) + (isLast ? (maxWidth % lineColumn) : 0);
					} else { // 手工输入百分比
						width = (maxWidth * configWidth / 100) + (isLast ? (maxWidth - (sumWidth + (maxWidth * configWidth / 100))) : 0);
						sumWidth += width;
					}
				} else { // 单位为像素
					if (configWidth == 0) { // 自动计算宽度
						width = (maxWidth / lineColumn) + (isLast ? (maxWidth % lineColumn) : 0);
					} else { // 手工输入像素
						width = configWidth;
						sumWidth += width;
						if (isLast) width += (maxWidth - sumWidth);
					}
				}// else end
					// 字段标题宽度
				captionWidth = 0;
				if (!showIndependentTitle) {
					if (configCaptionWidth > 0) {
						captionWidth = configCaptionWidth;
					} else if (configCaptionWidth == 0) {
						captionWidth = formConfigCaptionWidth;
					} else {
						captionWidth = 0;
					}
				} else {
					captionWidth = 0;
				}

				controlWidth = (width - captionWidth - (showIndependentTitle && !isLast ? 5 : 0));

				itemBodyElement = this.getItemBodyHtmlElement(itemDisplay);
				itemBodyElement.appendAttribute("style", "width:" + width + "px;");

				if (!visible) {
					itemBodyElement.appendAttribute("style", "display:none;");
					if (lineColumn == 1) lineElement.appendAttribute("style", "display:none;");
				}

				// 字段标题输出
				itemTitleElement = null;
				if (showIndependentTitle) {
					itemTitleElement = this.getItemTitleHtmlElement(itemDisplay);
					itemTitleElement.appendAttribute("style", "width:100%");
				} else if (captionWidth > 0) {
					itemTitleElement = this.getItemTitleHtmlElement(itemDisplay);
					itemTitleElement.appendAttribute("style", "width:" + captionWidth + "px;");
				}
				// 编辑/选择框打开热点
				if (itemTitleElement != null && ItemDataSourceInput.SelectorHotspot == itemDisplay.getDatasourceInput()) {
					SelectorForm selectorForm = ItemRenderSelectorProvider.getSelectorForm(itemDisplay, jspContext);
					HtmlElement selectorHotspot = ItemRenderSelectorProvider.getSelectorHtmlElement("div", selectorForm, item.getType() == ItemType.Link ? "[编辑]" : "[选择]");
					if (selectorHotspot != null) {
						itemTitleElement.appendSubElement(selectorHotspot);
						m_documentDisplayInfo.setHasSelector(true);
					}
				}

				// 字段控件容器
				itemDisplay.setContentWidth(controlWidth);
				itemControlElement = this.getItemControlHtmlElement(itemDisplay);
				itemControlElement.appendAttribute("style", "width:" + controlWidth + "px;");

				// 字段控件
				itemRender = null;
				if (visible) {
					itemRender = ItemRender.getItemRender(itemDisplay, m_document, m_documentDisplayInfo);
				} else {
					if (item.getOutputHiddenField()) itemRender = new Hidden(itemDisplay, this.m_document, this.m_documentDisplayInfo);
				}
				itemControlElement.setInnerHTML(itemRender != null ? itemRender.render(jspContext) : "");

				// 整合
				if (itemTitleElement != null) itemBodyElement.appendSubElement(itemTitleElement);
				itemBodyElement.appendSubElement(itemControlElement);
				lineElement.appendSubElement(itemBodyElement);

				// 校验和转换
				if (!itemDisplay.getReadonly()) {
					String jsv = new ValidationRender(item).render(jspContext);
					String jst = new TranslationRender(item).render(jspContext);
					if (jsv != null && jsv.length() > 0) sbv.append(sbv.length() > 0 ? "," : "").append("\r\n\t\t").append(jsv);
					if (jst != null && jst.length() > 0) sbt.append(sbt.length() > 0 ? "," : "").append("\r\n\t\t").append(jst);
				}
				// 关联选择
				String linkageInfoJs = this.buildLinkageInfoJS(item);
				if (linkageInfoJs != null && linkageInfoJs.length() > 0) sbl.append(sbl.length() > 0 ? "\r\n" : "").append(linkageInfoJs);
			}// for j end
			if (height > 0) {
				// String cssHeight = (jspContext.getBrowser() == Browser.IE6 ? "_" : "min-") + "height:" + height + "px;";
				String cssHeight = (jspContext.getBrowser().check(Browser.MSIE, 6) ? "_" : "min-") + "height:" + height + "px;";
				lineElement.appendAttribute("style", cssHeight);
				itemControlElement.appendAttribute("style", cssHeight);
			}
			sb.append(lineElement.render(jspContext));
		}// for i end

		// 校验、转换、关联选择
		if (sbv.length() > 0 || sbt.length() > 0 || sbl.length() > 0) {
			sb.append("\r\n<script type=\"text/javascript\">");
			if (sbv.length() > 0) {
				sb.append("\r\nif(!window.itemValidators) window.itemValidators=[];\r\nwindow.itemValidators.push(function(){\r\n\treturn validator.validate([").append(sbv.toString()).append("\r\n\t]);\r\n});\r\n");
				m_documentDisplayInfo.setHasValidator(true);
			}
			if (sbt.length() > 0) {
				sb.append("\r\nif(!window.itemTranslators) window.itemTranslators=[];\r\nwindow.itemTranslators.push(function(){\r\n\treturn translator.translate([").append(sbt.toString()).append("\r\n\t]);\r\n});\r\n");
				m_documentDisplayInfo.setHasTranslator(true);
			}
			if (sbl.length() > 0) sb.append("\r\nEVENT.add(window,'load',function(){\r\n").append(sbl.toString()).append("});\r\n");
			sb.append("</script>\r\n");
		}

		return sb.toString();
	}

	/**
	 * 构造并返回指定参与联动选择字段的客户端初始化json信息。
	 * 
	 * @param item 参与联动选择的字段资源对象。
	 * @param isRoot 参与联动选择的字段资源是否为第一级（根字段）字段。
	 * @return String 返回构造好的json文本。
	 */
	protected String getLinkageInfo(Item item, boolean isRoot) {
		if (item == null) return "";
		StringBuilder sb = new StringBuilder();
		sb.append("\r\n\t{");
		sb.append("id:'").append(item.getItemName()).append("'");
		sb.append(",").append("value:'").append(StringUtil.encode4Json(this.m_document.getItemValue(item.getItemName()))).append("'");
		String valueStoreItem = item.getValueStoreItem();
		if (valueStoreItem != null && valueStoreItem.length() > 0) sb.append(",").append("valueStoreItem:'").append(valueStoreItem).append("'");
		if (!StringUtil.isBlank(item.getDelimiter())) sb.append(",").append("delimiter:'").append(item.getDelimiter()).append("'");
		if (isRoot) {
			DataSource ds = item.getDataSource();
			if (ds != null && (ds instanceof DataSourceResource)) {
				DataSourceResource dsr = (DataSourceResource) ds;
				Resource r = ResourceContext.getInstance().getResource(dsr.getUNID(), dsr.getDirectory());
				if (r == null) r = ResourceContext.getInstance().getResource(ResourceAliasContext.getInstance().getUNIDByAlias(dsr.getDirectory(), dsr.getUNID()), dsr.getDirectory());
				if (r != null) {
					sb.append(",").append("datasource:{");
					sb.append("x:'").append(dsr.getDirectory()).append("://").append(dsr.getUNID()).append("'");
					sb.append(",name:'").append(r.getName()).append("'");
					sb.append(",titlePropName:'").append(dsr.getTitlePropName()).append("'");
					sb.append(",valuePropName:'").append(dsr.getValuePropName()).append("'");
					sb.append("}");
				}
			}
		}
		sb.append("}");
		return sb.toString();
	}

	/**
	 * 构造并返回指定字段配置的联动选择信息客户端初始化js代码。
	 * 
	 * @return String
	 */
	protected String buildLinkageInfoJS(Item item) {
		if (item == null) return "";
		String linkageItemNames = item.getLinkageItem();
		if (linkageItemNames == null || linkageItemNames.length() == 0) return "";
		String[] lins = StringUtil.splitString(linkageItemNames, ';');
		if (lins == null || lins.length == 0) return "";
		StringBuilder sbl = new StringBuilder();
		sbl.append(getLinkageInfo(item, true));
		Item li = null;
		for (String lin : lins) {
			if (lin == null || lin.length() == 0) continue;
			li = this.m_document.getItem(lin);
			if (li == null) continue;
			sbl.append(",");
			sbl.append(getLinkageInfo(li, false));
		}
		if (sbl.length() > 0) {
			sbl.insert(0, "linkageActions.setLinkageInfo([");
			sbl.append("\r\n]);");
		}
		return sbl.toString();
	}

	/**
	 * 获取显示目标文挡字段内容的{@link ItemsRender}对象实例。
	 * 
	 * @param parser 字段解析器{@link ItemsParser}。
	 * @param doc 目标文档。
	 * @param ddi 目标文挡显示选项。
	 * @return ItemsRender 如果文档有自定义的实现类，则返回其对象，如果没有则返回默认的{@link ItemsRender}对象。
	 */
	public static ItemsRender getItemsRender(ItemsParser parser, Document doc, DocumentDisplayInfo ddi) {
		ItemsRender render = null;
		String display = ddi.getDisplay();
		if (!StringUtil.isBlank(display) && display.indexOf(".jsp") < 0) {
			render = Instance.newInstance(display, ItemsRender.class);
		}
		if (render == null) render = new ItemsRender();
		render.init(parser, doc, ddi);
		return render;
	}

	protected static final ItemDisplayFilter readonlyFilter = new ItemDisplayReadonlyFilter(); // 只读设置
	protected static final ItemDisplayFilter visibileFilter = new ItemDisplayVisibleFilter(); // 可见设置
	protected static final ItemDisplayFilter datasourceInputFilter = new ItemDisplayDatasourceInputFilter(); // 数据源输入方式设置

	/**
	 * 执行呈现属性过滤操作。
	 * 
	 * @param itemDisplay
	 * @param doc
	 * @param jspContext
	 */
	protected static void itemDisplayFilter(ItemDisplay itemDisplay, Document doc, JSPContext jspContext) {
		readonlyFilter.filter(itemDisplay, doc, jspContext);
		visibileFilter.filter(itemDisplay, doc, jspContext);
		datasourceInputFilter.filter(itemDisplay, doc, jspContext);
	}
}

