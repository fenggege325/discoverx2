/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemDataSourceInput;
import com.tansuosoft.discoverx.web.ui.Coordinate;

/**
 * 字段显示/呈现信息类。
 * 
 * <p>
 * 它包含要显示/呈现在网页上的字段本身和额外的信息。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ItemDisplay {
	private ItemParsed m_itemParsed = null;

	/**
	 * 接收{@link ItemParsed}的构造器。
	 * 
	 * @param itemParsed ItemParsed
	 */
	public ItemDisplay(ItemParsed itemParsed) {
		this.m_itemParsed = itemParsed;
	}

	private boolean m_visible = true; // 是否可显示。
	private boolean m_readonly = false; // 是否只读。
	private ItemDataSourceInput m_datasourceInput = null; // 实际使用的数据源输入方式。
	private DocumentDisplayInfo m_documentDisplayInfo = null; // 绑定的文档显示选项。
	private ItemDisplay m_groupItemDisplay = null; // 所属的字段组对应的{@link ItemDisplay}。
	private int m_contentWidth = 0; // 字段控件最大像素宽度。
	private int m_contentHeight = 0; // 字段控件最大像素高度。
	private int m_rowNumber = 0; // 字段控件所在行序号。
	private int m_columnCount = 0; // 字段控件所在行包含的列个数
	private int m_columnNumber = 0; // 字段控件所在行包含的列序号

	/**
	 * 返回是否可显示。
	 * 
	 * @return boolean 是否可显示。
	 */
	public boolean getVisible() {
		return this.m_visible;
	}

	/**
	 * 设置是否可显示。
	 * 
	 * @param visible boolean 是否可显示。
	 */
	public void setVisible(boolean visible) {
		this.m_visible = visible;
	}

	/**
	 * 返回是否只读。
	 * 
	 * @return boolean 是否只读。
	 */
	public boolean getReadonly() {
		return this.m_readonly;
	}

	/**
	 * 设置是否只读。
	 * 
	 * @param readonly boolean 是否只读。
	 */
	public void setReadonly(boolean readonly) {
		this.m_readonly = readonly;
	}

	/**
	 * 返回绑定的文档显示选项。
	 * 
	 * @return DocumentDisplayInfo
	 */
	public DocumentDisplayInfo getDocumentDisplayInfo() {
		return this.m_documentDisplayInfo;
	}

	/**
	 * 设置绑定的文档显示选项。
	 * 
	 * @param documentDisplayInfo DocumentDisplayInfo
	 */
	public void setDocumentDisplayInfo(DocumentDisplayInfo documentDisplayInfo) {
		this.m_documentDisplayInfo = documentDisplayInfo;
	}

	/**
	 * 返回所属的字段组对应的{@link ItemDisplay}。
	 * 
	 * <p>
	 * 没有属于任何字段组的字段则返回null。
	 * </p>
	 * 
	 * @return ItemDisplay
	 */
	public ItemDisplay getGroupItemDisplay() {
		return this.m_groupItemDisplay;
	}

	/**
	 * 设置所属的字段组对应的{@link ItemDisplay}。
	 * 
	 * @param groupItemDisplay ItemDisplay
	 */
	public void setGroupItemDisplay(ItemDisplay groupItemDisplay) {
		this.m_groupItemDisplay = groupItemDisplay;
	}

	/**
	 * 返回字段控件最大像素宽度。
	 * 
	 * @return int
	 */
	public int getContentWidth() {
		return this.m_contentWidth;
	}

	/**
	 * 设置字段控件最大像素宽度。
	 * 
	 * @param contentWidth int
	 */
	public void setContentWidth(int contentWidth) {
		this.m_contentWidth = contentWidth;
	}

	/**
	 * 返回字段控件最大像素高度。
	 * 
	 * <p>
	 * 默认为0表示自动高度。
	 * </p>
	 * 
	 * @return int
	 */
	public int getContentHeight() {
		return this.m_contentHeight;
	}

	/**
	 * 设置字段控件最大像素高度。
	 * 
	 * @param contentHeight int
	 */
	public void setContentHeight(int contentHeight) {
		this.m_contentHeight = contentHeight;
	}

	/**
	 * 调用{@link ItemParsed}同名方法并返回。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.web.ui.document.ItemParsed#getCoordinate()
	 */
	public Coordinate getCoordinate() {
		return m_itemParsed.getCoordinate();
	}

	/**
	 * 调用{@link ItemParsed}同名方法并返回。
	 * 
	 * @param coordinate
	 * @see com.tansuosoft.discoverx.web.ui.document.ItemParsed#setCoordinate(com.tansuosoft.discoverx.web.ui.Coordinate)
	 */
	public void setCoordinate(Coordinate coordinate) {
		m_itemParsed.setCoordinate(coordinate);
	}

	/**
	 * 返回实际使用的数据源/编辑框显示方式。
	 * 
	 * @return {@link ItemDataSourceInput}，如果没有有效的数据源输入方式，则返回null。
	 */
	public ItemDataSourceInput getDatasourceInput() {
		return this.m_datasourceInput;
	}

	/**
	 * 设置实际使用的数据源/编辑框显示方式。
	 * 
	 * @param datasourceInput {@link ItemDataSourceInput}
	 */
	public void setDatasourceInput(ItemDataSourceInput datasourceInput) {
		this.m_datasourceInput = datasourceInput;
	}

	/**
	 * 调用{@link ItemParsed}同名方法并返回。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.web.ui.document.ItemParsed#getItem()
	 */
	public Item getItem() {
		return m_itemParsed.getItem();
	}

	/**
	 * 返回是否必填标记。
	 * 
	 * @return
	 */
	public boolean getRequired() {
		Item item = this.getItem();
		if (item == null) return false;
		String jsValidator = item.getJsValidator();
		if (jsValidator != null && jsValidator.indexOf("validator.required") >= 0) return true;
		return item.getRequired();
	}

	/**
	 * 返回字段控件呈现时所在行序号。
	 * 
	 * <p>
	 * 序号从1开始。
	 * </p>
	 * 
	 * @return int
	 */
	public int getRowNumber() {
		return this.m_rowNumber;
	}

	/**
	 * 设置字段控件呈现时所在行序号。
	 * 
	 * @param rowNumber int
	 */
	protected void setRowNumber(int rowNumber) {
		this.m_rowNumber = rowNumber;
	}

	/**
	 * 返回字段控件呈现时所在行包含的列个数。
	 * 
	 * @return int
	 */
	public int getColumnCount() {
		return this.m_columnCount;
	}

	/**
	 * 设置字段控件呈现时所在行包含的列个数。
	 * 
	 * @param columnCount int
	 */
	protected void setColumnCount(int columnCount) {
		this.m_columnCount = columnCount;
	}

	/**
	 * 返回字段控件呈现时所在行包含的列序号
	 * 
	 * <p>
	 * 序号从1开始。
	 * </p>
	 * 
	 * @return int
	 */
	public int getColumnNumber() {
		return this.m_columnNumber;
	}

	/**
	 * 设置字段控件呈现时所在行包含的列序号
	 * 
	 * @param columnNumber int
	 */
	protected void setColumnNumber(int columnNumber) {
		this.m_columnNumber = columnNumber;
	}
}

