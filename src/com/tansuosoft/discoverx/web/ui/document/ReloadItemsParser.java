/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Form;

/**
 * 表单保存后，更新表单字段解析缓存的事件处理实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ReloadItemsParser implements EventHandler {
	/**
	 * 缺省构造器。
	 */
	public ReloadItemsParser() {
	}

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		ResourceEventArgs resourceEvent = (ResourceEventArgs) e;
		Form form = (Form) resourceEvent.getResource();
		if (form == null) return;
		ItemsParser.reload(form);
	}

}

