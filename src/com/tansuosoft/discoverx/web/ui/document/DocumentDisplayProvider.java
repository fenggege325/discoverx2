/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentDisplay;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 提供文档显示选项{@link com.tansuosoft.discoverx.model.DocumentDisplay}的类.
 * 
 * @author coca@tansuosoft.cn
 */
class DocumentDisplayProvider {
	/**
	 * 缺省构造器。
	 */
	private DocumentDisplayProvider() {
	}

	/**
	 * 为指定文档提供{@link DocumentDisplay}对象。
	 * 
	 * @param document
	 * @param jspContext
	 * @return
	 */
	public static DocumentDisplay provide(Document document, JSPContext jspContext) {
		int appCnt = document.getApplicationCount();
		Application app = document.getApplication(appCnt);
		DocumentDisplay display = (app == null ? null : app.getDocumentDisplay());
		// for (int i = appCnt; i >= 1; i--) {
		// app = document.getApplication(i);
		// if (app != null) {
		// display = app.getDocumentDisplay();
		// if (display != null) break;
		// }
		// }
		if (display == null) return null;
		String formula = display.getExpression();
		if (formula != null && formula.length() > 0) {
			ExpressionEvaluator evaluator = new ExpressionEvaluator(formula, jspContext.getUserSession(), document);
			if (!evaluator.evalBoolean()) return null;
		}

		return display;
	}
}

