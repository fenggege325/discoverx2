/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 用于处理字段呈现前设置其对应的({@link ItemDisplay})中某些属性的过滤器基类。
 * 
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ItemDisplayFilter {
	/**
	 * 缺省构造器。
	 */
	public ItemDisplayFilter() {
	}

	/**
	 * 设置{@link ItemDisplay}的某个属性值。
	 * 
	 * @param itemDisplay
	 * @param document
	 * @param jspContext
	 */
	public abstract void filter(ItemDisplay itemDisplay, Document document, JSPContext jspContext);
}

