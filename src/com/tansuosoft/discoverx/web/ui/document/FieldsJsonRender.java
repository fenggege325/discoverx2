/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import java.util.List;

import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Field;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemType;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 文档字段json内容对应的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class FieldsJsonRender implements HtmlRender {
	private Document m_document = null;

	/**
	 * 接收文档资源的构造器。
	 * 
	 * @param document
	 */
	public FieldsJsonRender(Document document) {
		m_document = document;
		if (m_document == null) throw new IllegalArgumentException("必须提供有效文档资源！");
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		try {
			StringBuilder sb = new StringBuilder();
			List<Field> fields = m_document.getFields();
			if (fields == null || fields.isEmpty()) return sb.toString();
			String val = null;
			String caption = null;
			int count = 0;
			StringBuilder captions = new StringBuilder();
			StringBuilder names = new StringBuilder();
			Item item = null;
			for (Field x : fields) {
				if (x == null) continue;

				names.append(names.length() > 0 ? ",\r\n" : "\r\n").append("'").append(x.getName()).append("'");
				sb.append(count > 0 ? ",\r\n" : "\r\n");
				sb.append(x.getName()).append(":");
				sb.append("{");
				caption = StringUtil.encode4Json(x.getCaption());
				captions.append(captions.length() > 0 ? ",\r\n" : "\r\n").append("'").append(caption).append("'");
				sb.append("caption:").append("'").append(caption).append("'");
				// 附件的值特殊处理。
				item = x.getItem();
				if (item != null && item.getType() == ItemType.Accessory) {
					val = "";
					List<Accessory> accessories = m_document.getAccessories();
					if (accessories != null && !accessories.isEmpty()) {
						StringBuilder sba = new StringBuilder();
						AccessoryType at = null;
						for (Accessory a : accessories) {
							if (a == null) continue;
							at = a.getAccessoryTypeResource();
							if (at != null && (at.getRestrictToConfig() || !at.getVisible())) continue;
							sba.append(sba.length() > 0 ? ";" : "").append(a.getName());
						}
						val = sba.toString();
					}
				} else {
					val = StringUtil.getValueString(x.getValue(), m_document.getItemValue(x.getName()));
				}
				if (val == null) {
					val = "";
				} else {
					val = StringUtil.encode4Json(val);
					val = val.replace("\r\n", "\\r\\n").replace("\r", "\\r").replace("\n", "\\n");
				}
				sb.append(",").append("value:").append("'").append(val).append("'");
				sb.append("}");
				count++;
			}
			sb.append(",").append("\r\nfieldNames:[").append(names.toString()).append("\r\n]");
			sb.append(",").append("\r\nfieldCaptions:[").append(captions.toString()).append("\r\n]");
			sb.append(",").append("\r\nfieldCount:").append(count);
			return sb.toString();
		} catch (Exception ex) {
			return "error:true,message:'" + StringUtil.encode4Json(ex.getMessage()) + "'";
		}
	}
}

