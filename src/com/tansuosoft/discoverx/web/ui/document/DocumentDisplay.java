/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import java.io.File;
import java.util.List;

import com.tansuosoft.discoverx.bll.function.OperationParser;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.HtmlAttribute;
import com.tansuosoft.discoverx.model.ItemGroupType;
import com.tansuosoft.discoverx.model.Tab;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.PathContext;

/**
 * 封装文档呈现相关初始化过程并在不同呈现页面中传递呈现相关参数的类。
 * 
 * @author coca@tansuosoft.cn
 */
public final class DocumentDisplay {
	private Document m_document = null;
	private DocumentDisplayInfo documentDisplayInfo = null;
	private String m_formStyle = null;
	private String m_extraStyle = null;
	protected static final String DEFAULT_FORM_CSS_FILE = "documentform_default.css";
	private OperationParser m_operationParser = null; // 当前文档对应的操作信息。
	private String m_appjsref = null; // 额外应用js引用。
	private String m_applicationSpecialUrlBase = null; // 应用程序特定功能和文件url根路径。
	private String m_metaInfo = ""; // html页面head部分的额外meta信息。
	private String m_bodyInfo = ""; // html页面body部分的额外属性信息。

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param document
	 * @param jspContext
	 */
	public DocumentDisplay(Document document, JSPContext jspContext) {
		PathContext pathContext = jspContext.getPathContext();
		m_document = document;
		documentDisplayInfo = DocumentDisplayInfoProvider.provide(this.m_document, jspContext);
		Application app = this.m_document.getApplication(m_document.getApplicationCount());
		m_applicationSpecialUrlBase = String.format("%1$s%2$s/", pathContext.getFramePath(), app.getAlias().toLowerCase());

		// 应用程序中定义的特有css、js、meta、额外body属性等
		List<HtmlAttribute> htmlAtts = documentDisplayInfo.getHtmlAttributes();
		if (htmlAtts != null) {
			StringBuilder sbMeta = new StringBuilder();
			StringBuilder sbBody = new StringBuilder();
			for (HtmlAttribute ha : htmlAtts) {
				String v = ha.getValue();
				if (v == null || v.length() == 0) continue;
				String n = ha.getName();
				if (HtmlAttribute.JS_ATTRIBUTE_NAME.equalsIgnoreCase(n)) {
					m_appjsref = pathContext.parseUrl(v);
					if (m_appjsref.indexOf('/') < 0) m_appjsref = m_applicationSpecialUrlBase + m_appjsref;
				} else if (HtmlAttribute.CSS_ATTRIBUTE_NAME.equalsIgnoreCase(n)) {
					m_extraStyle = pathContext.parseUrl(v);
					if (m_extraStyle.indexOf('/') < 0) m_extraStyle = m_applicationSpecialUrlBase + m_extraStyle;
				} else if (n != null && n.startsWith(HtmlAttribute.META_ATTRIBUTE_NAME)) {
					sbMeta.append(sbMeta.length() > 0 ? "\r\n" : "");
					sbMeta.append("<meta ").append(v).append("/>");
				} else {
					sbBody.append(sbBody.length() > 0 ? " " : "");
					sbBody.append(n).append("=").append("\"").append(v).append("\"");
				}
			}
			if (sbMeta.length() > 0) {
				sbMeta.append("\r\n");
				m_metaInfo = sbMeta.toString();
			}
			if (sbBody.length() > 0) {
				sbBody.insert(0, " ");
				m_bodyInfo = sbBody.toString();
			}
		}

		// 表单自定义css显示样式
		Form f = m_document.getForm();
		String style = f.getStyle();
		if (style == null || style.trim().length() == 0) {
			m_formStyle = pathContext.getThemePath() + DEFAULT_FORM_CSS_FILE;
		} else {
			m_formStyle = pathContext.parseUrl(style);
			if (m_formStyle.indexOf('/') < 0) m_formStyle = m_applicationSpecialUrlBase + m_formStyle;
		}

		// 表单特有js
		if (m_appjsref == null || m_appjsref.length() == 0) {
			String frame = jspContext.getPortal().getFrame();
			String fn = String.format("%1$s%2$s%3$s%4$s%5$s%6$s.js", CommonConfig.getInstance().getInstallationPath(), frame, File.separator, app.getAlias().toLowerCase(), File.separator, f.getAlias().toLowerCase());
			File file = new File(fn);
			if (file.exists() && file.isFile()) {
				m_appjsref = String.format("%1$s%2$s.js", m_applicationSpecialUrlBase, f.getAlias().toLowerCase());
			}
		}
	}

	/**
	 * 返回当前文档使用的{@link DocumentDisplayInfo}。
	 * 
	 * @return
	 */
	public DocumentDisplayInfo getDocumentDisplayInfo() {
		return documentDisplayInfo;
	}

	/**
	 * 返回当前呈现的文档。
	 * 
	 * @return
	 */
	public Document getDocument() {
		return this.m_document;
	}

	/**
	 * 获取文档所属应用程序特定功能url根路径。
	 * 
	 * <p>
	 * 应用程序特定功能url根路径为当前页面框架（{frame}）下以转化为小写字母的应用程序别名作为子目录的路径。<br/>
	 * 比如当前页面框架url为：“/discoverx2/frame/”，应用程序别名为：appTest，则返回“/discoverx2/frame/apptest/”。<br/>
	 * 参考{@link Application}中的说明。
	 * </p>
	 * <p>
	 * 应用程序扩展或特有功能对应的文件类型说明：
	 * <ul>
	 * <li>以“应用程序特定功能目录”作为存放应用程序扩展或特有功能对应的所有web可引用文件；</li>
	 * <li>常见的应用程序扩展或特有功能有：
	 * <ul>
	 * <li>js脚本文件（包括表单、操作、视图等的特有js等）；</li>
	 * <li>css样式文件；</li>
	 * <li>图片文件；</li>
	 * <li>操作向导相关文件；</li>
	 * <li>文档自定义显示页面；</li>
	 * <li>其它各类文件...</li>
	 * </ul>
	 * </li>
	 * <li>这些文件的目录结构和命名可以自由规划；</li>
	 * <li>导出完整应用程序时将自动导出这些文件。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return
	 */
	public String getApplicationSpecialUrlBase() {
		return m_applicationSpecialUrlBase;
	}

	/**
	 * 获取文档表单信息呈现时所使用的样式文件（css）url路径。
	 * 
	 * <p>
	 * 获取css引用的步骤：<br/>
	 * 1.如果文档表单的{@link Form#getStyle()}有有效返回值，则返回之；<br/>
	 * 2.否则返回系统默认使用的“documentform_default.css”样式。
	 * </p>
	 * 
	 * @return
	 */
	public String getFormStyle() {
		return m_formStyle;
	}

	/**
	 * 获取文档所属应用程序特有的javascript引用url路径。
	 * 
	 * <p>
	 * 获取js引用的步骤：<br/>
	 * 1.如果有通过文档所属应用程序的{@link com.tansuosoft.discoverx.model.DocumentDisplay#getHtmlAttributes()}配置了js引用，则返回之；<br/>
	 * 2.如果第一步中没有配置js，以文档所属应用程序别名为子目录（这个子目录应该放在当前页面框架目录下）查找是否存在“文档所属表单别名（转化为小写字母）.js”的文件，如果有则自动引用之。
	 * </p>
	 * 
	 * @return 返回有效的完整js引用url路径，如果没有有效js引用，则返回null。
	 */
	public String getExtraJs() {
		return this.m_appjsref;
	}

	/**
	 * 获取文档信息呈现时所使用的额外样式文件（css）url路径。
	 * 
	 * <p>
	 * 获取css引用的步骤：<br/>
	 * 1.如果有通过文档所属应用程序的{@link com.tansuosoft.discoverx.model.DocumentDisplay#getHtmlAttributes()}配置了css引用，则返回之；<br/>
	 * 2.否则返回null。
	 * </p>
	 * 
	 * @return
	 */
	public String getExtraFormStyle() {
		return m_extraStyle;
	}

	/**
	 * 获取文档所属应用程序中通过html属性配置的输出于呈现时的html页面head部分的额外meta信息。
	 * 
	 * <p>
	 * 如果有多个meta信息则输出为多行，如果没有配置meta信息则返回空字符串，配置方法请参考：{@link com.tansuosoft.discoverx.model.HtmlAttribute#META_ATTRIBUTE_NAME}
	 * </p>
	 * 
	 * @return
	 */
	public String getExtraMetaInfo() {
		return m_metaInfo;
	}

	/**
	 * 获取文档所属应用程序中通过html属性配置的输出于呈现时的html页面body部分的额外属性信息。
	 * 
	 * <p>
	 * 如果没有配置属性则返回空字符串，配置时请按html的标准属性格式进行配置，多个属性之间用空格分隔，比如：<br/>
	 * 属性名配置为<b>onclick</b>和<b>ondblclick</b>，属性值配置为<b>alert('a');</b>和<b>alert(22);</b>，则输出：<b>onclick="alert(a');" ondblclick="alert(22);"</b><br/>
	 * 请特别注意值中有引号时的配置方法。
	 * </p>
	 * 
	 * @return
	 */
	public String getExtraBodyInfo() {
		return m_bodyInfo;
	}

	/**
	 * 返回当前文档包含的操作向导对应的不可见虚拟操作的操作解析对象。
	 * 
	 * <p>
	 * 只有某个文档操作显示了操作向导界面时才会有有效返回值。<br/>
	 * 本方法由系统内部调用。
	 * </p>
	 * 
	 * @return OperationParser
	 */
	public OperationParser getOperationParser() {
		return this.m_operationParser;
	}

	/**
	 * 设置当前文档包含的操作向导对应的不可见虚拟操作的操作解析对象。
	 * 
	 * <p>
	 * 只有某个文档操作显示了操作向导界面时才调用此方法设置操作向导对应的不可见虚拟操作的操作解析对象。<br/>
	 * 本方法由系统内部调用，请勿直接使用，否则可能会导致文档操作向导等功能异常。
	 * </p>
	 * 
	 * @param operationParser OperationParser
	 */
	public void setOperationParser(OperationParser operationParser) {
		this.m_operationParser = operationParser;
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return int
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getMaxWidth()
	 */
	public int getMaxWidth() {
		return documentDisplayInfo.getMaxWidth();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param maxWidth
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setMaxWidth(int)
	 */
	public void setMaxWidth(int maxWidth) {
		documentDisplayInfo.setMaxWidth(maxWidth);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return String
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getDisplay()
	 */
	public String getDisplay() {
		return documentDisplayInfo.getDisplay();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param display
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setDisplay(java.lang.String)
	 */
	public void setDisplay(String display) {
		documentDisplayInfo.setDisplay(display);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return List&lt;Tab&gt;
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getTabs()
	 */
	public List<Tab> getTabs() {
		return documentDisplayInfo.getTabs();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param tabs
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setTabs(java.util.List)
	 */
	public void setTabs(List<Tab> tabs) {
		documentDisplayInfo.setTabs(tabs);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return List&lt;HtmlAttribute&gt;
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getHtmlAttributes()
	 */
	public List<HtmlAttribute> getHtmlAttributes() {
		return documentDisplayInfo.getHtmlAttributes();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param htmlAttributes
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setHtmlAttributes(java.util.List)
	 */
	public void setHtmlAttributes(List<HtmlAttribute> htmlAttributes) {
		documentDisplayInfo.setHtmlAttributes(htmlAttributes);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return boolean
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getReadonly()
	 */
	public boolean getReadonly() {
		return documentDisplayInfo.getReadonly();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param readonly
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setReadonly(boolean)
	 */
	public void setReadonly(boolean readonly) {
		documentDisplayInfo.setReadonly(readonly);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return boolean
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getHasAccessory()
	 */
	public boolean getHasAccessory() {
		return documentDisplayInfo.getHasAccessory();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param hasAccessory
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setHasAccessory(boolean)
	 */
	public void setHasAccessory(boolean hasAccessory) {
		documentDisplayInfo.setHasAccessory(hasAccessory);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return boolean
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getHasSelector()
	 */
	public boolean getHasSelector() {
		return documentDisplayInfo.getHasSelector();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param hasSelector
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setHasSelector(boolean)
	 */
	public void setHasSelector(boolean hasSelector) {
		documentDisplayInfo.setHasSelector(hasSelector);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return boolean
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getHasTranslator()
	 */
	public boolean getHasTranslator() {
		return documentDisplayInfo.getHasTranslator();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param hasTranslator
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setHasTranslator(boolean)
	 */
	public void setHasTranslator(boolean hasTranslator) {
		documentDisplayInfo.setHasTranslator(hasTranslator);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return boolean
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getHasValidator()
	 */
	public boolean getHasValidator() {
		return documentDisplayInfo.getHasValidator();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param hasValidator
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setHasValidator(boolean)
	 */
	public void setHasValidator(boolean hasValidator) {
		documentDisplayInfo.setHasValidator(hasValidator);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param igt
	 * @return boolean
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getHasItemGroupType(com.tansuosoft.discoverx.model.ItemGroupType)
	 */
	public boolean getHasItemGroupType(ItemGroupType igt) {
		return documentDisplayInfo.getHasItemGroupType(igt);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param igt
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setItemGroupType(com.tansuosoft.discoverx.model.ItemGroupType)
	 */
	public void setItemGroupType(ItemGroupType igt) {
		documentDisplayInfo.setItemGroupType(igt);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return boolean
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getHasLog()
	 */
	public boolean getHasLog() {
		return documentDisplayInfo.getHasLog();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param hasLog
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setHasLog(boolean)
	 */
	public void setHasLog(boolean hasLog) {
		documentDisplayInfo.setHasLog(hasLog);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return boolean
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getHasOpinion()
	 */
	public boolean getHasOpinion() {
		return documentDisplayInfo.getHasOpinion();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param hasOpinion
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setHasOpinion(boolean)
	 */
	public void setHasOpinion(boolean hasOpinion) {
		documentDisplayInfo.setHasOpinion(hasOpinion);
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @return boolean
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#getHasOperationUI()
	 */
	public boolean getHasOperationUI() {
		return documentDisplayInfo.getHasOperationUI();
	}

	/**
	 * 调用{@link DocumentDisplayInfo}的同名方法。
	 * 
	 * @param hasOperationUI
	 * @see com.tansuosoft.discoverx.web.ui.document.DocumentDisplayInfo#setHasOperationUI(boolean)
	 */
	public void setHasOperationUI(boolean hasOperationUI) {
		documentDisplayInfo.setHasOperationUI(hasOperationUI);
	}

}

