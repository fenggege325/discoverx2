/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 设置是否可见（{@link ItemDisplay#getVisible()}）的{@link ItemDisplayFilter}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemDisplayVisibleFilter extends ItemDisplayFilter {

	/**
	 * 重载filter
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.document.ItemDisplayFilter#filter(com.tansuosoft.discoverx.web.ui.document.ItemDisplay,
	 *      com.tansuosoft.discoverx.model.Document, com.tansuosoft.discoverx.web.JSPContext)
	 */
	public void filter(ItemDisplay itemDisplay, Document document, JSPContext jspContext) {
		Item item = itemDisplay.getItem();
		if (item == null) return;
		boolean result = false;

		String visible = item.getVisibleExpression();// 获取隐藏公式
		if (!StringUtil.isNullOrEmpty(visible)) {
			ExpressionEvaluator evaluator = new ExpressionEvaluator(visible, jspContext.getUserSession(), document);
			result = evaluator.evalBoolean();
			itemDisplay.setVisible(result);
		} else {
			ItemDisplay groupItemDisplay = itemDisplay.getGroupItemDisplay();
			if (groupItemDisplay != null) itemDisplay.setVisible(groupItemDisplay.getVisible());
		}
	}

}

