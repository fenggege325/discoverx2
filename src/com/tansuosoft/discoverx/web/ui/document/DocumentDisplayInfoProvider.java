/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.document;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentDefaultIssuedTabConfig;
import com.tansuosoft.discoverx.model.DocumentDefaultTabConfig;
import com.tansuosoft.discoverx.model.DocumentDisplay;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.DocumentType;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.Tab;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 提供{@link DocumentDisplayInfo}对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
class DocumentDisplayInfoProvider {
	/**
	 * 为指定文档提供{@link DocumentDisplayInfo}对象。
	 * 
	 * @param document
	 * @param jspContext
	 * @return
	 */
	public static DocumentDisplayInfo provide(Document document, JSPContext jspContext) {
		boolean isProfile = (document != null && document.getType() == DocumentType.Profile);
		Session s = jspContext.getUserSession();
		DocumentDisplay documentDisplay = DocumentDisplayProvider.provide(document, jspContext);
		DocumentDisplayInfo result = new DocumentDisplayInfo();
		HttpServletRequest request = (HttpServletRequest) jspContext.getRequest();
		int availWidth = StringUtil.getValueInt(request.getParameter("width"), CommonConfig.getInstance().getMaxDocumentContentWidth());
		List<Tab> usedTabs = (document != null && document.checkState(DocumentState.Issued) ? DocumentDefaultIssuedTabConfig.getInstance().getTabs() : DocumentDefaultTabConfig.getInstance().getTabs());
		if (documentDisplay == null) {
			result.setMaxWidth(availWidth);
			result.setTabs(usedTabs);
			return result;
		}

		if (!isProfile) result.setDisplay(documentDisplay.getDisplay());
		if (!isProfile) result.setHtmlAttributes(documentDisplay.getHtmlAttributes());
		result.setMaxWidth(documentDisplay.getMaxWidth());
		if (result.getMaxWidth() <= 0) result.setMaxWidth(availWidth);
		if (!isProfile) {
			String formula = documentDisplay.getReadonlyExpression();
			if (formula != null && formula.length() > 0) {
				ExpressionEvaluator evaluator = new ExpressionEvaluator(formula, s, document);
				result.setReadonly(evaluator.evalBoolean());
			}
		}
		List<Tab> tabs = documentDisplay.getTabs();
		List<Tab> validTabs = null;
		if (tabs != null && tabs.size() > 0) {
			validTabs = new ArrayList<Tab>(tabs.size());
			ExpressionEvaluator evaluator = null;
			for (Tab t : tabs) {
				if (t == null || StringUtil.isBlank(t.getTitle())) continue;
				String expression = t.getExpression();
				if (expression != null && expression.length() > 0) {
					evaluator = new ExpressionEvaluator(expression, s, document);
					if (!evaluator.evalBoolean()) continue;
				}
				validTabs.add(t);
			}
		}
		result.setTabs(validTabs);
		if (validTabs == null || validTabs.isEmpty()) result.setTabs(usedTabs);
		return result;
	}
}

