/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 表示描述html节点并可以输出html节点对应html文本的类。
 * 
 * <p>
 * 比如一个div、一个input等都是一个html节点。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class HtmlElement implements HtmlRender {
	private String m_tagName = null; // html标记名。
	private List<HtmlAttribute> m_attributes = null; // html元素包含的html属性集合。
	private String m_text = null; // 包含的文本。
	private List<HtmlElement> m_subElements = null;
	private HtmlElement m_outerElement = null; // 外部节点。

	/**
	 * 缺省构造器。
	 */
	public HtmlElement() {
	}

	/**
	 * 接收标记名称的构造器。
	 * 
	 * @param tagName
	 */
	public HtmlElement(String tagName) {
		this.m_tagName = tagName;
	}

	/**
	 * 返回html标记名。
	 * 
	 * @return String
	 */
	public String getTagName() {
		return this.m_tagName;
	}

	/**
	 * 设置html标记名。
	 * 
	 * @param tagName String
	 */
	public void setTagName(String tagName) {
		this.m_tagName = tagName;
	}

	/**
	 * 返回包含的html属性集合。
	 * 
	 * @return List&lt;HtmlAttribute&gt;
	 */
	public List<HtmlAttribute> getAttributes() {
		return this.m_attributes;
	}

	/**
	 * 设置包含的html属性集合。
	 * 
	 * @param attributes List&lt;HtmlAttribute&gt;
	 */
	public void setAttributes(List<HtmlAttribute> attributes) {
		this.m_attributes = attributes;
	}

	/**
	 * 追加一个html属性。
	 * 
	 * <p>
	 * 如果有同名属性，则追加val到原有值列表中。
	 * </p>
	 * 
	 * @param htmlAttribute {@link HtmlAttribute}
	 */
	public void appendAttribute(HtmlAttribute htmlAttribute) {
		if (htmlAttribute == null) return;
		String attributeName = htmlAttribute.getName();
		if (attributeName == null || attributeName.length() == 0) return;
		if (this.m_attributes == null) this.m_attributes = new ArrayList<HtmlAttribute>();
		boolean found = false;
		for (HtmlAttribute x : this.m_attributes) {
			if (x != null && attributeName.equalsIgnoreCase(x.getName())) {
				x.appendValue(htmlAttribute.getValue());
				x.appendValues(htmlAttribute.getValues());
				found = true;
				break;
			}
		}
		if (!found) this.m_attributes.add(htmlAttribute);
	}

	/**
	 * 追加一个name作为名称的val作为属性值的属性。
	 * 
	 * <p>
	 * 如果有同名属性，则追加val到原有值列表中。
	 * </p>
	 * 
	 * @param name String
	 * @param val String
	 */
	public void appendAttribute(String name, String val) {
		if (name == null || name.length() == 0) return;
		HtmlAttribute htmlAttribute = null;
		htmlAttribute = new HtmlAttribute(name, val);
		this.appendAttribute(htmlAttribute);
	}

	/**
	 * 根据属性名获取属性值。
	 * 
	 * @param name
	 * @return 找不到则可能返回null，如果没有值，也可能返回null。
	 */
	public String getAttribute(String name) {
		if (this.m_attributes == null || name == null || name.length() == 0) return null;
		for (HtmlAttribute x : this.m_attributes) {
			if (x != null && name.equalsIgnoreCase(x.getName())) { return x.getValue(); }
		}
		return null;
	}

	/**
	 * 返回包含的html文本。
	 * 
	 * @return String
	 */
	public String getInnerHTML() {
		return this.m_text;
	}

	/**
	 * 设置包含的html文本。
	 * 
	 * @param text String
	 */
	public void setInnerHTML(String text) {
		this.m_text = text;
	}

	/**
	 * 返回包含的下级{@link HtmlElement}列表。
	 * 
	 * @return List&lt;HtmlElement&gt;
	 */
	public List<HtmlElement> getSubElements() {
		return this.m_subElements;
	}

	/**
	 * 设置包含的下级{@link HtmlElement}列表。
	 * 
	 * @param subElements List&lt;HtmlElement&gt;
	 */
	public void setSubElements(List<HtmlElement> subElements) {
		this.m_subElements = subElements;
	}

	/**
	 * 追加一个下级元素。
	 * 
	 * @param htmlElement {@link HtmlElement}
	 */
	public void appendSubElement(HtmlElement htmlElement) {
		if (this.m_subElements == null) this.m_subElements = new ArrayList<HtmlElement>();
		this.m_subElements.add(htmlElement);
	}

	/**
	 * 返回外部节点。
	 * 
	 * @return HtmlElement
	 */
	public HtmlElement getOuterElement() {
		return this.m_outerElement;
	}

	/**
	 * 设置外部节点。
	 * 
	 * <p>
	 * 外部节点的{@link #render(JSPContext)}输出在当前节点内容输出之后。
	 * </p>
	 * 
	 * @param outerElement HtmlElement
	 */
	public void setOuterElement(HtmlElement outerElement) {
		this.m_outerElement = outerElement;
	}

	/**
	 * 重载render：输出组合好的最终HTML文本。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		boolean hasTagName = (this.m_tagName != null && this.m_tagName.length() > 0);
		if (hasTagName) sb.append("\r\n").append("<").append(this.m_tagName);
		if (this.m_attributes != null && this.m_attributes.size() > 0) {
			for (HtmlAttribute x : this.m_attributes) {
				if (x == null || x.getName() == null) continue;
				sb.append(x.render(jspContext));
			}
		}
		if ((this.m_subElements == null || this.m_subElements.isEmpty()) && this.m_text == null) {
			if (hasTagName) sb.append("/>");
		} else {
			if (hasTagName) sb.append(">");
			sb.append(this.m_text == null ? "" : this.m_text);
			if (this.m_subElements != null && !this.m_subElements.isEmpty()) {
				for (HtmlElement x : this.m_subElements) {
					sb.append(x.render(jspContext));
				}
			}
			if (hasTagName) sb.append("</").append(this.m_tagName).append(">");
		}
		if (this.m_outerElement != null) {
			sb.append(m_outerElement.render(jspContext));
		}
		return sb.toString();
	}
}

