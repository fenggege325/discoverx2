/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 表示输出枚举选择框(select)的{@link HtmlElement}类。
 * 
 * @author coca@tansuosoft.cn
 */
public class EnumSelectElement extends HtmlElement {

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param enumx {@link EnumBase}类型的某个具体枚举类信息，必须。
	 * @param v {@link EnumBase}类型的某个具体枚举值，必须。
	 * @param titles 用于提供options标题数组，必须和枚举个数一致，如果某个title为null或""则不显示。
	 */
	public EnumSelectElement(Class<? super EnumBase> clazz, EnumBase v, String[] titles) {
		super("select");
		HtmlElement option = null;
		EnumBase[] arr = (EnumBase[]) clazz.getEnumConstants();
		if (arr != null && arr.length > 0 && titles.length == arr.length) {
			String title = null;
			for (int i = 0; i < arr.length; i++) {
				title = titles[i];
				if (title == null || title.length() == 0) continue;
				option = new HtmlElement("option");
				option.appendAttribute("value", arr[i].toString());
				option.setInnerHTML(title);
				if (arr[i].getIntValue() == v.getIntValue()) option.appendAttribute("selected", "true");
				this.appendSubElement(option);
			}
		}
	}

	/**
	 * 接收tagName的构造器。
	 * 
	 * @param tagName
	 */
	protected EnumSelectElement(String tagName) {
		super(tagName);
	}
}

