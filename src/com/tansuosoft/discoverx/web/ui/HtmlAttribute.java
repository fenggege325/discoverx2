/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 表示描述HTML属性的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class HtmlAttribute extends com.tansuosoft.discoverx.model.HtmlAttribute implements HtmlRender {
	private List<String> m_values = null; // 属性值列表。
	private String m_delimiter = ""; // 多个属性值的分隔符，默认为空串。

	/**
	 * 缺省构造器。
	 */
	public HtmlAttribute() {
		super(null, null);
	}

	/**
	 * 接收属性名和属性值的构造器。
	 * 
	 * @param name
	 */
	public HtmlAttribute(String name, String val) {
		super(name, val);
		appendValue(val);
	}

	/**
	 * 接收{@link com.tansuosoft.discoverx.model.HtmlAttribute}对象的构造器。
	 * 
	 * @param htmlAttribute {@link com.tansuosoft.discoverx.model.HtmlAttribute}
	 */
	public HtmlAttribute(com.tansuosoft.discoverx.model.HtmlAttribute htmlAttribute) {
		this(htmlAttribute.getName(), htmlAttribute.getValue());
	}

	/**
	 * 重载setValue
	 * 
	 * @see com.tansuosoft.discoverx.model.HtmlAttribute#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		super.setValue(value);
		appendValue(value);
	}

	/**
	 * 返回属性值列表。
	 * 
	 * @return List&lt;String&gt;
	 */
	public List<String> getValues() {
		return this.m_values;
	}

	/**
	 * 追加一个属性值。
	 * 
	 * @param val String
	 */
	public void appendValue(String val) {
		String appendValue = (val == null ? "" : val);
		if (this.m_values == null) this.m_values = new ArrayList<String>();
		if (!this.m_values.contains(appendValue)) this.m_values.add(appendValue);
	}

	/**
	 * 追加多个属性值。
	 * 
	 * @param vals List&lt;String&gt;
	 */
	public void appendValues(List<String> vals) {
		if (vals == null || vals.isEmpty()) return;
		for (String x : vals) {
			this.appendValue(x);
		}
	}

	/**
	 * 返回多个属性值之间的分隔符。
	 * 
	 * <p>
	 * 若追加了多个属性值，那么输出为具体html代码时使用此分隔符分隔多个属性值结果。
	 * </p>
	 * <p>
	 * 默认为空字符串（即无分隔符）。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDelimiter() {
		return this.m_delimiter;
	}

	/**
	 * 设置多个属性值之间的分隔符。
	 * 
	 * @param delimiter String
	 */
	public void setDelima(String delimiter) {
		this.m_delimiter = delimiter;
	}

	/**
	 * 重载render：输出组合好的最终HTML文本。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		sb.append(" ").append(this.getName()).append("=");
		sb.append("\"");
		if (this.m_values != null && !this.m_values.isEmpty()) {
			for (String x : this.m_values) {
				sb.append(x).append(this.m_delimiter);
			}
			if (this.m_delimiter != null && this.m_delimiter.length() > 0) sb.deleteCharAt(sb.length() - this.m_delimiter.length());
		}

		sb.append("\"");
		return sb.toString();
	}
}

