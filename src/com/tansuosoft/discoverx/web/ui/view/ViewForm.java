/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.function.CommonForm;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;

/**
 * 表示获取视图查询、显示等属性值的{@link CommonForm}实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ViewForm extends CommonForm implements JsonSerializable {
	/**
	 * 缺省构造器。
	 */
	public ViewForm() {
	}

	private String m_UNID = null; // 视图UNID
	private int m_currentPage = 1; // 当前显示第几页。
	private int m_displayCountPerPage = View.DEFAULT_DISPLAY_COUNT_PER_PAGE; // 每页显示几条，默认为20。
	private String m_filter = null; // 视图过滤信息
	private String m_viewEntryCallback = null; // 视图条目显示回调类信息，默认为“HtmlViewEntryCallback”。
	private boolean m_showNavigator = true; // 是否显示分页导航，默认为true。
	private boolean m_showOperation = true; // 是否显示操作，默认为true。
	private boolean m_showSearch = true; // 是否显示搜索，默认为true。
	private boolean m_showTitle = true; // 是否显示视图标题，默认为true。
	private int m_maxWidth = 0; // 最大可显示宽度。
	private boolean m_noCache = false; // 是否强行关闭视图缓存。

	/**
	 * 返回视图UNID。
	 * 
	 * <p>
	 * 从名为“view”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置视图UNID。
	 * 
	 * <p>
	 * 从名为“view”的http请求参数中获取。
	 * </p>
	 * 
	 * @param UNID String
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回当前显示第几页，默认为1，如果为0则表示不分页（返回所有数据）。
	 * 
	 * <p>
	 * 从名为“vp”的http请求参数中获取。
	 * </p>
	 * 
	 * @return int
	 */
	public int getCurrentPage() {
		return this.m_currentPage;
	}

	/**
	 * 设置当前显示第几页。
	 * 
	 * <p>
	 * 从名为“vp”的http请求参数中获取。
	 * </p>
	 * 
	 * @param currentPage int
	 */
	public void setCurrentPage(int currentPage) {
		this.m_currentPage = currentPage;
	}

	/**
	 * 返回每页显示几条，默认为{@link View#DEFAULT_DISPLAY_COUNT_PER_PAGE}。
	 * 
	 * <p>
	 * 从名为“vc”的http请求参数中获取。
	 * </p>
	 * 
	 * @return int
	 */
	public int getDisplayCountPerPage() {
		return this.m_displayCountPerPage;
	}

	/**
	 * 设置每页显示几条，默认为{@link View#DEFAULT_DISPLAY_COUNT_PER_PAGE}。
	 * 
	 * <p>
	 * 从名为“vc”的http请求参数中获取。
	 * </p>
	 * 
	 * @param displayCountPerPage int
	 */
	public void setDisplayCountPerPage(int displayCountPerPage) {
		this.m_displayCountPerPage = displayCountPerPage;
	}

	/**
	 * 返回视图过滤信息
	 * 
	 * <p>
	 * 从名为“vf”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getFilter() {
		return this.m_filter;
	}

	/**
	 * 设置视图过滤信息
	 * 
	 * <p>
	 * 从名为“vf”的http请求参数中获取。
	 * </p>
	 * 
	 * @param filter String
	 */
	public void setFilter(String filter) {
		this.m_filter = filter;
	}

	/**
	 * 调用{@link ViewFilterParser#parse()}并返回获取到的当前视图对应过滤信息对应的额外搜索条件列表。
	 * 
	 * @return
	 */
	public List<ViewCondition> getFilterConditions() {
		ViewFilterParser viewFilterParser = new ViewFilterParser(this.getFilter(), this.getView());
		return viewFilterParser.parse();
	}

	/**
	 * 返回视图数据条目后台组装对象信息。
	 * 
	 * <p>
	 * 提交的参数值应为{@link ViewEntryCallbackConfig}中某一个配置项的名称，如果找不到或者不提供则默认为“listview”，表示使用{@link ListViewJsonViewEntryCallback}组装结果。<br/>
	 * 参考“{@link View#getDisplay()}”中的说明。
	 * </p>
	 * <p>
	 * 从名为“vd”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getViewEntryCallback() {
		return this.m_viewEntryCallback;
	}

	/**
	 * 设置视图数据条目后台组装对象信息。
	 * 
	 * <p>
	 * 从名为“vd”的http请求参数中获取。
	 * </p>
	 * 
	 * @param viewEntryCallback String
	 */
	public void setViewEntryCallback(String viewEntryCallback) {
		this.m_viewEntryCallback = viewEntryCallback;
	}

	/**
	 * 返回是否显示分页，默认为true。
	 * 
	 * <p>
	 * 从名为“vn”的http请求参数中获取。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getShowNavigator() {
		return this.m_showNavigator;
	}

	/**
	 * 设置是否显示分页，默认为true。
	 * 
	 * <p>
	 * 从名为“vn”的http请求参数中获取。
	 * </p>
	 * 
	 * @param showNavigator boolean
	 */
	public void setShowNavigator(boolean showNavigator) {
		this.m_showNavigator = showNavigator;
	}

	/**
	 * 返回是否显示操作，默认为true。
	 * 
	 * <p>
	 * 从名为“vo”的http请求参数中获取。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getShowOperation() {
		return this.m_showOperation;
	}

	/**
	 * 设置是否显示操作，默认为true。
	 * 
	 * <p>
	 * 从名为“vo”的http请求参数中获取。
	 * </p>
	 * 
	 * @param showOperation boolean
	 */
	public void setShowOperation(boolean showOperation) {
		this.m_showOperation = showOperation;
	}

	/**
	 * 返回是否显示搜索，默认为true。
	 * 
	 * <p>
	 * 从名为“vs”的http请求参数中获取。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getShowSearch() {
		return this.m_showSearch;
	}

	/**
	 * 设置是否显示搜索，默认为true。
	 * 
	 * <p>
	 * 从名为“vs”的http请求参数中获取。
	 * </p>
	 * 
	 * @param showSearch boolean
	 */
	public void setShowSearch(boolean showSearch) {
		this.m_showSearch = showSearch;
	}

	/**
	 * 返回是否显示视图标题，默认为true。
	 * 
	 * <p>
	 * 从名为“vt”的http请求参数中获取。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getShowTitle() {
		return this.m_showTitle;
	}

	/**
	 * 设置是否显示视图标题，默认为true。
	 * 
	 * <p>
	 * 从名为“vt”的http请求参数中获取。
	 * </p>
	 * 
	 * @param showTitle boolean
	 */
	public void setShowTitle(boolean showTitle) {
		this.m_showTitle = showTitle;
	}

	/**
	 * 返回最大可显示宽度。
	 * 
	 * <p>
	 * 单位为像素（px）。
	 * </p>
	 * <p>
	 * 从名为“vw”或“width”的http请求参数中获取。
	 * </p>
	 * 
	 * @return int
	 */
	public int getMaxWidth() {
		return this.m_maxWidth;
	}

	/**
	 * 设置最大可显示宽度。
	 * 
	 * <p>
	 * 从名为“vw”或“width”的http请求参数中获取。
	 * </p>
	 * 
	 * @param maxWidth int
	 */
	public void setMaxWidth(int maxWidth) {
		this.m_maxWidth = maxWidth;
	}

	/**
	 * 返回是否强行关闭视图查询数据缓存。
	 * 
	 * <p>
	 * 默认为false，表示使用视图查询缓存（如果有的话）；如果设置为true，则总是不使用视图数据缓存（哪怕有缓存）且会删除对应的缓存！
	 * </p>
	 * <p>
	 * 从名为“vb”的http请求参数中获取。
	 * </p>
	 * <p>
	 * 此处返回的结果比{@link ViewRender#setNoCache(boolean)}设置的结果具有更低优先权！
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getNoCache() {
		return this.m_noCache;
	}

	/**
	 * 设置是否强行关闭视图查询数据缓存。
	 * 
	 * <p>
	 * 从名为“vb”的http请求参数中获取。
	 * </p>
	 * 
	 * @param noCache boolean
	 */
	public void setNoCache(boolean noCache) {
		this.m_noCache = noCache;
	}

	private View m_view = null;

	/**
	 * 获取当前请求对应的视图对象。
	 * 
	 * @return {@link View}
	 */
	public View getView() {
		if (this.m_view != null) return this.m_view;
		if (this.m_UNID == null || this.m_UNID.length() == 0) return this.m_view;
		m_view = ResourceContext.getInstance().getResource(m_UNID.startsWith("view") ? ResourceAliasContext.getInstance().getUNIDByAlias(View.class, this.m_UNID) : this.m_UNID, View.class);
		if (m_view != null && !m_view.getUNID().equalsIgnoreCase(m_UNID)) this.m_UNID = m_view.getUNID();
		return m_view;
	}

	/**
	 * 根据web请求参数设置相应属性。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.CommonForm#fillWebRequestForm(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public CommonForm fillWebRequestForm(HttpServletRequest request) {
		if (request == null) return null;
		this.m_UNID = request.getParameter("view");
		getView();
		this.m_currentPage = StringUtil.getValueInt(request.getParameter("vp"), this.m_currentPage);
		this.m_displayCountPerPage = StringUtil.getValueInt(request.getParameter("vc"), this.m_displayCountPerPage);
		this.m_filter = request.getParameter("vf");
		this.m_viewEntryCallback = request.getParameter("vd");
		this.m_showNavigator = StringUtil.getValueBool(request.getParameter("vn"), this.m_showNavigator);
		this.m_showOperation = StringUtil.getValueBool(request.getParameter("vo"), this.m_showOperation);
		this.m_showSearch = StringUtil.getValueBool(request.getParameter("vs"), this.m_showSearch);
		this.m_showTitle = StringUtil.getValueBool(request.getParameter("vt"), this.m_showTitle);
		this.m_noCache = StringUtil.getValueBool(request.getParameter("vb"), this.m_noCache);
		String vw = request.getParameter("vw");
		if (vw == null || vw.length() == 0) vw = request.getParameter("width");
		this.m_maxWidth = StringUtil.getValueInt(vw, 0);
		return this;
	}

	/**
	 * 重载toJson
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	@Override
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		sb.append("unid:").append("'").append(this.m_UNID).append("'");
		sb.append(",").append("currentPage:").append(this.m_currentPage);
		if (this.m_filter != null) sb.append(",").append("filter:").append("'").append(StringUtil.encode4Json(this.m_filter)).append("'");
		if (this.m_viewEntryCallback != null) sb.append(",").append("viewEntryCallback:").append("'").append(StringUtil.encode4Json(this.m_viewEntryCallback)).append("'");
		sb.append(",").append("showNavigator:").append(this.m_showNavigator ? "true" : "false");
		sb.append(",").append("showOperation:").append(this.m_showOperation ? "true" : "false");
		sb.append(",").append("showSearch:").append(this.m_showSearch ? "true" : "false");
		sb.append(",").append("showTitle:").append(this.m_showTitle ? "true" : "false");
		sb.append(",").append("maxWidth:").append(this.m_maxWidth);
		return sb.toString();
	}
}

