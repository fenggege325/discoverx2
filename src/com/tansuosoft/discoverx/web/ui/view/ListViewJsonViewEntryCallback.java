/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.view.Pagination;
import com.tansuosoft.discoverx.bll.view.ViewEntry;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 根据获取到的{@link ViewEntry}组合并返回ListView格式的json对象内容的{@link ViewEntryCallback}实现类。
 * 
 * <p>
 * 必须事先通过{@link ViewEntryCallback#setParameter(String, Object)}设置名为“{@link ViewRender#VIEWFORM_PARAMNAME}”的参数值结果为有效的{@link ViewForm}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ListViewJsonViewEntryCallback extends ViewEntryCallback {
	// 构造结果字符串
	private StringBuilder entryResult = new StringBuilder();
	// 条目计数器，起始为0，每解析一条增加1
	private int counter = 0;
	private ViewForm m_viewForm = null;
	private ViewQuery viewQuery = null;
	private View view = null;
	private String lastLviId = null;

	/**
	 * 缺省构造器。
	 */
	public ListViewJsonViewEntryCallback() {
		super();
	}

	/**
	 * 重载entryFetched
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#entryFetched(com.tansuosoft.discoverx.bll.view.ViewEntry, com.tansuosoft.discoverx.bll.view.ViewQuery)
	 */
	public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
		if (this.viewQuery == null) this.viewQuery = query;
		if (view == null) view = this.getView();

		entryResult.append(counter > 0 ? "," : "");
		entryResult.append("\r\n{"); // row begin
		String id = viewEntry.getUnid();
		if (id == null || id.length() == 0) {
			entryResult.append("id:").append("'idl_").append(counter).append("'");
		} else {
			if (id.equalsIgnoreCase(lastLviId)) {
				entryResult.append("id:").append("'").append(id).append("_").append(counter).append("'");
			} else {
				entryResult.append("id:").append("'").append(id).append("'");
			}
			lastLviId = id;
		}
		if (id != null && id.length() > 0) entryResult.append(",").append("data:{id:'").append(id).append("'}");

		String rowDesc = null;
		StringBuilder hiddenColumnValues = new StringBuilder();
		List<ViewColumn> vcs = view.getColumns();
		ViewColumn vc = null;
		if (viewEntry.getValues() != null) {
			entryResult.append(",").append("columns:").append("[");
			int index = 0;
			String encVal = null;
			for (String value : viewEntry.getValues()) {
				vc = vcs.get(index);
				if (vc == null) continue;
				if (value == null) {
					encVal = "";
				} else {
					encVal = StringUtil.encode4Json(value).trim();
				}
				String itemName = vc.getColumnAlias();
				if (!vc.getVisible()) {
					hiddenColumnValues.append(hiddenColumnValues.length() > 0 ? "," : "");
					hiddenColumnValues.append("'").append(itemName).append("'").append(":").append("'").append(encVal).append("'");
				} else {
					if (rowDesc == null || rowDesc.length() == 0) rowDesc = StringUtil.getValueString(vc.getTitle(), "") + ":" + encVal;
					entryResult.append(index == 0 ? "" : ",");
					entryResult.append("\r\n{");
					entryResult.append("id:").append("'idc_").append(counter).append("_").append(index).append("'");
					entryResult.append(",").append("label:").append("'").append(encVal).append("'");
					entryResult.append(",").append("desc:").append("'").append(encVal).append("'");
					entryResult.append(",").append("vci:").append(index);
					entryResult.append(",").append("colAlias:'").append(itemName).append("'");
					entryResult.append(",").append("data:{}");
					entryResult.append("}");
				}
				index++;
			}
			entryResult.append("]"); // columns end
		}
		entryResult.append(",").append("desc:").append("'").append(rowDesc).append("'");
		if (hiddenColumnValues.length() > 0) {
			entryResult.append(",").append("hiddenColumnValues:{");
			entryResult.append(hiddenColumnValues.toString());
			entryResult.append("}"); // hiddenColumnValues end
		}
		entryResult.append("}"); // row end
		counter++;
	}

	/**
	 * 重载getResult：返回构造好的json对象文本。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#getResult()
	 */
	public Object getResult() {
		StringBuilder sb = new StringBuilder();
		try {
			Object viewForm = this.getParameter(ViewRender.VIEWFORM_PARAMNAME);
			if (viewForm == null || !(viewForm instanceof ViewForm)) throw new RuntimeException("没有设置ViewForm对象！");
			this.m_viewForm = (ViewForm) viewForm;
			if (this.viewQuery == null) this.viewQuery = this.getViewQuery();
			if (view == null) view = this.getView();

			SizeMeasurement measurement = view.getMeasurement();
			boolean isPercentage = (measurement == SizeMeasurement.Percentage);
			sb.append("id:").append("'").append(view.getAlias()).append("'");
			if (isPercentage) sb.append(",").append("widthMeasurement:").append("'").append("%").append("'");
			if (m_viewForm.getMaxWidth() > 0) sb.append(",").append("width:").append(m_viewForm.getMaxWidth());
			if (view.getShowSelector()) sb.append(",").append("selectable:true");
			if (view.getNumberIndicator()) sb.append(",").append("showSequence:true");
			String desc = view.getDescription();
			if (desc != null && desc.length() > 0) sb.append(",").append("desc:").append("'").append(desc).append("'");
			sb.append("\r\n").append(",").append("data:").append("{"); // data begin

			sb.append("viewform:{").append(this.m_viewForm.toJson()).append("}");

			// resourceDescriptor
			String directory = (view.getViewType() == ViewType.DocumentView ? ResourceDescriptorConfig.DOCUMENT_DIRECTORY : null);
			if (directory == null && view.getViewType() == ViewType.XMLResourceView) {
				directory = view.getTable();
			} else if (directory == null && view.getViewType() == ViewType.DBResourceView) {
				directory = view.getTable().substring(2);
			}
			if (directory != null) {
				ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
				sb.append(",\r\n").append("resourceDescriptor:").append("{"); // resourceDescriptor begin
				sb.append("directory:").append("'").append(directory).append("'");
				if (rd != null) {
					sb.append(",").append("name:").append("'").append(rd.getName()).append("'");
					sb.append(",").append("xmlStore:").append(rd.getXmlStore() ? "true" : "false");
					sb.append(",").append("configurable:").append(rd.getConfigurable() ? "true" : "false");
				}
				sb.append("}"); // resourceDescriptor end
			}

			// view begin
			sb.append(",\r\n").append("view:").append("{");
			sb.append("alias:").append("'").append(view.getAlias()).append("'");
			sb.append(",").append("unid:").append("'").append(view.getUNID()).append("'");
			sb.append(",").append("label:").append("'").append(view.getName()).append("'");
			sb.append(",").append("table:").append("'").append(view.getTable()).append("'");
			sb.append(",").append("viewType:").append(view.getViewType());
			sb.append(",").append("showSelector:").append(view.getShowSelector() ? "true" : "false");
			sb.append(",").append("hasRawQuery:").append(StringUtil.isBlank(view.getRawQuery()) ? "false" : "true");
			List<Accessory> accs = view.getAccessories();
			if (accs != null && accs.size() > 0) {
				sb.append(",\r\n").append("printTemplates:[");
				int aidx = 0;
				for (Accessory a : accs) {
					if (a == null || !"actViewPrintTemplate".equalsIgnoreCase(a.getAccessoryTypeAlias())) continue;
					sb.append(aidx == 0 ? "" : ",").append("\r\n{");
					sb.append("name:'").append(StringUtil.encode4Json(a.getName())).append("'");
					sb.append(",unid:'").append(a.getUNID()).append("'");
					sb.append(",fileExt:'").append(FileHelper.getExtName(a.getFileName(), "")).append("'");
					sb.append("}");
					aidx++;
				}
				sb.append("]");
			}

			if (view.getParamValueBool("nonewtbb", false)) sb.append(",").append("noNewTBB:false");
			String newTBBLabel = (view.getParamValueString("newtbblabel", ""));
			if (newTBBLabel != null && newTBBLabel.length() > 0) sb.append(",").append("newTBBLabel:'").append(StringUtil.encode4Json(newTBBLabel)).append("'");
			String newTBBTitle = (view.getParamValueString("newtbbtitle", ""));
			if (newTBBTitle != null && newTBBTitle.length() > 0) sb.append(",").append("newTBBTitle:'").append(StringUtil.encode4Json(newTBBTitle)).append("'");
			sb.append("}"); // view end
			Pagination p = this.viewQuery.getPagination();
			if (p != null) {
				sb.append(",\r\n").append("pagination:").append("{");
				sb.append("currentPage:").append(p.getCurrentPage());
				sb.append(",").append("displayCountPerPage:").append(p.getDisplayCountPerPage());
				sb.append(",").append("pageCount:").append(p.getPageCount());
				sb.append(",").append("totalCount:").append(p.getTotalCount());
				sb.append("}");
			}
			sb.append("}"); // data end

			// header
			sb.append("\r\n").append(",").append("header:").append("{");
			sb.append("id:'idh_").append(view.getUNID()).append("'").append(",itemType: 1");

			// columns
			sb.append(",columns:[");
			List<ViewColumn> cols = view.getColumns();
			if (cols == null) cols = new ArrayList<ViewColumn>();

			int maxWidth = this.m_viewForm.getMaxWidth();
			if (maxWidth <= 0) maxWidth = 0;

			int width = 0;
			int columnIndex = 0;
			StringBuilder sbdict = new StringBuilder();
			ViewColumnDictionaryRender viewColumnDictionaryRender = null;
			String viewColumnDictionaryRenderResult = null;
			for (ViewColumn c : cols) {
				viewColumnDictionaryRender = ViewColumnDictionaryRender.getViewColumnDictionaryRender(c);
				viewColumnDictionaryRenderResult = (viewColumnDictionaryRender == null ? null : viewColumnDictionaryRender.render(null));
				sbdict.append(sbdict.length() > 0 ? ",\r\n" : "\r\n").append((viewColumnDictionaryRenderResult != null && viewColumnDictionaryRenderResult.length() > 0) ? viewColumnDictionaryRenderResult : "[]");
				width = c.getWidth();
				if (width >= 0) {
					sb.append(columnIndex == 0 ? "" : ",");
					sb.append("\r\n{");
					sb.append("id:").append("'idc_").append(columnIndex).append("'");
					sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(c.getTitle())).append("'");
					sb.append(",").append("width:").append((columnIndex == (cols.size() - 1)) ? 0 : width);
					sb.append(",").append("data:").append("{");
					sb.append("categorized").append(":").append(c.getCategorized() ? "true" : "false");
					sb.append(",").append("linkable").append(":").append(c.getLinkable() ? "true" : "false");
					sb.append("}"); // column data end
					sb.append("}"); // column end
				}
				columnIndex++;
			}
			sb.append("]"); // columns end
			sb.append("}"); // header end

			// dictionary
			sb.append("\r\n,").append("dictionary:");
			sb.append("["); // dictionary begin
			sb.append(sbdict.toString());
			sb.append("]"); // dictionary end

			// rows
			sb.append("\r\n").append(",").append("rows:").append("[");
			sb.append(this.entryResult.toString());
			sb.append("]"); // rows end
		} catch (Exception ex) {
			FileLogger.error(ex);
			sb = new StringBuilder();
			sb.append("id:'lvError',error:true,data:{},header:{ id: 'lvhError1',itemType: 1, columns: [{ id: 'lvchError1', label:'错误'}]},rows:[{id:'lviError1',columns:[{id:'lvcError1',label:'" + StringUtil.encode4Json(ex.getMessage()) + "'}]}]");
		}
		return sb.toString();
	}
}

