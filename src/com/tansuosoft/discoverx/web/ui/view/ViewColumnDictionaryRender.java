/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 用于输出视图列对应的字典结果JSON对象数组信息的基类。
 * 
 * @author coca@tensosoft.com
 */
public abstract class ViewColumnDictionaryRender implements HtmlRender {
	protected ViewColumn viewColumn = null;

	/**
	 * 接收{@link ViewColumn}的构造器。
	 */
	public ViewColumnDictionaryRender(ViewColumn vc) {
		viewColumn = vc;
	}

	/**
	 * 根据指定{@link ViewColumn}对象获取其对应的{@link ViewColumnDictionaryRender}对象。
	 * 
	 * @param vc
	 * @return
	 */
	public static ViewColumnDictionaryRender getViewColumnDictionaryRender(ViewColumn vc) {
		ViewColumnDictionaryRender result = null;
		if (vc != null) {
			String cfgDict = vc.getDictionary();
			if (cfgDict != null && cfgDict.startsWith("[")) { // json数组
				result = new VCDROfJson(vc);
			} else if (cfgDict != null && cfgDict.startsWith("{")) { // 选择框
				result = new VCDROfDatasource(vc);
			} else if (cfgDict != null && (cfgDict.startsWith("dict") || StringUtil.isUNID(cfgDict))) { // 字典
				result = new VCDROfResource(vc);
			}
		}
		if (result == null) result = new ViewColumnDictionaryRender(vc) {
			/**
			 * 默认实现：返回空字符串。
			 * 
			 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
			 */
			@Override
			public String render(JSPContext jspContext) {
				return null;
			}
		};
		return result;
	}
}

