/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 解析视图过滤信息的类。
 * 
 * <p>
 * 通过{@link ViewForm#getFilter()}获取原始过滤信息，过滤信息格式如下：<br/>
 * <strong>{视图列序号1},{比较符1},{比较右值1},{视图列序号2},{比较符2},{比较右值2}...</strong><br/>
 * 其中：<br/>
 * 视图列序号表示从0开始的视图列，视图列中配置的表名和字段名必须是可比较字段，如果此处不是序号，那么可以是“表名.字段名的格式”，只能包含可搜索的文档表及其字段；<br/>
 * 比较符为形如如“=”、“&gt;”等合法的比较符；<br/>
 * 比较右值即比较符右边的值，比较右值的结果数据类型需与视图列序号对应的视图列中的表字段结果一致。<br/>
 * 如果过滤信息格式为：[关键词]，即包括在中括号中的关键词格式，则说明在文档摘要中搜索指定关键词。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewFilterParser {
	/**
	 * 视图列过滤信息分隔符:“,”（半角逗号）。
	 */
	public static final char FILTER_DELIMITER = ',';
	/**
	 * 关键字过滤信息左分隔符:“[”（半角中括号左边部分）。
	 */
	public static final String FILTER_KEYWORD_LEFT = "[";
	/**
	 * 关键字过滤信息右分隔符:“]”（半角中括号右边部分）。
	 */
	public static final String FILTER_KEYWORD_RIGHT = "]";

	private String m_filter = null;
	private View m_view = null;

	/**
	 * 接收原始过滤信息和视图信息的构造器。
	 * 
	 * @param filter
	 * @param view
	 */
	public ViewFilterParser(String filter, View view) {
		this.m_filter = filter;
		if (this.m_filter != null && this.m_filter.length() > 0 && this.m_filter.indexOf(',') < 0 && !this.m_filter.startsWith(FILTER_KEYWORD_LEFT) && !this.m_filter.endsWith(FILTER_KEYWORD_RIGHT)) {
			m_filter = FILTER_KEYWORD_LEFT + m_filter + FILTER_KEYWORD_RIGHT;
		}
		this.m_view = view;
		if (this.m_view == null) throw new IllegalArgumentException("必须提供有效的视图资源！");
	}

	List<ViewCondition> m_result = null;

	/**
	 * 解析并返回追加到视图选中条件中以过滤视图数据的{@link ViewCondition}列表。
	 * 
	 * @return
	 */
	public List<ViewCondition> parse() {
		if (m_result != null) return m_result;
		if (this.m_filter == null || this.m_filter.length() == 0 || m_view == null) return m_result;
		List<ViewColumn> vcs = m_view.getColumns();
		ViewCondition c = null;

		// 按关键词检索
		if (this.m_filter.startsWith(FILTER_KEYWORD_LEFT) && this.m_filter.endsWith(FILTER_KEYWORD_RIGHT)) {
			String rv = this.m_filter.substring(1, this.m_filter.length() - 1);
			if (!StringUtil.isBlank(m_view.getRawQuery())) {
				m_result = getConditions4RawQueryView(rv);
				return m_result;
			}
			String compare = " LIKE ";
			int vt = (this.m_view == null ? ViewType.DocumentView.getIntValue() : this.m_view.getViewType().getIntValue());
			switch (vt) {
			case 0:
				m_result = getConditions4DocumentView(vcs, rv, compare);
				return m_result;
			case 1:
				m_result = getConditions4DbResourceView(rv, compare);
				return m_result;
			case 2:
				m_result = getConditions4XmlView(rv, compare);
				return m_result;
			default:
				return m_result;
			}
		}

		// 按组合条件检索
		String filters[] = StringUtil.splitString(this.m_filter, FILTER_DELIMITER);
		int columnIdx = 0;
		String compare = null;
		String rightValue = null;
		ViewColumn vc = null;

		if (vcs == null) return m_result;
		String tableDotColumn = null;
		for (int i = 0; i < filters.length; i += 3) {
			columnIdx = StringUtil.getValueInt(filters[i], -1);
			compare = filters[i + 1];
			rightValue = filters[i + 2];
			if (columnIdx < 0) {
				tableDotColumn = filters[i];
				if (tableDotColumn != null && tableDotColumn.length() > 0 && tableDotColumn.indexOf('.') > 0) {
					String tableName = StringUtil.stringLeft(tableDotColumn, ".");
					String columnName = StringUtil.stringRight(tableDotColumn, ".");
					c = new ViewCondition();
					c.setTableName(tableName);
					c.setColumnName(columnName);
					if (compare != null && compare.length() > 0) c.setCompare(compare);
					c.setRightValue(rightValue);
					if (m_result == null) m_result = new ArrayList<ViewCondition>();
					m_result.add(c);
					continue;
				}
			}
			vc = vcs.get(columnIdx);
			if (vc == null) continue;
			if (rightValue == null || rightValue.length() == 0) continue;
			c = new ViewCondition();
			c.setTableName(vc.getTableName());
			c.setColumnName(vc.getColumnName());
			c.setExpression(vc.getExpression());
			if (compare != null && compare.length() > 0) c.setCompare(compare);
			c.setRightValue(rightValue);
			if (m_result == null) m_result = new ArrayList<ViewCondition>();
			m_result.add(c);
		}
		if (m_result.size() >= 2) {
			m_result.get(0).setLeftBracket("(");
			m_result.get(m_result.size() - 1).setRightBracket(")");
		}
		return m_result;
	}

	/**
	 * 返回文档视图的关键词查询条件。
	 * 
	 * @param rv
	 * @param compare
	 * @return
	 */
	protected List<ViewCondition> getConditions4RawQueryView(String rv) {
		if (rv == null || rv.isEmpty()) return null;
		List<ViewCondition> r = new ArrayList<ViewCondition>();
		ViewCondition c = new ViewCondition();
		c.setTableName("");
		c.setColumnName("");
		c.setCompare("");
		c.setRightValue(rv);
		r.add(c);
		return r;
	}

	private static final String T_DOCUMENT = "t_document";
	private static final String C_ABSTRACT = "c_abstract";

	/**
	 * 返回文档视图的关键词查询条件。
	 * 
	 * @param rv
	 * @param compare
	 * @return
	 */
	protected List<ViewCondition> getConditions4DocumentView(List<ViewColumn> vcs, String rv, String compare) {
		List<ViewCondition> r = new ArrayList<ViewCondition>();

		ViewCondition ca = new ViewCondition();
		ca.setTableName(T_DOCUMENT);
		ca.setColumnName(C_ABSTRACT);
		ca.setCompare(compare);
		ca.setRightValue(rv);

		String tbl = null;
		String col = null;
		for (ViewColumn vc : vcs) {
			if (vc == null) continue;
			tbl = vc.getTableName();
			col = vc.getColumnName();
			if (StringUtil.isBlank(tbl) || StringUtil.isBlank(col)) continue;
			if (T_DOCUMENT.equalsIgnoreCase(tbl) && C_ABSTRACT.equalsIgnoreCase(col) && StringUtil.isBlank(vc.getExpression())) continue;
			ViewCondition c = new ViewCondition();
			c.setTableName(tbl);
			c.setColumnName(col);
			c.setCompare(compare);
			c.setRightValue(rv);
			c.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_OR);
			if (r.size() == 0) c.setLeftBracket("(");
			r.add(c);
		}
		if (r.isEmpty()) {
			r.add(ca);
			return r;
		}
		ca.setRightBracket(")");
		ca.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_OR);
		r.add(ca);

		return r;
	}

	/**
	 * 返回xml资源视图的关键词查询条件。
	 * 
	 * @param rv
	 * @param compare
	 * @return
	 */
	protected List<ViewCondition> getConditions4XmlView(String rv, String compare) {
		List<ViewCondition> r = new ArrayList<ViewCondition>();
		ViewCondition c = new ViewCondition();
		c.setLeftBracket("(");
		c.setTableName(this.m_view.getTable());
		c.setColumnName("name");
		c.setCompare(compare);
		c.setRightValue(rv);
		c.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_OR);
		r.add(c);

		c = new ViewCondition();
		c.setTableName(this.m_view.getTable());
		c.setColumnName("alias");
		c.setCompare(compare);
		c.setRightValue(rv);
		c.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_OR);
		r.add(c);

		c = new ViewCondition();
		c.setTableName(this.m_view.getTable());
		c.setColumnName("description");
		c.setCompare(compare);
		c.setRightValue(rv);
		c.setRightBracket(")");
		r.add(c);

		return r;
	}

	/**
	 * 返回非文档数据库资源视图的关键词查询条件。
	 * 
	 * @param rv
	 * @param compare
	 * @return
	 */
	protected List<ViewCondition> getConditions4DbResourceView(String rv, String compare) {
		List<ViewCondition> r = new ArrayList<ViewCondition>();
		ViewCondition c = new ViewCondition();
		c.setLeftBracket("(");
		c.setTableName(this.m_view.getTable());
		c.setColumnName("c_name");
		c.setCompare(compare);
		c.setRightValue(rv);
		c.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_OR);
		r.add(c);

		c = new ViewCondition();
		c.setTableName(this.m_view.getTable());
		c.setColumnName("c_alias");
		c.setCompare(compare);
		c.setRightValue(rv);
		c.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_OR);
		r.add(c);

		c = new ViewCondition();
		c.setTableName(this.m_view.getTable());
		c.setColumnName("c_description");
		c.setCompare(compare);
		c.setRightValue(rv);
		c.setRightBracket(")");
		r.add(c);

		return r;
	}
}

