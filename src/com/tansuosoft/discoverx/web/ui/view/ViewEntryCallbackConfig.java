/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 视图数据条目结果组装/回调信息配置类。
 * 
 * <p>
 * 用于保存系统注册的所有{@link ViewEntryCallback}信息的实现类信息。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ViewEntryCallbackConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private ViewEntryCallbackConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static ViewEntryCallbackConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return ViewEntryCallbackConfig
	 */
	public static ViewEntryCallbackConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new ViewEntryCallbackConfig();
			}
		}
		return m_instance;
	}

	/**
	 * 根据配置项名称获取对应的{@link ViewEntryCallback}对象实例。
	 * 
	 * <p>
	 * 如果找不到对应{@link ViewEntryCallback}实现类名称且name为有效全限定类名则构造一个name指定的类的对象实例。
	 * </p>
	 * 
	 * @param name
	 * @return
	 */
	public ViewEntryCallback getViewEntryCallback(String name) {
		String impl = this.getValue(name);
		ViewEntryCallback vec = null;
		if (impl != null && impl.length() > 0) {
			vec = Instance.newInstance(impl, ViewEntryCallback.class);
		} else if (name != null && name.indexOf(".") > 0 && name.indexOf(':') < 0 && name.indexOf('/') < 0) {
			vec = Instance.newInstance(name, ViewEntryCallback.class);
		}
		return vec;
	}
}

