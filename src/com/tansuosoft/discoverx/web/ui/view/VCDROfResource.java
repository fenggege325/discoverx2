/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.model.Dictionary;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 指向资源（通常是字典资源）的{@link ViewColumnDictionaryRender}实现类。
 * 
 * @author coca@tensosoft.com
 */
public class VCDROfResource extends ViewColumnDictionaryRender {
	protected Resource resource = null;

	/**
	 * 接收{@link ViewColumn}的构造器。
	 * 
	 * @param vc
	 */
	public VCDROfResource(ViewColumn vc) {
		super(vc);
	}

	/**
	 * 缺省{@link ViewColumn}和目标资源的构造器。
	 * 
	 * @param vc
	 */
	public VCDROfResource(ViewColumn vc, Resource r) {
		super(vc);
		this.resource = r;
	}

	/**
	 * 重载：实现。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (this.viewColumn == null) return null;
		if (resource == null) {
			String cfgDict = viewColumn.getDictionary();
			String unid = (StringUtil.isUNID(cfgDict) ? cfgDict : ResourceAliasContext.getInstance().getUNIDByAlias(Dictionary.class, cfgDict));
			resource = ResourceContext.getInstance().getResource(unid, Dictionary.class);
		}
		if (resource == null) return null;
		return renderResource(resource);
	}

	/**
	 * 递归输出资源包含的下级资源信息。
	 * 
	 * @param r
	 * @return
	 */
	protected String renderResource(Resource r) {
		if (r == null) return null;
		List<Resource> list = r.getChildren();
		if (list == null || list.isEmpty()) return null;
		StringBuilder sb = new StringBuilder();
		String name = null;
		String alias = null;
		for (Resource x : list) {
			if (x == null) continue;
			name = x.getName();
			alias = x.getAlias();
			if (alias != null && alias.length() > 0 && !alias.equalsIgnoreCase(name)) {
				sb.append(sb.length() > 0 ? "," : "");
				sb.append("{");
				sb.append("t:'").append(StringUtil.encode4Json(name)).append("'");
				sb.append(",");
				sb.append("v:'").append(StringUtil.encode4Json(alias)).append("'");
				sb.append("}");
			}
			String result = renderResource(x);
			if (result != null && result.length() > 0) sb.append(sb.length() > 0 ? "," : "").append(result);
		}
		if (sb.length() > 0) {
			sb.insert(0, "[");
			sb.append("]");
		}
		return sb.toString();
	}
}

