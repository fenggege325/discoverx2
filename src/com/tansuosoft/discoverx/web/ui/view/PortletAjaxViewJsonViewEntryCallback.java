/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import com.tansuosoft.discoverx.bll.view.ViewEntry;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 组装供客户端js代码使用的小窗口视图内容呈现所要求格式结果的{@link ViewEntryCallback}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class PortletAjaxViewJsonViewEntryCallback extends ViewEntryCallback {
	// 构造结果字符串
	private StringBuilder entryResult = new StringBuilder();
	// 条目计数器，起始为0，每解析一条增加1
	private int counter = 0;
	private ViewQuery viewQuery = null;
	private View view = null;

	/**
	 * 缺省构造器。
	 */
	public PortletAjaxViewJsonViewEntryCallback() {
		super();
	}

	/**
	 * 重载entryFetched
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#entryFetched(com.tansuosoft.discoverx.bll.view.ViewEntry,
	 *      com.tansuosoft.discoverx.bll.view.ViewQuery)
	 */
	public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
		if (this.viewQuery == null) this.viewQuery = query;
		if (view == null) view = this.getView();

		entryResult.append(counter > 0 ? "," : "");
		entryResult.append("\r\n{");
		String id = viewEntry.getUnid();
		if (id == null || id.length() == 0) {
			entryResult.append("id:").append("'errorid_").append(counter).append("'");
		} else {
			entryResult.append("id:").append("'").append(id).append("'");
		}

		if (viewEntry.getValues() != null) {
			int index = 0;
			entryResult.append(",").append("values:[");
			for (String value : viewEntry.getValues()) {
				entryResult.append(index == 0 ? "" : ",").append("'").append(StringUtil.encode4Json(value)).append("'");
				index++;
			}
			entryResult.append("]");
		}
		entryResult.append("}");
		counter++;
	}

	/**
	 * 重载getResult：返回构造好的json对象文本。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#getResult()
	 */
	public Object getResult() {
		StringBuilder sb = new StringBuilder();
		try {
			if (this.viewQuery == null) this.viewQuery = this.getViewQuery();
			if (view == null) view = this.getView();
			sb.append("id:").append("'").append(view.getAlias()).append("'");
			sb.append(",").append("unid:").append("'").append(view.getUNID()).append("'");
			sb.append(",").append("total:").append(this.viewQuery.getEntryCount());
			sb.append(",").append("titles:[");
			int idx = 0;
			for (ViewColumn vc : view.getColumns()) {
				sb.append(idx == 0 ? "" : ",").append("\r\n{");
				sb.append("width:'").append(vc.getWidth()).append(view.getMeasurement() == SizeMeasurement.Percentage ? "%" : "px").append("'");
				sb.append(",name:'").append(vc.getTitle()).append("'");
				sb.append("}");
				idx++;
			}
			sb.append("\r\n]"); // titles end
			sb.append(",").append("entries:").append("[");
			sb.append(this.entryResult.toString());
			sb.append("\r\n]"); // entries end
		} catch (Exception ex) {
			FileLogger.error(ex);
			sb = new StringBuilder();
			sb.append("id:'lvError',error:true,message:'").append(StringUtil.encode4Json(ex.getMessage()) + "'");
		}
		return sb.toString();
	}
}

