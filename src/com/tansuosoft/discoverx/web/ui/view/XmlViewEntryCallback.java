/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import com.tansuosoft.discoverx.bll.view.ViewEntry;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 根据获取到的{@link ViewEntry}组合并返回xml结果的{@link ViewEntryCallback}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class XmlViewEntryCallback extends ViewEntryCallback {
	// 构造结果字符串
	private StringBuilder resultBuilder = new StringBuilder();
	private int total = -1;

	/**
	 * 缺省构造器。
	 */
	public XmlViewEntryCallback() {
		super();
	}

	/**
	 * 重载entryFetched
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#entryFetched(com.tansuosoft.discoverx.bll.view.ViewEntry, com.tansuosoft.discoverx.bll.view.ViewQuery)
	 */
	public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
		if (total == -1) total = query.getEntryCount();
		resultBuilder.append("\r\n<entry");
		String unid = viewEntry.getUnid();
		if (unid != null && unid.length() > 0) {
			resultBuilder.append(" unid=\"").append(unid).append("\"");
		}
		resultBuilder.append(">");
		if (viewEntry.getValues() != null) {
			for (String value : viewEntry.getValues()) {
				resultBuilder.append("\r\n<column>");
				resultBuilder.append("<text>").append(value == null ? "" : StringUtil.encode4Xml(value)).append("</text>");
				resultBuilder.append("</column>");
			}
		}

		resultBuilder.append("\r\n</entry>");
	}

	/**
	 * 重载getResult：返回构造好的xml文本。
	 * 
	 * <p>
	 * 格式如下：<br/>
	 * 
	 * <pre>
	 * &lt;entries view=&quot;{viewunid}&quot; name=&quot;{viewname}&quot;&gt; alias=&quot;{viewalias}&quot;&gt;
	 * &lt;title&gt;
	 * &lt;name&gt;{titlename}&lt;/name&gt;
	 * &lt;width&gt;{titlewidth}&lt;/width&gt;
	 * &lt;/title&gt;
	 * ...
	 * &lt;entry unid=&quot;{unid}&quot;&gt;
	 * &lt;column&gt;&lt;text&gt;{value}&lt;/text&gt;&lt;/column&gt;
	 * ...
	 * &lt;/entry&gt;
	 * ...
	 * &lt;/entries&gt;
	 * </pre>
	 * 
	 * 说明：<br/>
	 * <ul>
	 * <li>{viewunid}为视图的unid</li>
	 * <li>{viewname}为视图的名称</li>
	 * <li>{viewalias}为视图的别名</li>
	 * <li>{titlename}为标题的名称</li>
	 * <li>{titlewidth}为标题的宽度，单位默认为百分比，如果是其他单位，如像素，请添加单位。</li>
	 * <li>{unid}表示记录的UNID，如果不存在，则不输出。</li>
	 * <li>{value}表示列值的文本结果。</li>
	 * <li>title的个数必须和entry中column的个数一一对应！</li>
	 * </ul>
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#getResult()
	 */
	public Object getResult() {
		View v = this.getView();
		if (v == null) throw new RuntimeException("视图未知！");

		StringBuilder titles = new StringBuilder();
		titles.append("\r\n<entries view=\"").append(v.getUNID()).append("\"");
		titles.append(" name=\"").append(v.getName()).append("\"");
		titles.append(" alias=\"").append(v.getAlias()).append("\"");
		titles.append(" total=\"").append(total).append("\"");
		titles.append(">");
		for (ViewColumn vc : v.getColumns()) {
			titles.append("\r\n<title>");
			titles.append("\r\n<name>");
			titles.append(StringUtil.encode4Xml(vc.getTitle()));
			titles.append("</name>");
			titles.append("\r\n<width>");
			titles.append(vc.getWidth()).append(v.getMeasurement() == SizeMeasurement.Percentage ? "%" : "px");
			titles.append("</width>");
			titles.append("</title>");
		}
		if (resultBuilder.length() > 0) {
			resultBuilder.insert(0, titles);
			resultBuilder.append("\r\n</entries>\r\n");
		} else {
			throw new RuntimeException("暂无内容！");
		}
		return resultBuilder.toString();
	}
}

