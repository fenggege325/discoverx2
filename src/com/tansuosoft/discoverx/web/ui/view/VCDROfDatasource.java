/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 指向选择框数据源的{@link ViewColumnDictionaryRender}实现类。
 * 
 * @author coca@tensosoft.com
 */
public class VCDROfDatasource extends ViewColumnDictionaryRender {

	/**
	 * 接收{@link ViewColumn}的构造器。
	 * 
	 * @param vc
	 */
	public VCDROfDatasource(ViewColumn vc) {
		super(vc);
	}

	/**
	 * 重载：实现。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (this.viewColumn == null) return null;
		String cfgDict = viewColumn.getDictionary();
		if (cfgDict == null || !cfgDict.startsWith("{")) return null;
		String resinfo = StringUtil.stringRight(cfgDict, "datasource");
		if (resinfo == null || resinfo.length() == 0) return null;
		resinfo = resinfo.trim();
		if (resinfo.startsWith(":")) resinfo = resinfo.substring(1);
		int aposCnt = 0;
		for (int i = 0; i < resinfo.length(); i++) {
			char c = resinfo.charAt(i);
			if (c == '\'') aposCnt++;
			if (aposCnt == 2) {
				resinfo = resinfo.substring(1, i);
				break;
			}
		}
		if (resinfo == null || resinfo.length() == 0) return null;
		String dir = StringUtil.stringLeft(resinfo, "://");
		String id = StringUtil.stringRight(resinfo, "://");
		if (StringUtil.isBlank(dir) || StringUtil.isBlank(id)) return null;
		String unid = (StringUtil.isUNID(id) ? id : ResourceAliasContext.getInstance().getUNIDByAlias(dir, id));
		Resource r = ResourceContext.getInstance().getResource(unid, dir);
		if (r == null) return null;
		return new VCDROfResource(viewColumn, r).render(jspContext);
	}
}

