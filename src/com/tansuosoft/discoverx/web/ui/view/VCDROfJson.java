/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 指向JSON对象数组本身的{@link ViewColumnDictionaryRender}实现类。
 * 
 * @author coca@tensosoft.com
 */
public class VCDROfJson extends ViewColumnDictionaryRender {

	/**
	 * 接收{@link ViewColumn}的构造器。
	 * 
	 * @param vc
	 */
	public VCDROfJson(ViewColumn vc) {
		super(vc);
	}

	/**
	 * 重载：实现。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (this.viewColumn == null) return "";
		StringBuilder sb = new StringBuilder();
		sb.append(viewColumn.getDictionary());
		return sb.toString();
	}

}

