/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.view.Pagination;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.bll.view.ViewQueryProvider;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 视图数据内容呈现的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ViewRender implements HtmlRender {
	/**
	 * 从{@link ViewEntryCallback}对象中获取{@link ViewForm}对象或设置{@link ViewForm}对象到{@link ViewEntryCallback}对象时使用的参数名。
	 */
	public static final String VIEWFORM_PARAMNAME = "__viewForm";
	/**
	 * 从{@link ViewEntryCallback}对象中获取{@link JSPContext}对象或设置{@link JSPContext}对象到{@link ViewEntryCallback}对象时使用的参数名。
	 */
	public static final String JSPCONTEXT_PARAMNAME = "__jspContext";

	private ViewForm m_viewForm = null;
	private ViewQuery viewQuery = null;
	private View view = null;
	private ViewEntryCallback m_vec = null;
	private List<ViewCondition> m_vcs = null;
	private String m_rawQuery = null; // 视图直接输入的原始查询语句。
	private boolean m_noCache = false; // 无缓存。

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param viewForm ViewForm 视图查询请求信息。
	 */
	public ViewRender(ViewForm viewForm) {
		m_viewForm = viewForm;
		if (m_viewForm != null) {
			view = m_viewForm.getView();
			m_noCache = m_viewForm.getNoCache();
		}
	}

	/**
	 * 设置组装视图数据内容的{@link ViewEntryCallback}对象。
	 * 
	 * <p>
	 * 如果提供，则使用此对象，否则由系统自动根据{@link View#getDisplay()}和{@link ViewForm#getViewEntryCallback()}等信息自动计算。
	 * </p>
	 * 
	 * @param vec
	 */
	public void setViewEntryCallback(ViewEntryCallback vec) {
		this.m_vec = vec;
	}

	/**
	 * 设置额外的搜索条件信息。
	 * 
	 * <p>
	 * 搜索条件组合的先后顺序为：内置条件最先，配置条件其次，通过此处设置的条件再次，通过{@link ViewForm#getFilterConditions()}获取的条件在最后。
	 * </p>
	 * 
	 * @param vcs List&lt;ViewCondition&gt;
	 */
	public void setExtraConditions(List<ViewCondition> vcs) {
		m_vcs = vcs;
	}

	/**
	 * 追加一条额外的搜索条件信息。
	 * 
	 * @param vc ViewCondition
	 */
	public void appendExtraCondition(ViewCondition vc) {
		if (m_vcs == null) m_vcs = new ArrayList<ViewCondition>();
		m_vcs.add(vc);
	}

	/**
	 * 返回视图直接输入的原始查询语句。
	 * 
	 * @return String
	 */
	public String getRawQuery() {
		if (m_rawQuery == null) m_rawQuery = (view == null ? null : view.getRawQuery());
		return m_rawQuery;
	}

	/**
	 * 设置视图直接输入的原始查询语句。
	 * 
	 * <p>
	 * <strong>此方法将使用传入的查询语句替换视图配置中直接输入的原始查询语句！</strong><br/>
	 * 与{@link com.tansuosoft.discoverx.bll.view.ViewQuery#setRawQuery(String)}效果一致。
	 * </p>
	 * 
	 * @param rawQuery String
	 */
	public void setRawQuery(String rawQuery) {
		m_rawQuery = rawQuery;
	}

	/**
	 * 设置是否强行关闭视图查询数据缓存。
	 * 
	 * <p>
	 * 默认为false，表示使用视图查询缓存（如果有的话）；如果设置为true，则总是不使用视图数据缓存（哪怕有缓存）且会删除对应的缓存！
	 * </p>
	 * <p>
	 * 此处设置的结果比{@link ViewForm#getNoCache()}获取的结果具有更高优先权！
	 * </p>
	 * 
	 * @param noCache
	 */
	public void setNoCache(boolean noCache) {
		m_noCache = noCache;
	}

	/**
	 * 重载render：根据{@link ViewForm}中的配置输出视图数据呈现内容。
	 * 
	 * <p>
	 * 如果获取数据并组装数据时提供了有效的{@link ViewEntryCallback}，则会自动通过{@link ViewEntryCallback#setParameter(String, Object)}设置2个名为“{@link #VIEWFORM_PARAMNAME}”和“ {@link #JSPCONTEXT_PARAMNAME}”的参数，其值分别为“
	 * {@link ViewForm}”和 “{@link JSPContext}”对象。此时在{@link ViewEntryCallback}内可以获取并使用这些对象。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		String display = "listview";
		try {
			if (m_viewForm == null) throw new RuntimeException("没有提供有效的视图查询信息！");
			if (view == null) throw new RuntimeException("无法获取“" + m_viewForm.getUNID() + "”对应的视图！");

			viewQuery = ViewQueryProvider.getViewQuery(view, jspContext.getUserSession());

			if (m_noCache) viewQuery.removeCache();

			ViewEntryCallback callback = null;
			if (m_vec == null) {
				String vc = this.m_viewForm.getViewEntryCallback();
				if (vc != null && vc.length() > 0) {
					callback = ViewEntryCallbackConfig.getInstance().getViewEntryCallback(vc);
					if (callback != null) display = vc;
				}
				if (callback == null) {
					vc = view.getDisplay();
					if (vc != null && vc.length() > 0) callback = ViewEntryCallbackConfig.getInstance().getViewEntryCallback(vc);
					if (callback != null) display = vc;
				}
				if (callback == null) callback = new ListViewJsonViewEntryCallback();
			} else {
				callback = m_vec;
			}

			if (callback != null) {
				if (display == null || display.length() == 0 || display.contains(".")) display = callback.getResultFormat();
				callback.setParameter(VIEWFORM_PARAMNAME, this.m_viewForm);
				callback.setParameter(JSPCONTEXT_PARAMNAME, jspContext);
			}
			viewQuery.addViewEntryCallback(callback);

			// 额外搜索条件
			List<ViewCondition> vcs = this.m_viewForm.getFilterConditions();
			if (vcs != null) {
				if (m_vcs != null && m_vcs.size() > 0) {
					m_vcs.addAll(vcs);
				} else {
					m_vcs = vcs;
				}
			}
			if (m_vcs != null) {
				for (ViewCondition x : m_vcs) {
					viewQuery.appendCondition(x);
				}
			}

			// 手工输入查询语句
			if (m_rawQuery != null && m_rawQuery.length() > 0) viewQuery.setRawQuery(m_rawQuery);

			// 分页
			Pagination p = null;
			if (this.m_viewForm.getShowNavigator()) {
				p = new Pagination();
				p.setCurrentPage(this.m_viewForm.getCurrentPage());
				p.setDisplayCountPerPage(this.m_viewForm.getDisplayCountPerPage() != View.DEFAULT_DISPLAY_COUNT_PER_PAGE ? this.m_viewForm.getDisplayCountPerPage() : view.getDisplayCountPerPage());
			}
			viewQuery.setPagination(p);

			// 获取结果
			viewQuery.getEntryCount();
			viewQuery.getViewEntries();

			return callback.getResult(String.class);
		} catch (Exception ex) {
			if (!(ex instanceof RuntimeException)) FileLogger.error(ex);
			StringBuilder sb = new StringBuilder();
			String msg = jspContext.stripExPrefx(ex.getMessage());
			if (display != null && display.indexOf("xml") >= 0) {
				sb.append("<response><status>-1</status><message>组装视图XML内容时出现异常：“").append(StringUtil.encode4Xml(msg)).append("”</message></response>");
			} else if (display != null && display.indexOf("listview") >= 0) {
				sb.append("id:'lvError',error:true,data:{},header:{ id: 'lvhError1',itemType: 1, columns: [{ id: 'lvchError1', label:'错误'}]},rows:[{id:'lviError1',columns:[{id:'lvcError1',label:'" + StringUtil.encode4Json(msg) + "'}]}]");
			} else if (display != null && display.indexOf("json") >= 0) {
				sb.append("id:'lvError',error:true,message:'").append(StringUtil.encode4Json(msg) + "'");
			} else {
				sb.append("<div class=\"error\">视图呈现时出现异常：“").append(msg).append("”</div>");
			}
			return sb.toString();
		}
	}
}

