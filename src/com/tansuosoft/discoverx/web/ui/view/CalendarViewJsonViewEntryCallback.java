/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import java.util.List;

import com.tansuosoft.discoverx.bll.view.ViewEntry;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 组装供客户端js代码使用的日历视图所要求格式结果的{@link ViewEntryCallback}实现类。
 * 
 * @author simon@tansuosoft.cn
 */
public class CalendarViewJsonViewEntryCallback extends ViewEntryCallback {
	// 构造结果字符串
	private StringBuilder entryResult = new StringBuilder();
	// 条目计数器，起始为0，每解析一条增加1
	private int counter = 0;
	private ViewQuery viewQuery = null;
	private View view = null;

	/**
	 * 缺省构造器。
	 */
	public CalendarViewJsonViewEntryCallback() {
		super();
	}

	/**
	 * 重载entryFetched
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#entryFetched(com.tansuosoft.discoverx.bll.view.ViewEntry,
	 *      com.tansuosoft.discoverx.bll.view.ViewQuery)
	 */
	public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
		if (this.viewQuery == null) this.viewQuery = query;
		if (view == null) view = this.getView();

		entryResult.append(counter > 0 ? "," : "");
		entryResult.append("\r\n").append("{");
		String id = viewEntry.getUnid();
		if (id == null || id.length() == 0) {
			entryResult.append("id:").append("'idl_").append(counter).append("'");
		} else {
			entryResult.append("id:").append("'").append(id).append("'");
		}

		String rowDesc = null;
		List<ViewColumn> vcs = view.getColumns();
		ViewColumn vc = null;
		if (viewEntry.getValues() != null) {
			int index = 0;
			for (String value : viewEntry.getValues()) {
				vc = vcs.get(index);
				if (vc == null || vc.getWidth() < 0) continue;
				if (rowDesc == null || rowDesc.length() == 0) rowDesc = value;
				entryResult.append(",").append(vc.getColumnAlias() + ":").append("'").append(StringUtil.encode4Json(value)).append("'");
				index++;
			}
		}
		entryResult.append("}");
		counter++;
	}

	/**
	 * 重载getResult：返回构造好的json对象文本。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#getResult()
	 */
	public Object getResult() {
		StringBuilder sb = new StringBuilder();
		try {
			if (this.viewQuery == null) this.viewQuery = this.getViewQuery();
			if (view == null) view = this.getView();
			sb.append("id:").append("'").append(view.getAlias()).append("'");
			sb.append(",").append("unid:").append("'").append(view.getUNID()).append("'");
			sb.append(",").append("events:").append("[");
			sb.append(this.entryResult.toString());
			sb.append("]"); // events end
		} catch (Exception ex) {
			FileLogger.error(ex);
			sb = new StringBuilder();
			sb.append("id:'lvError',error:true,message:'").append(StringUtil.encode4Json(ex.getMessage()) + "'");
		}
		return sb.toString();
	}
}

