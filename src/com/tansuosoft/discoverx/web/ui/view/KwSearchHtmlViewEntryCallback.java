/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.view;

import com.tansuosoft.discoverx.bll.view.Pagination;
import com.tansuosoft.discoverx.bll.view.ViewEntry;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 根据获取到的{@link ViewEntry}组合并返回供关键词搜索界面使用的html结果的{@link ViewEntryCallback}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class KwSearchHtmlViewEntryCallback extends ViewEntryCallback {
	// 构造结果字符串
	private StringBuilder resultBuilder = new StringBuilder();

	private ViewForm m_viewForm = null;
	private ViewQuery viewQuery = null;
	private View view = null;

	private static final String[] Classes = { "name", "abstract", "created", "creator" };

	/**
	 * 缺省构造器。
	 */
	public KwSearchHtmlViewEntryCallback() {
		super();
	}

	/**
	 * 重载entryFetched
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#entryFetched(com.tansuosoft.discoverx.bll.view.ViewEntry,
	 *      com.tansuosoft.discoverx.bll.view.ViewQuery)
	 */
	public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
		resultBuilder.append("<div");
		resultBuilder.append(" class=\"entry\"");
		resultBuilder.append(">");
		if (viewEntry.getValues() != null) {
			int index = 0;
			for (String value : viewEntry.getValues()) {
				resultBuilder.append("<div class=\"").append(Classes[index]).append("\">");
				if (index == 0) {
					resultBuilder.append("<a");
					resultBuilder.append(" href=\"document.jsp?unid=").append(viewEntry.getUnid()).append("\"");
					resultBuilder.append(" target=\"_blank\"");
					resultBuilder.append(">");
				}
				resultBuilder.append(value == null ? "" : value);
				if (index == 0) {
					resultBuilder.append("</a>");
				}
				resultBuilder.append("</div>");
				index++;
				if (index > 4) break;
			}
		}

		resultBuilder.append("</div>\r\n");
	}

	/**
	 * 重载getResult：返回构造好的html文本。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewEntryCallback#getResult()
	 */
	public Object getResult() {
		StringBuilder sb = new StringBuilder();
		try {
			Object viewForm = this.getParameter(ViewRender.VIEWFORM_PARAMNAME);
			if (viewForm == null || !(viewForm instanceof ViewForm)) throw new RuntimeException("没有设置ViewForm对象！");
			this.m_viewForm = (ViewForm) viewForm;
			if (this.viewQuery == null) this.viewQuery = this.getViewQuery();
			if (view == null) view = this.getView();

			// viewform
			sb.append("viewform:{").append(this.m_viewForm.toJson()).append("}");

			// view
			sb.append(",").append("view:").append("{");
			sb.append("alias:").append("'").append(view.getAlias()).append("'");
			sb.append(",").append("unid:").append("'").append(view.getUNID()).append("'");
			sb.append(",").append("label:").append("'").append(view.getName()).append("'");
			sb.append(",").append("table:").append("'").append(view.getTable()).append("'");
			sb.append(",").append("viewType:").append(view.getViewType());
			sb.append(",").append("showSelector:").append(view.getShowSelector() ? "true" : "false");
			sb.append("}");

			// pagination
			Pagination p = this.viewQuery.getPagination();
			if (p != null) {
				sb.append(",").append("pagination:").append("{");
				sb.append("currentPage:").append(p.getCurrentPage());
				sb.append(",").append("displayCountPerPage:").append(p.getDisplayCountPerPage());
				sb.append(",").append("pageCount:").append(p.getPageCount());
				sb.append(",").append("totalCount:").append(p.getTotalCount());
				sb.append("}");
			}
		} catch (Exception ex) {
			FileLogger.error(ex);
			sb = new StringBuilder();
			sb.append("<div class=\"error\">按关键词搜索时出现异常：“").append(ex.getMessage()).append("”</div>");
			return sb;
		}
		if (resultBuilder.length() == 0) resultBuilder.append("<div class=\"error\">没有找到符合搜索条件的记录！</div>\r\n");
		resultBuilder.append("<script type=\"text/javascript\" language=\"language\">\r\n");
		resultBuilder.append("var metadata={").append(sb.toString()).append("};\r\n");
		resultBuilder.append("</script>\r\n");
		return resultBuilder.toString();
	}
}

