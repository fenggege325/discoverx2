/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui;

/**
 * 表示一个坐标位置的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Coordinate {
	private int m_column = 0; // 所在行位置。
	private int m_row = 0; // 所在列位置。

	/**
	 * 缺省构造器。
	 */
	public Coordinate() {
	}

	/**
	 * 接收必要属性的构造器。
	 */
	public Coordinate(int row, int column) {
		this.m_row = row;
		this.m_column = column;
	}

	/**
	 * 返回所在行位置。
	 * 
	 * @return int。
	 */
	public int getColumn() {
		return this.m_column;
	}

	/**
	 * 设置所在行位置。
	 * 
	 * @param column int。
	 */
	public void setColumn(int column) {
		this.m_column = column;
	}

	/**
	 * 返回所在列位置。
	 * 
	 * @return int。
	 */
	public int getRow() {
		return this.m_row;
	}

	/**
	 * 设置所在列位置。
	 * 
	 * @param row int。
	 */
	public void setRow(int row) {
		this.m_row = row;
	}

}

