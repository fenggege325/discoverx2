/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.List;

import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletContentType;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 内容类型为Entry（{@link PortletContentType#Entry}）的小窗口对应的{@link PortletContentRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class EntryPortletRender extends PortletContentRender {
	private List<PortletEntry> m_list = null;
	private List<PortletEntryTitle> m_portletEntryTitles = null; // 小窗口数据条目包含的{@link PortletEntryTitle}对象列表。

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portal
	 * @param portlet
	 * @param list List&lt;PortletEntry&gt;表示{@link PortletEntry}条目列表。
	 */
	public EntryPortletRender(Portal portal, Portlet portlet, List<PortletEntry> list) {
		super(portal, portlet);
		m_list = list;
	}

	/**
	 * 返回小窗口数据条目包含的{@link PortletEntryTitle}对象列表。
	 * 
	 * <p>
	 * 如果返回null或空值，客户端默认呈现函数会平均分布各列的宽度。
	 * </p>
	 * 
	 * @return List&lt;PortletEntryTitle&gt;
	 */
	public List<PortletEntryTitle> getPortletEntryTitles() {
		return this.m_portletEntryTitles;
	}

	/**
	 * 设置小窗口数据条目包含的{@link PortletEntryTitle}对象列表。
	 * 
	 * <p>
	 * 用于控制客户端呈现时，小窗口数据条目包含的列值（{@link PortletEntry#getValues()}）中每一个列的宽度。<br/>
	 * 设置的结果中包含的{@link PortletEntryTitle}个数必须与{@link PortletEntry#getValues()}包含的列值个数一致。
	 * </p>
	 * 
	 * @param portletEntryTitles List&lt;PortletEntryTitle&gt;
	 */
	public void setPortletEntryTitles(List<PortletEntryTitle> portletEntryTitles) {
		this.m_portletEntryTitles = portletEntryTitles;
	}

	/**
	 * 重载render：显示请求内容的相关html/javascript代码。
	 * 
	 * <p>
	 * 格式说明：<br/>
	 * 
	 * <pre>
	 * &lt;div id=&quot;portlet_entryresult_{portletalias}&quot;&gt;&lt;/div&gt;
	 * &lt;script type=&quot;text/javascript&quot; language=&quot;javascript&quot;&gt;
	 * {@link PortletDataSource#getClientContentRender()}或portletContent.defaultEntryResultRender（不配置时的系统默认值）}([
	 * 每一条{@link PortletEntry}对象对应的json对象...
	 * ],{portletJson},{dataSourceJson},[每一条{@link PortletEntryTitle}对象对应的json对象...]);
	 * &lt;/script&gt;
	 * </pre>
	 * 
	 * 其中“{portletalias}”为小窗口别名，“{portletJson}”、“{dataSourceJson}”分别为包含portlet和对应数据源常用属性的json对象。。 <br/>
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		try {
			PortletDataSource dataSource = this.getPortletDataSource();
			if (dataSource == null) throw new RuntimeException("无法获取小窗口数据源！");
			if (m_list == null || m_list.isEmpty()) throw new RuntimeException("无内容！");
			String palias = this.m_portlet.getAlias();
			String entryResultId = "portlet_entryresult_" + palias;
			String displayFunction = dataSource.getClientContentRender();
			if (displayFunction == null || displayFunction.length() == 0) displayFunction = "portletContent.defaultEntryResultRender";
			String crlf = "\r\n";
			sb.append("<div id=\"").append(entryResultId).append("\"></div>").append(crlf);
			sb.append("<script type=\"text/javascript\">").append(crlf);

			sb.append(displayFunction).append("([");
			int idx = 0;
			String id = null;
			for (PortletEntry x : m_list) {
				sb.append(idx == 0 ? "" : ",").append("\r\n{");
				id = x.getUnid();
				if (id == null || id.length() == 0) {
					sb.append("id:").append("'errorid_").append(idx).append("'");
				} else {
					sb.append("id:").append("'").append(id).append("'");
				}
				if (x.getValues() != null) {
					int index = 0;
					sb.append(",").append("values:[");
					for (String value : x.getValues()) {
						sb.append(index == 0 ? "" : ",").append("'").append(StringUtil.encode4Json(value)).append("'");
						index++;
					}
					sb.append("]");
				}
				sb.append("}");
				idx++;
			}
			sb.append("]");
			sb.append(",").append("{").append(this.getPortletJson()).append("}");
			sb.append(",").append("{").append(this.getDataSourceJson()).append("}");
			idx = 0;
			if (m_portletEntryTitles != null && m_portletEntryTitles.size() > 0) {
				sb.append(",").append("[");
				for (PortletEntryTitle pet : m_portletEntryTitles) {
					sb.append(idx++ == 0 ? "" : ",").append("\r\n").append(pet.toString());
				}
				sb.append("]");
			}
			sb.append(");\r\n");
			sb.append("</script>");
		} catch (Exception ex) {
			return new PortletContentExceptionRender(this.m_portal, this.m_portlet, ex).render(jspContext);
		}
		return sb.toString();
	}
}

