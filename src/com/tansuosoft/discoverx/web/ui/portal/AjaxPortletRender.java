/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.model.PortletDataSourceType;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 类型为“{@link PortletDataSourceType#Ajax}”的小窗口对应的{@link PortletContentRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AjaxPortletRender extends PortletContentRender {
	private String m_url = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portal
	 * @param portlet
	 */
	public AjaxPortletRender(Portal portal, Portlet portlet) {
		super(portal, portlet);
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portal
	 * @param portlet
	 * @param url ajax请求的url地址。
	 */
	public AjaxPortletRender(Portal portal, Portlet portlet, String url) {
		this(portal, portlet);
		this.m_url = url;
	}

	/**
	 * 重载render：返回发送ajax请求并显示请求内容的相关html/javascript代码。
	 * 
	 * <p>
	 * 格式说明：<br/>
	 * 
	 * 
	 * <pre>
	 * &lt;div id=&quot;portlet_ajaxresult_{portletalias}&quot;&gt;&lt;div class=&quot;waiting&quot;&gt;请稍侯，正在获取数据...&lt;/div&gt;&lt;/div&gt;
	 * &lt;script type=&quot;text/javascript&quot; language=&quot;javascript&quot;&gt;
	 * function sendAjaxRequest_{portletalias}(){
	 * 	var ajax=new AjaxRequest({method:'get',async:true,unique:true,onComplet:{@link PortletDataSource#getClientContentRender()}|portletContent.defaultAjaxResultRenderCallbackFunc（不配置时的系统默认值）});
	 * 	ajax.portlet={{@link Portlet}常用属性对应的json对象};
	 * 	ajax.dataSource={{@link PortletDataSource}常用属性对应的json对象};
	 * 	ajax.request({@link PortletDataSource#getTarget()});
	 * }
	 * window.setTimeout(sendAjaxRequest_{portletalias},{@link PortletContentRender#getAjaxPortletRequestTimeout()});
	 * window.setInterval(sendAjaxRequest_{portletalias},{@link PortletContentRender#getAjaxPortletRequestInterval()});
	 * &lt;/script&gt;
	 * </pre>
	 * 
	 * 其中“{portletalias}”为小窗口别名。 <br/>
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		try {
			PortletDataSource dataSource = this.getPortletDataSource();
			if (dataSource == null) throw new RuntimeException("无法获取小窗口数据源！");
			String targetUrl = jspContext.getPathContext().parseUrl((this.m_url != null ? this.m_url : dataSource.getTarget()));
			if (targetUrl == null || targetUrl.length() == 0) throw new RuntimeException("小窗口数据源中没有提供有效的url地址！");
			String palias = this.m_portlet.getAlias();
			String ajaxResultId = "portlet_ajaxresult_" + palias;
			String functionName = String.format("sendAjaxRequest_%1$s", palias);
			String complete = dataSource.getClientContentRender();
			if (complete == null || complete.length() == 0) complete = "portletContent.defaultAjaxResultRenderCallbackFunc";

			String crlf = "\r\n";
			sb.append("<div id=\"").append(ajaxResultId).append("\"><div class=\"waiting\">请稍侯，正在获取数据...</div></div>").append(crlf);
			sb.append("<script type=\"text/javascript\">").append(crlf);
			sb.append("function ").append(functionName).append("(){").append(crlf);
			sb.append("var ajax=new AjaxRequest({method:'get',async:true,unique:true,onComplete:").append(complete).append("});").append(crlf);
			sb.append("ajax.portlet={").append(this.getPortletJson()).append("};").append(crlf);
			sb.append("ajax.dataSource={").append(this.getDataSourceJson()).append("};").append(crlf);
			sb.append("ajax.request('").append(targetUrl).append("');").append(crlf);
			sb.append("}").append(crlf);
			sb.append("window.setTimeout(").append(functionName).append(",").append(this.getAjaxPortletRequestTimeout()).append(");").append(crlf);
			sb.append("window.setInterval(").append(functionName).append(",").append(this.getAjaxPortletRequestInterval()).append(");").append(crlf);
			sb.append("</script>");
		} catch (Exception ex) {
			return new PortletContentExceptionRender(this.m_portal, this.m_portlet, ex).render(jspContext);
		}
		return sb.toString();
	}
}

