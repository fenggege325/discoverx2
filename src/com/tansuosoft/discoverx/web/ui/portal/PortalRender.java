/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.List;

import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出门户({@link Portal})html内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class PortalRender implements HtmlRender {
	private Portal m_portal = null;
	private PortalParser m_parser = null;

	/**
	 * 接收门户资源（{@link Portal}）和小窗口显示区最大宽度像素值的构造器。
	 * 
	 * @param portal
	 * @param maxAvailWidth
	 */
	public PortalRender(Portal portal) {
		m_portal = portal;
		if (m_portal == null) throw new IllegalArgumentException("必须提供有效门户资源！");
		m_parser = PortalParser.getInstance(m_portal);
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		List<Portlet> portlets = null;
		PortletRender portletRender = null;
		StringBuilder sb = new StringBuilder();
		int columnCount = m_portal.getColumnCount();
		if (columnCount == 0) return "没有配置最大栏目数信息！";
		List<Integer> cws = m_portal.getColumnWidth();
		if (cws == null || cws.isEmpty()) return "没有配置栏目宽度信息！";
		if (cws.size() < columnCount) return "门户允许最大栏目数和小窗口实际栏目数不符！";

		int width = 0;
		for (int i = 0; i < columnCount; i++) {
			portlets = m_parser.getColumnPortlets(i + 1);
			sb.append("<div id=\"portletcolumn").append(i).append("\" class=\"portletcolumn\"");
			sb.append(" style=\"");
			width = cws.get(i);

			sb.append("width:").append(width).append("%;");
			sb.append("\"");
			sb.append(">\r\n");
			sb.append("<div class=\"portletdelimiter\"></div>\r\n");
			if (portlets != null && !portlets.isEmpty()) {
				for (Portlet x : portlets) {
					if (x == null) continue;
					portletRender = new PortletRender(x, m_portal, i, m_parser);
					sb.append(portletRender.render(jspContext));
				}
			}
			sb.append("</div>\r\n");
		}
		return sb.toString();
	}

	/**
	 * 返回对应的{@link PortalParser}对象。
	 * 
	 * @return
	 */
	public PortalParser getPortalParser() {
		return this.m_parser;
	}
}

