/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.List;

import com.tansuosoft.discoverx.bll.user.PortalShortcut;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出门户快捷操作、快捷链接内容的TreeView的{@link HtmlRender}实现类。
 * 
 * @author simon@tansuosoft.cn
 */
public class ShortcutsTreeViewRender implements HtmlRender {
	private List<PortalShortcut> m_shortcuts = null;

	/**
	 * 接收要呈现的PortalShortcut列表的构造器。
	 * 
	 * @param shortcuts List&lt;PortalShortcut&gt;，如果为null或空列表，则返回空字符串。
	 */
	public ShortcutsTreeViewRender(List<PortalShortcut> shortcuts) {
		this.m_shortcuts = shortcuts;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (this.m_shortcuts == null || this.m_shortcuts.isEmpty()) return "";
		StringBuilder sb = new StringBuilder();
		sb.append("id:").append("'").append("tvr_shortcutCustom").append("'");
		sb.append(",").append("label:").append("'").append("快捷菜单定制向导").append("'");

		sb.append(",").append("data:{"); // data begin
		sb.append("}"); // data end

		sb.append(",").append("children:").append("["); // children begin
		
		int index = 0;
		String icon = null;
		PathContext pathContext = jspContext.getPathContext();
		for (PortalShortcut x : m_shortcuts) {
			if (x == null) continue;
			if (index > 0) sb.append(",");
			sb.append("{"); // tvi child begin
			icon = x.getIcon();
			icon = pathContext.parseUrl(icon);
			sb.append("id:").append("'").append(x.getUNID()).append("'");
			sb.append(",").append("label:").append("'<img src=\"").append(icon).append("\">&nbsp").append(x.getTitle()).append("'");
			sb.append(",").append("desc:").append("'").append(x.getDescription()).append("'");
			sb.append(",").append("data:{"); // data begin
			sb.append("}"); // data end
			sb.append("}"); // tvi child end
			index++;
		}
		sb.append("]"); // children end

		return sb.toString();
	}
}

