/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;

/**
 * 解析门户（{@link Portal}）包含的小窗口（{@link Portlet}）信息以供显示时使用的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class PortalParser {
	private Portal m_portal = null; // 当前处理的的门户资源类
	private int m_columnCount = 0;
	private int m_maxRowCount = 0;
	private TreeSet<Portlet> m_sortedHiddenPortlets = null;
	private TreeSet<Portlet> m_sortedVisiblePortlets = null;
	private HashMap<Integer, List<Portlet>> m_parsed = new HashMap<Integer, List<Portlet>>();
	private HashMap<String, Integer> m_seq = new HashMap<String, Integer>();
	private boolean m_parseOk = false;

	private static HashMap<String, PortalParser> m_instances = new HashMap<String, PortalParser>();

	/**
	 * 获取指定{@link Portal}对应的{@link PortalParser}对象实例。
	 * 
	 * @param portal
	 * @return
	 */
	public synchronized static PortalParser getInstance(Portal portal) {
		if (portal == null) throw new IllegalArgumentException("必须提供有效门户资源！");
		PortalParser result = m_instances.get(portal.getUNID());
		if (result == null) {
			result = new PortalParser(portal);
			m_instances.put(portal.getUNID(), result);
		}
		return result;
	}

	/**
	 * 重新加载指定{@link Portal}对应的{@link PortalParser}对象实例。
	 * 
	 * @param portal
	 * @return
	 */
	public synchronized static PortalParser reloadInstance(Portal portal) {
		if (portal == null) throw new IllegalArgumentException("必须提供有效门户资源！");
		m_instances.remove(portal.getUNID());
		return getInstance(portal);
	}

	/**
	 * 接收门户资源（{@link Portal}）的构造器。
	 * 
	 * @param portal {@link Portal}
	 */
	private PortalParser(Portal portal) {
		this.m_portal = portal;
		parse();
	}

	/**
	 * 解析通过构造器提供的Portal。
	 */
	private void parse() {
		if (m_parseOk) return;
		if (this.m_portal == null) return;
		List<Portlet> portlets = this.m_portal.getPortlets();
		if (portlets == null || portlets.isEmpty()) return;
		m_sortedHiddenPortlets = new TreeSet<Portlet>(new PortletComparator());
		m_sortedVisiblePortlets = new TreeSet<Portlet>(new PortletComparator());
		TreeSet<Portlet> hSortedVisiblePortlets = new TreeSet<Portlet>(new PortletComparator(true));
		for (Portlet x : portlets) {
			if (x.getColumn() == 0 || x.getRow() == 0) {
				m_sortedHiddenPortlets.add(x);
			} else {
				m_sortedVisiblePortlets.add(x);
				hSortedVisiblePortlets.add(x);
			}
		}
		int column = 0;
		this.m_columnCount = this.m_portal.getColumnCount();
		int row = 0;
		int rowCount = 0;
		List<Portlet> list = null;
		for (Portlet x : m_sortedVisiblePortlets) {
			column = x.getColumn();
			if (row != x.getRow()) {
				rowCount++;
				if (this.m_maxRowCount < rowCount) this.m_maxRowCount = rowCount;
			}
			list = m_parsed.get(column);
			if (list == null) {
				list = new ArrayList<Portlet>();
				m_parsed.put(column, list);
			}
			list.add(x);
			row = x.getRow();
		}

		int portletSequence = 0;
		for (Portlet x : hSortedVisiblePortlets) {
			m_seq.put(x.getUNID(), portletSequence);
			portletSequence++;
		}

		m_parseOk = true;
	}

	/**
	 * 获取门户小窗口实际最大列数（栏目数）。
	 * 
	 * @return int
	 */
	public int getColumnCount() {
		return this.m_columnCount;
	}

	/**
	 * 获取门户小窗口实际最大行数。
	 * 
	 * @return int
	 */
	public int getMaxRowCount() {
		return this.m_maxRowCount;
	}

	/**
	 * 按栏目（列）序号返回同一栏目的所有{@link Portlet}的列表集合。
	 * 
	 * <p>
	 * 返回的列表集合中的{@link Portlet}对象按从上到下的顺序排序（即按{@link Portlet#getRow()}的结果从小到大的顺序排序）。
	 * </p>
	 * 
	 * @param columnIndex int，1表示第一个栏目，2表示第二个栏目，以此类推。
	 * @return List&lt;Portlet&gt; 如果找不到则返回null。
	 */
	public List<Portlet> getColumnPortlets(int columnIndex) {
		if (this.m_parsed == null || this.m_parsed.isEmpty() || columnIndex <= 0) return null;
		return this.m_parsed.get(columnIndex);
	}

	/**
	 * 获取指定小窗口的显示顺序。
	 * 
	 * @param portlet
	 * @return 显示顺序序号，从0开始，假设有3栏，则按第一栏第一行、第二栏第一行、第三栏第一行、第一栏第二行...的顺序依次递增。
	 */
	public int getPortletDisplaySequence(Portlet portlet) {
		if (portlet == null) return -1;
		Integer i = this.m_seq.get(portlet.getUNID());
		if (i == null) return -1;
		return i.intValue();
	}

}

