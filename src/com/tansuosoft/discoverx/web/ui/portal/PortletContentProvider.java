/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.List;

import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletContentType;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 提供{@link Portlet}内容的所有类需继承的基类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class PortletContentProvider {
	/**
	 * 返回运行时的{@link JSPContext}对象。
	 */
	protected JSPContext jspContext = null;

	/**
	 * 缺省构造器。
	 */
	public PortletContentProvider() {
	}

	/**
	 * 初始化，由系统调用。
	 * 
	 * @param jspContext
	 */
	protected void init(JSPContext jspContext) {
		this.jspContext = jspContext;
	}

	/**
	 * 获取小窗口内容提供对象返回的内容类型({@link PortletContentType})。
	 * 
	 * @return PortletContentType
	 */
	public abstract PortletContentType getContentType();

	/**
	 * 获取Xml类型小窗口内容结果。
	 * 
	 * <p>
	 * 返回结果为xml文本内容，{@link PortletContentProvider#getContentType()}应返回{@link PortletContentType#Xml}，否则此方法应返回null。
	 * </p>
	 * 
	 * @return String
	 */
	public abstract String getXmlResult();

	/**
	 * 获取Html类型小窗口内容结果。
	 * 
	 * <p>
	 * 返回结果为html文本内容，{@link PortletContentProvider#getContentType()}应返回{@link PortletContentType#Html}，否则此方法应返回null。
	 * </p>
	 * 
	 * @return String
	 */
	public abstract String getHtmlResult();

	/**
	 * 获取{@link PortletEntry}集合类型小窗口内容结果。
	 * 
	 * <p>
	 * 返回结果为{@link PortletEntry}列表集合，{@link PortletContentProvider#getContentType()}应返回{@link PortletContentType#Entry}，否则此方法应返回null。
	 * </p>
	 * 
	 * @return List<PortletEntry>
	 */
	public abstract List<PortletEntry> getEntriesResult();

	/**
	 * 获取Url地址类型小窗口内容结果。
	 * 
	 * <p>
	 * 返回结果为Url地址文本，{@link PortletContentProvider#getContentType()}应返回{@link PortletContentType#Url}，否则此方法应返回null。
	 * </p>
	 * <p>
	 * 如果是返回url时，默认使用ajax方式请求url返回的地址以显示小窗口内容，如果要用iframe显示，请在对应数据源（{@link PortletDataSource}）中配置一个名为“iframeFlag”值为“true”的额外参数以指示使用iframe显示url内容。
	 * </p>
	 * 
	 * @return String
	 */
	public abstract String getUrlResult();
}

