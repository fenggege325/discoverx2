/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletContentType;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.Browser;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.app.tsim.Presence;

/**
 * 输出会话信息的{@link Portlet}内容的{@link PortletContentProvider}实现类。
 * 
 * @author coca@tensosoft.com
 */
public class SessionInfoPortlet extends PortletContentProvider {

	/**
	 * @see com.tansuosoft.discoverx.web.ui.portal.PortletContentProvider#getContentType()
	 */
	@Override
	public PortletContentType getContentType() {
		return PortletContentType.Html;
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.portal.PortletContentProvider#getXmlResult()
	 */
	@Override
	public String getXmlResult() {
		return null;
	}

	private static long validAccount = 0;

	/**
	 * 重载：以html格式输出会话信息对应的小窗口内容。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.portal.PortletContentProvider#getHtmlResult()
	 */
	@Override
	public String getHtmlResult() {
		Map<String, HttpSession> httpSessions = Session.getAllHttpSession();
		int httpSessionCount = httpSessions.size();
		Map<String, Session> sessions = Collections.synchronizedMap(Session.getAllSession());
		int sessionCount = sessions.size();
		int presence = 0;
		String presenceName = null;
		TreeMap<String, Integer> presenceInfo = new TreeMap<String, Integer>();
		int tsimCnt = 0;
		Integer tmpCnt = 0;
		Collection<Session> sessionValues = sessions.values();
		synchronized (sessions) {
			for (Session s : sessionValues) {
				if (s == null) continue;
				presence = s.getTsimPresence();
				if (presence == 0 || presence == 40011) continue;
				tsimCnt++;
				presenceName = (presence == 0 ? null : Presence.getName(presence + ""));
				if (!presenceName.equals(presence)) {
					tmpCnt = presenceInfo.get(presenceName);
					if (tmpCnt == null) {
						presenceInfo.put(presenceName, 1);
					} else {
						presenceInfo.put(presenceName, tmpCnt.intValue() + 1);
					}
				}
			}
		}
		if (validAccount == 0) {
			DBRequest dbr = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper result = new SQLWrapper();
					result.setSql("select count(*) from t_user where c_accounttype=3");
					result.setRequestType(RequestType.Scalar);
					return result;
				}
			};
			dbr.sendRequest();
			validAccount = dbr.getResultLong();
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<ul style=\"margin:0px;padding:3px 0px;padding-left:20px;line-height:20px;\">");
		sb.append("<li>").append("可登录用户数：").append(validAccount).append("</li>");
		sb.append("<li>").append("活动会话个数：").append(httpSessionCount).append("</li>");
		sb.append("<li>").append("登录会话个数：").append(sessionCount).append("</li>");
		if (tsimCnt > 0) {
			sb.append("<li>").append("&nbsp;&nbsp;小助手登录会话个数：").append(tsimCnt).append("</li>");
			if (presenceInfo.size() > 0) {
				for (String s : presenceInfo.keySet()) {
					sb.append("<li>").append("&nbsp;&nbsp;&nbsp;&nbsp;").append(s).append("：").append(presenceInfo.get(s)).append("</li>");
				}
			}
		}
		sb.append("</ul>");
		if (jspContext.hasLogin()) {
			sb.append("<hr style=\"float:right;width:50%;margin-right:3px;\"/>");
			sb.append("<a style=\"clear:both;float:right;margin-right:3px;\" target=\"_blank\" href=\"../admin/session_info.jsp\">[查看详细会话信息]</a>");
		}
		return sb.toString();
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.portal.PortletContentProvider#getEntriesResult()
	 */
	@Override
	public List<PortletEntry> getEntriesResult() {
		return null;
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.portal.PortletContentProvider#getUrlResult()
	 */
	@Override
	public String getUrlResult() {
		return null;
	}

	public static final String NA = "-";

	/**
	 * 输出详细会话信息。
	 * 
	 * @param jspContext
	 * @return String
	 */
	public static String outputDetail(JSPContext jspContext) {
		boolean isAdmin = SecurityHelper.checkUserRole(jspContext.getLoginUser(), Role.ROLE_ADMIN_SC);
		StringBuilder sb = new StringBuilder();
		sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">").append("\r\n");
		sb.append("<colgroup><col/><col width=\"140\"/><col width=\"140\"/><col width=\"80\"/><col width=\"100\"/><col width=\"80\"/></colgroup>").append("\r\n");
		sb.append("<tr class=\"head\"><td>用户</td><td>会话创建时间</td><td>最近访问时间</td><td>登录客户端</td><td>网络地址</td><td>小助手状态</td></tr>").append("\r\n");

		Map<String, HttpSession> httpSessions = Collections.synchronizedMap(Session.getAllHttpSession());
		Map<String, Session> sessions = Collections.synchronizedMap(Session.getAllSession());
		Map<String, String> findMap = new HashMap<String, String>();
		TreeMap<String, GenericPair<HttpSession, Session>> tree = new TreeMap<String, GenericPair<HttpSession, Session>>();
		// System.out.println("http会话个数：" + httpSessions.size() + "，用户会话个数：" + sessions.size());

		GenericPair<HttpSession, Session> gp = null;
		Object obj = null;
		Session s = null;
		String key = null;
		String cdt = null;
		String ladt = null;
		String fn = null;
		User a = User.getAnonymous();
		Collection<HttpSession> httpSessionValues = httpSessions.values();
		synchronized (httpSessions) {
			for (HttpSession x : httpSessionValues) {
				if (x == null) continue;
				cdt = new DateTime(x.getCreationTime()).toString();
				ladt = new DateTime(x.getLastAccessedTime()).toString();
				obj = x.getAttribute(Session.USERSESSION_PARAM_NAME_IN_HTTPSESSION);
				s = (obj != null && (obj instanceof Session) ? (Session) obj : null);
				fn = Session.getUser(s).getName();
				if (findMap.containsKey(fn)) continue;
				key = String.format("%1$s%2$s%3$s", cdt, ladt, fn);
				if (a != null && a.getName().equals(fn)) {
					key = cdt + ladt;
				} else {
					findMap.put(fn, cdt + ladt);
				}
				gp = new GenericPair<HttpSession, Session>(x, s);
				tree.put(key, gp);
			}
		}
		Collection<Session> sessionValues = sessions.values();
		synchronized (sessions) {
			for (Session x : sessionValues) {
				if (x == null) continue;
				fn = Session.getUser(x).getName();
				key = String.format("%1$s%2$s", StringUtil.getValueString(findMap.get(fn), ""), fn);
				if (!tree.containsKey(key)) {
					gp = new GenericPair<HttpSession, Session>(null, x);
					tree.put(key, gp);
				}
			}
		}
		findMap.clear();
		findMap = null;

		HttpSession x = null;
		int idx = 0;
		String userInfo = null;
		Browser b = null;
		String client = null;
		String clients[] = null;
		StringBuilder clientNames = null;
		String address[] = null;
		String loginAddress = null;
		int presence = 0;
		String presenceName = null;
		String tsimStatus = null;
		Map<String, Boolean> cm = new HashMap<String, Boolean>();
		for (GenericPair<HttpSession, Session> gpx : tree.values()) {
			x = gpx.getKey();
			s = gpx.getValue();
			if (s == null) continue;
			cdt = (x == null ? NA : new DateTime(x.getCreationTime()).toString());
			ladt = (x == null ? NA : new DateTime(x.getLastAccessedTime()).toString());
			userInfo = String.format(Session.getUser(s).getName());
			client = (s == null ? NA : StringUtil.getValueString(s.getLoginClient(), NA));
			if (!client.equals(NA)) {
				clientNames = new StringBuilder();
				clients = StringUtil.splitString(client, Session.ADDRESS_AND_CLIENT_SEP);
				String bvn = null;
				for (String c : clients) {
					if (c == null || c.length() == 0) continue;
					b = Browser.getBrowser(c);
					bvn = Browser.getBrowserVersionName(b);
					if (!cm.containsKey(bvn)) {
						clientNames.append(clientNames.length() > 0 ? "<br/>" : "");
						clientNames.append(bvn);
						cm.put(bvn, true);
					}
				}
				client = clientNames.toString();
			}

			loginAddress = StringUtil.getValueString(s.getLoginAddress(), NA);
			presence = (s == null ? 0 : s.getTsimPresence());
			presenceName = (presence == 0 ? null : Presence.getName(presence + ""));
			tsimStatus = (presenceName == null || presenceName.equals(presence) || presence == 40011 ? NA : presenceName);
			sb.append("<tr class=\"").append((idx % 2 == 0 ? "even" : "odd")).append("\">");
			sb.append("<td>").append(userInfo);
			if (isAdmin && (x != null || s != null)) {
				sb.append("<a href=\"?").append(x != null ? "" : "u").append("sid=").append(x != null ? x.getId() : s.getSessionUNID()).append("\" target=\"removetarget\"");
				sb.append(" onclick=\"return window.confirm('此操作将导致用户会话被删除，确定要继续此操作？');\"");
				sb.append(">");
				sb.append("[删除]");
				sb.append("</a>");
			}
			sb.append("</td>");
			sb.append("<td>").append(cdt).append("</td>");
			sb.append("<td>").append(ladt).append("</td>");
			sb.append("<td>").append(client).append("</td>");
			sb.append("<td>");
			if (loginAddress == null) {
				sb.append(NA);
			} else {
				address = StringUtil.splitString(loginAddress, Session.ADDRESS_AND_CLIENT_SEP);
				cm.clear();
				int aidx = 0;
				for (String c : address) {
					if (c == null || c.length() == 0) continue;
					if (!cm.containsKey(c)) {
						sb.append(aidx == 0 ? "" : "<br/>").append(c);
						cm.put(c, true);
						aidx++;
					}
				}
			}
			sb.append("</td>");
			sb.append("<td>").append(tsimStatus).append("</td>");
			sb.append("</tr>").append("\r\n");
			idx++;
		}// for end
		sb.append("<tr class=\"").append((idx % 2 == 0 ? "even" : "odd")).append("\">");
		sb.append("<td colspan=\"6\" style=\"text-align:right;\">");
		sb.append("活动会话个数：<strong>").append(httpSessions.size()).append("</strong>，登录会话个数：<strong>").append(sessions.size()).append("</strong>");
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</table>").append("\r\n");
		return sb.toString();
	}
}

