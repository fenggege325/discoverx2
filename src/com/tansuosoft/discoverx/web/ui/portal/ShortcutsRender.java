/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.List;

import com.tansuosoft.discoverx.bll.user.PortalShortcut;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出门户快捷操作、快捷链接内容的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ShortcutsRender implements HtmlRender {
	private List<PortalShortcut> m_shortcuts = null;

	/**
	 * 接收要呈现的PortalShortcut列表的构造器。
	 * 
	 * @param shortcuts List&lt;PortalShortcut&gt;，如果为null或空列表，则返回空字符串。
	 */
	public ShortcutsRender(List<PortalShortcut> shortcuts) {
		this.m_shortcuts = shortcuts;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (this.m_shortcuts == null || this.m_shortcuts.isEmpty()) return "";
		StringBuilder sb = new StringBuilder();
		boolean isJavascript = false;
		String target = null;
		String desc = null;
		String title = null;
		String targetWindow = null;
		String icon = null;
		PathContext pathContext = jspContext.getPathContext();
		for (PortalShortcut x : m_shortcuts) {
			if (x == null) continue;
			target = x.getTarget();
			if (target == null || target.length() == 0) target = "#";
			isJavascript = target.toLowerCase().startsWith("javascript:");
			title = x.getTitle();
			desc = x.getDescription();
			targetWindow = x.getTargetWindow();
			if (targetWindow == null || targetWindow.length() == 0) targetWindow = "_blank";
			if (desc == null || desc.length() == 0) desc = title;
			icon = x.getIcon();
			icon = pathContext.parseUrl(icon);
			sb.append("<a id=\"").append(x.getUNID()).append("\" class=\"portalshortcut\" title=\"").append(desc).append("\"");
			if (isJavascript) {
				sb.append(" href=\"").append(target).append("\"");
			} else {
				sb.append(" href=\"").append(pathContext.parseUrl(target)).append("\"");
				sb.append(" target=\"").append(targetWindow).append("\"");
			}
			sb.append(" icon=\"").append(icon).append("\"");
			sb.append(">").append(title);
			sb.append("</a>");
		}
		return sb.toString();
	}
}

