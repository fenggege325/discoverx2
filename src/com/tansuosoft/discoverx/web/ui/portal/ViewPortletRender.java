/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.view.Pagination;
import com.tansuosoft.discoverx.bll.view.ViewEntry;
import com.tansuosoft.discoverx.bll.view.ViewEntryCallback;
import com.tansuosoft.discoverx.bll.view.ViewQuery;
import com.tansuosoft.discoverx.bll.view.ViewQueryProvider;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletContentType;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.model.PortletDataSourceType;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.view.ViewForm;
import com.tansuosoft.discoverx.web.ui.view.XmlViewEntryCallback;

/**
 * 类型为“{@link PortletDataSourceType#View}”的小窗口对应的{@link PortletContentRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewPortletRender extends PortletContentRender {

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portal
	 * @param portlet
	 */
	public ViewPortletRender(Portal portal, Portlet portlet) {
		super(portal, portlet);
	}

	/**
	 * 重载render
	 * <p>
	 * 根据{@link PortletDataSource#getContentType()}的结果有不同的返回格式：<br/>
	 * <ul>
	 * <li>{@link PortletContentType#Html}类型：<br/>
	 * 
	 * <pre>
	 * &lt;ul class=&quot;{ulclass}&quot;&gt;
	 * &lt;li class=&quot;{liclass}&quot;&gt;&lt;a href=&quot;目标资源（一般是文档）打开地址&quot; target=&quot;{target}&quot;&gt;{视图所有列值用空格分隔}&lt;/a&gt;&lt;/li&gt;
	 * ...
	 * &lt;/ul&gt;
	 * </pre>
	 * 
	 * {ulclass}表示通过{@link PortletDataSource#getParamValueString(String, String)}获取的名为“ulclass”的参数值结果；<br/>
	 * {liclass}表示通过{@link PortletDataSource#getParamValueString(String, String)}获取的名为“liclass”的参数值结果；<br/>
	 * {target}表示通过{@link PortletDataSource#getParamValueString(String, String)}获取的名为“target”的参数值结果，默认为“blank”。</li>
	 * <li>
	 * {@link PortletContentType#Entry}类型：返回通过{@link EntryPortletRender}输出的视图数据条目对应的{@link PortletEntry}列表结果。</li>
	 * <li>
	 * {@link PortletContentType#Xml}类型：返回通过{@link XmlPortletRender}输出的结果，传给{@link XmlPortletRender}的xml内容通过{@link XmlViewEntryCallback}组合。</li>
	 * <li>其余类型不支持！</li>
	 * </ul>
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@SuppressWarnings("unchecked")
	public String render(JSPContext jspContext) {
		ViewEntryCallback callback = null;
		PortletContentRender delegate = null;
		try {
			PortletDataSource dataSource = this.getPortletDataSource();
			if (dataSource == null) throw new RuntimeException("无法获取小窗口数据源！");
			PortletContentType contentType = dataSource.getContentType();
			String viewID = dataSource.getTarget();
			String viewUNID = viewID.startsWith("view") ? ResourceAliasContext.getInstance().getUNIDByAlias(View.class, viewID) : viewID;
			View view = ResourceContext.getInstance().getResource(viewUNID, View.class);
			if (view == null) throw new RuntimeException("无法获取小窗口绑定数据源中“" + viewID + "”对应的视图！");
			ViewForm viewForm = new ViewForm();
			viewForm.setUNID(viewUNID);
			Pagination pagination = new Pagination();
			pagination.setDisplayCountPerPage(this.m_portlet.getDisplayCount());
			ViewQuery viewQuery = ViewQueryProvider.getViewQuery(view, jspContext.getUserSession(), pagination);

			final List<PortletEntryTitle> portletEntryTitles = new ArrayList<PortletEntryTitle>();

			if (contentType.equals(PortletContentType.Html)) {// html类型
				callback = new ViewEntryCallback() {
					private StringBuilder sb = new StringBuilder();
					private View view = null;
					private String liclass = "";
					private String target = null;

					@Override
					public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
						if (target == null) {
							view = query.getViewParser().getView();
							target = this.getParameterString("target", "_blank");
							liclass = this.getParameterString("liclass", "portletentryli");
							if (liclass != null && liclass.length() > 0) liclass = String.format(" class=\"%1$s\"", liclass);
						}
						sb.append("<li").append(liclass).append(">");
						String dir = null;
						if (view.getViewType() == ViewType.XMLResourceView) {
							dir = view.getTable();
						} else if (view.getViewType() == ViewType.DBResourceView) {
							dir = StringUtil.stringRight(view.getTable(), "_");
						} else {
							dir = ResourceDescriptorConfig.DOCUMENT_DIRECTORY;
						}

						sb.append("<a target=\"").append(target).append("\" href=\"").append(dir).append(".jsp?unid=").append(viewEntry.getUnid()).append("\">");
						List<String> colValues = viewEntry.getValues();
						if (colValues != null) {
							int colIndex = 0;
							for (String x : colValues) {
								sb.append(colIndex == 0 ? "" : "&nbsp;").append(x);
								colIndex++;
							}
						}
						sb.append("</a>");
						sb.append("</li>\r\n");
					}

					@Override
					public Object getResult() {
						String ulclass = this.getParameterString("ulclass", "portletentryul");
						if (ulclass != null && ulclass.length() > 0) ulclass = String.format(" class=\"%1$s\"", ulclass);
						sb.insert(0, "<ul" + ulclass + ">\r\n");
						sb.append("</ul>");
						return sb.toString();
					}
				};
				callback.setParameter("target", dataSource.getParamValueString("target", "_blank"));
				callback.setParameter("ulclass", dataSource.getParamValueString("ulclass", ""));
				callback.setParameter("liclass", dataSource.getParamValueString("liclass", ""));
			} else if (contentType.equals(PortletContentType.Xml)) {// xml类型
				callback = new XmlViewEntryCallback();
			} else if (contentType.equals(PortletContentType.Entry)) {// json Entry类型
				callback = new ViewEntryCallback() {
					private List<PortletEntry> list = null;

					@Override
					public void entryFetched(ViewEntry viewEntry, ViewQuery query) {
						if (list == null) list = new ArrayList<PortletEntry>();
						list.add(new PortletEntry(viewEntry));
					}

					@Override
					public Object getResult() {
						View view = this.getView();
						if (view == null) new RuntimeException("未知视图！");
						for (ViewColumn vc : view.getColumns()) {
							portletEntryTitles.add(new PortletEntryTitle(vc.getTitle(), String.format("%1$d%2$s", vc.getWidth(), view.getMeasurement() == SizeMeasurement.Percentage ? "%" : "px")));
						}
						return list;
					}
				};
			} else if (contentType.equals(PortletContentType.Url)) {// Url类型
				throw new RuntimeException("视图数据源不支持Url内容类型！");
			}

			if (callback == null) throw new RuntimeException("无法获取匹配的视图数据组装对象！");
			viewQuery.addViewEntryCallback(callback);
			int total = viewQuery.getEntryCount();
			viewQuery.getViewEntries();
			if (contentType.equals(PortletContentType.Entry)) {
				delegate = new EntryPortletRender(m_portal, m_portlet, (List<PortletEntry>) callback.getResult());
				EntryPortletRender r = (EntryPortletRender) delegate;
				if (portletEntryTitles.size() > 0) r.setPortletEntryTitles(portletEntryTitles);
				delegate.setTotal(total);
			} else if (contentType.equals(PortletContentType.Xml)) {
				delegate = new XmlPortletRender(m_portal, m_portlet, callback.getResult().toString());
				delegate.setTotal(total);
			}
		} catch (Exception ex) {
			return new PortletContentExceptionRender(this.m_portal, this.m_portlet, ex).render(jspContext);
		}
		return (delegate == null ? callback.getResult().toString() : delegate.render(jspContext));
	}
}

