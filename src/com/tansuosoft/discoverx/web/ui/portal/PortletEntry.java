/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.List;

import com.tansuosoft.discoverx.bll.view.ViewEntry;

/**
 * 表示一条小窗口内容数据条目的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class PortletEntry extends ViewEntry {
	/**
	 * 缺省构造器。
	 */
	public PortletEntry() {
		super();
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param id
	 * @param values
	 */
	public PortletEntry(String id, List<String> values) {
		super(id, values);
	}

	/**
	 * 接收{@link ViewEntry}的构造器。
	 * 
	 * @param viewEntry
	 */
	public PortletEntry(ViewEntry viewEntry) {
		this((viewEntry == null ? null : viewEntry.getUnid()), (viewEntry == null ? null : viewEntry.getValues()));
	}
}

