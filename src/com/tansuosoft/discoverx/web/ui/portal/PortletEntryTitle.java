/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

/**
 * 表示一个小窗口数据条目标题信息的类。
 * 
 * <p>
 * 用于控制客户端呈现时，小窗口数据条目包含的列值（{@link PortletEntry#getValues()}）中每一个列的宽度。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class PortletEntryTitle {
	/**
	 * 缺省构造器。
	 */
	public PortletEntryTitle() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param name
	 * @param width
	 */
	public PortletEntryTitle(String name, String width) {
		this.m_name = name;
		this.m_width = width;
	}

	private String m_name = null; // 小窗口数据条目标题名称。
	private String m_width = null; // 小窗口数据条目列的宽度。

	/**
	 * 返回小窗口数据条目标题名称。
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置小窗口数据条目标题名称。
	 * 
	 * @param name String
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回小窗口数据条目列的宽度。
	 * 
	 * @return String
	 */
	public String getWidth() {
		return this.m_width;
	}

	/**
	 * 设置小窗口数据条目列的宽度。
	 * 
	 * <p>
	 * 宽度必须带有效的单位，如“%”、“px”等。
	 * </p>
	 * 
	 * @param width String
	 */
	public void setWidth(String width) {
		this.m_width = width;
	}

	/**
	 * 重载：按照“{name:'[namevalue]',width:'[widthvalue]'}”格式输出结果。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("{name:'%1$s',width:'%2$s'}", m_name, m_width);
	}
}

