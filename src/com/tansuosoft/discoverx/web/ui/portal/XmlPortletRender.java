/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletContentType;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 内容类型为Xml（{@link PortletContentType#Xml}）的小窗口对应的{@link PortletContentRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class XmlPortletRender extends PortletContentRender {
	private String m_xml = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portal
	 * @param portlet
	 * @param xml 原始xml文本。
	 */
	public XmlPortletRender(Portal portal, Portlet portlet, String xml) {
		super(portal, portlet);
		m_xml = xml;
	}

	/**
	 * 重载render
	 * 
	 * <p>
	 * 输出格式：<br/>
	 * 
	 * <pre>
	 * &lt;div id=&quot;portlet_xmlresult_{portletalias}&quot;&gt;&lt;/div&gt;
	 * &lt;xml id=&quot;portlet_xmlcontent_{portletalias}&quot;&gt;{xml}&lt;/xml&gt;
	 * &lt;script type=&quot;text/javascript&quot; language=&quot;javascript&quot;&gt;{@link PortletDataSource#getClientContentRender()}或portletContent.defaultXmlResultRender（不配置时的系统默认值）('portlet_xmlcontent_{portletalias}',{portletJson},{dataSourceJson});&lt;/script&gt;
	 * </pre>
	 * 
	 * 其中“{portletalias}”为小窗口别名，“{portletJson}”、“{dataSourceJson}”分别为包含portlet和对应数据源常用属性的json对象。 <br/>
	 * “{xml}”为提供的原始xml文本。<br/>
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		try {
			PortletDataSource dataSource = this.getPortletDataSource();
			if (dataSource == null) throw new RuntimeException("无法获取小窗口数据源！");
			String palias = m_portlet.getAlias();
			String xmlResultId = "portlet_xmlresult_" + palias;
			String xmlContentId = "portlet_xmlcontent_" + palias;
			sb.append("<div id=\"").append(xmlResultId).append("\"></div>\r\n");
			sb.append("<xml id=\"").append(xmlContentId).append("\">").append(this.m_xml).append("</xml>\r\n");
			String clientRender = dataSource.getClientContentRender();
			if (clientRender == null || clientRender.length() == 0) clientRender = "portletContent.defaultXmlResultRender";
			sb.append("<script type=\"text/javascript\">").append(clientRender).append("(");
			sb.append("'").append(xmlContentId).append("'");
			sb.append(",").append("{").append(this.getPortletJson()).append("}");
			sb.append(",").append("{").append(this.getDataSourceJson()).append("}");
			sb.append(");").append("</script>\r\n");
		} catch (Exception ex) {
			return new PortletContentExceptionRender(this.m_portal, this.m_portlet, ex).render(jspContext);
		}
		return sb.toString();
	}
}

