/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import com.tansuosoft.discoverx.common.exception.BaseException;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 未知类型数据源小窗口对应的{@link PortletContentRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class PortletContentExceptionRender extends PortletContentRender {

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portal
	 * @param portlet
	 * @param exception
	 */
	public PortletContentExceptionRender(Portal portal, Portlet portlet, Exception exception) {
		super(portal, portlet);
		m_exception = exception;
	}

	private Exception m_exception = null;

	/**
	 * 重载render：输出一个div节点，其内容为传入的异常对象的message属性（即{@link Exception#getMessage()}的结果）。
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		if (this.m_exception == null) return "<div>显示内容时发生未知错误！</div>";
		if (this.m_exception instanceof java.lang.NullPointerException) return "<div>出现空指针或空引用异常！</div>";
		Throwable root = null;
		if (this.m_exception instanceof BaseException) {
			root = ((BaseException) this.m_exception).getRootThrowable();
		}
		String msg = (root == null ? this.m_exception.getMessage() : root.getMessage());
		return String.format("<div>%1$s</div>", msg);
	}
}

