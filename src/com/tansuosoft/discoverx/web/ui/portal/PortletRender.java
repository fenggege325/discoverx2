/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.web.Browser;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出{@link Portlet}内容Html代码的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class PortletRender implements HtmlRender {
	private Portlet m_portlet = null;
	private Portal m_portal = null;
	private int m_columnIndex = 0;
	private PortalParser m_portalParser = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portlet
	 * @param portal
	 * @param columnIndex
	 * @param rowIndex
	 * @param portalParser
	 */
	protected PortletRender(Portlet portlet, Portal portal, int columnIndex, PortalParser portalParser) {
		m_portlet = portlet;
		if (m_portlet == null) throw new IllegalArgumentException("必须提供有效的小窗口对象！");
		m_portal = portal;
		m_columnIndex = columnIndex;
		m_portalParser = portalParser;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		boolean isFirst = (m_columnIndex == 0);
		boolean isLast = (m_portalParser != null && m_columnIndex == (m_portalParser.getColumnCount() - 1));
		StringBuilder sb = new StringBuilder();
		sb.append("<div id=\"portlet_").append(m_portlet.getUNID()).append("\" class=\"portlet\"");
		sb.append(" style=\"");
		sb.append("width:").append(isFirst || isLast ? "99.5" : "99").append("%;");
		sb.append("margin-top:").append("6px").append(";");
		if (isFirst) {
			sb.append("margin-right:").append("0.5").append("%;");
		} else if (isLast) {
			sb.append("margin-left:").append("0.5").append("%;");
		} else {
			sb.append("margin-left:").append("0.5").append("%;");
			sb.append("margin-right:").append("0.5").append("%;");
		}
		sb.append("\"");
		sb.append(">\r\n");
		if (m_portlet.getShowTitle()) {
			sb.append("<div class=\"portlettitle\">");
			sb.append("<div class=\"portlettitleleft\"></div>");
			sb.append("<div class=\"portlettitlemiddle\">");
			sb.append("<div class=\"portleticon\"");
			String titleIcon = m_portlet.getTitleIcon();
			if (titleIcon != null && titleIcon.length() > 0) {
				PathContext pathContext = jspContext.getPathContext();
				titleIcon = pathContext.parseUrl(titleIcon);
				if (titleIcon != null && titleIcon.indexOf('/') < 0) {
					titleIcon = pathContext.getCommonIconsPath() + titleIcon;
				}
				if (titleIcon != null && titleIcon.length() > 0) {
					sb.append(" style=\"");
					sb.append("background-image:url(").append(titleIcon).append(");");
					sb.append("\"");
				}
			}
			sb.append("></div>");
			sb.append("<div class=\"portletcaption\" id=\"portlet_").append(m_portlet.getUNID()).append("_h\"");
			sb.append(" title=\"单击此处拖动此小窗口到释放区以调整位置\">");
			sb.append(m_portlet.getName()).append("</div>");
			sb.append("<div class=\"portletcontrol\">");
			sb.append("<a class=\"portletcontrolmin\" title=\"显示/隐藏\" onclick=\"portletMin('portlet_").append(m_portlet.getUNID()).append("',this);\"></a>");
			sb.append("<a class=\"portletcontroldel\" title=\"删除\" onclick=\"portletDel('portlet_").append(m_portlet.getUNID()).append("');\"></a>");
			sb.append("</div>"); // portletcontrol end
			sb.append("</div>"); // portlettitlemiddle end
			sb.append("<div class=\"portlettitleright\"></div>");
			sb.append("</div>\r\n"); // portlettitle end
		}
		sb.append("<div class=\"portletbody\"");
		sb.append(" style=\"");
		int h = (m_portlet.getHeight() <= 0 ? 120 : m_portlet.getHeight());
		sb.append("min-height:").append(h).append("px;");
		// if (jspContext.getBrowser() == Browser.IE6) {
		if (jspContext.getBrowser().check(Browser.MSIE, 6)) {
			sb.append("_height:").append(h).append("px;");
		}
		sb.append("\"");
		sb.append(">\r\n");
		PortletContentRender portletContentRender = PortletContentRender.getPortletContentRender(m_portal, m_portlet);
		if (portletContentRender == null) portletContentRender = new PortletContentExceptionRender(m_portal, m_portlet, new Exception("无法获取有效小窗口内容提供对象！"));
		sb.append(portletContentRender.render(jspContext)).append("\r\n");
		sb.append("</div>\r\n"); // portletbody end
		sb.append("<div class=\"portletbottom\"><div class=\"portletbottomleft\"></div><div class=\"portletbottommiddle\"></div><div class=\"portletbottomright\"></div></div>");
		sb.append("</div>\r\n"); // portlet end
		return sb.toString();
	}
}

