/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.user.PortalShortcut;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 表示系统默认使用的快捷操作、快捷链接配置的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ShortcutsConfig extends Config {

	/**
	 * 缺省构造器。
	 */
	private ShortcutsConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static ShortcutsConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return ShortcutsConfig
	 */
	public static ShortcutsConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new ShortcutsConfig();
			}
		}
		return m_instance;
	}

	private List<PortalShortcut> m_shortcutActions = null; // 系统默认使用的快捷操作列表。
	private List<PortalShortcut> m_shortcutLinks = null; // 系统默认使用的快捷链接列表。

	/**
	 * 获取系统默认使用的快捷操作列表。
	 * 
	 * @return List<PortalShortcut>
	 */
	public List<PortalShortcut> getShortcutActions() {
		return m_shortcutActions;
	}

	/**
	 * 设置系统默认使用的快捷操作列表。
	 * 
	 * @param actions List<PortalShortcut>
	 */
	public void setShortcutActions(List<PortalShortcut> actions) {
		m_shortcutActions = actions;
	}

	/**
	 * 获取系统默认使用的快捷链接列表。
	 * 
	 * @return List<PortalShortcut>
	 */
	public List<PortalShortcut> getShortcutLinks() {
		return m_shortcutLinks;
	}

	/**
	 * 设置系统默认使用的快捷链接列表。
	 * 
	 * @param links List<PortalShortcut>
	 */
	public void setShortcutLinks(List<PortalShortcut> links) {
		m_shortcutLinks = links;
	}

	/**
	 * 获取指定用户自定义会话对应用户的门户快捷链接。
	 * 
	 * <p>
	 * 如果用户有自定义的快捷链接则返回之，否则返回系统默认的快捷链接。
	 * </p>
	 * 
	 * @param session
	 * @return
	 */
	public static List<PortalShortcut> getShortcutLinks(Session session) {
		Profile profile = Profile.getInstance(session);
		List<String> result = profile.getShortcutLinks();
		List<PortalShortcut> portalShortcutResult = new ArrayList<PortalShortcut>();
		List<PortalShortcut> config = ShortcutsConfig.getInstance().getShortcutLinks();
		if (config == null) config = new ArrayList<PortalShortcut>();
		if (result != null && result.size() > 0) {
			for (String s : result) {
				if (s == null || s.length() == 0) continue;
				for (PortalShortcut p : config) {
					if (s.equalsIgnoreCase(p.getUNID())) portalShortcutResult.add(p);
				}
			}
			return portalShortcutResult;
		}
		for (PortalShortcut p : config) {
			if (p != null && p.getSystemDefault()) portalShortcutResult.add(p);
		}
		return portalShortcutResult;
	}

	/**
	 * 获取指定用户自定义会话对应用户的门户快捷操作。
	 * 
	 * <p>
	 * 如果用户有自定义的快捷操作则返回之，否则返回系统默认的快捷操作。
	 * </p>
	 * 
	 * @param session
	 * @return
	 */
	public static List<PortalShortcut> getShortcutActions(Session session) {
		Profile profile = Profile.getInstance(session);
		List<String> result = profile.getShortcutActions();
		List<PortalShortcut> portalShortcutResult = new ArrayList<PortalShortcut>();
		List<PortalShortcut> config = ShortcutsConfig.getInstance().getShortcutActions();
		if (config == null) config = new ArrayList<PortalShortcut>();
		if (result != null && result.size() > 0) {
			for (String s : result) {
				if (s == null || s.length() == 0) continue;
				for (PortalShortcut p : config) {
					if (s.equalsIgnoreCase(p.getUNID())) {
						portalShortcutResult.add(p);
					}
				}
			}
			return portalShortcutResult;
		}
		for (PortalShortcut p : config) {
			if (p != null && p.getSystemDefault()) portalShortcutResult.add(p);
		}

		return portalShortcutResult;
	}
}

