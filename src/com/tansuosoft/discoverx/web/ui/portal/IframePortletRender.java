/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.model.PortletDataSourceType;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 类型为“{@link PortletDataSourceType#Iframe}”的小窗口对应的{@link PortletContentRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class IframePortletRender extends PortletContentRender {
	private String m_url = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portal
	 * @param portlet
	 */
	public IframePortletRender(Portal portal, Portlet portlet) {
		super(portal, portlet);
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portal
	 * @param portlet
	 * @param url 显示iframe内容对应的url地址。
	 */
	public IframePortletRender(Portal portal, Portlet portlet, String url) {
		this(portal, portlet);
		m_url = url;
	}

	/**
	 * 重载render
	 * <p>
	 * 格式说明：<br/>
	 * 
	 * <pre>
	 * &lt;iframe {attr} id=&quot;iframe_{@link Portlet#getUNID()}&quot; src=&quot;{@link PortletDataSource#getTarget()}&gt;&lt;/iframe&gt;&quot;
	 * </pre>
	 * 
	 * {attr}表示额外iframe信息，可以通过{@link PortletDataSource#getParamValueString(String, String)}从名为“iframeAttributes”的额外参数中获取。<br/>
	 * 默认为：<br/>
	 * “marginheight="0" marginwidth="0" allowTransparency="true" border="0" frameborder="0" width="100%" height="100%" scrolling="no"”。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		PortletDataSource dataSource = this.getPortletDataSource();
		try {
			if (dataSource == null) throw new RuntimeException("无法获取小窗口数据源！");
			String targetUrl = jspContext.getPathContext().parseUrl((this.m_url != null && this.m_url.length() > 0 ? this.m_url : dataSource.getTarget()));
			if (targetUrl == null || targetUrl.length() == 0) throw new RuntimeException("中没有提供有效iframe目标地址！");
			String iframeattrs = dataSource.getParamValueString("iframeAttributes", "marginheight=\"0\" marginwidth=\"0\" allowTransparency=\"true\" border=\"0\" frameborder=\"0\" scrolling=\"no\" width=\"100%\" height=\"100%\"");
			sb.append("<iframe ").append(iframeattrs).append(" id=\"iframe_").append(this.m_portlet.getUNID()).append("\"").append(" src=\"").append(targetUrl).append("\" >").append("</iframe>");
		} catch (Exception ex) {
			return new PortletContentExceptionRender(this.m_portal, this.m_portlet, ex).render(jspContext);
		}
		return sb.toString();
	}
}

