/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.Comparator;

import com.tansuosoft.discoverx.model.Portlet;

/**
 * 实现小窗口资源对象按其行列坐标排序的类。
 * 
 * @author coca@tansuosoft.cn
 */

public class PortletComparator implements Comparator<Portlet> {
	/**
	 * 缺省构造器。
	 */
	public PortletComparator() {
	}

	/**
	 * 接收是否横向排序标记的构造器。
	 * 
	 * @param horizontalCompare
	 */
	public PortletComparator(boolean horizontalCompare) {
		this.horizontalCompare = horizontalCompare;
	}

	private boolean horizontalCompare = false;

	/**
	 * 设置是否横向排序标记。
	 * 
	 * <p>
	 * 默认为纵向排序，即按栏目从左到右方式排序。如果为横向，则按从上到下的方式排序。
	 * </p>
	 * 
	 * @param horizontalCompare boolean
	 */
	public void setHorizontalCompare(boolean horizontalCompare) {
		this.horizontalCompare = horizontalCompare;
	}

	/**
	 * 重载compare：按小窗口行列坐标排序。
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Portlet o1, Portlet o2) {
		if (o1 == null && o2 == null) return 0;
		if (o1 != null && o2 == null) return 1;
		if (o1 == null && o2 != null) return -1;
		int diff1 = (horizontalCompare ? (o1.getRow() - o2.getRow()) : (o1.getColumn() - o2.getColumn()));
		int diff2 = (horizontalCompare ? (o1.getColumn() - o2.getColumn()) : (o1.getRow() - o2.getRow()));
		int ret = diff1;
		if (ret == 0) {
			ret = diff2;
			if (ret == 0) { // 行相同比较名称，否则比较unid
				ret = o1.getName().compareTo(o2.getName());
				if (ret == 0) return o1.getUNID().compareTo(o2.getUNID());
			}
		}
		return ret > 0 ? 1 : -1;
	}
}

