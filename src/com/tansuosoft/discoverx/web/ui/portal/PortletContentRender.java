/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import java.util.List;

import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 用于为指定门户({@link Portal})对应的特定小窗口({@link Portlet})输出小窗口实际内容的类需继承的统一抽象类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class PortletContentRender implements HtmlRender {
	/**
	 * 缺省构造器。
	 */
	public PortletContentRender() {
	}

	/**
	 * 接收{@link Portal}和{@link Portlet}的构造器。
	 * 
	 * @param portal {@link Portal}，当前门户。
	 * @param portlet {@link Portlet}，当前门户中的某个要显示其内容的小窗口。
	 */
	public PortletContentRender(Portal portal, Portlet portlet) {
		this.m_portal = portal;
		this.m_portlet = portlet;
	}

	/**
	 * 当前门户。
	 */
	protected Portal m_portal = null;
	/**
	 * 当前小窗口。
	 */
	protected Portlet m_portlet = null;

	/**
	 * 获取当前小窗口绑定的数据源。
	 * 
	 * @return PortletDataSource
	 */
	protected PortletDataSource getPortletDataSource() {
		if (this.m_portlet == null) return null;
		Reference ref = this.m_portlet.getDataSource();
		if (ref == null || StringUtil.isNullOrEmpty(ref)) return null;
		PortletDataSource dataSource = ResourceContext.getInstance().getResource(ref.getUnid(), PortletDataSource.class);
		return dataSource;
	}

	/**
	 * 根据{@link Portal}和{@link Portlet}的获取对应的{@link PortletContentRender}对象实例。
	 * 
	 * @param portal
	 * @param portlet
	 * @return
	 */
	public static PortletContentRender getPortletContentRender(Portal portal, Portlet portlet) {
		PortletContentRender result = null;
		if (portal == null || portlet == null) {
			result = new PortletContentExceptionRender(portal, portlet, new Exception("未提供有效门户资源！"));
			return result;
		}

		Reference ref = portlet.getDataSource();
		if (ref == null || StringUtil.isNullOrEmpty(ref)) return new PortletContentExceptionRender(portal, portlet, new Exception("未配置有效数据源，请联系系统管理员！"));
		PortletDataSource portletDataSource = ResourceContext.getInstance().getResource(ref.getUnid(), PortletDataSource.class);
		if (portletDataSource == null) return new PortletContentExceptionRender(portal, portlet, new Exception("未配置“" + ref.getTitle() + "”对应的有效数据源，请联系系统管理员！"));

		String impl = portletDataSource.getServerContentRender();
		if (impl != null && impl.length() > 0) {
			result = Instance.newInstance(impl, PortletContentRender.class);
			if (result != null) {
				result.m_portal = portal;
				result.m_portlet = portlet;
				return result;
			}
		}
		switch (portletDataSource.getDataSourceType().getIntValue()) {
		case 0: // View
			result = new ViewPortletRender(portal, portlet);
			break;
		case 1: // Ajax
			result = new AjaxPortletRender(portal, portlet);
			break;
		case 2: // Iframe
			result = new IframePortletRender(portal, portlet);
			break;
		case 9: // Custom
			result = new CustomPortletRender(portal, portlet);
			break;
		default:
			result = new PortletContentExceptionRender(portal, portlet, new Exception("未知类型数据源，请联系系统管理员！"));
			break;
		}
		return result;
	}

	/**
	 * 组合并返回{@link Portlet}内容呈现时提供给客户端使用的{@link Portlet}对象对应的json对象的文本。
	 * 
	 * @return
	 */
	protected String getPortletJson() {
		if (this.m_portlet == null) return "";
		StringBuilder sb = new StringBuilder();
		sb.append(this.getJson(m_portlet));
		sb.append(",").append("column:").append(m_portlet.getColumn());
		sb.append(",").append("row:").append(m_portlet.getRow());
		sb.append(",").append("displayCount:").append(m_portlet.getDisplayCount());
		sb.append(",").append("width:").append(m_portlet.getWidth());
		sb.append(",").append("height:").append(m_portlet.getHeight());
		sb.append(",").append("portletScroll:").append(m_portlet.getPortletScroll());
		sb.append(",").append("showTitle:").append(m_portlet.getShowTitle() ? "true" : "false");
		if (m_total != -1) sb.append(",").append("total:").append(m_total);
		return sb.toString();
	}

	private int m_total = -1;

	/**
	 * 设置总条目数以供输出。
	 */
	protected void setTotal(int total) {
		m_total = total;
	}

	/**
	 * 组合并返回{@link Portlet}内容呈现时提供给客户端使用的{@link PortletDataSource}对象对应的json对象的文本。
	 * 
	 * @return
	 */
	protected String getDataSourceJson() {
		PortletDataSource pds = this.getPortletDataSource();
		if (pds == null) return "";
		StringBuilder sb = new StringBuilder();
		sb.append(this.getJson(pds));
		sb.append(",").append("dataSourceType:").append(pds.getDataSourceType());
		sb.append(",").append("contentType:").append(pds.getContentType());
		sb.append(",").append("target:").append("'").append(StringUtil.encode4Json(pds.getTarget())).append("'");
		return sb.toString();
	}

	/**
	 * 组合并返回{@link Portlet}内容呈现时提供给客户端使用的{@link Portlet}、{@link PortletDataSource}等对象的通用json属性。
	 * 
	 * @param r
	 * @return
	 */
	protected String getJson(Resource r) {
		if (r == null) return "";
		StringBuilder sb = new StringBuilder();
		sb.append("UNID:").append("'").append(r.getUNID()).append("'");
		sb.append(",").append("name:").append("'").append(r.getName()).append("'");
		sb.append(",").append("alias:").append("'").append(r.getAlias()).append("'");
		sb.append(",").append("description:").append("'").append(StringUtil.encode4Json(r.getDescription())).append("'");
		List<Parameter> params = r.getParameters();
		sb.append(",").append("parameters:[");
		if (params != null) {
			int idx = 0;
			for (Parameter p : params) {
				sb.append(idx == 0 ? "" : ",");
				sb.append("{");
				sb.append("name:").append("'").append(p.getName()).append("'");
				sb.append(",").append("value:").append("'").append(StringUtil.encode4Json(p.getValue())).append("'");
				sb.append("}");
				idx++;
			}
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * 获取Ajax类型{@link Portlet}第一次发送ajax请求以获取呈现数据的时间。
	 * 
	 * <p>
	 * 单位：毫秒
	 * </p>
	 * 
	 * @return int
	 */
	protected int getAjaxPortletRequestTimeout() {
		int portletSequence = PortalParser.getInstance(m_portal).getPortletDisplaySequence(m_portlet);
		if (portletSequence < 0) portletSequence = m_portlet.getRow() + m_portlet.getColumn();
		return 100 + portletSequence * 100;
	}

	/**
	 * 获取Ajax类型{@link Portlet}定时发送ajax请求以获取呈现数据的时间。
	 * 
	 * <p>
	 * 单位：毫秒，请参考“{@link PortletDataSource#getRefreshInterval()}”中的说明。
	 * </p>
	 * 
	 * @return int
	 */
	protected int getAjaxPortletRequestInterval() {
		int result = 0;
		PortletDataSource pds = this.getPortletDataSource();
		if (pds != null) result = pds.getRefreshInterval() * 1000;
		if (result == 0) {
			int portletSequence = PortalParser.getInstance(m_portal).getPortletDisplaySequence(m_portlet);
			if (portletSequence < 0) portletSequence = m_portlet.getRow() + m_portlet.getColumn();
			result = 10 * 60 * 1000 + portletSequence * 1000;
		}
		return result;
	}
}

