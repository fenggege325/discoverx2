/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.portal;

import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletContentType;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.model.PortletDataSourceType;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 类型为“{@link PortletDataSourceType#Custom}”的小窗口对应的{@link PortletContentRender}实现类。
 * 
 * <p>
 * 用于输出{@link PortletContentProvider}实现类的门户小窗口内容呈现结果。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class CustomPortletRender extends PortletContentRender {

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param portal
	 * @param portlet
	 */
	public CustomPortletRender(Portal portal, Portlet portlet) {
		super(portal, portlet);
	}

	/**
	 * 重载render：返回{@link PortletContentProvider}实现类的门户小窗口内容输出结果。
	 * 
	 * <p>
	 * 格式说明：<br/>
	 * 根据{@link PortletContentProvider#getContentType()}返回值，返回相应{@link PortletContentRender}实现类的输出结果。<br/>
	 * 不同类型{@link PortletContentType}的输出结果说明如下：<br/>
	 * <ul>
	 * <li>{@link PortletContentType#Entry}：返回{@link EntryPortletRender}的输出结果。</li>
	 * <li>{@link PortletContentType#Html}：返回{@link PortletContentProvider#getHtmlResult()}的输出结果。</li>
	 * <li>{@link PortletContentType#Xml}：返回{@link XmlPortletRender}的输出结果。</li>
	 * <li>{@link PortletContentType#Url}：如果门户数据源资源中配置了名为“iframeFlag”的参数且值为“true”则返回{@link IframePortletRender}的输出结果，否则返回{@link AjaxPortletRender}的输出结果。</li>
	 * </ul>
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	public String render(JSPContext jspContext) {
		StringBuilder sb = new StringBuilder();
		PortletContentProvider provider = null;
		try {
			PortletDataSource dataSource = this.getPortletDataSource();
			if (dataSource == null) throw new RuntimeException("无法获取小窗口数据源！");
			String impl = dataSource.getTarget();
			if (impl == null || impl.length() == 0) throw new RuntimeException("小窗口数据源中没有提供有效的小窗口内容提供类信息！");
			provider = Instance.newInstance(impl, PortletContentProvider.class);
			if (provider == null) throw new RuntimeException("无法初始化“" + impl + "”对应的类！");
			provider.init(jspContext);
			switch (provider.getContentType().getIntValue()) {
			case 0: // Entry
				return new EntryPortletRender(this.m_portal, this.m_portlet, provider.getEntriesResult()).render(jspContext);
			case 1: // Html
				return provider.getHtmlResult();
			case 2: // Xml
				return new XmlPortletRender(this.m_portal, this.m_portlet, provider.getXmlResult()).render(jspContext);
			case 4: // Url
				boolean iframeFlag = dataSource.getParamValueBool("iframeFlag", false);
				if (iframeFlag) {
					return new IframePortletRender(this.m_portal, this.m_portlet, provider.getUrlResult()).render(jspContext);
				} else {
					return new AjaxPortletRender(this.m_portal, this.m_portlet, provider.getUrlResult()).render(jspContext);
				}
			}
		} catch (Exception ex) {
			return new PortletContentExceptionRender(this.m_portal, this.m_portlet, ex).render(jspContext);
		}
		return sb.toString();
	}
}

