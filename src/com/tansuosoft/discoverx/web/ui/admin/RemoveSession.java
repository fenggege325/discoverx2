/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Session;

/**
 * 删除用户对应的会话的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>用户unid，必须</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>返回json格式的执行结果消息。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "removeSession", name = "删除用户会话", desc = "删除当前用户可能存在的会话信息。")
public class RemoveSession extends Operation {
	/**
	 * 缺省构造器。
	 */
	public RemoveSession() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		String unid = request.getParameter("unid");
		boolean found = false;
		try {
			if (unid == null || unid.length() == 0) throw new Exception("无法获取目标用户信息！");
			Map<String, HttpSession> map = Session.getAllHttpSession();
			if (map != null && map.values() != null) {
				Object obj = null;
				Session ss = null;
				HttpSession s = null;
				HttpSession[] arr = new HttpSession[map.values().size()];
				map.values().toArray(arr);
				for (int i = 0; i < arr.length; i++) {
					s = arr[i];
					if (s == null) continue;
					obj = s.getAttribute(Session.USERSESSION_PARAM_NAME_IN_HTTPSESSION);
					if (obj == null || !(obj instanceof Session)) continue;
					ss = (Session) obj;
					if (unid.equalsIgnoreCase(ss.getSessionUNID())) {
						Document doc = ss.getLastDocument();
						if (doc != null) DocumentBuffer.getInstance().removeHolder(doc.getUNID(), Session.getUser(ss).getName());
						s.invalidate();

						while (ss != null) {
							ss = Session.removeSession(ss.getSessionUNID());
						}
						found = true;
					}
				}
			}
			if (found) {
				this.setLastMessage("会话被成功清除！");
			} else {
				this.setLastMessage("没有当前用户的会话信息！");
			}
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

