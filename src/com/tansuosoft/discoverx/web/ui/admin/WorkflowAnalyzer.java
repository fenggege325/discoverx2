/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.GenericTriplet;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.ActivityType;
import com.tansuosoft.discoverx.workflow.Automation;
import com.tansuosoft.discoverx.workflow.Participation;
import com.tansuosoft.discoverx.workflow.Transition;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 用于输出流程分析报告的{@link HtmlRender}实现类。
 * 
 * @author coca@tensosoft.com
 */
public class WorkflowAnalyzer implements HtmlRender {
	private Workflow m_workflow = null;

	/**
	 * 接收目标流程资源对象的构造器。
	 * 
	 * @param wf
	 */
	public WorkflowAnalyzer(String wfid) {
		if (wfid == null || wfid.length() == 0) return;
		String unid = wfid;
		if (wfid.startsWith("wf")) unid = ResourceAliasContext.getInstance().getUNIDByAlias(Workflow.class, wfid);
		m_workflow = ResourceContext.getInstance().getResource(unid, Workflow.class);
	}

	private static final String PARAMETER_ERROR_STRING = "无法获取要分析的流程对象！";

	/**
	 * 问题与建议级别0：建议
	 */
	public static final int SEVERITY_SUGGESTION = 0;
	/**
	 * 问题与建议级别1：警告
	 */
	public static final int SEVERITY_WARNING = 1;
	/**
	 * 问题与建议级别2：错误
	 */
	public static final int SEVERITY_ERROR = 2;

	protected List<GenericTriplet<String, String, Integer>> problems = new ArrayList<GenericTriplet<String, String, Integer>>();
	protected Map<String, List<Activity>> mapOut = new HashMap<String, List<Activity>>();
	protected Map<String, List<Activity>> mapIn = new HashMap<String, List<Activity>>();
	protected List<List<Integer>> pathResult = new ArrayList<List<Integer>>();
	protected int pathes[][] = null;

	/**
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (m_workflow == null || jspContext == null) return PARAMETER_ERROR_STRING;
		StringBuilder sb = new StringBuilder();
		sb.append("<h1>流程配置分析报告-").append(m_workflow.getName()).append("</h1>\r\n");
		sb.append("<hr/>\r\n");
		sb.append("<h2>信息、警告、问题与建议</h2>\r\n");
		int problemsInsertLen = sb.length();

		List<EventHandlerInfo> elist = m_workflow.getEventHandlers();
		if (elist != null) {
			String clsName = null;
			try {
				for (EventHandlerInfo e : elist) {
					if (e == null || StringUtil.isBlank(e.getEventHandler())) continue;
					clsName = e.getEventHandler();
					Class.forName(clsName);
				}
			} catch (ClassNotFoundException e1) {
				problems.add(new GenericTriplet<String, String, Integer>("流程事件处理程序中名为“" + clsName + "”的实现类无法找到", "删除之、修改之或确保jar、classes等引用路径中能够找到这个类", SEVERITY_WARNING));
			}
		}

		List<Activity> list = m_workflow.getActivities();
		if (list == null || list.size() == 0) {
			problems.add(new GenericTriplet<String, String, Integer>("流程没有配置任何环节", "配置至少一个流程环节", SEVERITY_ERROR));
			sb.insert(problemsInsertLen, buildProblems());
			return sb.toString();
		}

		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree pt = null;

		int beginNodesCount = 0;
		int endNodesCount = 0;
		List<Transition> tlist = null;
		List<Operation> olist = null;
		List<Automation> alist = null;
		List<Participant> plist = null;

		pathes = new int[list.size()][list.size()];
		for (int i = 0; i < pathes.length; i++) {
			for (int j = 0; j < pathes[i].length; j++) {
				pathes[i][j] = 0;
			}
		}
		int idx = 0;
		for (Activity a : list) {
			if (a.getActivityType() == ActivityType.Begin) {
				beginNodesCount++;
			}
			if (a.getActivityType() == ActivityType.End) {
				endNodesCount++;
			}
			tlist = a.getTransitions();
			int validTCount = 0;
			if (tlist != null) {
				for (Transition t : tlist) {
					if (t == null) continue;
					int tidx = getActivityIdx(list, t.getUNID());
					if (tidx >= 0) {
						pathes[idx][tidx] += 1;
						if (!StringUtil.isBlank(t.getCondition())) pathes[idx][tidx] += 2;
						validTCount++;
					}
				}
			}
			olist = a.getOperations();
			if (olist != null && olist.size() > 0) {
				boolean foundWFDone = false;
				boolean foundWFDispatch = false;
				boolean foundWFForward = false;
				for (Operation o : olist) {
					if (o == null) continue;
					String exp = o.getExpression();
					if (exp == null || exp.length() == 0) continue;
					if (!foundWFDone && exp.contains("wfdone")) foundWFDone = true;
					if (!foundWFDispatch && exp.contains("wfdispatch")) foundWFDispatch = true;
					if (!foundWFForward && exp.contains("wfforward")) foundWFForward = true;
				}
				if (list.size() > 1 && !foundWFDone) problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”的流程操作中似乎找不到审批/处理完毕操作（“@wfdone()”）", a.getName()), "建议配置审批/处理完毕操作以便流程能够继续流转或结束", SEVERITY_SUGGESTION));
				if (foundWFDispatch && foundWFForward) problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”的流程操作中同时配置了转办（“@wfforward”）和交办（“@wfdispath”），可能导致流程运转过程混乱", a.getName()), "建议只配置转办或交办中的某一个", SEVERITY_SUGGESTION));
			} else if (list.size() > 1) {
				problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”环节没有配置任何操作", a.getName()), "通常有多个环节的流程的每个环节都至少具有一个办理/审批完毕/发送操作以继续或结束流程，同时可能存在一些其它辅助操作，比如填写审批意见等。", SEVERITY_WARNING));
			}
			alist = a.getAutomations();
			if (alist != null) {
				boolean autoAndTFlag = false;
				for (Automation au : alist) {
					if (au == null) continue;
					String aunid = au.getActivity();
					int auaidx = (aunid != null && aunid.length() == 32 ? getActivityIdx(list, aunid) : list.size() + 1);
					int validPCount = 0;
					if (auaidx < 0) { // 无效自动流转
						problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”环节配置了自动流转，但是无法找到目标环节", a.getName()), "重新选择目标环节", SEVERITY_ERROR));
					} else if (auaidx < list.size()) { // 有效自动流转 begin
						if (validTCount > 0 && !autoAndTFlag) {
							autoAndTFlag = true;
							problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”环节既配置了自动流转又配置了环节出口", a.getName()), "请检查自动流转和所有环节出口的启用条件表达式以确保它们都能够被遍历且不会互相干扰", SEVERITY_SUGGESTION));
						}
						plist = au.getParticipants();
						if (plist != null && plist.size() > 0) {
							for (Participant p : plist) {
								if (p == null) continue;
								validPCount++;
								pt = ptp.getParticipantTree(p.getSecurityCode());
								if (pt == null && !(p.getSecurityCode() >= 10 && p.getSecurityCode() <= 19)) {
									problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”开启了自动流转，但是%2$s", a.getName(), "找不到“" + p.getName() + "(安全编码:" + p.getSecurityCode() + ")”对应的自动流转参与者账号。"), "删除之或配置为有效的自动流转参与者", SEVERITY_WARNING));
								}
							}
							if (au.getParticipation() == Participation.Single && validPCount > 1) {
								problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”开启了自动流转，但是配置了多个自动流转参与者", a.getName()), "建议删除多余的自动流转参与者", SEVERITY_SUGGESTION));
							}
							if (au.getParticipation() != Participation.Single && validPCount == 1) {
								problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”开启了自动流转且为多人审批方式，但是只配置了一个自动流转参与者", a.getName()), "建议添加多个自动流转参与者或变更审批方式", SEVERITY_SUGGESTION));
							}
						}
						if (validPCount == 0) {
							problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”开启了自动流转，但是没有配置任何自动流转参与者", a.getName()), "建议根据自动流转审批方式配置相应自动流转参与者", SEVERITY_WARNING));
						}
						pathes[idx][auaidx] += 4;
						if (!StringUtil.isBlank(au.getCondition())) pathes[idx][auaidx] += 8;
					} // 有效自动流转 end
				}
			}
			plist = a.getParticipants();
			if (plist != null && plist.size() > 0) {
				int validPCount = 0;
				for (Participant p : plist) {
					if (p == null) continue;
					validPCount++;
					pt = ptp.getParticipantTree(p.getSecurityCode());
					if (pt == null && !(p.getSecurityCode() >= 10 && p.getSecurityCode() <= 19)) {
						problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”环节限定了参与者范围，但是%2$s", a.getName(), "找不到“" + p.getName() + "(安全编码:" + p.getSecurityCode() + ")”对应的参与者账号。"), "删除之或配置为有效的参与者", SEVERITY_WARNING));
					}
				}
				if (a.getActivityType() == ActivityType.Begin && validPCount > 0) {
					problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”为开始环节，但是配置了参与者范围", a.getName()), "建议删除之", SEVERITY_SUGGESTION));
				}
			}
			if (a.getCommutatorSupport()) {
				Participant p = a.getCommutator();
				pt = (p == null ? null : ptp.getParticipantTree(p.getSecurityCode()));
				if (pt == null && !(p != null && p.getSecurityCode() >= 10 && p.getSecurityCode() <= 19)) problems.add(new GenericTriplet<String, String, Integer>(String.format("“%1$s”的启用了环节转接，但是%2$s", a.getName(), p == null ? "没有配置转接人参与者" : "找不到“" + p.getName() + "(安全编码:" + p.getSecurityCode() + ")”对应的转接人参与者账号。"), "配置有效的转接人参与者", SEVERITY_ERROR));
			}
			idx++;
		}

		if (beginNodesCount == 0) problems.add(new GenericTriplet<String, String, Integer>("流程没有开始环节", "需配置一个开始环节", SEVERITY_ERROR));
		if (beginNodesCount > 1) problems.add(new GenericTriplet<String, String, Integer>("流程有多个开始环节", "只能配置一个开始环节", SEVERITY_ERROR));
		if (list != null && list.size() > 1 && endNodesCount == 0) problems.add(new GenericTriplet<String, String, Integer>("流程没有结束环节", "建议配置一个结束环节", SEVERITY_ERROR));

		// 流程走向路径分析并分解为具体几条走向，同时提示孤立节点、死循环、走不下去等情况。
		if (list.size() > 1) calcPath(list);
		// 检测重复路径
		for (int i = 0; i < pathResult.size(); i++) {
			List<Integer> l1 = pathResult.get(i);
			boolean foundDupPath = false;
			for (int j = 0; j < pathResult.size(); j++) {
				if (i == j) continue;
				List<Integer> l2 = pathResult.get(j);
				if (l1.size() != l2.size()) continue;
				int k = 0;
				foundDupPath = true;
				for (int aidx : l1) {
					if (aidx != l2.get(k)) {
						foundDupPath = false;
						break;
					}
					k++;
				}
				if (foundDupPath) break;
			}
			if (foundDupPath) {
				problems.add(new GenericTriplet<String, String, Integer>("流程中存在重复的流转路径", "可能是自动流转和出口都指向了同一个目标环节", SEVERITY_SUGGESTION));
				break;
			}
		}
		sb.insert(problemsInsertLen, buildProblems());
		sb.append("<h2>流转路径</h2>\r\n");
		sb.append(buildPathes(list));

		return sb.toString();
	}

	/**
	 * 输出所有可能的流转路径结果。
	 * 
	 * @param list
	 * @return String
	 */
	protected String buildPathes(List<Activity> list) {
		if (pathResult.size() == 0) return "<p>无有效流转路径！</p>";
		StringBuilder sb = new StringBuilder();
		sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" id=\"problems\">\r\n");
		sb.append("<colgroup><col width=\"30\"/><col/><col width=\"30\"/><col width=\"120\"/></colgroup>\r\n");
		sb.append("<thead>");
		sb.append("<tr>");

		sb.append("<td>");
		sb.append("序号");
		sb.append("</td>\r\n");

		sb.append("<td>");
		sb.append("路径");
		sb.append("</td>\r\n");

		sb.append("<td>");
		sb.append("步骤");
		sb.append("</td>\r\n");

		sb.append("<td>");
		sb.append("备注");
		sb.append("</td>\r\n");

		sb.append("</tr>\r\n");
		sb.append("</thead>\r\n");
		sb.append("<tbody>");
		Activity a = null;
		int pidx = 0;

		for (List<Integer> l : pathResult) {
			sb.append("\r\n<tr class=\"").append(pidx % 2 == 0 ? "even" : "odd").append("\">");
			sb.append("\r\n<td>").append(pidx + 1).append("</td>");
			sb.append("\r\n<td style=\"text-align:left\">");
			int idx = 0;

			for (int i : l) {
				a = list.get(i);
				int px = 0;
				List<Participant> plist = a.getParticipants();
				if (plist != null && plist.size() > 0) {
					px = 2;
					boolean allSelected = true;
					boolean selected = true;
					for (Participant p : plist) {
						if (p == null) continue;
						selected = p.getSelected();
						if (selected) px = 3;
						if (!selected) {
							allSelected = false;
							break;
						}
					}
					if (allSelected) px = 1;
				}

				if (idx > 0) {
					boolean withCondition = false;
					sb.append("<span class=\"arrow\">");
					int pv = pathes[l.get(idx - 1)][i];
					int pp = a.getParticipation();
					if ((pv & 4) == 4) { // 自动流转标记
						pv -= 4;
						px = 1;
						sb.append("^");
						Activity lasta = list.get(l.get(idx - 1));
						List<Automation> alist = lasta.getAutomations();
						for (Automation au : alist) {
							if (a.getUNID().equalsIgnoreCase(au.getActivity())) {
								pp = au.getParticipation().getIntValue();
							}
						}
						pathes[l.get(idx - 1)][i] -= 4;
						withCondition = ((pv & 8) == 8);
						if (withCondition) pathes[l.get(idx - 1)][i] -= 8;
					} else if ((pv & 1) == 1) {
						pathes[l.get(idx - 1)][i] -= 1;
						withCondition = ((pv & 2) == 2);
						if (withCondition) pathes[l.get(idx - 1)][i] -= 2;
					}
					sb.append(getArrow(pp));
					sb.append(withCondition ? "?" : ""); // 条件流转标记
					sb.append("</span>");
				}
				sb.append("<span class=\"activity\">");
				sb.append(a.getName());
				sb.append("</span>");
				sb.append("<span class=\"info\">").append(getparticipantFlag(px)).append("</span>");
				idx++;
			}
			boolean normalEnd = (list.get(l.get(l.size() - 1)).getActivityType() == ActivityType.End);
			boolean hasLoop = false;
			int last = l.get(l.size() - 1);
			for (int i = 0; i < l.size() - 1; i++) {
				if (l.get(i) == last) {
					hasLoop = true;
					break;
				}
			}
			sb.append("</td>");
			sb.append("\r\n<td>").append(l.size() - 1).append("</td>");
			sb.append("\r\n<td style=\"text-align:left\">");
			if (normalEnd) {
				sb.append("线性流转路径");
			} else {
				if (hasLoop) {
					sb.append("循环流转路径");
				} else {
					sb.append("尾节点非结束节点");
				}
			}
			sb.append("</td>");
			sb.append("\r\n</tr>");
			pidx++;
		}
		sb.append("\r\n</tbody>");
		sb.append("\r\n</table>");
		return sb.toString();
	}

	protected static final String arrows[] = { "-&gt;", "=&gt;&gt;", "->&gt;&gt;", "~&gt;&gt;", "-:&gt;" };

	/**
	 * 获取p指定的审批方式对应的目标审批人。
	 * 
	 * @param p
	 * @return String
	 */
	protected static String getArrow(int p) {
		if (p == 0) return arrows[4];
		int v = 0;
		int r = p >> 1;
		while (r != 0) {
			v++;
			r >>= 1;
		}
		return arrows[v];
	}

	protected static final String participantFlag[] = { "*", "1", "x", "?" };

	/**
	 * 获取p指定的参与折范围对应的标记。
	 * 
	 * @param p
	 * @return String
	 */
	protected static String getparticipantFlag(int p) {
		return participantFlag[p];
	}

	/**
	 * 分析计算流程路径。
	 * 
	 * @param list 表示原始流程配置的环节列表。
	 */
	protected void calcPath(List<Activity> list) {
		Activity ta = null;
		String aid = null;
		List<Transition> tlist = null;
		List<Automation> alist = null;

		for (Activity a : list) {
			aid = a.getUNID();
			tlist = a.getTransitions();
			if (tlist != null && tlist.size() > 0) {
				for (Transition t : tlist) {
					if (t == null) continue;
					int tidx = getActivityIdx(list, t.getUNID());
					if (tidx >= 0) {
						ta = list.get(tidx);
						if (ta != null) {
							List<Activity> l = mapOut.get(aid);
							if (l == null) {
								l = new ArrayList<Activity>();
								mapOut.put(aid, l);
							}
							l.add(ta);
							//
							l = mapIn.get(ta.getUNID());
							if (l == null) {
								l = new ArrayList<Activity>();
								mapIn.put(ta.getUNID(), l);
							}
							l.add(a);
						}
					}
				}// for end
			}// if end
			alist = a.getAutomations();
			if (alist == null || alist.size() == 0) continue;
			for (Automation au : alist) {
				if (au == null) continue;
				String aunid = au.getActivity();
				int auaidx = (aunid != null && aunid.length() == 32 ? getActivityIdx(list, aunid) : list.size() + 1);
				if (auaidx >= 0 && auaidx < list.size()) {
					ta = list.get(auaidx);
					if (ta != null) {
						List<Activity> l = mapOut.get(aid);
						if (l == null) {
							l = new ArrayList<Activity>();
							mapOut.put(aid, l);
						}
						l.add(ta);
						//
						l = mapIn.get(ta.getUNID());
						if (l == null) {
							l = new ArrayList<Activity>();
							mapIn.put(ta.getUNID(), l);
						}
						l.add(a);
					}
				}
			}// for end
		}// for end
		List<Activity> outlist = null;
		List<Activity> inlist = null;
		List<Activity> firsts = new ArrayList<Activity>();
		for (Activity a : list) {
			if (a.getActivityType() == ActivityType.Begin) firsts.add(a);
			aid = a.getUNID();
			outlist = mapOut.get(aid);
			inlist = mapIn.get(aid);
			if (outlist == null && inlist == null) {
				problems.add(new GenericTriplet<String, String, Integer>("“" + a.getName() + "”环节为孤立环节", "设置可以转到此环节或从此环节转到别的环节", SEVERITY_WARNING));
			}
			if (outlist == null && inlist != null && a.getActivityType() != ActivityType.End) {
				problems.add(new GenericTriplet<String, String, Integer>("“" + a.getName() + "”环节只有入口没有出口", "设置为可以从此环节转到别的环节或将此环节设置为结束环节", SEVERITY_WARNING));
			}
			if (inlist == null && outlist != null && a.getActivityType() != ActivityType.Begin) {
				problems.add(new GenericTriplet<String, String, Integer>("“" + a.getName() + "”环节只有出口没有入口", "是否可以将此环节设置为开始环节", SEVERITY_WARNING));
			}
		}

		for (Activity f : firsts) {
			List<Integer> r = new ArrayList<Integer>();
			r.add(getActivityIdx(list, f.getUNID()));
			getPath(f, list, r);
		}
	}// func end

	/**
	 * 获取路径
	 * 
	 * @param from 开始环节
	 * @param list 表示原始流程配置的环节列表。
	 * @param result 路径结果
	 */
	protected void getPath(Activity from, List<Activity> list, List<Integer> result) {
		if (from == null || list == null) return;
		List<Activity> acts = mapOut.get(from.getUNID());
		if (acts == null || acts.size() == 0) {
			pathResult.add(result);
			return;
		}
		int listIdx = -1;
		Activity x = null;

		for (int i = 0; i < acts.size(); i++) {
			x = acts.get(i);
			listIdx = getActivityIdx(list, x.getUNID());
			if (listIdx < 0) continue;
			List<Integer> l = new ArrayList<Integer>(result);
			boolean recursiveFlag = true;
			for (int j : l) {
				if (listIdx == j) {
					recursiveFlag = false;
					break;
				}
			}
			l.add(listIdx);
			if (recursiveFlag) {
				getPath(x, list, l);
			} else {
				pathResult.add(l);
			}
		}
	}

	/**
	 * 构造问题列表表格结果.
	 * 
	 * @return String
	 */
	protected String buildProblems() {
		if (problems == null || problems.size() == 0) { return "<p>流程配置没有问题和改进建议！</p>\r\n"; }
		StringBuilder sb = new StringBuilder();
		sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" id=\"problems\">\r\n");
		sb.append("<colgroup><col width=\"30\"/><col width=\"60\"/><col width=\"45%\"/><col/></colgroup>\r\n");
		sb.append("<thead>");
		sb.append("<tr>");

		sb.append("<td>");
		sb.append("序号");
		sb.append("</td>\r\n");

		sb.append("<td>");
		sb.append("级别");
		sb.append("</td>\r\n");

		sb.append("<td>");
		sb.append("说明");
		sb.append("</td>\r\n");

		sb.append("<td>");
		sb.append("建议修改措施");
		sb.append("</td>\r\n");

		sb.append("</tr>\r\n");
		sb.append("</thead>\r\n");
		sb.append("<tbody>\r\n");
		int pidx = 0;
		for (GenericTriplet<String, String, Integer> x : problems) {
			sb.append("<tr class=\"").append(pidx % 2 == 0 ? "even" : "odd").append("\">");

			sb.append("<td>").append(pidx + 1).append("</td>\r\n");
			sb.append("<td").append(" class=\"severity").append(x.getThird()).append("\"").append(">");
			sb.append(x.getThird() == SEVERITY_SUGGESTION ? "信息" : (x.getThird() == SEVERITY_WARNING ? "警告" : "错误"));
			sb.append("</td>\r\n");

			sb.append("<td style=\"text-align:left\">");
			sb.append(x.getFirst());
			sb.append("</td>\r\n");

			sb.append("<td style=\"text-align:left\">");
			sb.append(x.getSecond());
			sb.append("</td>\r\n");

			sb.append("</tr>\r\n");
			pidx++;
		}
		sb.append("</tbody>\r\n");
		sb.append("</table>\r\n");
		return sb.toString();
	}

	/**
	 * 获取流程环节id指定的环节在环节列表中的序号。
	 * 
	 * @param list
	 * @param activityId
	 * @return int
	 */
	protected int getActivityIdx(List<Activity> list, String activityId) {
		if (list == null || activityId == null) return -1;
		int idx = 0;
		for (Activity a : list) {
			if (a.getUNID().equalsIgnoreCase(activityId)) return idx;
			idx++;
		}
		return -1;
	}
}

