/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Organization;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.selector.ParticipantTreeViewProvider;

/**
 * 输出供管理通道使用的授权TreeView对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AuthTreeViewRender implements HtmlRender {
	private boolean m_firstFlag = true;
	private String m_nonPersonTviLeafIcon = null;
	private String m_loadingTviLeafIcon = null;
	private String m_groupTviLeafIcon = null;
	private int m_waitForFetchDataIndex = 0;
	protected static final String AsyncLabel = ParticipantTreeViewProvider.TV_ASYNC_LABEL; // "请稍候，正在获取包含的下级分支...";
	private int m_securityCode = 0;
	private Map<Integer, Boolean> m_staticGroups = null;

	/**
	 * 缺省构造器。
	 */
	public AuthTreeViewRender() {
	}

	/**
	 * 接收具体群组或角色安全编码的构造器。
	 * 
	 * @param securityCode int
	 */
	public AuthTreeViewRender(int securityCode) {
		m_securityCode = securityCode;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		PathContext pathContext = new PathContext((HttpServletRequest) jspContext.getRequest());
		m_nonPersonTviLeafIcon = pathContext.getThemeImagesPath() + "tvfc.gif";
		m_loadingTviLeafIcon = pathContext.getThemeImagesPath() + "loading.gif";
		m_groupTviLeafIcon = pathContext.getCommonIconsPath() + "icon_usertwo.gif";
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree root = null;
		if (m_securityCode == 0) {
			root = ptp.getRoot();
		} else {
			root = ptp.getParticipantTree(m_securityCode);
		}
		StringBuilder sb = new StringBuilder();
		if (root == null) {
			sb.append("id:'tverror',error:true,label:'").append("无法获取").append(m_securityCode == 0 ? "" : "指定").append("参与者根节点信息！").append("'");
			return sb.toString();
		}
		Organization orgroot = ResourceContext.getInstance().getResource(OrganizationsContext.getInstance().getContextUserOrgUnid(), Organization.class);
		if (!SecurityHelper.authorize(jspContext.getLoginUser(), orgroot, SecurityLevel.View)) {
			sb.append("id:'tverror',error:true,label:'对不起，您没有查看分组与授权信息的权限！'");
			return sb.toString();
		}
		if (m_securityCode > 0) {
			List<Participant> persons = ptp.getParticipants(root, jspContext.getUserSession());
			List<ParticipantTree> oldpts = null;
			if (persons != null && persons.size() > 0) {
				List<ParticipantTree> pts = new ArrayList<ParticipantTree>(persons.size());
				for (Participant p : persons) {
					if (p != null) pts.add(new ParticipantTree(p));
				}
				oldpts = root.getChildren();
				root.setChildren(pts);
			}
			this.renderTreeView(root, sb);
			root.setChildren(oldpts);
			return sb.toString();
		}
		sb.append("id:").append("'").append(root.getUNID()).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(root.getName())).append("'");
		sb.append(",").append("data:{"); // data begin
		sb.append("participantType:").append(root.getParticipantType().getIntValue());
		sb.append(",").append("alias:'").append(root.getAlias()).append("'");
		sb.append(",").append("securityCode:").append(root.getSecurityCode());
		sb.append("}");

		sb.append(",").append("children:").append("["); // children begin

		// 群组
		ParticipantTree pt = ptp.getParticipantTree(ParticipantTree.GROUP_ROOT_CODE);
		if (pt != null && pt.getChildren() != null) m_staticGroups = new HashMap<Integer, Boolean>(pt.getChildren().size());
		else m_staticGroups = new HashMap<Integer, Boolean>();
		m_firstFlag = true;
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper r = new SQLWrapper();
				r.setSql("select c_securitycode from t_group where c_grouptype=1");
				return r;
			}
		};
		dbr.setResultBuilder(new ResultBuilder() {
			@Override
			public Object build(DBRequest request, Object rawResult) {
				if (rawResult == null || !(rawResult instanceof DataReader)) return null;
				DataReader dr = (DataReader) rawResult;
				try {
					int sc = 0;
					while (dr.next()) {
						sc = dr.getInt(1, -1);
						if (sc > 0) m_staticGroups.put(sc, true);
					}
				} catch (SQLException e) {
					FileLogger.error(e);
				}
				return null;
			}
		});
		dbr.sendRequest();
		this.renderTreeView(pt, sb);

		// 角色
		pt = ptp.getParticipantTree(ParticipantTree.ROLE_ROOT_CODE);
		m_firstFlag = false;
		this.renderTreeView(pt, sb);

		sb.append("]"); // children end

		return sb.toString();
	}

	private long tid = Thread.currentThread().getId();

	/**
	 * 输出TreeView
	 * 
	 * @param pt
	 * @param sb
	 */
	private void renderTreeView(ParticipantTree pt, StringBuilder sb) {
		if (pt == null) return;
		int sc = pt.getSecurityCode();
		if (pt.getParticipantType() == ParticipantType.Role && (sc == Role.ROLE_DEFAULT_SC || sc == Role.ROLE_ANONYMOUS_SC)) return;
		if (pt.getParticipantType() == ParticipantType.Group && sc != ParticipantTree.GROUP_ROOT_CODE && m_staticGroups != null && !m_staticGroups.containsKey(pt.getSecurityCode())) return;

		sb.append(this.m_firstFlag ? "" : ",");
		sb.append("{"); // treeview begin

		sb.append("id:'pt_").append(tid).append(pt.getParticipantType()).append(Math.abs(pt.getSecurityCode())).append(uniqueNum.get()).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(ParticipantHelper.getFormatValue(pt, "cn"))).append("'");
		sb.append(",").append("data:{"); // data begin
		sb.append("participantType:").append(pt.getParticipantType().getIntValue());
		sb.append(",").append("unid:'").append(pt.getUNID()).append("'");
		sb.append(",").append("alias:'").append(pt.getAlias()).append("'");
		sb.append(",").append("securityCode:").append(sc);
		sb.append("}"); // data end

		sb.append(",").append("children:").append("["); // children begin
		boolean ajaxChildren = false;
		if (m_securityCode == 0 && sc != ParticipantTree.ROOT_CODE && sc != ParticipantTree.ROLE_ROOT_CODE && sc != ParticipantTree.GROUP_ROOT_CODE) {
			ajaxChildren = true;
			sb.append("{");
			sb.append("id:").append("'waitforfetchdata_").append(m_waitForFetchDataIndex++).append("'");
			sb.append(",").append("label:").append("'").append(AsyncLabel).append("'");
			sb.append(",").append("desc:").append("''");
			sb.append(",").append("data:{"); // data begin
			sb.append("participantType:-1");
			sb.append(",").append("securityCode:-100");
			sb.append(",").append("ajaxPlaceholder:true");
			sb.append("}"); // data end
			sb.append(",").append("leafIcon:'").append(m_loadingTviLeafIcon).append("'");
			sb.append("}");
		} else {
			List<ParticipantTree> list = pt.getChildren();
			if (list != null) {
				m_firstFlag = true;
				for (ParticipantTree x : list) {
					if (x == null) continue;
					this.renderTreeView(x, sb);
					m_firstFlag = false;
				}
			}
		}
		sb.append("]"); // children end
		if (ajaxChildren) sb.append(",").append("ajaxChildren:true");
		if (m_securityCode == 0 && pt.getParticipantType() != ParticipantType.Person) {
			sb.append(",").append("leafIcon:'").append(m_nonPersonTviLeafIcon).append("'");
		} else if (m_securityCode > 0 && pt.getSecurityCode() > 0 && pt.getParticipantType() == ParticipantType.Group) {
			sb.append(",").append("leafIcon:'").append(m_groupTviLeafIcon).append("'");
		}

		sb.append("}"); // treeview end

		return;
	}

	private static final ThreadLocal<Integer> uniqueNum = new ThreadLocal<Integer>() {
		private int seq = 0;

		@Override
		protected Integer initialValue() {
			seq = 0;
			return 0;
		}

		/**
		 * 重载：
		 * 
		 * @see java.lang.ThreadLocal#get()
		 */
		@Override
		public Integer get() {
			return seq++;
		}

		/**
		 * 重载：
		 * 
		 * @see java.lang.ThreadLocal#set(java.lang.Object)
		 */
		@Override
		public void set(Integer value) {
			seq = value;
		}

	};
}

