/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.common.license.Version;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 导入资源的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>fn</td>
 * <td>要导入的资源的文件名参数，参数值必须以“{资源unid}.xml”作为文件名。<br/>
 * 需先上传成功相应的xml文件后才能正常执行。</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>要导入的资源的目录名参数，参数值必须是系统有效的资源目录名，如果不提供，则系统尝试自动获取目录名。
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ImportResource extends Operation {
	/**
	 * 缺省构造器。
	 */
	public ImportResource() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();

		Resource resource = null;
		String fn = null;
		String directory = null;
		String unid = null;
		boolean resourceExists = false;
		try {
			fn = request.getParameter("fn");
			if (fn == null || fn.length() == 0) throw new Exception("无法获取要导入资源的文件名。");
			unid = fn.substring(0, 32);
			if (unid == null || unid.length() != 32) throw new Exception("无法获取要导入资源的UNID。");
			String tempFolder = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath();
			String target = String.format("%1$s%2$s", tempFolder, fn);
			File f = new File(target);
			if (!f.exists() || !f.isFile()) throw new Exception("资源对应的xml文件不存在。");

			directory = request.getParameter("directory");
			if (directory == null || directory.length() == 0) {
				XmlDeserializer des = new XmlDeserializer();
				Object obj = des.produce(f);
				if (obj == null || !(obj instanceof Resource)) throw new Exception("提供的文件不是合法资源！");
				Resource r = (Resource) obj;
				directory = r.getDirectory();
			}
			resource = ResourceContext.getInstance().getResource(unid, directory);
			resourceExists = (resource != null);

			String destFolder = CommonConfig.getInstance().getResourcePath() + directory + File.separator;
			File destFolderFile = new File(destFolder);
			destFolderFile.mkdirs();
			String destFn = destFolder + fn;
			File destFnFile = new File(destFn);
			if (!f.renameTo(destFnFile)) {
				InputStream in = new FileInputStream(f);
				OutputStream out = new FileOutputStream(destFnFile);
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
			}
			if (!f.delete()) f.deleteOnExit();
			Version.getInstance().appendPatch(unid, DateTime.getNowDTString(), null);
			if (resourceExists) ResourceContext.getInstance().removeObjectFromCache(unid);
			this.setLastMessage("资源" + (resourceExists ? "更新" : "导入") + "成功！");
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
	}
}

