/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出供管理通道中通过角色安全编码查询角色名称信息对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class RoleInfoRender implements HtmlRender {

	/**
	 * 缺省构造器。
	 */
	public RoleInfoRender() {

	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		ParticipantTree roleRoot = ParticipantTreeProvider.getInstance().getParticipantTree(ParticipantTree.ROLE_ROOT_CODE);
		List<ParticipantTree> list = roleRoot.getChildren();
		if (list == null || list.isEmpty()) return "";
		StringBuilder sb = new StringBuilder();
		for (ParticipantTree x : list) {
			sb.append(sb.length() == 0 ? "" : ",");
			sb.append(x.getSecurityCode()).append(":'").append(StringUtil.encode4Json(x.getName())).append("'");
		}
		return sb.toString();
	}
}

