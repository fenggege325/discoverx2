/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.InitDB;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 注册操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>o</td>
 * <td>用户单位名称，必须，如果不特别注明，参数值可以通过http请求或配置获取，下同。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>返回ajax返回结果，其中如果有激活请求串，则通过message属性返回其内容。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Register extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Register() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		try {
			User u = this.getUser();
			if (u == null || u.isAnonymous() || !u.isSystemBuiltinAdmin()) throw new Exception("请您使用内置超级管理员登录系统后再执行此操作！");
			String o = this.getParameterStringFromAll("o", null);
			if (o == null || o.length() == 0) throw new Exception("没有提供有效单位名称！");
			CommonConfig cc = CommonConfig.getInstance();
			if (!o.equalsIgnoreCase(cc.getBelong())) {
				cc.setBelong(o);
				if (!cc.save()) throw new Exception("无法保存单位名称！");
				cc.reload();
			}
			try {
				InitDB dbr = new InitDB();
				dbr.sendRequest();
				if (dbr.getResultLong() > 0) ParticipantTreeProvider.reload();
			} catch (Exception ex) {
				FileLogger.error(ex);
			}
			response.setMessage("系统注册成功！");
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		this.setLastAJAXResponse(response);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}

}
