/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.model.Dictionary;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.model.ViewOrder;
import com.tansuosoft.discoverx.model.ViewQueryConfig;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 导出视图设计信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class ViewDesignExporter extends DesignExporter {

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecial(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecial(Resource r) {
		if (r == null || !(r instanceof View)) return "";
		View view = (View) r;

		StringBuilder sb = new StringBuilder();

		ViewType vt = view.getViewType();
		String vtt = "";
		if (vt == ViewType.DocumentView) {
			vtt = "文档视图";
		} else if (vt == ViewType.DBResourceView) {
			vtt = "数据库资源视图";
		} else if (vt == ViewType.XMLResourceView) {
			vtt = "xml资源视图";
		} else {
			vtt = "其它类型视图";
		}
		sb.append("<tr><td>视图类型</td><td>").append(vtt).append("</td></tr>").append("\r\n");

		String tmps = view.getTable();
		Resource tmpr = ResourceContext.getInstance().getResource(ResourceAliasContext.getInstance().getUNIDByAlias(Form.class, tmps), Form.class);
		sb.append("<tr><td>主数据来源表</td><td>").append(tmps == null ? "" : tmps).append(tmpr == null ? "" : "(对应表单:" + tmpr.getName() + ")").append("</td></tr>").append("\r\n");
		sb.append("<tr><td>固定查询条件</td><td>").append(StringUtil.getValueString(view.getCommonCondition(), "")).append("</td></tr>").append("\r\n");

		sb.append("<tr><td>分页视图每页显示记录数</td><td>").append(view.getDisplayCountPerPage()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>是否显示视图数据条目选择器</td><td>").append(view.getShowSelector() ? "是" : "否").append("</td></tr>").append("\r\n");
		sb.append("<tr><td>是否显示视图数据条目序号</td><td>").append(view.getNumberIndicator() ? "是" : "否").append("</td></tr>").append("\r\n");

		sb.append("<tr><td>是否输出sql查询语句以供调试</td><td>").append(view.getDebugable() ? "是" : "否").append("</td></tr>").append("\r\n");
		sb.append("<tr><td>是否可作为小窗口数据源</td><td>").append(view.getPortalDatasource() ? "是" : "否").append("</td></tr>").append("\r\n");
		sb.append("<tr><td>是否可作为模块大纲</td><td>").append(view.getApplicationOutline() ? "是" : "否").append("</td></tr>").append("\r\n");

		sb.append("<tr><td>默认尺寸度量单位</td><td>").append(view.getMeasurement() == SizeMeasurement.Percentage ? "百分比" : "像素").append("</td></tr>").append("\r\n");
		sb.append("<tr><td>是否缓存视图查询结果</td><td>").append(view.getCache() ? "是" : "否").append("</td></tr>").append("\r\n");
		sb.append("<tr><td>缓存过期时间</td><td>").append(view.getCacheExpires()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>缓存页数</td><td>").append(view.getCachePages()).append("</td></tr>").append("\r\n");

		sb.append("<tr><td>自定义呈现信息</td><td>").append(StringUtil.getValueString(view.getDisplay(), "")).append("</td></tr>").append("\r\n");

		sb.append("<tr><td>手工输入查询条件</td><td>").append(StringUtil.getValueString(view.getRawQuery(), "")).append("</td></tr>").append("\r\n");

		sb.append("<tr><td>视图解析自定义实现类</td><td>").append(StringUtil.getValueString(view.getViewParserImplement(), "")).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>视图查询自定义实现类</td><td>").append(StringUtil.getValueString(view.getViewQueryImplement(), "")).append("</td></tr>").append("\r\n");

		return sb.toString();
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecialCollection(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecialCollection(Resource r) {
		if (r == null || !(r instanceof View)) return "";
		View view = (View) r;
		StringBuilder sb = new StringBuilder();

		// 视图列
		List<ViewColumn> vcs = view.getColumns();
		if (vcs != null && vcs.size() > 0) {
			sb.append(this.exportH2("视图列信息", r.getAlias() + "_columns"));
			sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
			sb.append("<colgroup><col width=\"16%\"/><col width=\"30%\"/><col width=\"30%\"/><col width=\"6%\"/><col width=\"6%\"/><col width=\"6%\"/><col/></colgroup>").append("\r\n");
			sb.append("<tr class=\"head\"><td>标题</td><td>表达式</td><td>字典</td><td>宽度</td><td>检索列</td><td>分类列</td><td>链接</td></tr>").append("\r\n");
			for (ViewColumn x : vcs) {
				if (x == null) continue;
				sb.append("<tr>");
				sb.append("<td>").append(x.getTitle()).append("</td>");
				sb.append("<td>").append(this.processViewQueryConfig(x, x.getColumnAlias())).append("</td>");
				String d = x.getDictionary();
				Dictionary dict = ResourceContext.getInstance().getResource((d != null && d.startsWith("dict") ? ResourceAliasContext.getInstance().getUNIDByAlias(Dictionary.class, d) : d), Dictionary.class);
				sb.append("<td>").append(d == null ? "" : d).append(dict != null ? "(" + dict.getName() + ")" : "").append("</td>");
				sb.append("<td>").append(x.getWidth()).append("</td>");
				sb.append("<td>").append(x.getSearchable() ? "是" : "否").append("</td>");
				sb.append("<td>").append(x.getCategorized() ? "是" : "否").append("</td>");
				sb.append("<td>").append(x.getLinkable() ? "是" : "否").append("</td>");
				sb.append("</tr>").append("\r\n");
			}
			sb.append("</table>").append("\r\n");
		}
		// 视图查询条件
		List<ViewCondition> vss = view.getConditions();
		if (vss != null && vss.size() > 0) {
			sb.append(this.exportH2("视图检索条件信息", r.getAlias() + "_conditions"));
			sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
			sb.append("<colgroup><col width=\"10%\"/><col width=\"20%\"/><col width=\"10%\"/><col width=\"30%\"/><col width=\"10%\"/><col width=\"10%\"/><col/></colgroup>").append("\r\n");
			sb.append("<tr class=\"head\"><td>左括号</td><td>表达式</td><td>比较符</td><td>比较右值</td><td>是否按数字处理右值</td><td>与下一个条件合并方式</td><td>右括号</td></tr>").append("\r\n");
			for (ViewCondition x : vss) {
				if (x == null) continue;
				sb.append("<tr>");
				sb.append("<td>").append(StringUtil.getValueString(x.getLeftBracket(), "")).append("</td>");
				sb.append("<td>").append(this.processViewQueryConfig(x, null)).append("</td>");
				sb.append("<td>").append(StringUtil.getValueString(x.getCompare(), "")).append("</td>");
				sb.append("<td>").append(StringUtil.getValueString(this.processExpression(x.getRightValue()), "")).append("</td>");
				sb.append("<td>").append(x.getNumericCompare() ? "是" : "否").append("</td>");
				sb.append("<td>").append(x.getCombineNextWith() != null && x.getCombineNextWith().indexOf("AND") > 0 ? "与" : "或").append("</td>");
				sb.append("<td>").append(StringUtil.getValueString(x.getRightBracket(), "")).append("</td>");
				sb.append("</tr>").append("\r\n");
			}
			sb.append("</table>").append("\r\n");
		}
		// 视图排序
		List<ViewOrder> vos = view.getOrders();
		if (vos != null && vos.size() > 0) {
			sb.append(this.exportH2("视图排序信息", r.getAlias() + "_orders"));
			sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
			sb.append("<colgroup><col width=\"62%\"/><col/></colgroup>").append("\r\n");
			sb.append("<tr class=\"head\"><td>表达式</td><td>排序方式</td></tr>").append("\r\n");
			for (ViewOrder x : vos) {
				if (x == null) continue;
				sb.append("<tr>");
				sb.append("<td>").append(this.processViewQueryConfig(x, null)).append("</td>");
				sb.append("<td>").append(x.getOrderBy() != null && x.getOrderBy().indexOf("DESC") >= 0 ? "降序" : "升序").append("</td>");
				sb.append("</tr>").append("\r\n");
			}
			sb.append("</table>").append("\r\n");
		}

		// 视图操作
		sb.append(this.exportOperations(view.getOperations(), "视图操作", r.getAlias() + "_operations"));

		return sb.toString();
	}

	/**
	 * 处理并返回ViewQueryConfig结果。
	 * 
	 * @param x
	 * @param alias
	 * @return String
	 */
	protected String processViewQueryConfig(ViewQueryConfig x, String alias) {
		if (x == null) return "";
		String exp = x.getExpression();
		String tc = String.format("%1$s.%2$s", StringUtil.getValueString(x.getTableName(), ""), StringUtil.getValueString(x.getColumnName(), ""));
		if (exp == null || exp.length() == 0) return tc + (alias == null || alias.length() == 0 ? "" : " " + alias);
		return exp.replace("{0}", tc) + (alias == null || alias.length() == 0 ? "" : " " + alias);
	}
}

