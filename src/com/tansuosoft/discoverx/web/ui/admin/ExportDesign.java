/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 实现导出目标资源设计摘要功能的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>要导出其设计摘要的资源目录名。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要导出其设计摘要的资源UNID。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>如果成功则重定向到“export_design.jsp”。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ExportDesign extends Operation {
	/**
	 * 成功后的重定向地址。
	 */
	protected static final String RESULT_REDIRECT_URL = "admin/export_design.jsp";

	/**
	 * 缺省构造器。
	 */
	public ExportDesign() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			String directory = this.getParameterStringFromAll("directory", "application");
			String unid = this.getParameterStringFromAll("unid", null);
			if (unid == null || unid.length() == 0) throw new Exception("没有指定目标资源id！");
			Resource r = ResourceContext.getInstance().getResource(unid, directory);
			if (r == null) r = ResourceContext.getInstance().getResource(ResourceAliasContext.getInstance().getUNIDByAlias(directory, unid), directory);
			if (r == null) throw new Exception("无法获取目标资源！");
			if (!SecurityHelper.authorize(this.getUser(), r, SecurityLevel.View)) throw new Exception("对不起，您没有执行此操作的权限！");
			DesignExporter exporter = null;
			if (r instanceof Application) {
				exporter = new ApplicationDesignExporter();
			} else if (r instanceof Form) {
				exporter = new FormDesignExporter();
			} else if (r instanceof View) {
				exporter = new ViewDesignExporter();
			} else if (r instanceof Workflow) {
				exporter = new WorkflowDesignExporter();
			} else {
				throw new Exception("您提供的资源不支持设计摘要导出！");
			}
			String result = exporter.export(r);
			this.setLastMessage(result);
		} catch (Exception e) {
			if ((e instanceof NullPointerException)) {
				this.setLastError(new Exception("空指针异常！"));
			} else {
				this.setLastError(e);
			}
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		OperationResult r = new OperationResult();
		r.setIsForward(true);
		r.setResultUrl(RESULT_REDIRECT_URL);
		return r;
	}
}

