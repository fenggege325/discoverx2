/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.resource.DBResourceInserter;
import com.tansuosoft.discoverx.bll.resource.DBResourceUpdater;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.common.license.Version;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Securer;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;
import com.tansuosoft.discoverx.web.ui.document.ItemsParser;

/**
 * 使用更新包中的文件更新系统的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>fn</td>
 * <td>获取更新包文件名的参数，参数值必须以“{有效更新包文件名}.zip”作为文件名。<br/>
 * 需先上传成功相应的zip文件后才能正常执行。</td>
 * </tr>
 * <tr>
 * <td>log</td>
 * <td>如果提供此参数值，则检查更新日志是否存在以确定更新是否完成。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class UpdateSystem extends Operation {
	protected static String WEBUPDATE_EXT = ".webupdate";
	public static String WEBUPDATE_DIR = "webupdates";

	/**
	 * 缺省构造器。
	 */
	public UpdateSystem() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		String fn = null;
		try {
			String installLocation = CommonConfig.getInstance().getInstallationPath();
			String log = request.getParameter("log");
			if (log != null && log.length() > 0) return checkLog(log, installLocation);
			User u = this.getUser();
			if (!u.isSystemBuiltinAdmin()) {
				if (OrganizationsContext.getInstance().isInTenantContext()) {
					this.setLastError(new Exception("您必须升级到独立版才能执行此操作！"));
					return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
				}
				this.setLastError(new Exception("必须以管理员身份执行系统更新。"));
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			}

			fn = request.getParameter("fn");
			if (fn == null || fn.length() == 0) throw new RuntimeException("无法获取更新包的文件名。");
			final String pid = StringUtil.stringLeftBack(fn, ".");
			String tempFolder = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath();
			String target = String.format("%1$s%2$s", tempFolder, fn);
			File f = new File(target);
			if (!f.exists() || !f.isFile()) throw new RuntimeException("更新包文件不存在。");
			boolean mayBeNeedRestart = false; // 是否可能会需要重启tomcat下的目标web应用。
			boolean willAutoRestart = f.getName().toLowerCase().endsWith(".jar"); // 是否可能会导致tomcat下的目标web应用自动重启。
			final List<String> resources = new ArrayList<String>();

			FileInputStream fis = new FileInputStream(f);
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
			try {
				ZipEntry entry = null;
				String entryName = null;

				final String resourcesPrefixUnix = ExportApplication.RESOURCE_DIRECTORY_NAME + "/";
				final String resourcesPrefixWindows = ExportApplication.RESOURCE_DIRECTORY_NAME + "\\";
				while ((entry = zis.getNextEntry()) != null) {
					entryName = entry.getName();
					if (entryName != null && (entryName.startsWith(resourcesPrefixUnix) || entryName.startsWith(resourcesPrefixWindows))) {
						int pos = entryName.lastIndexOf('/');
						String pureName = (pos >= 0 ? entryName.substring(pos + 1) : entryName);
						resources.add(pureName);
					} else if (entryName != null && entryName.toLowerCase().endsWith(".jar")) {
						mayBeNeedRestart = true;
						willAutoRestart = true;
					} else if (entryName != null && entryName.toLowerCase().indexOf("classes/") > 0) {
						mayBeNeedRestart = true;
					} else if (entryName != null && entryName.startsWith(ExportApplication.DB_RESOURCE_DIRECTORY_NAME)) {
						parseDBResources(zis, entry);
					}
					zis.closeEntry();
				} // while end
			} catch (IllegalArgumentException ex) {
				throw new Exception("错误：更新包中不能包含中文目录或文件名！");
			} finally {
				fis.close();
				zis.close();
			}

			ResourceContext rc = ResourceContext.getInstance();

			// 处理待更新的群组、角色等数据库资源。
			StringBuilder dbresTip = new StringBuilder();
			if (toBeUpdatedDBResources.size() > 0) {
				fillPreservedSecurityCodes();
				Session s = this.getSession();
				fillToBeUpdatedDBResourcesExistMap();
				Resource belongToApp = null;
				for (String unid : toBeUpdatedDBResources.keySet()) {
					Securer securer = toBeUpdatedDBResources.get(unid);
					if (securer == null || !(securer instanceof Resource)) continue;
					Resource dbres = (Resource) securer;
					if (securer.getSecurityCode() > Securer.MIN_AVAILABLE_CODE) securer.setSecurityCode(getNextAvailSC());
					belongToApp = (Resource) (StringUtil.isBlank(dbres.getPUNID()) ? null : rc.getResource(dbres.getPUNID(), Application.class));
					dbresTip.append(dbresTip.length() == 0 ? "," : "").append(dbres.getName()).append("(").append(dbres instanceof Group ? "角色" : "群组").append(belongToApp == null ? "" : "所属应用:" + belongToApp.getName()).append(")");
					if (toBeUpdatedDBResourcesExistMap.containsKey(unid)) {
						DBResourceUpdater updater = new DBResourceUpdater();
						updater.update(dbres, s);
					} else {
						DBResourceInserter inserter = new DBResourceInserter();
						inserter.insert(dbres, s);
					}
					rc.removeObjectFromCache(unid);
				}
				ParticipantTreeProvider.reload();
				FileHelper.deleteFolder(ExportApplication.DB_RES_PATH);
			}

			String resourceLocation = CommonConfig.getInstance().getResourcePath();
			String accessoryLocation = CommonConfig.getInstance().getAccessoryPath();
			String appServerLocation = ExportApplication.getAppServerLocation();
			final String curdt = DateTime.getNowDTString();
			String logFile = String.format("%1$s_%2$d_%3$d", pid, System.currentTimeMillis(), u.getSecurityCode());

			Version.getInstance().appendPatch(pid, curdt, logFile + ".log");

			String n = null;
			for (String r : resources) {
				n = StringUtil.stringLeftBack(r, ".");
				if (n == null || n.length() == 0) continue;
				FileLogger.debug(DateTime.getNowDTString() + ":重载“" + n + "”对应的缓存...");
				if (StringUtil.isUNID(n)) {
					rc.removeObjectFromCache(n);
					ItemsParser.reload(n);
				} else {
					Config.reload(n);
				}
			}
			DocumentBuffer.getInstance().clearCache();

			// 生成web更新指令
			String args = String.format("-i%1$s|-r%2$s|-a%3$s|-p%4$s|-s%5$s|-l%6$s.log", installLocation, resourceLocation, accessoryLocation, target, appServerLocation, logFile);
			String webUpdateDir = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath() + WEBUPDATE_DIR + File.separator;
			File webUpdate = new File(webUpdateDir);
			webUpdate.mkdirs();
			String argfile = String.format("%1$s%2$s%3$s", webUpdateDir, new DateTime().toString("yyyyMMddHHmmss"), WEBUPDATE_EXT);
			BufferedWriter w = null;
			try {
				w = new BufferedWriter(new FileWriter(argfile, false));
				w.write(args);
				w.write("\r\n");
				w.flush();
			} catch (Exception ex) {
				FileLogger.error(ex);
			} finally {
				try {
					if (w != null) w.close();
				} catch (IOException e) {
				}
			}
			FileLogger.debug(DateTime.getNowDTString() + ":开始对“" + CommonConfig.getInstance().getSystemName() + "”进行更新...");
			Thread.sleep(1000);
			if (dbresTip.length() > 0) {
				dbresTip.insert(0, "此次更新过程涉及到更新以下数据库资源:");
				dbresTip.append(".由于被更新的数据库资源的安全编码等信息会被修改，所以请您在更新完毕后重新修改引用这些资源的配置，通常要修改配置的地方包括：流程配置的流程参与者范围、应用配置的文档权限等，您应仔细查找并修改所有引用这些资源的配置.");
			}
			AJAXResponse r = new AJAXResponse();
			r.setMessage(logFile);
			if (dbresTip.length() > 0) r.setTag(dbresTip);
			if (mayBeNeedRestart) {
				r.setStatus(willAutoRestart ? 2 : 1);
				this.setLastAJAXResponse(r);
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			} else {
				r.setStatus(0);
				this.setLastAJAXResponse(r);
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			}
		} catch (Exception e) {
			FileLogger.error(e);
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
	}

	private Map<String, Securer> toBeUpdatedDBResources = new HashMap<String, Securer>(); // 等待更新的群组、角色等数据库资源。
	private Map<String, Boolean> toBeUpdatedDBResourcesExistMap = new HashMap<String, Boolean>(); // 等待更新的群组、角色等数据库资源是否存在的检查。
	private static int[] availSCs = null;

	/**
	 * 获取下一个可用的保留安全编码。
	 * 
	 * @return int
	 */
	protected synchronized static int getNextAvailSC() {
		for (int i = 21; i < availSCs.length; i++) {
			if (availSCs[i] == 0) {
				availSCs[i] = i;
				return i;
			}
		}
		return 0;
	}

	/**
	 * 填充已使用的保留安全编码。
	 */
	protected void fillPreservedSecurityCodes() {
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper r = new SQLWrapper();
				r.setSql("select c_securitycode from t_user where c_securitycode<100 union select c_securitycode from t_group where c_securitycode<100 union select c_securitycode from t_role where c_securitycode<100");
				return r;
			}
		};
		dbr.setResultBuilder(new ResultBuilder() {
			@Override
			public Object build(DBRequest request, Object rawResult) {
				if (rawResult == null || !(rawResult instanceof DataReader)) return null;
				DataReader dr = (DataReader) rawResult;
				availSCs = new int[Securer.MIN_AVAILABLE_CODE];
				for (int i = 0; i < availSCs.length; i++) {
					availSCs[i] = 0;
				}
				try {
					while (dr.next()) {
						int sc = dr.getInt(1, 0);
						if (sc <= 0 || sc >= 100) continue;
						availSCs[sc] = sc;
					}
				} catch (Exception ex) {
					FileLogger.error(ex);
				}
				return null;
			}
		});
		dbr.sendRequest();
	}

	/**
	 * 填充待更新的数据库资源是否存在的信息。
	 */
	protected void fillToBeUpdatedDBResourcesExistMap() {
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper r = new SQLWrapper();
				StringBuilder sbin = new StringBuilder();
				for (String unid : toBeUpdatedDBResources.keySet()) {
					sbin.append(sbin.length() == 0 ? "" : ",").append("'").append(unid).append("'");
				}
				StringBuilder sb = new StringBuilder();
				sb.append("select c_unid from t_group where c_unid in (").append(sbin.toString()).append(") union select c_unid from t_role where  c_unid in (").append(sbin.toString()).append(")");
				r.setSql(sb.toString());
				return r;
			}
		};
		dbr.setResultBuilder(new ResultBuilder() {
			@Override
			public Object build(DBRequest request, Object rawResult) {
				if (rawResult == null || !(rawResult instanceof DataReader)) return null;
				DataReader dr = (DataReader) rawResult;
				try {
					while (dr.next()) {
						toBeUpdatedDBResourcesExistMap.put(dr.getString(1), true);
					}
				} catch (Exception ex) {
					FileLogger.error(ex);
				}
				return null;
			}
		});
		dbr.sendRequest();
	}

	/**
	 * 解析更新包中需要更新的数据库资源。
	 * 
	 * @param zis
	 */
	private void parseDBResources(ZipInputStream zis, ZipEntry entry) {
		try {

			int readSize = 0;
			byte[] buffer = new byte[8192];
			if (entry.getName().endsWith("\\") || entry.getName().endsWith("/")) return;
			String name = entry.getName().replace(ExportApplication.DB_RESOURCE_DIRECTORY_NAME, "").substring(1);
			String tmp = name.replace('\\', '/');
			String dir = StringUtil.stringLeft(tmp, "/");
			String unid = StringUtil.stringRight(tmp, "/").replace(".xml", "");
			File d = new File(ExportApplication.DB_RES_PATH + dir + File.separator);
			d.mkdirs();
			File f = new File(ExportApplication.DB_RES_PATH + dir + File.separator + unid + ".xml");
			FileOutputStream fos = new FileOutputStream(f);
			BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length);
			while ((readSize = zis.read(buffer, 0, buffer.length)) != -1) {
				bos.write(buffer, 0, readSize);
			}
			bos.flush();
			bos.close();
			if (f.exists()) {
				ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(dir);
				if (rd == null) return;
				Object result = Instance.newInstance(rd.getEntity());
				XmlDeserializer xmldes = new XmlDeserializer();
				Object obj = xmldes.deserialize(f.getAbsolutePath(), result.getClass());
				if (obj == null || !(obj instanceof Securer)) return;
				Securer s = (Securer) obj;
				toBeUpdatedDBResources.put(unid, s);
			}
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}

	/**
	 * 检查log对应的更新结果日志文件是否存在，存在则表示更新完成!
	 * 
	 * @param log
	 * @param installLocation
	 * @return
	 */
	private OperationResult checkLog(String log, String installLocation) {
		String logPath = String.format("%1$slogs%2$s%3$s.log", installLocation, File.separator, log);
		File f = new File(logPath);
		boolean fileFlag = false;
		boolean errorFlag = false;
		boolean endFlag = false;
		if (f.exists() && f.isFile()) {
			fileFlag = true;
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(f));
				String line = br.readLine();
				while (line != null) {
					if (line.length() == 0) continue;
					if (!errorFlag && line.indexOf("更新结果:失败.") >= 0 || line.indexOf("执行更新时出现异常") >= 0) {
						errorFlag = true;
					}
					if (!endFlag && line.indexOf("完成执行") == 0) {
						endFlag = true;
					}
					line = br.readLine();
				}
			} catch (IOException ex) {
			} finally {
				try {
					if (br != null) br.close();
				} catch (IOException ex) {
				}
			}
		}
		AJAXResponse r = new AJAXResponse();
		r.setStatus(fileFlag && !errorFlag && endFlag ? 0 : -1);
		if (!fileFlag || !endFlag) {
			r.setMessage("更新过程还在进行中！");
		} else if (errorFlag) {
			r.setMessage("更新过程出现异常！");
		} else {
			FileLogger.debug(DateTime.getNowDTString() + ":完成对“" + CommonConfig.getInstance().getSystemName() + "”的更新.");
			r.setMessage("更新完成！");
		}
		this.setLastAJAXResponse(r);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

