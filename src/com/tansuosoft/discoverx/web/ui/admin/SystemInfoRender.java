/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.license.Version;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出系统信息对应的JSON对象的{@link HtmlRender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class SystemInfoRender implements HtmlRender {
	protected GenericPair<Long, String> gp = null;
	protected String regDt = null;
	protected static StringBuilder m_phtm = new StringBuilder();
	protected static final String t1 = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	protected static final String t2 = "</table>";
	protected static final String tr1 = "<tr>";
	protected static final String tr2 = "</tr>";
	protected static final String td1 = "<td>";
	protected static final String td2 = "</td>";
	protected String unitUnid = null;

	/**
	 * 缺省构造器。
	 */
	public SystemInfoRender() {

	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		fetchInfoFromDb(jspContext);
		StringBuilder sb = new StringBuilder();
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		sb.append("{");
		sb.append("version:").append(Version.getInstance().toJson());
		sb.append(",\r\n").append("a:").append("{b:true,c:'000-FREE-0000000',d:'',e:'").append(gp == null ? "" : StringUtil.stringLeft(gp.getValue(), " ")).append("'}");
		sb.append(",\r\n").append("a1:'").append(StringUtil.getValueString(ptp.getRoot().getName(), CommonConfig.getInstance().getBelong())).append("'");
		sb.append(",\r\n").append("a2:'").append("免费版").append("'");
		sb.append(",\r\n").append("regdt:'").append(regDt == null ? "" : StringUtil.stringLeft(regDt, " ")).append("'");
		sb.append(",\r\n").append("participantCount:").append(ptp.getParticipantCount());
		sb.append(",\r\n").append("userCount:").append(ptp.getParticipantCount(ParticipantType.Person));
		sb.append(",\r\n").append("resourceCacheCapacity:").append(CommonConfig.getInstance().getResourceCacheCapacity());
		sb.append(",\r\n").append("cachedResourceCount:").append(ResourceContext.getInstance().getCachedCount());
		sb.append(",\r\n").append("resourceCount:").append(getResourceCount(new File(CommonConfig.getInstance().getResourcePath())));
		sb.append(",\r\n").append("documentCacheCapacity:").append(CommonConfig.getInstance().getDocumentCacheCapacity());
		sb.append(",\r\n").append("cachedDocumentCount:").append(DocumentBuffer.getInstance().getCachedCount());
		sb.append(",\r\n").append("documentCount:").append(gp == null ? 0 : gp.getKey());
		sb.append("}");
		return sb.toString();
	}

	/**
	 * 从数据库获取信息。
	 * 
	 * @param orgUnid
	 */
	protected void fetchInfoFromDb(JSPContext jspContext) {
		final User u = jspContext.getLoginUser();
		final String orgUnid = OrganizationsContext.getInstance().getOrgUnid(jspContext.getUserSession());
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				StringBuilder sb = new StringBuilder();
				sb.append("select c1,c2 from (");
				sb.append("select -1 c1,min(c_created) c2 from t_user where c_punid='").append(orgUnid).append("' and c_created is not null and c_created!=''");
				if (u != null && u.isSystemBuiltinAdmin()) sb.append(" union select count(*) c1,min(c_created) c2 from t_document");
				sb.append(") t order by c1");
				result.setSql(sb.toString());
				result.setRequestType(RequestType.Query);
				return result;
			}
		};
		dbr.setResultBuilder(new ResultBuilder() {
			@Override
			public Object build(DBRequest request, Object rawResult) {
				if (rawResult == null || !(rawResult instanceof DataReader)) return null;
				DataReader dr = (DataReader) rawResult;
				try {
					while (dr.next()) {
						if (regDt == null) regDt = dr.getString(2);
						else gp = new GenericPair<Long, String>(dr.getLong(1), dr.getString(2));
					}
				} catch (SQLException ex) {
					FileLogger.error(ex);
				}
				return gp;
			}
		});
		dbr.sendRequest();
	}

	/**
	 * 获取更新包详细信息json文本。
	 * 
	 * @return String
	 */
	public static String getPatchDetailJson() {
		// Version.appendPatch中的格式变化时，此处应相应变化
		Version v = Version.getInstance();
		List<StringPair> ps = v.getPatchs();
		StringBuilder sb = new StringBuilder();
		m_phtm.delete(0, m_phtm.length());
		if (ps == null || ps.isEmpty()) {
			sb.append("{status:1,message:'暂无内容'}");
			m_phtm.append(t1).append(tr1).append(td1).append("暂无内容").append(td2).append(tr2).append(t2);
			return sb.toString();
		}
		sb.append("[");
		m_phtm.append(t1);
		m_phtm.append("<colgroup><col width=\"140\"/><col width=\"120\"/><col/></colgroup>");
		m_phtm.append("<tr class=\"thead\">");
		m_phtm.append(td1).append("更新日期").append(td2);
		m_phtm.append(td1).append("更新日志").append(td2);
		m_phtm.append(td1).append("更新包信息").append(td2);
		m_phtm.append(tr2);
		int idx = 0;
		for (int i = ps.size() - 1; i >= 0; i--) {
			StringPair sp = ps.get(i);
			if (sp == null) continue;
			String k = sp.getKey();
			if (k == null || k.length() == 0) continue;
			String strs[] = StringUtil.splitString(k, '|');
			String dt = sp.getValue();
			if (dt == null || strs == null || strs.length == 0) continue;
			sb.append(idx == 0 ? "" : ",").append("\r\n");
			m_phtm.append("\r\n").append("<tr class=\"tbody tr").append((idx % 2) == 0 ? "even" : "odd").append("\">");
			m_phtm.append(td1).append(dt).append(td2);
			m_phtm.append(td1);
			if (strs.length >= 2 && strs[1].endsWith(".log")) {
				m_phtm.append("<a href=\"display_update_log.jsp?log=/").append(strs[1].replace(".log", "")).append("\" target=\"_blank\">").append("查看更新日志").append("</a>");
			} else {
				m_phtm.append("无更新日志");
			}
			m_phtm.append(td2);
			m_phtm.append(td1);
			if (StringUtil.isUNID(strs[0])) {
				m_phtm.append("此更新可能来自网络，您可以尝试<a href=\"http://www.tensosoft.com/program/").append(strs[0]).append(".html\" target=\"_blank\">").append("查看其详细信息").append("</a>");
			} else {
				m_phtm.append("此更新可能来自本地文件");
			}
			m_phtm.append(td2);
			sb.append("{");
			sb.append("dt:'").append(dt).append("'");
			int subIdx = 0;
			for (String s : strs) {
				sb.append(",v").append(subIdx).append(":").append("'").append(StringUtil.encode4Json(s)).append("'");
				subIdx++;
			}
			sb.append("}");
			m_phtm.append(tr2);
			idx++;
		}
		sb.append("]");
		m_phtm.append(t2).append("\r\n");
		return sb.toString();
	}

	/**
	 * 获取更新包详细信息html文本。
	 * 
	 * @return String;
	 */
	public static String getPatchDetailHtml() {
		getPatchDetailJson();
		return m_phtm.toString();
	}

	/**
	 * 获取xml文件个数。
	 * 
	 * @param f
	 * @return long
	 */
	private static long getResourceCount(File f) {
		long result = 0;
		File fs[] = f.listFiles();
		if (fs == null || fs.length == 0) return 0;
		for (File x : fs) {
			if (x.isDirectory()) {
				result += getResourceCount(x);
			} else {
				result++;
			}
		}
		return result;
	}

}

