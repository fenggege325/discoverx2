/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletRequest;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UriUtil;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 获取更新服务器上的程序信息。
 * 
 * @author coca@tensosoft.com
 */
public class ProgramsProvider {
	/**
	 * 私有缺省构造器。
	 */
	private ProgramsProvider() {
	}

	/**
	 * 从远程服务器获取可更新的程序信息（json结果文本）。
	 * 
	 * @param jspContext
	 * @return String
	 */
	public static String provide(JSPContext jspContext) {
		OutputStreamWriter wr = null;
		BufferedReader rd = null;
		try {
			ServletRequest r = jspContext.getRequest();
			StringBuilder sb = new StringBuilder();
			String tsid = getTsid();
			String v = r.getParameter("v");
			String cp = r.getParameter("cp");
			if (tsid == null || tsid.length() == 0) throw new Exception("无法获取验证信息！");
			String add = "http://www.tensosoft.com/login";
			// 请求参数准备
			String data = UriUtil.encodeURIComponent("sid") + "=" + UriUtil.encodeURIComponent(tsid);
			data += "&" + UriUtil.encodeURIComponent("redirect") + "=" + UriUtil.encodeURIComponent("au_programs_viewdata.jsp?view=" + v + "&cp=" + cp + "&__seq=" + System.currentTimeMillis());

			// 发送请求
			URL url = new URL(add);
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0; Discoverx2_ProgramsProvider))");
			conn.setUseCaches(false);
			conn.setAllowUserInteraction(true);
			CookieHandler.setDefault(new CookieManager());
			conn.setDoOutput(true);
			wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();
			wr.close();

			// 获取请求结果
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null;
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			rd.close();
			return sb.toString();
		} catch (Exception ex) {
			return "{error:true:message:'" + StringUtil.encode4Json(ex.getMessage()) + "'}";
		}
	}

	/**
	 * 获取更新网站登录信息。
	 * 
	 * @return String
	 * @throws Exception
	 */
	protected static String getTsid() throws Exception {
		String tsidPath = CommonConfig.getInstance().getInstallationPath() + "WEB-INF" + File.separator + "classes" + File.separator + "tsid.dat";
		File f = new File(tsidPath);
		if (!f.exists() || !f.isFile() || f.length() <= 0) throw new Exception("无法获取校验信息！");
		BufferedReader input = new BufferedReader(new FileReader(f));
		String line = null;
		StringBuilder sbTsid = new StringBuilder();
		try {
			while ((line = input.readLine()) != null) {
				sbTsid.append(line);
			}
		} finally {
			input.close();
		}
		String[] result = ValidateAccount.unwrapAccount(sbTsid.toString());
		return (result != null && result.length > 0 ? result[0] : "");
	}
}

