/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.app.message.Message;
import com.tansuosoft.discoverx.web.app.message.MessageSender;

/**
 * 重置用户密码。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要重置密码的用户的UNID，必须。</td>
 * </tr>
 * <tr>
 * <td>notify</td>
 * <td>是否要发送密码重置通知，可选，默认为不发送，如果值为1则会给目标用户发送密码重置通知。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link Operation#returnResult(String, com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.OperationResult)}</td>
 * <td>通过message属性返回结果文本消息，默认返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * <strong>只有用户本身和有更改用户权限的管理员才能更改密码！</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class UserPasswordReset extends Operation {
	/**
	 * 缺省构造器。
	 */
	public UserPasswordReset() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		User u = this.getUser();
		HttpServletRequest request = this.getHttpRequest();
		String unid = request.getParameter("unid");
		if (StringUtil.isBlank(unid)) {
			this.setLastError(new FunctionException("未提供用户UNID！"));
			return this.returnResult(null, null, this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE));
		}
		boolean canReset = false;
		try {
			User user = null;
			if (u != null && unid.equalsIgnoreCase(u.getUNID())) {
				canReset = true;
				user = u;
			}
			if (!canReset) {
				user = ResourceContext.getInstance().getResource(unid, User.class);
				if (user != null) canReset = SecurityHelper.authorize(u, user, SecurityLevel.Modify);
			}
			if (!canReset) {
				this.setLastError(new FunctionException("您没有重置密码的权限！"));
				return this.returnResult(null, null, this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE));
			}
			com.tansuosoft.discoverx.dao.impl.UserPasswordReset dbr = new com.tansuosoft.discoverx.dao.impl.UserPasswordReset();
			dbr.setParameter(DBRequest.UNID_PARAM_NAME, unid);
			dbr.sendRequest();
			Object result = dbr.getResult();
			if (result != null && "1".equalsIgnoreCase(result.toString())) {
				response.setMessage("密码重置成功！");
				user.setPassword(User.DEFAULT_PASSWORD);
				Session s = Session.getSession(user.getUNID());
				if (s != null && s.getUser() != null) s.getUser().setPassword(User.DEFAULT_PASSWORD);
			} else {
				response.setMessage("密码重置失败，请联系管理员！");
			}
			boolean notify = this.getParameterBoolFromAll("notify", false);
			if (notify) {
				Message m = new Message();
				String subject = String.format("密码重置通知");
				m.setSubject(subject);
				String body = String.format("管理员“%1$s”已经将您的密码重置为默认密码：“12345678”，为了确保您的账号安全，请您用默认密码登录后，通过个性设置修改您的密码！", u.getCN());
				m.setBody(body);
				List<Integer> receivers = new ArrayList<Integer>(1);
				receivers.add(user.getSecurityCode());
				m.setReceivers(receivers);
				MessageSender.sendMessage(m, this.getSession());
			}
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult(null, null, this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE));
		}

		this.setLastAJAXResponse(response);
		// return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		return this.returnResult(null, null, this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE));
	}
}

