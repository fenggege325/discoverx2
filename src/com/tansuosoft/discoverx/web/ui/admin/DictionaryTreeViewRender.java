/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import com.tansuosoft.discoverx.model.Dictionary;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.json.ResourceTreeViewRender;

/**
 * 输出供管理通道使用的字典资源TreeView对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DictionaryTreeViewRender extends ResourceTreeViewRender implements HtmlRender {
	private Dictionary m_resource = null;

	/**
	 * 接收字典资源的构造器。
	 * 
	 * @param dict {@link Dictionary}
	 */
	public DictionaryTreeViewRender(Dictionary dict) {
		m_resource = dict;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (m_resource == null) throw new RuntimeException("没有提供有效字典资源！");
		StringBuilder sb = new StringBuilder();
		renderTreeView(m_resource, sb);
		return sb.toString();
	}
}

