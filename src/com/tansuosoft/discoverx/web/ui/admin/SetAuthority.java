/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.UpdateSuccessConditionalDBRequestAdapter;
import com.tansuosoft.discoverx.dao.impl.UserDeserializer;
import com.tansuosoft.discoverx.model.AuthorityEntry;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Securer;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 将用户或群组加入某个群组（只针对用户）或角色。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>authunid</td>
 * <td>被授予的群组或角色的UNID，必须。</td>
 * </tr>
 * <tr>
 * <td>authdir</td>
 * <td>被授予的群组或角色的目录名，必须。</td>
 * </tr>
 * <tr>
 * <td>unids</td>
 * <td>authunid包含的群组或用户参与者unid（可能多个），即这些群组或用户的unid属于authunid指定的群组或角色。</td>
 * </tr>
 * <tr>
 * <td>dirs</td>
 * <td>参与者目录名，需与参与者unid一一对应</td>
 * </tr>
 * <tr>
 * <td>delete</td>
 * <td>是否删除授权标记，不提供则默认为增加授权，如果值为1则表示删除授权。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>通过message属性返回结果文本消息。</td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * <strong>只有超级管理员/配置管理员和具有“authdis://authunid”指定的群组或角色的用户才能进行授权！</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SetAuthority extends Operation {
	/**
	 * 缺省构造器。
	 */
	public SetAuthority() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		User u = this.getUser();
		HttpServletRequest request = this.getHttpRequest();
		boolean isDelete = StringUtil.getValueBool(request.getParameter("delete"), false);
		String authunid = request.getParameter("authunid");
		String authdir = StringUtil.getValueString(request.getParameter("authdir"), null);
		String[] unids = request.getParameterValues("unids");
		String[] dirs = request.getParameterValues("dirs");

		try {
			if (StringUtil.isBlank(authunid)) throw new FunctionException("未提供群组/角色UNID！");
			if (StringUtil.isBlank(authunid)) throw new FunctionException("未提供群组/角色资源目录名！");
			if (unids == null || unids.length == 0) throw new FunctionException("未提供群组/用户UNID！");
			if (dirs == null || unids.length != dirs.length) throw new FunctionException("未提供群组/用户资源目录或者目录信息与UNID没有一一对应！");

			ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
			ParticipantTree auth = ptp.getParticipantTree(authunid);
			List<Participant> pts = ptp.getParticipants(auth, this.getSession());
			Resource r = ResourceContext.getInstance().getResource(authunid, authdir);
			if (r == null) throw new FunctionException("无法获取“" + authdir + "://" + authunid + "”对应的资源！");
			boolean canSet = (SecurityHelper.checkUserRole(u, Role.ROLE_ADMIN) || SecurityHelper.checkUserRole(u, Role.ROLE_SETTINGSADMIN));
			if (!canSet) {
				canSet = ((r instanceof Role) ? SecurityHelper.checkUserRole(u, authunid) : SecurityHelper.checkUserGroup(u, authunid));
			}
			if (!canSet) throw new FunctionException("对不起，您没有执行此操作的授权！");

			DBRequest dbr = null;
			String unid = null;
			String dir = null;
			Object dbrResult = null;

			boolean requestFlag = true;
			AuthorityEntry ae = null;
			for (int i = 0; i < unids.length; i++) {
				unid = unids[i];
				if (unid == null || unid.length() == 0) continue;
				dir = dirs[i];
				if (dir == null || dir.length() == 0) dir = "user";

				requestFlag = (isDelete ? false : true); // 除非下面的代码中找到数据库中有对应记录否则删除时不更改数据库
				if (pts != null) {
					for (Participant x : pts) {
						if (x == null) continue;
						if (unid.equalsIgnoreCase(x.getUNID())) {
							requestFlag = (isDelete ? true : false); // 如果用户已经包含了指定的群组或角色，则新增时不更改数据库
							break;
						}
					}
				}
				if (requestFlag) {
					if (isDelete) {
						dbr = new com.tansuosoft.discoverx.dao.impl.AuthorityEntryRemover();
					} else {
						dbr = new com.tansuosoft.discoverx.dao.impl.AuthorityEntryInserter();
					}
					ae = new AuthorityEntry();
					ae.setDirectory(authdir);
					ae.setUNID(authunid);
					dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, ae);
					dbr.setParameter(DBRequest.UNID_PARAM_NAME, unid);
					dbr.setParameter("directory", dir);
					// 如果用户已经登录，要更新其信息
					DBRequest dbruser = null;
					Session loginedSession = null;
					if ((loginedSession = Session.getSession(unid)) != null) {
						dbruser = new UserDeserializer();
						dbruser.setParameter(DBRequest.UNID_PARAM_NAME, unid);
						dbr.setNextRequest(new UpdateSuccessConditionalDBRequestAdapter(dbruser));
					}
					dbr.sendRequest();
					dbrResult = dbr.getResult();
					if (dbrResult == null || !"1".equalsIgnoreCase(dbrResult.toString())) {
						Resource target = ResourceContext.getInstance().getResource(unid, dir);
						if (target == null) {
							throw new RuntimeException("无法更改“" + dir + "://" + unid + "”指向的实际不存在的资源的授权：“" + r.getName() + "”！");
						} else {
							throw new RuntimeException("无法更改“" + target.getName() + "”的授权：“" + r.getName() + "！");
						}
					}
					if (dbruser != null && loginedSession != null) {
						User reloaded = dbruser.getResult(User.class);
						if (reloaded != null) {
							Session.newSession(reloaded);
							ResourceContext.getInstance().reloadObject(unid, reloaded);
						}
					}
				}// if end
			}// for end
			ParticipantTreeProvider.getInstance().reloadRoleOrGroupParticipants(((Securer) r).getSecurityCode());
			response.setMessage("操作成功！");
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		this.setLastError(null);
		response.setStatus(0);
		this.setLastAJAXResponse(response);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

