/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.common.ItemTranslator;
import com.tansuosoft.discoverx.common.ItemTranslatorConfig;
import com.tansuosoft.discoverx.common.ItemValidator;
import com.tansuosoft.discoverx.common.ItemValidatorConfig;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.DataSource;
import com.tansuosoft.discoverx.model.DataSourceConstant;
import com.tansuosoft.discoverx.model.DataSourceParticipant;
import com.tansuosoft.discoverx.model.DataSourceResource;
import com.tansuosoft.discoverx.model.DataSourceView;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemGroup;
import com.tansuosoft.discoverx.model.ItemTypeAccessoryOption;
import com.tansuosoft.discoverx.model.ItemTypeDateTimeOption;
import com.tansuosoft.discoverx.model.ItemTypeLinkOption;
import com.tansuosoft.discoverx.model.ItemTypeNumberOption;
import com.tansuosoft.discoverx.model.ItemTypeOption;
import com.tansuosoft.discoverx.model.ItemValueComputationMethod;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 导出表单字段设计信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class ItemDesignExporter extends DesignExporter {
	private Form m_f = null;

	/**
	 * 接收所属表单的构造器。
	 * 
	 * @param f
	 */
	public ItemDesignExporter(Form f) {
		m_f = f;
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecial(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecial(Resource r) {
		if (r == null || !(r instanceof Item)) return "";
		Item item = (Item) r;
		ItemGroup itemGroup = null;
		if (item instanceof ItemGroup) itemGroup = (ItemGroup) item;

		StringBuilder sb = new StringBuilder();

		// 基本
		sb.append("<tr><td>行列位置(row,column)</td><td>").append(item.getRow()).append(",").append(item.getColumn()).append("</td></tr>").append("\r\n");
		if (itemGroup != null) {
			int gt = itemGroup.getGroupType().getIntValue();
			sb.append("<tr><td>分组类型</td><td>").append(gt == 0 ? "控制" : (gt == 1 ? "表格" : "段落")).append("</td></tr>").append("\r\n");
			sb.append("<tr><td>最低条目数</td><td>").append(itemGroup.getMinCount()).append("</td></tr>").append("\r\n");
			sb.append("<tr><td>最高条目数</td><td>").append(itemGroup.getMaxCount()).append("</td></tr>").append("\r\n");
		}

		// 数据
		if (itemGroup == null) {
			String itts[] = { "文本", "数字", "日期时间", "图片", "链接", "富文本", "附加文件", "字段组" };
			int it = item.getType().getIntValue();
			if (it == 101)
				it = 5;
			else if (it == -1)
				it = 6;
			else if (it == -2)
				it = 7;
			else if (it <= 5) it--;
			sb.append("<tr><td>数据类型</td><td>").append(itts[it]).append(item.getDataTypeOption() != null ? "<a href=\"#" + item.getAlias() + "_dataTypeOption\">查看选项</a>" : "").append("</td></tr>").append("\r\n");
			sb.append("<tr><td>最大输入长度</td><td>").append(item.getSize()).append("</td></tr>").append("\r\n");
			sb.append("<tr><td>缺省值公式</td><td>").append(this.processExpression(item.getDefaultValue())).append("</td></tr>").append("\r\n");
			sb.append("<tr><td>多值分隔符</td><td>").append(StringUtil.getValueString(item.getDelimiter(), "<不允许多值>")).append("</td></tr>").append("\r\n");

			String dsts[] = { "无数据源", "资源数据源", "视图数据源", "参与者数据源", "常数数据源", "自定义数据源" };
			int ds = item.getDataSourceType().getIntValue();
			if (ds == 100) {
				ds = 5;
			}
			sb.append("<tr><td>数据源类型</td><td>").append(dsts[ds]).append(item.getDataSource() != null ? "<a href=\"#" + item.getAlias() + "_dataSource\">查看选项</a>" : "").append("</td></tr>").append("\r\n");
			String dsits[] = { "自动", "单机选择标记弹出", "获取焦点时弹出", "随输入自动完成" };
			int dsi = item.getDataSourceInput().getIntValue();
			sb.append("<tr><td>数据源输入方式</td><td>").append(dsits[dsi]).append("</td></tr>").append("\r\n");
		}
		// 呈现
		sb.append("<tr><td>大小(宽*高)</td><td>").append(item.getWidth()).append("*").append(item.getHeight()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>字段标题大小(宽*高)</td><td>").append(item.getCaptionWidth()).append("*").append(item.getCaptionHeight() == 0 ? "与字段内容同一行" : "与字段内容不同行").append("</td></tr>").append("\r\n");

		String dts[] = { "自定义", "单行文本框", "多行文本框", "下拉组合框", "多选组合框", "多选框", "单选框", "图片控件", "链接控件", "富文本编辑控件", "附加文件处理控件", "分组控件", "系统默认" };
		int d = item.getDisplay().getIntValue();
		if (d == -1) {
			d = 12;
		}
		sb.append("<tr><td>呈现方式</td><td>").append(dts[d]).append(d == 0 ? "呈现类:" + item.getDisplayImplement() + ",只读呈现类:" + item.getReadonlyDisplayImplement() : "").append("</td></tr>").append("\r\n");

		// 行为
		if (itemGroup == null) {
			sb.append("<tr><td>是否控制字段</td><td>").append(item.getControl() ? "是" : "否").append("</td></tr>").append("\r\n");
			sb.append("<tr><td>是否必填字段</td><td>").append(item.getRequired() ? "是" : "否").append("</td></tr>").append("\r\n");
			ItemValidator iv = ItemValidatorConfig.getInstance().getItemValidator(item.getJsValidator());
			sb.append("<tr><td>字段校验</td><td>").append(iv != null ? iv.getValidatorName() + "[" + iv.getJsFunctionName() + "]" : "").append("</td></tr>").append("\r\n");
			ItemTranslator it = ItemTranslatorConfig.getInstance().getItemTranslator(item.getJsTranslator());
			sb.append("<tr><td>字段转换</td><td>").append(it != null ? it.getTranslatorName() + "[" + it.getJsFunctionName() + "]" : "").append("</td></tr>").append("\r\n");
			sb.append("<tr><td>是否摘要字段</td><td>").append(item.getSummarizable() ? "是" : "否").append("</td></tr>").append("\r\n");
			String g = item.getGroup();
			String gt = "";
			ItemGroup ig = null;
			if (g != null && g.length() > 0) {
				ig = m_f.getItemGroupByUNID(g);
				if (ig != null) gt = ig.getCaption();
			}
			sb.append("<tr><td>所属字段组</td><td>").append((ig != null ? "<a href=\"#" + ig.getAlias() + "_basic\">" : "") + gt + (ig != null ? "</a>" : "")).append("</td></tr>").append("\r\n");
			String exp = this.processExpression(item.getComputationExpression());
			if (exp != null && exp.length() > 0) {
				sb.append("<tr><td>字段值计算公式</td><td>").append(exp).append("</td></tr>").append("\r\n");
				sb.append("<tr><td>字段值计算方式</td><td>").append(item.getComputationMethod() == ItemValueComputationMethod.Create ? "创建时计算" : "打开时计算").append("</td></tr>").append("\r\n");
			}
		}
		sb.append("<tr><td>可见公式</td><td>").append(this.processExpression(item.getVisibleExpression())).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>不可见时的呈现选项</td><td>").append(item.getOutputHiddenField() ? "对用户隐藏对浏览器不隐藏" : "对用户和浏览器都隐藏").append("</td></tr>").append("\r\n");
		sb.append("<tr><td>只读公式</td><td>").append(this.processExpression(item.getReadonlyExpression())).append("</td></tr>").append("\r\n");

		return sb.toString();
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecialCollection(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecialCollection(Resource r) {
		if (r == null || !(r instanceof Item)) return "";
		Item item = (Item) r;
		ItemGroup itemGroup = null;
		if (item instanceof ItemGroup) itemGroup = (ItemGroup) item;

		StringBuilder sb = new StringBuilder();
		if (itemGroup == null) {
			// 数据类型选项
			ItemTypeOption ito = item.getDataTypeOption();
			if (ito != null) {
				sb.append(this.exportH2("数据类型选项", r.getAlias() + "_dataTypeOption"));
				sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
				sb.append("<colgroup><col width=\"38%\"/><col/></colgroup>").append("\r\n");
				sb.append("<tr class=\"head\"><td>属性名</td><td>属性值</td></tr>").append("\r\n");
				if (ito instanceof ItemTypeAccessoryOption) {
					ItemTypeAccessoryOption itoa = (ItemTypeAccessoryOption) ito;
					List<Reference> ats = itoa.getAccessoryTypes();
					String att = "";
					if (ats != null && ats.size() > 0) {
						for (Reference rx : ats) {
							if (rx == null) continue;
							AccessoryType at = ResourceContext.getInstance().getResource(rx.getUnid(), AccessoryType.class);
							if (at == null) continue;
							att += att.length() == 0 ? "" : "," + at.getName();
						}
					}
					sb.append("<tr><td>允许的附加文件类型</td><td>").append(att).append("</td></tr>").append("\r\n");
				} else if (ito instanceof ItemTypeDateTimeOption) {
					ItemTypeDateTimeOption itodt = (ItemTypeDateTimeOption) ito;
					sb.append("<tr><td>值格式</td><td>").append(StringUtil.getValueString(itodt.getValueFormat(), "")).append("</td></tr>").append("\r\n");
					sb.append("<tr><td>标题格式</td><td>").append(StringUtil.getValueString(itodt.getTitleFormat(), "")).append("</td></tr>").append("\r\n");
				} else if (ito instanceof ItemTypeLinkOption) {
					ItemTypeLinkOption itol = (ItemTypeLinkOption) ito;
					sb.append("<tr><td>链接打开模式</td><td>").append(itol.getTarget()).append("</td></tr>").append("\r\n");
				} else if (ito instanceof ItemTypeNumberOption) {
					ItemTypeNumberOption iton = (ItemTypeNumberOption) ito;
					sb.append("<tr><td>整数位数</td><td>").append(iton.getPrecision()).append("</td></tr>").append("\r\n");
					sb.append("<tr><td>小数位数</td><td>").append(iton.getScale()).append("</td></tr>").append("\r\n");
				}
				sb.append("</table>").append("\r\n");
			}
			// 数据源选项
			DataSource ds = item.getDataSource();
			if (ds != null) {
				sb.append(this.exportH2("数据源选项", r.getAlias() + "_dataSource"));
				sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
				sb.append("<colgroup><col width=\"38%\"/><col/></colgroup>").append("\r\n");
				sb.append("<tr class=\"head\"><td>属性名</td><td>属性值</td></tr>").append("\r\n");
				sb.append("<tr><td>值格式</td><td>").append(StringUtil.getValueString(ds.getValueFormat(), "")).append("</td></tr>").append("\r\n");
				sb.append("<tr><td>标题格式</td><td>").append(StringUtil.getValueString(ds.getTitleFormat(), "")).append("</td></tr>").append("\r\n");
				String astart = StringUtil.isBlank(item.getValueStoreItem()) ? "" : "<a href=\"#" + item.getValueStoreItem() + "_basic\">";
				String aend = StringUtil.isBlank(item.getValueStoreItem()) ? "" : "</a>";
				sb.append("<tr><td>数据源值保存字段名</td><td>").append(astart + StringUtil.getValueString(item.getValueStoreItem(), "") + aend).append("</td></tr>").append("\r\n");
				sb.append("<tr><td>层次数据源联动选择字段信息</td><td>").append(StringUtil.getValueString(item.getLinkageItem(), "")).append("</td></tr>").append("\r\n");
				if (ds instanceof DataSourceConstant) {
					DataSourceConstant dsc = (DataSourceConstant) ds;
					sb.append("<tr><td>数据源值</td><td>").append(dsc.getConfigValue()).append("</td></tr>").append("\r\n");
				} else if (ds instanceof DataSourceParticipant) {
					DataSourceParticipant dsp = (DataSourceParticipant) ds;
					sb.append("<tr><td>参与者类型</td><td>").append(dsp.getParticipantType() == ParticipantType.Person ? "用户" : "部门").append("</td></tr>").append("\r\n");
				} else if (ds instanceof DataSourceResource) {
					DataSourceResource dsr = (DataSourceResource) ds;
					ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(dsr.getDirectory());
					Resource rx = ResourceContext.getInstance().getResource(dsr.getUNID(), rd);
					sb.append("<tr><td>资源</td><td>").append((rd != null && rx != null) ? rd.getName() + ":" + rx.getName() : "").append("</td></tr>").append("\r\n");
				} else if (ds instanceof DataSourceView) {
					DataSourceView dsv = (DataSourceView) ds;
					String vid = dsv.getUNID();
					View v = null;
					if (vid != null && vid.length() > 0) v = ResourceContext.getInstance().getResource(vid.startsWith("view") ? (ResourceAliasContext.getInstance().getUNIDByAlias(View.class, vid)) : vid, View.class);
					sb.append("<tr><td>视图</td><td>").append(v == null ? "<未知>" : v.getName()).append("</td></tr>").append("\r\n");
				} else {
					sb.append("<tr><td>自定义数据源实现类</td><td>").append(item.getCustomDataSourceImplement()).append("</td></tr>").append("\r\n");
				}

				sb.append("</table>").append("\r\n");
			}

			// 字段控件扩展html属性
			sb.append(this.exportHtmlAttributes(item.getHtmlAttributes(), "字段控件扩展html属性", item.getAlias() + "_documentDisplay_htmlAttributes"));
		}
		return sb.toString();
	}
}

