/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 获取功能函数内置属性信息的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>imp</td>
 * <td>要获取的功能函数的实现类全名，必须</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_CONTENT}</td>
 * <td>返回功能函数类的{@link FunctionAttributes}注释的结果对应的json对象。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "getFunctionInfo", name = "获取函数资源属性缺省值", desc = "获取并返回初始配置函数对应的资源时相关属性的缺省值信息对应的json文本")
public class GetFunctionInfo extends Operation {
	/**
	 * 缺省构造器。
	 */
	public GetFunctionInfo() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			HttpServletRequest request = this.getHttpRequest();
			String imp = request.getParameter("imp");
			if (imp == null || imp.length() == 0) throw new Exception("没有提供有效的功能函数实现类。");
			Class<?> clazz = Class.forName(imp);

			FunctionAttributes fas = clazz.getAnnotation(FunctionAttributes.class);
			StringBuilder sb = new StringBuilder();
			if (fas == null) {
				sb.append("");
			} else {
				sb.append("name:'").append(StringUtil.encode4Json(fas.name())).append("'");
				sb.append(",").append("alias:'").append(fas.alias()).append("'");
				sb.append(",").append("type:").append(fas.type());
				sb.append(",").append("desc:'").append(StringUtil.encode4Json(fas.desc())).append("'");
				sb.append(",").append("result:'").append(fas.result()).append("'");
				sb.append(",").append("icon:'").append(StringUtil.encode4Json(fas.icon())).append("'");
			}
			this.setLastMessage(sb.toString());
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_CONTENT);
	}
}

