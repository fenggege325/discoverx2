/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.resource.DBResourceInserter;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.model.AccountType;
import com.tansuosoft.discoverx.model.Authority;
import com.tansuosoft.discoverx.model.AuthorityEntry;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.LogType;
import com.tansuosoft.discoverx.model.Organization;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.GBK;
import com.tansuosoft.discoverx.util.HttpUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 用户注册相关操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>uid</td>
 * <td>账号名，必须。</td>
 * </tr>
 * <tr>
 * <td>cn</td>
 * <td>用户名，必须。</td>
 * </tr>
 * <tr>
 * <td>pid</td>
 * <td>密码，必须。</td>
 * </tr>
 * <tr>
 * <td>cpid</td>
 * <td>确认密码，必须且和密码一致。</td>
 * </tr>
 * <tr>
 * <td>org</td>
 * <td>单位名称，可选。</td>
 * </tr>
 * <tr>
 * <td>address</td>
 * <td>联系方式，可选。</td>
 * </tr>
 * <tr>
 * <td>verifycode</td>
 * <td>验证码，必须。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的url结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class UserRegister extends Operation {

	/**
	 * 重载：实现操作功能。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		response.setTag("userregister");
		Session s = null;
		try {
			HttpServletRequest request = this.getHttpRequest();
			if (!verifyCode(request, true)) throw new RuntimeException("对不起，您没有提供有效的验证码！");
			s = Session.getSystemSession();
			User user = s.getUser();
			String uid = this.getParameterStringFromAll("uid", null);
			if (uid == null || uid.trim().length() == 0) throw new RuntimeException("对不起，您没有提供有效登录帐号！");
			if (uid.length() > 200) throw new RuntimeException("对不起，您提供的登录帐号太长！");
			String cn = this.getParameterStringFromAll("cn", null);
			if (cn == null || cn.trim().length() == 0) throw new RuntimeException("对不起，您没有提供有效姓名！");
			if (cn.length() > 100) throw new RuntimeException("对不起，您提供的姓名太长！");
			String password = this.getParameterStringFromAll("pid", null);
			String confirmPassword = this.getParameterStringFromAll("cpid", null);
			if (password != null && !password.equals(confirmPassword)) throw new RuntimeException("对不起，您输入的登录密码和确认密码不匹配！");
			String org = this.getParameterStringFromAll("org", null);
			if (org != null && org.length() > 100) throw new RuntimeException("对不起，您提供的单位名称太长！");

			// 单位
			Organization o = new Organization();
			if (o.getSecurityCode() == 0) o.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
			o.setName(org);
			o.setFullName(org);
			o.setPUNID(null);
			StringBuilder orgpy = new StringBuilder();
			char cs[] = org.toCharArray();
			String py = null;
			for (char c : cs) {
				py = GBK.getSpell(c);
				if (py == null || py.length() == 0) orgpy.append(c);
				else orgpy.append(py);
			}
			o.setAlias("org" + (orgpy.length() >= 197 ? orgpy.substring(0, 190) : orgpy.toString()));
			o.setCreator(user.getName());
			o.setSort(o.getSecurityCode());
			String address = this.getParameterStringFromAll("address", null);
			if (address != null && address.length() > 500) throw new RuntimeException("对不起，您提供的联系方式太长！");
			o.setDescription(address);
			DBResourceInserter insert = new DBResourceInserter();
			if (insert.insert(o, s) == null) throw new RuntimeException("服务器出现异常(0)，请您联系管理员！");

			// 用户
			User newUser = new User();
			if (newUser.getSecurityCode() == 0) newUser.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
			newUser.setAccountType(AccountType.ValidAccount.getIntValue() + AccountType.User.getIntValue());
			newUser.setName(o.getFullName() + User.SEPARATOR + cn);
			newUser.setAlias(uid);
			newUser.setPassword(StringUtil.getMD5HashString(password));
			newUser.setCategory(org);
			newUser.setSort(newUser.getSecurityCode());
			newUser.setPUNID(o.getUNID());
			newUser.setAuthority(new Authority(newUser.getUNID()));
			newUser.getAuthority().setAuthorityEntries(new ArrayList<AuthorityEntry>(1));
			AuthorityEntry ae = new AuthorityEntry();
			ae.setDirectory("role");
			ae.setSecurityCode(Role.ROLE_SETTINGSADMIN_SC);
			ae.setUNID(Role.ROLE_SETTINGSADMIN);
			newUser.getAuthority().getAuthorityEntries().add(ae);
			insert = new DBResourceInserter();
			if (insert.insert(newUser, s) == null) throw new RuntimeException("服务器出现异常(1)，请您联系管理员！");

			// 日志
			Log log = new Log();
			log.setCreated(new DateTime().toString());
			log.setSubject(ParticipantHelper.getFormatValue(newUser, "cn"));
			log.setVerb("注册");
			log.setOwner(newUser.getUNID());
			log.setResult("成功");
			log.setLogType(LogType.User);
			log.setObject((request == null ? "" : request.getHeader("User-Agent")) + "[" + HttpUtil.getRemoteAddr(request) + "]");
			this.writeLog(log);
		} catch (Exception ex) {
			FileLogger.error(ex);
			response.setMessage(ex.getMessage());
			response.setStatus(-1);
			this.setLastAJAXResponse(response);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		response.setMessage("注册成功，您现在可以使用注册的账号和密码<a href=\"../login.jsp\">登录</a>您的云OA系统了！");
		this.setLastAJAXResponse(response);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_INFO_MESSAGE);
	}

	/**
	 * 返回验证码是否匹配。
	 * 
	 * <p>
	 * 生成的验证码以“<strong>rand</strong>”为参数名保存在http会话中；用户提供的校验码通过http请求提供，其参数名为“<strong>verifycode</strong>”。
	 * </p>
	 * 
	 * @param request
	 * @param removedAfterVerfied 验证完后是否删除http会话中的验证码。
	 * @return boolean
	 */
	public static boolean verifyCode(HttpServletRequest request, boolean removedAfterVerfied) {
		if (request == null) return false;
		HttpSession httpSession = request.getSession();
		final String randAttName = "rand";
		Object obj = (httpSession == null ? null : httpSession.getAttribute(randAttName));
		String rand = (obj == null ? "1" : obj.toString());
		String providedRand = StringUtil.getValueString(request.getParameter("verifycode"), "2");
		if (removedAfterVerfied && httpSession != null) httpSession.removeAttribute(randAttName);
		return rand.equalsIgnoreCase(providedRand);
	}
}

