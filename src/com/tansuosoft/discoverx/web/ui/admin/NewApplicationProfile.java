/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ApplicationProfile;
import com.tansuosoft.discoverx.bll.ResourceConstructor;
import com.tansuosoft.discoverx.bll.ResourceConstructorProvider;
import com.tansuosoft.discoverx.bll.ResourceInserter;
import com.tansuosoft.discoverx.bll.ResourceInserterProvider;
import com.tansuosoft.discoverx.bll.ResourceUpdater;
import com.tansuosoft.discoverx.bll.ResourceUpdaterProvider;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 为指定应用程序创建一个空白配置表单的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>app</td>
 * <td>要为其创建空白表单的应用程序UNID，必须。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>通过message属性返回创建的表单的UNID。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "newApplicationProfile", name = "创建简要表表单", desc = "为指定应用程序创建一个空白配置表单")
public class NewApplicationProfile extends Operation {
	/**
	 * 缺省构造器。
	 */
	public NewApplicationProfile() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		HttpServletRequest request = this.getHttpRequest();
		String appUnid = request.getParameter("app");
		if (StringUtil.isBlank(appUnid)) {
			this.setLastError(new FunctionException("未提供必要的应用程序UNID参数！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}

		Application app = ResourceContext.getInstance().getResource(appUnid, Application.class);
		if (app == null) {
			this.setLastError(new FunctionException("无法获取“" + appUnid + "”对应的应用程序资源！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}

		Form f = null;
		Session s = this.getSession();
		try {
			String dir = ResourceDescriptorConfig.getInstance().getResourceDirectory(Form.class);
			ResourceConstructor rc = ResourceConstructorProvider.getResourceConstructor(dir);
			f = (Form) rc.construct(dir, s, null, appUnid, null);
			if (f == null) {
				this.setLastError(new FunctionException("无法为应用程序“" + app.getName() + "”创建配置项表单，请检查是否有足够权限！"));
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			}
			String alias = app.getAlias();
			if (alias != null && alias.toLowerCase().startsWith("app")) alias = alias.substring(3);
			f.setAlias("frmp" + alias);
			f.setName(app.getName() + "配置项表单");
			f.setItems(new ArrayList<Item>());
			f.setPUNID(appUnid);
			Item item = new Item();
			item.setAlias("fld_subject");
			item.setName("配置项文档说明");
			item.setRow(1);
			item.setColumn(1);
			item.setPUNID(f.getUNID());
			f.getItems().add(item);
			ResourceInserter inserter = ResourceInserterProvider.getResourceInserter(f);
			inserter.insert(f, s);
			app.setProfileForm(f.getUNID());
			ResourceUpdater updater = ResourceUpdaterProvider.getResourceUpdater(app);
			updater.update(app, s);
			ApplicationProfile.clear(app);
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}

		response.setMessage(f.getUNID());
		this.setLastAJAXResponse(response);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

