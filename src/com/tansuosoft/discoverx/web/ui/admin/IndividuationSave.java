/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.ResourceSerializerProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Scroll;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UNIDProvider;
import com.tansuosoft.discoverx.util.serialization.Serializer;
import com.tansuosoft.discoverx.util.serialization.XmlSerializer;
import com.tansuosoft.discoverx.web.ui.portal.PortalParser;

/**
 * 保存用户个性定制的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>type</td>
 * <td>个性定制类型</td>
 * </tr>
 * <tr>
 * <td>value</td>
 * <td>个性定制结果的值</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>如果个性定制结果保存成功，则message结果为'true'。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author simon@tansuosoft.cn
 */
public class IndividuationSave extends Operation {
	/**
	 * 缺省构造器。
	 */
	public IndividuationSave() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		HttpServletRequest request = this.getHttpRequest();
		String type = request.getParameter("type");
		String value = request.getParameter("value");
		String leavedt = request.getParameter("leavedt");
		String returndt = request.getParameter("returndt");
		String agent = request.getParameter("agent");
		String broadcast = request.getParameter("broadcast");
		String notificationtemplate = request.getParameter("notificationtemplate");
		String smsnotificationoption = request.getParameter("smsnotificationoption");
		String sms = request.getParameter("sms");
		String portletsUnid = request.getParameter("portlets");
		String theme = request.getParameter("theme");
		String frame = request.getParameter("frame");
		String columnWidth = request.getParameter("columnWidth");
		String columnCount = request.getParameter("columnCount");
		String delportletUNID = request.getParameter("delportletUNID");
		Session session = this.getSession();
		Profile profile = Profile.getInstance(session);
		try {

			// 快捷链接列表保存
			if (type.equalsIgnoreCase("shortcutLinks")) {
				if (!profile.setShortcutLinks(value)) {
					response.setMessage("快捷链接列表保存失败！");
				} else
					response.setMessage("您的快捷链接列表已保存成功！");
			}

			// 快捷操作列表保存
			if (type.equalsIgnoreCase("shortcutActions")) {
				if (!profile.setShortcutActions(value)) {
					response.setMessage("快捷操作列表保存失败！");
				} else
					response.setMessage("您的快捷操作列表已保存成功！");
			}

			// 离开状态设置保存
			if (type.equalsIgnoreCase("leaveOffice")) {
				if (!profile.setLeaveOffice(leavedt, returndt, agent, broadcast, notificationtemplate)) {
					response.setMessage("离开状态设置保存失败！");
					response.setStatus(-1);
				} else
					response.setMessage("您的离开状态设置已保存成功！");
				response.setStatus(1);
			}
			
			// 恢复正常工作设置
			if (type.equalsIgnoreCase("onwork")) {
				if (!profile.setLeaveOffice("", "", "", "", "")) {
					response.setMessage("恢复正常工作设置失败！");
					response.setStatus(-1);
				} else
					response.setMessage("您已成功恢复正常工作状态！");
				response.setStatus(1);
			}

			// 短信通知设置保存
			if (type.equalsIgnoreCase("sms")) {
				if (!profile.setSms(sms) || !profile.setSmsNotification(smsnotificationoption)) {
					response.setStatus(-1);
					response.setMessage("短信通知设置保存失败！");
				} else {
					response.setStatus(1);
					response.setMessage("您的短信通知设置已保存成功！");
				}
			}
			
			// 外部邮件设置完成保存
			if (type.equalsIgnoreCase("email")) {
				String email = request.getParameter("email");
				String smtp = request.getParameter("smtp");
				String pop3 = request.getParameter("pop3");
				String account = request.getParameter("account");
				String password = request.getParameter("password");
				if (!profile.setEmails(email, smtp, pop3, account, password)) {
					response.setStatus(-1);
					response.setMessage("外部邮件设置保存失败！");
				} else {
					response.setStatus(1);
					response.setMessage("您的外部邮件设置完成已保存成功！");
				}
			}
			
			// 删除外部邮件设置
			if (type.equalsIgnoreCase("emailDel")) {
				String email = "";
				String smtp = "";
				String pop3 = "";
				String account = "";
				String password = "";
				if (!profile.setEmails(email, smtp, pop3, account, password)) {
					response.setStatus(-1);
					response.setMessage("删除外部邮件设置失败！");
				} else {
					response.setStatus(1);
					response.setMessage("您的外部邮件设置已删除成功！");
				}
			}

			// 小窗口拖拽保存
			if (type.equalsIgnoreCase("portletDrag")) {
				Portal udfPortal = Profile.getInstance(session.getUser()).getPortal();
				String[] columns = portletsUnid.split("\\|");
				if (udfPortal == null) {
					Portal defaultPortal = ResourceContext.getInstance().getDefaultResource(Portal.class);
					udfPortal = (Portal) defaultPortal.clone();
					udfPortal.setUNID(UNIDProvider.getUNID());
				}
				for (int i = 0; i < columns.length; i++) {
					if (columns[i] != null && columns[i].length() > 0) {
						String[] portlets = columns[i].split(";");
						for (int j = 0; j < portlets.length; j++) {
							String portletUNID = portlets[j];
							if (udfPortal.getPortletByUNID(portletUNID) == null) continue;
							udfPortal.getPortletByUNID(portletUNID).setColumn(i + 1);
							udfPortal.getPortletByUNID(portletUNID).setRow(j + 1);
						}
					}
				}

				// 序列化
				Serializer ser = ResourceSerializerProvider.getResourceSerializer(udfPortal.getClass());
				if (ser == null) ser = new XmlSerializer();
				StringWriter writer = new StringWriter();
				ser.serialize(udfPortal, writer);
				StringBuilder sb = new StringBuilder();
				sb.append("<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n");
				sb.append("<resource type=\"");
				sb.append("com.tansuosoft.discoverx.model.Portal");
				sb.append("\">\r\n");
				sb.append(writer.toString());
				sb.append("</resource>");
				if (!profile.setPortal(sb.toString())) {
					response.setMessage("自定义门户设置保存失败！");
				} else {
					PortalParser.reloadInstance(udfPortal);
					response.setMessage("您的门户自定义已保存成功！");
				}
			}

			// 小窗口删除保存
			if (type.equalsIgnoreCase("portletDel")) {
				Portal udfPortal = Profile.getInstance(session.getUser()).getPortal();
				if (udfPortal == null) {
					Portal defaultPortal = ResourceContext.getInstance().getDefaultResource(Portal.class);
					udfPortal = (Portal) defaultPortal.clone();
					udfPortal.setUNID(UNIDProvider.getUNID());
				}
				udfPortal.getPortlets().remove(udfPortal.getPortletByUNID(delportletUNID));

				// 序列化
				Serializer ser = ResourceSerializerProvider.getResourceSerializer(udfPortal.getClass());
				if (ser == null) ser = new XmlSerializer();
				StringWriter writer = new StringWriter();
				ser.serialize(udfPortal, writer);
				StringBuilder sb = new StringBuilder();
				sb.append("<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n");
				sb.append("<resource type=\"");
				sb.append("com.tansuosoft.discoverx.model.Portal");
				sb.append("\">\r\n");
				sb.append(writer.toString());
				sb.append("</resource>");
				if (!profile.setPortal(sb.toString())) {
					response.setStatus(-1);
					response.setMessage("小窗口删除失败！");
				} else {
					PortalParser.reloadInstance(udfPortal);
					response.setStatus(1);
					response.setMessage("小窗口删除成功！");
				}
			}

			// 门户样式自定义保存
			if (type.equalsIgnoreCase("portal")) {
				Portal udfPortal = Profile.getInstance(session.getUser()).getPortal();
				if (udfPortal == null) {
					Portal defaultPortal = ResourceContext.getInstance().getDefaultResource(Portal.class);
					udfPortal = (Portal) defaultPortal.clone();
					udfPortal.setUNID(UNIDProvider.getUNID());
				}
				PortalParser portalParser = PortalParser.getInstance(udfPortal);
				int oldcolumnCount = udfPortal.getColumnCount();
				if (theme != null && theme.trim().length() > 0) {
					udfPortal.setTheme(theme);
				}
				if (frame != null && frame.trim().length() > 0) {
					udfPortal.setFrame(frame);
				}
				if (columnCount != null && columnCount.trim().length() > 0) {
					udfPortal.setColumnCount(Integer.parseInt(columnCount));
				}
				if (columnWidth != null && columnWidth.trim().length() > 0) {
					String[] cwstr = columnWidth.split("[，；,;\\s]");
					if (cwstr.length != udfPortal.getColumnCount()) {
						response.setStatus(-1);
						response.setMessage("门户允许最大栏目数和栏目宽度配置中的栏目数不符！");
						this.setLastAJAXResponse(response);
						return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
					}
					List<Integer> cw = new ArrayList<Integer>();
					if (cwstr != null && cwstr.length > 0) {
						for (String x : cwstr) {
							if (x != null) cw.add(Integer.parseInt(x));
						}
					}
					if (cw != null && cw.size() == udfPortal.getColumnCount()) udfPortal.setColumnWidth(cw);

					if (cwstr.length < oldcolumnCount) {
						int rowCount = 0;
						if (portalParser.getColumnPortlets(cwstr.length) != null && portalParser.getColumnPortlets(cwstr.length).size() > 0) rowCount = portalParser.getColumnPortlets(cwstr.length).size();
						List<Portlet> portlets = null;
						for (int i = cwstr.length + 1; i <= oldcolumnCount; i++) {
							portlets = portalParser.getColumnPortlets(i);
							if (portlets != null && portlets.size() > 0) {
								for (Portlet x : portlets) {
									udfPortal.getPortletByUNID(x.getUNID()).setColumn(cwstr.length);
									udfPortal.getPortletByUNID(x.getUNID()).setRow(x.getRow() + rowCount);
								}
							}
						}
					}
				}

				// 序列化
				Serializer ser = ResourceSerializerProvider.getResourceSerializer(udfPortal.getClass());
				if (ser == null) ser = new XmlSerializer();
				StringWriter writer = new StringWriter();
				ser.serialize(udfPortal, writer);
				StringBuilder sb = new StringBuilder();
				sb.append("<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n");
				sb.append("<resource type=\"");
				sb.append("com.tansuosoft.discoverx.model.Portal");
				sb.append("\">\r\n");
				sb.append(writer.toString());
				sb.append("</resource>");
				// System.out.println(sb.toString());
				if (!profile.setPortal(sb.toString())) {
					response.setStatus(-1);
					response.setMessage("门户样式自定义保存失败！");
				} else {
					PortalParser.reloadInstance(udfPortal);
					response.setStatus(1);
					response.setMessage("您的门户样式自定义保存成功！");
				}
			}

			// 添加小窗口保存
			if (type.equalsIgnoreCase("portlet")) {
				Portal udfPortal = Profile.getInstance(session.getUser()).getPortal();
				if (udfPortal == null) {
					Portal defaultPortal = ResourceContext.getInstance().getDefaultResource(Portal.class);
					udfPortal = (Portal) defaultPortal.clone();
					udfPortal.setUNID(UNIDProvider.getUNID());
				}
				PortalParser portalParser = PortalParser.getInstance(udfPortal);
				String name = StringUtil.getValueString(request.getParameter("portlettitle"), null);
				int column = StringUtil.getValueInt(request.getParameter("column"), 0);
				int row = 1;
				int displayCount = StringUtil.getValueInt(request.getParameter("displayCount"), 10);
				boolean showTitle = true;
				int height = StringUtil.getValueInt(request.getParameter("height"), 100);
				String titleIcon = null;
				Scroll portletScroll = Scroll.NoScroll;
				String ds = StringUtil.getValueString(request.getParameter("portletdatasource"), null);
				Reference ref = null;
				PortletDataSource pds = null;
				if (ds != null && ds.trim().length() > 0) {
					String pdsUNID = ds;
					if (ds.startsWith("pds")) {
						pdsUNID = ResourceAliasContext.getInstance().getUNIDByAlias(PortletDataSource.class, ds);
					}
					pds = ResourceContext.getInstance().getResource(pdsUNID, PortletDataSource.class);
					if (pds != null) {
						ref = new Reference();
						ref.setDirectory(ResourceDescriptorConfig.getInstance().getResourceDirectory(PortletDataSource.class));
						ref.setUnid(pds.getUNID());
						ref.setTitle(pds.getName());
					}
				} else {
					throw new RuntimeException("没有提供有效的小窗口数据源！");
				}
				List<Portlet> portlets = portalParser.getColumnPortlets(column);
				if (portlets != null && portlets.size() > 0) row = portlets.size() + 1;

				Portlet pl = new Portlet();
				pl.setName(name);
				pl.setColumn(column);
				pl.setRow(row);
				pl.setDisplayCount(displayCount);
				pl.setHeight(height);
				pl.setShowTitle(showTitle);
				pl.setTitleIcon(titleIcon);
				pl.setPortletScroll(portletScroll);
				pl.setDataSource(ref);
				udfPortal.getPortlets().add(pl);

				// 序列化
				Serializer ser = ResourceSerializerProvider.getResourceSerializer(udfPortal.getClass());
				if (ser == null) ser = new XmlSerializer();
				StringWriter writer = new StringWriter();
				ser.serialize(udfPortal, writer);
				StringBuilder sb = new StringBuilder();
				sb.append("<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n");
				sb.append("<resource type=\"");
				sb.append("com.tansuosoft.discoverx.model.Portal");
				sb.append("\">\r\n");
				sb.append(writer.toString());
				sb.append("</resource>");

				if (!profile.setPortal(sb.toString())) {
					response.setStatus(-1);
					response.setMessage("添加门户小窗口失败！");
				} else {
					PortalParser.reloadInstance(udfPortal);
					response.setStatus(1);
					response.setMessage("添加门户小窗口成功！");
				}
			}

			// 恢复系统缺省的门户内容和样式
			if (type.equalsIgnoreCase("restoretodefaultportal")) {
				if (!profile.setPortal("")) {
					response.setStatus(-1);
					response.setMessage("恢复系统缺省的门户内容和样式失败！");
				} else {
					response.setStatus(1);
					response.setMessage("恢复系统缺省的门户内容和样式成功！");
				}
			}
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		this.setLastAJAXResponse(response);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

