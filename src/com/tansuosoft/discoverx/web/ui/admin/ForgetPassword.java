/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.app.message.Message;
import com.tansuosoft.discoverx.web.app.message.MessageSender;

/**
 * 实现发送密码重置请求消息的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>uid</td>
 * <td>要重置密码的用户的登录帐号，必须。</td>
 * </tr>
 * <tr>
 * <td>fn</td>
 * <td>要重置密码的用户的全名，必须。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要重置密码的用户的UNID，必须。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_SHOW_MESSAGE_AND_CLOSE}</td>
 * <td>通过message属性返回结果文本消息。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ForgetPassword extends Operation {
	/**
	 * 缺省构造器。
	 */
	public ForgetPassword() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = null;
		try {
			request = this.getHttpRequest();
			String uid = request.getParameter("uid");
			String fn = request.getParameter("fn");
			String unid = request.getParameter("unid");
			if (StringUtil.isBlank(uid) && StringUtil.isBlank(fn)) throw new Exception("您必须提供您的登录名和对应的注册用户全名！");
			if (StringUtil.isBlank(unid)) throw new Exception("您提供的登录名和注册用户全名不匹配！");
			Message m = new Message();
			String subject = String.format("来自用户“%1$s”的密码重置请求", StringUtil.isBlank(fn) ? uid : fn);
			m.setSubject(subject);
			String body = String.format("密码重置请求详细信息：<br/>登录名：%1$s<br/>注册全名：%2$s<br/>如果您审核通过，请单击<a href=\"../dispatch?fx=UserPasswordReset&unid=%3$s&notify=1&resulturl=%4$s\" target=\"_blank\">重置密码</a>以帮助用户重置密码。", uid, fn, unid, UrlConfig.getInstance().getUrl(UrlConfig.URLCFGNAME_INFO_MESSAGE));
			m.setBody(body);
			List<Integer> receivers = new ArrayList<Integer>(1);
			receivers.add(1);
			m.setReceivers(receivers);
			MessageSender.sendMessage(m, Session.getSystemSession());
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		this.setLastMessage("密码重置请求发送成功！");
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE);
	}
}

