/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import com.tansuosoft.discoverx.bll.document.DocumentOpener;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowForm;

/**
 * 设置流程流转选项的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>目标文档unid，必须</td>
 * </tr>
 * <tr>
 * <td>wf_option</td>
 * <td>流转选项，参考{@link WorkflowForm#getOption()}中的说明。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>返回{@link AJAXResponse}对应的json对象。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "setWorkflowOption", name = "设置流转选项", desc = "设置流程流转选项的操作")
public class SetWorkflowOption extends Operation {

	/**
	 * 缺省构造器。
	 */
	public SetWorkflowOption() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			String unid = this.getParameterStringFromAll("unid", null);
			if (unid == null || unid.length() == 0) throw new Exception("没有提供有效的目标文档！");
			int option = this.getParameterIntFromAll(Workflow.WORKFLOW_OPTION_PARAMNAME, 0);
			Session s = this.getSession();
			Document doc = (Document) new DocumentOpener().open(unid, Document.class, s);
			if (doc == null) throw new RuntimeException("无法打开目标文档！");
			Parameter p = new Parameter(doc.getUNID());
			p.setName(Workflow.WORKFLOW_OPTION_PARAMNAME);
			p.setValue(option + "");
			p.setDescription("流程流转选项。");
			DBRequest dbr = Instance.newInstance("com.tansuosoft.discoverx.dao.impl.ParameterInserter", DBRequest.class);
			dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, p);
			dbr.sendRequest();
			if (dbr.getResultLong() > 0) {
				doc.setParameter(p);
				this.setLastMessage("流转选项设置成功！");
			} else {
				throw new Exception("流转选项设置失败！");
			}
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

