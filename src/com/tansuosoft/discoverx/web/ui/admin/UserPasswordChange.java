/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 更改用户密码。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要更改密码的用户的UNID，必须。</td>
 * </tr>
 * <tr>
 * <td>oldpassword</td>
 * <td>原始密码，必须。</td>
 * </tr>
 * <tr>
 * <td>newpassword</td>
 * <td>新密码，必须。</td>
 * </tr>
 * <tr>
 * <td>format</td>
 * <td>密码格式，默认为1（表示加密格式），0表示明文格式，可选。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>通过message属性返回结果文本消息。</td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * <strong>只有用户本身才能更改密码！</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class UserPasswordChange extends Operation {
	/**
	 * 缺省构造器。
	 */
	public UserPasswordChange() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		User u = this.getUser();
		HttpServletRequest request = this.getHttpRequest();
		String unid = request.getParameter("unid");
		String oldPassword = StringUtil.getValueString(request.getParameter("oldpassword"), null);
		String newPassword = StringUtil.getValueString(request.getParameter("newpassword"), null);
		int pwdFormat = StringUtil.getValueInt(request.getParameter("format"), 1);
		if (StringUtil.isBlank(unid)) {
			this.setLastError(new FunctionException("未提供用户UNID！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		if (u == null || !unid.equalsIgnoreCase(u.getUNID())) {
			this.setLastError(new FunctionException("必须用户自身登录后能修改自己的登录密码！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		if (oldPassword == null) {
			this.setLastError(new FunctionException("未提供老密码！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		if (pwdFormat == 0) oldPassword = StringUtil.getMD5HashString(oldPassword);
		if (newPassword == null) {
			this.setLastError(new FunctionException("未提供新密码！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		if (pwdFormat == 0) newPassword = StringUtil.getMD5HashString(newPassword);
		try {
			com.tansuosoft.discoverx.dao.impl.UserPasswordChange dbr = new com.tansuosoft.discoverx.dao.impl.UserPasswordChange();
			dbr.setParameter(DBRequest.UNID_PARAM_NAME, unid);
			dbr.setParameter("oldpassword", oldPassword);
			dbr.setParameter("newpassword", newPassword);
			dbr.sendRequest();
			Object result = dbr.getResult();
			if (result != null && "1".equalsIgnoreCase(result.toString())) {
				response.setStatus(1);
				response.setMessage("密码更改成功，下一次登录时请使用新密码！");
				u.setPassword(newPassword);
			} else {
				response.setStatus(-1);
				response.setMessage("密码更改失败，请确认原始密码是否正确，如果还是不能修改密码请联系管理员！");
			}
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}

		this.setLastAJAXResponse(response);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

