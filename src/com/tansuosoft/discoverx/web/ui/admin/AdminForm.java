/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.CommonForm;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 表示获取管理通道首页内容相关请求参数的{@link CommonForm}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AdminForm extends CommonForm {
	/**
	 * 缺省构造器。
	 */
	public AdminForm() {
	}

	private int m_tabIndex = 0; // 标签页索引。

	/**
	 * 返回标签页索引。
	 * 
	 * <p>
	 * 从名为“tab”的http请求参数中获取。
	 * </p>
	 * 
	 * @return int
	 */
	public int getTabIndex() {
		return this.m_tabIndex;
	}

	/**
	 * 设置标签页索引。
	 * 
	 * <p>
	 * 从名为“tab”的http请求参数中获取。
	 * </p>
	 * 
	 * @param tabIndex int
	 */
	public void setTabIndex(int tabIndex) {
		this.m_tabIndex = tabIndex;
	}

	/**
	 * 重载fillWebRequestForm
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.CommonForm#fillWebRequestForm(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public CommonForm fillWebRequestForm(HttpServletRequest request) {
		this.m_tabIndex = StringUtil.getValueInt(request.getParameter("tab"), 0);
		return null;
	}

}

