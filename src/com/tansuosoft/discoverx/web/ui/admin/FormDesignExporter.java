/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemGroup;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 导出表单设计信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class FormDesignExporter extends DesignExporter {

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecial(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecial(Resource r) {
		if (r == null || !(r instanceof Form)) return "";
		Form f = (Form) r;

		StringBuilder sb = new StringBuilder();

		sb.append("<tr><td>默认尺寸度量单位</td><td>").append(f.getMeasurement() == SizeMeasurement.Percentage ? "百分比" : "像素").append("</td></tr>").append("\r\n");
		sb.append("<tr><td>所定义文档扩展样式</td><td>").append(StringUtil.getValueString(f.getStyle(), "")).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>默认字段标题宽度</td><td>").append(f.getItemCaptionWidth()).append("</td></tr>").append("\r\n");
		List<Item> list = f.getItems();
		String tip = (list == null || list.isEmpty() ? "" : "<a href=\"#" + r.getAlias() + "_items\">查看包含的" + list.size() + "个字段</a>");
		sb.append("<tr><td>包含的字段</td><td>").append(list == null || list.isEmpty() ? "<无>" : tip).append("</td></tr>").append("\r\n");
		List<ItemGroup> igs = f.getItemGroups();
		tip = (igs == null || igs.isEmpty() ? "" : "<a href=\"#" + r.getAlias() + "_itemGroups\">查看包含的" + igs.size() + "个字段组</a>");
		sb.append("<tr><td>包含的字段组</td><td>").append(igs == null || igs.isEmpty() ? "<无>" : tip).append("</td></tr>").append("\r\n");

		return sb.toString();
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecialCollection(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecialCollection(Resource r) {
		if (r == null || !(r instanceof Form)) return "";
		Form f = (Form) r;

		StringBuilder sb = new StringBuilder();

		// 包含字段
		List<Item> list = f.getItems();
		if (list != null && list.size() > 0) {
			sb.append(this.exportH2("包含的字段", r.getAlias() + "_items"));
			// sb.append("<fieldset>").append("\r\n");
			// sb.append("<legend>包含的字段</legend>").append("\r\n");
			StringBuilder sba = new StringBuilder();
			ItemDesignExporter exporter = new ItemDesignExporter(f);
			for (Item x : list) {
				if (x == null) continue;
				sb.append("<a href=\"#").append(x.getAlias()).append("_basic\">" + x.getName() + "-" + x.getItemName() + "</a><br/>\r\n");
				sba.append(exporter.export(x));
			}
			sb.append(sba);
			// sb.append("</fieldset>").append("\r\n");
		}

		// 包含字段组
		List<ItemGroup> igs = f.getItemGroups();
		if (igs != null && igs.size() > 0) {
			sb.append(this.exportH2("包含的字段组", r.getAlias() + "_itemGroups"));
			// sb.append("<fieldset>").append("\r\n");
			// sb.append("<legend>包含的字段组</legend>").append("\r\n");
			StringBuilder sba = new StringBuilder();
			ItemDesignExporter exporter = new ItemDesignExporter(f);
			for (ItemGroup x : igs) {
				if (x == null) continue;
				sb.append("<a href=\"#").append(x.getAlias()).append("_basic\">" + x.getName() + "-" + x.getItemName() + "</a><br/>\r\n");
				sba.append(exporter.export(x));
			}
			sb.append(sba);
			// sb.append("</fieldset>").append("\r\n");
		}

		// 事件处理程序
		sb.append(this.exportEventHandlers(f.getEventHandlers(), "文档事件处理程序", r.getAlias() + "_eventHandlers"));

		return sb.toString();
	}

}

