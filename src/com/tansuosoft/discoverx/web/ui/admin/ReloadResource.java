/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceOpener;
import com.tansuosoft.discoverx.bll.ResourceOpenerProvider;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.view.FormViewTable;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Securer;
import com.tansuosoft.discoverx.web.ui.document.ItemsParser;

/**
 * 从数据库或文件中重新加载缓存的资源的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>目标资源unid，必须</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>目标资源目录名，必须</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link #returnRedirectToResource(com.tansuosoft.discoverx.model.Resource)}</td>
 * <td>返回重新打开资源的url地址。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "reloadResource", name = "重新加载资源", desc = "从数据库或文件中重新加载缓存的资源的操作")
public class ReloadResource extends Operation {
	/**
	 * 缺省构造器。
	 */
	public ReloadResource() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		String directory = this.getParameterStringFromAll("directory", ResourceDescriptorConfig.DOCUMENT_DIRECTORY);
		String unid = request.getParameter("unid");
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
		if (rd == null) {
			this.setLastError(new FunctionException("无法获取“" + directory + "”对应的资源描述！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}

		Class<?> clazz = null;
		Resource resource = null;
		ResourceOpener opener = null;
		try {
			if (rd.getCache()) {
				ResourceContext.getInstance().removeObjectFromCache(unid);
			} else if (ResourceDescriptorConfig.DOCUMENT_DIRECTORY.equalsIgnoreCase(directory)) {
				DocumentBuffer.getInstance().removeDocument(unid);
			}
			clazz = Class.forName(rd.getEntity());
			opener = ResourceOpenerProvider.getResourceOpener(clazz);
			resource = opener.open(unid, clazz, this.getSession());
			if (resource instanceof Securer) {
				ParticipantTreeProvider.reload();
			} else if (resource instanceof Form) {
				Form f = (Form) resource;
				FormViewTable.getInstance().checkAndRebuildFormViewTable(f, true);
				ItemsParser.reload(f);
			}
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		if (resource == null) {
			this.setLastError(new FunctionException("无法重载并打开“" + directory + "://" + unid + "”对应的资源！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		return this.returnRedirectToResource(resource);
	}
}

