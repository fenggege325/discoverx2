/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 导出更新的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>dt1</td>
 * <td>要导出的更新文件的修改日期开始值，系统将导出此日期（包含此日期）之后的文件。</td>
 * </tr>
 * <tr>
 * <td>dt2</td>
 * <td>要导出的更新文件的修改日期结束值，系统将导出此日期之前（包含此日期）的文件，如果不提供，则表示dt1指定日期之后修改的所有文件。</td>
 * </tr>
 * <tr>
 * <td>includefiles</td>
 * <td>要额外包含的文件名列表，可使用“?”和“*”通配符。</td>
 * </tr>
 * <tr>
 * <td>excludefiles</td>
 * <td>要剔除的文件名列表，可使用“?”和“*”通配符。</td>
 * </tr>
 * <tr>
 * <td>includeresources</td>
 * <td>是否包含资源更新，默认为false。</td>
 * </tr>
 * <tr>
 * <td>includeaccessories</td>
 * <td>是否包含附加文件更新，默认为false。</td>
 * </tr>
 * <tr>
 * <td>includeapps</td>
 * <td>是否包含所有应用程序更新，默认为false。</td>
 * </tr>
 * <tr>
 * <td>includejava</td>
 * <td>是否包含java运行时文件更新，默认为false。</td>
 * </tr>
 * <tr>
 * <td>includeappsrv</td>
 * <td>是否包含web应用服务器文件更新，默认为false。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回输出zip文件内容的地址，如果出现错误则返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "exportupdate", name = "导出更新包", desc = "导出并下载指定日期之后修改文件的更新包")
public class ExportUpdate extends Operation {
	/**
	 * 缺省构造器。
	 */
	public ExportUpdate() {
		super();
	}

	protected String resourcePath = CommonConfig.getInstance().getResourcePath();
	protected String accessoryPath = CommonConfig.getInstance().getAccessoryPath();
	protected String installPath = CommonConfig.getInstance().getInstallationPath();
	protected String appServerPath = ExportApplication.getAppServerLocation();
	protected String javaHome = System.getProperty("java.home");
	protected Map<String, Boolean> map = new HashMap<String, Boolean>();
	protected String[] includes = null;
	protected String[] excludes = null;
	protected DateTime dt1 = null;
	protected DateTime dt2 = null;
	protected FileFilter ffDefault = null;
	protected boolean resourceFlag = false;
	protected boolean accessoryFlag = false;
	protected boolean appFlag = false;
	protected String framePath = null;

	/**
	 * 文件名通配符文件匹配器。
	 * 
	 * @author coca@tensosoft.com
	 */
	public final class FFName implements FileFilter {
		public FFName(String p, String s, int c) {
			prefix = p;
			if (prefix == null) prefix = "";
			suffix = s;
			if (suffix == null) suffix = "";
			count = c;
		}

		protected String prefix = null;
		protected String suffix = null;
		protected int count = -1;

		@Override
		public boolean accept(File f) {
			String fn = f.getAbsolutePath();
			int lenDif = (fn.length() - prefix.length() - suffix.length());
			return (fn.startsWith(prefix) && fn.endsWith(suffix) && (count == 1 ? lenDif == count : lenDif >= count));
		}
	};

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			User u = this.getUser();
			if (!u.isSystemBuiltinAdmin()) throw new Exception("您没有执行此操作的权限！");
			if (appServerPath != null && appServerPath.length() > 0 && !(appServerPath.endsWith("\\") || appServerPath.endsWith("/"))) {
				appServerPath += File.separator;
			}
			if (javaHome != null && javaHome.length() > 0 && !(javaHome.endsWith("\\") || javaHome.endsWith("/"))) {
				javaHome += File.separator;
			}

			String dt1v = this.getParameterStringFromAll("dt1", null);
			if (dt1v == null) throw new Exception("无法获取要导出的更新文件的修改日期开始值信息！");
			dt1 = new DateTime(dt1v);
			String dt2v = this.getParameterStringFromAll("dt2", null);
			if (dt2v != null && dt2v.length() > 0) dt2 = new DateTime(dt2v);
			resourceFlag = this.getParameterBoolFromAll("includeresources", false);
			accessoryFlag = this.getParameterBoolFromAll("includeaccessories", false);
			appFlag = this.getParameterBoolFromAll("includeapps", false);
			framePath = installPath + "frame";
			boolean javaFlag = this.getParameterBoolFromAll("includejava", false);
			boolean appSrvFlag = this.getParameterBoolFromAll("includeappsrv", false);
			String includeFilesV = this.getParameterStringFromAll("includefiles", null);
			if (!StringUtil.isBlank(includeFilesV)) includes = StringUtil.splitString(includeFilesV, "\r\n");
			int idx = 0;
			if (includes != null) {
				for (String ix : includes) {
					if (ix == null || ix.length() == 0) continue;
					ix = ix.replace('/', File.separatorChar).replace('\\', File.separatorChar);
					if (!ix.startsWith("/") && ix.indexOf(':') < 0) {
						if (ix.startsWith(ExportApplication.JAVA_DIRECTORY_NAME + File.separator)) {
							ix = javaHome + StringUtil.stringRight(ix, File.separator);
						} else if (ix.startsWith(ExportApplication.APPSERVER_DIRECTORY_NAME + File.separator)) {
							ix = appServerPath + StringUtil.stringRight(ix, File.separator);
						} else if (ix.startsWith(ExportApplication.RESOURCE_DIRECTORY_NAME + File.separator)) {
							ix = resourcePath + StringUtil.stringRight(ix, File.separator);
						} else if (ix.startsWith(ExportApplication.ACCESSORY_DIRECTORY_NAME + File.separator)) {
							ix = accessoryPath + StringUtil.stringRight(ix, File.separator);
						} else {
							ix = installPath + ix;
						}
					}
					includes[idx] = ix;
					idx++;
				}
			}
			String excludeFilesV = this.getParameterStringFromAll("excludefiles", null);
			if (!StringUtil.isBlank(excludeFilesV)) excludes = StringUtil.splitString(excludeFilesV, "\r\n");
			if (excludes != null) {
				idx = 0;
				for (String ix : excludes) {
					if (ix == null || ix.length() == 0) continue;
					ix = ix.replace('/', File.separatorChar).replace('\\', File.separatorChar);
					if (!ix.startsWith("/") && ix.indexOf(':') < 0) {
						if (ix.startsWith(ExportApplication.JAVA_DIRECTORY_NAME + File.separator)) {
							ix = javaHome + StringUtil.stringRight(ix, File.separator);
						} else if (ix.startsWith(ExportApplication.APPSERVER_DIRECTORY_NAME + File.separator)) {
							ix = appServerPath + StringUtil.stringRight(ix, File.separator);
						} else if (ix.startsWith(ExportApplication.RESOURCE_DIRECTORY_NAME + File.separator)) {
							ix = resourcePath + StringUtil.stringRight(ix, File.separator);
						} else if (ix.startsWith(ExportApplication.ACCESSORY_DIRECTORY_NAME + File.separator)) {
							ix = accessoryPath + StringUtil.stringRight(ix, File.separator);
						} else {
							ix = installPath + ix;
						}
					}
					excludes[idx] = ix;
					idx++;
				}
			}

			ffDefault = new FileFilter() {
				@Override
				public boolean accept(File f) {
					if (f.isHidden()) return false;
					String n = f.getName();
					if (n.endsWith(".svn") || n.endsWith(".cvs")) return false;
					String fn = f.getAbsolutePath();
					if (!appFlag && fn.startsWith(framePath) && f.isDirectory() && n.startsWith("app")) return false;
					if (!resourceFlag && fn.startsWith(resourcePath)) return false;
					if (!accessoryFlag && fn.startsWith(accessoryPath)) return false;
					if (f.isDirectory()) return true;
					long lm = f.lastModified();
					return (lm >= dt1.getTimeMillis() && (dt2 == null ? true : (lm <= dt2.getTimeMillis())));
				}
			};

			List<String> files = new ArrayList<String>();
			findFiles(installPath, files);
			if (resourceFlag) findFiles(resourcePath, files);
			if (accessoryFlag) findFiles(accessoryPath, files);
			if (javaFlag) findFiles(javaHome, files);
			if (appSrvFlag) findFiles(appServerPath, files);
			if (includes != null) {
				for (String s : includes) {
					if (s == null || s.length() == 0) continue;
					if (s.endsWith(File.separator)) {
						findFiles(s, files);
					} else {
						int pos = -1;
						if ((pos = s.lastIndexOf('?')) > 0 || (pos < 0 && (pos = s.lastIndexOf('*')) > 0)) {
							FFName ff = new FFName(s.substring(0, pos), s.substring(pos + 1), s.lastIndexOf('?') > 0 ? 1 : 0);
							findFiles(StringUtil.stringLeftBack(s, File.separator), files, ff, false);
						} else if (!map.containsKey(s) && !isExcluded(new File(s))) {
							files.add(s);
							map.put(s, true);
						}
					}
				}
			}
			if (files.isEmpty()) throw new Exception("没有找到符合条件的文件！");
			String target = this.buildZip(files);
			if (target == null || target.length() == 0) throw new Exception("无法生成更新包文件！");
			this.setLastMessage(target);
			OperationResult result = new OperationResult();
			result.setResultUrl("download?fn=" + target);
			return result;
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE);
		}
	}

	/**
	 * 检查指定文件名是否要被排除。
	 * 
	 * @param fn
	 * @return boolean
	 */
	protected boolean isExcluded(File f) {
		if (excludes == null || excludes.length == 0) return false;
		if (f == null || !f.exists()) return true;
		String fn = f.getAbsolutePath();
		for (String s : excludes) {
			if (s == null || s.length() == 0) continue;
			if (s.endsWith(File.separator)) {
				if ((f.isFile() && fn.startsWith(s)) || (f.isDirectory() && s.equalsIgnoreCase(fn + File.separator))) return true;
			} else {
				int pos = -1;
				if ((pos = s.lastIndexOf('?')) > 0 || (pos < 0 && (pos = s.lastIndexOf('*')) > 0)) {
					String prefix = s.substring(0, pos);
					String suffix = s.substring(pos + 1);
					int count = (s.lastIndexOf('?') > 0 ? 1 : 0);
					int lenDif = (fn.length() - prefix.length() - suffix.length());
					if (fn.startsWith(prefix) && fn.endsWith(suffix) && (count == 1 ? lenDif == count : lenDif >= count)) return true;
				} else {
					if (fn.equalsIgnoreCase(s)) return true;
				}
			}
		}
		return false;
	}

	/**
	 * 生产压缩包并返回其文件名。
	 * 
	 * @param files
	 * @return String
	 * @throws IOException
	 */
	protected String buildZip(List<String> files) throws IOException {
		String tempFolder = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath();
		File p = new File(tempFolder);
		p.mkdirs();
		String result = String.format("%1$s.zip", new DateTime().toString("yyyyMMddHHmm"));
		String target = String.format("%1$s%2$s", tempFolder, result);
		byte[] buf = new byte[1024];
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(target));
		String entryName = null;
		for (String s : files) {
			if (s.startsWith(resourcePath)) {
				entryName = ExportApplication.RESOURCE_DIRECTORY_NAME + File.separator + s.replace(resourcePath, "");
			} else if (s.startsWith(accessoryPath)) {
				entryName = ExportApplication.ACCESSORY_DIRECTORY_NAME + File.separator + s.replace(accessoryPath, "");
			} else if (s.startsWith(installPath)) {
				entryName = s.replace(installPath, "");
			} else if (s.startsWith(javaHome)) {
				entryName = ExportApplication.JAVA_DIRECTORY_NAME + File.separator + s.replace(javaHome, "");
			} else if (s.startsWith(appServerPath)) {
				entryName = ExportApplication.APPSERVER_DIRECTORY_NAME + File.separator + s.replace(appServerPath, "");
			} else {
				entryName = StringUtil.stringRightBack(s, File.separator);
			}
			File f = new File(s);
			if (!f.exists() || !f.isFile() || f.length() == 0) continue;
			ZipEntry entry = new ZipEntry(entryName);
			out.putNextEntry(entry);
			FileInputStream in = new FileInputStream(s);
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

			out.closeEntry();
			in.close();
		}
		out.close();
		return result;
	}

	/**
	 * 添加指定目录下符合条件的文件到文件列表。
	 * 
	 * @param dir
	 * @param files
	 */
	protected void findFiles(String dir, List<String> files) {
		findFiles(dir, files, ffDefault, true);
	}

	/**
	 * 添加指定目录下符合条件的文件到文件列表。
	 * 
	 * @param dir
	 * @param files
	 * @param ff 文件过滤器
	 * @param recurse 是否递归查找。
	 */
	protected void findFiles(String dir, List<String> files, FileFilter ff, boolean recurse) {
		File d = new File(dir);
		if (!d.exists() || !d.isDirectory()) return;

		File[] fs = d.listFiles(ff);
		if (fs == null) return;
		for (File f : fs) {
			String af = f.getAbsolutePath();
			if (f.isFile() && !map.containsKey(af) && !isExcluded(f)) {
				files.add(af);
				map.put(af, true);
			}
			if (f.isDirectory() && recurse) addDirectoryFiles(f, files, ff);
		}
	}

	/**
	 * 递归添加目录及其子目录下的文件到文件列表。
	 * 
	 * @param dir
	 * @param files
	 * @param ff 文件过滤器
	 */
	protected void addDirectoryFiles(File dir, List<String> files, FileFilter ff) {
		if (dir == null || !dir.isDirectory() || dir.getName().startsWith(".svn") || dir.getName().startsWith(".cvs")) return;
		File fs[] = dir.listFiles(ff);
		if (fs != null && fs.length > 0) {
			for (File f : fs) {
				String af = f.getAbsolutePath();
				if (f.isFile() && !map.containsKey(af) && !isExcluded(f)) {
					files.add(af);
					map.put(af, true);
				}
				if (f.isDirectory()) addDirectoryFiles(f, files, ff);
			}
		}
	}
}// class end

