/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.Crypt;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UriUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 校验腾硕网站注册用户相关操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>sid</td>
 * <td>腾硕网站注册账号和对应密码信息，如果提供则说明是第一次登录，否则为返回自动登录信息。</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class ValidateAccount extends Operation {

	/**
	 * 缺省构造器。
	 */
	public ValidateAccount() {
		super();
	}

	/**
	 * 重载：实现功能。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = null;
		AJAXResponse ajax = new AJAXResponse();
		try {
			request = this.getHttpRequest();
			if (request == null) throw new RuntimeException("无法获取Http请求！");

			String tsidPath = CommonConfig.getInstance().getInstallationPath() + "WEB-INF" + File.separator + "classes" + File.separator + "tsid.dat";
			File f = new File(tsidPath);
			String sid = request.getParameter("sid");
			String tsid = null;
			boolean err = false;
			if (sid != null && sid.length() > 0) {
				AJAXResponse result = validateTSWAccount(sid);
				if (result == null) {
					err = true;
					tsid = "无法验证您的账号，请联系提供商以解决此问题！";
				}
				tsid = result.getMessage();
				if (result.getStatus() == 0) {
					if (tsid != null && tsid.length() > 0) {
						BufferedWriter bw = null;
						try {
							bw = new BufferedWriter(new FileWriter(f));
							bw.write(result.getMessage());
						} finally {
							if (bw != null) bw.close();
						}
					}
				} else {
					err = true;
				}
			}
			StringBuilder sbTsid = new StringBuilder();
			if ((tsid == null || tsid.length() == 0) && f.exists() && f.isFile() && f.length() > 0) {
				BufferedReader input = new BufferedReader(new FileReader(f));
				String line = null;
				try {
					while ((line = input.readLine()) != null) {
						sbTsid.append(line);
					}
				} finally {
					input.close();
				}
				tsid = sbTsid.toString();
			}
			String[] result = (!err && tsid != null && tsid.length() > 0 ? unwrapAccount(tsid) : null);
			if (result != null && result.length > 0) {
				ajax.setMessage(result[0]);
				ajax.setStatus(0);
				this.setLastAJAXResponse(ajax);
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			} else {
				ajax.setMessage(tsid == null ? "" : tsid);
				ajax.setStatus(-1);
				this.setLastAJAXResponse(ajax);
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			}
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
	}

	/**
	 * 校验sid包含的账号密码是否在腾硕网站有登录。
	 * 
	 * @param sid
	 * @return AJAXResponse
	 * @throws Exception
	 */
	protected static AJAXResponse validateTSWAccount(String sid) throws Exception {
		if (sid == null || sid.length() == 0) return null;
		String add = "http://www.tensosoft.com/va.jsp";
		OutputStreamWriter wr = null;
		BufferedReader rd = null;
		AJAXResponse result = null;
		try {
			// 请求参数准备
			String data = UriUtil.encodeURIComponent("sid") + "=" + UriUtil.encodeURIComponent(sid.indexOf(':') > 0 ? StringUtil.toHexString(new Crypt("11235813").encrypt(sid.getBytes("utf-8"))) : sid);
			data += "&" + UriUtil.encodeURIComponent("lci") + "=" + UriUtil.encodeURIComponent("free");

			// 发送请求
			URL url = new URL(add);
			URLConnection conn = url.openConnection();
			conn.setUseCaches(false);
			conn.setDoOutput(true);
			conn.setAllowUserInteraction(true);
			CookieHandler.setDefault(new CookieManager());
			wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();

			// 获取请求结果
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null;
			StringBuilder response = new StringBuilder();
			while ((line = rd.readLine()) != null) {
				response.append(line);
			}

			// 解析请求结果
			String r = response.toString();
			if (r.length() == 0) return null;
			int posStatus = r.indexOf("status");
			int posMessage = r.indexOf("message");
			if (posStatus < 0 || posMessage < 0) return null;
			char c = 0;
			boolean append = false;
			StringBuilder sbStatus = new StringBuilder();
			StringBuilder sbMessage = new StringBuilder();
			for (int i = posStatus + 6; i < r.length(); i++) {
				c = r.charAt(i);
				if (c == ':') {
					append = true;
					continue;
				}
				if (c == ',' || c == '}') break;
				if (append == true) sbStatus.append(c);
			}
			append = false;
			for (int i = posMessage + 7; i < r.length(); i++) {
				c = r.charAt(i);
				if (!append && (c == '\'' || c == '"') && r.charAt(i - 1) != '\\') {
					append = true;
					continue;
				}
				if (c == ',' || c == '}') break;
				if ((c == '\'' || c == '"') && r.charAt(i - 1) != '\\') break;
				if (append == true) sbMessage.append(c);
			}
			result = new AJAXResponse();
			result.setMessage(sbMessage.toString().trim());
			result.setStatus(StringUtil.getValueInt(sbStatus.toString().trim(), -1));
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (wr != null) wr.close();
				if (rd != null) rd.close();
			} catch (IOException e) {
				FileLogger.error(e);
			}
		}
		return result;
	}

	/**
	 * 解包账号信息以供使用。
	 * 
	 * @param accountInfo
	 * @return String[]，返回结果依次包含用户unid、用户登录账号、用户密码、用户注册时间四个值。
	 */
	public static String[] unwrapAccount(String accountInfo) {
		if (accountInfo == null || accountInfo.length() <= 16) return null;
		try {
			String keyHex = accountInfo.substring(0, 16);
			String encryptedHex = accountInfo.substring(16);

			byte[] key = StringUtil.fromHexString(keyHex);
			for (int i = 0; i < key.length; i += 2) {
				key[i + 1] ^= key[i];
			}
			keyHex = StringUtil.toHexString(key);
			StringBuilder encryptKey = new StringBuilder();
			for (int i = 0; i < keyHex.length(); i += 2) {
				encryptKey.append(keyHex.charAt(i));
			}

			Crypt c = null;
			String decrypted = null;

			c = new Crypt(encryptKey.toString());
			byte[] bsDecrypted = c.decrypt(StringUtil.fromHexString(encryptedHex));
			decrypted = new String(bsDecrypted, "utf-8");
			if (decrypted != null && decrypted.length() > 0 && decrypted.indexOf("`") > 0) { return StringUtil.splitString(decrypted, '`'); }
			return null;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("校验时出现异常，错误码：1！");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("校验时出现异常，错误码：9！");
		}
	}

	//
	// public static void main(String[] args) {
	// String uid = "test测试 13";
	// String pwd = "1af d++}\\23";
	// String sid = wrapSid(uid, pwd);
	// System.out.println(sid);
	// StringPair sp = unwrapSid(sid);
	// if (sp != null && uid.equals(sp.getKey()) && pwd.equals(sp.getValue())) {
	// System.out.println("good");
	// }
	// User u = new User();
	// System.out.println(u.getUNID());
	// String aid = wrapAccount(uid, pwd, u);
	// System.out.println(aid);
	// String[] unwraps = unwrapAccount(aid);
	// if (unwraps != null && uid.equals(unwraps[1]) && pwd.equals(unwraps[2])) {
	// System.out.println(unwraps[0] + "," + unwraps[3]);
	// System.out.println("good");
	// }
	// }
}

