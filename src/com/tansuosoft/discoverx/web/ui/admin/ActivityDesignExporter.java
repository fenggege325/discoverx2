/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.ActivityType;
import com.tansuosoft.discoverx.workflow.Automation;
import com.tansuosoft.discoverx.workflow.Participation;
import com.tansuosoft.discoverx.workflow.Transition;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowHelper;

/**
 * 导出流程环节设计信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class ActivityDesignExporter extends DesignExporter {
	private Workflow m_wf = null;
	private WorkflowHelper m_wfh = null;

	/**
	 * 接收所属流程的构造器。
	 * 
	 * @param wf
	 */
	public ActivityDesignExporter(Workflow wf) {
		m_wf = wf;
		m_wfh = new WorkflowHelper(m_wf);
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecial(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecial(Resource r) {
		if (r == null || !(r instanceof Activity)) return "";
		Activity a = (Activity) r;

		StringBuilder sb = new StringBuilder();

		ActivityType at = a.getActivityType();
		String att = "";
		if (at == ActivityType.Begin) {
			att = "开始节点";
		} else if (at == ActivityType.End) {
			att = "结束节点";
		} else if (at == ActivityType.Branch) {
			att = "分支节点";
		} else if (at == ActivityType.Normal) {
			att = "普通节点";
		}
		sb.append("<tr><td>节点类型</td><td>").append(att).append("</td></tr>").append("\r\n");

		String pt = processParticipation(a.getParticipation());
		sb.append("<tr><td>审批方式</td><td>").append(pt).append("</td></tr>").append("\r\n");

		sb.append("<tr><td>是否启用环节转接</td><td>").append(a.getCommutatorSupport() ? "是" : "否").append("</td></tr>").append("\r\n");
		if (a.getCommutatorSupport()) sb.append("<tr><td>环节转接人</td><td>").append(this.processParticipant(a.getCommutator())).append("</td></tr>").append("\r\n");

		List<Participant> ps = a.getParticipants();
		if (ps != null && ps.size() > 0) {
			sb.append("<tr><td>环节参与者范围</td><td>");
			for (Participant p : ps) {
				if (p == null) continue;
				sb.append(this.processParticipant(p)).append("<br/>").append("\r\n");
			}
			sb.append("</td></tr>").append("\r\n");
		}

		sb.append("<tr><td>位置坐标(x,y)</td><td>").append(a.getLeft()).append(",").append(a.getTop()).append("</td></tr>").append("\r\n");
		return sb.toString();
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecialCollection(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecialCollection(Resource r) {
		if (r == null || !(r instanceof Activity)) return "";
		Activity a = (Activity) r;

		StringBuilder sb = new StringBuilder();

		// 自动流转选项
		List<Automation> as = a.getAutomations();
		if (as != null && as.size() > 0) {
			for (Automation x : as) {
				if (x == null) continue;
				sb.append(this.exportH2("自动流状选项", r.getAlias() + "_automations"));
				sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
				sb.append("<colgroup><col width=\"38%\"/><col/></colgroup>").append("\r\n");
				sb.append("<tr class=\"head\"><td>属性名</td><td>属性值</td></tr>").append("\r\n");
				sb.append("<tr><td>启用表达式</td><td>").append(StringUtil.getValueString(x.getCondition(), "")).append("</td></tr>").append("\r\n");
				String wfid = x.getWorkflow();
				Workflow wft = ResourceContext.getInstance().getResource(wfid, Workflow.class);
				sb.append("<tr><td>目标流程</td><td>").append(wft != null ? wft.getName() : StringUtil.getValueString(wfid, m_wf.getName())).append("</td></tr>").append("\r\n");
				String aid = x.getActivity();
				if (wft == null) wft = m_wf;
				WorkflowHelper wfh = new WorkflowHelper(wft);
				Activity ax = null;
				if (aid != null) ax = wfh.getActivity(aid);
				String axn = (ax == null ? "<未指定>" : ax.getName());
				sb.append("<tr><td>目标环节</td><td>").append(axn).append("</td></tr>").append("\r\n");
				sb.append("<tr><td>审批方式</td><td>").append(processParticipation(x.getParticipation().getIntValue())).append("</td></tr>").append("\r\n");
				List<Participant> ps = x.getParticipants();
				if (ps != null && ps.size() > 0) {
					sb.append("<tr><td>自动流转目标参与者</td><td>");
					for (Participant p : ps) {
						if (p == null) continue;
						sb.append(this.processParticipant(p)).append("<br/>").append("\r\n");
					}
					sb.append("</td></tr>").append("\r\n");
				}
				sb.append("</table>").append("\r\n");
			}
		}

		// 环节出口
		List<Transition> ts = a.getTransitions();
		if (ts != null && ts.size() > 0) {
			sb.append(this.exportH2("环节出口", r.getAlias() + "_transitions"));
			sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
			sb.append("<colgroup><col width=\"30%\"/><col width=\"60%\"/><col/></colgroup>").append("\r\n");
			sb.append("<tr class=\"head\"><td>目标环节</td><td>条件表达式</td><td>排序号</td></tr>").append("\r\n");
			for (Transition x : ts) {
				if (x == null) continue;
				sb.append("<tr>");
				sb.append("<td>");
				String unid = x.getUNID();
				Activity ax = m_wfh.getActivity(unid);
				String t = (ax == null ? StringUtil.getValueString(x.getTitle(), "") : ax.getName());
				sb.append(t);
				sb.append("</td>");
				sb.append("<td>");
				sb.append(this.processExpression(x.getCondition()));
				sb.append("</td>");
				sb.append("<td>");
				sb.append(x.getSort());
				sb.append("</td>");
				sb.append("</tr>").append("\r\n");
			}
			sb.append("</table>").append("\r\n");
		}

		// 环节操作
		sb.append(this.exportOperations(a.getOperations(), "环节操作", r.getAlias() + "_operations"));

		return sb.toString();
	}

	/**
	 * 处理审批方式。
	 * 
	 * @param participation
	 * @return String
	 */
	protected static String processParticipation(int participation) {
		int p = participation;
		String pt = "";
		if (p == 0) {
			pt += ((pt.length() == 0 ? "" : ",") + "自定义");
		}
		if ((p & Participation.Parallel.getIntValue()) == Participation.Parallel.getIntValue()) {
			pt += ((pt.length() == 0 ? "" : ",") + "多人并行");
		}
		if ((p & Participation.Series.getIntValue()) == Participation.Series.getIntValue()) {
			pt += ((pt.length() == 0 ? "" : ",") + "多人顺序");
		}
		if ((p & Participation.Priority.getIntValue()) == Participation.Priority.getIntValue()) {
			pt += ((pt.length() == 0 ? "" : ",") + "优先审批");
		}
		if ((p & Participation.Single.getIntValue()) == Participation.Single.getIntValue()) {
			pt += ((pt.length() == 0 ? "" : ",") + "单人审批");
		}
		return pt;
	}
}

