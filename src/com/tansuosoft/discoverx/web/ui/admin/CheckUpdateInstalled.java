/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.common.license.Version;
import com.tansuosoft.discoverx.util.StringPair;

/**
 * 检查更新包是否已经安装过的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>pid</td>
 * <td>要检查的更新包id，可以允许多值，必须</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_CONTENT}</td>
 * <td>返回每个更新包id对应的更新包安装状态。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "checkUpdateInstalled", name = "检查更新包安装状态", desc = "检查更新包安装状态并返回校验结果json文本")
public class CheckUpdateInstalled extends Operation {
	/**
	 * 缺省构造器。
	 */
	public CheckUpdateInstalled() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			HttpServletRequest request = this.getHttpRequest();
			String pids[] = request.getParameterValues("pid");
			if (pids == null || pids.length == 0) throw new Exception("没有提供有效的待检查更新包id。");
			Version v = Version.getInstance();
			List<StringPair> ps = v.getPatchs();
			StringBuilder sb = new StringBuilder();
			String dt = null;
			for (String pid : pids) {
				if (pid == null || pid.length() == 0) continue;
				sb.append(sb.length() == 0 ? "" : ",").append("\r\n{");
				sb.append("pid:'").append(pid).append("'");
				boolean installed = false;
				if (ps != null && ps.size() > 0) {
					for (StringPair sp : ps) {
						if (sp != null && sp.getKey() != null && sp.getKey().startsWith(pid)) {
							installed = true;
							dt = sp.getValue();
							break;
						}
					}
				}
				sb.append(",installed:").append(installed ? "true" : "false");
				if (installed) sb.append(",dt:'").append(dt).append("'");
				sb.append("}");
			}
			if (sb.length() > 0) {
				sb.insert(0, "result:[");
				sb.append("\r\n]");
			}
			this.setLastMessage(sb.toString());
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_CONTENT);
	}
}

