/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 清理缓存的资源的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>key</td>
 * <td>资源唯一id，必须。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回提示结果消息并关闭窗口的地址。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "clearResourceCache", name = "清理资源缓存", desc = "清理资源缓存以便重新加载资源。")
public class ClearResourceCache extends Operation {
	/**
	 * 缺省构造器。
	 */
	public ClearResourceCache() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		try {
			User u = this.getUser();
			if (!u.isSystemBuiltinAdmin()) throw new Exception("您没有执行此操作的权限！");
			String key = request.getParameter("key");
			if (key == null || key.trim().length() == 0) throw new Exception("没有提供有效的唯一ID！");
			Object obj = null;
			if (StringUtil.isUNID(key)) {
				obj = ResourceContext.getInstance().removeObjectFromCache(key);
				if (obj == null) {
					obj = DocumentBuffer.getInstance().removeDocument(key);
				}
			} else if (key.indexOf('.') > 0) {
				obj = (Config.reload(key) ? Boolean.TRUE : null);
			}
			this.setLastMessage(obj != null ? "缓存已清理。" : "指定的资源没有在缓存中。");
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE);
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE);
		}
	}
}

