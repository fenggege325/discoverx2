/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Transition;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 用于输出文档流转日志图的{@link HtmlRender}实现类。
 * 
 * @author coca@tensosoft.com
 */
public class WorkflowChart implements HtmlRender {
	private Document m_document = null;

	/**
	 * 接收目标文档对象的构造器。
	 * 
	 * @param doc
	 */
	public WorkflowChart(Document doc) {
		m_document = doc;
	}

	private static final String EMPTY_STRING = "";

	/**
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		WorkflowRuntime wfr = null;
		StringBuilder sb = new StringBuilder();
		try {
			if (m_document == null || jspContext == null) return EMPTY_STRING;
			wfr = WorkflowRuntime.getInstance(m_document, jspContext.getUserSession());
			Workflow wf = wfr.getCurrentWorkflow();
			if (wf == null) return EMPTY_STRING;
			List<Activity> list = wf.getActivities();
			if (list == null || list.size() < 2) return EMPTY_STRING;
			sb.append("id:").append("'").append(wf.getUNID()).append("'").append("\r\n");
			sb.append(",").append("name:").append("'").append(StringUtil.encode4Json(wf.getName())).append("'").append("\r\n");
			sb.append(",").append("alias:").append("'").append(wf.getAlias()).append("'").append("\r\n");
			sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(wf.getDescription())).append("'").append("\r\n");

			List<WFData> allwfdata = wfr.getWFData();
			Map<String, String> map = new HashMap<String, String>();
			Map<String, List<WFData>> actReaders = new HashMap<String, List<WFData>>();
			List<WFData> readers = null;
			if (allwfdata != null) {
				WFData lasta = null;
				WFData a = null;
				for (int i = (allwfdata.size() - 1); i >= 0; i--) {
					a = allwfdata.get(i);
					if (a == null) continue;
					// 环节变换
					if (lasta != null && !a.getActivity().equalsIgnoreCase(lasta.getActivity())) {
						map.put(a.getUNID(), lasta.getActivity());
					}
					lasta = a;

					if (a.checkLevel(WFDataLevel.Reader)) {
						readers = actReaders.get(a.getActivity());
						if (readers == null) {
							readers = new ArrayList<WFData>();
							actReaders.put(a.getActivity(), readers);
						}
						readers.add(a);
					}// if end
				}// for end
			}// if end

			ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
			ParticipantTree pt = null;
			int actIdx = 0;
			int wfdIdx = 0;
			int tIdx = 0;
			int activityTraversalCount = 0;
			String startdt = null;
			String enddt = null;
			List<WFData> wfdata = null;
			Activity current = wfr.getCurrentActivity();
			List<Transition> transitions = null;
			DateTime expect = null;
			DateTime created = null;
			DateTime done = null;
			long lall = 0L;
			long lalla = 0L;
			boolean hasReadersTraversals = false;
			sb.append(",").append("activities:").append("[");
			for (Activity x : list) {
				if (x == null) continue;
				lalla = 0L;
				if (actIdx > 0) sb.append(",");
				sb.append("\r\n{").append("\r\n");
				sb.append("id:").append("'").append(x.getUNID()).append("'").append("\r\n");
				sb.append(",").append("name:").append("'").append(StringUtil.encode4Json(x.getName())).append("'").append("\r\n");
				sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(x.getDescription())).append("'").append("\r\n");
				sb.append(",").append("x:").append(x.getLeft()).append("\r\n");
				sb.append(",").append("y:").append(x.getTop()).append("\r\n");
				sb.append(",").append("current:").append(x.getUNID().equalsIgnoreCase(current.getUNID()) ? "true" : "false").append("\r\n");
				transitions = x.getTransitions();
				if (transitions != null && transitions.size() > 0) {
					sb.append(",").append("transitions:").append("[");
					tIdx = 0;
					for (Transition t : transitions) {
						if (t == null) continue;
						sb.append(tIdx == 0 ? "" : ",").append("'").append(t.getUNID()).append("'");
						tIdx++;
					}
					sb.append("]").append("\r\n"); // transitions
				}

				activityTraversalCount = wfr.getActivityTraversalCount(x.getUNID());
				readers = actReaders.get(x.getUNID());
				hasReadersTraversals = (readers != null && readers.size() > 0);
				if (hasReadersTraversals) activityTraversalCount += 1;
				if (activityTraversalCount > 0) {
					sb.append(",").append("traversals:").append("[");
					for (int i = 0; i < activityTraversalCount; i++) {
						String from = null;
						boolean isLastReaders = false;
						if (hasReadersTraversals && i == (activityTraversalCount - 1)) {
							startdt = "";
							enddt = "";
							wfdata = readers;
							isLastReaders = true;
						} else {
							startdt = wfr.getActivityStartDateTime(x.getUNID(), i);
							enddt = wfr.getActivityEndDateTime(x.getUNID(), i);
							wfdata = wfr.getWFData(x.getUNID(), i);
						}
						sb.append(i == 0 ? "" : ",").append("\r\n{").append("\r\n");
						sb.append("start:").append("'").append(startdt).append("'").append("\r\n");
						sb.append(",").append("end:").append("'").append(enddt).append("'").append("\r\n");
						if (wfdata != null && wfdata.size() > 0) {
							sb.append(",").append("data:").append("[");
							wfdIdx = 0;
							for (WFData y : wfdata) {
								if (y == null) continue;
								if (!isLastReaders && y.checkLevel(WFDataLevel.Reader)) continue;
								if (!(y.checkLevel(WFDataLevel.Reader) || y.checkLevel(WFDataLevel.Approver))) continue;
								if (map.containsKey(y.getUNID())) from = map.get(y.getUNID());
								pt = ptp.getParticipantTree(y.getData());
								sb.append(wfdIdx == 0 ? "" : ",").append("\r\n{").append("\r\n");
								sb.append("user:").append("'").append(pt == null ? y.getData() : StringUtil.encode4Json(pt.getName())).append("'").append("\r\n");
								sb.append(",").append("start:").append("'").append(y.getCreated()).append("'").append("\r\n");
								sb.append(",").append("end:").append("'").append(y.getDone() ? y.getAccomplished() : "").append("'").append("\r\n");
								if (y.getDone()) {
									done = new DateTime(y.getAccomplished());
									created = new DateTime(y.getCreated());
									expect = StringUtil.isBlank(y.getExpect()) ? null : new DateTime(y.getExpect());
									long l = done.getTimeMillis() - created.getTimeMillis();
									sb.append(",").append("timeSpent:").append("'").append(getTimeRange(l)).append("'").append("\r\n");
									if (expect != null) {
										l = expect.getTimeMillis() - done.getTimeMillis();
										sb.append(",").append("efficiency:").append("'").append(l == 0 ? "0" : l > 0 ? "" : "-").append(l != 0 ? getTimeRange(l) : "").append("'").append("\r\n");
									}
								}
								sb.append(",").append("expect:").append("'").append(StringUtil.getValueString(y.getExpect(), "")).append("'").append("\r\n");
								sb.append(",").append("level:").append(y.getLevel()).append("\r\n");
								sb.append(",").append("subLevel:").append(y.getSubLevel()).append("\r\n");
								sb.append("}"); // data
								wfdIdx++;
							}
							sb.append("]").append("\r\n"); // data arr
						}
						if (isLastReaders) sb.append(",").append("readerTraversal:true").append("\r\n");
						if (from != null && !isLastReaders) sb.append(",").append("from:").append("'").append(from).append("'").append("\r\n");
						if (!StringUtil.isBlank(enddt)) {
							long l = new DateTime(enddt).getTimeMillis() - new DateTime(startdt).getTimeMillis();
							lall += l;
							lalla += l;
							sb.append(",").append("timeSpent:").append("'").append(getTimeRange(l)).append("'").append("\r\n");
						}
						sb.append("}"); // traversal
					}
					sb.append("]").append("\r\n"); // traversals
				}
				if (lalla > 0) sb.append(",").append("timeSpent:").append("'").append(getTimeRange(lalla)).append("'").append("\r\n");
				sb.append("}"); // activity
				actIdx++;
			}
			sb.append("]").append("\r\n"); // activities
			if (lall > 0) sb.append(",").append("timeSpent:").append("'").append(getTimeRange(lall)).append("'").append("\r\n");
		} catch (Exception ex) {
			FileLogger.error(ex);
			return "error:true,message:'" + StringUtil.encode4Json(ex.getMessage()) + "'";
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		return sb.toString();
	}

	/**
	 * 计算millis指定的毫秒数对应的时间跨度。
	 * 
	 * @param millis
	 * @return String 以“[[[[d天]h小时]m分]s秒]”的格式返回。
	 */
	protected String getTimeRange(long millis) {
		long l = (millis < 0 ? -millis : millis);
		if (l == 0) return "";
		long ls[] = { 0, 0, 0, 0 };
		long s = l / 1000;
		long m = s / 60;
		s = s % 60;
		ls[3] = s;
		long h = m / 60;
		m = m % 60;
		ls[2] = m;
		long d = h / 24;
		h = h % 24;
		ls[1] = h;
		ls[0] = d;
		String lts[] = { "天", "小时", "分", "秒" };
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ls.length; i++) {
			if (ls[i] > 0) sb.append(ls[i]).append(lts[i]);
		}
		return sb.toString();
	}
}
