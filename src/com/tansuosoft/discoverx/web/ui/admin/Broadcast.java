/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.web.app.tsim.NotificationType;
import com.tansuosoft.discoverx.web.app.tsim.Notifier;

/**
 * 广播系统消息的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>broadcastmsg</td>
 * <td>消息内容，必须。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回提示结果消息并关闭窗口的地址。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "broadcast", name = "广播消息", desc = "向所有登录用户对应的小助手广播消息。")
public class Broadcast extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Broadcast() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		try {
			User u = this.getUser();
			if (u == null || u.isAnonymous()) throw new Exception("您没有广播消息的权限！");
			String msg = request.getParameter("broadcastmsg");
			if (msg == null || msg.trim().length() == 0) throw new Exception("没有提供有效消息内容！");
			if (msg.length() > 500) throw new Exception("不能广播超过500个字符的消息！");
			Notifier.broadcastNotification(u.getCN() + "广播：" + msg, getSession(), NotificationType.SYSTEMBROADCASE);
			this.setLastMessage("消息已发送。");
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE);
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE);
		}
	}
}

