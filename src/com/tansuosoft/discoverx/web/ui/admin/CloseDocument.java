/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.OperationResult;

/**
 * 强行关闭指定文档的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要关闭文档的UNID，必须。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link Operation#returnResult(String, com.tansuosoft.discoverx.model.Resource, OperationResult)}</td>
 * <td></td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class CloseDocument extends Operation {
	/**
	 * 缺省构造器。
	 */
	public CloseDocument() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		OperationResult or = null;
		try {
			HttpServletRequest request = this.getHttpRequest();
			String unid = request.getParameter("unid");
			if (unid == null || unid.length() == 0) throw new RuntimeException("未提供UNID！");
			Document doc = DocumentBuffer.getInstance().removeDocument(unid);
			or = this.returnResult((doc == null ? "文档没有在缓存中打开！" : "文档被成功关闭！"), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE));
		} catch (Exception ex) {
			this.setLastMessage(ex.getMessage());
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		return or;
	}
}

