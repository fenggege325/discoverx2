/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.bll.ResourceOpener;
import com.tansuosoft.discoverx.bll.ResourceOpenerProvider;
import com.tansuosoft.discoverx.bll.ResourceSerializerProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Dictionary;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.OpinionType;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.Serializer;
import com.tansuosoft.discoverx.util.serialization.XmlSerializer;
import com.tansuosoft.discoverx.web.operation.Workflow;

/**
 * 导出完整应用程序的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要导出的应用程序资源的unid，必须。通过http请求提供参数值。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回输出zip文件内容的地址，如果出现错误则返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ExportApplication extends Operation {
	/**
	 * 更新包中包含待更新的资源文件的目录名：resources
	 */
	public static final String RESOURCE_DIRECTORY_NAME = "resources";
	/**
	 * 更新包中包含待更新的资源文件的目录名：files
	 */
	public static final String ACCESSORY_DIRECTORY_NAME = "files";
	/**
	 * 更新包中包含待更新的java运行时文件的目录名：java
	 */
	public static final String JAVA_DIRECTORY_NAME = "java";
	/**
	 * 更新包中包含待更新的web应用服务器运行时文件的目录名：appserver
	 */
	public static final String APPSERVER_DIRECTORY_NAME = "appserver";
	/**
	 * 更新包中包含待更新的数据库资源文件的目录名：dbresources
	 */
	public static final String DB_RESOURCE_DIRECTORY_NAME = "dbresources";

	/**
	 * 缺省构造器。
	 */
	public ExportApplication() {
		super();
	}

	private String frame = null;
	protected static final String DB_RES_PATH = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath() + DB_RESOURCE_DIRECTORY_NAME + File.separator;

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		ResourceOpener opener = null;
		Application app = null;

		Session ss = this.getSession();
		User u = Session.getUser(ss);
		if (u == null || !(SecurityHelper.checkUserRole(u, Role.ROLE_ADMIN_SC) || SecurityHelper.checkUserRole(u, Role.ROLE_SETTINGSADMIN_SC))) {
			FunctionException ex = new FunctionException("您没有导出应用程序包的权限！");
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		String unid = this.getParameterStringFromAll("unid", null);
		if (unid == null) {
			FunctionException ex = new FunctionException("无法获取资源UNID！");
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		Class<?> clazz = Application.class;
		opener = ResourceOpenerProvider.getResourceOpener(clazz);

		try {
			app = (Application) opener.open(unid, clazz, ss);
			if (app == null) {
				FunctionException ex = new FunctionException("您没有导出应用程序包的权限！");
				this.setLastError(ex);
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			}
			Portal portal = Profile.getInstance(this.getUser()).getPortal();
			if (portal == null) portal = ResourceContext.getInstance().getDefaultResource(Portal.class);
			frame = this.getParameterStringFromAll("frame", StringUtil.getValueString((portal == null ? null : portal.getFrame()), "frame"));
			String tempFolder = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath();
			File p = new File(tempFolder);
			p.mkdirs();
			String target = String.format("%1$s%2$s.zip", tempFolder, app.getUNID());

			String resourcePath = CommonConfig.getInstance().getResourcePath();
			String accessoryPath = CommonConfig.getInstance().getAccessoryPath();
			String installPath = CommonConfig.getInstance().getInstallationPath();
			String appServerLocation = getAppServerLocation();
			if (appServerLocation != null && appServerLocation.length() > 0 && !(appServerLocation.endsWith("\\") || appServerLocation.endsWith("/"))) {
				appServerLocation += File.separator;
			}
			String javaHome = System.getProperty("java.home");
			if (javaHome != null && javaHome.length() > 0 && !(javaHome.endsWith("\\") || javaHome.endsWith("/"))) {
				javaHome += File.separator;
			}

			List<String> files = new ArrayList<String>();
			findResourceFiles(app, files);
			findProgramFiles(app, frame, files);
			findParamsSpecifiedFiles(app, files);
			if (files.size() > 0) {
				byte[] buf = new byte[1024];
				ZipOutputStream out = new ZipOutputStream(new FileOutputStream(target));
				String entryName = null;
				for (String s : files) {
					if (s.startsWith(DB_RES_PATH)) {
						entryName = DB_RESOURCE_DIRECTORY_NAME + File.separator + s.replace(DB_RES_PATH, "");
					} else if (s.startsWith(resourcePath)) {
						entryName = RESOURCE_DIRECTORY_NAME + File.separator + s.replace(resourcePath, "");
					} else if (s.startsWith(accessoryPath)) {
						entryName = ACCESSORY_DIRECTORY_NAME + File.separator + s.replace(accessoryPath, "");
					} else if (s.startsWith(installPath)) {
						entryName = s.replace(installPath, "");
					} else if (s.startsWith(javaHome)) {
						entryName = JAVA_DIRECTORY_NAME + File.separator + s.replace(javaHome, "");
					} else if (s.startsWith(appServerLocation)) {
						entryName = APPSERVER_DIRECTORY_NAME + File.separator + s.replace(appServerLocation, "");
					} else {
						entryName = StringUtil.stringRightBack(s, File.separator);
					}
					File f = new File(s);
					if (!f.exists() || !f.isFile() || f.length() == 0) continue;
					ZipEntry entry = new ZipEntry(entryName);
					out.putNextEntry(entry);
					FileInputStream in = new FileInputStream(s);
					int len;
					while ((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
					}

					out.closeEntry();
					in.close();
				}
				out.close();
			}
			this.setLastMessage(target);
			OperationResult result = new OperationResult();
			result.setResultUrl(request.getContextPath() + "/download?fn=" + target.replace(tempFolder, ""));
			if (m_tmpDBResFiles.size() > 0) {
				for (File fx : m_tmpDBResFiles) {
					if (fx == null || !fx.exists() || fx.isFile()) continue;
					FileHelper.deleteFolder(fx.getAbsolutePath());
				}
			}
			return result;
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
	}

	/**
	 * 为指定应用程序导出所有关联的表单、视图、流程、下级应用程序文件列表。
	 * 
	 * @param app
	 * @param files
	 */
	protected void findResourceFiles(Application app, List<String> files) {
		if (app == null) return;
		ResourceDescriptorConfig rdc = ResourceDescriptorConfig.getInstance();
		String formDir = rdc.getResourceDirectory(Form.class);
		String wfDir = rdc.getResourceDirectory(Workflow.class);
		String viewDir = rdc.getResourceDirectory(View.class);

		String fn = ResourceHelper.getXmlLocationPath(app);
		if (fn != null && fn.length() > 0) files.add(fn);

		// form
		String formUNID = app.getForm();
		fn = ResourceHelper.getXmlLocationPath(formDir, formUNID);
		if (fn != null && fn.length() > 0) files.add(fn);

		// profileform
		String pformUNID = app.getProfileForm();
		fn = ResourceHelper.getXmlLocationPath(formDir, pformUNID);
		if (fn != null && fn.length() > 0) files.add(fn);

		// workflow
		String wfUNID = app.getWorkflow();
		fn = ResourceHelper.getXmlLocationPath(wfDir, wfUNID);
		if (fn != null && fn.length() > 0) files.add(fn);

		// views
		List<Reference> refs = app.getViews();
		if (refs != null && refs.size() > 0) {
			for (Reference f : refs) {
				if (f == null) continue;
				fn = ResourceHelper.getXmlLocationPath(viewDir, f.getUnid());
				if (fn != null && fn.length() > 0) files.add(fn);
			}
		}

		// accessories
		List<Accessory> accs = app.getAccessories();
		if (accs != null && accs.size() > 0) {
			for (Accessory acc : accs) {
				if (acc == null) continue;
				fn = AccessoryPathHelper.getAbsoluteAccessoryServerPath(app) + acc.getFileName();
				if (fn != null && fn.length() > 0) files.add(fn);
			}
		}

		// subApplications
		refs = app.getSubApplications();
		if (refs != null && refs.size() > 0) {
			Application subapp = null;
			for (Reference f : refs) {
				if (f == null) continue;
				subapp = ResourceContext.getInstance().getResource(f.getUnid(), Application.class);
				if (subapp == null) continue;
				findResourceFiles(subapp, files);
				findProgramFiles(subapp, frame, files);
				findParamsSpecifiedFiles(subapp, files);
			}
		}

		// Function
		addOtherResources(com.tansuosoft.discoverx.model.Function.class, app, null, files);

		// Dictionary
		Class<Dictionary> clsDict = Dictionary.class;
		try {
			Method belongMethod = clsDict.getMethod("getBelong");
			if (belongMethod != null) addOtherResources(Dictionary.class, app, belongMethod, files);
		} catch (Exception e) {
			FileLogger.error(e);
		}

		// AccessoryType
		addOtherResources(AccessoryType.class, app, null, files);

		// OpinionType
		addOtherResources(OpinionType.class, app, null, files);

		// 群组和角色
		addGroupsAndRoles(app, files);
	}

	protected List<File> m_tmpDBResFiles = new ArrayList<File>(); // 所有动态生成的属于此应用的群组和角色等数据库资源文件和路径

	/**
	 * 添加群组和角色。
	 * 
	 * @param app
	 * @param files
	 */
	private void addGroupsAndRoles(final Application app, List<String> files) {
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper r = new SQLWrapper();
				r.setSql("select 'group',c_unid from t_group where c_punid='" + app.getUNID() + "' union select 'role',c_unid from t_role where c_punid='" + app.getUNID() + "'");
				return r;
			}
		};
		dbr.setResultBuilder(new ResultBuilder() {
			@Override
			public Object build(DBRequest request, Object rawResult) {
				if (rawResult == null || !(rawResult instanceof DataReader)) return null;
				DataReader dr = (DataReader) rawResult;
				ResourceContext rc = ResourceContext.getInstance();
				Resource r = null;

				try {
					while (dr.next()) {
						String dir = dr.getString(1);
						String unid = dr.getString(2);
						r = rc.getResource(unid, dir);
						if (r == null) continue;
						String fullDir = DB_RES_PATH + dir + File.separator;
						File d = new File(fullDir);
						d.mkdirs();
						m_tmpDBResFiles.add(d);
						File f = new File(fullDir + unid + ".xml");
						if (f.exists()) f.delete();
						FileWriter fw = new FileWriter(f);
						fw.write("<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n");
						fw.write("<resource type=\"");
						fw.write(r.getClass().getName());
						fw.write("\">\r\n");
						Serializer ser = ResourceSerializerProvider.getResourceSerializer(r.getClass());
						if (ser == null) ser = new XmlSerializer();
						StringWriter writer = new StringWriter();
						ser.serialize(r, writer);
						fw.write(writer.toString());
						fw.write("</resource>");
						fw.flush();
						fw.close();
						m_tmpDBResFiles.add(f);
					}
				} catch (Exception ex) {
					FileLogger.error(ex);
				}
				return null;
			}
		});
		dbr.sendRequest();
		if (m_tmpDBResFiles.size() > 0) {
			for (File fx : m_tmpDBResFiles) {
				if (fx == null || !fx.exists() || fx.isDirectory()) continue;
				files.add(fx.getAbsolutePath());
			}
		}
	}

	/**
	 * 追加其它属于特定应用程序的资源。
	 * 
	 * @param clazz
	 * @param app
	 * @param files
	 */
	private void addOtherResources(Class<? extends Resource> clazz, Application app, Method m, List<String> files) {
		if (clazz == null || app == null) return;
		try {
			List<Resource> resources = XmlResourceLister.getResources(clazz);
			ResourceDescriptorConfig rdc = ResourceDescriptorConfig.getInstance();
			String appUNID = app.getUNID();
			if (resources != null && resources.size() > 0) {
				String dir = rdc.getResourceDirectory(clazz);
				Object mv = null;
				for (Resource r : resources) {
					if (r == null) continue;
					if (m == null) {
						if (!appUNID.equalsIgnoreCase(r.getPUNID())) continue;
					} else {
						mv = m.invoke(r);
						if (mv == null || !appUNID.equalsIgnoreCase(mv.toString())) continue;
					}
					String fn = ResourceHelper.getXmlLocationPath(dir, r.getUNID());
					files.add(fn);
				}
			}
		} catch (Exception e) {
			FileLogger.error(e);
		}
	}

	/**
	 * 为指定应用程序导出所有关联的特定功能目录下的文件和jar文件列表。
	 * 
	 * <p>
	 * 以当前用户使用的页面框架作为默认框架。
	 * </p>
	 * 
	 * @param app
	 * @param frame
	 * @param files
	 */
	protected void findProgramFiles(Application app, String frame, List<String> files) {
		if (app == null) return;
		String alias = app.getAlias();
		if (alias == null || alias.length() == 0) return;
		alias = alias.toLowerCase();
		String installPath = CommonConfig.getInstance().getInstallationPath();
		String specialDir = String.format("%1$s%2$s%3$s%4$s%5$s", installPath, frame, File.separator, alias, File.separator);
		File sdir = new File(specialDir);
		if (sdir.exists() && sdir.isDirectory()) {
			File fs[] = sdir.listFiles();
			if (fs != null && fs.length > 0) {
				for (File f : fs) {
					if (f.isFile()) files.add(f.getAbsolutePath());
					if (f.isDirectory()) addDirectoryFiles(f, files);
				}
			}
		}
		String jarFile = String.format("%1$sWEB-INF%2$slib%3$s%4$s.jar", installPath, File.separator, File.separator, alias);
		File jf = new File(jarFile);
		if (jf.exists() && jf.isFile()) files.add(jarFile);
	}

	private static final String PARAM_SPECIFIED_EXPORT_FILE_NAME_PREFIX = "exportdesignfile";

	/**
	 * 为指定应用程序导出所有通过额外参数指定的文件列表。
	 * 
	 * <p>
	 * 参数名符合“exportdesignfile[n]”（其中[n]为顺序增长的文件序号数字）的参数值指定的文件将被导出。<br/>
	 * 参数值所指的文件除非是在web应用程序安装目录下（此时可以配置为相对路径），否则必须配置为完整绝对路径。
	 * </p>
	 * 
	 * @param app
	 * @param files
	 */
	protected void findParamsSpecifiedFiles(Application app, List<String> files) {
		if (app == null) return;
		List<Parameter> list = app.getParameters();
		if (list == null || list.isEmpty()) return;
		final String installationPath = CommonConfig.getInstance().getInstallationPath();
		for (Parameter p : list) {
			if (p == null) continue;
			String n = p.getName();
			if (n == null || !n.toLowerCase().startsWith(PARAM_SPECIFIED_EXPORT_FILE_NAME_PREFIX)) continue;
			String v = p.getValue();
			if (v == null || v.length() == 0) continue;
			if (!(v.startsWith("/") || v.indexOf(':') > 0)) v = installationPath + v;
			File f = new File(v);
			if (!f.exists() || !f.isFile()) continue;
			files.add(f.getAbsolutePath());
		}
	}

	/**
	 * 递归添加目录及其子目录下的文件到文件列表。
	 * 
	 * @param dir
	 * @param files
	 */
	protected static void addDirectoryFiles(File dir, List<String> files) {
		if (dir == null || !dir.isDirectory() || dir.getName().startsWith(".svn") || dir.getName().startsWith(".cvs")) return;
		File fs[] = dir.listFiles();
		if (fs != null && fs.length > 0) {
			for (File f : fs) {
				if (f.isFile()) files.add(f.getAbsolutePath());
				if (f.isDirectory()) addDirectoryFiles(f, files);
			}
		}
	}

	/**
	 * 返回web应用服务器安装路径，先从名为“DISCOVERX_APPSRV_PATH”的环境变量中获取如果取不到则从名为“catalina.home”的系统属性中获取（只针对Tomcat服务器）。
	 * 
	 * @return String 如果没有有效结果，可能返回空指针或空字符串。
	 */
	protected static String getAppServerLocation() {
		String r = System.getenv("DISCOVERX_APPSRV_PATH");
		if (r == null || r.length() == 0) r = System.getProperty("catalina.home");
		return r;
	}

}// class end

