/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.model.Organization;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.Browser;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出供管理通道使用的用户资源TreeView对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class UserTreeViewRender implements HtmlRender {
	/**
	 * 采用异步模式获取用户参与者的最少参与者数。
	 */
	protected int m_asyncFetchMinParticipantCount = 0;
	private int m_participantCount = 0;
	private int m_waitForFetchDataIndex = 0;
	private String m_nonPersonTviLeafIcon = null;
	private String m_loadingTviLeafIcon = null;
	protected static final String AsyncLabel = "请稍候，正在获取包含的用户...";
	private boolean m_branchFlag = false;

	/**
	 * 缺省构造器。
	 */
	public UserTreeViewRender() {
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		m_asyncFetchMinParticipantCount = (jspContext.getBrowser().check(Browser.MSIE, 6) ? 618 : 1000);
		PathContext pathContext = new PathContext((HttpServletRequest) jspContext.getRequest());
		m_nonPersonTviLeafIcon = "tvfc.gif";
		m_loadingTviLeafIcon = pathContext.getThemeImagesPath() + "loading.gif";
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		m_participantCount = ptp.getParticipantCount();
		ParticipantTree root = ptp.getRoot();
		ServletRequest request = jspContext.getRequest();
		int f = (request == null ? -1 : StringUtil.getValueInt(request.getParameter("f"), -1));
		if (f >= 0) {
			ParticipantTree pt = ParticipantTreeProvider.getInstance().getParticipantTree(f);
			if (pt != null) {
				root = pt;
				m_branchFlag = true;
			}
		}
		StringBuilder sb = new StringBuilder();
		Organization orgroot = ResourceContext.getInstance().getResource(OrganizationsContext.getInstance().getContextUserOrgUnid(), Organization.class);
		if (!SecurityHelper.authorize(jspContext.getLoginUser(), orgroot, SecurityLevel.View)) {
			sb.append("id:'tverror',error:true,label:'对不起，您没有查看组织结构和用户信息的权限！'");
			return sb.toString();
		}
		if (m_branchFlag || m_participantCount <= m_asyncFetchMinParticipantCount) {
			this.renderTreeView(root, sb);
		} else {
			this.renderTreeViewAsync(root, sb);
		}
		return sb.toString();
	}

	/**
	 * 输出管理通道用户维护树。
	 * 
	 * @param pt
	 * @param sb
	 */
	private void renderTreeView(ParticipantTree pt, StringBuilder sb) {
		if (pt == null) return;
		boolean isPerson = (pt.getParticipantType() == ParticipantType.Person);
		boolean rootFlag = (sb.length() == 0);
		if (!rootFlag) sb.append("{"); // treeview begin
		sb.append("id:").append("'").append(pt.getUNID()).append("'");
		sb.append(",").append("columns:[");
		if (rootFlag) {
			sb.append("'安全编码'");
			sb.append(",").append("'登录账号|代码'");
			sb.append(",").append("'排序号'");
		} else {
			sb.append("'").append(pt.getSecurityCode()).append("'");
			sb.append(",").append("'").append(StringUtil.encode4Json(pt.getAlias())).append("'");
			sb.append(",").append("'").append(pt.getSort()).append("'");
		}
		sb.append("]");
		String lbl = StringUtil.encode4Json(ParticipantHelper.getFormatValue(pt, "cn"));
		sb.append(",").append("label:").append("'").append(lbl).append("'");
		sb.append(",").append("desc:").append("'").append(lbl).append("'");
		sb.append(",").append("data:{"); // data begin
		sb.append("alias:").append("'").append(StringUtil.encode4Json(pt.getAlias())).append("'");
		sb.append(",").append("participantType:").append(pt.getParticipantType());
		sb.append(",").append("level:").append(pt.getLevel());
		sb.append(",").append("securityCode:").append(pt.getSecurityCode());
		sb.append(",").append("name:").append("'").append(StringUtil.encode4Json(pt.getName())).append("'");
		sb.append(",").append("sort:").append(pt.getSort());
		sb.append("}"); // data end
		List<ParticipantTree> list = pt.getChildren();
		if (list != null) {
			sb.append(",").append("children:").append("["); // children begin
			int idx = 0;
			for (ParticipantTree x : list) {
				if (x == null || x.getSecurityCode() == ParticipantTree.GROUP_ROOT_CODE || x.getSecurityCode() == ParticipantTree.ROLE_ROOT_CODE) continue;
				if (m_branchFlag && x.getParticipantType() != ParticipantType.Person) continue;
				if (idx > 0) sb.append(",\r\n");
				this.renderTreeView(x, sb);
				idx++;
			}
			sb.append("]"); // children end
		}
		if (!isPerson) {
			sb.append(",").append("leafIcon:'").append(m_nonPersonTviLeafIcon).append("'");
		}

		if (!rootFlag) sb.append("}"); // treeview end

		return;
	}

	/**
	 * 输出管理通道组织树以便以异步方式获取其下的用户。
	 * 
	 * @param pt
	 * @param sb
	 */
	private void renderTreeViewAsync(ParticipantTree pt, StringBuilder sb) {
		if (pt == null) return;
		boolean isPerson = (pt.getParticipantType() == ParticipantType.Person);
		if (isPerson) return;
		boolean rootFlag = (sb.length() == 0);
		if (!rootFlag) sb.append("{"); // treeview begin
		sb.append("id:").append("'").append(pt.getUNID()).append("'");
		sb.append(",").append("columns:[");
		if (rootFlag) {
			sb.append("'安全编码'");
			sb.append(",").append("'登录账号|代码'");
			sb.append(",").append("'排序号'");
		} else {
			sb.append("'").append(pt.getSecurityCode()).append("'");
			sb.append(",").append("'").append(StringUtil.encode4Json(pt.getAlias())).append("'");
			sb.append(",").append("'").append(pt.getSort()).append("'");
		}
		sb.append("]");
		String lbl = StringUtil.encode4Json(ParticipantHelper.getFormatValue(pt, "cn"));
		sb.append(",").append("label:").append("'").append(lbl).append("'");
		sb.append(",").append("desc:").append("'").append(lbl).append("'");
		sb.append(",").append("data:{"); // data begin
		sb.append("alias:").append("'").append(StringUtil.encode4Json(pt.getAlias())).append("'");
		sb.append(",").append("participantType:").append(pt.getParticipantType());
		sb.append(",").append("level:").append(pt.getLevel());
		sb.append(",").append("securityCode:").append(pt.getSecurityCode());
		sb.append(",").append("name:").append("'").append(StringUtil.encode4Json(pt.getName())).append("'");
		sb.append(",").append("sort:").append(pt.getSort());
		sb.append("}"); // data end
		List<ParticipantTree> list = pt.getChildren();
		if (list != null) {
			sb.append(",").append("children:").append("["); // children begin
			boolean asyncRenderFlag = false;
			int idx = 0;
			for (ParticipantTree x : list) {
				if (x == null || x.getSecurityCode() == ParticipantTree.GROUP_ROOT_CODE || x.getSecurityCode() == ParticipantTree.ROLE_ROOT_CODE) continue;
				if (!asyncRenderFlag) {
					sb.append("{");
					sb.append("id:").append("'waitforfetchdata_").append(m_waitForFetchDataIndex++).append("'");
					sb.append(",").append("label:").append("'").append(AsyncLabel).append("'");
					sb.append(",").append("selectable:false");
					sb.append(",").append("leafIcon:'").append(m_loadingTviLeafIcon).append("'");
					sb.append("}");
					asyncRenderFlag = true;
					idx++;
				}
				if (x.getParticipantType() != ParticipantType.Person) {
					if (idx > 0) sb.append(",\r\n");
					this.renderTreeViewAsync(x, sb);
					idx++;
				}
			}
			sb.append("]"); // children end
			sb.append(",").append("ajaxChildren:true");
		}
		if (!isPerson) {
			sb.append(",").append("leafIcon:'").append(m_nonPersonTviLeafIcon).append("'");
		}

		if (!rootFlag) sb.append("}"); // treeview end

		return;
	}
}

