/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.util.Crypt;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 发送重置密码请求。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>uid</td>
 * <td>账号名，必须。</td>
 * </tr>
 * <tr>
 * <td>email</td>
 * <td>邮件地址，必须。</td>
 * </tr>
 * <tr>
 * <td>verifycode</td>
 * <td>验证码，必须。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link UrlConfig#URLCFGNAME_INFO_MESSAGE}对应的url结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class ResetPassword extends Operation {

	/**
	 * 重载：实现功能。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			HttpServletRequest request = this.getHttpRequest();
			if (!UserRegister.verifyCode(request, true)) throw new Exception("对不起，您没有提供有效的验证码！");
			final String uid = request.getParameter("uid");
			if (uid == null || uid.isEmpty()) throw new Exception("对不起，您没有提供有效的登录账号！");
			final String email = request.getParameter("email");
			if (email == null || email.isEmpty()) throw new Exception("对不起，您没有提供有效的邮件地址！");
			DBRequest dbr = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper r = new SQLWrapper();
					r.setSql("select c_unid from t_user where c_alias=?");
					r.setRequestType(RequestType.Scalar);
					r.setParameterized(true);
					return r;
				}
			};
			dbr.setParametersSetter(new ParametersSetter() {
				@Override
				public void setParameters(DBRequest request, CommandWrapper cw) {
					try {
						cw.setString(1, uid);
					} catch (SQLException e) {
						FileLogger.error(e);
					}
				}
			});
			dbr.sendRequest();
			final String unid = dbr.getResult(String.class);
			if (unid == null || unid.isEmpty()) throw new Exception("您的登录账号不存在！");
			final String r = sendMail(uid, email);
			if (r != null && r.length() > 0) {
				dbr = new DBRequest() {
					@Override
					protected SQLWrapper buildSQL() {
						SQLWrapper r = new SQLWrapper();
						r.setSql("update t_user set c_description=? where c_unid=?");
						r.setRequestType(RequestType.NonQuery);
						r.setParameterized(true);
						return r;
					}
				};
				dbr.setParametersSetter(new ParametersSetter() {
					@Override
					public void setParameters(DBRequest request, CommandWrapper cw) {
						try {
							cw.setString(1, r);
							cw.setString(2, unid);
						} catch (SQLException e) {
							FileLogger.error(e);
						}
					}
				});
				dbr.sendRequest();
				if (dbr.getResultLong() <= 0) throw new Exception("无法处理重置密码请求，请联系我们帮您解决此问题！");
			}
		} catch (Exception ex) {
			if (ex instanceof NullPointerException) FileLogger.error(ex);
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		OperationResult r = new OperationResult();
		this.setLastMessage("您的密码重置请求邮件已发出！");
		r.setResultUrl("login.jsp");
		return r;
	}

	private static final String _CONTENT_HTML = "text/html; charset=GBK";
	private static final String _PROXY = "腾硕客服";
	private static final String _FROM = "service@tensosoft.com";
	private static final String _SMTP = "smtp.exmail.qq.com";
	private static final String _PWD = "112358";
	private static final String _PORT = "25";
	private static final String _ENCKEY = "11235813";

	/**
	 * 发送密码重置请求邮件。
	 * 
	 * @param uid
	 * @param email
	 * @return String
	 */
	protected static String sendMail(String uid, String email) {
		try {
			// smtp地址
			Properties props = new Properties();
			props.put("mail.smtp.host", _SMTP);
			props.put("mail.smtp.port", _PORT);
			props.put("mail.smtp.auth", "true");

			// 获取smtp会话
			Authenticator auth = new SMTPAuthenticator(_FROM, _PWD);
			javax.mail.Session mailSession = javax.mail.Session.getDefaultInstance(props, auth);
			mailSession.setDebug(false);

			// 创建MimeMessage
			MimeMessage msg = new MimeMessage(mailSession);

			// 设置发送人和接收人
			InternetAddress addressFrom = new InternetAddress(_FROM);
			msg.setFrom(addressFrom);

			InternetAddress[] addressTo = new InternetAddress[1];
			addressTo[0] = new InternetAddress(email);
			msg.setRecipients(javax.mail.Message.RecipientType.TO, addressTo);

			// 可选：添加额外邮件头信息
			msg.addHeader("PROXY", _PROXY);
			String subject = "腾硕协作应用社区密码重置请求";
			long dt = System.currentTimeMillis();
			String x = new Crypt(_ENCKEY).encryptToHexString(dt + ":" + uid);
			String link = "http://www.tensosoft.com/resetpassword.jsp?x=" + x;
			String message = "您因忘记在腾硕云办公系统的密码而提交了密码重置请求，请您单击以下链接重置密码：<br/><a href=\"" + link + "\">" + link + "</a><br/>如果您无法直接打开此链接，您可以复制链接地址到浏览器地址栏以打开链接。<br/>如果您没有发送任何密码重置请求，请忽略此邮件并报告此情况给我们。<br/>此为系统根据您的请求自动发送的邮件，请勿直接回复！";
			msg.setSubject(subject);
			// 内容
			msg.setContent(message, _CONTENT_HTML);
			msg.setSentDate(new Date());
			Transport.send(msg);
			return dt + "";
		} catch (Exception ex) {
			if (ex instanceof java.net.ConnectException) {
				FileLogger.debug("密码重置请求SMTP连接异常:%1$s", ex.getMessage());
			} else {
				FileLogger.error(ex);
			}
		}
		return null;
	}

	/**
	 * 通过smtp发送邮件时用于提供登录验证信息的{@link javax.mail.Authenticator}实现类。
	 * 
	 * @author coca@tansuosoft.cn
	 */
	private static class SMTPAuthenticator extends javax.mail.Authenticator {
		/**
		 * 接收用户名和密码的构造器。
		 * 
		 * @param uid
		 * @param pwd。
		 */
		public SMTPAuthenticator(String uid, String pwd) {
			this.uid = uid;
			this.pwd = pwd;
		}

		private String uid = null;
		private String pwd = null;

		/**
		 * 重载getPasswordAuthentication
		 * 
		 * @see javax.mail.Authenticator#getPasswordAuthentication()
		 */
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(uid, pwd);
		}
	}

	/**
	 * 解码请求。
	 * 
	 * @param x
	 * @return 返回要重置密码的用户账号。
	 * @throws Exception
	 */
	public static String decode(String x) throws Exception {
		if (x == null || x.isEmpty()) new Exception("执行了一个非法的操作请求(代码:0)！");
		String raw = null;
		try {
			raw = new Crypt(_ENCKEY).decryptFromHexString(x);
		} catch (Exception e) {
			throw new Exception("执行了一个非法的操作请求(代码:1)！");
		}
		if (raw == null || raw.isEmpty()) throw new RuntimeException("执行了一个非法的操作请求(代码:2)！");
		long l = StringUtil.getValueLong(StringUtil.stringLeft(raw, ":"), 0L);
		final String uid = StringUtil.getValueString(StringUtil.stringRight(raw, ":"), null);
		if (l <= 0 || uid == null || uid.length() == 0) throw new RuntimeException("执行了一个非法的操作请求(代码:3)！");
		return uid;
	}

	/**
	 * 重置密码。
	 * 
	 * @param jspContext
	 * @return 返回重置后的新密码。
	 * @throws Exception
	 */
	public static String reset(JSPContext jspContext) throws Exception {
		String confirmPassword = null;
		try {
			HttpServletRequest r = (HttpServletRequest) jspContext.getRequest();
			if (!UserRegister.verifyCode(r, true)) throw new Exception("对不起，您没有提供有效的验证码！");

			final String password = r.getParameter("pid");
			confirmPassword = r.getParameter("pid");
			if (password == null || password.isEmpty()) throw new RuntimeException("对不起，您必须提供一个新密码！");
			if (!password.equals(confirmPassword)) throw new RuntimeException("对不起，您输入的新密码和确认密码不匹配！");
			final String uid = r.getParameter("uid");
			DBRequest dbr = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper r = new SQLWrapper();
					r.setSql("select c_unid,c_description from t_user where c_alias=?");
					r.setParameterized(true);
					return r;
				}
			};
			dbr.setParametersSetter(new ParametersSetter() {
				@Override
				public void setParameters(DBRequest request, CommandWrapper cw) {
					try {
						cw.setString(1, uid);
					} catch (SQLException e) {
						FileLogger.error(e);
					}
				}
			});
			dbr.setResultBuilder(new ResultBuilder() {
				@Override
				public Object build(DBRequest request, Object rawResult) {
					if (rawResult == null || !(rawResult instanceof DataReader)) return null;
					DataReader dr = (DataReader) rawResult;
					StringPair sp = null;
					try {
						while (dr.next()) {
							sp = new StringPair(dr.getString(1), dr.getString(2));
							break;
						}
					} catch (SQLException e) {
						FileLogger.error(e);
					}
					return sp;
				}
			});
			dbr.sendRequest();
			final StringPair sp = dbr.getResult(StringPair.class);
			if (sp == null || StringUtil.isBlank(sp.getKey()) || StringUtil.isBlank(sp.getValue())) throw new Exception("您没有提交过密码重置申请！");
			dbr = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper result = new SQLWrapper();
					result.setSql("update t_user set c_password=?,c_description=? where c_unid=?");
					return result;
				}
			};
			dbr.setParametersSetter(new ParametersSetter() {
				@Override
				public void setParameters(DBRequest request, CommandWrapper cw) {
					try {
						cw.setString(1, StringUtil.getMD5HashString(password));
						cw.setString(2, "");
						cw.setString(3, sp.getKey());
					} catch (SQLException e) {
						FileLogger.error(e);
					}
				}
			});
			dbr.sendRequest();
			if (dbr.getResultLong() <= 0) throw new RuntimeException("无法设置您的新密码，请联系提供商帮您解决此问题！");
		} catch (Exception ex) {
			throw ex;
		}
		return confirmPassword;
	}

	public static void main(String args[]) {
		try {
			long dt = System.currentTimeMillis();
			String x = new Crypt(_ENCKEY).encryptToHexString(dt + ":" + "coca@21cn.com");
			System.out.println(x);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
