/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceConstructor;
import com.tansuosoft.discoverx.bll.ResourceConstructorProvider;
import com.tansuosoft.discoverx.bll.ResourceInserter;
import com.tansuosoft.discoverx.bll.ResourceInserterProvider;
import com.tansuosoft.discoverx.bll.ResourceUpdater;
import com.tansuosoft.discoverx.bll.ResourceUpdaterProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.ActivityType;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 为指定应用程序创建一个空白流程的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>app</td>
 * <td>要为其创建空白流程的应用程序UNID，必须。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>通过message属性返回创建的流程的UNID。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class NewApplicationWorkflow extends Operation {
	/**
	 * 缺省构造器。
	 */
	public NewApplicationWorkflow() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		HttpServletRequest request = this.getHttpRequest();
		String appUnid = request.getParameter("app");
		if (StringUtil.isBlank(appUnid)) {
			this.setLastError(new FunctionException("未提供必要的应用程序UNID参数！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}

		Application app = (Application) ResourceContext.getInstance().getResource(appUnid, Application.class);
		if (app == null) {
			this.setLastError(new FunctionException("无法获取“" + appUnid + "”对应的应用程序资源！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}

		Workflow wf = null;
		Session s = this.getSession();
		try {
			String dir = ResourceDescriptorConfig.getInstance().getResourceDirectory(Workflow.class);
			ResourceConstructor rc = ResourceConstructorProvider.getResourceConstructor(dir);
			wf = (Workflow) rc.construct(dir, s, null, appUnid, null);
			if (wf == null) {
				this.setLastError(new FunctionException("无法为应用程序“" + app.getName() + "”创建流程，请检查是否有足够权限！"));
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			}
			String alias = app.getAlias();
			if (alias != null && alias.toLowerCase().startsWith("app")) alias = alias.substring(3);
			wf.setAlias("wf" + alias);
			wf.setName(app.getName() + "流程");
			wf.setPUNID(app.getUNID());
			Activity activity = new Activity();
			activity.setName("开始");
			activity.setAlias("wn0");
			activity.setDescription("开始环节。");
			activity.setActivityType(ActivityType.Begin);
			activity.setOperations(new ArrayList<com.tansuosoft.discoverx.model.Operation>());
			com.tansuosoft.discoverx.model.Operation operation = new com.tansuosoft.discoverx.model.Operation();
			operation.setTitle("发送");
			operation.setExpression("@wfdone()");
			operation.setIcon("icon_right.gif");
			operation.setSort(100);
			activity.getOperations().add(operation);
			wf.addActivity(activity);
			ResourceInserter inserter = ResourceInserterProvider.getResourceInserter(wf);
			inserter.insert(wf, s);
			app.setWorkflow(wf.getUNID());
			ResourceUpdater updater = ResourceUpdaterProvider.getResourceUpdater(app);
			updater.update(app, s);
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}

		response.setMessage(wf.getUNID());
		this.setLastAJAXResponse(response);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

