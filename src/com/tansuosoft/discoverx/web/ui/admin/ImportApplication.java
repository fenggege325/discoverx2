/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.ResourceOpener;
import com.tansuosoft.discoverx.bll.ResourceOpenerProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;

/**
 * 导入完整应用程序的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>fn</td>
 * <td>要导入的应用程序资源的文件名参数，参数值必须以“{应用程序的unid}.zip”作为文件名。<br/>
 * 需先上传成功相应的zip文件后才能正常执行。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ImportApplication extends Operation {
	/**
	 * 缺省构造器。
	 */
	public ImportApplication() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		ResourceOpener opener = null;
		Application app = null;

		String fn = null;
		String unid = null;
		Session session = this.getSession();
		boolean applicationExists = false;
		try {
			fn = request.getParameter("fn");
			if (fn == null) throw new RuntimeException("无法获取应用程序包的文件名。");
			unid = fn.substring(0, 32);
			if (unid == null || unid.length() != 32) throw new RuntimeException("无法获取应用程序UNID。");
			opener = ResourceOpenerProvider.getResourceOpener(Application.class);
			app = (Application) opener.open(unid, Application.class, session);
			applicationExists = (app != null);

			String tempFolder = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath();
			String target = String.format("%1$s%2$s", tempFolder, fn);
			File f = new File(target);
			if (!f.exists() || !f.isFile()) throw new RuntimeException("应用程序包的文件不存在。");

			FileInputStream fis = new FileInputStream(target);
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
			ZipEntry entry = null;

			String entryName = null;
			String targetName = null;
			String resourceLocation = CommonConfig.getInstance().getResourcePath();
			String accessoryLocation = CommonConfig.getInstance().getAccessoryPath();
			final String resourcesPrefix = "resources";
			final String accessoryPrefix = "files";

			String importedUnid = null;
			while ((entry = zis.getNextEntry()) != null) {
				entryName = entry.getName();
				if (entryName != null && entryName.startsWith(resourcesPrefix)) {
					targetName = resourceLocation + entryName.substring(resourcesPrefix.length() + 1);
					importedUnid = targetName.substring(targetName.lastIndexOf(File.separatorChar) + 1);
					if (importedUnid != null && importedUnid.length() > 32) importedUnid = importedUnid.substring(0, 32);
				} else if (entryName != null && entryName.startsWith(accessoryPrefix)) {
					targetName = accessoryLocation + entryName.substring(accessoryPrefix.length() + 1);
				}

				int size = 0;
				byte[] buffer = new byte[2048];
				FileOutputStream fos = new FileOutputStream(targetName);
				BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length);
				while ((size = zis.read(buffer, 0, buffer.length)) != -1) {
					bos.write(buffer, 0, size);
				}
				bos.flush();
				bos.close();
				ResourceContext.getInstance().removeObjectFromCache(importedUnid); // 清理缓存。
			} // while end
			zis.close();
			fis.close();
			this.setLastMessage("应用程序" + (applicationExists ? "更新" : "安装") + "成功！");
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
	}
}

