/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.Comparator;

import com.tansuosoft.discoverx.model.Resource;

/**
 * 用于XML资源视图查询类中排序输出使用的资源比较器。
 * 
 * <p>
 * 它依据传入的List&lt;ParsedOrder&gt;信息进行排序，如果不提供则按排序号和名称升序排序。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FunctionComparator implements Comparator<Resource> {
	private static final RuleBasedCollator collator = (RuleBasedCollator) Collator.getInstance(java.util.Locale.CHINA);

	/**
	 * 缺省构造器。
	 */
	public FunctionComparator() {

	}

	/**
	 * 获取函数分类信息对应的排序基数。
	 * 
	 * @param c
	 * @return int
	 */
	protected int getCategoryBaseSort(String c) {
		if (c == null || c.length() == 0) return 0;
		if (c.equalsIgnoreCase("系统公式")) return 10000;
		if (c.indexOf("文档操作") >= 0) return 100000;
		if (c.indexOf("其它操作") >= 0) return 1000000;
		return 0;
	}

	/**
	 * 重载compare：按指定属性值排序函数，默认按函数资源的排序号、名称、UNID升序排序(确保顺序每次都不变)。
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Resource o1, Resource o2) {
		int result = 0;
		int sort1 = getCategoryBaseSort(o1.getCategory()) + o1.getSort();
		int sort2 = getCategoryBaseSort(o2.getCategory()) + o2.getSort();
		result = sort1 - sort2;
		if (result != 0) return result;
		// 如果sort相等,按name的升序排列
		String value1 = o1.getName();
		String value2 = o2.getName();
		if (value1 == null || value2 == null) { // 如果是空值总是排在后面
			result = (value1 == null ? 1 : -1);
		} else if (value1 != null && value1.equalsIgnoreCase(value2)) {
			result = 0;
		} else {// 升序
			result = collator.compare(value1, value2);
		}
		// 如果两条相等，则按照unid排类，以保证排序条件得出的结果的情况下，顺序一样
		if (result == 0) {
			result = o1.getUNID().compareTo(o2.getUNID());
		}
		return result;
	}

}

