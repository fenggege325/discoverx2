/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.DocumentDisplay;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.PublishOption;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.ReferenceComparatorDefault;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Tab;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 导出应用程序（模块）设计信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class ApplicationDesignExporter extends DesignExporter {
	private Form bindf = null;
	private Form bindpf = null;
	private Workflow bindwf = null;

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecial(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecial(Resource r) {
		if (r == null || !(r instanceof Application)) return "";
		Application app = (Application) r;

		StringBuilder sb = new StringBuilder();

		String tmps = app.getForm();
		Resource tmpr = ResourceContext.getInstance().getResource(tmps, Form.class);
		if (tmpr != null) bindf = (Form) tmpr;
		sb.append("<tr><td>文档表单</td><td>");
		if (tmpr == null) {
			sb.append("<无>");
		} else {
			sb.append("<a href=\"#").append(tmpr.getAlias()).append("_basic\">" + tmpr.getName() + "-" + tmpr.getAlias() + "</a>");
		}
		sb.append("</td></tr>").append("\r\n");

		tmps = app.getWorkflow();
		tmpr = ResourceContext.getInstance().getResource(tmps, Workflow.class);
		if (tmpr != null) bindwf = (Workflow) tmpr;
		sb.append("<tr><td>文档流程</td><td>");
		if (tmpr == null) {
			sb.append("<无>");
		} else {
			sb.append("<a href=\"#").append(tmpr.getAlias()).append("_basic\">" + tmpr.getName() + "-" + tmpr.getAlias() + "</a>");
		}
		sb.append("</td></tr>").append("\r\n");

		tmps = app.getProfileForm();
		tmpr = ResourceContext.getInstance().getResource(tmps, Form.class);
		if (tmpr != null) bindpf = (Form) tmpr;
		sb.append("<tr><td>配置文件表单</td><td>");
		if (tmpr == null) {
			sb.append("<无>");
		} else {
			sb.append("<a href=\"#").append(tmpr.getAlias()).append("_basic\">" + tmpr.getName() + "-" + tmpr.getAlias() + "</a>");
		}
		sb.append("</td></tr>").append("\r\n");

		List<Reference> list = app.getViews();
		String tip = (list == null || list.isEmpty() ? "" : "<a href=\"#" + app.getAlias() + "_views\">查看包含的" + list.size() + "个视图</a>");
		sb.append("<tr><td>包含的视图</td><td>").append(list == null || list.isEmpty() ? "<无>" : tip).append("</td></tr>").append("\r\n");
		list = app.getSubApplications();
		tip = (list == null || list.isEmpty() ? "" : "<a href=\"#" + app.getAlias() + "_subApplications\">查看包含的" + list.size() + "个子模块</a>");
		sb.append("<tr><td>包含的下级应用程序（子模块）</td><td>").append(list == null || list.isEmpty() ? "<无>" : tip).append("</td></tr>").append("\r\n");

		sb.append("<tr><td>业务序号模式</td><td>").append(StringUtil.getValueString(app.getBusinessSequencePattern(), "")).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>流水序号（流水号）模式</td><td>").append(StringUtil.getValueString(app.getProcessSequencePattern(), "")).append("</td></tr>").append("\r\n");

		sb.append("<tr><td>发布选项</td><td>").append(app.getPublish() ? "发布并公开" : "仅发布").append("</td></tr>").append("\r\n");
		PublishOption po = app.getPublishOption();
		String pot = "";
		if (po == PublishOption.All) {
			pot = "对所有人公开";
		} else if (po == PublishOption.User) {
			pot = "对所有注册用户公开";
		} else if (po == PublishOption.Custom) {
			pot = "在发布时选择";
		} else if (po == PublishOption.DirectOU) {
			pot = "对直属同部门的用户公开";
		} else if (po == PublishOption.OU1) {
			pot = "对同属一级部门的用户公开";
		}
		sb.append("<tr><td>发布范围（公开选项）</td><td>").append(pot).append("</td></tr>").append("\r\n");

		sb.append("<tr><td>文档名称提供信息</td><td>").append(StringUtil.getValueString(app.getDocumentNameProviderClassName(), "")).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>文档摘要提供信息</td><td>").append(StringUtil.getValueString(app.getDocumentAbstractProviderClassName(), "")).append("</td></tr>").append("\r\n");

		sb.append("<tr><td>支持的正文类型附加文件个数</td><td>").append(app.getTextAccessoryCount()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>是否自动记录文档状态信息</td><td>").append(app.getAutoRecordStateInfo() ? "是" : "否").append("</td></tr>").append("\r\n");

		return sb.toString();
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecialCollection(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecialCollection(Resource r) {
		if (r == null || !(r instanceof Application)) return "";
		Application app = (Application) r;
		StringBuilder sb = new StringBuilder();

		// 文档安全信息
		sb.append(this.exportSecurity(app.getDocumentDefaultSecurity(), "文档权限控制", r.getAlias() + "_documentDefaultSecurity"));

		// 文档呈现信息
		DocumentDisplay dd = app.getDocumentDisplay();
		if (dd != null) {
			sb.append(this.exportH2("文档显示（呈现）信息", r.getAlias() + "_basic"));
			sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
			sb.append("<colgroup><col width=\"38%\"/><col/></colgroup>").append("\r\n");
			sb.append("<tr class=\"head\"><td>属性名</td><td>属性值</td></tr>").append("\r\n");
			sb.append("<tr><td>启用条件表达式</td><td>").append(StringUtil.getValueString(dd.getExpression(), "")).append("</td></tr>").append("\r\n");
			sb.append("<tr><td>最大可用显示宽度</td><td>").append(dd.getMaxWidth()).append("</td></tr>").append("\r\n");
			sb.append("<tr><td>自定义显示实现信息</td><td>").append(StringUtil.getValueString(dd.getDisplay(), "")).append("</td></tr>").append("\r\n");
			sb.append("<tr><td>文档只读表达式</td><td>").append(StringUtil.getValueString(dd.getReadonlyExpression(), "")).append("</td></tr>").append("\r\n");
			sb.append("</table>").append("\r\n");

			// 文档呈现扩展html属性
			sb.append(this.exportHtmlAttributes(dd.getHtmlAttributes(), "文档呈现扩展html属性", app.getAlias() + "_documentDisplay_htmlAttributes"));

			// 标签页
			List<Tab> tabs = dd.getTabs();
			if (tabs != null && tabs.size() > 0) {
				sb.append(this.exportH2("文档内容呈现标签页", r.getAlias() + "_documentDisplay_tabs"));
				sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
				sb.append("<colgroup><col width=\"38%\"/><col/></colgroup>").append("\r\n");
				sb.append("<tr class=\"head\"><td>属性名</td><td>属性值</td></tr>").append("\r\n");
				int idx = 0;
				for (Tab x : tabs) {
					if (x == null) continue;
					idx++;
					sb.append("<tr class=\"subhead\"><td colspan=\"2\">标签页").append(idx).append("：").append(x.getTitle()).append("</td></tr>").append("\r\n");
					sb.append("<tr><td>绑定内容显示控件</td><td>").append(x.getControl()).append("</td></tr>").append("\r\n");
					sb.append("<tr><td>启用表达式</td><td>").append(this.processExpression(x.getExpression())).append("</td></tr>").append("\r\n");
					sb.append("<tr><td>唯一标识</td><td>").append(x.getId()).append("</td></tr>").append("\r\n");
					sb.append("<tr><td>离开时执行的js代码</td><td>").append(StringUtil.getValueString(x.getOnleave(), "")).append("</td></tr>").append("\r\n");
					sb.append("<tr><td>选中时执行的js代码</td><td>").append(StringUtil.getValueString(x.getOnselect(), "")).append("</td></tr>").append("\r\n");
					sb.append("<tr><td>序号</td><td>").append(x.getSort()).append("</td></tr>").append("\r\n");
					sb.append("<tr><td>说明</td><td>").append(StringUtil.getValueString(x.getDescription(), "")).append("</td></tr>").append("\r\n");
				}
				sb.append("</table>").append("\r\n");
			}
		}

		// 通用操作
		sb.append(this.exportOperations(app.getOperations(), "通用操作", r.getAlias() + "_operations"));

		// 新增操作
		sb.append(this.exportOperations(app.getNewStateOperations(), "新增状态操作", r.getAlias() + "_newStateOperations"));

		// 事件处理程序
		sb.append(this.exportEventHandlers(app.getEventHandlers(), "文档事件处理程序", r.getAlias() + "_eventHandlers"));

		// 绑定表单
		sb.append(new FormDesignExporter().export(bindf));

		// 绑定配置文件表单
		sb.append(new FormDesignExporter().export(bindpf));

		// 绑定流程
		sb.append(new WorkflowDesignExporter().export(bindwf));

		// 视图
		List<Reference> list = app.getViews();
		if (list != null && list.size() > 0) {
			StringBuilder sbview = new StringBuilder();
			ViewDesignExporter exporter = new ViewDesignExporter();
			Collections.sort(list, new ReferenceComparatorDefault());
			View view = null;
			sb.append(this.exportH2("包含的视图", r.getAlias() + "_views"));
			// sb.append("<fieldset>").append("\r\n");
			// sb.append("<legend>包含的视图</legend>").append("\r\n");
			for (Reference ref : list) {
				if (ref == null || ref.getUnid() == null) continue;
				view = ResourceContext.getInstance().getResource(ref.getUnid(), View.class);
				if (view == null) continue;
				sb.append("<a href=\"#").append(view.getAlias()).append("_basic\">" + view.getName() + "-" + view.getAlias() + "</a><br/>\r\n");
				sbview.append(exporter.export(view));
			}
			sb.append(sbview);
			// sb.append("</fieldset>").append("\r\n");
		}

		// 包含的下级应用程序(子模块)
		list = app.getSubApplications();
		if (list != null && list.size() > 0) {
			StringBuilder sbsub = new StringBuilder();
			ApplicationDesignExporter exporter = new ApplicationDesignExporter();
			Collections.sort(list, new ReferenceComparatorDefault());
			Application sub = null;
			sb.append(this.exportH2("包含的下级应用程序（子模块）", r.getAlias() + "_subApplications"));
			// sb.append("<fieldset>").append("\r\n");
			// sb.append("<legend>包含的下级应用程序（子模块）</legend>").append("\r\n");
			for (Reference ref : list) {
				if (ref == null || ref.getUnid() == null) continue;
				sub = ResourceContext.getInstance().getResource(ref.getUnid(), Application.class);
				if (sub == null) continue;
				sb.append("<a href=\"#").append(sub.getAlias()).append("_basic\">" + sub.getName() + "-" + sub.getAlias() + "</a><br/>\r\n");
				sbsub.append(exporter.export(sub));
			}
			sb.append(sbsub);
			// sb.append("</fieldset>").append("\r\n");
		}

		return sb.toString();
	}
}
