/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.model.Organization;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出供管理通道使用的组织结构资源TreeView对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OrganizationTreeViewRender implements HtmlRender {
	/**
	 * 缺省构造器。
	 */
	public OrganizationTreeViewRender() {
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		Organization org = ResourceContext.getInstance().getResource(OrganizationsContext.getInstance().getContextUserOrgUnid(), Organization.class);
		if (org == null) { return "id:'tverror',error:true,label:'无法获取组织结构信息！'"; }
		if (!SecurityHelper.authorize(jspContext.getLoginUser(), org, SecurityLevel.View)) { return "id:'tverror',error:true,label:'对不起，您没有查看组织结构信息的权限！'"; }
		StringBuilder sb = new StringBuilder();
		renderOrgTreeView(org, sb);
		return sb.toString();
	}

	/**
	 * 输出组织树。
	 * 
	 * @param res
	 * @param sb
	 */
	protected static void renderOrgTreeView(Organization res, StringBuilder sb) {
		if (res == null) return;
		boolean rootFlag = (sb.length() == 0);
		if (!rootFlag) sb.append("{"); // treeview begin
		sb.append("id:").append("'").append(res.getUNID()).append("'");
		sb.append(",").append("columns:[");
		if (rootFlag) {
			sb.append("'类型'");
			sb.append(",").append("'安全编码'");
			sb.append(",").append("'代码'");
			sb.append(",").append("'排序号'");
		} else {
			sb.append("'").append(StringUtil.encode4Json(StringUtil.getValueString(res.getCategory(), "部门"))).append("'");
			sb.append(",").append("'").append(res.getSecurityCode()).append("'");
			sb.append(",").append("'").append(StringUtil.encode4Json(res.getAlias())).append("'");
			sb.append(",").append("'").append(res.getSort()).append("'");
		}
		sb.append("]");
		String lbl = StringUtil.encode4Json(res.getName());
		sb.append(",").append("label:").append("'").append(lbl).append("'");
		String desc = StringUtil.getValueString(res.getDescription(), "");
		if (desc == null || desc.length() == 0) desc = lbl;
		sb.append(",").append("desc:").append("'").append(desc).append("'");
		int idx = 0;
		List<Resource> list = res.getChildren();
		if (list != null) {
			sb.append(",").append("children:").append("["); // children begin
			for (Resource x : list) {
				if (x == null || !(x instanceof Organization)) continue;
				sb.append(idx == 0 ? "" : ",");
				renderOrgTreeView((Organization) x, sb);
				idx++;
			}
			sb.append("]"); // children end
		}

		if (!rootFlag) sb.append("}"); // treeview end

		return;
	}
}

