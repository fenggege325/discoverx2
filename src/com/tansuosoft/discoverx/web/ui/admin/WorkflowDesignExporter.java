/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 导出流程设计信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class WorkflowDesignExporter extends DesignExporter {

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecial(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecial(Resource r) {
		if (r == null || !(r instanceof Workflow)) return "";
		Workflow wf = (Workflow) r;

		StringBuilder sb = new StringBuilder();

		List<Activity> list = wf.getActivities();
		String tip = (list == null || list.isEmpty() ? "" : "<a href=\"#" + r.getAlias() + "_activities\">查看包含的" + list.size() + "个环节</a>");
		sb.append("<tr><td>包含的环节</td><td>").append(list == null || list.isEmpty() ? "<无>" : tip).append("</td></tr>").append("\r\n");

		return sb.toString();
	}

	/**
	 * @see com.tansuosoft.discoverx.web.ui.admin.DesignExporter#exportSpecialCollection(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	protected String exportSpecialCollection(Resource r) {
		if (r == null || !(r instanceof Workflow)) return "";
		Workflow wf = (Workflow) r;
		StringBuilder sb = new StringBuilder();

		// 事件处理程序
		sb.append(this.exportEventHandlers(wf.getEventHandlers(), "流程事件处理程序", r.getAlias() + "_eventHandlers"));

		// 流程环节
		List<Activity> list = wf.getActivities();
		if (list != null && list.size() > 0) {
			StringBuilder sba = new StringBuilder();
			ActivityDesignExporter exporter = new ActivityDesignExporter(wf);
			sb.append(this.exportH2("包含的环节", r.getAlias() + "_activities"));
			// sb.append("<fieldset>").append("\r\n");
			// sb.append("<legend>包含的环节</legend>").append("\r\n");
			for (Activity x : list) {
				if (x == null) continue;
				sb.append("<a href=\"#").append(x.getAlias()).append("_basic\">" + x.getName() + "-" + x.getAlias() + "</a><br/>\r\n");
				sba.append(exporter.export(x));
			}
			sb.append(sba);
			// sb.append("</fieldset>").append("\r\n");
		}

		return sb.toString();
	}

}

