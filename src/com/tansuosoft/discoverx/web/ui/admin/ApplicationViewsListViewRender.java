/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.web.ui.view.ViewForm;
import com.tansuosoft.discoverx.web.ui.view.ViewRender;

/**
 * 输出某个应用程序包含的视图列表对应的js ListView对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ApplicationViewsListViewRender implements HtmlRender {
	private String m_appUnid = null;

	/**
	 * 接收应用模块UNID的构造器。
	 * 
	 * @param appUnid
	 */
	public ApplicationViewsListViewRender(String appUnid) {
		m_appUnid = appUnid;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (this.m_appUnid == null || this.m_appUnid.length() == 0) throw new RuntimeException("没有提供有效模块信息！");
		StringBuilder sb = new StringBuilder();
		String viewUNID = ResourceDescriptorConfig.getInstance().getResourceDescriptor(View.class).getConfigView();
		if (viewUNID == null || viewUNID.length() == 0) throw new RuntimeException("没有配置视图配置列表对应的视图资源！");
		ViewForm viewForm = new ViewForm();
		viewForm.setUNID(viewUNID);
		viewForm.setShowOperation(false);
		viewForm.setShowNavigator(false);
		ViewRender viewRender = new ViewRender(viewForm);
		sb.append(viewRender.render(jspContext));
		return sb.toString();
	}
}

