/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.ResourceConstructor;
import com.tansuosoft.discoverx.bll.ResourceConstructorProvider;
import com.tansuosoft.discoverx.bll.ResourceInserter;
import com.tansuosoft.discoverx.bll.ResourceInserterProvider;
import com.tansuosoft.discoverx.bll.ResourceUpdater;
import com.tansuosoft.discoverx.bll.ResourceUpdaterProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.bll.view.CommonCondition;
import com.tansuosoft.discoverx.bll.view.CommonConditionConfig;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.DocumentDisplay;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.ResourceTreeHelper;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.Tab;
import com.tansuosoft.discoverx.model.TabShow;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.model.ViewOrder;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UNIDProvider;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.ActivityType;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 实现新增应用程序向导确定功能的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>name</td>
 * <td>应用程序名称。</td>
 * </tr>
 * <tr>
 * <td>alias</td>
 * <td>要应用程序别名。</td>
 * </tr>
 * <tr>
 * <td>punid</td>
 * <td>上级应用程序资源UNID。</td>
 * </tr>
 * <tr>
 * <td>menu</td>
 * <td>所在菜单项信息，如{菜单资源unid}\\{一级菜单项unid}...。</td>
 * </tr>
 * <tr>
 * <td>form</td>
 * <td>绑定的表单信息。</td>
 * </tr>
 * <tr>
 * <td>workflow</td>
 * <td>绑定的流程信息。</td>
 * </tr>
 * <tr>
 * <td>view</td>
 * <td>绑定的视图信息。</td>
 * </tr>
 * <tr>
 * <td>template</td>
 * <td>应用程序所属模板应用程序unid。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>如果成功，则message结果为新增应用程序资源的unid。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ApplicationWizardOk extends Operation {
	/**
	 * 缺省构造器。
	 */
	public ApplicationWizardOk() {
		super();
	}

	private List<Resource> toBeInsertedList = new ArrayList<Resource>();
	private List<Resource> toBeUpdatedList = new ArrayList<Resource>();

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		this.setLastAJAXResponse(response);
		HttpServletRequest request = this.getHttpRequest();
		Session s = this.getSession();

		String name = request.getParameter("name");
		String alias = request.getParameter("alias");
		String aliasPostfix = (alias.startsWith("app") ? alias.substring(3) : alias);
		String punid = request.getParameter("punid");
		String template = request.getParameter("template");
		Application templetApplication = null;
		String templetForm = null;
		String templetWorkflow = null;
		List<Reference> templetSubApps = null;
		List<Reference> templetSubViews = null;
		List<Accessory> accs = null;
		if (template != null && template.length() > 0) {
			templetApplication = ResourceContext.getInstance().getResource(template, Application.class);
			if (templetApplication != null) {
				templetForm = StringUtil.getValueString(templetApplication.getForm(), null);
				templetWorkflow = StringUtil.getValueString(templetApplication.getWorkflow(), null);
			}
		}
		String menu = request.getParameter("menu");
		String form = (StringUtil.isBlank(templetForm) ? request.getParameter("form") : templetForm);
		String workflow = (StringUtil.isBlank(templetWorkflow) ? request.getParameter("workflow") : templetWorkflow);
		String view = request.getParameter("view");
		ResourceConstructor constructor = null;
		ResourceInserter inserter = null;
		ResourceUpdater updater = null;
		Application app = null;
		Application papp = null;
		Form frm = null;
		Workflow wf = null;
		Menu m = null;
		View v = null;
		try {
			if (templetApplication != null) {
				app = (Application) templetApplication.clone();
				app.setUNID(UNIDProvider.getUNID());
				app.setPUNID(null);
			} else {
				constructor = ResourceConstructorProvider.getResourceConstructor(Application.class);
				app = (Application) constructor.construct(Application.class, s, null, punid, null);
			}
			if (app == null) throw new Exception("无法构造新应用程序！");
			app.setName(name);
			app.setAlias(alias);
			app.setOperations(new ArrayList<com.tansuosoft.discoverx.model.Operation>(1));
			com.tansuosoft.discoverx.model.Operation oprdel = new com.tansuosoft.discoverx.model.Operation();
			oprdel.setExpression("@delete()");
			oprdel.setSort(1);
			oprdel.setTitle("作废");
			oprdel.setDescription("彻底删除当前文件");
			app.getOperations().add(oprdel);
			if (punid != null && punid.length() > 0) {
				papp = (Application) ResourceContext.getInstance().getResource(punid, Application.class);
				if (papp == null) throw new Exception("无法获取上级应用程序！");

				List<Reference> rs = papp.getSubApplications();
				if (rs == null) {
					rs = new ArrayList<Reference>(1);
					papp.setSubApplications(rs);
				}
				Reference r = new Reference(app.getUNID(), name, app.getDirectory(), rs.size());
				rs.add(r);
				app.setPUNID(papp.getUNID());
				toBeUpdatedList.add(papp);
			}
			if (menu != null && menu.length() > 0) {
				String[] unids = StringUtil.splitString(menu, '\\');
				m = (Menu) ResourceContext.getInstance().getResource(unids[0], Menu.class);
				if (m == null) throw new Exception("无法获取菜单！");
				Menu newMenu = new Menu();
				newMenu.setName(name);
				newMenu.setAlias("menu" + aliasPostfix);
				Menu subMenu = null;
				if (unids.length > 1) {
					subMenu = (Menu) ResourceTreeHelper.getChildByUNID(m, unids[unids.length - 1]);
					if (subMenu != null) subMenu.addChild(newMenu);
				}
				if (subMenu == null) {
					m.addChild(newMenu);
				}
				toBeUpdatedList.add(m);
			}
			if (StringUtil.isUNID(form)) {
				frm = (Form) ResourceContext.getInstance().getResource(form, Form.class);
				if (frm == null) form = "1";
				Form clonedForm = (Form) frm.clone();
				clonedForm.setUNID(UNIDProvider.getUNID());
				clonedForm.setPUNID(app.getUNID());
				clonedForm.setName(name + "表单");
				clonedForm.setAlias("frm" + aliasPostfix);
				toBeInsertedList.add(clonedForm);
				if (!StringUtil.isBlank(templetForm)) viewTableMaps.put(frm.getAlias().toLowerCase(), clonedForm.getAlias().toLowerCase());
				app.setForm(clonedForm.getUNID());
				frm = clonedForm;
			} else {
				switch (StringUtil.getValueInt(form, 0)) {
				case 0: // 新增
					constructor = ResourceConstructorProvider.getResourceConstructor(Form.class);
					frm = (Form) constructor.construct(Form.class, s, null, punid, null);
					if (frm == null) throw new Exception("无法构造空白表单！");
					frm.setName(name + "表单");
					frm.setAlias("frm" + aliasPostfix);
					frm.setPUNID(app.getUNID());
					frm.setItems(new ArrayList<Item>());
					Item item = new Item();
					item.setAlias("fld_subject");
					item.setName("主题");
					item.setRow(1);
					item.setColumn(1);
					item.setPUNID(frm.getUNID());
					frm.getItems().add(item);
					toBeInsertedList.add(frm);
					app.setForm(frm.getUNID());
					break;
				case 1: // 无绑定
				case 2: // 从父模块继承
				default:
					break;
				}
			}
			if (StringUtil.isUNID(workflow)) {
				wf = (Workflow) ResourceContext.getInstance().getResource(workflow, Workflow.class);
				if (wf == null) {
					workflow = "1";
				} else {
					Workflow clonedWF = (Workflow) wf.clone();
					clonedWF.setUNID(UNIDProvider.getUNID());
					clonedWF.setPUNID(app.getUNID());
					clonedWF.setName(name + "流程");
					clonedWF.setAlias("wf" + aliasPostfix);
					toBeInsertedList.add(clonedWF);
					app.setWorkflow(clonedWF.getUNID());
				}
			} else {
				switch (StringUtil.getValueInt(form, 0)) {
				case 0: // 新增
					constructor = ResourceConstructorProvider.getResourceConstructor(Workflow.class);
					wf = (Workflow) constructor.construct(Workflow.class, s, null, punid, null);
					if (wf == null) throw new Exception("无法构造空白流程！");
					wf.setName(name + "流程");
					wf.setPUNID(app.getUNID());
					wf.setAlias("wf" + aliasPostfix);
					Activity activity = new Activity();
					activity.setName("开始");
					activity.setAlias("wn0");
					activity.setDescription("开始环节。");
					activity.setActivityType(ActivityType.Begin);
					activity.setOperations(new ArrayList<com.tansuosoft.discoverx.model.Operation>());
					com.tansuosoft.discoverx.model.Operation operation = new com.tansuosoft.discoverx.model.Operation();
					operation.setTitle("送审批");
					operation.setExpression("@wfdone()");
					operation.setIcon("icon_right.gif");
					operation.setSort(100);
					activity.getOperations().add(operation);
					wf.addActivity(activity);
					toBeInsertedList.add(wf);
					app.setWorkflow(wf.getUNID());
					break;
				case 1: // 无绑定
				case 2: // 从父模块继承
				default:
					break;
				}
			}
			app.setDocumentDisplay(new DocumentDisplay());
			if (wf == null || (wf != null && (wf.getActivities() == null || wf.getActivities().size() <= 1))) {
				oprdel.setVisibleExpression("@CheckSecLevel()||@IsAdmin()");
				app.getDocumentDisplay().setReadonlyExpression("!(@CheckSecLevel()||@IsAdmin())");
				app.getDocumentDisplay().setTabs(new ArrayList<Tab>(1));
				Tab tab = new Tab();
				tab.setSort(0);
				tab.setControl("defaultdocctrls/document_basic.jsp");
				tab.setDescription(app.getName());
				tab.setTitle(app.getName());
				tab.setTabShow(TabShow.ShowAll);
				app.getDocumentDisplay().getTabs().add(tab);
			} else {
				oprdel.setVisibleExpression("(@CheckSecLevel() && @CurrentActivity(p:=4)==\"1\")||@IsAdmin()");
				app.getDocumentDisplay().setReadonlyExpression("!@IsApprover()");
			}
			if (templetSubViews == null && view != null && view.length() > 0 && !view.equalsIgnoreCase("-1")) {
				constructor = ResourceConstructorProvider.getResourceConstructor(View.class);
				String[] idxs = StringUtil.splitString(view, ',');
				List<ViewColumn> vcs = null;
				ViewColumn vc = null;
				CommonConditionConfig cfg = CommonConditionConfig.getInstance();
				List<CommonCondition> ccs = cfg.getCommonConditions();
				String[] viewAliases = { "Todo", "All", "AllAvail", "AllIssued", "AllToBeIssued", "AllArchived" };
				CommonCondition cc = null;
				int idx = 0;
				int sort = 0;
				String frmAlias = (frm != null ? frm.getAlias() : null);
				String viewName = null;
				for (String x : idxs) {
					idx = StringUtil.getValueInt(x, -1);
					if (idx < 0) continue;
					cc = ccs.get(idx);
					if (cc == null) continue;
					v = (View) constructor.construct(View.class, s, null, punid, null);
					if (v == null) throw new Exception("无法构造空白视图！");
					viewName = StringUtil.getValueString(request.getParameter("viewname" + x), cc.getName().replace("未删除", ""));
					v.setName(viewName);
					v.setDescription(name + cc.getName());
					v.setAlias("view" + (viewAliases.length > idx ? viewAliases[idx] : x) + "Of" + aliasPostfix);
					v.setTable(frmAlias);
					vcs = new ArrayList<ViewColumn>(1);
					vc = new ViewColumn("t_document", "c_name", "文档名称");
					vcs.add(vc);
					v.setColumns(vcs);
					v.setCommonCondition(cc.getName());
					v.setPUNID(app.getUNID());
					v.setSort(sort);
					sort += 3;
					if (app.getViews() == null) app.setViews(new ArrayList<Reference>(idxs.length));
					app.getViews().add(new Reference(v.getUNID(), v.getName(), v.getDirectory(), idx));
					toBeInsertedList.add(v);
				}
			} else if ((templetSubViews = dupViews(templetApplication, app.getUNID(), aliasPostfix)) != null && templetSubViews.size() > 0) {
				app.setViews(templetSubViews);
			}
			if ((templetSubApps = dupSubApps(templetApplication, app.getUNID(), aliasPostfix)) != null && templetSubApps.size() > 0) {
				app.setSubApplications(templetSubApps);
			}
			if ((accs = dupAccs(templetApplication, app)) != null && accs.size() > 0) {
				app.setAccessories(accs);
			}

			// 配置文件表单
			dupProfileForm(templetApplication, app);

			toBeInsertedList.add(app);
			for (Resource x : toBeInsertedList) {
				if (!SecurityHelper.authorize(s, x, SecurityLevel.Add)) throw new Exception("您没有新建“" + x.getDirectory() + "”类型的资源的权限！");
			}
			for (Resource x : toBeUpdatedList) {
				if (!SecurityHelper.authorize(s, x, SecurityLevel.Modify)) throw new Exception("您没有更改“" + x.getDirectory() + "”类型的资源的权限！");
			}
			for (Resource x : toBeInsertedList) {
				inserter = ResourceInserterProvider.getResourceInserter(x);
				if (inserter == null) throw new Exception("无法获取“" + x.getDirectory() + "”类型资源保存实现对象！");
				inserter.insert(x, s);
			}
			for (Resource x : toBeUpdatedList) {
				updater = ResourceUpdaterProvider.getResourceUpdater(x);
				if (updater == null) throw new Exception("无法获取“" + x.getDirectory() + "”类型资源更新实现对象！");
				updater.update(x, s);
			}
			// 创建frame下的应用程序特定程序目录
			Writer out = null;
			try {
				String appSpecialPath = CommonConfig.getInstance().getInstallationPath() + "frame" + File.separator + app.getAlias().toLowerCase() + File.separator;
				File appSpecialPathFile = new File(appSpecialPath);
				appSpecialPathFile.mkdirs();
				out = new OutputStreamWriter(new FileOutputStream(appSpecialPath + "readme.txt"), "utf-8");
				out.write("将应用程序特定功能对应文件（除jar外）保存在此应用程序特定功能目录下(目录命名以转化为小写后的应用程序别名作为路径)，此目录可以包含：\r\n");
				out.write("1.应用程序特定的js、css、图片、页面文件（.jsp、.aspx...）等文件；\r\n");
				out.write("2.如果有以应用程序包含的表单别名（小写）为文件名的.js文件，则打开此表单时会自动引用这个js文件；\r\n");
				out.write("3.所有其它类型的各类文件。\r\n");
				out.write("应用程序特定功能对应的程序包文件（.jar或.dll等)应以“{转化为小写后的应用程序别名}.jar/.dll”命名规则保存于web程序目录下，通常java版为“WEB-INF\\lib”，.NET版为“bin”。\r\n");
				out.write("导出应用程序时，系统会自动将这些目录和文件一并导出并打包。 ");
			} catch (Exception ex) {
			} finally {
				out.close();
			}
		} catch (Exception e) {
			if (m != null) ResourceContext.getInstance().removeObjectFromCache(m.getUNID());
			if (papp != null) ResourceContext.getInstance().removeObjectFromCache(papp.getUNID());
			response.setStatus(-1);
			response.setMessage(e.getMessage());
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		response.setMessage(app.getUNID());
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}

	private int subAppLevel = 1;
	private Map<String, String> viewTableMaps = new HashMap<String, String>();

	/**
	 * 复制parent指定的应用程序的下级应用程序资源为新资源（包括实际文件）。
	 * 
	 * @param parent
	 * @param punid
	 * @param aliasSuffix
	 * @return
	 */
	protected List<Reference> dupSubApps(Application parent, String punid, String aliasSuffix) {
		if (parent == null) return null;
		List<Reference> subapps = parent.getSubApplications();
		if (subapps == null || subapps.isEmpty()) return null;

		List<Reference> result = null;
		Application sub = null;
		Application newsub = null;
		ResourceContext rc = ResourceContext.getInstance();
		try {
			int idx = 0;
			for (Reference r : subapps) {
				if (r == null) continue;
				sub = rc.getResource(r.getUnid(), Application.class);
				if (sub == null) continue;
				// 本身
				newsub = (Application) sub.clone();
				if (result == null) result = new ArrayList<Reference>();
				newsub.setAlias("app" + aliasSuffix + "_" + subAppLevel + "_" + (++idx));
				newsub.setPUNID(punid);
				newsub.setUNID(UNIDProvider.getUNID());
				// 表单
				String frmUNID = sub.getForm();
				Form frm = rc.getResource(frmUNID, Form.class);
				if (frm != null) {
					Form clonedForm = (Form) frm.clone();
					clonedForm.setUNID(UNIDProvider.getUNID());
					clonedForm.setPUNID(newsub.getUNID());
					clonedForm.setAlias("frm" + aliasSuffix + "_" + subAppLevel + "_" + (++idx));
					viewTableMaps.put(frm.getAlias().toLowerCase(), clonedForm.getAlias().toLowerCase());
					newsub.setForm(clonedForm.getUNID());
					toBeInsertedList.add(clonedForm);
				}

				// 配置文件表单
				dupProfileForm(sub, newsub);

				// 流程
				String wfUNID = sub.getWorkflow();
				Workflow wf = rc.getResource(wfUNID, Workflow.class);
				if (wf != null) {
					Workflow clonedWF = (Workflow) wf.clone();
					clonedWF.setUNID(UNIDProvider.getUNID());
					clonedWF.setPUNID(newsub.getUNID());
					clonedWF.setAlias("wf" + aliasSuffix + "_" + subAppLevel + "_" + (++idx));
					toBeInsertedList.add(clonedWF);
					newsub.setWorkflow(clonedWF.getUNID());
				}
				// 子模块
				subAppLevel++;
				newsub.setSubApplications(dupSubApps(sub, newsub.getUNID(), aliasSuffix));
				subAppLevel--;
				// 视图
				newsub.setViews(dupViews(sub, newsub.getUNID(), aliasSuffix));
				// 附加文件
				newsub.setAccessories(dupAccs(sub, newsub));

				// 待确定：扩展前段代码是否要一并处理？

				result.add(new Reference(newsub.getUNID(), r.getTitle(), r.getDirectory(), r.getSort()));
				toBeInsertedList.add(newsub);
			}
		} catch (CloneNotSupportedException e) {
			FileLogger.debug("克隆子应用程序“%1$s”时出现错误：%2$s", sub.getName(), e.getMessage());
		}
		return result;
	}

	/**
	 * 复制src指定的应用程序的配置文件表单到dst指定的目标应用程序。
	 * 
	 * @param src
	 * @param dst
	 * @return
	 */
	protected Form dupProfileForm(Application src, Application dst) {
		if (src == null) return null;
		try {
			String pfrmUNID = src.getProfileForm();
			Form pfrm = ResourceContext.getInstance().getResource(pfrmUNID, Form.class);
			if (pfrm != null) {
				Form clonedPForm = (Form) pfrm.clone();
				clonedPForm.setUNID(UNIDProvider.getUNID());
				clonedPForm.setPUNID(dst.getUNID());
				clonedPForm.setAlias("frmp" + dst.getAlias().substring(3));
				viewTableMaps.put(pfrm.getAlias().toLowerCase(), clonedPForm.getAlias().toLowerCase());
				dst.setProfileForm(clonedPForm.getUNID());
				toBeInsertedList.add(clonedPForm);
			}
			return pfrm;
		} catch (CloneNotSupportedException e) {
			FileLogger.debug("克隆子应用程序“%1$s”时出现错误：%2$s", src.getName(), e.getMessage());
		}
		return null;
	}

	/**
	 * 复制parent指定的应用程序包含的视图资源为新资源（包括实际文件）。
	 * 
	 * @param parent
	 * @param punid
	 * @param aliasSuffix
	 * @return
	 */
	protected List<Reference> dupViews(Application parent, String punid, String aliasSuffix) {
		if (parent == null) return null;
		List<Reference> views = parent.getViews();
		if (views == null || views.isEmpty()) return null;

		List<Reference> result = null;
		View v = null;
		View nv = null;
		ResourceContext rc = ResourceContext.getInstance();
		try {
			int idx = 0;
			for (Reference r : views) {
				if (r == null) continue;
				v = rc.getResource(r.getUnid(), View.class);
				if (v == null) continue;
				nv = (View) v.clone();
				if (result == null) result = new ArrayList<Reference>();
				nv.setAlias("view" + aliasSuffix + "_" + subAppLevel + "_" + (++idx));
				nv.setPUNID(punid);
				nv.setUNID(UNIDProvider.getUNID());

				// 视图引用的表单别名处理
				String t = nv.getTable();
				if (t != null && t.length() > 0) {
					t = t.toLowerCase();
					String nt = viewTableMaps.get(t);
					if (nt != null) {
						nv.setTable(nt);
						List<ViewColumn> vcs = nv.getColumns();
						if (vcs != null && vcs.size() > 0) {
							for (ViewColumn x : vcs) {
								if (x == null) continue;
								String tbl = x.getTableName();
								if (tbl == null || tbl.length() == 0) continue;
								tbl = tbl.toLowerCase();
								if (!tbl.endsWith(t)) continue;
								tbl = tbl.replace(t, nt);
								x.setTableName(tbl);
							}
						}
						List<ViewCondition> vds = nv.getConditions();
						if (vds != null && vds.size() > 0) {
							for (ViewCondition x : vds) {
								if (x == null) continue;
								String tbl = x.getTableName();
								if (tbl == null || tbl.length() == 0) continue;
								tbl = tbl.toLowerCase();
								if (!tbl.endsWith(t)) continue;
								tbl = tbl.replace(t, nt);
								x.setTableName(tbl);
							}
						}
						List<ViewOrder> vos = nv.getOrders();
						if (vos != null && vos.size() > 0) {
							for (ViewOrder x : vos) {
								if (x == null) continue;
								String tbl = x.getTableName();
								if (tbl == null || tbl.length() == 0) continue;
								tbl = tbl.toLowerCase();
								if (!tbl.endsWith(t)) continue;
								tbl = tbl.replace(t, nt);
								x.setTableName(tbl);
							}
						}
					}
				}

				result.add(new Reference(nv.getUNID(), r.getTitle(), r.getDirectory(), r.getSort()));
				toBeInsertedList.add(nv);
			}
		} catch (CloneNotSupportedException e) {
			FileLogger.debug("克隆视图“%1$s”时出现错误：%2$s", v.getName(), e.getMessage());
		}
		return result;
	}

	/**
	 * 复制parent指定的应用程序包含的附加文件资源为新资源（包括实际文件）。
	 * 
	 * @param src
	 * @param dst
	 * @return
	 */
	protected List<Accessory> dupAccs(Application src, Application dst) {
		if (src == null || dst == null) return null;
		List<Accessory> accs = src.getAccessories();
		if (accs == null || accs.isEmpty()) return null;
		List<Accessory> result = null;
		Accessory newacc = null;
		try {
			String psrc = AccessoryPathHelper.getAbsoluteAccessoryServerPath(src);
			String pdst = AccessoryPathHelper.getAbsoluteAccessoryServerPath(dst);
			String punid = dst.getUNID();
			File fsrc = null;
			File fdst = null;
			for (Accessory r : accs) {
				if (r == null) continue;
				if (result == null) result = new ArrayList<Accessory>(accs.size());
				newacc = (Accessory) r.clone();
				newacc.setUNID(UNIDProvider.getUNID());
				newacc.setPUNID(punid);
				fsrc = new File(psrc + r.getFileName());
				fdst = new File(pdst + newacc.getFileName());
				FileHelper.copyFile(fsrc, fdst);
				result.add(newacc);
			}
		} catch (CloneNotSupportedException e) {
			FileLogger.debug("克隆附加文件列表时出现错误：%2$s", e.getMessage());
		}
		return result;
	}
}

