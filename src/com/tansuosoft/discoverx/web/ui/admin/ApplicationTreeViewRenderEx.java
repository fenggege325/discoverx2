/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.bll.view.XmlResourceOrderByComparator;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UNIDProvider;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 输出供管理通道应用程序管理中使用的TreeView对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ApplicationTreeViewRenderEx implements HtmlRender {
	/**
	 * 缺省构造器。
	 */
	public ApplicationTreeViewRenderEx() {
	}

	protected int m_idIndex = 0;
	protected static String m_adminImagesPath = null;
	protected static Map<String, String> m_iconMap = new HashMap<String, String>();

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (m_adminImagesPath == null) {
			m_adminImagesPath = jspContext.getPathContext().getWebAppRoot() + "admin/images/";
			m_iconMap.put("application", m_adminImagesPath + "application.gif");
			m_iconMap.put("view", m_adminImagesPath + "application_view.gif");
			m_iconMap.put("form", m_adminImagesPath + "application_form.gif");
			m_iconMap.put("workflow", m_adminImagesPath + "application_workflow.gif");
			m_iconMap.put("profile", m_adminImagesPath + "content_col_icon.gif");
		}
		List<Resource> lister = XmlResourceLister.getResources(Application.class);
		if (lister == null || lister.isEmpty()) { return null; }
		StringBuilder sb = new StringBuilder();
		sb.append("folderOpenIcon:'").append(m_iconMap.get("application")).append("'");
		sb.append(",").append("folderCloseIcon:'").append(m_iconMap.get("application")).append("'");
		sb.append(",").append("children:").append("[\r\n");
		Application app = null;
		int idx = 0;
		Collections.sort(lister, new XmlResourceOrderByComparator());
		for (Resource x : lister) {
			app = (Application) x;
			if (app == null || !StringUtil.isBlank(app.getPUNID())) continue;
			sb.append(idx == 0 ? "" : ",");
			this.renderTreeView(app, sb);
			idx++;
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * 输出Application对象对应的TreeView json对象。
	 * 
	 * @param app
	 * @param sb
	 */
	protected void renderTreeView(Application app, StringBuilder sb) {
		sb.append("{"); // treeview begin
		sb.append("id:").append("'").append(app.getUNID()).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(app.getName())).append("'");
		sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(StringUtil.getValueString(app.getDescription(), ""))).append("'");
		sb.append(",").append("directory:'").append(app.getDirectory()).append("'");
		sb.append(",").append("folderOpenIcon:'").append(m_iconMap.get(app.getDirectory())).append("'");
		sb.append(",").append("folderCloseIcon:'").append(m_iconMap.get(app.getDirectory())).append("'");
		// data
		sb.append(",").append("data:").append("{"); // data begin
		sb.append("}"); // data end

		// children
		sb.append(",").append("children:").append("[\r\n"); // children begin

		ResourceContext rc = ResourceContext.getInstance();
		Resource tmp = null;
		// 表单
		String form = StringUtil.getValueString(app.getForm(), "");
		tmp = rc.getResource(form, Form.class);
		sb.append(tmp == null ? buildPlaceholderChild("表单", "form", "form") : buildResourceTVChild(tmp, "表单", null)).append(",\r\n");

		// 流程
		String wf = StringUtil.getValueString(app.getWorkflow(), "");
		tmp = rc.getResource(wf, Workflow.class);
		sb.append(tmp == null ? buildPlaceholderChild("流程", "workflow", "workflow") : buildResourceTVChild(tmp, "流程", null)).append(",\r\n");

		// 视图
		sb.append("{"); // view begin
		sb.append("id:").append("'").append("folder_of_view_" + (m_idIndex++)).append("'");
		sb.append(",").append("label:").append("'视图'");
		sb.append(",").append("directory:'view'");
		sb.append(",").append("leafIcon:'").append(m_iconMap.get("view")).append("'");
		sb.append(",").append("data:").append("{"); // data begin
		sb.append("}"); // data end
		sb.append("}").append(",\r\n"); // view end

		// 下级应用程序
		List<Reference> subs = null;
		subs = app.getSubApplications();
		if (subs != null && subs.size() > 0) {
			Application appsub = null;
			for (Reference y : subs) {
				appsub = ResourceContext.getInstance().getResource(y.getUnid(), Application.class);
				if (appsub == null) continue;
				this.renderTreeView(appsub, sb);
				sb.append(",").append("\r\n");
			}
		}

		// 配置文件表单
		String profileForm = StringUtil.getValueString(app.getProfileForm(), "");
		tmp = rc.getResource(profileForm, Form.class);
		sb.append(tmp == null ? buildPlaceholderChild("配置文件表单", "profile", "profile") : buildResourceTVChild(tmp, "配置文件表单", "profile"));

		sb.append("]"); // children end
		sb.append("}"); // treeview end

		return;
	}

	/**
	 * 
	 * @param r
	 * @param l
	 * @param iconDirectory
	 * @return
	 */
	protected String buildResourceTVChild(Resource r, String l, String iconDirectory) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("id:").append("'").append(r.getUNID()).append("'");
		sb.append(",").append("label:").append("'").append(l).append("'");
		sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(StringUtil.getValueString(r.getDescription(), r.getName()))).append("'");
		sb.append(",").append("directory:'").append(r.getDirectory()).append("'");
		sb.append(",").append("leafIcon:'").append(m_iconMap.get(iconDirectory == null ? r.getDirectory() : iconDirectory)).append("'");
		// data
		sb.append(",").append("data:").append("{"); // data begin
		sb.append("}"); // data end
		sb.append("}");
		return sb.toString();
	}

	/**
	 * 
	 * @param l
	 * @param directory
	 * @param iconDirectory
	 * @return
	 */
	protected String buildPlaceholderChild(String l, String directory, String iconDirectory) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("id:").append("'").append(UNIDProvider.getUNID()).append("'");
		sb.append(",").append("label:").append("'").append(l).append("'");
		sb.append(",").append("directory:'").append(directory).append("'");
		sb.append(",").append("placeholder:true");
		sb.append(",").append("leafIcon:'").append(m_iconMap.get(iconDirectory)).append("'");
		sb.append(",").append("folderOpenIcon:'").append(m_iconMap.get(iconDirectory)).append("'");
		sb.append(",").append("folderCloseIcon:'").append(m_iconMap.get(iconDirectory)).append("'");
		// data
		sb.append(",").append("data:").append("{"); // data begin
		sb.append("}"); // data end
		sb.append("}");
		return sb.toString();
	}
}

