/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.participant.CrossDeptContext;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.impl.AuthorityEntryInserter;
import com.tansuosoft.discoverx.dao.impl.AuthorityEntryRemover;
import com.tansuosoft.discoverx.model.AuthorityEntry;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.workflow.WorkflowForm;

/**
 * 管理用户交叉部门。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>目标用户的UNID，必须。</td>
 * </tr>
 * <tr>
 * <td>dept</td>
 * <td>交叉部门信息，必须。</td>
 * </tr>
 * <tr>
 * <td>del</td>
 * <td>0表示添加交叉部门，1表示删除，默认不提供则为0。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link Operation#returnResult(String, com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.OperationResult)}</td>
 * <td>通过message属性返回结果文本消息，默认返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * <strong>只有用户本身和有更改用户权限的管理员才能更改密码！</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "crossDept", name = "管理用户交叉部门", desc = "管理用户交叉部门的操作。")
public class CrossDept extends Operation {
	/**
	 * 缺省构造器。
	 */
	public CrossDept() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		User u = this.getUser();
		HttpServletRequest request = this.getHttpRequest();
		String unid = request.getParameter("unid");
		if (StringUtil.isBlank(unid)) {
			this.setLastError(new FunctionException("没有提供必要的用户信息！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		boolean canChange = false;
		boolean isDel = this.getParameterBoolFromAll("del", false);
		try {
			User user = null;
			if (u != null && unid.equalsIgnoreCase(u.getUNID())) {
				canChange = true;
				user = u;
			}
			if (!canChange) {
				Session s = Session.getSession(unid);
				if (s != null) user = Session.getUser(s);
				if (user == null) user = ResourceContext.getInstance().getResource(unid, User.class);
				if (user != null) canChange = SecurityHelper.authorize(u, user, SecurityLevel.Modify);
			}
			if (!canChange) {
				this.setLastError(new FunctionException("您没有设置交叉部门的权限！"));
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			}

			List<Integer> deptscs = WorkflowForm.getSecurityCodes(request, "dept");
			if (deptscs == null || deptscs.isEmpty()) {
				this.setLastError(new FunctionException("没有提供必要的部门信息！"));
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			}
			ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
			ParticipantTree pt = null;

			List<AuthorityEntry> toBeAddAues = new ArrayList<AuthorityEntry>();
			List<AuthorityEntry> toBeDelAues = new ArrayList<AuthorityEntry>(deptscs.size());
			AuthorityEntry aue = null;

			DBRequest dbr = null;
			DBRequest first = null;
			for (int sc : deptscs) {
				pt = ptp.getParticipantTree(sc);
				if (pt == null) continue;
				aue = new AuthorityEntry();
				aue.setUNID(pt.getUNID());
				aue.setDirectory("organization");
				aue.setSecurityCode(sc);
				aue.setName(pt.getName());
				dbr = null;
				if (isDel) {
					toBeDelAues.add(aue);
					dbr = new AuthorityEntryRemover();
				} else {
					toBeAddAues.add(aue);
					dbr = new AuthorityEntryInserter();
				}// else end
				dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, aue);
				dbr.setParameter(DBRequest.UNID_PARAM_NAME, user.getUNID());
				if (dbr != null) {
					if (first == null) {
						first = dbr;
					} else {
						first.setNextRequest(dbr);
					}
				}
			}// for end

			if (first != null) {
				first.sendRequest();
				if (first.getResultLong() <= 0L) throw new Exception("无法更改用户所属交叉部门信息！");
				// Authority au = user.getAuthority();
				// List<AuthorityEntry> aues = (au == null ? null : au.getAuthorityEntries());
				if (toBeAddAues.size() > 0) {
					// if (au == null) {
					// au = new Authority(user.getUNID());
					// au.setAuthorityEntries(toBeAddAues);
					// user.setAuthority(au);
					// } else {
					// if (aues == null) {
					// aues = new ArrayList<AuthorityEntry>(toBeAddAues);
					// au.setAuthorityEntries(aues);
					// } else {
					// aues.addAll(toBeAddAues);
					// }
					// }
					CrossDeptContext.getInstance().addCrossDeptList(user.getSecurityCode(), deptscs);
				} else if (toBeDelAues.size() > 0) {
					// if (aues != null && aues.size() > 0) {
					// for (AuthorityEntry x : toBeDelAues) {
					// for (int i = 0; i < aues.size(); i++) {
					// AuthorityEntry y = aues.get(i);
					// if (y != null && x.getSecurityCode() == y.getSecurityCode()) {
					// aues.remove(i);
					// break;
					// }
					// }
					// }// for end
					// }
					CrossDeptContext.getInstance().removeCrossDeptList(user.getSecurityCode(), deptscs);
				}
			} else {
				throw new Exception("没有更改任何交叉部门信息！");
			}
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		this.setLastMessage("交叉部门" + (isDel ? "删除" : "添加") + "成功！");
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

