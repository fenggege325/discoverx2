/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UriUtil;

/**
 * 下载指定应用程序并在下载完成后自动更新到系统的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>target</td>
 * <td>应用程序UNID，必须。</td>
 * </tr>
 * <tr>
 * <td>punid</td>
 * <td>应用程序所属文档UNID，必须。</td>
 * </tr>
 * <tr>
 * <td>ext</td>
 * <td>应用程序文件扩展名，必须。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>以{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}返回下载到的应用程序文件名，如果出现错误则使用它返回出错信息。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
@FunctionAttributes(type = 1, alias = "fetchProgram", name = "下载指定的更新程序包", desc = "下载指定的更新程序包。")
public class FetchProgram extends Operation {
	/**
	 * 缺省构造器。
	 */
	public FetchProgram() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		OutputStreamWriter wr = null;
		BufferedOutputStream os = null;
		InputStream is = null;
		String fn = null;
		try {
			String tempDir = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath();
			String tsid = ProgramsProvider.getTsid();
			if (tsid == null || tsid.length() == 0) throw new Exception("无法获取验证信息！");
			String target = request.getParameter("target");
			String punid = request.getParameter("punid");
			String ext = request.getParameter("ext");
			if (StringUtil.isBlank(target) || StringUtil.isBlank(punid) || StringUtil.isBlank(ext)) throw new Exception("没有提供有效的请求参数！");
			String add = "http://www.tensosoft.com/login";
			// 请求参数准备
			String data = UriUtil.encodeURIComponent("sid") + "=" + UriUtil.encodeURIComponent(tsid);
			data += "&" + UriUtil.encodeURIComponent("redirect") + "=" + UriUtil.encodeURIComponent("download?target=" + target + "&punid=" + punid);
			// 发送请求
			URL url = new URL(add);
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0; Discoverx2_ProgramsProvider))");
			conn.setUseCaches(false);
			conn.setAllowUserInteraction(true);
			CookieHandler.setDefault(new CookieManager());
			conn.setDoOutput(true);
			wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();
			wr.close();
			// 获取请求结果
			is = conn.getInputStream();
			byte b[] = new byte[1024];
			int read = 0;
			fn = punid + (ext.startsWith(".") ? ext : "." + ext);
			os = new BufferedOutputStream(new FileOutputStream(tempDir + fn));
			while ((read = is.read(b, 0, 1024)) != -1) {
				os.write(b, 0, read);
			}
			os.flush();
			os.close();
			is.close();
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		this.setLastMessage(fn);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}

}

