/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.List;

import com.tansuosoft.discoverx.bll.function.OperationParser;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.HtmlAttribute;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemGroup;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.Source;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 导出设计信息的基类。
 * 
 * @author coca@tensosoft.com
 */
public abstract class DesignExporter {
	/**
	 * 缺省构造器。
	 */
	public DesignExporter() {
	}

	/**
	 * 导出资源包含的特有普通属性设计信息。
	 * 
	 * @param r
	 * @return String
	 */
	protected abstract String exportSpecial(Resource r);

	/**
	 * 导出资源包含的特有集合和对象属性设计信息。
	 * 
	 * @param r
	 * @return String
	 */
	protected abstract String exportSpecialCollection(Resource r);

	/**
	 * 导出资源通用的设计信息。
	 * 
	 * @param r
	 * @return
	 */
	public String export(Resource r) {
		if (r == null) return "";
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(r instanceof ItemGroup ? Item.class : r.getClass());
		if (rd == null) return "";
		StringBuilder sb = new StringBuilder();

		sb.append("<h1>").append(rd.getName()).append("：").append(r.getName()).append("</h1>").append("\r\n");

		// sb.append("<fieldset>").append("\r\n");
		// sb.append("<legend>").append(rd.getName()).append(":").append(r.getName()).append("</legend>").append("\r\n");

		sb.append(this.exportH2("基本信息", r.getAlias() + "_basic"));
		sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
		sb.append("<colgroup><col width=\"240\"/><col/></colgroup>").append("\r\n");
		sb.append("<tr class=\"head\"><td>属性名</td><td>属性值</td></tr>").append("\r\n");
		sb.append("<tr><td>名称</td><td>").append(r.getName()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>描述</td><td>").append(StringUtil.getValueString(r.getDescription(), "")).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>别名</td><td>").append(r.getAlias()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>资源目录名</td><td>").append(r.getDirectory()).append("[").append(rd.getName()).append("]").append("</td></tr>").append("\r\n");
		sb.append("<tr><td>实体类名</td><td>").append(r.getClass().getName()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>UNID</td><td>").append(r.getUNID()).append("</td></tr>").append("\r\n");

		sb.append(this.exportSpecial(r));

		if (!StringUtil.isBlank(r.getPUNID())) sb.append("<tr><td>PUNID</td><td>").append(r.getPUNID()).append("</td></tr>").append("\r\n");
		if (!StringUtil.isBlank(r.getCategory())) sb.append("<tr><td>类别</td><td>").append(r.getCategory()).append("</td></tr>").append("\r\n");
		// sb.append("<tr><td>作者</td><td>").append(r.getCreator()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>创建时间</td><td>").append(r.getCreated()).append("</td></tr>").append("\r\n");
		// sb.append("<tr><td>最后修改者</td><td>").append(r.getModifier()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>最后修改时间</td><td>").append(r.getModified()).append("</td></tr>").append("\r\n");
		sb.append("<tr><td>排序号</td><td>").append(r.getSort()).append("</td></tr>").append("\r\n");
		Source s = r.getSource();
		sb.append("<tr><td>来源类型</td><td>").append(s == Source.Builtin ? "系统默认内置资源" : (s == Source.System ? "系统资源" : "用户资源")).append("</td></tr>").append("\r\n");
		sb.append("</table>").append("\r\n");

		// 安全信息
		sb.append(this.exportSecurity(r.getSecurity(), "配置权限", r.getAlias() + "_security"));

		// 附加文件
		List<Accessory> accs = r.getAccessories();
		if (accs != null && accs.size() > 0) {
			sb.append(this.exportH2("附加文件", r.getAlias() + "_accessories"));
			sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
			sb.append("<colgroup><col width=\"15%\"/><col width=\"12%\"><col width=\"40%\"/><col width=\"10%\"/><col/></colgroup>").append("\r\n");
			sb.append("<tr class=\"head\"><td>名称</td><td>类型</td><td>路径</td><td>大小(KB)</td><td>说明</td></tr>").append("\r\n");
			for (Accessory x : accs) {
				if (x == null) continue;
				sb.append("<tr><td>").append(x.getName()).append("</td><td>").append(x.getAccessoryTypeName()).append("</td><td>").append(x.getFileName()).append("</td><td>").append(x.getSize()).append("</td><td>").append(StringUtil.getValueString(x.getDescription(), "")).append("</td></tr>").append("\r\n");
			}
			sb.append("</table>").append("\r\n");
		}

		// 额外参数
		List<Parameter> ps = r.getParameters();
		if (ps != null && ps.size() > 0) {
			sb.append(this.exportH2("额外参数", r.getAlias() + "_parameters"));
			sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
			sb.append("<colgroup><col width=\"10%\"/><col width=\"40%\"/><col/></colgroup>").append("\r\n");
			sb.append("<tr class=\"head\"><td>参数名</td><td>参数值</td><td>说明</td></tr>").append("\r\n");
			for (Parameter x : ps) {
				if (x == null || StringUtil.isBlank(x.getValue())) continue;
				sb.append("<tr><td>").append(x.getName()).append("</td><td>").append(x.getValue()).append("</td><td>").append(StringUtil.getValueString(x.getDescription(), "")).append("</td></tr>").append("\r\n");
			}
			sb.append("</table>").append("\r\n");
		}
		// 特定集合/对象属性
		sb.append(this.exportSpecialCollection(r));
		// sb.append("</fieldset>").append("\r\n");

		return sb.toString();
	}

	/**
	 * 导出安全控制信息设计信息。
	 * 
	 * @param security
	 * @param h2
	 * @param h2name
	 * @return String
	 */
	protected String exportSecurity(Security security, String h2, String h2name) {
		if (security == null || !security.hasValidEntries()) return "";
		StringBuilder sb = new StringBuilder();

		sb.append(this.exportH2(h2, h2name));
		sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
		sb.append("<colgroup><col width=\"20%\"/><col/></colgroup>").append("\r\n");
		sb.append("<tr class=\"head\"><td>参与者</td><td>权限级别</td></tr>").append("\r\n");
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree pt = null;
		for (SecurityEntry x : security.getSecurityEntries()) {
			if (x == null) continue;
			int sc = x.getSecurityCode();
			pt = ptp.getParticipantTree(sc);
			sb.append("<tr><td>").append(sc);
			if (pt != null) sb.append("(").append(pt.getName()).append(")");
			sb.append("</td><td>").append(x.getSecurityLevel()).append("</td>").append("\r\n");
		}
		sb.append("</table>").append("\r\n");

		return sb.toString();
	}

	/**
	 * 导出操作列表设计信息。
	 * 
	 * @param oprs
	 * @param h2
	 * @param h2name
	 * @return String
	 */
	protected String exportOperations(List<Operation> oprs, String h2, String h2name) {
		if (oprs == null || oprs.isEmpty()) return "";
		StringBuilder sb = new StringBuilder();

		sb.append(this.exportH2(h2, h2name));

		sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
		sb.append("<colgroup><col width=\"6%\"/><col width=\"12%\"/><col width=\"18%\"/><col width=\"15%\"/><col width=\"5%\"/><col/><col width=\"25%\"/></colgroup>").append("\r\n");
		sb.append("<tr class=\"head\"><td>标题</td><td>表达式</td><td>显示表达式</td><td>图标</td><td>排序号</td><td>描述</td><td>参数</td></tr>").append("\r\n");

		OperationParser operationParser = null;
		String icon = null;
		String title = null;
		String desc = null;
		String exp = null;
		String vexp = null;
		int sort = 0;
		String params = "";
		for (Operation x : oprs) {
			if (x == null) continue;
			operationParser = null;
			try {
				operationParser = new OperationParser(x.getExpression(), null);
			} catch (Exception ex) {
				operationParser = null;
			}
			sort = 0;
			title = "";
			exp = "";
			vexp = "";
			desc = "";
			icon = "";
			params = "";
			if (operationParser == null) {
				title = StringUtil.getValueString(x.getTitle(), "");
				icon = StringUtil.getValueString(x.getIcon(), "");
				desc = StringUtil.getValueString(x.getDescription(), "");
				exp = StringUtil.getValueString(x.getExpression(), "");
				vexp = StringUtil.getValueString(x.getVisibleExpression(), "");
				sort = x.getSort();
			} else {
				title = x.getTitle();
				if (title == null || title.length() == 0) title = operationParser.getFunction().getName();
				icon = x.getIcon();
				if (icon == null || icon.length() == 0) icon = operationParser.getOperation().getParameterString("icon", "");
				exp = StringUtil.getValueString(this.processExpression(x.getExpression()), "");
				vexp = StringUtil.getValueString(this.processExpression(x.getVisibleExpression()), "");
				desc = x.getDescription();
				if (desc == null || desc.length() == 0) desc = operationParser.getFunction().getDescription();
				sort = x.getSort();
				String js = operationParser.getJSFunction(null);
				if (!StringUtil.isBlank(js)) params += "js:" + js + "<br/>";
				String next = operationParser.getNextOperation(null);
				if (!StringUtil.isBlank(next)) params += "next:" + next + "<br/>";
				String ru = operationParser.getResultUrl(null);
				if (!StringUtil.isBlank(ru)) params += "resultUrl:" + ru + "<br/>";
				String ui = operationParser.getUiPath(null);
				if (!StringUtil.isBlank(ui)) params += "uiPath:" + ui + "<br/>";
				String f = operationParser.getWebRequestForm(null);
				if (!StringUtil.isBlank(f)) params += "form:" + f + "<br/>";

				sb.append("</tr>");
				sb.append("<td>").append(title).append("</td>");
				sb.append("<td>").append(exp).append("</td>");
				sb.append("<td>").append(vexp).append("</td>");
				sb.append("<td>").append(icon).append("</td>");
				sb.append("<td>").append(sort).append("</td>");
				sb.append("<td>").append(desc).append("</td>");
				sb.append("<td>").append(params).append("</td>");
				sb.append("</tr>").append("\r\n");
			}
		}
		sb.append("</table>").append("\r\n");

		return sb.toString();
	}

	/**
	 * 导出html属性列表设计信息。
	 * 
	 * @param atts
	 * @param h2
	 * @param h2name
	 * @return String
	 */
	protected String exportHtmlAttributes(List<HtmlAttribute> atts, String h2, String h2name) {
		if (atts == null || atts.isEmpty()) return "";
		StringBuilder sb = new StringBuilder();

		sb.append(this.exportH2(h2, h2name));

		sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
		sb.append("<colgroup><col width=\"38%\"/><col/></colgroup>").append("\r\n");
		sb.append("<tr class=\"head\"><td>属性名</td><td>属性值</td></tr>").append("\r\n");
		for (HtmlAttribute x : atts) {
			if (x == null) continue;
			sb.append("<tr><td>").append(x.getName()).append("</td><td>").append(x.getValue()).append("</td></tr>").append("\r\n");
		}
		sb.append("</table>").append("\r\n");

		return sb.toString();
	}

	/**
	 * 导出事件处理程序列表设计信息。
	 * 
	 * @param ehi
	 * @param h2
	 * @param h2name
	 * @return String
	 */
	protected String exportEventHandlers(List<EventHandlerInfo> ehi, String h2, String h2name) {
		if (ehi == null || ehi.isEmpty()) return "";
		StringBuilder sb = new StringBuilder();

		sb.append(this.exportH2(h2, h2name));

		sb.append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">").append("\r\n");
		sb.append("<colgroup><col width=\"10%\"/><col width=\"10%\"/><col width=\"70%\"/><col/></colgroup>").append("\r\n");
		sb.append("<tr class=\"head\"><td>名称</td><td>类型</td><td>实现类</td><td>序号</td></tr>").append("\r\n");
		for (EventHandlerInfo x : ehi) {
			if (x == null || StringUtil.isBlank(x.getEventHandler())) continue;
			sb.append("<tr><td>").append(StringUtil.getValueString(x.getEventHandlerName(), "<无>")).append("</td><td>").append(x.getEventHandlerType()).append("</td><td>").append(x.getEventHandler()).append("</td><td>").append(x.getSort()).append("</td></tr>").append("\r\n");
		}
		sb.append("</table>").append("\r\n");

		return sb.toString();
	}

	/**
	 * 处理表达式。
	 * 
	 * @param expression
	 * @return String
	 */
	protected String processExpression(String expression) {
		if (expression == null || expression.length() == 0) return "";
		return expression;
	}

	/**
	 * 处理参与者。
	 * 
	 * @param p
	 * @return String
	 */
	protected String processParticipant(Participant p) {
		if (p == null) return "";
		String ptts[] = { "个人", "部门", "群组", "角色" };
		int pt = p.getParticipantType().getIntValue();
		int i = 0;
		int d = pt / 2;
		while (d != 0) {
			i++;
			d = d / 2;
		}
		return String.format("%1$s(选中与否:%2$s,类型：%3$s)", p.getName(), p.getSelected() ? "是" : "否", ptts[i]);
	}

	/**
	 * 导出第二级别的标题信息。
	 * 
	 * @param h2
	 * @param h2name
	 * @return
	 */
	protected String exportH2(String h2, String h2name) {
		StringBuilder sb = new StringBuilder();
		sb.append("<h2><a name=\"").append(h2name).append("\">").append(h2).append("</a></h2>").append("\r\n");
		return sb.toString();
	}
}

