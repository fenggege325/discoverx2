/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.common.license.Version;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 导出系统错误日志的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>incuserinfo</td>
 * <td>0或1表示是否在导出包中同时提供用户信息，默认为0。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回输出zip文件内容的地址，如果出现错误则返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ExportLogs extends Operation {
	/**
	 * 缺省构造器。
	 */
	public ExportLogs() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		String userinfoFN = null;
		try {
			User u = this.getUser();
			if (!u.isSystemBuiltinAdmin()) throw new Exception("您没有执行此操作的权限！");
			String tempFolder = AccessoryPathHelper.getAbsoluteTempAccessoryServerPath();
			File p = new File(tempFolder);
			p.mkdirs();
			String target = String.format("%1$slogs%2$s.zip", tempFolder, new DateTime().toString("yyyyMMddHHmmss"));

			boolean incuserinfo = this.getParameterBoolFromAll("incuserinfo", false);
			if (incuserinfo) {
				userinfoFN = String.format("%1$suserinfo.dat", tempFolder);
				FileWriter fw = null;
				StringBuilder sb = new StringBuilder();
				sb.append("{");
				sb.append("belong:").append("'").append(StringUtil.encode4Json(CommonConfig.getInstance().getBelong())).append("'");
				sb.append(",\r\n").append("provider:").append("'").append(StringUtil.encode4Json(CommonConfig.getInstance().getProvider())).append("'");
				sb.append(",\r\n").append("systemName:").append("'").append(StringUtil.encode4Json(CommonConfig.getInstance().getSystemName())).append("'");

				sb.append(",\r\n").append("l:").append("1");
				sb.append(",\r\n").append("ldt:").append("''");
				sb.append(",\r\n").append("lt:1");
				sb.append(",\r\n").append("idt:").append("''");

				Version v = Version.getInstance();
				sb.append(",\r\n").append("bdt:").append("'").append(v.getBuildDate()).append("'");
				sb.append(",\r\n").append("v:").append("'").append(v.getMajorNumber()).append(".").append(v.getMinorNumber()).append(".").append(v.getBuildNumber()).append("'");
				List<StringPair> ps = v.getPatchs();
				if (ps != null && ps.size() > 0) {
					sb.append(",\r\n").append("patches:[");
					int pidx = 0;
					for (StringPair sp : ps) {
						if (sp == null) continue;
						String pid = sp.getKey();
						String dt = sp.getValue();
						sb.append(pidx == 0 ? "" : ",\r\n").append("{");
						sb.append("pid:'").append(StringUtil.encode4Json(pid)).append("'");
						sb.append(",").append("pidt:").append("'").append(dt).append("'");
						sb.append("}");
						pidx++;
					}
					sb.append("]");
				}
				sb.append("}");
				try {
					fw = new FileWriter(userinfoFN);
					fw.write(sb.toString());
					fw.flush();
					fw.close();
				} catch (Exception ex) {
					FileLogger.error(ex);
					incuserinfo = false;
				} finally {
					fw.close();
				}
			}

			FileLogger.flush();
			String installationPath = CommonConfig.getInstance().getInstallationPath();
			List<String> files = new ArrayList<String>();
			String logsPath = String.format("%1$slogs%2$s", installationPath, File.separator);
			File logsFile = new File(logsPath);
			if (logsFile.exists() && logsFile.isDirectory()) {
				String[] fs = logsFile.list();
				if (fs != null && fs.length > 0) {
					for (String f : fs) {
						if (!f.endsWith(".log")) continue;
						files.add(logsPath + f);
					}
				}
			}
			if (incuserinfo) files.add(userinfoFN);
			if (files.size() > 0) {
				byte[] buf = new byte[1024];
				ZipOutputStream out = new ZipOutputStream(new FileOutputStream(target));
				String entryName = null;
				for (String s : files) {
					if (s.startsWith(tempFolder)) {
						entryName = s.replace(tempFolder, "");
					} else {
						entryName = s.replace(logsPath, "");
					}
					ZipEntry entry = new ZipEntry(entryName);
					out.putNextEntry(entry);
					FileInputStream in = new FileInputStream(s);
					int len;
					while ((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
					}

					out.closeEntry();
					in.close();
				}
				out.close();
			}
			// this.setLastMessage(target);
			OperationResult result = new OperationResult();
			result.setResultUrl(request.getContextPath() + "/download?fn=" + target.replace(tempFolder, ""));
			return result;
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		} finally {
			if (userinfoFN != null && userinfoFN.length() > 0) {
				File f = new File(userinfoFN);
				if (f.exists() && !f.delete()) f.deleteOnExit();
			}
		}
	}
}

