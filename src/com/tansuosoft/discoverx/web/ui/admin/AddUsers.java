/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.dao.impl.AuthorityEntryInserter;
import com.tansuosoft.discoverx.dao.impl.GroupInserter;
import com.tansuosoft.discoverx.dao.impl.OrganizationInserter;
import com.tansuosoft.discoverx.dao.impl.RoleInserter;
import com.tansuosoft.discoverx.dao.impl.UserInserter;
import com.tansuosoft.discoverx.model.AccountType;
import com.tansuosoft.discoverx.model.Authority;
import com.tansuosoft.discoverx.model.AuthorityEntry;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.Organization;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Securer;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 批量添加部门、群组、角色、用户的操作。
 * 
 * @author coca@tensosoft.com
 */
public class AddUsers extends Operation {
	private Map<String, Securer> orgs = new HashMap<String, Securer>();
	private Map<String, Securer> roles = new HashMap<String, Securer>();
	private Map<String, Securer> groups = new HashMap<String, Securer>();
	private ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
	private String rootOrgName = ptp.getRoot().getName();
	private String rootOrgUnid = OrganizationsContext.getInstance().getContextUserOrgUnid();

	/**
	 * 重载：实现功能。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		StringBuilder msg = new StringBuilder();
		try {
			User user = this.getUser();
			if (!SecurityHelper.checkUserRole(user, Role.ROLE_SETTINGSADMIN_SC)) throw new Exception("您必须是管理员才能执行此操作！");
			HttpServletRequest req = this.getHttpRequest();
			final char sep = '~';
			String names[] = StringUtil.splitString(req.getParameter("name"), sep);
			if (names == null || names.length == 0) throw new Exception("没有提供用户姓名！");
			String emails[] = StringUtil.splitString(req.getParameter("email"), sep);
			if (emails == null || emails.length != names.length) throw new Exception("没有提供做为登录账号的电子邮件！");
			String depts[] = StringUtil.splitString(req.getParameter("dept"), sep);
			if (depts == null || depts.length != names.length) throw new Exception("没有提供用户部门！");
			String roleNames[] = StringUtil.splitString(req.getParameter("role"), sep);
			if (roleNames == null || roleNames.length != names.length) throw new Exception("提供的角色数不匹配！");
			String groupNames[] = StringUtil.splitString(req.getParameter("group"), sep);
			if (groupNames == null || groupNames.length != names.length) throw new Exception("提供的群组数不匹配！");

			addOrgs(depts);
			addRoles(roleNames);
			addGroups(groupNames);

			List<User> users = new ArrayList<User>();
			String cn = null;
			String orgName = null;
			String fn = null;
			String email = null;
			GenericPair<String, Integer> g = null;
			GenericPair<String, Integer> r = null;
			String strs[] = null;
			for (int i = 0; i < names.length; i++) {
				cn = names[i];
				if (cn == null || cn.isEmpty()) continue;
				if (depts[i] == null || depts[i].length() == 0) continue;
				email = emails[i];
				if (email == null || email.isEmpty()) continue;
				if (ptp.getParticipantTree(email) != null) {
					msg.append(msg.length() > 0 ? "\r\n" : "").append("用户“").append(cn).append("”对应的登录账号“").append(email).append("”已经存在。");
					continue;
				}
				orgName = rootOrgName + User.SEPARATOR + depts[i].replace('/', '\\');
				if (orgName.endsWith("\\")) orgName = orgName.substring(0, orgName.length() - 1);
				if (!checkOrgFn(orgName)) fn = rootOrgName + User.SEPARATOR + cn;
				else fn = orgName + User.SEPARATOR + cn;
				if (ptp.getParticipantTree(fn) != null) {
					msg.append(msg.length() > 0 ? "\r\n" : "").append("用户“").append(fn).append("”对应的登录账号已经存在。");
					continue;
				}
				User newUser = new User();
				if (newUser.getSecurityCode() == 0) newUser.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
				newUser.setAccountType(AccountType.ValidAccount.getIntValue() + AccountType.User.getIntValue());
				newUser.setName(fn);
				newUser.setAlias(email);
				newUser.setPassword(User.DEFAULT_PASSWORD);
				newUser.setSort(newUser.getSecurityCode());
				newUser.setPUNID(rootOrgUnid);
				newUser.setAuthority(new Authority(newUser.getUNID()));
				newUser.getAuthority().setAuthorityEntries(new ArrayList<AuthorityEntry>());
				strs = toArray(roleNames[i]);
				if (strs != null && strs.length > 0) {
					for (String roleName : strs) {
						if (roleName == null || roleName.isEmpty()) continue;
						r = getRole(roleName);
						if (r == null) continue;
						AuthorityEntry ae = new AuthorityEntry();
						ae.setDirectory("role");
						ae.setSecurityCode(r.getValue());
						ae.setUNID(r.getKey());
						newUser.getAuthority().getAuthorityEntries().add(ae);
					}
				}
				strs = toArray(groupNames[i]);
				if (strs != null && strs.length > 0) {
					for (String groupName : strs) {
						if (groupName == null || groupName.isEmpty()) continue;
						g = getGroup(groupName);
						if (g == null) continue;
						AuthorityEntry ae = new AuthorityEntry();
						ae.setDirectory("group");
						ae.setSecurityCode(g.getValue());
						ae.setUNID(g.getKey());
						newUser.getAuthority().getAuthorityEntries().add(ae);
					}
				}
				users.add(newUser);
			}
			List<DBRequest> dbrs = new ArrayList<DBRequest>();
			for (Securer s : orgs.values()) {
				if (s == null || s instanceof Participant) continue;
				DBRequest dbr = new OrganizationInserter();
				dbr.setResource((Organization) s);
				dbrs.add(dbr);
			}
			for (Securer s : roles.values()) {
				if (s == null || s instanceof Participant) continue;
				DBRequest dbr = new RoleInserter();
				dbr.setResource((Role) s);
				dbrs.add(dbr);
			}
			for (Securer s : groups.values()) {
				if (s == null || s instanceof Participant) continue;
				DBRequest dbr = new GroupInserter();
				dbr.setResource((Group) s);
				dbrs.add(dbr);
			}
			for (User u : users) {
				DBRequest dbr = new UserInserter();
				dbr.setResource(u);
				dbrs.add(dbr);
				for (AuthorityEntry x : u.getAuthority().getAuthorityEntries()) {
					if (x == null) continue;
					DBRequest dbrAei = new AuthorityEntryInserter();
					dbrAei.setParameter(DBRequest.ENTITY_PARAM_NAME, x);
					dbrAei.setParameter(DBRequest.UNID_PARAM_NAME, u.getUNID());
					dbrAei.setParameter("directory", u.getDirectory());
					dbrs.add(dbrAei);
				}
			}
			if (dbrs.size() == 0) throw new Exception("没有生成任何新登录账号。\r\n" + msg.toString());
			DBRequest first = dbrs.get(0);
			for (int i = 1; i < dbrs.size(); i++) {
				first.setNextRequest(dbrs.get(i));
			}
			first.setUseTransaction(true);
			first.sendRequest();
			ParticipantTreeProvider.reload();
		} catch (Exception ex) {
			if (ex instanceof NullPointerException) FileLogger.error(ex);
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		if (msg.length() > 0) this.setLastMessage("生成账号结果:\r\n" + msg.toString());
		else this.setLastMessage("生成账号成功！");
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}

	/**
	 * 逗号分隔的内容转为数组。
	 * 
	 * @param str
	 * @return
	 */
	protected String[] toArray(String str) {
		if (str == null || str.isEmpty()) return null;
		return StringUtil.splitString(str.replace(' ', ',').replace(';', ','), ',');
	}

	/**
	 * 解析群组。
	 * 
	 * @param strs
	 */
	protected void addGroups(String strs[]) {
		if (strs == null || strs.length == 0) return;
		Group g = null;
		Participant pt = null;
		for (String str : strs) {
			if (str == null || str.isEmpty()) continue;
			String names[] = StringUtil.splitString(str.replace(' ', ',').replace(';', ','), ',');
			for (String name : names) {
				if (name == null || name.isEmpty()) continue;
				pt = ptp.getParticipantTree(name);
				if (pt != null) {
					groups.put(name, pt);
					continue;
				}
				g = new Group();
				g.setName(name);
				g.setPUNID(rootOrgUnid);
				if (g.getSecurityCode() == 0) g.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
				g.setAlias("grp" + g.getSecurityCode());
				g.setSort(g.getSecurityCode());
				groups.put(name, g);
			}
		}
	}

	/**
	 * 解析角色。
	 * 
	 * @param strs
	 */
	protected void addRoles(String strs[]) {
		if (strs == null || strs.length == 0) return;
		Role r = null;
		Participant pt = null;
		for (String str : strs) {
			if (str == null || str.isEmpty()) continue;
			String names[] = StringUtil.splitString(str.replace(' ', ',').replace(';', ','), ',');
			for (String name : names) {
				if (name == null || name.isEmpty()) continue;
				pt = ptp.getParticipantTree(name);
				if (pt != null) {
					roles.put(name, pt);
					continue;
				}
				r = new Role();
				r.setName(name);
				r.setPUNID(rootOrgUnid);
				if (r.getSecurityCode() == 0) r.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
				r.setAlias("role" + r.getSecurityCode());
				r.setSort(r.getSecurityCode());
				roles.put(name, r);
			}
		}
	}

	/**
	 * 解析部门。
	 * 
	 * @param names
	 */
	protected void addOrgs(String names[]) {
		if (names == null || names.length == 0) return;
		Organization o = null;
		Participant pt = null;
		String fn = null;
		for (String name : names) {
			if (name == null || name.isEmpty()) continue;
			fn = rootOrgName + User.SEPARATOR + name.replace('/', '\\');
			pt = ptp.getParticipantTree(fn);
			if (pt != null) {
				orgs.put(fn, pt);
				continue;
			}
			o = new Organization();
			o.setFullName(fn);
			o.setName(StringUtil.stringRightBack(fn, User.SEPARATOR));
			o.setPUNID(rootOrgUnid);
			if (o.getSecurityCode() == 0) o.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
			o.setAlias("org" + o.getSecurityCode());
			o.setSort(o.getSecurityCode());
			orgs.put(fn, o);
		}
	}

	/**
	 * 根据名称获取群组unid和安全编码。
	 * 
	 * @param name
	 * @return
	 */
	protected GenericPair<String, Integer> getGroup(String name) {
		Securer s = groups.get(name);
		if (s == null) return null;
		if (s instanceof Participant) {
			Participant pt = (Participant) s;
			return new GenericPair<String, Integer>(pt.getUNID(), pt.getSecurityCode());
		}
		Group x = (Group) s;
		return new GenericPair<String, Integer>(x.getUNID(), x.getSecurityCode());
	}

	/**
	 * 根据名称获取角色unid和安全编码。
	 * 
	 * @param name
	 * @return
	 */
	protected GenericPair<String, Integer> getRole(String name) {
		Securer s = roles.get(name);
		if (s == null) return null;
		if (s instanceof Participant) {
			Participant pt = (Participant) s;
			return new GenericPair<String, Integer>(pt.getUNID(), pt.getSecurityCode());
		}
		Role x = (Role) s;
		return new GenericPair<String, Integer>(x.getUNID(), x.getSecurityCode());
	}

	/**
	 * 根据部门全名检查部门是否存在。
	 * 
	 * @param name
	 * @return
	 */
	protected boolean checkOrgFn(String name) {
		Securer s = orgs.get(name);
		if (s == null) return false;
		return true;
	}
}

