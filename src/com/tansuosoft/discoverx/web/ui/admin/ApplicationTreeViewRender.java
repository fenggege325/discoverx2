/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.bll.view.XmlResourceOrderByComparator;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 输出供管理通道应用程序管理中使用的TreeView对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ApplicationTreeViewRender implements HtmlRender {
	/**
	 * 缺省构造器。
	 */
	public ApplicationTreeViewRender() {
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		List<Resource> lister = XmlResourceLister.getResources(Application.class);
		if (lister == null || lister.isEmpty()) { return null; }
		StringBuilder sb = new StringBuilder();
		sb.append("children:").append("[");
		Application app = null;
		int idx = 0;
		Collections.sort(lister, new XmlResourceOrderByComparator());
		for (Resource x : lister) {
			app = (Application) x;
			if (app == null || !StringUtil.isBlank(app.getPUNID())) continue;
			sb.append(idx == 0 ? "" : ",");
			this.renderTreeView(app, sb);
			idx++;
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * 输出Application对象对应的TreeView json对象。
	 * 
	 * @param app
	 * @param sb
	 */
	protected void renderTreeView(Application app, StringBuilder sb) {
		sb.append("{"); // treeview
		sb.append("id:").append("'").append(app.getUNID()).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(app.getName())).append("'");
		sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(StringUtil.getValueString(app.getDescription(), ""))).append("'");

		// data
		sb.append(",").append("data:").append("{");

		Resource tmp = null;
		String form = StringUtil.getValueString(app.getForm(), "");
		if (form != null && form.length() > 0) {
			tmp = ResourceContext.getInstance().getResource(form, Form.class);
			if (tmp == null) form = "";
		}

		String wf = StringUtil.getValueString(app.getWorkflow(), "");
		if (wf != null && wf.length() > 0) {
			tmp = ResourceContext.getInstance().getResource(wf, Workflow.class);
			if (tmp == null) wf = "";
		}
		sb.append("form:").append("'").append(form).append("'");
		sb.append(",").append("workflow:").append("'").append(wf).append("'");

		int idx = 0;
		// views
		List<Reference> views = null;
		views = app.getViews();
		sb.append(",").append("views:").append("[");
		if (views != null) {
			for (Reference y : views) {
				sb.append(idx == 0 ? "" : ",");
				sb.append("{");
				sb.append("title:").append("'").append(y.getTitle()).append("'");
				sb.append(",").append("unid:").append("'").append(y.getUnid()).append("'");
				sb.append(",").append("sort:").append(y.getSort());
				sb.append("}");
				idx++;
			}
		}
		sb.append("]"); // views

		sb.append("}"); // data

		// subApplications
		idx = 0;
		List<Reference> subs = null;
		subs = app.getSubApplications();
		sb.append(",").append("children:").append("[");
		if (subs != null) {
			Application appsub = null;
			ResourceContext rc = ResourceContext.getInstance();
			for (Reference y : subs) {
				appsub = (Application) rc.getResource(y.getUnid(), Application.class);
				if (appsub != null) {
					sb.append(idx == 0 ? "" : ",");
					this.renderTreeView(appsub, sb);
					idx++;
				}
			}
		}
		sb.append("]"); // children of subApplications

		sb.append("}"); // treeview

		return;
	}
}

