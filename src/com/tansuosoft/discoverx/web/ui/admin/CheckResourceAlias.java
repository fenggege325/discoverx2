/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.db.Database;
import com.tansuosoft.discoverx.db.DatabaseFactory;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 检查资源别名的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>要检查别名的资源的资源目录，必须</td>
 * </tr>
 * <tr>
 * <td>alias</td>
 * <td>要检查的资源别名，必须</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>资源UNID，必须</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回地址</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}</td>
 * <td>如果别名已经存在，则message结果为'true'。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class CheckResourceAlias extends Operation {
	/**
	 * 缺省构造器。
	 */
	public CheckResourceAlias() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		HttpServletRequest request = this.getHttpRequest();
		String directory = request.getParameter("directory");
		String alias = request.getParameter("alias");
		String unid = request.getParameter("unid");
		if (StringUtil.isBlank(directory) || StringUtil.isBlank(alias) || StringUtil.isBlank(unid)) {
			this.setLastError(new FunctionException("无法获取“" + directory + "”对应的资源描述！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
		if (rd == null) {
			this.setLastError(new FunctionException("无法获取“" + directory + "”对应的资源描述！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
		boolean found = false;
		if (rd.getXmlStore()) {
			List<Resource> list = XmlResourceLister.getResources(directory);
			if (list != null && !list.isEmpty()) {
				for (Resource x : list) {
					if (alias.equalsIgnoreCase(x.getAlias()) && !unid.equalsIgnoreCase(x.getUNID())) {
						found = true;
						break;
					}
				}
			}
		} else {
			Database db = DatabaseFactory.getDatabase();
			try {
				String tableName = null;
				if (rd != null) tableName = rd.getDbTableName();
				Object ret = db.executeScalar("select c_unid from " + tableName + " where c_alias='" + alias + "'");
				if (ret != null && !unid.equalsIgnoreCase(ret.toString())) {
					found = true;
				}
			} catch (SQLException e) {
				this.setLastError(e);
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
			}
		}
		response.setMessage(found ? "true" : "false");
		this.setLastAJAXResponse(response);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}
}

