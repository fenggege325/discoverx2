/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui.admin;

import java.io.StringWriter;
import java.util.List;

import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.serialization.JsonSerializer;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 输出供管理通道流程管理使用的TreeView对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowTreeViewRender implements HtmlRender {
	private String m_xml = null;

	/**
	 * 接收流程内容xml的构造器。
	 * 
	 * @param xml
	 */
	public WorkflowTreeViewRender(String xml) {
		this.m_xml = xml;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		if (this.m_xml == null || this.m_xml.length() == 0) return null;
		XmlDeserializer dser = new XmlDeserializer();
		Workflow wf = new Workflow();
		dser.setTarget(wf);
		dser.deserialize(m_xml, Workflow.class);

		List<Activity> acts = wf.getActivities();

		StringBuilder sb = new StringBuilder();
		sb.append("id:").append("'tvwf'");
		sb.append(",nodeType: 0, expanded: true");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(wf.getName())).append("'");
		sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(wf.getDescription())).append("'");
		sb.append(",").append("data:").append("{");
		StringWriter sw = new StringWriter();
		JsonSerializer ser = new JsonSerializer();
		ser.serialize(wf, sw);
		sb.append(sw.toString());
		sb.append("}"); // data end

		sb.append(",").append("children:").append("[");
		// 基本信息
		sb.append("{"); // treeview
		sb.append("id:").append("'tvi_basic").append("'");
		sb.append(",").append("label:").append("'基本信息'");
		sb.append(",").append("desc:").append("'设置流程基本信息'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editorId:").append("'workfloweditor'");
		sb.append(",").append("editor:").append("WFPropActions.setWorkflow");
		sb.append("}"); // data end
		sb.append("}"); // treeview
		// 额外参数
		sb.append(",").append("{"); // treeview
		sb.append("id:").append("'tvi_parameters").append("'");
		sb.append(",").append("label:").append("'流程参数'");
		sb.append(",").append("desc:").append("'设置流程的额外参数'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editor:").append("WFPropActions.setWFParameters");
		sb.append("}"); // data end
		sb.append("}"); // treeview
		// 事件
		sb.append(",").append("{"); // treeview
		sb.append("id:").append("'tvi_eventHandlers").append("'");
		sb.append(",").append("label:").append("'流程事件'");
		sb.append(",").append("desc:").append("'设置流程运行时触发的事件'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editor:").append("WFPropActions.setWFEventHandlers");
		sb.append("}"); // data end
		sb.append("}"); // treeview
		// 权限
		sb.append(",").append("{"); // treeview
		sb.append("id:").append("'tvi_security").append("'");
		sb.append(",").append("label:").append("'权限控制'");
		sb.append(",").append("desc:").append("'设置流程配置/使用权限'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editor:").append("WFPropActions.setWFSecurity");
		sb.append("}"); // data end
		sb.append("}"); // treeview

		// activities
		if (acts != null && acts.size() > 0) {
			int activityIndex = 0;
			for (Activity x : acts) {
				if (x == null) continue;
				sb.append(",");
				renderActivityTreeView(x, activityIndex, sb);
				activityIndex++;
			}
		}
		sb.append("]"); // children end

		return sb.toString();
	}

	/**
	 * 输出Activity对象对应的TreeView json对象。
	 * 
	 * @param activity
	 * @param activityIndex
	 * @param sb
	 */
	protected void renderActivityTreeView(Activity activity, int activityIndex, StringBuilder sb) {
		if (activity == null || sb == null) return;
		sb.append("{"); // treeview

		String unid = activity.getUNID();

		// 环节属性
		sb.append("id:").append("'").append(unid).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(activity.getName())).append("'");
		sb.append(",").append("desc:").append("'").append(StringUtil.encode4Json(StringUtil.getValueString(activity.getDescription(), ""))).append("'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editor:").append("WFPropActions.setActivity");
		sb.append(",").append("activityIndex:").append(activityIndex);
		sb.append("}"); // data end

		// 子节点
		sb.append(",").append("children:").append("[");

		// 环节参与者设置
		sb.append("{"); // treeview
		sb.append("id:").append("'tvi0_").append(unid).append("'");
		sb.append(",").append("label:").append("'参与者范围'");
		sb.append(",").append("desc:").append("'设置环节的审批人范围'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editor:").append("WFPropActions.setActivityParticipants");
		sb.append("}"); // data end
		sb.append("}"); // treeview
		// 环节操作设置
		sb.append(",").append("{"); // treeview
		sb.append("id:").append("'tvi1_").append(unid).append("'");
		sb.append(",").append("label:").append("'环节操作'");
		sb.append(",").append("desc:").append("'设置本环节参与者可执行的操作'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editor:").append("WFPropActions.setActivityOperations");
		sb.append("}"); // data end
		sb.append("}"); // treeview
		// 环节转换设置
		sb.append(",").append("{"); // treeview
		sb.append("id:").append("'tvi2_").append(unid).append("'");
		sb.append(",").append("label:").append("'环节转换'");
		sb.append(",").append("desc:").append("'设置环节出口（即本环节可转换到哪些其它环节）选项'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editor:").append("WFPropActions.setActivityTransitions");
		sb.append("}"); // data end
		sb.append("}"); // treeview
		// 自动流转设置
		sb.append(",").append("{"); // treeview
		sb.append("id:").append("'tvi3_").append(unid).append("'");
		sb.append(",").append("label:").append("'自动流转'");
		sb.append(",").append("desc:").append("'设置环节自动流转选项'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editorId:'").append("automationeditor").append("'");
		sb.append(",").append("editor:").append("WFPropActions.setActivityAutomationParticipants");
		sb.append("}"); // data end
		sb.append("}"); // treeview
		// 参数
		sb.append(",").append("{"); // treeview
		sb.append("id:").append("'tvi4_").append(unid).append("'");
		sb.append(",").append("label:").append("'环节参数'");
		sb.append(",").append("desc:").append("'设置环节的额外参数'");
		sb.append(",").append("data:{"); // data begin
		sb.append("editor:").append("WFPropActions.setActivityParameters");
		sb.append("}"); // data end
		sb.append("}"); // treeview

		sb.append("]"); // children end

		sb.append("}"); // treeview
		return;
	}
}

