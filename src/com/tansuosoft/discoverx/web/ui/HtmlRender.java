/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.ui;

import com.tansuosoft.discoverx.web.JSPContext;

/**
 * 输出客html/js等文本供客户端呈现使用的类需实现的接口。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public interface HtmlRender {
	/**
	 * 为jspContext指定的jsp页面输出HTML文本。
	 * 
	 * @param jspContext
	 * @return
	 */
	public String render(JSPContext jspContext);
}

