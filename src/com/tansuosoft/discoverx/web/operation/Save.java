/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceConstructor;
import com.tansuosoft.discoverx.bll.ResourceConstructorProvider;
import com.tansuosoft.discoverx.bll.ResourceInserter;
import com.tansuosoft.discoverx.bll.ResourceInserterProvider;
import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.bll.ResourceReceiverProvider;
import com.tansuosoft.discoverx.bll.ResourceUpdater;
import com.tansuosoft.discoverx.bll.ResourceUpdaterProvider;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.document.DocumentOpener;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Source;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 保存资源的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>要保存的资源的目录名，如果不提供则默认为文档资源。通过http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要保存的资源的unid，必须。通过http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td colspan="2">其它具体内容通过http请求提供参数值。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link Operation#returnResult(String, Resource, OperationResult)}的结果（默认为保存成功的资源重新打开的url)。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Save extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Save() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		String directory = request.getParameter("directory");
		if (directory == null || directory.length() == 0) directory = ResourceDescriptorConfig.DOCUMENT_DIRECTORY;
		String unid = request.getParameter("unid");

		boolean isDocument = (ResourceDescriptorConfig.DOCUMENT_DIRECTORY.equalsIgnoreCase(directory));
		boolean isNew = false;

		Resource resource = null;
		OperationResult result = null;
		try {
			if (unid == null || unid.length() == 0) throw new FunctionException("没有提供有效资源唯一ID。");

			if (isDocument) {
				resource = DocumentBuffer.getInstance().getDocument(unid);
				if (resource == null) resource = this.getDocument();
				if (resource != null && !unid.equalsIgnoreCase(resource.getUNID())) resource = null;
				if (resource == null) resource = new DocumentOpener().open(unid, Document.class, this.getSession());
				if (resource != null) isNew = ((Document) resource).checkState(DocumentState.New);
			} else {
				resource = ResourceContext.getInstance().getResource(unid, directory);
			}
			if (!isDocument && resource == null) {
				isNew = true;
				ResourceConstructor resourceConstructor = ResourceConstructorProvider.getResourceConstructor(directory);
				resource = resourceConstructor.construct(directory, this.getSession(), null, null, null);
			}
			if (resource == null) {
				String msg = "无法获取要保存的“" + directory + "”类型的资源，可能的原因有：长时间没有操作导致登录会话被自动注销；服务器端重启或缓存重置等！";
				FileLogger.debug(msg);
				this.setLastMessage(msg);
				return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
			}

			if (resource.getSource() == Source.System) throw new FunctionException("您不能修改系统资源！");
			if (resource.getSource() == Source.Builtin && OrganizationsContext.getInstance().isMultipleOrgs() && !SecurityHelper.checkUserRole(this.getUser(), Role.ROLE_ADMIN_SC)) throw new FunctionException("只有管理员才能修改内置资源！");

			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
			if (rd == null) throw new FunctionException("无法获取“" + directory + "”对应的资源描述！");

			if (isNew) resource.setUNID(unid);
			result = this.returnRedirectToResource(resource);

			ResourceReceiver receiver = ResourceReceiverProvider.getResourceReceiver(directory);
			if (receiver == null) throw new FunctionException("无法获取“" + rd.getName() + "”资源对应的web提交内容接收对象。");
			receiver.setHttpRequest(request);
			receiver.setResource(resource);
			receiver.receive();
			resource = receiver.getResource();

			ResourceInserter inserter = null;
			ResourceUpdater updater = null;

			if (isNew) {
				inserter = ResourceInserterProvider.getResourceInserter(resource);
				inserter.insert(resource, getSession());
			} else {
				updater = ResourceUpdaterProvider.getResourceUpdater(resource);
				updater.update(resource, getSession());
			}
		} catch (ResourceException ex) {
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		}

		return this.returnResult("保存成功！", resource, result);
	}
}

