/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import com.tansuosoft.discoverx.bll.ResourceOpener;
import com.tansuosoft.discoverx.bll.ResourceOpenerProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Resource;

/**
 * 执行导出资源的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>要导出的资源的目录名，如果不提供则默认为文档资源。通过预先设置或http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要导出的资源的unid，必须。通过预先设置或http请求提供参数值。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回或{@link UrlConfig#URLCFGNAME_XML_CONTENT}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Export extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Export() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		ResourceOpener opener = null;
		Resource resource = null;
		String directory = this.getParameterStringFromAll("directory", ResourceDescriptorConfig.DOCUMENT_DIRECTORY);
		String unid = this.getParameterStringFromAll("unid", null);
		if (unid == null) {
			FunctionException ex = new FunctionException("无法获取资源UNID！");
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_XML_MESSAGE);
		}
		Class<?> clazz = ResourceDescriptorConfig.getInstance().getResourceClass(directory);
		opener = ResourceOpenerProvider.getResourceOpener(clazz);
		try {
			resource = opener.open(unid, clazz, this.getSession());
		} catch (ResourceException e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_XML_MESSAGE);
		}

		if (resource == null) {
			FunctionException ex = new FunctionException(String.format("找不到或无法打开“%1$s://%2$s”指定的资源！", directory, unid));
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_XML_MESSAGE);
		}
		this.setLastResource(resource);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_XML_CONTENT);
	}
}

