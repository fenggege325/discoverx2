/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.event.DocumentStateEventArgs;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.function.OperationParser;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.impl.DocumentFieldUpdater;
import com.tansuosoft.discoverx.dao.impl.SecurityEntryInserter;
import com.tansuosoft.discoverx.dao.impl.SecurityEntryUpdater;
import com.tansuosoft.discoverx.dao.impl.SecurityUpdater;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.PublishOption;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.workflow.WorkflowForm;

/**
 * 发布文档的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>issuerange</td>
 * <td>
 * 发布范围，0表示发布给所有人、1表示发布给所有登录人员、2表示发布给发布者所在直接部门、3表示发布给发布者所在一级部门、4表示发布给选定的人员。<br/>
 * 通过http请求提供。</td>
 * </tr>
 * <tr>
 * <td>issuerangeresult</td>
 * <td>用户选择的发布目标人员参与者安全编码，多个用半角分号分隔（发布范围为4时才有意义），通过http请求提供。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>目标文档unid，通过http请求提供。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link Operation#returnResult(String, com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.OperationResult)}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Issue extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Issue() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = null;
		try {
			Session session = this.getSession();
			User u = this.getUser();
			int usc = u.getSecurityCode();
			String unid = this.getParameterStringFromAll("unid", null);
			doc = this.getDocument();
			if (doc == null || !doc.getUNID().equalsIgnoreCase(unid)) doc = this.openDocument(unid);
			if (doc == null) throw new Exception("无法获取目标文档！");
			Application app = doc.getApplication(doc.getApplicationCount());
			int issueRange = this.getParameterIntFromAll("issuerange", -1);

			DocumentStateEventArgs dsea = new DocumentStateEventArgs();
			dsea.setDocument(doc);
			dsea.setSession(session);
			dsea.setOldState(doc.getState());
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_DOCUMENTSETSTATEBEGIN, EventHandler.EHT_DOCUMENTSETSTATEAFTER), EventHandlerInfoProvider.getProvider(EventCategory.Document).provide(doc, null));

			if (app != null && app.getPublish() && issueRange < 0) {
				PublishOption po = app.getPublishOption();
				if (po == PublishOption.Custom) {
					OperationParser opeationParser = new OperationParser(this.getExpression(), session);
					Operation operation = opeationParser.getOperation();
					operation.setParameter(Operation.BUILTIN_PARAMNAME_UIPATH, "operations/document_issue.jsp");
					return this.returnRequestUrl(opeationParser);
				} else {
					issueRange = po.getIntValue();
				}
			} else if (app != null && !app.getPublish()) {
				int state = doc.getState();
				state |= DocumentState.Issued.getIntValue();

				dsea.setState(state);
				this.callEventHandler(EventHandler.EHT_DOCUMENTSETSTATEBEGIN, dsea);

				DBRequest dbr = new DocumentFieldUpdater();
				dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, doc);
				dbr.setParameter(DocumentFieldUpdater.FIELD_NAME_PARAM_NAME, "state");
				dbr.setParameter(DocumentFieldUpdater.FIELD_VALUE_PARAM_NAME, state);
				dbr.sendRequest();
				if (dbr.getResultLong() > 0) {
					doc.setState(state);
					this.callEventHandler(EventHandler.EHT_DOCUMENTSETSTATEAFTER, dsea);
					return this.returnResult("发布成功！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_INFO_MESSAGE));
				} else {
					throw new Exception("无法设置文档状态！");
				}
			}

			// 获取发布范围对应目标用户/角色安全编码。
			ParticipantTree ptu = null;
			Participant target = null;
			List<Participant> persons = null;
			List<Integer> targetSecurityCodes = null;
			switch (issueRange) {
			case 0: // 所有人
				targetSecurityCodes = new ArrayList<Integer>(1);
				targetSecurityCodes.add(Role.ROLE_ANONYMOUS_SC);
				break;
			case 1: // 所有注册登录用户
				targetSecurityCodes = new ArrayList<Integer>(1);
				targetSecurityCodes.add(Role.ROLE_DEFAULT_SC);
				break;
			case 2: // 发布者所在直接部门
				ptu = ParticipantTreeProvider.getInstance().getParticipantTree(usc);
				if (ptu != null) {
					ParticipantTree parent = ptu.getParent();
					if (parent != null) target = parent;
				}
				if (target != null) persons = ParticipantTreeProvider.getInstance().getParticipants(target, session);
				if (persons != null && persons.size() > 0) {
					targetSecurityCodes = new ArrayList<Integer>(1);
					for (Participant p : persons) {
						if (p == null) continue;
						targetSecurityCodes.add(p.getSecurityCode());
					}
				}
				break;
			case 3: // 发布者所在一级部门
				ptu = ParticipantTreeProvider.getInstance().getParticipantTree(usc);
				ParticipantTree parent = ptu.getParent();
				ParticipantTree last = null;
				while (parent != null) {
					if (parent.getSecurityCode() == ParticipantTree.ROOT_CODE && last != null) {
						target = last;
						break;
					}
					last = parent;
					parent = parent.getParent();
				} // while end
				if (target != null) persons = ParticipantTreeProvider.getInstance().getParticipants(target, session);
				if (persons != null && persons.size() > 0) {
					targetSecurityCodes = new ArrayList<Integer>(1);
					for (Participant p : persons) {
						if (p == null) continue;
						targetSecurityCodes.add(p.getSecurityCode());
					}
				}
				break;
			case 4: // 指定人员
				targetSecurityCodes = WorkflowForm.getSecurityCodes(getHttpRequest(), "issuerangeresult");
				break;
			}

			// 插入安全编码。
			Security security = doc.getSecurity();
			Security securityNew = (Security) security.clone();
			DBRequest dbr = null;
			DBRequest first = null;
			int setResult = 0;
			SecurityEntry securityEntry = null;
			if (targetSecurityCodes != null && targetSecurityCodes.size() > 0) {
				for (int sc : targetSecurityCodes) {
					if (sc <= 0) continue;
					setResult = securityNew.setSecurityLevel(sc, SecurityLevel.View.getIntValue());
					if (sc == Role.ROLE_ANONYMOUS_SC || sc == Role.ROLE_DEFAULT_SC) continue;
					securityEntry = securityNew.getSecurityEntry(sc);
					if (securityEntry == null) continue;
					dbr = (setResult < 0 ? new SecurityEntryInserter() : new SecurityEntryUpdater());
					dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, securityEntry);
					if (first == null) {
						first = dbr;
					} else {
						first.setNextRequest(dbr);
					}
				}// for end
			}// if end

			int state = doc.getState();
			state |= DocumentState.Issued.getIntValue();
			if (securityNew.getPublished()) state |= DocumentState.Published.getIntValue();

			dsea.setState(state);
			this.callEventHandler(EventHandler.EHT_DOCUMENTSETSTATEBEGIN, dsea);

			DBRequest dbrlast = new SecurityUpdater();
			dbrlast.setParameter(DBRequest.ENTITY_PARAM_NAME, securityNew);
			dbrlast.setParameter("state", state);
			if (first == null) {
				first = dbrlast;
			} else {
				first.setNextRequest(dbrlast);
				first.setUseTransaction(true);
			}
			first.sendRequest();
			if (first.getResultLong() > 0) {
				doc.setState(state);
				doc.setSecurity(securityNew);
				this.callEventHandler(EventHandler.EHT_DOCUMENTSETSTATEAFTER, dsea);
			}
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		}
		return this.returnResult("发布成功！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_INFO_MESSAGE));
	}
}

