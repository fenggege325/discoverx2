/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.web.app.message.Message;
import com.tansuosoft.discoverx.web.app.message.MessageSender;
import com.tansuosoft.discoverx.workflow.WorkflowForm;

/**
 * 催办反馈相关操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>notificationtarget</td>
 * <td>接收用户安全编码，多个用半角逗号分隔。</td>
 * </tr>
 * <tr>
 * <td>notificationbody</td>
 * <td>内容信息。</td>
 * </tr>
 * <tr>
 * <td>notification</td>
 * <td>通知选项，0为邮件通知（默认）、1为短信通知。</td>
 * </tr>
 * <tr>
 * <td>verb</td>
 * <td>动作名称，默认为“催办”。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link Operation#returnResult(String, com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.OperationResult)}执行的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Notification extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Notification() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			HttpServletRequest request = this.getHttpRequest();
			Session session = this.getSession();
			String unid = this.getParameterStringFromAll("unid", null);
			Document doc = this.getDocument();
			if (doc == null || !doc.getUNID().equalsIgnoreCase(unid)) doc = this.openDocument(unid);
			if (doc == null) throw new Exception("无法获取目标文档！");
			User u = this.getUser();
			String verb = this.getParameterStringFromAll("verb", "催办");

			Message m = new Message();
			String subject = String.format("来自%1$s的%2$s:%3$s", u.getCN(), verb, doc.getName());
			m.setSubject(subject);
			String body = this.getParameterStringFromAll("notificationbody", "");
			body = body.concat(String.format("<hr/><p>单击<a href=\"../dispatch?fx=open&unid=%1$s\">%2$s</a>打开目标文档。</p>", doc.getUNID(), doc.getName()));
			m.setBody(body);
			List<Integer> targetSecurityCodes = WorkflowForm.getSecurityCodes(request, "notificationtarget");
			m.setReceivers(targetSecurityCodes);
			List<Integer> notification = WorkflowForm.getSecurityCodes(request, "notification");
			if (notification != null && notification.size() > 0) {
				int mt = m.getMessageType();
				for (int n : notification) {
					mt |= n;
				}
				m.setMessageType(mt);
			}

			MessageSender.sendMessage(m, session);
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		}
		return this.returnResult("发送成功！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_INFO_MESSAGE));
	}
}

