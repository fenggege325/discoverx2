/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.event.DocumentStateEventArgs;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.impl.SecurityUpdater;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.Session;

/**
 * 设置文档状态值的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>state</td>
 * <td>要设置的状态值（{@link DocumentState} 中除了0以外的某一个数字值（如果为同值负数，则表示去掉此状态值），通过预设值或http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>目标文档unid，必须。通过http请求提供参数值（可以允许多值）。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回 {@link Operation#returnResult(String, com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.OperationResult)} 对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SetDocumentState extends Operation {
	/**
	 * 缺省构造器。
	 */
	public SetDocumentState() {
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		DocumentStateEventArgs dsea = null;
		Session s = null;

		try {
			s = this.getSession();
			String directory = this.getParameterStringFromAll("directory", ResourceDescriptorConfig.DOCUMENT_DIRECTORY);
			boolean isDocument = directory.equalsIgnoreCase(ResourceDescriptorConfig.DOCUMENT_DIRECTORY);
			if (!isDocument) throw new FunctionException("只能对文档资源执行此操作！");
			HttpServletRequest request = this.getHttpRequest();
			String[] unids = request.getParameterValues("unid");
			if (unids == null || unids.length == 0) throw new FunctionException("没有指定目标文档unid！");
			int stateValue = this.getParameterIntFromAll("state", Integer.MAX_VALUE);
			if (stateValue == Integer.MAX_VALUE) throw new FunctionException("没有指定有效状态！");
			DocumentState state = DocumentState.parse(stateValue >= 0 ? stateValue : -stateValue);
			if (state == null || state == DocumentState.New) throw new FunctionException("没有指定有效状态！");

			DBRequest dbr = null;

			DocumentBuffer documentBuffer = DocumentBuffer.getInstance();
			Document doc = null;

			int newState = 0;
			int oldState = 0;
			for (String x : unids) {
				if (x == null || x.length() == 0) continue;
				doc = documentBuffer.getDocument(x);
				if (doc == null) doc = (Document) ResourceContext.getInstance().getResource(x, directory);
				if (doc == null) throw new FunctionException("无法打开“" + x + "”对应的文档！");
				oldState = doc.getState();
				newState = oldState | stateValue;
				dsea = new DocumentStateEventArgs();
				dsea.setDocument(doc);
				dsea.setSession(s);
				dsea.setState(newState);
				dsea.setOldState(oldState);
				EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_DOCUMENTSETSTATEBEGIN, EventHandler.EHT_DOCUMENTSETSTATEAFTER), EventHandlerInfoProvider.getProvider(EventCategory.Document).provide(doc, null));
				this.callEventHandler(EventHandler.EHT_DOCUMENTSETSTATEBEGIN, dsea);

				dbr = new SecurityUpdater();
				Security sec = doc.getSecurity();
				if (sec == null) sec = new Security(doc.getUNID());
				dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, sec);
				dbr.setParameter("state", newState);
				dbr.sendRequest();
				if (dbr.getResultLong() > 0) {
					doc.setState(newState);
					this.callEventHandler(EventHandler.EHT_DOCUMENTSETSTATEAFTER, dsea);
				}
			}// for end
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult("文档状态设置失败！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		}
		return this.returnResult("文档状态设置成功！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_INFO_MESSAGE));
	}
}

