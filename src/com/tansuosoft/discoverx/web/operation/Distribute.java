/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.impl.SecurityEntryInserter;
import com.tansuosoft.discoverx.dao.impl.SecurityEntryUpdater;
import com.tansuosoft.discoverx.dao.impl.SecurityUpdater;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.web.app.message.Message;
import com.tansuosoft.discoverx.web.app.message.MessageSender;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.transaction.DistributeTransaction;

/**
 * 分发文档的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>distribution</td>
 * <td>分发方式：0表示共享方式，1表示传阅方式。</td>
 * </tr>
 * <tr>
 * <td>notification</td>
 * <td>通知方式：0表示邮件通知，1表示短信通知。</td>
 * </tr>
 * <tr>
 * <td>distributiontarget</td>
 * <td>用户选择的分发目标人员参与者安全编码，通过http请求提供。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>目标文档unid，通过http请求提供。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link Operation#returnResult(String, com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.OperationResult)}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Distribute extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Distribute() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = null;
		WorkflowRuntime wfr = null;
		try {
			String unid = this.getParameterStringFromAll("unid", null);
			Session session = this.getSession();
			doc = this.getDocument();
			if (doc == null || !doc.getUNID().equalsIgnoreCase(unid)) doc = this.openDocument(unid);
			if (doc == null) throw new Exception("无法获取目标文档！");
			int distribution = this.getParameterIntFromAll("distribution", 0);

			List<Integer> targetSecurityCodes = WorkflowForm.getSecurityCodes(getHttpRequest(), "distributiontarget");
			switch (distribution) {
			case 0: // 共享方式
				List<Participant> persons = null;
				Security security = doc.getSecurity();
				Security securityNew = (Security) security.clone();
				DBRequest dbr = null;
				DBRequest first = null;
				int setResult = 0;
				SecurityEntry securityEntry = null;
				ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
				ParticipantTree pt = null;
				if (targetSecurityCodes != null && targetSecurityCodes.size() > 0) {
					for (int sc : targetSecurityCodes) {
						if (sc <= 0) continue;
						pt = ptp.getParticipantTree(sc);
						if (pt == null) continue;

						setResult = securityNew.setSecurityLevel(sc, SecurityLevel.View.getIntValue());
						securityEntry = securityNew.getSecurityEntry(sc);
						if (securityEntry == null) continue;
						dbr = (setResult < 0 ? new SecurityEntryInserter() : new SecurityEntryUpdater());
						dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, securityEntry);
						if (first == null) {
							first = dbr;
						} else {
							first.setNextRequest(dbr);
						}
						if (pt.getParticipantType() != ParticipantType.Person) {
							persons = ptp.getParticipants(pt, session);
							if (persons == null || persons.isEmpty()) continue;
							for (Participant x : persons) {
								setResult = securityNew.setSecurityLevel(x.getSecurityCode(), SecurityLevel.View.getIntValue());
								securityEntry = securityNew.getSecurityEntry(x.getSecurityCode());
								if (securityEntry == null) continue;
								dbr = (setResult < 0 ? new SecurityEntryInserter() : new SecurityEntryUpdater());
								dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, securityEntry);
								if (first == null) {
									first = dbr;
								} else {
									first.setNextRequest(dbr);
								}
							}// for end
						}// if end
					}// for end
				}// if end
				if (first != null) {
					DBRequest dbrlast = new SecurityUpdater();
					dbrlast.setParameter(DBRequest.ENTITY_PARAM_NAME, securityNew);
					first.setNextRequest(dbrlast);
					first.setUseTransaction(true);
					first.sendRequest();
					if (first.getResultLong() > 0) {
						doc.setSecurity(securityNew);
					}
				}
				break;
			case 1: // 传阅方式
				wfr = WorkflowRuntime.getInstance(doc, session);
				WorkflowForm wff = new WorkflowForm();
				wff.setParticipants(targetSecurityCodes);
				wfr.setWorkflowForm(wff);
				Transaction t = new DistributeTransaction();
				wfr.transact(t, session);
				wfr.shutdown();
				break;
			}

			HttpServletRequest request = this.getHttpRequest();
			Message m = new Message();
			String subject = String.format("文件传阅通知：%1$s", doc.getName());
			m.setSubject(subject);
			String body = String.format("单击<a href=\"../dispatch?fx=open&unid=%1$s\">%2$s</a>打开目标文档。", doc.getUNID(), doc.getName());
			m.setBody(body);
			m.setReceivers(targetSecurityCodes);
			List<Integer> notification = WorkflowForm.getSecurityCodes(request, "notification");
			if (notification != null && notification.size() > 0) {
				int mt = m.getMessageType();
				for (int n : notification) {
					mt |= n;
				}
				m.setMessageType(mt);
			}

			MessageSender.sendMessage(m, session);
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		return this.returnResult("分发成功！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE));
	}
}

