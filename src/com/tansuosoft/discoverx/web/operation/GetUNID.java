/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.UNIDProvider;

/**
 * 获取一个UNID的操作
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>count</td>
 * <td>一次调用获取的个数，默认为1，返回时会通过status属性返回。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>通过{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}返回UNID，如果指定返回多个则没有分隔符把UNID直接串联起来（可按UNID为32位长度获取）。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tensosoft.com
 */
@FunctionAttributes(type = 1, alias = "getUNID", name = "获取UNID", desc = "获取UNID的操作。")
public class GetUNID extends Operation {

	/**
	 * 重载：返回UNID文本
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			int count = this.getParameterIntFromAll("count", 1);
			if (count == 0) count = 1;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < count; i++) {
				sb.append(UNIDProvider.getUNID());
			}
			AJAXResponse r = new AJAXResponse();
			r.setStatus(count);
			r.setMessage(sb.toString());
			this.setLastAJAXResponse(r);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}
	}
}

