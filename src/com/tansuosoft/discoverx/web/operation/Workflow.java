/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.document.DocumentOpener;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.function.OperationParser;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction;

/**
 * 执行流程事务的文档操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td colspan="2">参见{@link WorkflowForm}中的参数说明。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link Operation#returnResult(String, com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.OperationResult)}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Workflow extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Workflow() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		WorkflowForm workflowForm = new WorkflowForm();
		WorkflowRuntime wfr = null;
		Session session = this.getSession();
		User user = Session.getUser(session);
		Document document = null;
		try {
			HttpServletRequest request = getHttpRequest();
			workflowForm.fillWebRequestForm(request);
			if (StringUtil.isBlank(workflowForm.getVerb())) {
				workflowForm.setVerb(this.getParameterStringFromAll("wf_verb", null));
			}
			if (workflowForm.getInteractionTransaction() == null) {
				workflowForm.setInteractionTransaction(this.getParameterValueAsObjectFromAll("wf_interaction", InteractionTransaction.class, null));
			}
			Transaction transaction = workflowForm.getTransaction();
			if (transaction == null) transaction = this.getParameterValueAsObjectFromAll("wf_transaction", Transaction.class, null);
			if (transaction == null) throw new FunctionException("无法获取本次请求对应的流程事务的处理对象，请联系管理员！");

			String unid = request.getParameter("unid");
			document = this.getDocument();
			if (document == null) document = (Document) (new DocumentOpener()).open(unid, Document.class, session);
			wfr = WorkflowRuntime.getInstance(document, session);
			wfr.setWorkflowForm(workflowForm);

			transaction = wfr.transact(transaction, session);
			if (transaction instanceof InteractionTransaction) {
				OperationParser opeationParser = new OperationParser(this.getExpression(), session);
				Operation operation = opeationParser.getOperation();
				operation.setParameter(Operation.BUILTIN_PARAMNAME_UIPATH, "operations/document_workflow.jsp");
				operation.setParameter("wf_interaction", transaction);

				return this.returnRequestUrl(opeationParser);
			}
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		if (document != null) DocumentBuffer.getInstance().removeHolder(document.getUNID(), user.getName());
		return this.returnResult("操作成功！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE));
	}
}

