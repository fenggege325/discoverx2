/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.web.accessory.AccessoryContainerHelper;
import com.tansuosoft.discoverx.web.accessory.AccessoryForm;
import com.tansuosoft.discoverx.web.accessory.AccessoryListViewRender;
import com.tansuosoft.discoverx.web.accessory.TextHelper;

/**
 * 执行附加文件（{@link com.tansuosoft.discoverx.model.Accessory}）相关处理的的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td colspan="2">请参考{@link AccessoryForm}中的各属性说明来对照解释支持的参数。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>除了下载/查看附件内容返回实际文件内容外，其余类型请求均返回{@link UrlConfig#URLCFGNAME_JSON_MESSAGE}对应的url结果。</td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * 如果父资源保存于数据库中，那么将触发附加文件资源的保存前/删除前和保存后/删除后事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父资源。 <br/>
 * 如果父资源保存于xml中，那么将触发附加文件所属父资源的保存前和保存后事件。<br/>
 * 将触发附加文件资源的打开前事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父资源。 <br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Accessory extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Accessory() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		AJAXResponse response = new AJAXResponse();
		HttpServletRequest request = null;
		HttpSession httpSession = null;
		try {
			/** 具体功能开始 * */
			Session session = this.getSession();
			request = this.getHttpRequest();
			if (request == null) throw new Exception("无法获取http请求！");
			httpSession = (request == null ? null : request.getSession());
			if (httpSession == null) throw new Exception("无法获取http会话！");

			AccessoryForm form = new AccessoryForm();
			form.fillWebRequestForm(request);
			int action = form.getAction();

			String fileName = form.getFileName();
			if (action == 1 && (fileName == null || fileName.length() == 0)) throw new FunctionException("文件名为空！" + fileName);

			String pdir = form.getParentDirectory();
			if (pdir == null || pdir.length() == 0) pdir = ResourceDescriptorConfig.DOCUMENT_DIRECTORY;
			String punid = form.getParentUNID();

			String unid = form.getUNID();
			if (TextHelper.SYSTEM_DEFAULT_TEMPLATE_ACCESSORY_PARENT_UNID.equalsIgnoreCase(punid)) {
				if (action == 8) { // 获取系统默认模板md5信息。
					response.setMessage(TextHelper.getSystemDefaultTemplateMD5(unid));
					this.setLastAJAXResponse(response);
					return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
				} else {
					String path = TextHelper.getSystemDefaultTemplatePath(unid);
					this.setLastMessage(path);
					return new OperationResult(request.getContextPath() + "/accessory.jsp");
				}
			}

			Resource parent = null;
			if (pdir.equalsIgnoreCase(ResourceDescriptorConfig.DOCUMENT_DIRECTORY)) parent = DocumentBuffer.getInstance().getDocument(punid);
			if (parent == null) parent = ResourceContext.getInstance().getResource(punid, pdir);
			if (parent == null) throw new Exception("无法获取附加文件资源的容器资源！");

			AccessoryContainerHelper containerHelper = new AccessoryContainerHelper(parent, session);
			com.tansuosoft.discoverx.model.Accessory accessory = null;
			if (action == 1 || action == 10 || action == 11 || action == 12 || action == 13) {
				accessory = new com.tansuosoft.discoverx.model.Accessory();
			} else {
				if (unid == null || unid.length() == 0) throw new FunctionException("无法获取附加文件UNID！");
				accessory = containerHelper.getAccessory(unid);
			}
			if (accessory == null) throw new RuntimeException("找不到资源“" + parent.getName() + "”中“" + form.getUNID() + "”对应的附加文件！");

			File accessoryFile = null;
			SecurityLevel currentLevel = null;
			boolean authorized = false;
			String newValue = null;
			String columnName = null;
			boolean isCRUD = true;
			switch (action) {
			case 1: // 新增
				currentLevel = SecurityLevel.Modify;
				authorized = SecurityHelper.authorize(session, accessory, parent, 3, currentLevel);
				if (!authorized) throw new Exception("对不起，您没有添加文件的权限！");
				receiveFromAccessoryForm(accessory, form);
				accessoryFile = new File(AccessoryPathHelper.getAbsoluteAccessoryServerPath(parent) + accessory.getFileName());
				if (!accessoryFile.exists()) throw new Exception("无法获取附加文件资源的文件大小，请检查文件是否存在或上传成功！");
				accessory.setSize((int) calcKBFromB(accessoryFile.length()));
				containerHelper.addAccessory(accessory);
				response.setMessage(accessory.getUNID());
				break;
			case 3: // 更新文件内容
				currentLevel = SecurityLevel.Modify;
				authorized = SecurityHelper.authorize(session, accessory, parent, 3, currentLevel);
				if (!authorized) throw new Exception("对不起，您没有更改此文件内容的权限！");
				accessoryFile = new File(AccessoryPathHelper.getAbsoluteAccessoryServerPath(parent) + accessory.getFileName());
				if (!accessoryFile.exists()) throw new Exception("无法获取附加文件资源对应的实际文件！");
				accessory.setSize((int) calcKBFromB(accessoryFile.length()));
				containerHelper.updateAccessorySize(accessory);
				response.setMessage(accessory.getUNID());
				break;
			case 4: // 删除
				currentLevel = SecurityLevel.Delete;
				authorized = SecurityHelper.authorize(session, accessory, parent, 3, currentLevel);
				if (!authorized) throw new Exception("对不起，您没有删除此文件的权限！");
				containerHelper.removeAccessory(accessory);
				response.setMessage(accessory.getUNID());
				break;
			case 5: // 修改名称
				newValue = form.getName();
				columnName = "c_name";
				isCRUD = false;
				break;
			case 6: // 修改类型
				newValue = form.getAccessoryType();
				columnName = "c_accessoryType";
				isCRUD = false;
				break;
			case 7: // 修改说明
				newValue = form.getDescription();
				columnName = "c_description";
				isCRUD = false;
				break;
			case 10: // 获取listview
				currentLevel = SecurityLevel.View;
				authorized = SecurityHelper.authorize(session, accessory, parent, 3, currentLevel);
				if (!authorized) throw new Exception("对不起，您没有查看附加文件列表的权限！");
				boolean readonly = this.getParameterBoolFromAll("readonly", false);
				int w = this.getParameterIntFromAll("width", 0);
				int h = this.getParameterIntFromAll("height", 0);
				AccessoryListViewRender acclvr = new AccessoryListViewRender(parent, w, h, readonly, null, null);
				request.setAttribute(acclvr.getClass().getName(), acclvr);
				OperationResult opr = new OperationResult("/accessory_ajax.jsp");
				opr.setIsForward(true);
				return opr;
			case 11: // 检查是否已经有指定文件名的附加文件
				String fn = form.getFileName();
				boolean duplicateFlag = false;
				String duplicateMsg = "";
				AccessoryType duplicateAt = null;
				List<com.tansuosoft.discoverx.model.Accessory> accs = parent.getAccessories();
				if (fn != null && accs != null) {
					for (com.tansuosoft.discoverx.model.Accessory ax : accs) {
						duplicateFlag = (ax != null && ax.getFileName() != null && ax.getFileName().endsWith(fn));
						if (duplicateFlag) {
							duplicateAt = ax.getAccessoryTypeResource();
							duplicateMsg = String.format("已经存在名为“%1$s”的文件", ax.getFileName());
							break;
						}
					}
				}
				response.setMessage(duplicateMsg);
				if (duplicateFlag && duplicateAt != null) response.setTag(String.format("%1$s:%2$s:%3$s", duplicateAt.getAlias(), duplicateAt.getUNID(), duplicateAt.getName()));
				break;
			case 12: // 检查是否已经存在指定类型的附加文件
				String act = form.getAccessoryType();
				boolean duplicateActFlag = false;
				String duplicateActMsg = "";
				List<com.tansuosoft.discoverx.model.Accessory> accs1 = parent.getAccessories();
				if (act != null && accs1 != null) {
					for (com.tansuosoft.discoverx.model.Accessory ax : accs1) {
						duplicateActFlag = (ax != null && (act.equalsIgnoreCase(ax.getAccessoryType()) || (ax.getAccessoryTypeResource() != null && act.equalsIgnoreCase(ax.getAccessoryTypeResource().getAlias()))));
						if (duplicateActFlag) {
							duplicateActMsg = String.format("已经存在类型为“%1$s”的文件", ax.getAccessoryTypeName());
							break;
						}
					}
				}
				response.setMessage(duplicateActMsg);
				break;
			case 13: // 判断上传文件的内容长度是否允许
				int size = form.getSize();
				if (size <= 0) throw new RuntimeException("文件内容长度未知！");
				String uploadAct = form.getAccessoryType();
				String uploadActUnid = ResourceAliasContext.getInstance().getUNIDByAlias(AccessoryType.class, uploadAct);
				AccessoryType actx = ResourceContext.getInstance().getResource((uploadActUnid == null || uploadActUnid.length() == 0 ? uploadAct : uploadActUnid), AccessoryType.class);
				int maxSize = (actx == null ? -1 : actx.getMaxSize());
				if (maxSize < 0) maxSize = CommonConfig.getInstance().getMaxUploadSize();
				if (maxSize > 0 && size > maxSize) throw new RuntimeException("提交的文件的内容长度超过系统允许上传内容的最大长度：“" + (maxSize / 1024) + "KB”！");
				break;
			case 2: // 下载/查看文件内容
			default:
				currentLevel = SecurityLevel.View;
				authorized = SecurityHelper.authorize(session, accessory, parent, 3, currentLevel);
				if (!authorized) throw new Exception("对不起，您没有查看此文件的权限！");
				String fullPath = AccessoryPathHelper.getAbsoluteAccessoryServerPath(parent) + accessory.getFileName();
				if (fullPath == null || fullPath.length() == 0) throw new Exception("无法获取文件路径！");
				this.setLastMessage(fullPath);
				// 添加事件处理程序。
				EventSourceImpl esi = new EventSourceImpl();
				List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(accessory, null);
				EventRegister.registerEventHandler(esi, String.format("%1$s", EventHandler.EHT_OPEN), eventHandlerInfoList);
				ResourceEventArgs e = new ResourceEventArgs(accessory, session);
				esi.callEventHandler(parent, EventHandler.EHT_OPEN, e); // 打开事件。
				OperationResult result = new OperationResult();
				result.setResultUrl(request.getContextPath() + "/accessory.jsp");
				return result;
			}
			// 如果是修改附加文件属性
			if (!isCRUD && containerHelper.updateAccessory(accessory, columnName, newValue)) {
				if (action == 5) {
					accessory.setName(newValue);
				} else if (action == 6) {
					accessory.setAccessoryType(newValue);
				} else {
					accessory.setDescription(newValue);
				}
				response.setMessage(accessory.getUNID());
			} // if end
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
		}

		this.setLastAJAXResponse(response);
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE);
	}

	/**
	 * 从字节计算千字节。
	 * 
	 * @param b
	 * @return
	 */
	protected static long calcKBFromB(long b) {
		long kb = b / 1024L;
		long kbd = b % 1024L;
		if (kb == 0 && b > 0) {
			kb = 1;
		} else if (kbd >= 512) {
			kb++;
		}
		return kb;
	}

	/**
	 * 根据{@link AccessoryForm}的内容更新{@link com.tansuosoft.discoverx.model.Accessory}中对应内容。
	 * 
	 * @param accessory
	 * @param form
	 */
	protected static void receiveFromAccessoryForm(com.tansuosoft.discoverx.model.Accessory accessory, AccessoryForm form) {
		String tmp = form.getName();
		if (tmp != null) accessory.setName(tmp);

		tmp = form.getTemplate();
		if (tmp != null) accessory.setStyleTemplate(tmp);

		tmp = form.getCodeTemplate();
		if (tmp != null) accessory.setCodeTemplate(tmp);

		tmp = form.getParentUNID();
		if (tmp != null) accessory.setPUNID(tmp);

		tmp = form.getParentDirectory();
		if (tmp != null) accessory.setParentDirectory(tmp);

		tmp = form.getFileName();
		if (tmp != null) accessory.setFileName(tmp);

		tmp = form.getUNID();
		if (tmp != null && tmp.length() == 32) accessory.setUNID(tmp);

		tmp = new DateTime().toString();
		if (tmp != null) accessory.setModified(tmp);

		tmp = form.getAlias();
		if (tmp != null) accessory.setAlias(tmp);

		tmp = form.getAccessoryType();
		if (tmp != null) accessory.setAccessoryType(tmp);

		tmp = form.getDescription();
		if (tmp != null) accessory.setDescription(tmp);
	}
}

