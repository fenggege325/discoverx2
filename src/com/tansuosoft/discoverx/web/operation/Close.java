/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;

/**
 * 关闭资源的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>要关闭的资源的目录名，如果不提供则默认为文档资源。通过http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要关闭的资源的unid，必须。通过http请求提供参数值。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回配置/通过参数指定的结果或{@link UrlConfig#URLCFGNAME_DIRECT_CLOSE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * 只有文档资源才会真正执行关闭操作！
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Close extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Close() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Session s = this.getSession();
		HttpServletRequest request = this.getHttpRequest();
		String unid = request.getParameter("unid");
		if (unid == null) { return new OperationResult(UrlConfig.getInstance().getValue(UrlConfig.URLCFGNAME_DIRECT_CLOSE)); }
		String directory = request.getParameter("directory");
		if (directory == null || directory.length() == 0) directory = ResourceDescriptorConfig.DOCUMENT_DIRECTORY;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
		boolean isCached = (rd == null ? false : rd.getCache());
		boolean isDocument = false;
		if (ResourceDescriptorConfig.DOCUMENT_DIRECTORY.equalsIgnoreCase(directory)) {
			isDocument = true;
			DocumentBuffer buffer = DocumentBuffer.getInstance();
			Document doc = buffer.getDocument(unid);
			ResourceEventArgs arg = null;
			if (doc != null) {
				// 注册事件
				List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Document).provide(doc, null);
				EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_CLOSE, EventHandler.EHT_CLOSED), eventHandlerInfoList);
				arg = new ResourceEventArgs(doc, s);
				this.callEventHandler(EventHandler.EHT_CLOSE, arg);
			}
			User user = this.getUser();
			buffer.removeHolder(unid, user == null ? null : user.getName());
			if (doc != null) this.callEventHandler(EventHandler.EHT_CLOSED, arg);
		} else if (isCached) {
			Resource r = ResourceContext.getInstance().getResource(unid, directory);
			if (r != null) {
				// 注册事件
				List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(r, null);
				EventRegister.registerEventHandler(this, String.format("%1$s", EventHandler.EHT_CLOSED), eventHandlerInfoList);
				ResourceEventArgs arg = new ResourceEventArgs(r, s);
				this.callEventHandler(EventHandler.EHT_CLOSED, arg);
			}
		}
		return this.returnResult(isDocument ? "成功关闭文档!" : "成功关闭资源!", null, new OperationResult(UrlConfig.getInstance().getValue(UrlConfig.URLCFGNAME_DIRECT_CLOSE)));
	}
}

