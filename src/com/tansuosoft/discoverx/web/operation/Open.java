/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceOpener;
import com.tansuosoft.discoverx.bll.ResourceOpenerProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Resource;

/**
 * 打开资源的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>要打开的资源的目录名，如果不提供则默认为文档资源。通过预设参数或http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要打开的资源的unid，必须。通过http请求提供参数值。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>成功则返回打开资源的{@link Operation#returnRedirectToResource(Resource)}对应的结果，错误则返回{@link UrlConfig#URLCFGNAME_ERROR_MESSAGE}对应的结果</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Open extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Open() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		String directory = this.getParameterStringFromAll("directory", ResourceDescriptorConfig.DOCUMENT_DIRECTORY);
		String unid = request.getParameter("unid");
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
		if (rd == null) {
			this.setLastError(new FunctionException("无法获取“" + directory + "”对应的资源描述！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}

		Class<?> clazz = null;
		Resource resource = null;
		ResourceOpener opener = null;
		try {
			clazz = Class.forName(rd.getEntity());
			opener = ResourceOpenerProvider.getResourceOpener(clazz);
			resource = opener.open(unid, clazz, this.getSession());
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		if (resource == null) {
			this.setLastError(new FunctionException("无法打开“" + directory + "://" + unid + "”对应的资源！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		return this.returnRedirectToResource(resource);
	}
}

