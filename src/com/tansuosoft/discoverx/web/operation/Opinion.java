/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.document.DocumentOpener;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.OpinionType;
import com.tansuosoft.discoverx.model.OpinionView;
import com.tansuosoft.discoverx.model.PersistenceState;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.Signature;
import com.tansuosoft.discoverx.model.SignatureConfig;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.opinion.OpinionHelper;

/**
 * 填写审批意见/答复信息的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>opinionType</td>
 * <td>绑定的意见类型unid或别名，如果不提供则使用系统默认意见类型({@link com.tansuosoft.discoverx.model.OpinionType#DEFAULT_OPINION_TYPE_UNID})。只能事先配置。</td>
 * </tr>
 * <tr>
 * <td>opinionView</td>
 * <td>意见查看权限，请参考{@link com.tansuosoft.discoverx.model.OpinionView}中的值进行配置。可以事先配置或通过http提交。</td>
 * </tr>
 * <tr>
 * <td>opinionViewSecurityCodes</td>
 * <td>如果“opinionView”的值为{@link com.tansuosoft.discoverx.model.OpinionView#Custom}，则可以通过此参数获取可查看意见参与者的安全编码。只能通过http提交，支持多值（不能在一个值中返回多个参与者安全编码列表）。</td>
 * </tr>
 * <tr>
 * <td>opinionSignature</td>
 * <td>签名信息，请参考{@link com.tansuosoft.discoverx.model.Signature}中的值进行配置。可以事先配置或通过http提交。</td>
 * </tr>
 * <tr>
 * <td>opinionBody</td>
 * <td>可以通过实现配置此参数值实现默认意见内容，可选。可以事先配置或通过http提交。</td>
 * </tr>
 * <tr>
 * <td>unid/parentunid</td>
 * <td>所属文档资源的unid，提交意见时必须提供有效值。只能通过http提交。</td>
 * </tr>
 * <tr>
 * <td>opinionUnid</td>
 * <td>要更新的意见资源unid，可选，如果不提供则表示新加意见。只能通过http提交。</td>
 * </tr>
 * <tr>
 * <td>opinionName</td>
 * <td>意见名称，可选，如果不提供则默认为“意见作者直接部门名称\意见作者姓名”。只能通过http提交。</td>
 * </tr>
 * <tr>
 * <td>opinionAlias</td>
 * <td>意见别名，可选，如果不提供则为空。只能通过http提交。</td>
 * </tr>
 * <tr>
 * <td>opinionDescription</td>
 * <td>意见描述，可选，如果不提供则默认为空。只能通过http提交。</td>
 * </tr>
 * <tr>
 * <td>opinionCategory</td>
 * <td>意见分类，可选，如果不提供则默认为所属意见类型资源的名称。只能通过http提交。</td>
 * </tr>
 * <tr>
 * <td>documentRankValue</td>
 * <td>附带的文档评分数值，可选，如果不提供则表示没有附带评分结果。只能通过http提交。</td>
 * </tr>
 * <tr>
 * <td>setmyopinionflag</td>
 * <td>表示是否把提交的意见内容设置为用户常用意见的标记，如果为1表示要设置（默认为不设置），只能通过http提交。</td>
 * </tr>
 * <tr>
 * <td>opinionPassed</td>
 * <td>表示是否通过，只能配置为1/0（true/false、yes/no)等，可选。只能事先配置，默认为true。</td>
 * </tr>
 * <tr>
 * <td colspan="2">所有操作支持的其它通用参数。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回配置的返回结果，如果没有配置，则返回打开文档的url。</td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * 将触发意见资源的保存前/删除前和保存后/删除后事件，其中{@link EventHandler#handle(Object, com.tansuosoft.discoverx.common.event.EventArgs)}的sender参数为所属父文档资源。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Opinion extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Opinion() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		OperationResult result = null;
		HttpServletRequest request = null;
		com.tansuosoft.discoverx.model.Opinion opinion = null;
		Document document = null;
		Session session = null;
		OpinionHelper helper = null;
		try {
			session = this.getSession();
			request = this.getHttpRequest();
			String body = request.getParameter("opinionBody");

			// 记录为用户常用意见。
			boolean setMyOpinionFlag = StringUtil.getValueBool(request.getParameter("setmyopinionflag"), false);
			if (setMyOpinionFlag) {
				if (!Profile.getInstance(session).setOpinion(body)) {
					FileLogger.debug("无法将输入的内容记录为用户“%1$s”的常用审批意见！", this.getUser().getName());
				}
			}

			String opinionType = this.getParameterStringFromAll("opinionType", OpinionType.DEFAULT_OPINION_TYPE_UNID);
			if (opinionType != null && opinionType.startsWith("opt")) opinionType = ResourceAliasContext.getInstance().getUNIDByAlias(OpinionType.class, opinionType);
			Signature signature = this.getParameterEnumFromAll("opinionSignature", Signature.class, Signature.Default);
			if ((SignatureConfig.getInstance().getSupportSignature() & signature.getIntValue()) != signature.getIntValue()) signature = Signature.Default;
			OpinionView opinionView = this.getParameterEnumFromAll("opinionView", OpinionView.class, OpinionView.ViewByDocumentViewer);

			String docUNID = request.getParameter("parentunid");
			if (docUNID == null || docUNID.length() == 0) docUNID = request.getParameter("unid");
			document = (Document) new DocumentOpener().open(docUNID, Document.class, session);
			if (document == null) throw new Exception("无法打开目标文档！");
			helper = new OpinionHelper(document, session);
			String opinionUnid = this.getParameterStringFromAll("opinionUnid", null);
			if (opinionUnid != null && opinionUnid.length() > 0) {
				opinion = helper.getOpinion(opinionUnid);
				if (opinion != null) opinion.setBody(body);
			}
			boolean isNew = false;
			if (opinion == null) {
				opinion = helper.newOpinion(body, opinionType, signature, opinionView);
				boolean isPassed = this.getParameterBoolean("opinionPassed", true);
				opinion.setPassed(isPassed);
				isNew = true;
			}
			// 设置能查看意见的参与者安全编码
			if (opinionView == OpinionView.Custom) {
				String[] strscs = request.getParameterValues("opinionViewSecurityCodes");
				if (strscs != null && strscs.length > 0) {
					int sc = 0;
					Security security = opinion.getSecurity();
					if (security == null) {
						security = new Security(opinion.getUNID());
						opinion.setSecurity(security);
					}
					for (String strsc : strscs) {
						sc = StringUtil.getValueInt(strsc, -1);
						if (sc <= 0) continue;
						security.setSecurityLevel(sc, SecurityLevel.View.getIntValue());
					}
				}
			}
			// 设置额外属性
			String pv = StringUtil.getValueString(request.getParameter("opinionName"), null);
			if (pv != null) opinion.setName(pv);
			pv = StringUtil.getValueString(request.getParameter("opinionDescription"), null);
			if (pv != null) opinion.setDescription(pv);
			pv = StringUtil.getValueString(request.getParameter("opinionAlias"), null);
			if (pv != null) opinion.setAlias(pv);
			pv = StringUtil.getValueString(request.getParameter("opinionCategory"), null);
			if (pv != null && pv.length() > 0) opinion.setCategory(pv);

			// 保存意见
			boolean saved = helper.persistenceOpinion(opinion, (isNew ? PersistenceState.New : PersistenceState.Update));
			if (!saved) throw new Exception("无法保存" + opinion.getOpinionTypeName() + "！");
			// 分数
			int rankValue = StringUtil.getValueInt(request.getParameter("documentRankValue"), Integer.MIN_VALUE);
			if (rankValue != Integer.MIN_VALUE) {
				final int rc = document.getRankCount() + 1;
				final int rv = document.getRankValue() + rankValue;
				final String documentUnid = document.getUNID();
				DBRequest dbr = new DBRequest() {
					@Override
					protected SQLWrapper buildSQL() {
						SQLWrapper r = new SQLWrapper();
						r.setSql("update t_document set c_rankCount=" + rc + ",c_rankValue=" + rv + " where c_unid='" + documentUnid + "'");
						return r;
					}
				};
				dbr.sendRequest();
				if (dbr.getResultLong() > 0) {
					document.setRankCount(rc);
					document.setRankValue(rv);
				}
			}
			result = this.returnResult(null, document, this.returnRedirectToResource(document));
			if (result == null) throw new Exception("无法获取到执行结果！");
		} catch (Exception ex) {
			this.setLastError(ex);
			OperationResult or = this.returnResultUrl();
			if (or != null) return or;
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		return result;
	}
}

