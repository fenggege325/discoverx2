/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.servlet.LoginServlet;

/**
 * 执行登录注销的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td colspan="2">无其它额外参数</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link UrlConfig#URLCFGNAME_LOGIN}对应的结果（默认）或者{@link Operation#BUILTIN_PARAMNAME_RESULTURL}参数值指向的url结果（如果提供了有效的参数值的话）。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Logout extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Logout() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Session session = this.getSession();
		if (session != null) {
			Document doc = session.getLastDocument();
			if (doc != null) DocumentBuffer.getInstance().removeHolder(doc.getUNID(), Session.getUser(session).getName());
		}
		HttpServletRequest request = this.getHttpRequest();
		if (request != null) {
			if (session != null && LoginServlet.checkTSIM(request)) session.setTsimLoginInfo(session.getTsimAddress(), 0, false);
			HttpSession httpSession = request.getSession();
			Session.getAllHttpSession().remove(httpSession.getId());
			httpSession.invalidate();
		}
		OperationResult result = this.returnResultUrl();
		if (result != null && !StringUtil.isBlank(result.getResultUrl())) {
			return result;
		} else {
			OperationResult r = this.returnConfigUrl(UrlConfig.URLCFGNAME_LOGIN);
			r.setResultUrl(r.getResultUrl() + "?f=logout");
			return r;
		}
	}
}

