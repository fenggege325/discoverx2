/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.ResourceConstructor;
import com.tansuosoft.discoverx.bll.ResourceConstructorProvider;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 新增资源的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>要新建的资源的目录名，如果不提供则默认为文档资源。可以预先设置或通过http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td>config</td>
 * <td>要新建的资源的配置信息，如果是新建文档资源，则此参数通常为文档所属直接应用程序的别名。具体格式请参考{@link Document#getConfigResource()}。可以预先设置或通过http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td>punid</td>
 * <td>要新建的资源所属上级资源的unid，可选。可以预先设置或通过http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td colspan="2">其它额外的参数可以一并通过http请求发送。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回打开资源的表单页面的url结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class New extends Operation {
	/**
	 * 缺省构造器。
	 */
	public New() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		String directory = this.getParameterStringFromAll("directory", ResourceDescriptorConfig.DOCUMENT_DIRECTORY);
		if (directory == null || directory.length() == 0) directory = ResourceDescriptorConfig.DOCUMENT_DIRECTORY;
		HttpServletRequest request = this.getHttpRequest();
		HttpSession httpSession = (request == null ? null : request.getSession());
		Session session = this.getSession();
		if (httpSession == null && session == null) {
			this.setLastError(new FunctionException("无法找到用户自定义会话和Http会话信息！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
		if (rd == null) {
			this.setLastError(new FunctionException("无法获取“" + directory + "”对应的资源描述！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}

		Resource resource = null;

		String config = this.getParameterStringFromAll("config", null);
		// 如果是文档资源，必须提供应用模块等信息，如果没有提供，那么重定向到错误页面
		if (directory.equalsIgnoreCase(ResourceDescriptorConfig.DOCUMENT_DIRECTORY) && (config == null || config.length() == 0)) {
			this.setLastError(new FunctionException("无法获取新建文档所需的应用程序信息！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}

		String punid = this.getParameterStringFromAll("punid", null);
		Map<String, Object> params = null;
		try {
			ResourceConstructor constructor = ResourceConstructorProvider.getResourceConstructor(directory);
			resource = constructor.construct(directory, session, config, punid, params);
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}
		if (resource == null) {
			this.setLastError(new FunctionException("无法构造“" + rd.getName() + "”资源！"));
			return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
		}

		if (resource instanceof Document) {
			Session.setSessionParam(session, httpSession, Session.LASTDOCUMENT_PARAM_NAME_IN_HTTPSESSION, (Document) resource);
		} else {
			Session.setSessionParam(session, httpSession, Session.LASTRESOURCE_PARAM_NAME_IN_HTTPSESSION, resource);
		}

		OperationResult result = this.returnRedirectToResource(resource);
		String url = result.getResultUrl();
		int width = StringUtil.getValueInt((request == null ? null : request.getParameter("width")), -1);
		int pos = url.indexOf('?');

		result.setResultUrl(url.substring(0, pos) + (width > 0 ? "?width=" + width : ""));
		return result;
	}
}

