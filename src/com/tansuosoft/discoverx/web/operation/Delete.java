/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.operation;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.ResourceRemover;
import com.tansuosoft.discoverx.bll.ResourceRemoverProvider;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.bll.view.DocumentViewParser;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Source;
import com.tansuosoft.discoverx.model.User;

/**
 * 删除资源的操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>directory</td>
 * <td>要删除的资源的目录名，如果不提供则默认为文档资源。通过http请求提供参数值。</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要关删除的资源的unid，必须。通过http请求提供参数值，允许多个。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回参数指定的结果或{@link UrlConfig#URLCFGNAME_SHOW_MESSAGE_AND_CLOSE}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Delete extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Delete() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		HttpServletRequest request = this.getHttpRequest();
		String directory = request.getParameter("directory");
		if (directory == null || directory.length() == 0) directory = ResourceDescriptorConfig.DOCUMENT_DIRECTORY;
		String[] unids = request.getParameterValues("unid");
		try {
			if (unids == null || unids.length == 0) throw new Exception("没有提供要删除资源的UNID。");
			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
			if (rd == null) throw new FunctionException("无法获取“" + directory + "”对应的资源描述！");
			boolean isDocument = ResourceDescriptorConfig.DOCUMENT_DIRECTORY.equalsIgnoreCase(rd.getDirectory());
			for (String unid : unids) {
				Resource resource = null;
				try {
					if (isDocument) {
						resource = DocumentBuffer.getInstance().getDocument(unid);
					} else {
						resource = ResourceContext.getInstance().getResource(unid, directory);
					}
				} catch (Exception ex) {
				}
				boolean found = (resource != null);
				if (found) {
					if (resource.getSource() != Source.UserDefine) throw new FunctionException("您不能删除系统或内置级别的资源！");
					ResourceRemover remover = ResourceRemoverProvider.getResourceRemover(resource);
					remover.remove(resource, getSession());
				} else if (isDocument) { // 找不到文档资源说明可能是没有缓存的文档，此时如果是超级管理员或文档管理员则可以删除之。
					User u = this.getUser();
					if (!(u.isSystemBuiltinAdmin() || SecurityHelper.checkUserRole(u, Role.ROLE_DOCUMENTADMIN_SC))) throw new Exception("您没有执行此操作的权限！。");

					String sqlMainTable = "delete from " + DocumentViewParser.T_DOCUMENT.getKey() + " where c_unid='" + unid + "'";
					DBRequest dbrMainTable = getDBRequest(sqlMainTable);

					String sqlAccessory = "delete from t_accessory where c_punid='" + unid + "'";
					DBRequest dbrAccessory = getDBRequest(sqlAccessory);
					dbrMainTable.setNextRequest(dbrAccessory);

					String sqlParameter = "delete from t_parameter where c_punid='" + unid + "'";
					DBRequest dbrParameter = getDBRequest(sqlParameter);
					dbrMainTable.setNextRequest(dbrParameter);

					String sqlLog = "delete from t_log where c_owner='" + unid + "'";
					DBRequest dbrLog = getDBRequest(sqlLog);
					dbrMainTable.setNextRequest(dbrLog);

					String sqlSecurity = "delete from t_security where c_punid='" + unid + "'";
					DBRequest dbrSecurity = getDBRequest(sqlSecurity);
					dbrMainTable.setNextRequest(dbrSecurity);

					String sqlItem = "delete from t_item where c_punid='" + unid + "'";
					DBRequest dbrItem = getDBRequest(sqlItem);
					dbrMainTable.setNextRequest(dbrItem);

					String sqlExtra = "delete from t_extra where c_punid='" + unid + "'";
					DBRequest dbrExtra = getDBRequest(sqlExtra);
					dbrMainTable.setNextRequest(dbrExtra);

					String sqlOpinion = "delete from t_opinion where c_punid='" + unid + "'";
					DBRequest dbrOpinion = getDBRequest(sqlOpinion);
					dbrMainTable.setNextRequest(dbrOpinion);

					String sqlWfdata = "delete from t_wfdata where c_punid='" + unid + "'";
					DBRequest dbrWfdata = getDBRequest(sqlWfdata);
					dbrMainTable.setNextRequest(dbrWfdata);

					String sqlAuthority = "delete from t_authority where c_unid='" + unid + "'";
					DBRequest dbrAuthority = getDBRequest(sqlAuthority);
					dbrMainTable.setNextRequest(dbrAuthority);

					dbrMainTable.sendRequest();
				} else {
					throw new Exception("无法获取要删除的资源！");
				}
			}
		} catch (Exception e) {
			this.setLastError(e);
			return this.returnResult("删除失败！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		}
		this.setLastMessage("删除成功！");
		return this.returnResult("删除成功！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE));
	}

	/**
	 * 获取sql对应的{@link DBRequest}对象。
	 * 
	 * @param sql
	 * @return
	 */
	private DBRequest getDBRequest(final String sql) {
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql(sql);
				result.setRequestType(RequestType.NonQuery);
				return result;
			}
		};
		return dbr;
	}
}

