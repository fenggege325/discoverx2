/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web;

import com.tansuosoft.discoverx.common.Agent;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 定时任务执行时的包装类。
 * 
 * @author coca@tensosoft.com
 */
final class AgentWrapper extends Agent {
	private Agent agent = null;
	private String n = null;

	/**
	 * 接收{@link Agent}对象的构造器。
	 * 
	 * @param agent
	 */
	public AgentWrapper(Agent agent) {
		this.agent = agent;
	}

	/**
	 * 重载：调用包装对象同名方法。
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getName()
	 */
	public String getName() {
		if (n != null && n.length() > 0) return n;
		n = agent.getName();
		if (n == null || n.length() == 0) n = agent.getClass().getName();
		return n;
	}

	/**
	 * 重载：调用包装对象同名方法。
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getDescription()
	 */
	public String getDescription() {
		return agent.getDescription();
	}

	/**
	 * 重载：调用包装对象同名方法。
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getInitialDelay()
	 */
	public long getInitialDelay() {
		return agent.getInitialDelay();
	}

	/**
	 * 重载：调用包装对象同名方法。
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getPeriod()
	 */
	public long getPeriod() {
		return agent.getPeriod();
	}

	/**
	 * 重载：调用包装对象同名方法。
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getAbsolute()
	 */
	public boolean getAbsolute() {
		return agent.getAbsolute();
	}

	/**
	 * 重载：调用包装对象同名方法。
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getVerbose()
	 */
	public boolean getVerbose() {
		return agent.getVerbose();
	}

	/**
	 * 重载：调用包装的{@link Agent}对象的同名方法。
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			String xn = this.getName();
			String xd = this.getDescription();
			CommonConfig cc = CommonConfig.getInstance();
			if (getVerbose()) {
				System.out.println(String.format("%1$s 开始执行来自%2$s的定时任务：%3$s(%4$s)...", DateTime.getNowDTString(), cc.getSystemNameAbbr(), xn, xd));
			}
			agent.run();
			if (getVerbose()) {
				System.out.println(String.format("%1$s 完成执行来自%2$s的定时任务：%3$s(%4$s)。", DateTime.getNowDTString(), cc.getSystemNameAbbr(), xn, xd));
			}
		} catch (Exception e) {
			FileLogger.error(e);
		}
	}
}

