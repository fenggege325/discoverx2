/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * Condition集合属性填充
 *
 * @author coca@tansuosoft.cn
 */
public class ConditionsReceiver {
	
	/**
	 * Condition集合属性填充
	 * 
	 * @return conditions
	 */
	public static List<ViewCondition> setConditions(String[] columnName, String[] combineNextWith,  
			String[] compare,String[] expression, String[] leftBracket, String[] numericCompare, 
			String[] rightBracket,String[] rightValue, String[] tableName){
		List<ViewCondition> conditions = new ArrayList<ViewCondition>();
		if(tableName == null) return null;
		for(int i = 0; i < tableName.length; i++){
			if(tableName[i].length()==0) continue;
			ViewCondition condition = new ViewCondition();
			condition.setColumnName(columnName[i]);
			condition.setCombineNextWith(StringUtil.isNullOrEmpty(combineNextWith[i]) ? " AND " : combineNextWith[i]);
			condition.setCompare(compare[i]);
			condition.setExpression(expression[i]);
			condition.setLeftBracket(StringUtil.isNullOrEmpty(leftBracket[i]) ? "" : leftBracket[i]);
			condition.setNumericCompare(StringUtil.isNullOrEmpty(numericCompare[i]) ? false : Boolean.parseBoolean(numericCompare[i]));
			condition.setRightBracket(StringUtil.isNullOrEmpty(rightBracket[i]) ? "" : rightBracket[i]);
			condition.setRightValue(rightValue[i]);
			condition.setTableName(tableName[i]);
			conditions.add(condition);
		}
		return conditions;
	}
}

