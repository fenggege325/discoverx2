/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Portlet;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Scroll;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.ui.portal.PortalParser;
import com.tansuosoft.discoverx.web.ui.portal.PortletComparator;

/**
 * {@link Portal}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class PortalReceiver extends ResourceReceiver {

	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		Portal portal = null;
		portal = (Portal) this.getResource();
		if (portal == null) throw new RuntimeException("非Portal资源！");
		portal.setFrame(this.getRequestParameterValueString("frame", null));
		portal.setTheme(this.getRequestParameterValueString("theme", null));
		portal.setMenu(this.getRequestParameterValueString("menu", null));
		portal.setColumnCount(this.getRequestParameterValueInt("columnCount", 3));
		portal.setColumnWidth(null);
		String columnWidth = this.getRequestParameterValueString("columnWidth", null);
		if (columnWidth != null && columnWidth.length() > 0) {
			String strs[] = columnWidth.split("[，；,;\\s]"); // StringUtil.splitString(columnWidth, ',');
			if (strs.length > 0) {
				List<Integer> list = new ArrayList<Integer>(strs.length);
				for (String s : strs) {
					if (s == null || s.length() == 0 || !StringUtil.isNumber(s)) continue;
					list.add(StringUtil.getValueInt(s, 0));
				}
				portal.setColumnWidth(list);
			}
		}
		portal.setPortalLevel1Js(this.getRequestParameterValueString("portalLevel1Js", null));
		portal.setPortalLevel2Js(this.getRequestParameterValueString("portalLevel2Js", null));
		portal.setPortalLevel1Css(this.getRequestParameterValueString("portalLevel1Css", null));
		portal.setPortalLevel2Css(this.getRequestParameterValueString("portalLevel2Css", null));

		// 门户绑定的小窗口对象填充
		portal.setPortlets(null);
		String[] portletsParams = { "portlets_name", "portlets_column", "portlets_row", "portlets_displayCount", "portlets_showTitle", "portlets_height", "portlets_width", "portlets_titleIcon", "portlets_portletScroll", "portlets_dataSource" };
		List<List<String>> vals = this.fillListString(portletsParams);
		List<Portlet> portlets = null;
		String name = null;
		int column = 0;
		int row = 0;
		int displayCount = 10;
		boolean showTitle = true;
		int height = 100;
		int width = 100;
		String titleIcon = null;
		Scroll portletScroll = Scroll.NoScroll;
		String ds = null;
		Reference ref = null;
		Portlet portlet = null;
		PortletDataSource pds = null;
		int idx = 0;
		if (vals != null && vals.size() > 0) {
			for (List<String> x : vals) {
				name = StringUtil.getValueString(x.get(0), null);
				if (name == null || name.trim().length() == 0) continue;
				column = StringUtil.getValueInt(x.get(1), 0);
				row = StringUtil.getValueInt(x.get(2), 0);
				displayCount = StringUtil.getValueInt(x.get(3), 10);
				showTitle = StringUtil.getValueBool(x.get(4), true);
				height = StringUtil.getValueInt(x.get(5), 100);
				width = StringUtil.getValueInt(x.get(6), 100);
				titleIcon = StringUtil.getValueString(x.get(7), null);
				portletScroll = Scroll.parse(StringUtil.getValueInt(x.get(8), Scroll.NoScroll.getIntValue()));
				ds = StringUtil.getValueString(x.get(9), null);
				ref = null;
				if (ds != null && ds.trim().length() > 0) {
					String pdsUNID = ds;
					if (ds.startsWith("pds")) {
						pdsUNID = ResourceAliasContext.getInstance().getUNIDByAlias(PortletDataSource.class, ds);
					}
					pds = ResourceContext.getInstance().getResource(pdsUNID, PortletDataSource.class);
					if (pds != null) {
						ref = new Reference();
						ref.setDirectory(ResourceDescriptorConfig.getInstance().getResourceDirectory(PortletDataSource.class));
						ref.setUnid(pds.getUNID());
						ref.setTitle(pds.getName());
					}
				}
				portlet = new Portlet();
				portlet.setName(name);
				portlet.setColumn(column);
				portlet.setRow(row);
				portlet.setDisplayCount(displayCount);
				portlet.setShowTitle(showTitle);
				portlet.setHeight(height);
				portlet.setWidth(width);
				portlet.setTitleIcon(titleIcon);
				portlet.setPortletScroll(portletScroll);
				portlet.setDataSource(ref);
				portlet.setAlias("ptt" + idx);
				portlet.setPUNID(portal.getUNID());
				idx++;
				if (portlets == null) portlets = new ArrayList<Portlet>();
				portlets.add(portlet);
			}
		}
		if (portlets != null) {
			Collections.sort(portlets, new PortletComparator());
			portal.setPortlets(portlets);
		}

		// 更新PortalParser缓存。
		PortalParser.reloadInstance(portal);
	}// func end

}
