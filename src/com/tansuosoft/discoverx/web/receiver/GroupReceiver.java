/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.GroupType;

/**
 * {@link Group}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class GroupReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		Group group = null;
		group = (Group) this.getResource();
		if (group == null) throw new RuntimeException("非Group资源！");
		group.setGroupType(this.getRequestParameterValueEnum("groupType", GroupType.class, GroupType.Static));
		group.setGroupImplement(this.getRequestParameterValueString("groupImplement", null));
		if (group.getSecurityCode() == 0) {
			int code = this.getRequestParameterValueInt("securityCode", 0);
			if (code == 0) group.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
		}
		// 授权信息
		String params[] = { "authority_directory", "authority_name", "authority_securityCode", "authority_unid" };
		group.setAuthority(this.receiveAuthority(params));
		if (group.getSort() == 0) group.setSort(group.getSecurityCode());
	}
}
