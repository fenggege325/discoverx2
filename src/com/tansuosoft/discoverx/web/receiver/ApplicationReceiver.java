/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.DocumentDisplay;
import com.tansuosoft.discoverx.model.HtmlAttribute;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.PublishOption;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.Tab;
import com.tansuosoft.discoverx.model.TabShow;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * {@link Application1}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ApplicationReceiver extends ResourceReceiver {

	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		Application app = null;
		app = (Application) this.getResource();
		if (app == null) throw new RuntimeException("非Application资源！");
		app.setForm(this.getRequestParameterValueString("form", null));
		app.setWorkflow(this.getRequestParameterValueString("workflow", null));
		app.setDocumentAbstractProviderClassName(this.getRequestParameterValueString("documentAbstractProviderClassName", null));
		app.setDocumentNameProviderClassName(this.getRequestParameterValueString("documentNameProviderClassName", null));
		app.setAutoRecordStateInfo(this.getRequestParameterValueBoolean("autoRecordStateInfo", false));
		app.setTextAccessoryCount(this.getRequestParameterValueInt("textAccessoryCount", 0));
		app.setBusinessSequencePattern(this.getRequestParameterValueString("businessSequencePattern", null));
		app.setProcessSequencePattern(this.getRequestParameterValueString("processSequencePattern", null));
		app.setPublish(this.getRequestParameterValueBoolean("publish", false));
		app.setPublishOption(this.getRequestParameterValueEnum("publishOption", PublishOption.class, PublishOption.All));

		// 配置文件管理员
		String strpascs = this.getRequestParameterValueString("profileAdmins", null);
		String strpats = this.getRequestParameterValueString("profileAdmins_label", null);
		if (strpascs != null && strpascs.length() > 0 && strpats != null && strpats.length() > 0) {
			String[] pascs = StringUtil.splitString(strpascs, ',');
			String[] pats = StringUtil.splitString(strpats, ',');
			if (pascs != null && pats != null && pascs.length == pats.length) {
				Map<Integer, String> pas = new HashMap<Integer, String>();
				app.setProfileAdmins(pas);
				int sc = 0;
				String strsc = null;
				for (int i = 0; i < pascs.length; i++) {
					strsc = pascs[i];
					sc = StringUtil.getValueInt(strsc, -1);
					if (sc <= 0) continue;
					pas.put(sc, pats[i]);
				}
			}
		}

		// 文档显示选项
		DocumentDisplay display = app.getDocumentDisplay();
		if (display == null) {
			display = new DocumentDisplay();
			app.setDocumentDisplay(display);
		}
		display.setExpression(this.getRequestParameterValueString("documentDisplay_expression", null));
		display.setDisplay(this.getRequestParameterValueString("documentDisplay_display", null));
		display.setMaxWidth(this.getRequestParameterValueInt("documentDisplay_maxWidth", 0));
		display.setReadonlyExpression(this.getRequestParameterValueString("documentDisplay_readonlyExpression", null));

		String[] tabParams = { "documentDisplay_tabs_title", "documentDisplay_tabs_sort", "documentDisplay_tabs_description", "documentDisplay_tabs_tabShow", "documentDisplay_tabs_onselect", "documentDisplay_tabs_onleave", "documentDisplay_tabs_control", "documentDisplay_tabs_expression" };
		List<List<String>> vals = this.fillListString(tabParams);
		display.setTabs(null);
		if (vals != null && vals.size() > 0) {
			Tab tab = null;
			for (List<String> x : vals) {
				tab = new Tab();
				tab.setTitle(x.get(0));
				if (tab.getTitle() == null || tab.getTitle().length() == 0) continue;
				tab.setSort(StringUtil.getValueInt(x.get(1), 0));
				tab.setDescription(x.get(2));
				tab.setTabShow(StringUtil.getValueEnum(x.get(3), TabShow.class, tab.getTabShow()));
				tab.setOnselect(x.get(4));
				tab.setOnleave(x.get(5));
				tab.setControl(x.get(6));
				tab.setExpression(x.get(7));
				if (display.getTabs() == null) display.setTabs(new ArrayList<Tab>());
				display.getTabs().add(tab);
			}
		}

		String[] htmlattParams = { "documentDisplay_htmlAttributes_name", "documentDisplay_htmlAttributes_value" };
		vals = this.fillListString(htmlattParams);
		display.setHtmlAttributes(null);
		if (vals != null && vals.size() > 0) {
			HtmlAttribute attr = null;
			for (List<String> x : vals) {
				attr = new HtmlAttribute(x.get(0), x.get(1));
				if (attr.getName() == null || attr.getName().length() == 0) continue;
				if (display.getHtmlAttributes() == null) display.setHtmlAttributes(new ArrayList<HtmlAttribute>());
				display.getHtmlAttributes().add(attr);
			}
		}
		if (StringUtil.isNullOrEmpty(display.getDisplay()) && StringUtil.isNullOrEmpty(display.getExpression()) && StringUtil.isNullOrEmpty(display.getReadonlyExpression()) && display.getMaxWidth() <= 0 && (display.getTabs() == null || display.getTabs().isEmpty()) && (display.getHtmlAttributes() == null || display.getHtmlAttributes().isEmpty())) {
			app.setDocumentDisplay(null);
		}

		// 包含的视图集合填充
		String[] viewRefParams = { "views_unid", "views_title", "views_sort" };
		List<Reference> views = this.receiveReferences(viewRefParams, ResourceDescriptorConfig.getInstance().getResourceDescriptor(View.class).getDirectory());
		if (views != null && views.size() > 0) app.setViews(views);

		// 包含的事件处理程序填充
		app.setEventHandlers(this.receiveEventHandlers());

		// 包含的文档权限填充
		String[] docSecurityParams = { "documentDefaultSecurity_securityCode", "documentDefaultSecurity_securityLevel", "documentDefaultSecurity_workflowLevel" };
		Security docSecurity = this.receiveSecurity(docSecurityParams);
		app.setDocumentDefaultSecurity(docSecurity);

		// 包含所定义的文档的操作集合填充
		String[] oprsParams = { "operations_expression", "operations_visibleExpression", "operations_title", "operations_icon", "operations_sort" };
		List<Operation> oprs = receiveOperations(oprsParams);
		app.setOperations(oprs);

		// 包含所定义的文档新增状态操作集合填充
		String[] newStateOprsParams = { "newStateOperations_expression", "newStateOperations_visibleExpression", "newStateOperations_title", "newStateOperations_icon", "newStateOperations_sort" };
		List<Operation> newStateOperations = receiveOperations(newStateOprsParams);
		app.setNewStateOperations(newStateOperations);

		// 包含所定义的文档的操作集合填充
		List<EventHandlerInfo> evts = this.receiveEventHandlers();
		app.setEventHandlers(evts);

		// 清理无效的引用
		List<Reference> refs = app.getViews();
		List<Reference> newrefs = new ArrayList<Reference>();
		ResourceContext rc = ResourceContext.getInstance();
		if (refs != null) {
			for (Reference ref : refs) {
				if (ref == null) continue;
				if (rc.getResource(ref.getUnid(), View.class) == null) continue;
				newrefs.add(ref);
			}
			if (refs.size() != newrefs.size()) app.setViews(newrefs);
		}

		refs = app.getSubApplications();
		newrefs = new ArrayList<Reference>();
		if (refs != null) {
			for (Reference ref : refs) {
				if (ref == null) continue;
				if (rc.getResource(ref.getUnid(), Application.class) == null) continue;
				newrefs.add(ref);
			}
			if (refs.size() != newrefs.size()) app.setSubApplications(newrefs);
		}
	}
}
