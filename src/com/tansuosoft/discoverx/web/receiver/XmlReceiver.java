/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * {@link Accessory}通过序列化的资源xml内容接收属性的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class XmlReceiver extends ResourceReceiver {
	protected static final String PARAM_NAME_FORM_XML_CONTENT = "xmlcontent";

	/**
	 * 重载receiveResourceDifference：直接返回
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		return;
	}

	/**
	 * 重载receive：通过提交的xml内容通过反序列化更新属性结果。
	 * 
	 * <p>
	 * xml内容需通过名为“xml”的请求参数提供。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receive()
	 */
	@Override
	public void receive() {
		Resource res = this.getResource();
		if (res == null) throw new RuntimeException("无法获取资源！");
		Resource c = null;
		try {
			c = (Resource) res.clone();
		} catch (CloneNotSupportedException e) {
			FileLogger.error(e);
		}
		XmlDeserializer deser = new XmlDeserializer();
		deser.setTarget(res);
		String xml = this.getRequestParameterValueString(PARAM_NAME_FORM_XML_CONTENT, null);
		if (xml == null || xml.length() == 0) throw new RuntimeException("无法获取资源Xml内容！");
		try {
			deser.deserialize(xml, res.getClass());
		} catch (Exception ex) {
			if (c != null) this.setResource(c);
		}
	}

}

