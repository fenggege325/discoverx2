/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.model.AccountType;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * {@link Participant}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class UserReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		User user = null;
		user = (User) this.getResource();
		if (user == null) throw new RuntimeException("非User资源！");
		if (user.getSecurityCode() == 0) {
			int code = this.getRequestParameterValueInt("securityCode", 0);
			if (code == 0) user.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
		}
		String password = this.getRequestParameterValueString("password", null);
		if (password == null || password.length() == 0) {
			password = User.DEFAULT_PASSWORD;
		} else {
			password = StringUtil.getMD5HashString(password);
		}
		if (StringUtil.isBlank(user.getPassword())) user.setPassword(password);
		user.setDuty(this.getRequestParameterValueString("duty", null));
		user.setAccountType(this.getRequestParameterValueInt("accountType", AccountType.ValidAccount.getIntValue() | AccountType.User.getIntValue()));
		// 用户全名
		String fullName = this.getRequestParameterValueString("fullName", null);
		String cn = this.getRequestParameterValueString("cn", null);
		if (cn == null || cn.length() == 0) throw new RuntimeException("用户姓名必须提供！");
		if (fullName == null) {
			Participant pt = ParticipantTreeProvider.getInstance().getParticipantTree(OrganizationsContext.getInstance().getContextUserOrgSc());
			if (pt == null) throw new RuntimeException("找不到用户所属的单位信息！");
			fullName = pt.getName() + User.SEPARATOR + cn;
		} else if (!fullName.endsWith(User.SEPARATOR + cn)) {
			fullName = StringUtil.stringLeftBack(fullName, User.SEPARATOR) + User.SEPARATOR + cn;
		}
		user.setName(fullName);
		// 所属单位unid
		String orgunid = OrganizationsContext.getInstance().getContextUserOrgUnid();
		if (orgunid == null) throw new RuntimeException("找不到用户所属的单位的UNID信息！");
		user.setPUNID(orgunid);

		if (user.getSort() == 0) user.setSort(user.getSecurityCode());
		// 创建用户配置文件。
		Document doc = Profile.getInstance(user).getDocument();
		if (doc != null) user.setProfile(doc.getUNID());

		// 如果用户没有登录，那么把缓存的配置文档删除。
		Session s = Session.getSession(user.getUNID());
		if (s == null) {
			Profile.clear(user);
		} else {
			s.getUser().setProfile(doc.getUNID());
		}
	}
}

