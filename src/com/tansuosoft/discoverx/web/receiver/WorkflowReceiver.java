/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * {@link Workflow}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference：直接返回。
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		return;
	}

	/**
	 * 重载receive：通过{@link XmlReceiver}接收内容。
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receive()
	 */
	@Override
	public void receive() {
		Workflow wf = null;
		wf = (Workflow) this.getResource();
		if (wf == null) throw new RuntimeException("非流程资源！");

		wf.setActivities(null);
		XmlReceiver xmlReceiver = new XmlReceiver();
		xmlReceiver.setHttpRequest(this.getHttpRequest());
		xmlReceiver.setResource(wf);
		xmlReceiver.setPrefix(this.getPrefix());
		xmlReceiver.receive();

		ResourceContext.getInstance().removeObjectFromCache(wf.getUNID());
	}

}

