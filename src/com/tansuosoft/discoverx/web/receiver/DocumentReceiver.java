/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.event.DocumentItemEventArgs;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Field;
import com.tansuosoft.discoverx.model.FieldType;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;

/**
 * {@link Document}资源字段值接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentReceiver extends ResourceReceiver {

	protected class DcoumentItemEventSource extends EventSourceImpl {
	}

	private DcoumentItemEventSource m_eventSource = new DcoumentItemEventSource();

	/**
	 * 重载receive
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receive()
	 */
	@Override
	public void receive() {
		Document doc = null;
		doc = (Document) this.getResource();
		if (doc == null) throw new RuntimeException("非文档资源！");
		List<Field> fields = doc.getFields();
		if (fields == null || fields.isEmpty()) throw new RuntimeException("无法获取文档字段信息！");
		Form form = doc.getForm();
		List<Item> items = (form == null ? null : form.getItems());
		Map<String, FieldType> itemNames = new HashMap<String, FieldType>();
		for (Field f : fields) {
			if (f == null || f.getName() == null) continue;
			itemNames.put(f.getName(), f.getFieldType());
		}
		if (items != null) {
			for (Item item : items) {
				if (item == null || item.getItemName() == null) continue;
				itemNames.put(item.getItemName(), FieldType.Form);
			}
		}
		EventRegister.registerEventHandler(m_eventSource, String.format("%1$s,%2$s", EventHandler.EHT_DOCUMENTITEMVALUECHANGE, EventHandler.EHT_DOCUMENTITEMVALUECHANGED), EventHandlerInfoProvider.getProvider(EventCategory.Document).provide(doc, null));
		List<DocumentItemEventArgs> list = new ArrayList<DocumentItemEventArgs>(itemNames.size());
		String itemValue = null;
		Item item = null;
		String oldValue = null;
		for (String itemName : itemNames.keySet()) {
			if (itemName == null || itemName.length() == 0) continue;
			if (itemNames.get(itemName) == FieldType.Builtin) continue;
			item = doc.getItem(itemName);
			if (item != null && !item.canSave()) continue;
			oldValue = doc.getItemValue(itemName);
			itemValue = this.getRequestParameterValueString(itemName, (item == null ? Item.DEFAULT_DELIMITER : item.getDelimiter()), oldValue);
			if (itemValue != null) {
				DocumentItemEventArgs diee = new DocumentItemEventArgs();
				diee.setDocument(doc);
				diee.setSession(SessionContext.getSession(this.getHttpRequest()));
				diee.setItemValueOld(oldValue);
				diee.setItemName(itemName);
				diee.setItemValueNew(itemValue);
				m_eventSource.callEventHandler(this, EventHandler.EHT_DOCUMENTITEMVALUECHANGE, diee);
				oldValue = doc.replaceItemValue(itemName, itemValue);
				// if (oldValue != null && !oldValue.equalsIgnoreCase(itemValue)) {
				if (!itemValue.equalsIgnoreCase(oldValue)) {
					list.add(diee);
					diee.setItemValueOld(oldValue);
				}
			}// if end
		}// for end
		if (list.size() > 0) {
			for (DocumentItemEventArgs x : list) {
				m_eventSource.callEventHandler(this, EventHandler.EHT_DOCUMENTITEMVALUECHANGED, x);
			}
		}// if end
	}

	/**
	 * 重载receiveResourceDifference：直接返回。
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		return;
	}
}

