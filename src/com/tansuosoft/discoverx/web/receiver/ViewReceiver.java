/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.resource.XmlResourceInserter;
import com.tansuosoft.discoverx.bll.resource.XmlResourceUpdater;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.PortletContentType;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.model.PortletDataSourceType;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.model.ViewOrder;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * {@link View}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		View view = null;
		view = (View) this.getResource();
		if (view == null) throw new RuntimeException("非View资源！");

		view.setTable(this.getRequestParameterValueString("table", null));
		view.setCommonCondition(this.getRequestParameterValueString("commonCondition", null));
		view.setDisplayCountPerPage(this.getRequestParameterValueInt("displayCountPerPage", 20));
		String rawQuery = this.getRequestParameterValueString("rawQuery", null);
		if (rawQuery != null && rawQuery.length() > 0) rawQuery = rawQuery.replace("\r\n", " ").replace("\r", " ").replace("\n", " ");
		view.setRawQuery(rawQuery);
		view.setShowSelector(this.getRequestParameterValueBoolean("showSelector", false));
		view.setNumberIndicator(this.getRequestParameterValueBoolean("numberIndicator", false));
		view.setViewType(this.getRequestParameterValueEnum("viewType", ViewType.class, ViewType.DocumentView));
		view.setApplicationOutline(this.getRequestParameterValueBoolean("applicationOutline", false));
		view.setPortalDatasource(this.getRequestParameterValueBoolean("portalDatasource", false));
		view.setDebugable(this.getRequestParameterValueBoolean("debugable", false));
		view.setDisplay(this.getRequestParameterValueString("display", null));
		view.setViewParserImplement(this.getRequestParameterValueString("viewParserImplement", null));
		view.setViewQueryImplement(this.getRequestParameterValueString("viewQueryImplement", null));
		view.setMeasurement(this.getRequestParameterValueEnum("measurement", SizeMeasurement.class, SizeMeasurement.Percentage));
		view.setCache(this.getRequestParameterValueBoolean("cache", false));
		view.setCacheExpires(this.getRequestParameterValueInt("cacheExpires", 60));
		view.setCacheByUser(this.getRequestParameterValueBoolean("cacheByUser", true));
		view.setCachePages(this.getRequestParameterValueInt("cachePages", 3));

		// 视图操作集合填充
		String[] oprsParams = { "operations_expression", "operations_visibleExpression", "operations_title", "operations_icon", "operations_sort" };
		List<Operation> oprs = receiveOperations(oprsParams);
		view.setOperations(oprs);

		List<List<String>> vals = null;

		// 视图列配置属性填充
		String[] columnParams = { "columns_tableName", "columns_columnName", "columns_columnAlias", "columns_title", "columns_dictionary", "columns_expression", "columns_width", "columns_categorized", "columns_searchable", "columns_linkable" };
		vals = this.fillListString(columnParams);
		if (vals != null) {
			List<ViewColumn> columns = null;
			ViewColumn column = null;
			for (List<String> x : vals) {
				column = new ViewColumn();
				column.setTableName(x.get(0));
				column.setColumnName(x.get(1));
				if (StringUtil.isBlank(column.getTableName()) && StringUtil.isBlank(column.getColumnName())) continue;
				column.setColumnAlias(x.get(2));
				column.setTitle(x.get(3));
				column.setDictionary(x.get(4));
				column.setExpression(x.get(5));
				column.setWidth(StringUtil.getValueInt(x.get(6), column.getWidth()));
				column.setCategorized(StringUtil.getValueBool(x.get(7), column.getCategorized()));
				column.setSearchable(StringUtil.getValueBool(x.get(8), column.getSearchable()));
				column.setLinkable(StringUtil.getValueBool(x.get(9), column.getLinkable()));
				if (columns == null) columns = new ArrayList<ViewColumn>();
				columns.add(column);
			}
			view.setColumns(columns);
		}
		buildRawQueryViewColumns(view);

		// 视图选择条件配置属性填充
		String[] conditionParams = { "conditions_tableName", "conditions_columnName", "conditions_combineNextWith", "conditions_compare", "conditions_expression", "conditions_leftBracket", "conditions_numericCompare", "conditions_rightBracket", "conditions_rightValue" };
		vals = this.fillListString(conditionParams);
		if (vals != null) {
			ViewCondition viewCondition = null;
			List<ViewCondition> viewConditions = null;
			for (List<String> x : vals) {
				viewCondition = new ViewCondition();
				viewCondition.setTableName(x.get(0));
				viewCondition.setColumnName(x.get(1));
				if (StringUtil.isBlank(viewCondition.getTableName()) && StringUtil.isBlank(viewCondition.getColumnName())) continue;
				viewCondition.setCombineNextWith(x.get(2));
				viewCondition.setCompare(x.get(3));
				viewCondition.setExpression(x.get(4));
				viewCondition.setLeftBracket(x.get(5));
				viewCondition.setNumericCompare(StringUtil.getValueBool(x.get(6), viewCondition.getNumericCompare()));
				viewCondition.setRightBracket(x.get(7));
				viewCondition.setRightValue(x.get(8));
				if (viewConditions == null) viewConditions = new ArrayList<ViewCondition>();
				viewConditions.add(viewCondition);
			}
			view.setConditions(viewConditions);
		}

		// 视图排序依据配置属性填充
		String[] orderParams = { "orders_tableName", "orders_columnName", "orders_expression", "orders_orderBy" };
		vals = this.fillListString(orderParams);
		view.setOrders(null);
		if (vals != null) {
			ViewOrder viewOrder = null;
			List<ViewOrder> viewOrders = null;
			for (List<String> x : vals) {
				viewOrder = new ViewOrder();
				viewOrder.setTableName(x.get(0));
				viewOrder.setColumnName(x.get(1));
				if (StringUtil.isBlank(viewOrder.getTableName()) && StringUtil.isBlank(viewOrder.getColumnName())) continue;
				viewOrder.setExpression(x.get(2));
				viewOrder.setOrderBy(x.get(3));
				if (viewOrders == null) viewOrders = new ArrayList<ViewOrder>();
				viewOrders.add(viewOrder);
			}
			view.setOrders(viewOrders);
		}

		// 文档视图要设置其所属模块的视图引用
		String punid = view.getPUNID();
		if (punid != null && punid.length() > 0) {
			Application app = ResourceContext.getInstance().getResource(punid, Application.class);
			if (app != null) {
				boolean updateFlag = false;
				List<Reference> views = app.getViews();
				if (views == null) {
					views = new ArrayList<Reference>();
					app.setViews(views);
				}
				if (views.isEmpty()) {
					updateFlag = true;
					views.add(new Reference(view.getUNID(), view.getName(), view.getDirectory(), view.getSort()));
				} else {
					boolean found = false;
					for (Reference r : views) {
						if (r != null && view.getUNID().equalsIgnoreCase(r.getUnid())) {
							found = true;
							if (r.getSort() != view.getSort()) {
								updateFlag = true;
								r.setSort(view.getSort());
							}
							break;
						}
					}
					if (!found) {
						updateFlag = true;
						views.add(new Reference(view.getUNID(), view.getName(), view.getDirectory(), view.getSort()));
					}// if end
				}// else end
				if (updateFlag) {
					XmlResourceUpdater updater = new XmlResourceUpdater();
					try {
						updater.update(app, SessionContext.getSession(this.getHttpRequest()));
					} catch (ResourceException e) {
						FileLogger.error(e);
					}
				}
			}// if end
		}// if end

		// 数据源处理
		if (view.getPortalDatasource()) {
			String unid = view.getUNID();
			if (ResourceHelper.exists(View.class, unid)) return;
			PortletDataSource pds = new PortletDataSource();
			pds.setName(view.getName());
			pds.setAlias("pds" + view.getAlias().substring(4));
			pds.setDataSourceType(PortletDataSourceType.Ajax);
			pds.setTarget("~viewdata.jsp?view=" + view.getAlias() + "&vd=portletajaxview&vo=0");
			pds.setContentType(PortletContentType.Url);
			pds.setDescription(view.getDescription());
			XmlResourceInserter inserter = new XmlResourceInserter();
			try {
				inserter.insert(pds, SessionContext.getSession(this.getHttpRequest()));
			} catch (ResourceException e) {
				FileLogger.error(e);
			}
		}
	}// func end

	/**
	 * 为手动输入sql生成配套ViewColumn。
	 * 
	 * @param view
	 */
	protected void buildRawQueryViewColumns(View view) {
		String sql = view.getRawQuery();
		if (sql == null || sql.trim().length() == 0) return;
		List<ViewColumn> list = view.getColumns();
		if (list != null && list.size() > 0) return;

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sql.length(); i++) {
			sb.append(sql.charAt(i));
		}
		String sqllc = sb.toString().toLowerCase();
		int start = sqllc.indexOf(SELECT);
		int end = sqllc.indexOf(FROM);
		if (start < 0 || end < 0 || end < start) return;

		String vcs[] = StringUtil.splitString(sql.substring(start + SELECT.length(), end), COMMA);
		if (vcs == null || vcs.length == 0) return;
		if (list == null) {
			list = new ArrayList<ViewColumn>(vcs.length);
			view.setColumns(list);
		}
		for (int i = 0; i < vcs.length; i++) {
			ViewColumn vc = new ViewColumn();
			vc.setTableName(DTPH);
			vc.setColumnName(DCPH);
			vc.setColumnAlias(tryGetColumnAlias(vcs[i], "alias" + (i + 1)));
			vc.setTitle("标题" + (i + 1));
			list.add(vc);
		}
	}

	/**
	 * 尝试解析并返回列别名。
	 * 
	 * @param raw
	 * @param d
	 * @return
	 */
	protected String tryGetColumnAlias(String raw, String d) {
		if (raw == null || raw.trim().length() == 0) return d;
		int pos = raw.indexOf(PEROID);
		String r = null;
		if (raw.indexOf(LB) < 0 && raw.indexOf(RB) < 0) {
			if (pos > 0) r = raw.substring(pos + 1);
			else r = raw;
		}
		if (r != null) return r.replace("||", "").replace("|", "").replace("&", "").replace("%", "").replace("+", "").replace("-", "").replace("*", "").replace("/", "").replace("\\", "").trim();
		return d;
	}

	protected static final String SELECT = "select ";
	protected static final String FROM = " from ";
	protected static final char COMMA = ',';
	protected static final char PEROID = '.';
	protected static final char LB = '(';
	protected static final char RB = ')';
	public static final String DTPH = "[dummy_table_placeholder]";
	public static final String DCPH = "[dummy_column_placeholder]";
}
