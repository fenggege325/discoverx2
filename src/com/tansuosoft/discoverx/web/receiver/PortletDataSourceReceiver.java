/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.model.PortletContentType;
import com.tansuosoft.discoverx.model.PortletDataSource;
import com.tansuosoft.discoverx.model.PortletDataSourceType;

/**
 * {@link com.tansuosoft.discoverx.model.PortletDataSource}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author wxs@tensosoft.com
 */
public class PortletDataSourceReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		PortletDataSource portletDataSource = null;
		portletDataSource = (PortletDataSource) this.getResource();
		if (portletDataSource == null) throw new RuntimeException("非PortletDataSource资源！");
		PortletDataSourceType dataSourceType = this.getRequestParameterValueEnum("dataSourceType", PortletDataSourceType.class, PortletDataSourceType.View);
		PortletContentType contentType = this.getRequestParameterValueEnum("contentType", PortletContentType.class, PortletContentType.Entry);
		portletDataSource.setDataSourceType(dataSourceType);
		portletDataSource.setContentType(contentType);
		portletDataSource.setRefreshInterval(this.getRequestParameterValueInt("refreshInterval", 0));
		portletDataSource.setTarget(this.getRequestParameterValueString("portletDataSource_target", null));
		portletDataSource.setServerContentRender(this.getRequestParameterValueString("serverContentRender", null));
		portletDataSource.setClientContentRender(this.getRequestParameterValueString("clientContentRender", null));
	}

}
