/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.model.AccountType;
import com.tansuosoft.discoverx.model.Organization;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * {@link Organization}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OrganizationReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		Organization organization = null;
		organization = (Organization) this.getResource();
		if (organization == null) throw new RuntimeException("非Organization资源！");
		if (organization.getSecurityCode() == 0) {
			int code = this.getRequestParameterValueInt("securityCode", 0);
			if (code == 0) organization.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
		}
		String fn = this.getRequestParameterValueString("fullName", "");
		if (fn.length() == 0 || fn.indexOf(User.SEPARATOR_CHAR) < 0) {
			ParticipantTree root = ParticipantTreeProvider.getInstance().getRoot();
			fn = root.getName() + User.SEPARATOR + organization.getName();
		} else if (fn.endsWith(User.SEPARATOR)) {
			fn += organization.getName();
		} else {
			fn = StringUtil.stringLeftBack(fn, User.SEPARATOR) + User.SEPARATOR + organization.getName();
		}
		organization.setFullName(fn);
		organization.setAccountType(this.getRequestParameterValueInt("accountType", AccountType.InvalidAccount.getIntValue() | AccountType.Unit.getIntValue()));
		if (organization.getSort() == 0) organization.setSort(organization.getSecurityCode());
		organization.setPUNID(OrganizationsContext.getInstance().getContextUserOrgUnid());
	}
}

