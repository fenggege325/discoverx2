/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.model.AccessoryType;

/**
 * {@link AccessoryType}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AccessoryTypeReceiver extends ResourceReceiver {

	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		AccessoryType res = null;
		res = (AccessoryType) this.getResource();
		if (res == null) throw new RuntimeException("非AccessoryType资源！");
		res.setRestrictToConfig(this.getRequestParameterValueBoolean("restrictToConfig", false));
		res.setVisible(this.getRequestParameterValueBoolean("visible", true));
		res.setMinCount(this.getRequestParameterValueInt("minCount", 0));
		res.setMaxCount(this.getRequestParameterValueInt("maxCount", 0));
		res.setMaxSize(this.getRequestParameterValueInt("maxSize", 0));
		res.setFileNameExtension(this.getRequestParameterValueString("fileNameExtension", null));
	}
}

