/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.model.Accessory;

/**
 * {@link Accessory}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AccessoryReceiver extends ResourceReceiver {

	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		Accessory res = null;
		res = (Accessory) this.getResource();
		if (res == null) throw new RuntimeException("非Accessory资源！");
		res.setSize(this.getRequestParameterValueInt("size", 0));
		res.setStyleTemplate(this.getRequestParameterValueString("template", null));
		res.setState(this.getRequestParameterValueString("state", null));
		res.setFileName(this.getRequestParameterValueString("fileName", null));
		res.setCodeTemplate(this.getRequestParameterValueString("codeTemplate", null));
		res.setParentDirectory(this.getRequestParameterValueString("parentDirectory", null));
		res.setAccessoryType(this.getRequestParameterValueString("accessoryType", null));
	}
}

