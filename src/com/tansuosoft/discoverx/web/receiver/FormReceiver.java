/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.tansuosoft.discoverx.bll.ItemComparator;
import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.AccessoryType;
import com.tansuosoft.discoverx.model.DataSourceConstant;
import com.tansuosoft.discoverx.model.DataSourceParticipant;
import com.tansuosoft.discoverx.model.DataSourceResource;
import com.tansuosoft.discoverx.model.DataSourceView;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.HtmlAttribute;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemGroup;
import com.tansuosoft.discoverx.model.ItemTypeAccessoryOption;
import com.tansuosoft.discoverx.model.ItemTypeDateTimeOption;
import com.tansuosoft.discoverx.model.ItemTypeLinkOption;
import com.tansuosoft.discoverx.model.ItemTypeNumberOption;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.SizeMeasurement;
import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UNIDProvider;

/**
 * {@link Form}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class FormReceiver extends ResourceReceiver {

	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		Form form = null;
		form = (Form) this.getResource();
		if (form == null) throw new RuntimeException("非表单资源！");

		// 2013-12-06:处理新增的通过通过可视化表单配置提交的修改
		String xml = this.getRequestParameterValueString(XmlReceiver.PARAM_NAME_FORM_XML_CONTENT, null);
		if (xml != null && xml.length() > 0 && xml.startsWith("<?xml")) {
			XmlReceiver xmlReceiver = new XmlReceiver();
			xmlReceiver.setHttpRequest(this.getHttpRequest());
			xmlReceiver.setResource(form);
			xmlReceiver.setPrefix(this.getPrefix());
			xmlReceiver.receive();
			return;
		}

		// 处理原有表单修改
		form.setMeasurement(this.getRequestParameterValueEnum("measurement", SizeMeasurement.class, form.getMeasurement()));
		int styleOption = this.getRequestParameterValueInt("styleOption", 0);
		if (styleOption != 0) {
			form.setStyle(this.getRequestParameterValueString("style", null));
		} else {
			form.setStyle(null);
		}
		form.setItemCaptionWidth(this.getRequestParameterValueInt("itemCaptionWidth", 120));

		String strDeleteditems = this.getRequestParameterValueString("deleteditems", "");
		String strDeleteditemgroups = this.getRequestParameterValueString("deleteditemgroups", "");
		int itemscount = this.getRequestParameterValueInt("itemscount", 0);
		int groupitemscount = this.getRequestParameterValueInt("groupitemscount", 0);

		String fieldName = null;
		String fieldValue = null;
		boolean foundFlag = false;
		boolean newFlag = false;

		// 字段组
		List<ItemGroup> itemGroups = form.getItemGroups();
		if (itemGroups == null) itemGroups = new ArrayList<ItemGroup>();
		List<ItemGroup> newitemGroups = new ArrayList<ItemGroup>();

		// 删除的
		String[] deleteditemgroups = StringUtil.splitString(strDeleteditemgroups, ',');
		for (ItemGroup x : itemGroups) {
			foundFlag = false;
			for (String unid : deleteditemgroups) {
				if (unid != null && unid.equalsIgnoreCase(x.getUNID())) {
					foundFlag = true;
					break;
				}
			}
			if (!foundFlag) newitemGroups.add(x);
		}

		// 更新和新增的
		ItemGroup itemGroup = null;
		String itemGroupFieldPrefix = "itemGroups";
		String[] itemGroupFieldNames = { "UNID", "name", "alias", "description", "groupType", "minCount", "maxCount", "display", "displayImplement", "readonlyDisplayImplement", "visibleExpression", "outputHiddenField", "readonlyExpression", "row", "column", "width", "captionWidth", "height", "captionHeight", "saveToTableOption", "createTableImplement", "saveToTableImplement", "removeFromTableImplement" };
		for (int i = 0; i < groupitemscount; i++) {
			itemGroup = null;
			for (int j = 0; j < itemGroupFieldNames.length; j++) {
				fieldName = String.format("%1$s_%2$s_%3$s", itemGroupFieldPrefix, i, itemGroupFieldNames[j]);
				fieldValue = this.getRequestParameterValueString(fieldName, null);
				if (j == 0) {
					foundFlag = false;
					newFlag = false;
					if (fieldValue == null || fieldValue.length() == 0) continue;
					if (fieldValue.equalsIgnoreCase("[new]")) {
						itemGroup = new ItemGroup();
						itemGroup.setPUNID(form.getUNID());
						newFlag = true;
					} else {
						itemGroup = form.getItemGroupByUNID(fieldValue);
					}
					foundFlag = (itemGroup != null);
					if (!foundFlag) break;
				} else {
					if (j == 1 && StringUtil.isBlank(fieldValue)) {
						foundFlag = false;
						break;
					}
					ObjectUtil.propSetValueFromText(itemGroup, StringUtil.toPascalCase(itemGroupFieldNames[j]), fieldValue);
				}
			}
			if (!foundFlag) continue;
			itemGroup.setCreated(null);
			itemGroup.setCreator(null);
			itemGroup.setModified(null);
			itemGroup.setModifier(null);
			if (newFlag) {
				itemGroup.setUNID(UNIDProvider.getUNID());
				newitemGroups.add(itemGroup);
			}
		}

		// 字段
		List<Item> items = form.getItems();
		if (items == null) items = new ArrayList<Item>();
		List<Item> newitems = new ArrayList<Item>();

		// 删除的
		String[] deleteditems = StringUtil.splitString(strDeleteditems, ',');
		for (Item x : items) {
			foundFlag = false;
			for (String unid : deleteditems) {
				if (unid != null && unid.equalsIgnoreCase(x.getUNID())) {
					foundFlag = true;
					break;
				}
			}
			if (!foundFlag) newitems.add(x);
		}

		// 更新和新增的
		Item item = null;
		String itemFieldPrefix = "items";
		String[] itemFieldNames = { "UNID", "alias", "name", "description", "type", "display", "displayImplement", "readonlyDisplayImplement", "delimiter", "defaultValue", "computationMethod", "computationExpression", "visibleExpression", "outputHiddenField", "readonlyExpression", "dataSourceType", "dataSourceInput", "linkageItem", "valueStoreItem", "customDataSourceImplement", "jsValidator", "jsTranslactor", "required", "group", "row", "column", "width", "captionWidth", "height", "captionHeight", "summarizable", "control", "size" };
		for (int i = 0; i < itemscount; i++) {
			item = null;
			for (int j = 0; j < itemFieldNames.length; j++) {
				fieldName = String.format("%1$s_%2$s_%3$s", itemFieldPrefix, i, itemFieldNames[j]);
				fieldValue = this.getRequestParameterValueString(fieldName, null);
				if (j == 0) {
					foundFlag = false;
					newFlag = false;
					if (fieldValue == null || fieldValue.length() == 0) break;
					if (fieldValue.equalsIgnoreCase("[new]")) {
						item = new Item();
						item.setPUNID(form.getUNID());
						newFlag = true;
					} else {
						item = form.getItemByUNID(fieldValue);
					}
					foundFlag = (item != null);
					if (!foundFlag) break;
				} else {
					if (foundFlag) ObjectUtil.propSetValueFromText(item, StringUtil.toPascalCase(itemFieldNames[j]), fieldValue);
				}
			} // for j end
			if (!foundFlag) continue;
			item.setCreated(null);
			item.setCreator(null);
			item.setModified(null);
			item.setModifier(null);
			// 字段类型选项
			item.setDataTypeOption(null);
			switch (item.getType().getIntValue()) {
			case 2: // 数字
				ItemTypeNumberOption itno = new ItemTypeNumberOption();
				fieldName = String.format("%1$s_%2$s_ItemTypeNumberOption_precision", itemFieldPrefix, i);
				itno.setPrecision(this.getRequestParameterValueInt(fieldName, 8));
				fieldName = String.format("%1$s_%2$s_ItemTypeNumberOption_scale", itemFieldPrefix, i);
				itno.setScale(this.getRequestParameterValueInt(fieldName, 0));
				item.setDataTypeOption(itno);
				break;
			case 3: // 日期时间
				ItemTypeDateTimeOption itdto = new ItemTypeDateTimeOption();
				fieldName = String.format("%1$s_%2$s_ItemTypeDateTimeOption_titleFormat", itemFieldPrefix, i);
				itdto.setTitleFormat(this.getRequestParameterValueString(fieldName, "yyyy-MM-dd HH:mm:ss"));
				fieldName = String.format("%1$s_%2$s_ItemTypeDateTimeOption_valueFormat", itemFieldPrefix, i);
				itdto.setValueFormat(this.getRequestParameterValueString(fieldName, "yyyy-MM-dd HH:mm:ss"));
				item.setDataTypeOption(itdto);
				break;
			case 5: // 链接
				ItemTypeLinkOption itlo = new ItemTypeLinkOption();
				fieldName = String.format("%1$s_%2$s_ItemTypeLinkOption_target", itemFieldPrefix, i);
				itlo.setTarget(this.getRequestParameterValueString(fieldName, "_blank"));
				item.setDataTypeOption(itlo);
				break;
			case -1: // 附加文件
				ItemTypeAccessoryOption itao = new ItemTypeAccessoryOption();
				fieldName = String.format("%1$s_%2$s_ItemTypeAccessoryOption_accessoryTypes", itemFieldPrefix, i);
				String acts[] = StringUtil.splitString(this.getRequestParameterValueString(fieldName, null), ',');
				List<Resource> actList = XmlResourceLister.getResources(AccessoryType.class);
				if (acts != null && actList != null) {
					List<Reference> refList = null;
					String actdir = ResourceDescriptorConfig.getInstance().getResourceDirectory(AccessoryType.class);
					for (Resource r : actList) {
						int refSort = 0;
						for (String s : acts) {
							if (s.equalsIgnoreCase(r.getName())) {
								Reference ref = new Reference();
								ref.setDirectory(actdir);
								ref.setUnid(r.getUNID());
								ref.setTitle(s);
								ref.setSort(refSort++);
								if (refList == null) refList = new ArrayList<Reference>();
								refList.add(ref);
							}// if end
						}// for end
					}// for end
					itao.setAccessoryTypes(refList);
				}// if end
				item.setDataTypeOption(itao);
				break;
			default:
				break;
			}
			// 数据源选项
			item.setDataSource(null);
			switch (item.getDataSourceType().getIntValue()) {
			case 1: // Resource
				DataSourceResource dsr = new DataSourceResource();
				fieldName = String.format("%1$s_%2$s_DataSource_titleFormat", itemFieldPrefix, i);
				dsr.setTitleFormat(this.getRequestParameterValueString(fieldName, "Name"));
				fieldName = String.format("%1$s_%2$s_DataSource_valueFormat", itemFieldPrefix, i);
				dsr.setValueFormat(this.getRequestParameterValueString(fieldName, "UNID"));
				fieldName = String.format("%1$s_%2$s_DataSourceResource_directory", itemFieldPrefix, i);
				dsr.setDirectory(this.getRequestParameterValueString(fieldName, null));
				fieldName = String.format("%1$s_%2$s_DataSourceResource_UNID", itemFieldPrefix, i);
				dsr.setUNID(this.getRequestParameterValueString(fieldName, null));
				fieldName = String.format("%1$s_%2$s_DataSourceResource_fullTree", itemFieldPrefix, i);
				dsr.setFullTree(this.getRequestParameterValueBoolean(fieldName, true));
				item.setDataSource(dsr);
				break;
			case 2: // View
				DataSourceView dsv = new DataSourceView();
				fieldName = String.format("%1$s_%2$s_DataSource_titleFormat", itemFieldPrefix, i);
				dsv.setTitleFormat(this.getRequestParameterValueString(fieldName, "1"));
				fieldName = String.format("%1$s_%2$s_DataSource_valueFormat", itemFieldPrefix, i);
				dsv.setValueFormat(this.getRequestParameterValueString(fieldName, "0"));
				fieldName = String.format("%1$s_%2$s_DataSourceView_UNID", itemFieldPrefix, i);
				dsv.setUNID(this.getRequestParameterValueString(fieldName, null));
				item.setDataSource(dsv);
				break;
			case 3: // Participant
				DataSourceParticipant dsp = new DataSourceParticipant();
				fieldName = String.format("%1$s_%2$s_DataSource_titleFormat", itemFieldPrefix, i);
				dsp.setTitleFormat(this.getRequestParameterValueString(fieldName, "cn"));
				fieldName = String.format("%1$s_%2$s_DataSource_valueFormat", itemFieldPrefix, i);
				dsp.setValueFormat(this.getRequestParameterValueString(fieldName, "fn"));
				fieldName = String.format("%1$s_%2$s_DataSourceParticipant_participantType", itemFieldPrefix, i);
				dsp.setParticipantType(this.getRequestParameterValueEnum(fieldName, ParticipantType.class, ParticipantType.Person));
				item.setDataSource(dsp);
				break;
			case 4: // Constant
				DataSourceConstant dsc = new DataSourceConstant();
				fieldName = String.format("%1$s_%2$s_DataSourceConstant_configValue", itemFieldPrefix, i);
				dsc.setConfigValue(this.getRequestParameterValueString(fieldName, null));
				item.setDataSource(dsc);
				break;
			case 100: // Custom
				fieldName = String.format("%1$s_%2$s_customDataSourceImplement", itemFieldPrefix, i);
				item.setCustomDataSourceImplement(this.getRequestParameterValueString(fieldName, null));
				break;
			default:
				break;
			}
			// 字段html属性
			String[] htmlAttributeParams = new String[2];
			htmlAttributeParams[0] = String.format("%1$s_htmlAttribute_name", item.getItemName());
			htmlAttributeParams[1] = String.format("%1$s_htmlAttribute_value", item.getItemName());
			List<List<String>> vals = this.fillListString(htmlAttributeParams);
			item.setHtmlAttributes(null);
			if (vals != null && vals.size() > 0) {
				HtmlAttribute attr = null;
				for (List<String> x : vals) {
					attr = new HtmlAttribute(x.get(0), x.get(1));
					if (attr.getName() == null || attr.getName().length() == 0) continue;
					if (item.getHtmlAttributes() == null) item.setHtmlAttributes(new ArrayList<HtmlAttribute>());
					item.getHtmlAttributes().add(attr);
				}
			}
			// 如果是新增的字段
			if (newFlag) {
				item.setUNID(UNIDProvider.getUNID());
				newitems.add(item);
			}
		} // for i end

		Comparator<Item> c = new ItemComparator();
		Collections.sort(newitemGroups, c);
		Collections.sort(newitems, c);
		form.setItemGroups(newitemGroups);
		form.setItems(newitems);

	}// func end
}
