/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.model.Opinion;

/**
 * {@link Opinion}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OpinionReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		Opinion opinion = null;
		opinion = (Opinion) this.getResource();
		if (opinion == null) throw new RuntimeException("非Opinion资源！");
		opinion.setSignature(this.getRequestParameterValueString("signature", null));
		opinion.setPassed(this.getRequestParameterValueBoolean("passed", true));
		opinion.setAgent(this.getRequestParameterValueString("agent", null));
		opinion.setBody(this.getRequestParameterValueString("body", null));
		opinion.setState(this.getRequestParameterValueString("state", null));
		opinion.setOpinionType(this.getRequestParameterValueString("opinionType", null));
	}
}

