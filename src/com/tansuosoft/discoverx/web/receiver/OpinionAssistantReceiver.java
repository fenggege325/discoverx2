/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.model.OpinionAssistant;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * {@link OpinionAssistant}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OpinionAssistantReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		OpinionAssistant opinionAssistant = null;
		opinionAssistant = (OpinionAssistant) this.getResource();
		if (opinionAssistant == null) throw new RuntimeException("非OpinionAssistant资源！");
		String strs[] = null;
		String requestParameterValue = null;
		
		//设置常见祈使词
		requestParameterValue=this.getRequestParameterValueString("imperatives", null);
		if(requestParameterValue!=null){
			strs=requestParameterValue.split("\r\n");
			List<String> imperatives = null;
			if(strs!=null){
				imperatives = new ArrayList<String>(strs.length);
				for (String x : strs) {
					if(x!=null && !StringUtil.isBlank(x)){
						imperatives.add(x.trim());
					}
				}
				opinionAssistant.setImperatives(imperatives);
			}
		}
		
		//设置常见称呼词
		requestParameterValue=this.getRequestParameterValueString("entitles", null);
		if(requestParameterValue!=null){
			strs=requestParameterValue.split("\r\n");
			List<String> entitles = null;
			if(strs!=null){
				entitles = new ArrayList<String>(strs.length);
				for (String x : strs) {
					if(x!=null && !StringUtil.isBlank(x)){
						entitles.add(x.trim());
					}
				}
				opinionAssistant.setEntitles(entitles);
			}
		}
		
		//设置常见动作词
		requestParameterValue=this.getRequestParameterValueString("actions", null);
		if(requestParameterValue!=null){
			strs=requestParameterValue.split("\r\n");
			List<String> actions = null;
			if(strs!=null){
				actions = new ArrayList<String>(strs.length);
				for (String x : strs) {
					if(x!=null && !StringUtil.isBlank(x)){
						actions.add(x.trim());
					}
				}
				opinionAssistant.setActions(actions);
			}
		}

		//设置常见意见内容
		requestParameterValue=this.getRequestParameterValueString("opinions", null);
		if(requestParameterValue!=null){
			strs=requestParameterValue.split("\r\n");
			List<String> opinions = null;
			if(strs!=null){
				opinions = new ArrayList<String>(strs.length);
				for (String x : strs) {
					if(x!=null && !StringUtil.isBlank(x)){
						opinions.add(x.trim());
					}
				}
				opinionAssistant.setOpinions(opinions);
			}
		}
	}

}

