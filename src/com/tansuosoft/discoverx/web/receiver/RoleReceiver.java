/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.model.Role;

/**
 * {@link Application1}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class RoleReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		Role role = null;
		role = (Role) this.getResource();
		if (role == null) throw new RuntimeException("非Role资源！");
		role.setSecurityRange(this.getRequestParameterValueInt("securityRange", 1));
		if (role.getSecurityCode() == 0) {
			int code = this.getRequestParameterValueInt("securityCode", 0);
			if (code == 0) role.setSecurityCode((int) SequenceProvider.getSecurityCodeInstance().getSequence());
		}
		if (role.getSort() == 0) role.setSort(role.getSecurityCode());
	}

}

