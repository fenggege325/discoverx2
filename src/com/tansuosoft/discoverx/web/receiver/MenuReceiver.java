/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.receiver;

import com.tansuosoft.discoverx.bll.ResourceReceiver;
import com.tansuosoft.discoverx.model.Menu;
import com.tansuosoft.discoverx.model.MenuTarget;
import com.tansuosoft.discoverx.util.UNIDProvider;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * {@link Menu}资源属性接收的{@link ResourceReceiver}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class MenuReceiver extends ResourceReceiver {
	/**
	 * 重载receiveResourceDifference
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceReceiver#receiveResourceDifference()
	 */
	@Override
	protected void receiveResourceDifference() {
		Menu menu = (Menu) this.getResource();
		if (menu == null) throw new RuntimeException("非Menu资源！");
		menu.setTarget(this.getRequestParameterValueString("target", null));
		menu.setMenuTarget(this.getRequestParameterValueEnum("menuTarget", MenuTarget.class, MenuTarget.Category));
		String childrenXml = this.getRequestParameterValueString("childrenXml", null);
		if (childrenXml == null || childrenXml.length() == 0) return;
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n");
		sb.append("<resource type=\"");
		sb.append(Menu.class.getName());
		sb.append("\">\r\n");
		sb.append("<UNID>").append(UNIDProvider.getUNID()).append("</UNID>");
		sb.append(childrenXml);
		sb.append("</resource>");
		Menu tmp = new Menu();
		XmlDeserializer deser = new XmlDeserializer();
		deser.setTarget(tmp);
		deser.deserialize(sb.toString(), Menu.class);
		menu.setChildren(tmp.getChildren());
	}

}
