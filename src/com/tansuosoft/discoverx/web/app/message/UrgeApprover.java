/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.event.WorkflowDataEventArgs;

/**
 * 流程催办相关的事件处理程序。
 * 
 * <p>
 * 系统内部使用。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class UrgeApprover implements EventHandler {

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof WorkflowDataEventArgs)) return;
		WorkflowDataEventArgs wfdea = ((WorkflowDataEventArgs) e);
		WFData wfdata = wfdea.getData();
		if (!wfdata.checkLevel(WFDataLevel.Approver)) return;
		try {
			UrgeContext nc = UrgeContext.getInstance();
			if (wfdata.getDone() || StringUtil.isBlank(wfdata.getNotify()) || wfdea.checkEventHandlerType(EventHandler.EHT_WORKFLOWDATADELETED)) {
				nc.remove(wfdata.getUNID());
				return;
			}
			String name = wfdea.getWorkflowRuntime().getDocument().getName();
			nc.append(wfdata, name);
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}// func end
}// class end

