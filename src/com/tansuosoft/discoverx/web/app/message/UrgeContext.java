/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.WFData;

/**
 * 管理催办通知相关功能的类。
 * 
 * @author coca@tensosoft.com
 */
public class UrgeContext {
	private static UrgeContext m_instance = null;
	private static Object m_lock = new Object();
	private final Map<String, ScheduledFuture<Long>> map = new HashMap<String, ScheduledFuture<Long>>();
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(10);

	/**
	 * 缺省构造器。
	 */
	private UrgeContext() {
		init();
	}

	/**
	 * 初始化。
	 * 
	 * @return boolean
	 */
	private boolean init() {
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				DateTime dtstart = new DateTime();
				result.setSql(String.format("select a.c_unid,a.c_punid,a.c_level, a.c_expect,a.c_notify,b.c_name from t_wfdata a,t_document b where b.c_unid=a.c_punid and a.c_level=1 and a.c_done='n' and a.c_expect>='%1$s'", dtstart.toString()));
				return result;
			}
		};
		dbr.setResultBuilder(new ResultBuilder() {
			@Override
			public Object build(DBRequest request, Object rawResult) {
				if (rawResult == null || !(rawResult instanceof DataReader)) return map;
				DataReader dr = (DataReader) rawResult;
				WFData d = null;
				ScheduledFuture<Long> sf = null;
				try {
					DateTime now = new DateTime();
					while (dr.next()) {
						d = new WFData();
						d.setUNID(dr.getString(1));
						d.setPUNID(dr.getString(2));
						d.setLevel(dr.getInt(3));
						d.setExpect(dr.getString(4));
						d.setNotify(dr.getString(5));
						if (!StringUtil.isBlank(d.getNotify())) {
							DateTime dt = new DateTime(d.getNotify());
							sf = scheduler.schedule(new Urger(d, dr.getString(6)), dt.getTimeMillis() - now.getTimeMillis(), TimeUnit.MILLISECONDS);
							map.put(d.getUNID(), sf);
						}
					}
				} catch (SQLException ex) {
					FileLogger.error(ex);
				}
				return map;
			}
		});
		dbr.sendRequest();
		return true;
	}

	/**
	 * 返回本对象唯一实例。
	 * 
	 * @return UrgeContext
	 */
	public static UrgeContext getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) m_instance = new UrgeContext();
		}
		return m_instance;
	}

	/**
	 * 追加{@link WFData}对象和文档名称对应的定时发送催办信息的任务。
	 * 
	 * @param data
	 * @param name
	 */
	protected void append(WFData data, String name) {
		if (data != null && !StringUtil.isBlank(name) && !StringUtil.isBlank(data.getNotify())) {
			DateTime now = new DateTime();
			DateTime dt = new DateTime(data.getNotify());
			ScheduledFuture<Long> sf = scheduler.schedule(new Urger(data, name), dt.getTimeMillis() - now.getTimeMillis(), TimeUnit.MILLISECONDS);
			map.put(data.getUNID(), sf);
		}
	}

	/**
	 * 删除已经注册的{@link WFData#getUNID()}的值为id的定时发送催办信息的任务。
	 * 
	 * @param id
	 * @return 返回被删除的内容或null（如果不存在的话）。
	 */
	protected ScheduledFuture<Long> remove(String id) {
		if (id == null) return null;
		ScheduledFuture<Long> sf = map.get(id);
		if (sf != null) {
			sf.cancel(false);
			map.remove(id);
		}
		return sf;
	}

	/**
	 * 关闭催办通知发送线程。
	 */
	public void shutdown() {
		try {
			map.clear();
			scheduler.shutdownNow();
			scheduler.awaitTermination(10, TimeUnit.SECONDS);
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}
}

