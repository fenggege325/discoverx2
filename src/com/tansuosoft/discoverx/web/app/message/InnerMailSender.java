/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.bll.document.DocumentConstructor;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.transaction.DistributeTransaction;

/**
 * 发送内部邮件消息的{@link MessageSender}实现类。
 * 
 * <p>
 * 内部邮件的收件人通过{@link Message#getSecurityCodes()}获取。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class InnerMailSender extends MessageSender {

	/**
	 * 重载send
	 * 
	 * @see com.tansuosoft.discoverx.web.app.message.MessageSender#send(com.tansuosoft.discoverx.web.app.message.Message, com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public boolean send(Message message, Session session) {
		if (message == null) throw new IllegalArgumentException("必须提供有效的通知消息对象！");
		List<Integer> receivers = message.getSecurityCodes();
		if (receivers == null || receivers.isEmpty()) throw new RuntimeException("必须提供有效的接收人信息！");

		WorkflowRuntime wfr = null;
		Document mailDoc = null;
		try {
			ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
			List<Integer> scs = new ArrayList<Integer>();
			ParticipantTree pt = null;
			StringBuilder sbSC = new StringBuilder();
			StringBuilder sbCN = new StringBuilder();
			for (int sc : receivers) {
				pt = ptp.getParticipantTree(sc);
				if (pt == null) continue;
				scs.add(pt.getSecurityCode());
				sbSC.append(sbSC.length() == 0 ? "" : ";").append(pt.getSecurityCode());
				sbCN.append(sbCN.length() == 0 ? "" : ";").append(ParticipantHelper.getFormatValue(pt, "cn"));
			}
			mailDoc = DocumentConstructor.constructDocument(session, "appMail", null, null);
			if (mailDoc == null) throw new Exception("无法构造新的邮件文档！");
			mailDoc.replaceItemValue("fld_subject", message.getSubject());
			mailDoc.replaceItemValue("fld_to", sbCN.toString());
			mailDoc.replaceItemValue("fld_tovalues", sbSC.toString());
			mailDoc.replaceItemValue("fld_body", message.getBody());
			mailDoc.replaceItemValue("fld_sentflag", "1");
			mailDoc.replaceItemValue("fld_maildt", DateTime.getNowDTString());

			if (ResourceHelper.save(mailDoc, session)) {
				wfr = WorkflowRuntime.getInstance(mailDoc, session);
				WorkflowForm wff = new WorkflowForm();
				wff.setParticipants(scs);
				wfr.setWorkflowForm(wff);
				DistributeTransaction t = new DistributeTransaction();
				t.setApplicationFlag(WFDataSubLevel.Application1.getIntValue());
				t.setLogVerb("发送");
				wfr.transact(t, session);
				return true;
			}
		} catch (Exception ex) {
			FileLogger.error(ex);
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		return false;
	}
}

