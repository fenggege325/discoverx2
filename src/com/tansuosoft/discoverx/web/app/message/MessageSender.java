/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.util.List;

import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 实现通知消息发送功能的对象的基类。
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class MessageSender {

	/**
	 * 缺省构造器。
	 */
	public MessageSender() {
	}

	private List<Parameter> m_parameters = null; // 可能存在的额外参数。

	/**
	 * 返回可能存在的额外参数。
	 * 
	 * <p>
	 * 额外参数通常在发送消息的代码中使用。
	 * </p>
	 * 
	 * @return List&lt;Parameter&gt;
	 */
	public List<Parameter> getParameters() {
		return this.m_parameters;
	}

	/**
	 * 设置可能存在的额外参数。
	 * 
	 * @param parameters List&lt;Parameter&gt;
	 */
	protected void setParameters(List<Parameter> parameters) {
		this.m_parameters = parameters;
	}

	/**
	 * 根据参数名获取参数对象。
	 * 
	 * @param paramName
	 * @return Parameter<String, Object>
	 */
	public Parameter getParameter(String paramName) {
		if (paramName == null || paramName.length() == 0 || this.m_parameters == null || this.m_parameters.isEmpty()) return null;
		for (Parameter x : this.m_parameters) {
			if (x != null && paramName.equalsIgnoreCase(x.getName())) return x;
		}
		return null;
	}

	/**
	 * 根据参数名获取对应参数中指定的参数值类型的结果。
	 * 
	 * @param paramName
	 * @return Object
	 */
	public Object getParameterTypeValue(String paramName) {
		Parameter p = this.getParameter(paramName);
		return (p == null ? null : p.getTypeValue());
	}

	/**
	 * 返回参数名对应参数值作为完整类名并调用完整类名对应的类的缺省公共构造器，然后构造好的对象。
	 * 
	 * @param &lt;T&gt; 返回的对象的目标泛型。
	 * @param paramName 参数名，其参数值必须是所定义的完整类名。
	 * @param clazz 指定返回的对象的目标类型。
	 * @param defaultReturn 如果类无效，或者无法构造，或者类型不符，则返回此结果。
	 * @return 返回构造好的对象。
	 */
	public <T> T getParamValueObject(String paramName, Class<T> clazz, T defaultReturn) {
		String clsName = this.getParamValueString(paramName, null);
		if (clsName == null || clsName.length() == 0) return defaultReturn;
		T ret = Instance.newInstance(clsName, clazz);
		if (ret == null) return defaultReturn;
		return ret;
	}

	/**
	 * 返回参数名对应参数值的String类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果参数值为null时返回的默认结果。
	 * @return String
	 */
	public String getParamValueString(String paramName, String defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueString(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的int类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return int
	 */
	public int getParamValueInt(String paramName, int defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueInt(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的long类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return long
	 */
	public long getParamValueLong(String paramName, long defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueLong(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的float类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return float
	 */
	public float getParamValueFloat(String paramName, float defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueFloat(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的double类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return double
	 */
	public double getParamValueDouble(String paramName, double defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueDouble(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的boolean类型结果。
	 * 
	 * <p>
	 * 参数值（不区分大小写）配置为“true”、“yes”、“y”、“1”等情况时返回true，否则返回false。
	 * </p>
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return boolean
	 */
	public boolean getParamValueBool(String paramName, boolean defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueBool(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值转换为enumCls定义的枚举中某一个具体枚举值类型之后的结果。
	 * 
	 * @param &lt;T&gt; 返回的枚举类型。
	 * @param paramName String 参数名，必须。
	 * @param enumCls Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return T 具体枚举结果。
	 */
	public <T> T getParamValueEnum(String paramName, Class<T> enumCls, T defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueEnum(enumCls, defaultReturn);
	}

	/**
	 * 发送消息。
	 * 
	 * <p>
	 * 具体类型的消息发送实现类应实现此方法以提供具体类型消息的发送功能。
	 * </p>
	 * 
	 * @param message 待发送的消息，必须。
	 * @param session 发送消息的用户对应的用户自定义会话
	 * 
	 * @return boolean 发送成功则返回true，否则返回false。
	 */
	public abstract boolean send(Message message, Session session);

	/**
	 * 发送消息。
	 * 
	 * <p>
	 * 此静态方法根据消息类型和消息包含的接收人等信息，通过在{@link MessageConfig}配置中查找消息类型对应的发送对象发送消息。<br/>
	 * 如果传入的消息对象没有指定消息类型，系统会默认为内部邮件({@link MessageType#InnerMail})类型。
	 * </p>
	 * 
	 * @param message 待发送的消息，必须。
	 * @param session 发送消息的用户对应的用户自定义会话
	 * 
	 * @return 发送成功则返回true，否则返回false。
	 */
	public static boolean sendMessage(Message message, Session session) {
		if (message == null) throw new IllegalArgumentException("必须提供有效的通知消息对象！");
		List<String> receivers = message.getReceivers();
		if (receivers == null || receivers.isEmpty()) throw new RuntimeException("必须提供有效的接收人信息！");
		boolean result = true;
		MessageSender ms = null;
		int mt = message.getMessageType();
		if (mt <= 0) mt = MessageType.InnerMail.getIntValue();
		for (MessageType mti : MessageType.values()) {
			if ((mt & mti.getIntValue()) == mti.getIntValue()) {
				ms = MessageConfig.getInstance().getMessageSender(mti);
				if (ms == null) {
					FileLogger.debug("消息配置文件中没有配置类型为“%1$s”的消息发送实现对象！", MessageType.getMessageName(mti));
					continue;
				}
				result = result && ms.send(message, session);
			}
		}

		return result;
	}
}

