/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.util.List;

import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.event.WorkflowEventArgs;
import com.tansuosoft.discoverx.workflow.transaction.DistributeTransaction;
import com.tansuosoft.discoverx.workflow.transaction.ForwardApproverTransaction;
import com.tansuosoft.discoverx.workflow.transaction.NormalApproverTransaction;
import com.tansuosoft.discoverx.workflow.transaction.WorkflowEndTransaction;

/**
 * 发送目标审批人内部邮件通知消息的流程事件实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class NotifyApproverByMail implements EventHandler {
	/**
	 * 缺省构造器。
	 */
	public NotifyApproverByMail() {
	}

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof WorkflowEventArgs)) return;
		WorkflowEventArgs wfe = (WorkflowEventArgs) e;

		WorkflowRuntime wfr = wfe.getWorkflowRuntime();
		if (wfr == null) return;

		WorkflowForm wff = wfr.getWorkflowForm();
		if (wff == null) return;
		int n = wff.getNotification();
		if ((n & 1) != 1) return;

		Transaction t = wfe.getTransaction();
		if (t == null || (t instanceof WorkflowEndTransaction) || (t instanceof NormalApproverTransaction) || (t instanceof ForwardApproverTransaction)) return;

		Document doc = wfr.getDocument();
		if (doc == null) return;

		Session s = wfr.getSession();
		User u = Session.getUser(s);
		try {
			List<Integer> scs = wff.getParticipants();

			if (scs.isEmpty()) return;
			String verb = (t == null ? null : t.getLogVerb());
			if (verb == null || verb.length() == 0) {
				verb = (t instanceof DistributeTransaction ? "传阅" : "发送");
			} else {
				verb = StringUtil.stringLeft(verb, "(");
			}
			String subject = null;
			if (doc.hasItem(Document.SUBJECT_ITEM_NAME)) {
				subject = StringUtil.getValueString(doc.getItemValue(Document.SUBJECT_ITEM_NAME), Document.NO_SUBJECT);
			} else {
				String firstItemName = null;
				Form f = doc.getForm();
				List<Item> items = f.getItems();
				if (items != null && items.size() > 0) firstItemName = items.get(0).getItemName();
				if (firstItemName != null && firstItemName.length() > 0) {
					subject = StringUtil.getValueString(doc.getItemValue(firstItemName), Document.NO_SUBJECT);
				}
			}

			if (subject == null || subject.length() == 0) subject = doc.getName();
			subject = String.format("收到“%1$s”%2$s的来自“%3$s”的文件：%4$s", u.getCN(), verb, doc.getTitle(), subject);
			String body = String.format("<p>收到“%1$s”%2$s的来自“%3$s”的文件：%4$s。</p><p><a href=\"document.jsp?unid=%5$s\" title=\"单击打开此文件\">[打开文件]</a></p>", u.getCN(), verb, doc.getTitle(), subject, doc.getUNID());
			Message msg = new Message();
			msg.setMessageType(MessageType.InnerMail.getIntValue());
			msg.setBody(body);
			msg.setReceivers(scs);
			msg.setSubject(subject);
			MessageSender.sendMessage(msg, s);
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}
}

