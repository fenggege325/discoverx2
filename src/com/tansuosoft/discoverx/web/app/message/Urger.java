/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.workflow.WFData;

/**
 * 实现发送催办通知功能的类。
 * 
 * @author coca@tensosoft.com
 */
public class Urger implements Callable<Long> {
	private WFData m_data = null;
	private String m_docname = null;

	/**
	 * 构造器。
	 * 
	 * @param data
	 * @param docname
	 */
	public Urger(WFData data, String docname) {
		m_data = data;
		m_docname = docname;
	}

	/**
	 * 重载：实现发送内部邮件和短信催办通知功能。
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Long call() throws Exception {
		if (m_data == null || m_docname == null || m_docname.length() == 0) return 0L;
		Long result = 0L;
		try {
			ParticipantTree pt = ParticipantTreeProvider.getInstance().getParticipantTree(m_data.getData());
			if (pt == null) return result;
			DateTime dt1 = null;
			DateTime dt2 = new DateTime(m_data.getNotify());
			String subject = null;
			String body = null;
			String expect = m_data.getExpect();
			if (expect != null && expect.length() > 0) {
				dt1 = new DateTime(expect);
				subject = String.format("审批期限将于%1$s到期的文件催办提醒：%2$s", expect, m_docname);
				body = String.format("<p>需要您审批的文件“%1$s”的完成期限为“%2$s”，请您及时处理。</p><p><a href=\"document.jsp?unid=%3$s\" title=\"单击打开此文件\">[打开文件]</a></p>", m_docname, expect, m_data.getPUNID());
			} else {
				subject = String.format("文件催办提醒：%1$s", m_docname);
				body = String.format("<p>文件“%1$s”需要您及时处理。</p><p><a href=\"document.jsp?unid=%2$s\" title=\"单击打开此文件\">[打开文件]</a></p>", m_docname, m_data.getPUNID());
			}
			if (dt1 != null) {
				result = (dt1.getTimeMillis() - dt2.getTimeMillis());
				if (result >= 1000L * 60L * 60L) {
					WFData appended = new WFData();
					appended.setUNID(m_data.getUNID());
					appended.setPUNID(m_data.getPUNID());
					appended.setLevel(m_data.getLevel());
					appended.setExpect(m_data.getExpect());
					appended.setNotify(new DateTime((dt1.getTimeMillis() + dt2.getTimeMillis()) / 2L).toString());
					UrgeContext.getInstance().append(appended, m_docname);
				}
			}
			// 内部邮件
			List<Integer> mailReceivers = new ArrayList<Integer>(1);
			mailReceivers.add(pt.getSecurityCode());
			Message msg = new Message();
			msg.setMessageType(MessageType.InnerMail.getIntValue());
			msg.setBody(body);
			msg.setReceivers(mailReceivers);
			msg.setSubject(subject);
			MessageSender.sendMessage(msg, Session.getSystemSession());

			// 短信
			User user = ResourceContext.getInstance().getResource(pt.getUNID(), User.class);
			Profile p = Profile.getInstance(user);
			String sms = p.getSms();
			if (sms != null && sms.length() > 0) {
				Map<Integer, String> smsReceivers = new HashMap<Integer, String>(1);
				smsReceivers.put(pt.getSecurityCode(), sms);
				msg = new Message();
				msg.setMessageType(MessageType.Sms.getIntValue());
				msg.setBody(subject);
				msg.setReceivers(smsReceivers);
				MessageSender.sendMessage(msg, Session.getSystemSession());
			}
		} catch (Exception ex) {
			throw ex;
		}
		return result;
	}
}

