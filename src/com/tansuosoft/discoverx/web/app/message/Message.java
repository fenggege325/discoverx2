/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 封装一条通知消息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Message {
	private String subject = null; // 主题
	private String body = null; // 内容
	private Map<Integer, String> receivers = new HashMap<Integer, String>(); // 接收人信息。
	private int messageType = MessageType.InnerMail.getIntValue(); // 消息类型。
	private List<String> m_attachments = null; // 包含所有附件对应的完整文件名的集合。

	/**
	 * 缺省构造器。
	 */
	public Message() {
	}

	/**
	 * 返回消息主题。
	 * 
	 * @return String
	 */
	public String getSubject() {
		return this.subject;
	}

	/**
	 * 设置消息主题。
	 * 
	 * @param subject String
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * 返回消息内容。
	 * 
	 * @return String
	 */
	public String getBody() {
		return this.body;
	}

	/**
	 * 设置消息内容。
	 * 
	 * @param body String
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * 返回接收人信息。
	 * 
	 * <p>
	 * 返回的结果和返回的接收人安全编码（{@link #getSecurityCodes()}）一一对应。
	 * </p>
	 * 
	 * @return List&lt;String&gt;
	 */
	public List<String> getReceivers() {
		if (receivers == null) return null;
		return new ArrayList<String>(this.receivers.values());
	}

	/**
	 * 返回接收人对应的安全编码信息。
	 * 
	 * <p>
	 * 返回的结果和返回的接收人信息（{@link #getReceivers()}）一一对应。
	 * </p>
	 * 
	 * @return List&lt;Integer&gt;
	 */
	public List<Integer> getSecurityCodes() {
		if (receivers == null) return null;
		return new ArrayList<Integer>(receivers.keySet());
	}

	/**
	 * 设置接收人信息。
	 * 
	 * @param receivers 包含的接收人信息，其中key为接收人安全编码，value为接收人实际地址，如外部邮件名、手机号、ip地址等。
	 */
	public void setReceivers(Map<Integer, String> receivers) {
		this.receivers = receivers;
	}

	/**
	 * 设置接收人的安全编码信息。
	 * 
	 * <p>
	 * 通过此方法设置安全编码时，{@link #getReceivers()}和{@link #getSecurityCodes()}返回结果值是一样的（除了一个以字符串的类型返回安全编码值，另一个以整数的类型返回安全编码值）。
	 * </p>
	 * 
	 * @param receivers 包含接收人安全编码的列表。
	 */
	public void setReceivers(List<Integer> receivers) {
		if (receivers == null || receivers.isEmpty()) return;
		String s = null;
		for (int r : receivers) {
			s = r + "";
			if (this.receivers.containsKey(r)) continue;
			this.receivers.put(r, s);
		}
	}

	/**
	 * 返回消息类型。
	 * 
	 * <p>
	 * 消息类型为{@link MessageType}枚举中的某一个或几个枚举值对应数字的组合，默认为{@link MessageType#InnerMail}对应的数字。
	 * </p>
	 * 
	 * @return int
	 */
	public int getMessageType() {
		return this.messageType;
	}

	/**
	 * 设置消息类型。
	 * 
	 * @param messageType int
	 */
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	/**
	 * 返回包含所有附件对应的完整文件名的集合。
	 * 
	 * <p>
	 * 只有发送允许包含附件的外部邮件消息时才有意义。
	 * </p>
	 * 
	 * @return List&lt;String&gt;
	 */
	public List<String> getAttachments() {
		return this.m_attachments;
	}

	/**
	 * 设置包含所有附件对应的完整文件名的集合。
	 * 
	 * <p>
	 * 只有发送允许包含附件的外部邮件消息时才有意义。
	 * </p>
	 * 
	 * @param accessories List&lt;String&gt;
	 */
	public void setAttachments(List<String> attachments) {
		this.m_attachments = attachments;
	}
}

