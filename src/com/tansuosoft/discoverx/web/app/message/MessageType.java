/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 通知消息类型枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum MessageType implements EnumBase {
	/**
	 * 内部邮件(1)。
	 */
	InnerMail(1),
	/**
	 * 外部邮件(2)。
	 */
	OuterMail(2),
	/**
	 * 短信(4)。
	 */
	Sms(4),
	/**
	 * 即时消息。
	 */
	InstantMessage(8),
	/**
	 * 微信消息。
	 */
	WeChat(16);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	MessageType(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return MessageType
	 */
	public MessageType parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (MessageType s : MessageType.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return MessageType.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return MessageType
	 */
	public static MessageType parse(int v) {
		for (MessageType s : MessageType.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

	/**
	 * 根据消息类型枚举值获取消息名称。
	 * 
	 * @param mt
	 * @return String
	 */
	public static String getMessageName(MessageType mt) {
		if (mt == null) return null;
		switch (mt.getIntValue()) {
		case 1:
			return "内部邮件";
		case 2:
			return "外部邮件";
		case 4:
			return "短消息";
		case 8:
			return "即时消息";
		case 16:
			return "微信消息";
		default:
			return null;
		}
	}

}

