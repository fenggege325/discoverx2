/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.util.List;

import com.tansuosoft.discoverx.common.ConfigEntry;
import com.tansuosoft.discoverx.model.Parameter;

/**
 * 表示{@link MessageConfig}中包含的某一类({@link MessageType})消息的相关配置项信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class MessageConfigEntry extends ConfigEntry {

	/**
	 * 缺省构造器。
	 */
	public MessageConfigEntry() {
	}

	private List<Parameter> m_parameters = null; // 可能存在的额外参数。

	/**
	 * 返回可能存在的额外参数。
	 * 
	 * <p>
	 * 额外参数通常在发送消息的代码中使用。
	 * </p>
	 * 
	 * @return List&lt;Parameter&gt;
	 */
	public List<Parameter> getParameters() {
		return this.m_parameters;
	}

	/**
	 * 设置可能存在的额外参数。
	 * 
	 * @param parameters List&lt;Parameter&gt;
	 */
	public void setParameters(List<Parameter> parameters) {
		this.m_parameters = parameters;
	}
}

