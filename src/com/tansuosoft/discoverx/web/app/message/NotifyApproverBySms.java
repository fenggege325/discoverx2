/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.event.WorkflowEventArgs;
import com.tansuosoft.discoverx.workflow.transaction.DistributeTransaction;
import com.tansuosoft.discoverx.workflow.transaction.ForwardApproverTransaction;
import com.tansuosoft.discoverx.workflow.transaction.NormalApproverTransaction;
import com.tansuosoft.discoverx.workflow.transaction.WorkflowEndTransaction;

/**
 * 发送目标审批人短信通知消息的流程事件实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class NotifyApproverBySms implements EventHandler {
	/**
	 * 缺省构造器。
	 */
	public NotifyApproverBySms() {
	}

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof WorkflowEventArgs)) return;
		WorkflowEventArgs wfe = (WorkflowEventArgs) e;

		WorkflowRuntime wfr = wfe.getWorkflowRuntime();
		if (wfr == null) return;

		WorkflowForm wff = wfr.getWorkflowForm();
		if (wff == null) return;
		int n = wff.getNotification();
		if ((n & 4) != 4) return;

		Transaction t = wfe.getTransaction();
		if (t == null || (t instanceof WorkflowEndTransaction) || (t instanceof NormalApproverTransaction) || (t instanceof ForwardApproverTransaction)) return;

		// List<WFData> wfdList = wfr.getContextWFData(isApprover ? WFDataLevel.Approver : WFDataLevel.Reader);
		// if (wfdList == null || wfdList.isEmpty()) return;

		List<Integer> ps = wfr.getWorkflowForm().getParticipants();
		if (ps == null || ps.isEmpty()) return;

		Document doc = wfr.getDocument();
		if (doc == null) return;

		Session s = wfr.getSession();
		User u = Session.getUser(s);
		try {
			Map<Integer, String> scms = new HashMap<Integer, String>();
			final StringBuilder sb = new StringBuilder();
			// for (WFData wfd : wfdList) {
			for (int i : ps) {
				// if (wfd == null) continue;
				if (i <= 0) continue;
				sb.append(sb.length() == 0 ? "" : ",").append(i); // wfd.getData()
			}
			if (sb.length() == 0) return;
			sb.insert(0, "select a.fld_sms,a.fld_smsnotification,b.c_securitycode from vt_frmuserprofile a,t_user b where b.c_profile=a.c_punid and b.c_securitycode in (");
			sb.append(")");
			DBRequest dbr = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper result = new SQLWrapper();
					result.setSql(sb.toString());
					return result;
				}
			};
			dbr.setResultBuilder(new ResultBuilder() {
				@Override
				public Object build(DBRequest request, Object rawResult) {
					DataReader dr = (DataReader) rawResult;
					if (dr == null) return null;
					Map<Integer, String> map = new HashMap<Integer, String>();
					try {
						while (dr.next()) {
							String sms = dr.getString(1);

							if (sms != null && sms.length() > 0) {
								boolean smsn = dr.getBool(2, true);
								int sc = dr.getInt(3);
								if (smsn && sc > 0) map.put(sc, sms);
							}
						}
					} catch (SQLException ex) {
						FileLogger.error(ex);
					}
					return map;
				}
			});
			dbr.sendRequest();
			scms = dbr.getResult(Map.class);
			if (scms.isEmpty()) return;
			String verb = (t == null ? null : t.getLogVerb());
			if (verb == null || verb.length() == 0) {
				verb = (t instanceof DistributeTransaction ? "传阅" : "发送");
			} else {
				verb = StringUtil.stringLeft(verb, "(");
			}
			String subject = null;
			if (doc.hasItem(Document.SUBJECT_ITEM_NAME)) {
				subject = StringUtil.getValueString(doc.getItemValue(Document.SUBJECT_ITEM_NAME), Document.NO_SUBJECT);
			} else {
				String firstItemName = null;
				Form f = doc.getForm();
				List<Item> items = f.getItems();
				if (items != null && items.size() > 0) firstItemName = items.get(0).getItemName();
				if (firstItemName != null && firstItemName.length() > 0) {
					subject = StringUtil.getValueString(doc.getItemValue(firstItemName), Document.NO_SUBJECT);
				}
			}

			if (subject == null || subject.length() == 0) subject = doc.getName();
			String body = String.format("收到“%1$s”%2$s的来自“%3$s”的文件：%4$s", u.getCN(), verb, doc.getTitle(), subject);
			Message msg = new Message();
			msg.setBody(body);
			msg.setMessageType(MessageType.Sms.getIntValue());
			msg.setReceivers(scms);
			MessageSender.sendMessage(msg, s);
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}
}

