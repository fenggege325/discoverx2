/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 表示系统消息通知支持和实现的配置类。
 * 
 * @author coca@tansuosoft.cn
 */
public class MessageConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private MessageConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static MessageConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return MessageConfig
	 */
	public static MessageConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new MessageConfig();
			}
		}
		return m_instance;
	}

	private List<MessageConfigEntry> m_messageConfigEntries = null; // 包含的消息发送实现类信息。

	/**
	 * 返回包含的消息发送实现类信息。
	 * 
	 * @return List&lt;MessageConfigEntry&gt;
	 */
	public List<MessageConfigEntry> getMessageConfigEntries() {
		return this.m_messageConfigEntries;
	}

	/**
	 * 设置包含的消息发送实现类信息。
	 * 
	 * @param messageConfigEntries List&lt;MessageConfigEntry&gt;
	 */
	public void setMessageConfigEntries(List<MessageConfigEntry> messageConfigEntries) {
		this.m_messageConfigEntries = messageConfigEntries;
	}

	/**
	 * 获取系统支持的指定消息类型的消息发送对象。
	 * 
	 * @param mt {@link MessageType}，消息类型。
	 * @return {@link MessageSender}，如果返回null，则表示系统不支持mt指定类型的通知消息的发送。
	 */
	public MessageSender getMessageSender(MessageType mt) {
		if (mt == null) { return null; }
		List<MessageConfigEntry> list = this.getMessageConfigEntries();
		if (list == null || list.isEmpty()) { return null; }
		String impl = null;
		MessageConfigEntry found = null;
		String mtstr = mt.toString();
		for (MessageConfigEntry x : list) {
			if (x != null && mtstr.equals(x.getName())) {
				impl = x.getValue();
				found = x;
				break;
			}
		}
		if (impl != null && impl.length() > 0) {
			MessageSender sender = Instance.newInstance(impl, MessageSender.class);
			if (sender != null && found != null && found.getParameters() != null) {
				List<Parameter> params = new ArrayList<Parameter>(found.getParameters().size());
				for (Parameter p : found.getParameters()) {
					if (p == null) continue;
					try {
						params.add((Parameter) p.clone());
					} catch (CloneNotSupportedException ex) {
						//
					}
				}
				sender.setParameters(params);
			}
			return sender;
		}
		return null;
	}
}

