/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.message;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.tansuosoft.discoverx.bll.user.Email;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 通过SMTP发送外部邮件消息的{@link MessageSender}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OuterMailSender extends MessageSender {
	private static final String _CONTENT_HTML = "text/html; charset=GBK";
	private static final String _PROXY = "Tensosoft Discoverx";

	/**
	 * 重载send
	 * 
	 * @see com.tansuosoft.discoverx.web.app.message.MessageSender#send(com.tansuosoft.discoverx.web.app.message.Message, com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public boolean send(Message message, Session session) {
		try {
			Profile p = Profile.getInstance(session);
			List<Email> emails = p.getEmails();
			if (emails == null || emails.isEmpty()) throw new RuntimeException("您没有配置有效的外部邮件信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");
			Email email = emails.get(0);
			if (email == null) throw new RuntimeException("您没有配置有效的外部邮件信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");
			String from = email.getEmail();
			if (from == null || from.length() == 0) throw new RuntimeException("您没有配置有效的外部邮件地址信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");
			String smtp = email.getSmtp();
			if (smtp == null || smtp.length() == 0) throw new RuntimeException("您没有配置有效的外部邮件发送服务器信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");
			String uid = email.getAccount();
			String pwd = email.getPassword();

			if (message == null) throw new IllegalArgumentException("必须提供有效的通知消息对象！");
			List<String> receivers = message.getReceivers();
			if (receivers == null || receivers.isEmpty()) throw new RuntimeException("必须提供有效的接收人信息！");

			List<String> atts = message.getAttachments();
			boolean debug = false;

			// smtp地址
			Properties props = new Properties();
			props.put("mail.smtp.host", smtp);
			props.put("mail.smtp.auth", "true");

			// 获取smtp会话
			Authenticator auth = new SMTPAuthenticator(uid, pwd);
			javax.mail.Session mailSession = javax.mail.Session.getDefaultInstance(props, auth);
			mailSession.setDebug(debug);

			// 创建MimeMessage
			MimeMessage msg = new MimeMessage(mailSession);

			// 设置发送人和接收人
			InternetAddress addressFrom = new InternetAddress(from);
			msg.setFrom(addressFrom);

			InternetAddress[] addressTo = new InternetAddress[receivers.size()];
			int idx = 0;
			for (String recipient : receivers) {
				addressTo[idx] = new InternetAddress(recipient);
				idx++;
			}
			msg.setRecipients(javax.mail.Message.RecipientType.TO, addressTo);

			// 可选：添加额外邮件头信息
			msg.addHeader("PROXY", _PROXY);

			msg.setSubject(message.getSubject());
			// 内容
			if (atts == null || atts.isEmpty()) {
				msg.setContent(message.getBody(), _CONTENT_HTML);
			} else {
				MimeBodyPart mbpb = new MimeBodyPart();
				mbpb.setText(message.getBody());
				mbpb.setContent(message.getBody(), _CONTENT_HTML);
				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbpb);
				for (String fn : atts) {
					MimeBodyPart mbp = new MimeBodyPart();
					FileDataSource fds = new FileDataSource(fn);
					mbp.setDataHandler(new DataHandler(fds));
					mbp.setFileName(fds.getName());
					mp.addBodyPart(mbp);
				}
				msg.setContent(mp);
			}
			msg.setSentDate(new Date());
			Transport.send(msg);
			return true;
		} catch (Exception ex) {
			if (ex instanceof NullPointerException) {
				FileLogger.error(ex);
			} else if (ex instanceof RuntimeException) {
				throw (RuntimeException) ex;
			} else {
				throw new RuntimeException(ex);
			}
		}
		return false;
	}

	/**
	 * 通过smtp发送邮件时用于提供登录验证信息的{@link javax.mail.Authenticator}实现类。
	 * 
	 * @author coca@tansuosoft.cn
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator {
		/**
		 * 接收用户名和密码的构造器。
		 * 
		 * @param uid
		 * @param pwd。
		 */
		public SMTPAuthenticator(String uid, String pwd) {
			this.uid = uid;
			this.pwd = pwd;
		}

		private String uid = null;
		private String pwd = null;

		/**
		 * 重载getPasswordAuthentication
		 * 
		 * @see javax.mail.Authenticator#getPasswordAuthentication()
		 */
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(uid, pwd);
		}
	}

}

