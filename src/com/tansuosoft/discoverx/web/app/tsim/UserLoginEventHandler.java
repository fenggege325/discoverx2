/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import com.tansuosoft.discoverx.bll.event.LoginEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.servlet.LoginServlet;

/**
 * 用户通过小助手登录时广播通知消息的事件处理程序实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class UserLoginEventHandler implements EventHandler {
	/**
	 * 记录用户登录信息到系统日志中。
	 */
	public void handle(Object sender, EventArgs e) {
		try {
			LoginEventArgs lea = (LoginEventArgs) e;
			if (lea == null) return;
			if (!LoginServlet.checkTSIM(lea.getHttpServletRequest())) return;
			Session s = lea.getSession();
			if (s == null) return;
			User u = s.getUser();
			if (u == null || u.isAnonymous()) return;
			String msg = String.format("%1$s登录了", u.getCN());
			Notifier.broadcastNotification(msg, s, "40012");
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}
}

