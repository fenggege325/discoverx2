/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.app.message.Message;
import com.tansuosoft.discoverx.web.app.message.MessageType;

/**
 * 表示一条要发送的即时消息的类。
 * 
 * <p>
 * 需通过{@link #setReceivers(List)}设置所有接收用户的ip地址。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class IMMessage extends Message {
	private static AtomicInteger ai = new AtomicInteger(1);
	private byte[] bodyContent = null;
	private int m_packetCount = 0;
	private int m_byteContentLen = 0;
	private int m_msgId = 0;
	/**
	 * 发送即时通知消息时的最大包大小。
	 */
	public static final int MAX_PACKET_LENGTH = 1024;
	/**
	 * 发送即时通知消息时的目标端口。
	 */
	public static final int DEFAULT_PORT = 52321;

	/**
	 * 接收消息内容和目标接收用户安全编码与ip地址对应信息的构造器。
	 * 
	 * @param message
	 * @param destIps
	 */
	public IMMessage(String message, Map<Integer, String> destIps) {
		super();
		m_msgId = ai.getAndIncrement();
		setMessageType(MessageType.InstantMessage.getIntValue());
		setBody(message);
		setReceivers(destIps);
		try {
			if (message != null && message.length() > 0) {
				bodyContent = message.getBytes("GB2312");
				m_byteContentLen = bodyContent.length;
			} else {
				bodyContent = "".getBytes("GB2312");
				m_byteContentLen = 0;
			}
		} catch (UnsupportedEncodingException e) {
			FileLogger.error(e);
		}
	}

	/**
	 * 获取消息唯一id。
	 * 
	 * @return int
	 */
	public int getMessageId() {
		return m_msgId;
	}

	/**
	 * 获取发送的消息包的头内容长度。
	 * 
	 * @return int
	 */
	public int getHeaderLength() {
		return 16;
	}

	/**
	 * 获取此消息内容发送时需被拆分为几个消息包。
	 * 
	 * @return int
	 */
	public int getPacketCount() {
		if (m_packetCount > 0) return m_packetCount;
		int headerLen = getHeaderLength();
		int dataLen = MAX_PACKET_LENGTH - headerLen;
		for (int i = 0; i < m_byteContentLen; i += dataLen) {
			m_packetCount++;
		}
		return m_packetCount;
	}

	/**
	 * 返回消息包序号（从0开始到{@link #getPacketCount()}-1)对应的消息包内容。
	 * 
	 * @param packetIndex
	 * @return byte[]
	 */
	public int getPacket(int packetIndex, byte[] result) {
		int pc = getPacketCount();
		if (packetIndex < 0 || packetIndex > pc) return 0;

		byte b[][] = new byte[4][4];

		ByteBuffer buf = ByteBuffer.wrap(b[0]);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.putInt(packetIndex);
		System.arraycopy(b[0], 0, result, 0, 4);

		buf = ByteBuffer.wrap(b[1]);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.putInt(m_packetCount);
		System.arraycopy(b[1], 0, result, 4, 4);

		buf = ByteBuffer.wrap(b[2]);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.putInt(0); // MessageType:0（IM）
		System.arraycopy(b[2], 0, result, 8, 4);

		buf = ByteBuffer.wrap(b[3]);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.putInt(getMessageId());
		System.arraycopy(b[3], 0, result, 12, 4);

		int headerLen = getHeaderLength();
		int dataLen = MAX_PACKET_LENGTH - headerLen;
		int dataLenToCopy = (packetIndex < (m_packetCount - 1) ? dataLen : m_byteContentLen - (packetIndex * dataLen));
		System.arraycopy(bodyContent, (packetIndex * dataLen), result, 16, dataLenToCopy);
		return headerLen + dataLenToCopy;
	}
}

