/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;

/**
 * 表示一条可订阅的小助手服务。
 * 
 * @author coca@tensosoft.com
 */
public class Subscribable implements JsonSerializable {
	/**
	 * 缺省构造器。
	 */
	public Subscribable() {
	}

	private String m_id = null; // 可订阅服务的唯一id。
	private String m_UNID = null; // 可订阅服务的32位唯一id。
	private String m_name = null; // 可订阅服务的名称。
	private String m_jsUrl = null; // 包含呈现可订阅服务内容的js引用路径。
	private String m_jsObjectName = null; // 包含呈现可订阅服务内容的js对象名。
	private boolean m_systemDefault = false; // 是否系统默认订阅。
	private int m_reloadPeriodDefault = 0; // 系统默认可订阅内容刷新周期。

	/**
	 * 返回可订阅服务的唯一id。
	 * 
	 * <p>
	 * 此id返回值将作为客户端js对象和html元素的id使用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getId() {
		return this.m_id;
	}

	/**
	 * 设置可订阅服务的唯一id。
	 * 
	 * <p>
	 * 只能以英文字符开始，且只能包含英文字符、数字和下划线。
	 * </p>
	 * 
	 * @param id String
	 */
	public void setId(String id) {
		this.m_id = id;
	}

	/**
	 * 返回可订阅服务的32位唯一id。
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置可订阅服务的32位唯一id。
	 * 
	 * @param UNID String
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回可订阅服务的名称。
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置可订阅服务的名称。
	 * 
	 * @param name String
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回包含呈现可订阅服务内容的js引用路径。
	 * 
	 * @return String
	 */
	public String getJsUrl() {
		return this.m_jsUrl;
	}

	/**
	 * 设置包含呈现可订阅服务内容的js引用路径。
	 * 
	 * @param jsUrl String
	 */
	public void setJsUrl(String jsUrl) {
		this.m_jsUrl = jsUrl;
	}

	/**
	 * 返回包含呈现可订阅服务内容的js对象名。
	 * 
	 * @return String
	 */
	public String getJsObjectName() {
		return this.m_jsObjectName;
	}

	/**
	 * 设置包含呈现可订阅服务内容的js对象名。
	 * 
	 * @param jsObjectName String
	 */
	public void setJsObjectName(String jsObjectName) {
		this.m_jsObjectName = jsObjectName;
	}

	/**
	 * 返回是否系统默认订阅。
	 * 
	 * <p>
	 * 如果为true，则表示此服务为默认给所有没有进行可订阅内容自定义的用户。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getSystemDefault() {
		return this.m_systemDefault;
	}

	/**
	 * 设置是否系统默认订阅。
	 * 
	 * @param systemDefault boolean
	 */
	public void setSystemDefault(boolean systemDefault) {
		this.m_systemDefault = systemDefault;
	}

	/**
	 * 返回系统默认可订阅内容刷新周期。
	 * 
	 * <p>
	 * 单位为秒，默认为0表示由系统自动计算。
	 * </p>
	 * 
	 * @return int
	 */
	public int getReloadPeriodDefault() {
		return this.m_reloadPeriodDefault;
	}

	/**
	 * 设置系统默认可订阅内容刷新周期。
	 * 
	 * <p>
	 * 单位为秒。
	 * </p>
	 * 
	 * @param reloadPeriodDefault int
	 */
	public void setReloadPeriodDefault(int reloadPeriodDefault) {
		this.m_reloadPeriodDefault = reloadPeriodDefault;
	}

	/**
	 * 输出为Json对象格式的文本。
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	@Override
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("id").append(":").append("'").append(m_id).append("'");
		sb.append(",").append("unid").append(":").append("'").append(m_UNID).append("'");
		String l = StringUtil.encode4Json(m_name);
		sb.append(",").append("label").append(":").append("'").append(l).append("'");
		sb.append(",").append("desc").append(":").append("'").append(l).append("'");
		sb.append(",").append("displayJsUrl").append(":").append("'").append(StringUtil.encode4Json(m_jsUrl)).append("'");
		sb.append(",").append("displayJsObjectName").append(":").append("'").append(m_jsObjectName).append("'");
		sb.append(",").append("systemDefault").append(":").append(m_systemDefault ? "true" : "false");
		sb.append(",").append("reloadPeriodDefault").append(":").append(m_reloadPeriodDefault);
		sb.append("}");
		return sb.toString();
	}
}

