/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.app.message.Message;
import com.tansuosoft.discoverx.web.app.message.MessageSender;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.event.WorkflowEventArgs;
import com.tansuosoft.discoverx.workflow.transaction.CommutatorTransaction;
import com.tansuosoft.discoverx.workflow.transaction.DispatchTransaction;
import com.tansuosoft.discoverx.workflow.transaction.DistributeTransaction;
import com.tansuosoft.discoverx.workflow.transaction.ForwardTransaction;
import com.tansuosoft.discoverx.workflow.transaction.ResendTransaction;
import com.tansuosoft.discoverx.workflow.transaction.TransitionTransaction;

/**
 * 发送目标审批人小助手通知消息的流程事件实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class NotifyApprover implements EventHandler {
	/**
	 * 缺省构造器。
	 */
	public NotifyApprover() {
	}

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof WorkflowEventArgs)) return;
		WorkflowEventArgs wfe = (WorkflowEventArgs) e;
		Transaction t = wfe.getTransaction();
		if (t == null) return;
		boolean isApprover = (t instanceof TransitionTransaction) || (t instanceof ResendTransaction) || (t instanceof ForwardTransaction) || (t instanceof DispatchTransaction) || (t instanceof CommutatorTransaction);
		boolean isReader = (t instanceof DistributeTransaction);
		if (!isApprover && !isReader) return;
		WorkflowRuntime wfr = wfe.getWorkflowRuntime();
		if (wfr == null) return;

		List<WFData> wfdList = wfr.getContextWFData(isApprover ? WFDataLevel.Approver : WFDataLevel.Reader);
		if (wfdList == null || wfdList.isEmpty()) return;

		Document doc = wfr.getDocument();
		if (doc == null) return;

		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree pt = null;

		Session s = wfr.getSession();
		User u = Session.getUser(s);
		Session loginedSession = null;
		try {
			Map<Integer, String> ips = new HashMap<Integer, String>();
			for (WFData wfd : wfdList) {
				if (wfd == null) continue;
				pt = ptp.getParticipantTree(wfd.getData());
				if (pt == null) continue;
				loginedSession = Session.getSession(pt.getUNID());
				if (loginedSession == null) continue;
				String ip = loginedSession.getTsimAddress();
				ips.put(pt.getSecurityCode(), ip);
			}
			if (ips.isEmpty()) return;
			String verb = (t == null ? null : t.getLogVerb());
			if (verb == null || verb.length() == 0) {
				verb = (isReader ? "传阅" : "发送");
			} else {
				verb = StringUtil.stringLeft(verb, "(");
			}
			String subject = null;
			if (doc.hasItem(Document.SUBJECT_ITEM_NAME)) {
				subject = StringUtil.getValueString(doc.getItemValue(Document.SUBJECT_ITEM_NAME), Document.NO_SUBJECT);
			} else {
				String firstItemName = null;
				Form f = doc.getForm();
				List<Item> items = f.getItems();
				if (items != null && items.size() > 0) firstItemName = items.get(0).getItemName();
				if (firstItemName != null && firstItemName.length() > 0) {
					subject = StringUtil.getValueString(doc.getItemValue(firstItemName), Document.NO_SUBJECT);
				}
			}

			if (subject == null || subject.length() == 0) subject = doc.getName();
			String body = String.format("%1$s:%2$s:收到“%3$s”%4$s的来自“%5$s”的文件：%6$s", Notifier.NOTIFIER_PREFIX, doc.getUNID(), u.getCN(), verb, doc.getTitle(), subject.replace("'", "\\'"));
			Message msg = new IMMessage(body, ips);
			MessageSender.sendMessage(msg, s);
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}
}

