/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 保存用户通知消息以便通过会话获取消息的消息容器类。
 * 
 * @author coca@tensosoft.com
 */
public class NotificationContainer implements Serializable {
	/**
	 * 序列化内容。
	 */
	private static final long serialVersionUID = 4809083476301206705L;
	/**
	 * 通过会话获取通知消息的参数名。
	 */
	public static final String NOTIFICATION_FETCH_PARAM_NAME = "$$notification";

	/**
	 * 缺省构造器。
	 */
	protected NotificationContainer() {
	}

	private List<String> m_list = Collections.synchronizedList(new ArrayList<String>());

	/**
	 * 追加msgBody对应的通知消息内容。
	 * 
	 * @param msgBody
	 */
	protected void append(String msgBody) {
		synchronized (m_list) {
			m_list.add(msgBody);
		}
	}

	/**
	 * 追加msgBody对应的通知消息内容给所有登录的会话。
	 * 
	 * @param msgBody
	 */
	protected static void appendBroadcast(String msgBody) {
		for (Session s : Session.getAllSession().values()) {
			if (s == null) continue;
			append(msgBody, s);
		}
	}

	/**
	 * 将msgBody指定的通知消息内容追加到s指定的的用户自定义会话中。
	 * 
	 * @param msgBody
	 * @param s
	 */
	protected static void append(String msgBody, Session s) {
		if (msgBody == null || msgBody.length() == 0 || s == null) return;
		NotificationContainer nc = null;
		Object obj = s.getParameter(NotificationContainer.NOTIFICATION_FETCH_PARAM_NAME);
		if (obj != null && obj instanceof NotificationContainer) nc = (NotificationContainer) obj;
		if (nc == null) nc = new NotificationContainer();
		nc.append(msgBody);
		s.setParameter(NotificationContainer.NOTIFICATION_FETCH_PARAM_NAME, nc);
	}

	/**
	 * 获取当前所有可用通知消息内容对应的字符串数组。
	 * 
	 * @return String[] 每条消息的格式为：{32位唯一ID}:{消息内容}
	 */
	protected String[] getNotification() {
		synchronized (m_list) {
			String[] strs = new String[m_list.size()];
			m_list.toArray(strs);
			m_list.clear();
			return strs;
		}
	}

	/**
	 * 自定义序列化写操作。
	 * 
	 * @param out
	 * @throws IOException
	 */
	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeObject(this.m_list);
	}

	/**
	 * 自定义序列化读操作。
	 * 
	 * @param in
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		m_list = (List<String>) in.readObject();
	}

	/**
	 * 重载：以json数组格式输出当前所有可用通知消息内容。
	 * 
	 * <p>
	 * 输出格式如下：<br/>
	 * [{id:'32位唯一ID1',nt:通知类型数字1,body:'消息内容1'},{id:'32位唯一ID2',nt:通知类型数字2,body:'消息内容2'}...]<br/>
	 * 如果没有消息，则输出空数组“[]”。
	 * </p>
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String[] strs = this.getNotification();
		if (strs == null || strs.length == 0) return "[]";
		StringBuilder sb = new StringBuilder();
		final char c = ':';
		for (String s : strs) {
			if (s == null || s.length() < 34) continue;
			sb.append(sb.length() > 0 ? "," : "").append("\r\n{");
			sb.append("id:'").append(s.substring(0, 32)).append("'");
			int pos = s.indexOf(c, 33);
			if (pos == 34) {
				sb.append(",nt:").append(s.substring(33, 34));
				sb.append(",body:'").append(StringUtil.encode4Json(s.substring(35))).append("'");
			} else {
				sb.append(",nt:0");
				sb.append(",body:'").append(StringUtil.encode4Json(s.substring(33))).append("'");
			}
			sb.append("}");
		}
		if (sb.length() > 0) {
			sb.insert(0, "[");
			sb.append("]");
		}
		return sb.toString();
	}
}

