/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import java.io.File;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.web.app.message.Message;
import com.tansuosoft.discoverx.web.app.message.MessageSender;

/**
 * 发送客户端用户小助手即时通知消息的类。
 * 
 * @author coca@tensosoft.com
 */
public class Notifier extends MessageSender {
	/**
	 * 即时通知消息内容前缀
	 */
	public static final String NOTIFIER_PREFIX = "5CA9C157652D4A42BA051DEE6B2E6928";

	/**
	 * 局域网广播地址。
	 */
	public static final String BORADCASE_ADDRESS = "255.255.255.255";

	/**
	 * 是否启用可小助手标记。
	 */
	public static final boolean TSIM_INSTALLED = new File(CommonConfig.getInstance().getInstallationPath() + "tsim" + File.separator + "index.jsp").exists();

	/**
	 * 系统本身作为用户时对应的安全编码。
	 */
	protected static final int SYSTEM_USER_SC = Session.getSystemSession().getUser().getSecurityCode();

	/**
	 * 广播msg指定的通知内容。
	 * 
	 * @param msg
	 * @param s
	 * @param nt 通知类型。
	 * @return boolean，表示是否发送成功。
	 */
	public static boolean broadcastNotification(String msg, Session s, NotificationType nt) {
		// if (!TSIM_INSTALLED) return false;
		if (msg == null || msg.length() == 0) return false;
		Map<Integer, String> receivers = new HashMap<Integer, String>(1);
		receivers.put(SYSTEM_USER_SC, Notifier.BORADCASE_ADDRESS);
		String unidOf32 = String.format("%1$032d", System.currentTimeMillis());
		String fullMsg = String.format("%1$s:%2$s:%3$d:%4$s", NOTIFIER_PREFIX, unidOf32, nt.getIntValue(), msg.replace("'", "\\'"));
		IMMessage immsg = new IMMessage(fullMsg, receivers);
		return MessageSender.sendMessage(immsg, s);
	}

	/**
	 * 广播msg和presence指定的用户在线状态通知。
	 * 
	 * @param msg
	 * @param s
	 * @param presence
	 * @return boolean，表示是否发送成功。
	 */
	public static boolean broadcastNotification(String msg, Session s, String presence) {
		if (!TSIM_INSTALLED) return false;
		if (presence == null || presence.length() == 0) return broadcastNotification(msg, s, NotificationType.USERSTATE);
		if (msg == null || msg.length() == 0) return false;
		Map<Integer, String> receivers = new HashMap<Integer, String>(1);
		receivers.put(SYSTEM_USER_SC, Notifier.BORADCASE_ADDRESS);
		User u = Session.getUser(s);
		String ipr = s.getTsimAddress();
		if (ipr == null || ipr.length() == 0) {
			String ips[] = StringUtil.splitString(s.getLoginAddress(), Session.ADDRESS_AND_CLIENT_SEP);
			if (ips != null && ips.length > 0) {
				if (ips.length > 1) {
					for (String ip : ips) {
						if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("127.0.0.1")) continue;
						ipr = ip;
						break;
					}
				} else {
					ipr = ips[0];
				}
			}
		}
		String unidOf32 = String.format("%1$s_%2$s_%3$d", presence.substring(0, 5), ipr, (u == null ? User.ANONYMOUS_USER_SC : u.getSecurityCode()));
		String dt = System.currentTimeMillis() + "";
		char[] cs = dt.toCharArray();
		int csLen = cs.length;
		StringBuilder sb = new StringBuilder(unidOf32);
		int dataLen = unidOf32.length();
		for (int i = dataLen; i < 32; i++) {
			if (i == dataLen) {
				sb.append("_");
			} else if (csLen > 0) {
				sb.append(cs[--csLen]);
			} else {
				sb.append("0");
			}
		}
		String fullMsg = String.format("%1$s:%2$s:%3$d:%4$s", NOTIFIER_PREFIX, sb.toString(), NotificationType.USERSTATE.getIntValue(), msg.replace("'", "\\'"));
		IMMessage immsg = new IMMessage(fullMsg, receivers);
		return MessageSender.sendMessage(immsg, s);
	}

	/**
	 * 重载：发送即时通知消息给{@link Message#getReceivers()}中包含的ip地址。
	 * 
	 * @see com.tansuosoft.discoverx.web.app.message.MessageSender#send(com.tansuosoft.discoverx.web.app.message.Message, com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public boolean send(Message message, Session session) {
		if (message == null || !(message instanceof IMMessage)) return false;

		IMMessage im = (IMMessage) message;
		List<String> receivers = im.getReceivers();
		if (receivers == null || receivers.isEmpty()) return false;

		String body = message.getBody();
		if (body.length() <= 33) return false;
		// 发给所有人的消息暂时通过Session传递
		if (Notifier.BORADCASE_ADDRESS.equals(receivers.get(0))) {
			NotificationContainer.appendBroadcast(body.substring(33));
			return true;
		}

		List<Session> sessions = new ArrayList<Session>(receivers.size());
		Map<Integer, Boolean> checkNotifyBySession = new HashMap<Integer, Boolean>(receivers.size());
		List<Integer> scs = im.getSecurityCodes();
		if (scs != null) {
			Session s = null;
			ParticipantTree pt = null;
			ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
			for (int sc : scs) {
				pt = ptp.getParticipantTree(sc);
				if (pt == null) continue;
				s = Session.getSession(pt.getUNID());
				if (s == null) continue;
				sessions.add(s);
			}
		}
		// 如果服务器端没有安装小助手。
		if (!TSIM_INSTALLED) {
			for (Session s : sessions) {
				NotificationContainer.append(body.substring(33), s);
			}
			return true;
		}
		// 如果通知不能直接发送则通过用户自定义会话发送通知消息
		for (Session s : sessions) {
			if (s.getTsimLogin()) continue; // 如小助手已登录则忽略
			String add = s.getTsimAddress();
			String loginAdd = s.getLoginAddress();
			boolean notifyBySession = (add == null || add.length() == 0); // 小助手使用的网络地址不存在
			if (!notifyBySession) notifyBySession = (loginAdd == null || loginAdd.indexOf(add) < 0); // 或小助手使用的网络地址与登录地址不在同一个网段（如通过网关或代理服务器访问应用服务器）
			if (notifyBySession) {
				NotificationContainer.append(body.substring(33), s);
				checkNotifyBySession.put(Session.getUser(s).getSecurityCode(), true);
			}
		}

		// 正常发送
		DatagramSocket clientSocket = null;
		DatagramPacket sendPacket = null;
		InetAddress ipv4Address = null;
		byte[] sendData = new byte[IMMessage.MAX_PACKET_LENGTH];
		int sendDataLen = 0;
		try {
			clientSocket = new DatagramSocket();
			for (int r = 0; r < receivers.size(); r++) {
				String ip = receivers.get(r);
				int sc = (scs.size() > r ? scs.get(r) : 0);
				if (checkNotifyBySession.containsKey(sc)) continue;
				for (int i = 0; i < im.getPacketCount(); i++) {
					ipv4Address = InetAddress.getByName(ip);
					sendDataLen = im.getPacket(i, sendData);
					if (sendDataLen <= 0) continue;
					sendPacket = new DatagramPacket(sendData, sendDataLen, ipv4Address, IMMessage.DEFAULT_PORT);
					clientSocket.send(sendPacket);
				}
			}
			clientSocket.close();
			return true;
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		return false;
	}

	/**
	 * 测试代码。
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		byte[] sendData = new byte[1024];

		byte b[][] = new byte[4][4];

		ByteBuffer buf = ByteBuffer.wrap(b[0]);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.putInt(0);
		System.arraycopy(b[0], 0, sendData, 0, 4);

		buf = ByteBuffer.wrap(b[1]);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.putInt(1);
		System.arraycopy(b[1], 0, sendData, 4, 4);

		buf = ByteBuffer.wrap(b[2]);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.putInt(0);
		System.arraycopy(b[2], 0, sendData, 8, 4);

		buf = ByteBuffer.wrap(b[3]);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		AtomicInteger ai = new AtomicInteger();
		buf.putInt(ai.incrementAndGet());
		System.arraycopy(b[3], 0, sendData, 12, 4);

		byte[] content = "测试内容\"\"\\'11\\'".getBytes("GB2312");
		System.arraycopy(content, 0, sendData, 16, content.length);

		// System.out.println("sendData=" + StringUtil.toHexString(sendData));

		DatagramSocket clientSocket = new DatagramSocket();
		InetAddress IPAddress = InetAddress.getByName("255.255.255.255");
		DatagramPacket sendPacket = new DatagramPacket(sendData, 16 + content.length, IPAddress, 52321);
		// clientSocket.send(sendPacket);
		// broadcastNotification("测试内容！", null, NotificationType.SYSTEMBROADCASE);

		StringBuilder sb = new StringBuilder();
		sb.append(Notifier.NOTIFIER_PREFIX).append(":").append("293C99566908463D85C66D6EB9351070").append(":").append("收到“张桂平”发送的来自“发文管理”的文件：关于中秋节放假的通知！");
		// sb.append(Notifier.NOTIFIER_PREFIX).append(":").append(UNIDProvider.getUNID()).append(":2:").append("这是一条来自系统的测试广播消息！");

		IMMessage im = new IMMessage(sb.toString(), null);
		for (int i = 0; i < im.getPacketCount(); i++) {
			int sendDataLen = im.getPacket(i, sendData);
			// System.out.println("sendData=" + StringUtil.toHexString(sendData));
			sendPacket = new DatagramPacket(sendData, sendDataLen, IPAddress, 52321);
			clientSocket.send(sendPacket);
		}

		clientSocket.close();
		System.out.println("Done!");
	}
}

