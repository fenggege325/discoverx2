/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 包含供小助手使用的可订阅服务的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class SubscribableConfig extends Config implements JsonSerializable {
	/**
	 * 缺省构造器。
	 */
	private SubscribableConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static SubscribableConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return SubscribableConfig
	 */
	public static SubscribableConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new SubscribableConfig();
			}
		}
		return m_instance;
	}

	private List<Subscribable> m_services = null; // 系统定义的有效的可订阅服务对象列表。

	/**
	 * 返回系统定义的有效的可订阅服务对象列表。
	 * 
	 * @return List&lt;Subscribable&gt;
	 */
	public List<Subscribable> getServices() {
		return this.m_services;
	}

	/**
	 * 设置系统定义的有效的可订阅服务对象列表。
	 * 
	 * @param services List&lt;Subscribable&gt;
	 */
	public void setServices(List<Subscribable> services) {
		this.m_services = services;
	}

	/**
	 * 根据unid获取对应的{@link Subscribable}对象。
	 * 
	 * @param unid
	 * @return 返回匹配的{@link Subscribable}或null.
	 */
	public Subscribable getSubscribable(String unid) {
		if (m_services == null || m_services.isEmpty() || unid == null || unid.length() == 0) return null;
		for (Subscribable x : m_services) {
			if (x != null && unid.equalsIgnoreCase(x.getUNID())) return x;
		}
		return null;
	}

	/**
	 * 输出包含的{@link Subscribable}json对象数组对应的文本。
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	@Override
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if (m_services != null) {
			int idx = 0;
			for (Subscribable x : m_services) {
				if (x == null) continue;
				sb.append(idx == 0 ? "\r\n" : ",\r\n");
				sb.append(x.toJson());
				idx++;
			}
		}
		sb.append("\r\n]");
		return sb.toString();
	}
}

