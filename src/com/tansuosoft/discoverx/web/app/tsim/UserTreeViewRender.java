/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.NetUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.JSPContext;
import com.tansuosoft.discoverx.web.PathContext;
import com.tansuosoft.discoverx.web.ui.HtmlRender;

/**
 * 输出供办公助手使用的用户资源TreeView对应的JSON对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class UserTreeViewRender implements HtmlRender {
	/**
	 * 采用异步模式获取用户参与者的最少参与者数。
	 */
	protected int ASYNC_PARTICIPANT_COUNT_MIN = 1001;
	private int m_participantCount = 0;
	private boolean m_firstFlag = true;
	private int m_waitForFetchDataIndex = 0;
	protected static final String AsyncLabel = "请稍候，正在获取用户...";
	private int m_rootSC = 0;
	private boolean m_asyncFlag = false; // 通过异步方式获取下级标记。
	private String m_tsimRoot = null;
	private String m_nonPersonTviLeafIcon = null;

	/**
	 * 接收根节点安全编码的泪构造器。
	 * 
	 * @param rootsc 安全编码，如果为0，则表示根组织。
	 */
	public UserTreeViewRender(int rootsc) {
		this.m_rootSC = rootsc;
	}

	/**
	 * 重载render
	 * 
	 * @see com.tansuosoft.discoverx.web.ui.HtmlRender#render(com.tansuosoft.discoverx.web.JSPContext)
	 */
	@Override
	public String render(JSPContext jspContext) {
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		m_participantCount = ptp.getParticipantCount();
		ParticipantTree root = null;
		if (this.m_rootSC <= 0) {
			root = ptp.getRoot();
			this.m_rootSC = root.getSecurityCode();
		} else {
			root = ptp.getParticipantTree(m_rootSC);
		}
		if (root == null) return "error:true,message:'无法获取跟节点！'";
		m_asyncFlag = (this.m_participantCount >= ASYNC_PARTICIPANT_COUNT_MIN);
		PathContext pathContext = jspContext.getPathContext();
		m_tsimRoot = pathContext.getWebAppRoot() + "tsim/";
		m_nonPersonTviLeafIcon = pathContext.getThemeImagesPath() + "tvfc.gif";
		StringBuilder sb = new StringBuilder();
		this.renderTreeView(root, sb);
		return sb.toString();
	}

	/**
	 * 输出小助手使用的登录用户信息树。
	 * 
	 * @param pt
	 * @param sb
	 */
	private boolean renderTreeView(ParticipantTree pt, StringBuilder sb) {
		if (pt == null) return false;
		if (pt.getParticipantType() == ParticipantType.Person && m_asyncFlag) { return false; }
		int sc = pt.getSecurityCode();
		int sblen = sb.length();
		sb.append(m_firstFlag ? "" : ",");
		boolean rootFlag = (sb.length() == 0);
		if (!rootFlag) sb.append("{"); // treeview begin
		sb.append("id:").append("'").append(sc).append("'");
		sb.append(",").append("label:").append("'").append(StringUtil.encode4Json(ParticipantHelper.getFormatValue(pt, "cn"))).append("'");
		sb.append(",").append("data:{"); // data begin
		sb.append("participantType:").append(pt.getParticipantType().getIntValue());
		sb.append(",").append("sc:").append(sc);
		boolean isPerson = (pt.getParticipantType() == ParticipantType.Person);
		int presence = 0;
		if (isPerson) {
			Session s = Session.getSession(pt.getUNID());
			User user = Session.getUser(s);
			if (!user.isAnonymous() && s.getTsimLogin()) {
				String addr = s.getTsimAddress();
				boolean tsimLogin = s.getTsimLogin();
				if (tsimLogin && addr != null && addr.length() > 0) {
					presence = s.getTsimPresence();
					if (addr.indexOf('-') >= 0) presence = 0;
				}
				sb.append(",").append("presence:'").append(presence).append("'");
				sb.append(",").append("ip:'").append(addr).append("'");
			}
		}
		sb.append("}"); // data end

		List<ParticipantTree> list = pt.getChildren();
		if (pt.getSecurityCode() != this.m_rootSC && !isPerson && (list == null || list.isEmpty())) {
			sb.delete(sblen, sb.length());
			return false;
		}
		if (list != null && list.size() > 0) {
			sb.append(",").append("children:").append("["); // children begin
			boolean asyncRenderFlag = false;
			m_firstFlag = true;
			for (ParticipantTree x : list) {
				if (x == null || x.getSecurityCode() == ParticipantTree.GROUP_ROOT_CODE || x.getSecurityCode() == ParticipantTree.ROLE_ROOT_CODE) continue;
				if (!asyncRenderFlag && m_asyncFlag) {
					sb.append("{");
					sb.append("id:").append("'waitforfetchdata_").append(m_waitForFetchDataIndex++).append("'");
					sb.append(",").append("label:").append("'").append(AsyncLabel).append("'");
					sb.append(",").append("desc:").append("''");
					sb.append(",").append("data:{"); // data begin
					sb.append("participantType:-1");
					sb.append(",").append("ajaxPlaceholder:true");
					sb.append("}"); // data end
					sb.append("}");
					asyncRenderFlag = true;
					m_firstFlag = false;
					break;
				}
				if (asyncRenderFlag && x.getParticipantType() == ParticipantType.Person) continue;
				this.renderTreeView(x, sb);
				m_firstFlag = false;
			}// for end
			sb.append("]"); // children end
			if (asyncRenderFlag) sb.append(",").append("ajaxChildren:true");
		}
		if (isPerson) {
			sb.append(",").append("leafIcon:'").append(m_tsimRoot).append("theme/images/");
			switch (presence) {
			case 40013:
			case 40016:
			case 40018:
				sb.append("oluserout.gif");
				break;
			case 40015:
			case 40017:
				sb.append("oluserbusy.gif");
				break;
			case 0:
			case 40011:
				sb.append("offluser.gif");
				break;
			case 40012:
			case 40014:
			default:
				sb.append("oluser.gif");
				break;
			}

			sb.append("'");
		} else {
			sb.append(",").append("leafIcon:'").append(m_nonPersonTviLeafIcon).append("'");
		}

		if (!rootFlag) sb.append("}"); // treeview end
		return true;
	}

	/**
	 * 获取服务器所有有效的ipv4格式的地址。
	 * 
	 * @return String，多个用半角分号分隔。
	 */
	public static String getServerIPv4Address() {
		return StringUtil.getValueString(NetUtil.getHostIPv4Address(), "");
	}

	public static void main(String args[]) throws SocketException {
		Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
		for (NetworkInterface netint : Collections.list(nets)) {
			displayInterfaceInformation(netint);
		}
		System.out.println("Server IPv4:");
		getServerIPv4Address();
	}

	static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {
		System.out.printf("Display name: %s\n", netint.getDisplayName());
		System.out.printf("Name: %s\n", netint.getName());
		List<InterfaceAddress> iads = netint.getInterfaceAddresses();
		for (InterfaceAddress iad : iads) {
			System.out.printf("Netmask: %s\n", NetUtil.getIPv4SubnetMask(iad.getNetworkPrefixLength()));
		}
		Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
		for (InetAddress inetAddress : Collections.list(inetAddresses)) {
			System.out.printf("InetAddress: %s\n", inetAddress);
		}
		System.out.printf("\n");
	}
}

