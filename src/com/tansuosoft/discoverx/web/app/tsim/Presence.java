/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import java.util.HashMap;
import java.util.Map;

/**
 * 表示小助手在线状态相关信息的类。
 * 
 * <p>
 * 小助手常见在线状态值和名称对应信息如下：
 * <ul>
 * <li>40011:脱机</li>
 * <li>40012:联机</li>
 * <li>40013:离开</li>
 * <li>40014:空闲</li>
 * <li>40015:忙碌</li>
 * <li>40016:马上回来</li>
 * <li>40017:电话中</li>
 * <li>40018:外出就餐</li>
 * </ul>
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class Presence {
	/**
	 * 缺省私有构造器。
	 */
	private Presence() {
	}

	private static final Map<String, String> map = new HashMap<String, String>();
	private static final Map<String, String> mapAction = new HashMap<String, String>();
	static {
		map.put("40011", "脱机");
		map.put("40012", "联机");
		map.put("40013", "离开");
		map.put("40014", "空闲");
		map.put("40015", "忙碌");
		map.put("40016", "马上回来");
		map.put("40017", "电话中");
		map.put("40018", "外出就餐");

		mapAction.put("40011", "下线了");
		mapAction.put("40012", "上线了");
		mapAction.put("40013", "现在不在电脑边");
		mapAction.put("40014", "现在处于空闲状态");
		mapAction.put("40015", "现在处于忙碌状态");
		mapAction.put("40016", "要离开一下子");
		mapAction.put("40017", "正在打电话");
		mapAction.put("40018", "外出就餐了");
	};

	/**
	 * 根据在线状态值获取对应好理解的名称。
	 * 
	 * @param presence
	 * @return String
	 */
	public static String getName(String presence) {
		String v = map.get(presence);
		return (v == null ? presence : v);
	}

	/**
	 * 根据在线状态值获取对应好理解的名称。
	 * 
	 * @param presence
	 * @return String
	 */
	public static String getName(int presence) {
		return getName("" + presence);
	}

	/**
	 * 根据在线状态值获取对应好理解的动作名称。
	 * 
	 * @param presence
	 * @return String
	 */
	public static String getAction(String presence) {
		String v = mapAction.get(presence);
		return (v == null ? null : v);
	}

	/**
	 * 根据在线状态值获取对应好理解的动作名称。
	 * 
	 * @param presence
	 * @return String
	 */
	public static String getAction(int presence) {
		return getAction("" + presence);
	}
}

