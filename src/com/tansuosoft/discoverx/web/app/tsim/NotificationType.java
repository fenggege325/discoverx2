/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.tsim;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 表示即时通知消息类型的枚举。
 * 
 * @author coca@tensosoft.com
 */
public enum NotificationType implements EnumBase {
	/**
	 * 文档链接
	 */
	DOCUMENTLINK(0),
	/**
	 * 用户状态
	 */
	USERSTATE(1),
	/**
	 * 系统广播
	 */
	SYSTEMBROADCASE(2);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	NotificationType(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @see com.tansuosoft.discoverx.util.EnumBase#parse(java.lang.String)
	 */
	@Override
	public NotificationType parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			int vi = Integer.parseInt(v);
			for (NotificationType s : NotificationType.values()) {
				if (s.getIntValue() == vi) return s;
			}
		} else {
			return NotificationType.valueOf(v);
		}
		return null;
	}
}

