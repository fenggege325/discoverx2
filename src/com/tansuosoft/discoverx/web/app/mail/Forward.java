/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.mail;

import com.tansuosoft.discoverx.bll.document.DocumentConstructor;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Session;

/**
 * 邮件转发操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td colspan="2">N/A</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link Operation#returnRedirectToResource(com.tansuosoft.discoverx.model.Resource)}对应的结果以打开新建的转发邮件文档。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Forward extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Forward() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = null;
		Document forwardDoc = null;
		try {
			Session session = this.getSession();

			String unid = this.getParameterStringFromAll("unid", null);
			doc = this.getDocument();
			if (doc == null || !doc.getUNID().equalsIgnoreCase(unid)) doc = this.openDocument(unid);
			if (doc == null) throw new Exception("无法获取目标邮件文档！");
			forwardDoc = DocumentConstructor.constructDocument(session, "appMail", doc.getUNID(), null);
			if (forwardDoc == null) throw new Exception("无法构造新的邮件文档！");
			forwardDoc.replaceItemValue("fld_subject", "转发：" + doc.getItemValue("fld_subject"));
			forwardDoc.replaceItemValue("fld_importance", doc.getItemValue("fld_importance"));
			forwardDoc.replaceItemValue("fld_urgency", doc.getItemValue("fld_urgency"));
			forwardDoc.replaceItemValue(Send.FORWARD_FLAG_ITEMNAME, "1");
			forwardDoc.replaceItemValue("fld_to", "");
			forwardDoc.replaceItemValue("fld_tovalues", "");
			forwardDoc.replaceItemValue("fld_cc", "");
			forwardDoc.replaceItemValue("fld_ccvalues", "");
			forwardDoc.replaceItemValue("fld_sentflag", "0");
			forwardDoc.replaceItemValue("fld_maildt", "");
			StringBuilder sb = new StringBuilder();
			sb.append("<p></p>\r\n");
			sb.append("<p></p>\r\n");
			sb.append("<hr/>\r\n");
			sb.append(doc.getItemValue("fld_body"));
			forwardDoc.replaceItemValue("fld_body", sb.toString());
			session.setLastDocument(forwardDoc);
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		}
		return this.returnRedirectToResource(forwardDoc);
	}
}

