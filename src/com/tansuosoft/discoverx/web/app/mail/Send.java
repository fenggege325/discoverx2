/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.mail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.impl.DocumentFieldUpdater;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.web.accessory.AccessoryContainerHelper;
import com.tansuosoft.discoverx.web.app.message.Message;
import com.tansuosoft.discoverx.web.app.message.MessageSender;
import com.tansuosoft.discoverx.web.app.message.MessageType;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.transaction.DistributeTransaction;

/**
 * 邮件发送操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td colspan="2">N/A</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link Operation#returnRequestUrl(com.tansuosoft.discoverx.bll.function.OperationParser)}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Send extends Operation {
	/**
	 * 用于记录转发标记信息的字段名
	 */
	protected static final String FORWARD_FLAG_ITEMNAME = "fld__forward";
	/**
	 * 用于记录答复标记信息的字段名
	 */
	protected static final String REPLY_FLAG_ITEMNAME = "fld__reply";

	/**
	 * 缺省构造器。
	 */
	public Send() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = null;
		WorkflowRuntime wfr = null;
		try {
			Session session = this.getSession();
			String unid = this.getParameterStringFromAll("unid", null);

			doc = this.getDocument();
			if (doc == null || !doc.getUNID().equalsIgnoreCase(unid)) doc = this.openDocument(unid);
			if (doc == null) throw new Exception("无法获取目标邮件文档！");

			// 将接收人、抄送人设置为待阅者。
			List<Integer> tos = WorkflowForm.getSecurityCodes(this.getHttpRequest(), "fld_tovalues");
			List<Integer> ccs = WorkflowForm.getSecurityCodes(this.getHttpRequest(), "fld_ccvalues");
			Map<Integer, Object> map = new HashMap<Integer, Object>();
			if (tos != null) {
				for (int x : tos) {
					map.put(x, null);
				}
			}
			if (ccs != null) {
				for (int x : ccs) {
					map.put(x, null);
				}
			}
			List<Integer> all = new ArrayList<Integer>(map.keySet());
			map.clear();
			String outerto = this.getParameterStringFromAll("fld_outerto", null);
			if (StringUtil.isBlank(outerto) && all.isEmpty()) throw new Exception("没有指定有效收件人信息！");
			List<Accessory> accs = doc.getAccessories();
			// 记录已发送标记
			DocumentFieldUpdater updater1 = new DocumentFieldUpdater();
			updater1.setParameter(DBRequest.ENTITY_PARAM_NAME, doc);
			updater1.setParameter(DocumentFieldUpdater.FIELD_NAME_PARAM_NAME, "fld_sentflag");
			updater1.setParameter(DocumentFieldUpdater.FIELD_VALUE_PARAM_NAME, "1");
			// 记录发送日期
			DocumentFieldUpdater updater2 = new DocumentFieldUpdater();
			updater2.setParameter(DBRequest.ENTITY_PARAM_NAME, doc);
			updater2.setParameter(DocumentFieldUpdater.FIELD_NAME_PARAM_NAME, "fld_maildt");
			updater2.setParameter(DocumentFieldUpdater.FIELD_VALUE_PARAM_NAME, DateTime.getNowDTString());
			updater1.setNextRequest(updater2);
			if (!all.isEmpty()) {
				// 获取答复或转发邮件的原始邮件文档
				String punid = doc.getPUNID();
				Document parent = null;
				if (punid != null && punid.length() > 0) {
					parent = DocumentBuffer.getInstance().getDocument(punid);
					if (parent == null) parent = ResourceContext.getInstance().getResource(punid, Document.class);
				}
				// 转发邮件前要先拷贝原始邮件文档的附加文件到目标转发文档。
				if ("1".equalsIgnoreCase(doc.getItemValue(FORWARD_FLAG_ITEMNAME)) && parent != null) {
					AccessoryContainerHelper ach = new AccessoryContainerHelper(parent, session);
					ach.copyAccessoriesTo(doc);
				}

				// 如果附件个数有变动，记录新的附件个数
				String accCntItemName = "fld_accessorycount";
				if (StringUtil.getValueInt(doc.getItemValue(accCntItemName), 0) != (accs == null ? 0 : accs.size())) {
					DocumentFieldUpdater updater3 = new DocumentFieldUpdater();
					updater3.setParameter(DBRequest.ENTITY_PARAM_NAME, doc);
					updater3.setParameter(DocumentFieldUpdater.FIELD_NAME_PARAM_NAME, accCntItemName);
					updater3.setParameter(DocumentFieldUpdater.FIELD_VALUE_PARAM_NAME, "" + accs.size());
					updater1.setNextRequest(updater3);
				}
				// 如果是转发邮件，记录原始文档的已转发标记
				if ("1".equalsIgnoreCase(doc.getItemValue(FORWARD_FLAG_ITEMNAME)) && parent != null) {
					DocumentFieldUpdater fu = new DocumentFieldUpdater();
					fu.setParameter(DBRequest.ENTITY_PARAM_NAME, parent);
					fu.setParameter(DocumentFieldUpdater.FIELD_NAME_PARAM_NAME, "fld_forwarded");
					fu.setParameter(DocumentFieldUpdater.FIELD_VALUE_PARAM_NAME, "1");
					updater1.setNextRequest(fu);
				}
				// 如果是答复邮件，记录原始文档的已答复标记
				if ("1".equalsIgnoreCase(doc.getItemValue(REPLY_FLAG_ITEMNAME)) && parent != null) {
					DocumentFieldUpdater ru = new DocumentFieldUpdater();
					ru.setParameter(DBRequest.ENTITY_PARAM_NAME, parent);
					ru.setParameter(DocumentFieldUpdater.FIELD_NAME_PARAM_NAME, "fld_replied");
					ru.setParameter(DocumentFieldUpdater.FIELD_VALUE_PARAM_NAME, "1");
					updater1.setNextRequest(ru);
				}
				updater1.setUseTransaction(true);
				updater1.sendRequest();
				// 记录内部邮件的收件人信息为应用程序特定的传阅者。
				if (updater1.getResultLong() > 0) {
					wfr = WorkflowRuntime.getInstance(doc, session);
					WorkflowForm wff = new WorkflowForm();
					wff.setParticipants(all);
					wfr.setWorkflowForm(wff);
					DistributeTransaction t = new DistributeTransaction();
					t.setApplicationFlag(WFDataSubLevel.Application1.getIntValue());
					t.setLogVerb("发送");
					wfr.transact(t, session);
				}
			}
			if (!StringUtil.isBlank(outerto)) {
				Message msg = new Message();
				String mails[] = StringUtil.splitString(outerto, ';');
				Map<Integer, String> mailmap = new HashMap<Integer, String>();
				int idx = 0;
				for (String m : mails) {
					mailmap.put(idx++, m);
				}
				msg.setReceivers(mailmap);
				msg.setSubject(doc.getItemValue("fld_subject"));
				msg.setBody(doc.getItemValue("fld_body"));
				msg.setMessageType(MessageType.OuterMail.getIntValue());
				if (accs != null && accs.size() > 0) {
					List<String> fns = new ArrayList<String>(accs.size());
					String pdir = AccessoryPathHelper.getAbsoluteAccessoryServerPath(doc);
					for (Accessory acc : accs) {
						fns.add(pdir + acc.getFileName());
					}
					msg.setAttachments(fns);
				}
				MessageSender.sendMessage(msg, session);
				updater1.setUseTransaction(true);
				updater1.sendRequest();
			}
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		return this.returnResult("发送成功！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE));
	}
}

