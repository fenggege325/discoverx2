/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.mail;

import com.tansuosoft.discoverx.bll.document.DocumentConstructor;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.Session;

/**
 * 邮件答复操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>mailsource</td>
 * <td>true/false，表示答复时是否带原文，默认为false。通过配置或执行时提供的参数来指定。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link Operation#returnRedirectToResource(com.tansuosoft.discoverx.model.Resource)}对应的结果以打开新建的答复邮件文档。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Reply extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Reply() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		Document doc = null;
		Document replyDoc = null;
		try {
			Session session = this.getSession();

			String unid = this.getParameterStringFromAll("unid", null);
			doc = this.getDocument();
			if (doc == null || !doc.getUNID().equalsIgnoreCase(unid)) doc = this.openDocument(unid);
			if (doc == null) throw new Exception("无法获取目标邮件文档！");
			replyDoc = DocumentConstructor.constructDocument(session, "appMail", doc.getUNID(), null);
			if (replyDoc == null) throw new Exception("无法构造新的邮件文档！");
			replyDoc.replaceItemValue("fld_subject", "答复：" + doc.getItemValue("fld_subject"));
			replyDoc.replaceItemValue("fld_to", ParticipantHelper.getFormatValue(doc.getItemValue("fld_sender"), "cn"));
			replyDoc.replaceItemValue("fld_tovalues", doc.getItemValue("fld_sendersc"));
			replyDoc.replaceItemValue("fld_cc", "");
			replyDoc.replaceItemValue("fld_ccvalues", "");
			replyDoc.replaceItemValue("fld_importance", doc.getItemValue("fld_importance"));
			replyDoc.replaceItemValue("fld_urgency", doc.getItemValue("fld_urgency"));
			replyDoc.replaceItemValue("fld_sentflag", "0");
			replyDoc.replaceItemValue("fld_maildt", "");
			replyDoc.replaceItemValue(Send.REPLY_FLAG_ITEMNAME, "1");
			boolean sourceFlag = this.getParameterBoolean("mailsource", true);
			if (sourceFlag) {
				StringBuilder sb = new StringBuilder();
				sb.append("<p></p>\r\n");
				sb.append("<p></p>\r\n");
				sb.append("<hr/>\r\n");
				sb.append(doc.getItemValue("fld_body"));
				replyDoc.replaceItemValue("fld_body", sb.toString());
			}
			session.setLastDocument(replyDoc);
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		}
		return this.returnRedirectToResource(replyDoc);
	}
}

