/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.mail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.FetchProfile;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.ParseException;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.SentDateTerm;

import com.sun.mail.pop3.POP3Folder;
import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.bll.document.DocumentConstructor;
import com.tansuosoft.discoverx.bll.function.FunctionAttributes;
import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.bll.user.Email;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.MIMEContentTypeConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UNIDProvider;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.transaction.DistributeTransaction;

/**
 * 通过pop3协议接收用户配置的的外部邮件中的邮件的类。
 * 
 * @author coca@tensosoft.com
 */
@FunctionAttributes(type = 1, alias = "pop3Receiver", name = "接收外部邮件", desc = "接收用户通过个性定制或用户配置文件配置的外部邮箱中的邮件。")
public class Pop3Receiver extends Operation {
	private static final String PROVIDER = "pop3";
	private static final String INBOX_FOLDER_NAME = "INBOX";
	private String resultTip = null;

	/**
	 * 接收s指定用户自定义会话对应的用户的外部邮件。
	 * 
	 * <p>
	 * 用户必须在个性定制的外部邮件收发或用户配置文件中配置好相应的外部邮件信息才能正常接收邮件。
	 * </p>
	 * 
	 * @param s
	 * @return 成功则返回true，否则返回false。
	 * @throws Exception
	 */
	public boolean receive(com.tansuosoft.discoverx.model.Session s) throws Exception {
		Store store = null;
		Folder inbox = null;
		Properties props = new Properties();
		try {
			User user = com.tansuosoft.discoverx.model.Session.getUser(s);
			if (user == null || user.isAnonymous()) throw new Exception("您没有配置有效的外部邮件信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");
			Profile p = Profile.getInstance(s);
			List<Email> emails = p.getEmails();
			if (emails == null || emails.isEmpty()) throw new Exception("您没有配置有效的外部邮件信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");
			Email email = emails.get(0);
			if (email == null) throw new Exception("您没有配置有效的外部邮件信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");
			String pop3 = email.getPop3();
			if (pop3 == null || pop3.length() == 0) throw new Exception("您没有配置有效的外部邮件接收服务器信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");
			String account = email.getAccount();
			if (account == null || account.length() == 0) throw new Exception("您没有配置有效的外部邮件账号信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");
			String pwd = email.getPassword();
			if (pwd == null || pwd.length() == 0) throw new Exception("您没有配置有效的外部邮件密码信息，请在“个性设置\\外部邮件收发”中先行配置好相关信息！");

			String host = pop3;
			String username = account;
			String password = pwd;

			// 打开pop3会话和收件箱
			Session session = Session.getDefaultInstance(props, null);
			store = session.getStore(PROVIDER);
			store.connect(host, username, password);
			inbox = store.getFolder(INBOX_FOLDER_NAME);
			if (inbox == null) throw new Exception("无法打开收件箱！");
			if (!(inbox instanceof com.sun.mail.pop3.POP3Folder)) throw new Exception("收件箱不是有效的POP3类型的收件箱！");
			POP3Folder pop3Folder = (POP3Folder) inbox;
			inbox.open(Folder.READ_ONLY);

			// 获取邮件是否已接收的判断比对信息
			DateTime now = new DateTime();
			DateTime dt = null;
			String lastReceivedSentDate = getLastReceivedSentDate(user); // 接收到的外部邮件的最新发送日期时间
			if ((lastReceivedSentDate == null || lastReceivedSentDate.length() == 0) || now.compareTo(new DateTime(lastReceivedSentDate)) < 0) {
				now.adjustDay(-90);
				dt = now;
			} else {
				dt = new DateTime(lastReceivedSentDate);
				dt.adjustDay(-30);
			}
			Map<String, String> map = getSentDtAndUids(dt.toString(), user);

			// 从服务器获取lastReceivedSentDate指定发送日期之后的邮件
			Message[] messages = inbox.search(new SentDateTerm(ComparisonTerm.GE, dt.getJavaDate())); // inbox.getMessages();
			if (messages != null && messages.length > 0) {
				FetchProfile fp = new FetchProfile();
				fp.add(UIDFolder.FetchProfileItem.UID);
				inbox.fetch(messages, fp);
				if (CommonConfig.getInstance().getDebugable()) FileLogger.debug("为“%1$s”接收邮箱“%2$s”中的邮件时返回“%3$d”封待处理的邮件。", user.getCN(), email.getEmail(), messages.length);
				Message m = null;
				String uid = null;
				int receivedCnt = 0;
				Date sentdt = null;
				for (int i = 0; i < messages.length; i++) {
					m = messages[i];
					uid = pop3Folder.getUID(m);
					if (map != null && map.containsKey(uid)) continue; // 已接收的不处理
					if (saveMail(m, uid, s)) {
						sentdt = (sentdt != null && sentdt.after(m.getSentDate()) ? sentdt : m.getSentDate());
						receivedCnt++;
						// m.setFlag(Flags.Flag.DELETED, true); //如果需要可以删除掉接收到的邮件以加快下次接收的性能。
					}
				}
				resultTip = String.format("为“%1$s”成功接收了邮箱“%2$s”中的“%3$d”封邮件。", user.getCN(), email.getEmail(), receivedCnt);
				if (CommonConfig.getInstance().getDebugable()) FileLogger.debug(resultTip);
			} else {
				resultTip = String.format("为“%1$s”接收邮箱“%2$s”中的邮件时没有获取到有效邮件。", user.getCN(), email.getEmail());
				if (CommonConfig.getInstance().getDebugable()) FileLogger.debug(resultTip);
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			try {
				if (inbox != null) inbox.close(true);
				if (store != null) store.close();
			} catch (Exception ex) {
				FileLogger.error(ex);
			}
		}
		return true;
	}

	/**
	 * 获取最近接收到的外部邮件中，发送日期时间最新的邮件对应的发送日期时间。
	 * 
	 * @param user
	 * @return String，找不到则返回null。
	 */
	protected String getLastReceivedSentDate(final User user) {
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql("select fld_maildt from vt_frmmail where fld_inner='0' and fld_sentflag='1' and fld_usersc='" + user.getSecurityCode() + "' order by fld_maildt desc");
				result.setRequestType(RequestType.Scalar);
				return result;
			}
		};
		dbr.sendRequest();
		return dbr.getResult(String.class);
	}

	/**
	 * 获取已接收外部邮件信息以供是否接收判断比对。
	 * 
	 * @param lastReceivedSentDate 已接收外部邮件的发送日期时间，获取此时间之后的邮件。
	 * @param user 目标用户
	 * @return Map&lt;String, String&gt 已接收邮件信息，其中key为外部邮件的uid值，value为外部邮件对应文档的unid值。
	 */
	@SuppressWarnings("unchecked")
	protected Map<String, String> getSentDtAndUids(final String lastReceivedSentDate, final User user) {
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql("select fld_uid,c_punid from vt_frmmail where fld_inner='0' and fld_sentflag='1' and fld_maildt>='" + lastReceivedSentDate + "' and fld_usersc='" + user.getSecurityCode() + "'");
				return result;
			}
		};
		dbr.setResultBuilder(new ResultBuilder() {
			@Override
			public Object build(DBRequest request, Object rawResult) {
				if (rawResult == null || !(rawResult instanceof DataReader)) return null;
				Map<String, String> map = new HashMap<String, String>();
				DataReader dr = (DataReader) rawResult;
				try {
					String uid = null;
					String unid = null;
					while (dr.next()) {
						uid = dr.getString(1);
						unid = dr.getString(2);
						if (uid != null) map.put(uid, unid);
					}
				} catch (SQLException ex) {
					FileLogger.error(ex);
				}
				return map;
			}
		});
		dbr.sendRequest();
		Object obj = dbr.getResult();
		return (obj == null ? null : (Map<String, String>) obj);
	}

	/**
	 * 保存接收到的外部邮件信息到内部邮件文档。
	 * 
	 * <p>
	 * 外部邮件的pop3协议必须支持完整且格式正确。<br/>
	 * <strong>在将邮件接收到内部邮箱后，外部邮箱上对应的邮件将被删除。</strong>
	 * </p>
	 * 
	 * @param m
	 * @param uid 邮件唯一id
	 * @param s
	 * @return boolean
	 */
	protected boolean saveMail(Message m, String uid, com.tansuosoft.discoverx.model.Session s) {
		WorkflowRuntime wfr = null;
		String subject = null;
		try {
			subject = m.getSubject();
			String[] from = m.getHeader("From");
			Document doc = DocumentConstructor.constructDocument(s, "appMail", null, null);
			if (doc == null) throw new Exception("无法构造邮件文档！");
			doc.replaceItemValue("fld_subject", subject);
			String to[] = m.getHeader("To");
			StringPair[] parsedTo = (to != null && to.length > 0 ? parseTo(to) : null);
			if (parsedTo != null && parsedTo.length > 0) {
				StringBuilder sbto = new StringBuilder();
				StringBuilder sbtov = new StringBuilder();
				for (StringPair sp : parsedTo) {
					sbto.append(sbto.length() == 0 ? "" : ";").append(sp.getKey());
					sbtov.append(sbtov.length() == 0 ? "" : ";").append(sp.getValue());
				}
				doc.replaceItemValue("fld_to", sbto.toString());
				doc.replaceItemValue("fld_tovalues", sbtov.toString());
			}
			doc.replaceItemValue("fld_cc", "");
			doc.replaceItemValue("fld_ccvalues", "");
			doc.replaceItemValue("fld_inner", "0");
			doc.replaceItemValue("fld_sentflag", "1");
			doc.replaceItemValue("fld_maildt", new DateTime(m.getSentDate()).toString());
			doc.replaceItemValue("fld_uid", uid);
			StringPair parsedFrom = (from != null && from.length > 0 ? parseFrom(from) : null);
			if (parsedFrom != null) {
				doc.replaceItemValue("fld_sender", parsedFrom.getKey());
				doc.replaceItemValue("fld_sendersc", parsedFrom.getValue());
			}
			String mct = m.getContentType();

			// 处理邮件内容，附件等。
			StringBuilder body = new StringBuilder();
			Object c = m.getContent();
			if (mct.indexOf("text/") == 0 || (c instanceof String)) {
				body.append((String) c);
			} else if (c instanceof Multipart) {
				Multipart mp = (Multipart) c;
				BodyPart bp = null;
				String bpct = null;
				Object bpc = null;
				String disposition = null;
				for (int i = 0; i < mp.getCount(); i++) {
					bp = mp.getBodyPart(i);
					bpc = bp.getContent();
					bpct = bp.getContentType();
					try {
						disposition = bp.getDisposition();
						if (bpc instanceof MimeMultipart) {
							bp = ((MimeMultipart) bpc).getBodyPart(0);
							bpc = bp.getContent();
							bpct = bp.getContentType();
							disposition = bp.getDisposition();
						}
					} catch (ParseException ex) {
						body.append(String.format("<p style=\"font-weight:bold;\">此邮件的MIME头信息或内容不符合RFC822规范，原因：%1$s</p>", ex.getMessage()));
						disposition = null;
					} catch (UnsupportedEncodingException ex) {
						body.append(String.format("<p style=\"font-weight:bold;\">系统不支持此邮件的编码：%1$s</p>", ex.getMessage()));
						disposition = null;
					}
					if (disposition != null && (disposition.equals(Part.ATTACHMENT) || (!(bpc instanceof String) && disposition.equals(Part.INLINE)))) {
						String unid = null;
						String fn = bp.getFileName();
						if (fn != null && (fn.startsWith("=?") || fn.startsWith("?"))) fn = MimeUtility.decodeText(MimeUtility.unfold(fn));
						String[] cts = StringUtil.splitString(bpct, ';');
						if ((fn == null || fn.length() == 0) && cts.length > 1) {
							fn = StringUtil.stringRight(cts[1], "=");
							fn = (fn == null || fn.length() == 0 ? UNIDProvider.getUNID() + MIMEContentTypeConfig.getInstance().checkExt(cts[0], "") : fn.replace("\"", "").trim());
							if (fn.startsWith("=?") || fn.startsWith("?")) fn = MimeUtility.decodeText(MimeUtility.unfold(fn));
							if (fn == null || fn.length() == 0) {
								unid = UNIDProvider.getUNID();
								fn = unid;
							}
						}
						String ext = null;
						if (unid != null) {
							String pureCT = StringUtil.stringRight(cts[0], "=");
							pureCT = (pureCT == null || pureCT.length() == 0 ? bpct : pureCT.replace("\"", "").trim());
							ext = MIMEContentTypeConfig.getInstance().checkExt(pureCT, "");
						}
						saveFile(fn + (ext == null || ext.length() == 0 ? "" : ext.toLowerCase()), bp.getInputStream(), doc, s);
					} else if (bpct.indexOf("text/") == 0 || (bpc instanceof String)) {
						body.append((String) bpc);
					} else {
						if (bpct.indexOf("text/") == 0 || (bpc instanceof String)) {
							body.append((String) bpc);
						} else if (bpc instanceof InputStream) {
							processStreamContent(bpct, (InputStream) bpc, doc, body, s, (i == 0));
						}
					}
				}// for end
			} else if (c instanceof InputStream) {
				processStreamContent(mct, (InputStream) c, doc, body, s, true);
			}
			doc.replaceItemValue("fld_body", body.toString());

			// 保存并设置已接收标记。
			if (ResourceHelper.save(doc, s)) {
				wfr = WorkflowRuntime.getInstance(doc, s);
				WorkflowForm wff = new WorkflowForm();
				List<Integer> all = new ArrayList<Integer>(1);
				all.add(com.tansuosoft.discoverx.model.Session.getUser(s).getSecurityCode());
				wff.setParticipants(all);
				wfr.setWorkflowForm(wff);
				DistributeTransaction t = new DistributeTransaction();
				t.setApplicationFlag(WFDataSubLevel.Application1.getIntValue());
				t.setLogVerb("接收");
				wfr.transact(t, s);
			}
			return true;
		} catch (Exception ex) {
			FileLogger.debug("接收邮件“%1$s”时出现异常！", subject == null ? "[未知标题]" : subject);
			FileLogger.error(ex);
		} finally {
			if (wfr != null) wfr.shutdown();
		}
		return false;
	}

	private static final String CHARSET = "charset";
	private static final String NAME = "name";

	/**
	 * 处理邮件内容中的字节流以保存为文档附件或者作为邮件内容。
	 * 
	 * @param ct MIME类型
	 * @param is 字节流
	 * @param doc 目标文档
	 * @param body 保存邮件内容的StringBuilder
	 * @param s 用户自定义会话
	 * @param isFirstPart 是否第一部分内容
	 * @return boolean 成功则返回true，否则返回false。
	 * @throws Exception
	 */
	protected boolean processStreamContent(String ct, InputStream is, Document doc, StringBuilder body, com.tansuosoft.discoverx.model.Session s, boolean isFirstPart) throws Exception {
		try {
			String[] cts = StringUtil.splitString(ct, ';');
			String cs = null;
			String n = null;
			if (cts.length > 0) cts[0] = cts[0].trim();
			if (cts.length > 1) {
				cts[1] = cts[1].trim();
				if (cts[1].startsWith(CHARSET)) {
					cs = StringUtil.stringRight(cts[1], "=");
					cs = (cs == null || cs.length() == 0 ? "GBK" : cs.replace("\"", "").trim());
				} else if (cts[1].startsWith(NAME)) {
					n = StringUtil.stringRight(cts[1], "=");
					n = (n == null || n.length() == 0 ? UNIDProvider.getUNID() + MIMEContentTypeConfig.getInstance().checkExt(cts[0], "") : n.replace("\"", "").trim());
					if (n.startsWith("=?") || n.startsWith("?")) n = MimeUtility.decodeText(MimeUtility.unfold(n));
				}
			}
			if (n == null || n.length() == 0) {
				InputStreamReader isr = new InputStreamReader(is, cs);
				int c = 0;
				StringBuilder sb = new StringBuilder();
				while ((c = isr.read()) != -1) {
					sb.append((char) c);
				}
				body.append(sb.toString());
			} else {
				saveFile(n, is, doc, s);
			}
		} catch (Exception ex) {
			throw ex;
		}
		return true;
	}

	private int accCnt = 0;

	/**
	 * 将外部邮件中包含的非文本信息和附件等字节流内容保存为目标文档对应的附件。
	 * 
	 * @param fn 文件名
	 * @param is
	 * @param doc
	 * @param s
	 * @return boolean
	 * @throws Exception
	 */
	protected boolean saveFile(String fn, InputStream is, Document doc, com.tansuosoft.discoverx.model.Session s) throws Exception {
		FileOutputStream fs = null;
		try {
			String n = fn;
			if (fn.indexOf('\\') > 0) n = StringUtil.stringRightBack(fn, "\\");
			if (fn.indexOf('/') > 0) n = StringUtil.stringRightBack(fn, "/");
			String dir = AccessoryPathHelper.getAbsoluteAccessoryServerPath(doc);
			File f = new File(dir);
			f.mkdirs();
			String path = dir + n;
			f = new File(path);
			fs = new FileOutputStream(f);
			final int len = 1024;
			byte[] b = new byte[len];
			int read = 0;
			long size = 0L;
			while ((read = is.read(b, 0, len)) > 0) {
				fs.write(b, 0, read);
				size += read;
			}
			List<Accessory> accs = doc.getAccessories();
			if (accs == null) {
				accs = new ArrayList<Accessory>();
				doc.setAccessories(accs);
			}
			Accessory a = new Accessory(doc.getUNID());
			a.setAccessoryType("6A8ACF76DC9DD59DE30E5C91B7FC6F73");
			a.setName(StringUtil.isUNID(StringUtil.stringLeft(n, ".")) ? "附件" + (++accCnt) : n);
			a.setFileName(n);
			a.setSize((int) size);
			a.setCreator(s.getUser().getName());
			a.setModifier(a.getCreator());
			accs.add(a);
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (fs != null) {
				fs.flush();
				fs.close();
			}
		}
		return true;
	}

	/**
	 * 解析外部邮件中名为“From”的头信息以获取邮件发送人信息。
	 * 
	 * @param from
	 * @return StringPair 发送人名称在key中，发送人地址在value中。
	 */
	protected StringPair parseFrom(String[] from) {
		if (from == null || from.length == 0) return null;
		String str = from[0];
		if (str == null || str.length() == 0) return null;
		try {
			str = MimeUtility.decodeText(MimeUtility.unfold(str));
		} catch (UnsupportedEncodingException localUnsupportedEncodingException) {
			str = null;
		}
		if (str != null) {
			str = str.trim();
			String r = StringUtil.stringRightBack(str, "<");
			if (r != null) {
				r = r.replace('>', '\0');
				r = r.trim();
			}
			String l = StringUtil.stringLeftBack(str, "<");
			if (l != null) {
				l = l.trim();
				if (l.startsWith("\"")) l = l.substring(1);
				if (l.endsWith("\"")) l = l.substring(0, l.length() - 1);
			}
			return new StringPair(l == null || l.length() == 0 ? r : l, r);
		}
		return null;
	}

	/**
	 * 解析外部邮件中名为“To”的头信息以获取邮件接收人信息。
	 * 
	 * @param to
	 * @return StringPair[] 接收人名称在key中（如果没有名称则与value相同），接收人地址在value中。
	 */
	protected StringPair[] parseTo(String[] to) {
		if (to == null || to.length == 0) return null;
		List<StringPair> list = new ArrayList<StringPair>();
		for (String t : to) {
			if (t == null || t.length() == 0) continue;
			String ts[] = StringUtil.splitString(t.replace(';', ','), ',');
			for (String s : ts) {
				String r = StringUtil.stringRightBack(s, "<");
				String l = StringUtil.stringLeftBack(s, "<");
				if (r == null && l == null) {
					list.add(new StringPair(s, s));
					continue;
				}
				if (r != null) {
					r = r.replace('>', '\0');
					r = r.trim();
				}
				if (l != null) {
					l = l.trim();
					if (l.startsWith("\"")) l = l.substring(1);
					if (l.endsWith("\"")) l = l.substring(0, l.length() - 1);
				}
				if (r == null) r = s;
				list.add(new StringPair(l == null || l.length() == 0 ? r : l, r));
			}
		}
		StringPair[] sps = new StringPair[list.size()];
		return list.toArray(sps);
	}

	/**
	 * 重载：实现通过pop3协议接收当前用户配置的外部邮箱中的邮件的过程。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			boolean result = receive(this.getSession());
			if (!result) throw new Exception("接收外部邮件时出现未知错误！");
		} catch (Exception ex) {
			if (ex instanceof java.lang.NullPointerException) {
				FileLogger.error(ex);
			}
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE));
		}
		return this.returnResult(resultTip, null, this.returnConfigUrl(UrlConfig.URLCFGNAME_JSON_MESSAGE));
	}
}

