/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.web.app.mail;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;

/**
 * 邮件恢复（从回收站恢复）操作。
 * 
 * <p>
 * 参数说明：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpading="2" style="table-layout:fixed;border-collapse:collapse">
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td width="200">参数名</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>unid</td>
 * <td>要恢复邮件的unid，可以是多个unid数组，通过http请求参数获取。</td>
 * </tr>
 * <tr style="font-weight:bold;background-color:#ece9d8">
 * <td>返回值类型</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>{@link com.tansuosoft.discoverx.model.OperationResult}</td>
 * <td>返回{@link Operation#returnRequestUrl(com.tansuosoft.discoverx.bll.function.OperationParser)}对应的结果。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Undelete extends Operation {
	/**
	 * 缺省构造器。
	 */
	public Undelete() {
		super();
	}

	/**
	 * 重载call
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		try {
			User u = this.getUser();
			int sc = u.getSecurityCode();
			HttpServletRequest request = this.getHttpRequest();
			if (request == null) throw new Exception("无法获取Http请求！");
			String unids[] = request.getParameterValues("unid");
			if (unids == null || unids.length == 0) {
				String unidstr = request.getParameter("unid");
				if (unidstr != null && unidstr.length() > 0) unids = unidstr.split("[,;\\s]");
			}
			if (unids == null || unids.length == 0) throw new Exception("无法获取要恢复的邮件信息！");
			DBRequest dbr = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper result = new SQLWrapper();
					result.setSql(this.getParamValueString("sql", ""));
					result.setRequestType(RequestType.NonQuery);
					return result;
				}
			};
			StringBuilder sql = new StringBuilder();
			for (String s : unids) {
				if (s == null || s.length() == 0) continue;
				sql.append(sql.length() > 0 ? "," : "");
				sql.append("'").append(s).append("'");
			}
			String unidCondtion = sql.toString();
			int subLevel = WFDataSubLevel.Application1.getIntValue();
			sql = new StringBuilder();
			sql.append("update t_wfdata set c_subLevel=").append(subLevel).append(" where c_data=").append(sc).append(" and c_level=2 and c_subLevel>=1024 and c_punid in (");
			sql.append(unidCondtion);
			sql.append(")");
			dbr.setParameter("sql", sql.toString());
			dbr.sendRequest();
			if (dbr.getResultLong() <= 0) throw new Exception("没有恢复任何邮件！");
		} catch (Exception ex) {
			this.setLastError(ex);
			return this.returnResult(ex.getMessage(), null, this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE));
		}
		return this.returnResult("邮件已被恢复！", null, this.returnConfigUrl(UrlConfig.URLCFGNAME_SHOW_MESSAGE_AND_CLOSE));
	}
}

