/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.user;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentPropertyValueProvider;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 提供用户配置文档名称的实现类。
 * 
 * @author coca@tensosoft.com
 */
public class ProfileDocumentNameProvider implements DocumentPropertyValueProvider {

	/**
	 * @see com.tansuosoft.discoverx.model.DocumentPropertyValueProvider#providePropertyValue(com.tansuosoft.discoverx.model.Document, java.lang.Object[])
	 */
	@Override
	public String providePropertyValue(Document doc, Object... objects) {
		if (doc == null) return null;
		int sc = StringUtil.getValueInt(doc.getItemValue("fld_parentsc"), 0);
		ParticipantTree pt = null;
		if (sc > 0) {
			pt = ParticipantTreeProvider.getInstance().getParticipantTree(sc);
			if (pt == null) {
				String fn = StringUtil.getValueString(doc.getItemValue("fld_parentuserfn"), "");
				return (fn != null && fn.length() > 0 ? ParticipantHelper.getFormatValue(fn, "“ou0\\cn”的配置文件") : "用户配置文件");
			} else {
				return ParticipantHelper.getFormatValue(pt, "“ou0\\cn”的配置文件");
			}
		} else {
			return "用户配置文件";
		}
	}

}

