/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.user;

/**
 * 描述用户外部电子邮件收发相关信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Email {
	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param email
	 * @param pop3
	 * @param smtp
	 * @param account
	 * @param password
	 */
	protected Email(String email, String pop3, String smtp, String account, String password) {
		this.m_email = email;
		this.m_pop3 = pop3;
		this.m_smtp = smtp;
		this.m_account = account;
		this.m_password = password;
	}

	private String m_email = null; // 电子邮件地址。
	private String m_pop3 = null; // pop3地址和端口。
	private String m_smtp = null; // smtp地址和端口。
	private String m_account = null; // 电子邮件账号。
	private String m_password = null; // 电子邮件密码。

	/**
	 * 返回电子邮件地址。
	 * 
	 * @return String
	 */
	public String getEmail() {
		return this.m_email;
	}

	/**
	 * 设置电子邮件地址。
	 * 
	 * @param email String
	 */
	public void setEmail(String email) {
		this.m_email = email;
	}

	/**
	 * 返回smtp地址和端口。
	 * 
	 * @return String
	 */
	public String getSmtp() {
		return this.m_smtp;
	}

	/**
	 * 设置smtp地址和端口。
	 * 
	 * <p>
	 * 如果是smtp默认端口则不用设置。设置端口的格式为：smtp地址:端口号
	 * </p>
	 * 
	 * @param smtp String
	 */
	public void setSmtp(String smtp) {
		this.m_smtp = smtp;
	}

	/**
	 * 返回pop3地址和端口。
	 * 
	 * @return String
	 */
	public String getPop3() {
		return this.m_pop3;
	}

	/**
	 * 设置pop3地址和端口。
	 * 
	 * <p>
	 * 如果是pop3默认端口则不用设置。设置端口的格式为：pop3地址:端口号
	 * </p>
	 * 
	 * @param pop3 String
	 */
	public void setPop3(String pop3) {
		this.m_pop3 = pop3;
	}

	/**
	 * 返回电子邮件账号。
	 * 
	 * @return String
	 */
	public String getAccount() {
		return this.m_account;
	}

	/**
	 * 设置电子邮件账号。
	 * 
	 * @param account String
	 */
	public void setAccount(String account) {
		this.m_account = account;
	}

	/**
	 * 返回电子邮件密码。
	 * 
	 * @return String
	 */
	public String getPassword() {
		return this.m_password;
	}

	/**
	 * 设置电子邮件密码。
	 * 
	 * @param password String
	 */
	public void setPassword(String password) {
		this.m_password = password;
	}
}

