/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.user;

/**
 * 描述用户离开办公室相关信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class LeaveOffice {
	/**
	 * 缺省构造器。
	 */
	protected LeaveOffice() {
	}

	private String m_leave = null; // 离开日期时间。
	private String m_return = null; // 返回日期时间。
	private String m_agent = null; // 离开期间代理人。
	private LeaveBroadcastOption m_broadcast = LeaveBroadcastOption.NoBroadcast; // 离开办公室广播选项。
	private String m_notificationTemplate = null; // 离开通知内容的模板。

	/**
	 * 返回离开日期时间。
	 * 
	 * @return String
	 */
	public String getLeave() {
		return this.m_leave;
	}

	/**
	 * 设置离开日期时间。
	 * 
	 * @param leave String
	 */
	public void setLeave(String leave) {
		this.m_leave = leave;
	}

	/**
	 * 返回返回日期时间。
	 * 
	 * @return String
	 */
	public String getReturn() {
		return this.m_return;
	}

	/**
	 * 设置返回日期时间。
	 * 
	 * @param returnDT String
	 */
	public void setReturn(String returnDT) {
		this.m_return = returnDT;
	}

	/**
	 * 返回离开期间代理人。
	 * 
	 * @return String
	 */
	public String getAgent() {
		return this.m_agent;
	}

	/**
	 * 设置离开期间代理人。
	 * 
	 * @param agent String
	 */
	public void setAgent(String agent) {
		this.m_agent = agent;
	}

	/**
	 * 返回离开办公室广播选项。
	 * 
	 * <p>
	 * 默认为{@link LeaveBroadcastOption#NoBroadcast}。
	 * </p>
	 * 
	 * @return LeaveBroadcastOption
	 */
	public LeaveBroadcastOption getBroadcast() {
		return this.m_broadcast;
	}

	/**
	 * 设置离开办公室广播选项。
	 * 
	 * @param broadcast LeaveBroadcastOption
	 */
	public void setBroadcast(LeaveBroadcastOption broadcast) {
		this.m_broadcast = broadcast;
	}

	/**
	 * 返回离开通知内容的模板。
	 * 
	 * @return String
	 */
	public String getNotificationTemplate() {
		return this.m_notificationTemplate;
	}

	/**
	 * 设置离开通知内容的模板。
	 * 
	 * @param notificationTemplate String
	 */
	public void setNotificationTemplate(String notificationTemplate) {
		this.m_notificationTemplate = notificationTemplate;
	}
}

