/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.user;

import com.tansuosoft.discoverx.util.UNIDProvider;

/**
 * 描述显示于门户上的用户快捷链接、快捷操作信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class PortalShortcut {
	/**
	 * 缺省构造器。
	 */
	public PortalShortcut() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param title
	 * @param target
	 * @param sort
	 * @param icon
	 * @param description
	 * @param targetWindow
	 */
	protected PortalShortcut(String title, String target, int sort, String icon, String description, String targetWindow) {
		this.m_UNID = UNIDProvider.getUNID();
		this.m_title = title;
		this.m_target = target;
		this.m_sort = sort;
		this.m_icon = icon;
		this.m_description = description;
		this.m_targetWindow = targetWindow;
	}

	private String m_UNID = null; // 唯一UNID。
	private String m_title = null; // 标题。
	private String m_description = null; // 说明。
	private String m_target = null; // 目标代码。
	private String m_icon = null; // 图标url路径。
	private int m_sort = 0; // 排序号。
	private String m_targetWindow = "_blank"; // 目标窗口。
	private boolean m_systemDefault = false; // 是否系统默认显示的快捷链接或者快捷操作。

	/**
	 * 返回唯一UNID。
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置唯一UNID。
	 * 
	 * @param UNID String
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回标题。
	 * 
	 * @return String
	 */
	public String getTitle() {
		return this.m_title;
	}

	/**
	 * 设置标题。
	 * 
	 * @param title String
	 */
	public void setTitle(String title) {
		this.m_title = title;
	}

	/**
	 * 返回说明。
	 * 
	 * @return String
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置说明。
	 * 
	 * @param description String
	 */
	public void setDescription(String description) {
		this.m_description = description;
	}

	/**
	 * 返回目标代码。
	 * 
	 * @return String
	 */
	public String getTarget() {
		return this.m_target;
	}

	/**
	 * 设置目标代码。
	 * 
	 * <p>
	 * 一般是指&lt;a href=""...中href属性的有效值。
	 * </p>
	 * 
	 * @param target String
	 */
	public void setTarget(String target) {
		this.m_target = target;
	}

	/**
	 * 返回图标url路径。
	 * 
	 * @return String
	 */
	public String getIcon() {
		return this.m_icon;
	}

	/**
	 * 设置图标url路径。
	 * 
	 * @param icon String
	 */
	public void setIcon(String icon) {
		this.m_icon = icon;
	}

	/**
	 * 返回排序号。
	 * 
	 * @return int
	 */
	public int getSort() {
		return this.m_sort;
	}

	/**
	 * 设置排序号。
	 * 
	 * @param sort int
	 */
	public void setSort(int sort) {
		this.m_sort = sort;
	}

	/**
	 * 返回目标窗口。
	 * 
	 * @return String
	 */
	public String getTargetWindow() {
		return this.m_targetWindow;
	}

	/**
	 * 设置目标窗口。
	 * 
	 * <p>
	 * 一般是指&lt;a target=""...中target属性的有效值，默认为“_blank”。
	 * </p>
	 * 
	 * @param targetWindow String
	 */
	public void setTargetWindow(String targetWindow) {
		this.m_targetWindow = targetWindow;
	}

	/**
	 * 返回是否系统默认显示的快捷链接或者快捷操作。
	 * 
	 * <p>
	 * 默认为false。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getSystemDefault() {
		return this.m_systemDefault;
	}

	/**
	 * 设置是否系统默认显示的快捷链接或者快捷操作。
	 * 
	 * @param systemDefault boolean
	 */
	public void setSystemDefault(boolean systemDefault) {
		this.m_systemDefault = systemDefault;
	}
}

