/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.ResourceConstructor;
import com.tansuosoft.discoverx.bll.ResourceConstructorProvider;
import com.tansuosoft.discoverx.bll.ResourceInserter;
import com.tansuosoft.discoverx.bll.ResourceInserterProvider;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 表示用户配置文件相关上下文的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Profile {
	private static HashMap<String, Profile> m_instances = new HashMap<String, Profile>();

	private Document m_document = null;
	private User m_user = null;

	private static final String FNPN = "FIELD_NAME";
	private static final String FVPN = "FIELD_VALUE";
	private static final String DOCUMENT_FIELD_UPDATER_IMPL = "com.tansuosoft.discoverx.dao.impl.DocumentFieldUpdater";

	/**
	 * 获取保存用户配置信息的文档。
	 * 
	 * @return {@link Document}对象或null（如果没有对应的配置文档的话）。
	 */
	public Document getDocument() {
		return m_document;
	}

	/**
	 * 获取用户自定义快捷操作列表。
	 * 
	 * @return List&lt;String&gt;，如果没有自定义快捷操作则返回null。
	 */
	public List<String> getShortcutActions() {
		if (m_document == null) return null;
		String[] shortcutsctions = m_document.getItemValues("fld_shortcutsctions");
		if (shortcutsctions == null || shortcutsctions.length == 0) return null;
		List<String> list = new ArrayList<String>();
		for (String s : shortcutsctions) {
			if (s == null || s.length() == 0) continue;
			list.add(s);
		}
		return list;
	}

	/**
	 * 设置指定内容为用户快捷操作菜单。
	 * 
	 * @param shortcutActionsid
	 * @return 如果设置成功则返回true，否则返回false。
	 */
	public boolean setShortcutActions(String shortcutActionsid) {
		if (m_document == null) return false;
		Item item = m_document.getItem("fld_shortcutsctions");
		if (item == null) return false;
		DBRequest dbr = DBRequest.getInstance(DOCUMENT_FIELD_UPDATER_IMPL);
		dbr.setResource(m_document);
		dbr.setParameter(FNPN, "fld_shortcutsctions");
		dbr.setParameter(FVPN, shortcutActionsid);
		dbr.sendRequest();
		if (dbr.getResultLong() > 0) {
			m_document.replaceItemValue("fld_shortcutsctions", shortcutActionsid);
			return true;
		}
		return false;
	}

	/**
	 * 获取用户自定义快捷链接列表。
	 * 
	 * @return List&lt;String&gt;，如果没有自定义快捷链接则返回null。
	 */
	public List<String> getShortcutLinks() {
		if (m_document == null) return null;
		String[] shortcutsctions = m_document.getItemValues("fld_shortcutlinks");
		if (shortcutsctions == null || shortcutsctions.length == 0) return null;
		List<String> list = new ArrayList<String>();
		for (String s : shortcutsctions) {
			if (s == null || s.length() == 0) continue;
			list.add(s);
		}
		return list;
	}

	/**
	 * 设置指定内容为用户快捷链接菜单。
	 * 
	 * @param shortcutActionsid
	 * @return 如果设置成功则返回true，否则返回false。
	 */
	public boolean setShortcutLinks(String shortcutLinksid) {
		if (m_document == null) return false;
		Item item = m_document.getItem("fld_shortcutlinks");
		if (item == null) return false;
		DBRequest dbr = DBRequest.getInstance(DOCUMENT_FIELD_UPDATER_IMPL);
		dbr.setResource(m_document);
		dbr.setParameter(FNPN, "fld_shortcutlinks");
		dbr.setParameter(FVPN, shortcutLinksid);
		dbr.sendRequest();
		if (dbr.getResultLong() > 0) {
			m_document.replaceItemValue("fld_shortcutlinks", shortcutLinksid);
			return true;
		}
		return false;
	}

	protected static final String FN_DEFAULT_USE_PORTAL = "fld_defaultportal";

	/**
	 * 获取用户使用的门户资源对象。
	 * 
	 * <p>
	 * 获取步骤：
	 * <ol>
	 * <li>如果{@link #getPortal()}有有效返回值则返回。</li>
	 * <li>检查用户配置文件中是否有通过名为“fld_defaultportal”的字段保存的默认使用的门户UNID或别名，有则获取这个门户资源并返回；</li>
	 * <li>如果都找不到有效的门户资源则使用系统配置的默认门户资源。</li>
	 * </ol>
	 * </p>
	 * <p>
	 * 获取用户实际使用的门户资源时应优先使用此方法。
	 * </p>
	 * 
	 * @return Portal
	 */
	public Portal getUsePortal() {
		Portal portal = getPortal();
		// 如果没有自定义的门户则检查是否有配置有效的默认使用门户
		if (portal == null && m_document != null && m_document.hasItem(FN_DEFAULT_USE_PORTAL)) {
			String defaultPortalId = m_document.getItemValue(FN_DEFAULT_USE_PORTAL);
			if (defaultPortalId != null && defaultPortalId.length() > 0) {
				if (defaultPortalId.startsWith("ptl")) defaultPortalId = ResourceAliasContext.getInstance().getUNIDByAlias(Portal.class, defaultPortalId);
				portal = ResourceContext.getInstance().getResource(defaultPortalId, Portal.class);
			}
		}
		// 如果都找不到则使用系统配置的默认门户资源。
		if (portal == null) portal = ResourceContext.getInstance().getDefaultResource(Portal.class);
		return portal;
	}

	/**
	 * 获取用户自定义门户对象。
	 * 
	 * <p>
	 * 自定义门户指用户通过门户首页拖放、删除、添加等自定义的门户。<br/>
	 * 此方法检查用户配置文件中是否有通过名为“fld_portal”的字段保存的自定义的门户xml文本，有则解析并返回。
	 * </p>
	 * 
	 * @return Portal 如果用户没有自定义门户则返回null。
	 */
	public Portal getPortal() {
		if (m_document == null) return null;
		Portal portal = null;
		// 是否有自定义的门户
		String portalXml = this.m_document.getItemValue("fld_portal");
		if (portalXml != null && portalXml.length() > 0) {
			XmlDeserializer deser = new XmlDeserializer();
			portal = new Portal();
			deser.setTarget(portal);
			deser.deserialize(portalXml, Portal.class);
		}
		return portal;
	}

	/**
	 * 设置用户自定义门户对象。
	 * 
	 * @param sms
	 * @return 如果设置成功则返回true，否则返回false。
	 */
	public boolean setPortal(String xml) {
		if (m_document == null) return false;
		Item item = m_document.getItem("fld_portal");
		if (item == null) return false;
		DBRequest dbr = DBRequest.getInstance(DOCUMENT_FIELD_UPDATER_IMPL);
		dbr.setResource(m_document);
		dbr.setParameter(FNPN, "fld_portal");
		dbr.setParameter(FVPN, xml);
		dbr.sendRequest();
		if (dbr.getResultLong() > 0) {
			m_document.replaceItemValue("fld_portal", xml);
			return true;
		}
		return false;
	}

	/**
	 * 返回有效的用户离开办公室信息。
	 * 
	 * @return {@link LeaveOffice}或null（当用户在当前日期时间内没有离开办公室时）。
	 */
	public LeaveOffice getLeaveOffice() {
		if (m_document == null) return null;
		LeaveOffice lo = new LeaveOffice();
		lo.setLeave(m_document.getItemValue("fld_leave"));
		lo.setReturn(m_document.getItemValue("fld_return"));
		lo.setAgent(m_document.getItemValue("fld_agent"));
		lo.setBroadcast(StringUtil.getValueEnum(m_document.getItemValue("fld_broadcast"), LeaveBroadcastOption.class, LeaveBroadcastOption.NoBroadcast));
		lo.setNotificationTemplate(m_document.getItemValue("fld_notificationtemplate"));
		String nowdt = new DateTime().toString();
		String leaveDt = lo.getLeave();
		String returnDt = lo.getReturn();
		if (leaveDt != null && nowdt.compareToIgnoreCase(leaveDt) >= 0 && returnDt != null && nowdt.compareToIgnoreCase(returnDt) <= 0) return lo;
		return null;
	}

	/**
	 * 设置用户离开办公室相关信息。
	 * 
	 * @param leavedt 离开时间
	 * @param returndt 返回时间
	 * @param agent 代理人
	 * @param broadcast 广播离开通知类型
	 * @param notificationTemplate 离开通知内容模板
	 * @return 如果设置成功则返回true，否则返回false。
	 */
	public boolean setLeaveOffice(String leavedt, String returndt, String agent, String broadcast, String notificationTemplate) {
		if (m_document == null) return false;
		boolean result = true;
		Map<String, String> m_itemsvalue = new HashMap<String, String>();
		m_itemsvalue.put("fld_leave", leavedt);
		m_itemsvalue.put("fld_return", returndt);
		m_itemsvalue.put("fld_agent", agent);
		m_itemsvalue.put("fld_broadcast", broadcast);
		m_itemsvalue.put("fld_notificationtemplate", notificationTemplate);
		String agentsc = "0";
		if (agent != null && agent.length() > 0) {
			ParticipantTree pt = ParticipantTreeProvider.getInstance().getParticipantTree(agent);
			if (pt != null) {
				agentsc = pt.getSecurityCode() + "";
			} else {
				FileLogger.debug("找不到委托者“%1$s”设置的代理用户“%2$s”对应的安全编码。", this.m_user.getName(), agent);
			}
		}
		m_itemsvalue.put("fld_agentsc", agentsc);
		DBRequest first = null;
		DBRequest dbr = null;
		for (String k : m_itemsvalue.keySet()) {
			Item item = m_document.getItem(k);
			if (item == null) continue;
			dbr = DBRequest.getInstance(DOCUMENT_FIELD_UPDATER_IMPL);
			dbr.setResource(m_document);
			dbr.setParameter(FNPN, k);
			dbr.setParameter(FVPN, m_itemsvalue.get(k));
			if (first == null) {
				first = dbr;
			} else {
				first.setNextRequest(dbr);
			}
		}
		if (first != null) {
			first.sendRequest();
			result = (first.getResultLong() > 0);
		}
		return result;
	}

	/**
	 * 获取收发外部电子邮件的电子邮件信息列表。
	 * 
	 * @return List&lt;Email&gt;，如果没有配置则返回null。
	 */
	public List<Email> getEmails() {
		if (m_document == null) return null;
		String[] emails = m_document.getItemValues("fld_email");
		String[] pop3s = m_document.getItemValues("fld_pop3");
		String[] smtps = m_document.getItemValues("fld_smtp");
		String[] accounts = m_document.getItemValues("fld_account");
		String[] passwords = m_document.getItemValues("fld_password");
		if (emails == null || emails.length == 0) return null;
		String email = null;
		String pop3 = null;
		String smtp = null;
		String account = null;
		String password = null;
		List<Email> list = null;
		for (int i = 0; i < emails.length; i++) {
			email = emails[i];
			if (email == null || email.length() == 0) continue;
			pop3 = (i < pop3s.length ? pop3s[i] : null);
			smtp = (i < smtps.length ? smtps[i] : null);
			account = (i < accounts.length ? accounts[i] : null);
			password = (i < passwords.length ? passwords[i] : null);
			if (list == null) list = new ArrayList<Email>(emails.length);
			list.add(new Email(email, pop3, smtp, account, password));
		}
		return list;
	}

	/**
	 * 设置收发外部电子邮件的电子邮件信息。
	 * 
	 * @param email
	 * @param smtp
	 * @param pop3
	 * @param account
	 * @param password
	 * @return 如果设置成功则返回true，否则返回false。
	 */
	public boolean setEmails(String email, String smtp, String pop3, String account, String password) {
		if (m_document == null) return false;
		boolean result = true;
		Hashtable<String, String> m_itemsvalue = new Hashtable<String, String>();
		m_itemsvalue.put("fld_email", email);
		m_itemsvalue.put("fld_smtp", smtp);
		m_itemsvalue.put("fld_pop3", pop3);
		m_itemsvalue.put("fld_account", account);
		m_itemsvalue.put("fld_password", password);
		DBRequest first = null;
		DBRequest dbr = null;
		for (String k : m_itemsvalue.keySet()) {
			Item item = m_document.getItem(k);
			if (item == null) continue;
			dbr = DBRequest.getInstance(DOCUMENT_FIELD_UPDATER_IMPL);
			dbr.setResource(m_document);
			dbr.setParameter(FNPN, k);
			dbr.setParameter(FVPN, m_itemsvalue.get(k));
			if (first == null) {
				first = dbr;
			} else {
				first.setNextRequest(dbr);
			}
		}
		if (first != null) {
			first.sendRequest();
			result = (first.getResultLong() > 0);
		}
		return result;
	}

	/**
	 * 获取用户是否接收短消息通知。
	 * 
	 * @return 如果没有配置则返回true。
	 */
	public boolean getSmsNotification() {
		if (m_document == null) return false;
		return StringUtil.getValueBool(m_document.getItemValue("fld_smsnotification"), true);
	}

	/**
	 * 设置用户是否接收短消息通知。
	 * 
	 * @param smsnotificationoption
	 * @return 如果设置成功则返回true，否则返回false。
	 */
	public boolean setSmsNotification(String smsnotificationoption) {
		if (m_document == null) return false;
		Item item = m_document.getItem("fld_smsnotification");
		if (item == null) return false;
		DBRequest dbr = DBRequest.getInstance(DOCUMENT_FIELD_UPDATER_IMPL);
		dbr.setResource(m_document);
		dbr.setParameter(FNPN, "fld_smsnotification");
		dbr.setParameter(FVPN, smsnotificationoption);
		dbr.sendRequest();
		if (dbr.getResultLong() > 0) {
			m_document.replaceItemValue("fld_smsnotification", smsnotificationoption);
			return true;
		}
		return false;
	}

	/**
	 * 获取收发短消息的号码。
	 * 
	 * @return 如果没有配置则返回null。
	 */
	public String getSms() {
		if (m_document == null) return null;
		return m_document.getItemValue("fld_sms");
	}

	/**
	 * 设置收发短消息的号码。
	 * 
	 * @param sms
	 * @return 如果设置成功则返回true，否则返回false。
	 */
	public boolean setSms(String sms) {
		if (m_document == null) return false;
		Item item = m_document.getItem("fld_sms");
		if (item == null) return false;
		DBRequest dbr = DBRequest.getInstance(DOCUMENT_FIELD_UPDATER_IMPL);
		dbr.setResource(m_document);
		dbr.setParameter(FNPN, "fld_sms");
		dbr.setParameter(FVPN, sms);
		dbr.sendRequest();
		if (dbr.getResultLong() > 0) {
			m_document.replaceItemValue("fld_sms", sms);
			return true;
		}
		return false;
	}

	/**
	 * 保存用户常用自定义意见的字段名。
	 */
	private static final String FLD_OPINIONS_NAME = "fld_opinions";

	/**
	 * 获取用户有效的自定义常用意见列表。
	 * 
	 * @return
	 */
	public List<String> getOpinions() {
		if (m_document == null) return null;
		String[] opinions = m_document.getItemValues(FLD_OPINIONS_NAME);
		if (opinions == null || opinions.length == 0) return null;
		List<String> list = new ArrayList<String>();
		for (String s : opinions) {
			if (s == null || s.length() == 0) continue;
			list.add(s);
		}
		return list;
	}

	/**
	 * 设置指定内容为用户常用意见。
	 * 
	 * @param opinionBody
	 * @return 如果设置成功则返回true，否则返回false。
	 */
	public boolean setOpinion(String opinionBody) {
		if (m_document == null || opinionBody == null || opinionBody.length() == 0) return false;
		Item item = m_document.getItem(FLD_OPINIONS_NAME);
		if (item == null) return false;
		List<String> list = this.getOpinions();
		if (list != null && list.contains(opinionBody)) return true;
		DBRequest dbr = DBRequest.getInstance(DOCUMENT_FIELD_UPDATER_IMPL);
		dbr.setParameter(FNPN, FLD_OPINIONS_NAME);
		String oldValue = m_document.getItemValue(FLD_OPINIONS_NAME);
		String newValue = (oldValue == null || oldValue.length() == 0 ? "" : oldValue + item.getDelimiter()) + opinionBody;
		dbr.setParameter(FVPN, newValue);
		dbr.setResource(m_document);
		dbr.sendRequest();
		if (dbr.getResultLong() > 0) {
			m_document.replaceItemValue(FLD_OPINIONS_NAME, newValue);
			return true;
		}
		return false;
	}

	/**
	 * 接收用户资源的构造器。
	 * 
	 * @param user
	 */
	private Profile(User user) {
		if (user == null || user.isAnonymous()) return;
		Session s = Session.getSession(user.getUNID());
		boolean nullSessionFlag = false;
		if (s == null) {
			nullSessionFlag = true;
			s = Session.newSession(user);
		}

		String profile = user.getProfile();
		if (profile != null) profile = profile.trim();
		// 如果用户还没有绑定配置文档，那么创建一个
		if (profile == null || profile.isEmpty() || !StringUtil.isUNID(profile)) {
			m_document = createProfileDocument(s);
		} else { // 否则从用户绑定的配置文档unid中打开配置文档
			m_document = DocumentBuffer.getInstance().getDocument(profile);
			if (m_document == null) {
				m_document = ResourceContext.getInstance().getResource(profile, Document.class);
				if (m_document != null) DocumentBuffer.getInstance().setDocument(m_document, user.getName());
			}
		}

		// 如果配置文档还找不到，那么可能是绑定的unid指向了无效的，那么重新创建一个配置文档
		if (m_document == null) {
			m_document = createProfileDocument(s);
		} else { // 如果parentsc（所属用户安全编码）没有值或与用户不符，则填充之
			String parentsc = m_document.getItemValue("fld_parentsc");
			String currentsc = user.getSecurityCode() + "";
			if (parentsc == null || !parentsc.equalsIgnoreCase(currentsc)) {
				DBRequest dbr = DBRequest.getInstance(DOCUMENT_FIELD_UPDATER_IMPL);
				dbr.setParameter(FNPN, "fld_parentsc");
				dbr.setParameter(FVPN, currentsc);
				dbr.setResource(m_document);
				dbr.sendRequest();
				if (dbr.getResultLong() > 0) m_document.replaceItemValue("fld_parentsc", currentsc);
			}
		}

		m_user = user;
		if (nullSessionFlag) {
			Session.removeSession(user);
		}
	}

	/**
	 * 获取用户对应的配置文档上下文类的实例。
	 * 
	 * @param user
	 * @return 返回用户对应的{@link Profile}对象。
	 */
	public synchronized static Profile getInstance(User user) {
		if (user == null) throw new RuntimeException("未提供有效用户对象！");
		Profile profile = m_instances.get(user.getUNID());
		if (profile == null) {
			profile = new Profile(user);
			m_instances.put(user.getUNID(), profile);
		}
		return profile;
	}

	/**
	 * 获取用户自定义会话对应的用户的配置文档上下文类的实例。
	 * 
	 * @param session
	 * @return
	 */
	public static Profile getInstance(Session session) {
		return getInstance(Session.getUser(session));
	}

	/**
	 * 清除用户对应的配置文档上下文类的实例。
	 * 
	 * @param user
	 * @return 返回清除的{@link Profile}对象或null(没有被清除的内容时）。
	 */
	public static Profile clear(User user) {
		if (user == null) throw new RuntimeException("未提供有效用户对象！");
		Profile profile = m_instances.remove(user.getUNID());
		// 同时清除文档缓存
		if (profile != null) {
			Document doc = profile.getDocument();
			if (doc != null) DocumentBuffer.getInstance().removeDocument(doc.getUNID());
		}
		return profile;
	}

	/**
	 * 清除用户自定义会话对应的用户的配置文档上下文类的实例。
	 * 
	 * @param session
	 * @return
	 */
	public static Profile clear(Session session) {
		return clear(Session.getUser(session));
	}

	/**
	 * 为指定用户自定义会话对应的用户创建一个新的配置文档并返回之。
	 * 
	 * <p>
	 * 如果配置文档已经存在，则直接返回之，否则返回创建的文档。
	 * </p>
	 * 
	 * @param session
	 * @return Document
	 */
	protected static Document createProfileDocument(Session session) {
		try {
			if (session == null) throw new ResourceException("没有提供有效用户自定义会话信息");
			User u = Session.getUser(session);
			if (u == null || u.isAnonymous()) return null;
			ResourceConstructor c = ResourceConstructorProvider.getResourceConstructor(Document.class);
			Document doc = (c == null ? null : (Document) c.construct(Document.class, session, "76A3F1482BDCA8911AA48F43D5E6D5D9", null, null));
			if (doc != null) {
				doc.replaceItemValue("fld_parentsc", u.getSecurityCode() + "");
				doc.replaceItemValue("fld_parentuserfn", u.getName());
				ResourceInserter inserter = ResourceInserterProvider.getResourceInserter(doc);
				if (inserter.insert(doc, session) != null) { // 文档保存成功则
					final String c_profile = doc.getUNID();
					final String c_unid = u.getUNID();
					DBRequest dbr = new DBRequest() {
						@Override
						protected SQLWrapper buildSQL() {
							SQLWrapper result = new SQLWrapper();
							result.setSql("update t_user set c_profile='" + c_profile + "' where c_unid='" + c_unid + "'");
							result.setRequestType(RequestType.NonQuery);
							return result;
						}
					};
					dbr.sendRequest();
					if (dbr.getResultLong() > 0) u.setProfile(doc.getUNID());
					System.out.println(DateTime.getNowDTString() + ":为“" + ParticipantHelper.getFormatValue(u, "ou0\\cn") + "”创建配置文件！");
				}
			}
			return doc;
		} catch (ResourceException e) {
			FileLogger.debug("无法创建用户“%1$s”对应的配置文档：“%2$s”，请联系管理员检查系统配置！", Session.getUser(session).getName(), e.getMessage());
		}
		return null;
	}
}

