/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

/**
 * 表示解析后的变量信息的类。
 * 
 * <p>
 * 即表达式中包含的字段或公式变量的解析结果。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ParsedVariable {

	/**
	 * 表示表达式中字段变量的指示前缀：“$”。
	 */
	public static final char FIELD_VAR_PREFIX = '$';

	/**
	 * 表示表达式中公式变量的指示前缀：“@”。
	 */
	public static final char FORMULA_VAR_PREFIX = '@';

	/**
	 * 为指定表达式创建对应ParsedVariable实例并返回创建的实例。
	 * 
	 * @param variable String
	 * @param range VariableRange
	 * @return ParsedVariable
	 */
	public static ParsedVariable createVariable(String variable, VariableRange range) {
		if (variable == null || variable.length() == 0) return null;

		ParsedVariable pv = null;
		char char0 = variable.charAt(0);
		if (char0 == FIELD_VAR_PREFIX) {
			pv = new ParsedField(variable, range);
		} else if (char0 == FORMULA_VAR_PREFIX) {
			pv = new ParsedFormula(variable, range);
		}
		return pv;

	}

	/**
	 * 缺省构造器。
	 */
	protected ParsedVariable() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param variable String
	 * @param range VariableRange
	 */
	protected ParsedVariable(String variable, VariableRange range) {
		this.m_variable = variable;
		this.m_variableRange = range;
	}

	private String m_variable = null; // 解析出来的原始变量体。
	private VariableRange m_variableRange = null; // 变量信息对应的VariableRange对象。

	/**
	 * 返回解析出来的原始变量体。
	 * 
	 * <p>
	 * 一个变量体表示一个可独立存在的公式或字段变量的完整内容。<br/>
	 * 以下即为可能的变量体：<br/>
	 * <list>
	 * <li>@Username(format:="cn")</li>
	 * <li>@F1(f2:=@F2(p:="pv"))</li>
	 * <li>#fld_subject<br/>
	 * </li>
	 * </list>
	 * </p>
	 * 
	 * @return String
	 */
	public String getVariable() {
		return this.m_variable;
	}

	/**
	 * 获取绑定的{@link VariableRange}对象。
	 * 
	 * @see VariableRange
	 * @return
	 */
	public VariableRange getVariableRange() {
		return this.m_variableRange;
	}

	/**
	 * 重载toString：输出格式为：variable:{变量内容}
	 * 
	 * <p>
	 * 示例：<br/>
	 * <list>
	 * <li>variable:@Username(format:="cn")</li>
	 * <li>variable:@F1(f2:=@F2(p:="pv"))</li>
	 * <li>variable:#fld_subject<br/>
	 * </li>
	 * </list>
	 * </p>
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("variable:").append(m_variable);
		return sb.toString();
	}

}

