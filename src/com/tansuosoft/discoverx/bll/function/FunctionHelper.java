/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.HashMap;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.model.Function;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;

/**
 * 系统功能扩展函数辅助工具类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class FunctionHelper {

	private static FunctionHelper m_instance = null;
	private HashMap<String, String> m_alias = null;// 缓存Function资源别名和和unid对照关系

	/**
	 * 私有构造器
	 */
	private FunctionHelper() {
		List<Resource> functions = XmlResourceLister.getResources(Function.class);
		if (functions != null && !functions.isEmpty()) {
			m_alias = new HashMap<String, String>(functions.size());
			String alias = null;
			String unid = null;
			for (Resource x : functions) {
				alias = x.getAlias();
				if (alias != null) alias = alias.toLowerCase();
				unid = x.getUNID();
				m_alias.put(alias, unid);
			}
		}
	}

	/**
	 * 获取唯一实例
	 * 
	 * @return FunctionHelper
	 */
	synchronized public static FunctionHelper getInstance() {
		if (m_instance == null) m_instance = new FunctionHelper();
		return m_instance;
	}

	/**
	 * 根据功能扩展函数别名获取对应UNID。
	 * 
	 * @param alias
	 * @return String
	 */
	public String getFunctionUNIDByAlias(String alias) {
		if (alias == null || alias.length() == 0) return null;
		String result = (m_alias == null || m_alias.isEmpty() ? null : m_alias.get(alias.toLowerCase()));
		if (result == null || result.length() == 0) {
			result = ResourceAliasContext.getInstance().getUNIDByAlias(Function.class, alias);
			if (result != null && result.length() > 0) m_alias.put(alias.toLowerCase(), result);
		}
		return result;
	}

	/**
	 * 根据功能扩展函数UNID获取对应别名。
	 * 
	 * @param unid
	 * @return String
	 */
	public String getFunctionAliasByUNID(String unid) {
		Function f = ResourceContext.getInstance().getResource(unid, Function.class);
		String alias = (f == null ? null : f.getAlias());
		if (alias != null && alias.length() > 0) {
			if (m_alias == null) m_alias = new HashMap<String, String>();
			m_alias.put(alias.toLowerCase(), f.getUNID());
		}
		return alias;
	}
}

