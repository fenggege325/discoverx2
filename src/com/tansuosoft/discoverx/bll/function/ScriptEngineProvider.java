/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 * 提供用于计算表达式最终结果的脚本引擎的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ScriptEngineProvider {
	private ScriptEngineManager scriptEngineManager = null;
	private ScriptEngine scriptEngine = null;

	private static ScriptEngineProvider m_instance = null;

	/**
	 * 缺省构造器。
	 */
	private ScriptEngineProvider() {
		if (scriptEngine == null) {
			scriptEngineManager = new ScriptEngineManager();
			scriptEngine = scriptEngineManager.getEngineByName("JavaScript");
		}
	}

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return ScriptEngineProvider
	 */
	public static synchronized ScriptEngineProvider getInstance() {
		if (m_instance == null) {
			m_instance = new ScriptEngineProvider();
		}
		return m_instance;
	}

	/**
	 * 获取脚本引擎实例。
	 * 
	 * @return ScriptEngine
	 */
	public ScriptEngine getScriptEngine() {
		return this.scriptEngine;
	}
}

