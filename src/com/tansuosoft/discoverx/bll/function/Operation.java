/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.LogHelper;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.LogType;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 表示所有处理http请求并返回处理结果(OperationResult)的系统功能函数实现类的基类。
 * 
 * <p>
 * 所有操作的预设的参数说明如下:
 * <ul>
 * <li>uipath:绑定的操作向导操作文件名，可选。</li>
 * <li>jsfunction:执行的客户端js代码，可选。</li>
 * <li>nextoperation:此操作执行完成后下一步操作表达式，可选。</li>
 * <li>resulturl:此操作执行完成后显示的url路径，可选。</li>
 * <li>webrequestform:可自动填充的web提交的请求信息对象的全限定名称，可选，默认为“com.tansuosoft.discoverx.bll.function.CommonDispatchForm”，如果需要“CommonDispatchForm”对象实例，则不能配置为其它的。</li>
 * </ul>
 * </p>
 * 
 * @see com.tansuosoft.discoverx.model.Function
 * @see com.tansuosoft.discoverx.bll.function.Function
 * @author coca@tansuosoft.cn
 */
public abstract class Operation extends Function {
	/**
	 * 系统默认的操作UNID。
	 * <p>
	 * 系统默认的操作为打开资源。
	 * </p>
	 */
	public static final String SYSTEM_DEFAULT_OPERATION = "07E851FE490444FCAEB739503434B442";

	/**
	 * 常用参数定义：uipath，表示获取操作绑定的操作向导控件文件名的参数名。
	 * 
	 * <p>
	 * 如果配置时参数值只提供操作向导实现的控件文件名，则表示是某个应用程序特有的操作向导界面；<br/>
	 * 否则如果提供了路径信息则每次都从路径所指定的文件中中获取操作向导控件文件。
	 * </p>
	 * 
	 * @see Operation#getPredefinedParameters()
	 */
	public static final String BUILTIN_PARAMNAME_UIPATH = "uipath";
	/**
	 * 常用参数定义：jsfunction，表示获取客户端js代码的参数名。
	 * 
	 * @see Operation#getPredefinedParameters()
	 */
	public static final String BUILTIN_PARAMNAME_JSFUNCTION = "jsfunction";
	/**
	 * 常用参数定义：js，表示获取客户端js代码的参数名的同义别名。
	 * 
	 * @see Operation#getPredefinedParameters()
	 */
	public static final String BUILTIN_PARAMNAME_JSFUNCTION_ALIAS = "js";
	/**
	 * 常用参数定义：nextoperation，表示获取操作链中下一个操作信息的参数名。
	 * 
	 * @see Operation#getPredefinedParameters()
	 */
	public static final String BUILTIN_PARAMNAME_NEXTOPERATION = "nextoperation";
	/**
	 * 常用参数定义：next，表示获取操作链中下一个操作信息的参数名的同义别名。
	 * 
	 * @see Operation#getPredefinedParameters()
	 */
	public static final String BUILTIN_PARAMNAME_NEXTOPERATION_ALIAS = "next";
	/**
	 * 常用参数定义：resulturl，表示获取操作结果url信息的参数名。
	 * 
	 * @see Operation#getPredefinedParameters()
	 */
	public static final String BUILTIN_PARAMNAME_RESULTURL = "resulturl";
	/**
	 * 常用参数定义：webrequestform，表示获取web请求信息封装对象全限定类名称的参数名。
	 * 
	 * @see Operation#getPredefinedParameters()
	 */
	public static final String BUILTIN_PARAMNAME_WEBREQUESTFORM = "webrequestform";

	/**
	 * 常用参数定义：operationpostback，表示获取是否回发标记的参数名。
	 */
	public static final String BUILTIN_PARAMNAME_POSTBACK = "operationpostback";

	/**
	 * 常用参数定义：operationsourcerequesturl，表示获取触发执行操作的原始页面的url地址的参数名。
	 */
	public static final String BUILTIN_PARAMNAME_SOURCEREQUESTURL = "operationsourcerequesturl";

	private HttpServletRequest m_httpRequest = null; // 当前http请求。
	private CommonForm m_webRequestForm = null; // web提交的请求信息对象。
	private String m_operationSourceRequestUrl = null;
	private boolean m_errorFlag = false; // 是否有{@link Operation#setLastError}调用标记

	/**
	 * 缺省构造器。
	 */
	public Operation() {
	}

	/**
	 * 设置当前http请求。
	 * 
	 * @param httpRequest HttpServletRequest 当前http请求。
	 */
	public void setHttpRequest(HttpServletRequest httpRequest) {
		this.m_httpRequest = httpRequest;
	}

	/**
	 * 重载getHttpRequest:获取绑定的http请求。
	 * 
	 * <p>
	 * 如果有预先设置的HttpServletRequest则返回之，否则尝试从Session（用户自定义会话）中获取，如果取不到，则返回null。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#getHttpRequest()
	 */
	public HttpServletRequest getHttpRequest() {
		if (this.m_httpRequest != null) return this.m_httpRequest;
		this.m_httpRequest = super.getHttpRequest();
		return this.m_httpRequest;
	}

	/**
	 * 返回绑定的web提交的请求信息对象，可选。
	 * 
	 * <p>
	 * 默认为com.tansuosoft.discoverx.bll.function.CommonDispatchForm对象。
	 * </p>
	 * 
	 * @return CommonForm
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.CommonDispatchForm
	 */
	public CommonForm getWebRequestForm() {
		return this.m_webRequestForm;
	}

	/**
	 * 设置绑定的web提交的请求信息对象，可选。
	 * 
	 * @param webRequestForm CommonForm
	 */
	public void setWebRequestForm(CommonForm webRequestForm) {
		this.m_webRequestForm = webRequestForm;
	}

	/**
	 * 根据当前用户、当前操作对应功能函数配置资源名称构造一个默认{@link Log}。
	 * 
	 * <p>
	 * 构造出来的{@link Log}对象的logType为{@link LogType.Operation}。<br/>
	 * 构造出来的{@link Log}对象的subject为当前用户全名。<br/>
	 * 构造出来的{@link Log}对象的verb为当前操作对应功能函数配置资源名称。<br/>
	 * 构造出来的{@link Log}对象的owner为当前文档的UNID。
	 * </p>
	 * 
	 * @return {@link Log}
	 */
	public Log buildOperationLogFormCurrentUser() {
		Log log = new Log();
		log.setLogType(LogType.Operation);
		User u = this.getUser();
		log.setSubject(u == null ? User.ANONYMOUS_USER_ALIAS : u.getName());
		log.setVerb(this.getFunction().getName());
		Document doc = this.getDocument();
		if (doc != null) {
			log.setOwner(doc.getUNID());
		}
		return log;
	}

	/**
	 * 写入一条日志。
	 * 
	 * @param log {@link Log}
	 * 
	 * @return 成功则返回true，否则返回false。
	 */
	public boolean writeLog(Log log) {
		return LogHelper.writeLog(log);
	}

	/**
	 * 写入一条日志并在成功后同步设置指定的文档的日志。
	 * 
	 * @param log {@link Log}
	 * @param document {@link Document}
	 * 
	 * @return 成功则返回true，否则返回false。
	 */
	public boolean writeLog(Log log, Document document) {
		return LogHelper.writeLog(log, document);
	}

	/**
	 * 检查是否有配置下一步的系统功能函数（从名为“next”的参数中获取），如果有，则执行并/或返回，若没有，则返回null。
	 * 
	 * <p>
	 * 参数值必须存在且只有一个有效的操作公式表达式，表达式格式参考{@link ExpressionParser}中的说明。
	 * </p>
	 * <p>
	 * 如果有设置过执行错误信息（即调用过{@link Operation#setLastError(Exception)}），则直接返回null。
	 * </p>
	 * 
	 * @return OperationResult 如果没有下一步操作或出现异常则返回null。
	 */
	public OperationResult checkAndExecuteNext() {
		if (this.m_errorFlag) return null;
		OperationResult result = null;
		String next = this.getParameterStringFromAll(Operation.BUILTIN_PARAMNAME_NEXTOPERATION_ALIAS, null);
		if (next == null || next.length() == 0) next = this.getParameterStringFromAll(Operation.BUILTIN_PARAMNAME_NEXTOPERATION, null);
		if (next == null || next.length() == 0) return null;

		OperationEvaluator evaluator = null;
		try {
			Session session = this.getSession();
			HttpServletRequest request = this.getHttpRequest();
			HttpSession httpSession = (request == null ? null : request.getSession());
			Resource r = this.getResource();
			if (r == null) r = this.getDocument();
			evaluator = new OperationEvaluator(next, session, request, r, this);
			OperationParser parser = evaluator.getOperationParser();
			String uiPath = parser.getUiPath(null);
			Operation operation = parser.getOperation();
			// 显示操作界面
			if (uiPath != null && uiPath.trim().length() > 0 && !operation.isPostback()) {
				Session.setSessionParam(session, httpSession, Session.LASTOPERATION_PARAM_NAME_IN_HTTPSESSION, parser);
				String requestUrl = this.getSourceRequestUrl();
				operation.setHttpRequest(null);
				return new OperationResult(requestUrl);
			}
			result = evaluator.evalResult();
			if (result == null) { throw new FunctionException("无法获取“" + next + "”对应的操作的有效执行结果信息！"); }
		} catch (FunctionException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	/**
	 * 检查是否有执行结果显示地址（“resulturl”参数的值），如果有，则返回其对应的OperationResult对象。
	 * 
	 * @return {@link OperationResult} 如果“resulturl”参数没有配置或没有值，则返回null。
	 */
	public OperationResult returnResultUrl() {
		String resultUrl = this.getParameterStringFromAll(Operation.BUILTIN_PARAMNAME_RESULTURL, null);
		if (resultUrl == null || resultUrl.length() == 0) return null;
		return new OperationResult(resultUrl);
	}

	/**
	 * 获取{@link Operation#getSourceRequestUrl()}Url地址并以{@link OperationResult}的方式返回。
	 * 
	 * @return {@link OperationResult} 如果{@link Operation#getSourceRequestUrl()}返回不了有效url则返回null。
	 */
	public OperationResult returnRequestUrl() {
		return this.returnRequestUrl(null);
	}

	/**
	 * 获取{@link Operation#getSourceRequestUrl()}Url地址并以{@link OperationResult}的方式返回。
	 * 
	 * @param operationParser 可为null。<br/>
	 *          如果提供了有效的{@link OperationParser}对象那么会将此对象保存到用户自定义会话或http会话（在用户自定义会话不存在时）中。<br/>
	 *          此参数不为null时通常用于在执行有界面交互的操作时先显示操作界面。
	 * @return {@link OperationResult} 如果{@link Operation#getSourceRequestUrl()}返回不了有效url则返回null。
	 */
	public OperationResult returnRequestUrl(OperationParser operationParser) {
		String url = this.getSourceRequestUrl();
		if (url == null || url.length() == 0) return null;
		if (operationParser != null) {
			Operation operation = operationParser.getOperation();
			if (operation != null) {
				operation.setExpression(this.getExpression());
				operation.setHttpRequest(null);
			}
			HttpServletRequest request = this.getHttpRequest();
			HttpSession httpSession = (request == null ? null : request.getSession());
			Session.setSessionParam(this.getSession(), httpSession, Session.LASTOPERATION_PARAM_NAME_IN_HTTPSESSION, operationParser);
			String unid = this.getParameterStringFromAll("unid", null);
			if (url.indexOf(unid) < 0) {
				if (url.indexOf('?') > 0) {
					url += "&unid=" + unid;
				} else {
					url += "?unid=" + unid;
				}
			}
		}
		return new OperationResult(url);
	}

	/**
	 * 返回{@link UrlConfig}中配置的url地址对应的{@link OperationResult}。
	 * 
	 * @param urlConfigName {@link UrlConfig}中的url配置项对应的名称，必须。
	 * @return {@link OperationResult}，它包含{@link UrlConfig}中名为urlConfigName的参数值对应的实际url地址。
	 */
	public OperationResult returnConfigUrl(String urlConfigName) {
		if (urlConfigName == null) return null;
		String url = UrlConfig.getInstance().getValue(urlConfigName);
		return new OperationResult(url);
	}

	/**
	 * 获取重定向到指定资源路径的执行结果。
	 * 
	 * <p>
	 * 文档资源为框架目录下的document.jsp，其它资源为web应用根路径下的admin/资源目录名.jsp
	 * </p>
	 * 
	 * @param resource 要打开并显示的资源，如果为null，则返回url地址配置中名为“error_msessage”对应的url对应的OperationResult。
	 * @return {@link OperationResult}
	 */
	public OperationResult returnRedirectToResource(Resource resource) {
		OperationResult result = new OperationResult();
		String resultUrl = null;
		if (resource == null) {
			this.setLastMessage("资源未知！");
			resultUrl = UrlConfig.getInstance().getValue("error_msessage");
		} else {
			String directory = resource.getDirectory();
			String unid = resource.getUNID();
			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
			if (rd == null) {
				this.setLastMessage("资源描述未知！");
				resultUrl = UrlConfig.getInstance().getValue("error_msessage");
				result.setResultUrl(resultUrl);
				return result;
			}
			String urlBase = ((resource instanceof Document) ? "{frame}" : "~admin/");
			resultUrl = String.format("%1$s%2$s.jsp?unid=%3$s", urlBase, StringUtil.toCamelCase(directory), unid);
		}
		result.setResultUrl(resultUrl);
		return result;
	}

	/**
	 * 返回web操作最终执行结果。
	 * 
	 * <p>
	 * 依次执行并获取{@link Operation#checkAndExecuteNext()}、{@link Operation#returnResultUrl()}、{@link Operation#returnRedirectToResource(Resource)}
	 * （如果resource不为null的话）的返回结果，如果它们其中有一个的返回值不为null，则返回之，否则返回defaultResult。
	 * </p>
	 * 
	 * @param message String 消息内容，可以为null。
	 * @param resource Resource 当前资源，可以为null。
	 * @param defaultResult 默认返回的结果对象实例，可以为null。
	 * @return {@link OperationResult}
	 */
	public OperationResult returnResult(String message, Resource resource, OperationResult defaultResult) {
		if (message != null) this.setLastMessage(message);
		OperationResult ret = null;
		ret = this.checkAndExecuteNext();
		if (ret != null) return ret;
		ret = this.returnResultUrl();
		if (ret != null) return ret;
		if (resource != null) ret = this.returnRedirectToResource(resource);
		if (ret != null) return ret;

		return defaultResult;
	}

	/**
	 * 设置返回给web请求的消息。
	 * 
	 * <p>
	 * 如果存在{@link Session}，那么保存到其中，否则保存到{@link javax.servlet.http.HttpSession}中名为“{@link Session#LASTAJAXRESPONSE_PARAM_NAME_IN_HTTPSESSION}”的参数中。
	 * </p>
	 * 
	 * @param message String 消息内容，可以为null。
	 */
	public void setLastMessage(String message) {
		HttpServletRequest request = this.getHttpRequest();
		HttpSession httpSession = (request == null ? null : request.getSession());
		Session session = this.getSession();
		Session.setSessionParam(session, httpSession, Session.LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION, message);
	}

	/**
	 * 设置返回给web请求的异常({@link Exception})。
	 * 
	 * <p>
	 * 如果存在{@link Session}，那么保存到其中，否则保存到{@link javax.servlet.http.HttpSession}中名为“{@link Session#LASTERROR_PARAM_NAME_IN_HTTPSESSION}”的参数中。
	 * </p>
	 * 
	 * @param ex Exception
	 */
	public void setLastError(Exception ex) {
		HttpServletRequest request = this.getHttpRequest();
		HttpSession httpSession = (request == null ? null : request.getSession());
		Session session = this.getSession();
		Session.setSessionParam(session, httpSession, Session.LASTERROR_PARAM_NAME_IN_HTTPSESSION, ex);
		this.m_errorFlag = (ex != null);
	}

	/**
	 * 设置返回给web请求的{@link AJAXResponse}对象。
	 * 
	 * <p>
	 * 如果存在{@link Session}，那么保存到其中，否则保存到{@link javax.servlet.http.HttpSession}中名为“{@link Session#LASTAJAXRESPONSE_PARAM_NAME_IN_HTTPSESSION}”的参数中。
	 * </p>
	 * 
	 * @param ajax
	 */
	public void setLastAJAXResponse(AJAXResponse ajax) {
		HttpServletRequest request = this.getHttpRequest();
		HttpSession httpSession = (request == null ? null : request.getSession());
		Session session = this.getSession();
		Session.setSessionParam(session, httpSession, Session.LASTAJAXRESPONSE_PARAM_NAME_IN_HTTPSESSION, ajax);
	}

	/**
	 * 设置返回给web请求的操作结果({@link OperationResult})信息。
	 * 
	 * <p>
	 * 如果存在{@link Session}，那么保存到其中，否则保存到{@link javax.servlet.http.HttpSession}中名为“{@link Session#LASTOPERATIONRESULT_PARAM_NAME_IN_HTTPSESSION}”的参数中。
	 * </p>
	 * 
	 * @param operationResult OperationResult
	 */
	public void setLastOperationResult(OperationResult operationResult) {
		HttpServletRequest request = this.getHttpRequest();
		HttpSession httpSession = (request == null ? null : request.getSession());
		Session session = this.getSession();
		Session.setSessionParam(session, httpSession, Session.LASTOPERATIONRESULT_PARAM_NAME_IN_HTTPSESSION, operationResult);
	}

	/**
	 * 把最近处理结果返回的资源设置到用户自定义会话或http会话中（在用户自定义会话为null时）。
	 * 
	 * <p>
	 * 如果存在{@link Session}，那么保存到其中，否则保存到{@link javax.servlet.http.HttpSession}中名为“{@link Session#LASTRESOURCE_PARAM_NAME_IN_HTTPSESSION}”的参数中。
	 * </p>
	 * <p>
	 * 一般是需要向前端web页面返回资源时使用。
	 * </p>
	 * 
	 * @param resource Resource
	 */
	public void setLastResource(Resource resource) {
		if (resource != null && resource instanceof Document) {
			this.setLastDocument((Document) resource);
			// return;
		}
		HttpServletRequest request = this.getHttpRequest();
		HttpSession httpSession = (request == null ? null : request.getSession());
		Session session = this.getSession();
		Session.setSessionParam(session, httpSession, Session.LASTRESOURCE_PARAM_NAME_IN_HTTPSESSION, resource);
	}// func end

	/**
	 * 把最近处理结果返回的文档资源设置到用户自定义会话或http会话中（在用户自定义会话为null时）。
	 * 
	 * <p>
	 * 如果存在{@link Session}，那么保存到其中，否则保存到{@link javax.servlet.http.HttpSession}中名为“{@link Session#LASTDOCUMENT_PARAM_NAME_IN_HTTPSESSION}”的参数中。
	 * </p>
	 * <p>
	 * 一般是需要向前端web页面返回文档资源时使用。
	 * </p>
	 * 
	 * @param document Document
	 */
	public void setLastDocument(Document document) {
		HttpServletRequest request = this.getHttpRequest();
		HttpSession httpSession = (request == null ? null : request.getSession());
		Session session = this.getSession();
		Session.setSessionParam(session, httpSession, Session.LASTDOCUMENT_PARAM_NAME_IN_HTTPSESSION, document);
	}// func end

	/**
	 * 从所有渠道获取参数文本值，如果存在（不为null或空字符串)则返回，否则返回defaultValue。
	 * 
	 * <p>
	 * 获取参数值的顺序：先从http请求参数中，然后从配置的参数中取。
	 * </p>
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return String
	 */
	public String getParameterStringFromAll(String paramName, String defaultValue) {
		HttpServletRequest request = this.getHttpRequest();
		if (request != null) {
			try {
				String result = request.getParameter(paramName);
				if (result != null && result.length() > 0) return result;
			} finally {
			}
		}
		return super.getParameterString(paramName, defaultValue);
	}

	/**
	 * 从所有渠道获取参数int结果，如果存在则返回，否则返回defaultValue。
	 * 
	 * <p>
	 * 获取参数值的顺序：先从http请求参数中，然后从配置的参数中取。
	 * </p>
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return int
	 */
	public int getParameterIntFromAll(String paramName, int defaultValue) {
		String str = this.getParameterStringFromAll(paramName, null);
		return StringUtil.getValueInt(str, defaultValue);
	}

	/**
	 * 从所有渠道获取参数long结果，如果存在则返回，否则返回defaultValue。
	 * 
	 * <p>
	 * 获取参数值的顺序：先从http请求参数中，然后从配置的参数中取。
	 * </p>
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return long
	 */
	public long getParameterLongFromAll(String paramName, long defaultValue) {
		String str = this.getParameterStringFromAll(paramName, null);
		return StringUtil.getValueLong(str, defaultValue);
	}

	/**
	 * 从所有渠道获取参数float结果，如果存在则返回，否则返回defaultValue。
	 * 
	 * <p>
	 * 获取参数值的顺序：先从http请求参数中，然后从配置的参数中取。
	 * </p>
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return float
	 */
	public float getParameterFloatFromAll(String paramName, float defaultValue) {
		String str = this.getParameterStringFromAll(paramName, null);
		return StringUtil.getValueFloat(str, defaultValue);
	}

	/**
	 * 从所有渠道获取参数bool结果，如果存在则返回，否则返回defaultValue。
	 * 
	 * <p>
	 * 获取参数值的顺序：先从http请求参数中，然后从配置的参数中取。
	 * </p>
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return boolean
	 */
	public boolean getParameterBoolFromAll(String paramName, boolean defaultValue) {
		String str = this.getParameterStringFromAll(paramName, null);
		return StringUtil.getValueBool(str, defaultValue);
	}

	/**
	 * 从所有渠道获取参数Enum结果，如果存在则返回，否则返回defaultValue。
	 * 
	 * <p>
	 * 获取参数值的顺序：先从http请求参数中，然后从配置的参数中取。
	 * </p>
	 * 
	 * @param <T>
	 * @param paramName
	 * @param enumCls
	 * @param defaultValue
	 * @return
	 */
	public <T> T getParameterEnumFromAll(String paramName, Class<T> enumCls, T defaultValue) {
		String str = this.getParameterStringFromAll(paramName, null);
		return StringUtil.getValueEnum(str, enumCls, defaultValue);
	}

	/**
	 * 从所有渠道获取指定名称的参数值作为全限定类名值并调用其默认构造器构造为对应实例的结果。
	 * 
	 * <p>
	 * 获取参数值的顺序：先从http请求参数中，然后从配置的参数中取。
	 * </p>
	 * 
	 * @param <T>
	 * @param paramName
	 * @param clazz
	 * @param defaultReturn
	 * @return 如果没有配置值则返回defaultReturn。
	 */
	public <T> T getParameterValueAsObjectFromAll(String paramName, Class<T> clazz, T defaultReturn) {
		String impl = this.getParameterStringFromAll(paramName, null);
		if (impl == null || impl.length() == 0) return defaultReturn;
		T result = Instance.newInstance(impl, clazz);
		if (result != null) return result;
		return defaultReturn;
	}

	/**
	 * 返回是否回发标记。
	 * 
	 * <p>
	 * 如果是回发，则不显示操作界面，直接执行操作功能。
	 * </p>
	 * 
	 * @return
	 */
	public boolean isPostback() {
		return this.getParameterBoolFromAll(BUILTIN_PARAMNAME_POSTBACK, false);
	}

	/**
	 * 获取触发执行操作的原始页面的url地址。
	 * 
	 * <p>
	 * 先从名为{@link Operation#BUILTIN_PARAMNAME_SOURCEREQUESTURL}的参数中获取，如果找不到则从http请求中名为“Referer”的头信息中获取，如果都去不到则返回null或空字符串。
	 * </p>
	 * 
	 * @return
	 */
	public String getSourceRequestUrl() {
		if (m_operationSourceRequestUrl != null && m_operationSourceRequestUrl.length() > 0) return m_operationSourceRequestUrl;
		m_operationSourceRequestUrl = this.getParameterStringFromAll(BUILTIN_PARAMNAME_SOURCEREQUESTURL, null);
		if (m_operationSourceRequestUrl == null || m_operationSourceRequestUrl.length() == 0) {
			HttpServletRequest request = this.getHttpRequest();
			m_operationSourceRequestUrl = (request == null ? null : StringUtil.getQueryString(request.getHeader("Referer")));
		}
		return m_operationSourceRequestUrl;
	}
}// class end

