/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import com.tansuosoft.discoverx.common.UrlConfig;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.OperationResult;

/**
 * 表示不执行任何功能的系统功能函数调用实现类。
 * 
 * <p>
 * 一般用于只做前端处理（如调用前端js函数）的操作。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class BlankOperation extends Operation {
	/**
	 * 缺省构造器。
	 */
	public BlankOperation() {
	}

	/**
	 * 重载call。
	 * 
	 * <p>
	 * 直接返回请求地址对应的OperationResult对象，如果找不到，则返回错误信息。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	@Override
	public Object call() throws FunctionException {
		OperationResult result = this.returnRequestUrl();
		if (result != null) return result;
		return this.returnConfigUrl(UrlConfig.URLCFGNAME_ERROR_MESSAGE);
	}
}

