/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import com.tansuosoft.discoverx.model.ParameterValueDataType;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 参数值解析结果类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ParsedParameter {
	private String m_name = null;
	private String m_rawValue = null;
	private ParameterValueDataType m_dt = null;

	/**
	 * 缺省构造器。
	 */
	protected ParsedParameter() {
	}

	/**
	 * 接收原始参数名和参数值的构造器。
	 * 
	 * @param name
	 * @param rawValue
	 */
	public ParsedParameter(String name, String rawValue) {
		this.m_name = name;
		this.m_rawValue = rawValue;
	}

	/**
	 * 接收原始参数名、参数值、参数值数据类型的构造器。
	 * 
	 * @param name
	 * @param rawValue
	 * @param dt
	 */
	public ParsedParameter(String name, String rawValue, ParameterValueDataType dt) {
		this(name, rawValue);
		this.m_dt = dt;
	}

	/**
	 * 获取参数名。
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 获取原始参数值。
	 * 
	 * <p>
	 * 原始参数值是指从表达式中解析出来的没有经过处理的参数值，比如对于字符串，还包括起始的引号信息。
	 * </p>
	 * 
	 * @return
	 */
	public String getRawValue() {
		return this.m_rawValue;
	}

	/**
	 * 获取参数值数据类型。
	 * 
	 * @return
	 */
	public ParameterValueDataType getValueDataType() {
		if (m_dt == null) {
			String v = this.m_rawValue;
			v = v.trim();
			if (v.equalsIgnoreCase("true") || v.equalsIgnoreCase("false")) {
				m_dt = ParameterValueDataType.Boolean;
			} else if (StringUtil.isNumber(v)) {
				m_dt = ParameterValueDataType.Int;
			} else if (StringUtil.isFloat(v)) {
				m_dt = ParameterValueDataType.Float;
			}
		}
		return m_dt;
	}

	/**
	 * 获取转换为指定类型后的参数值。
	 * 
	 * <p>
	 * 按以下规则转换参数值并返回：
	 * <ul>
	 * <li>整数类型：返回原始参数值转换为int类型的结果。</li>
	 * <li>浮点类型：返回原始参数值转换为float类型的结果。</li>
	 * <li>布尔类型：返回原始参数值转换为boolean类型的结果。</li>
	 * <li>表达式类型：返回原始参数值。</li>
	 * <li>文本类型或其它未说明的类型：原始参数值去掉开始和结束引号。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return Object 指定类型的参数值。
	 */
	public Object getValue() {
		String vx = this.m_rawValue.trim();
		switch (this.getValueDataType().getIntValue()) {
		case 1: // Text
			if (vx.length() == 2) {
				return "";
			} else {
				return vx.substring(1, vx.length() - 1).replace("\\\"", "\"");
			}
		case 5: // Int
			return StringUtil.getValueInt(vx, 0);
		case 6: // Float
			return StringUtil.getValueFloat(vx, 0.0f);
		case 8: // Bool
			return StringUtil.getValueBool(vx, false);
		case 9: // Expression
			return this.m_rawValue;
		}
		return null;
	}
}

