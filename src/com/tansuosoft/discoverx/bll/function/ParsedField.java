/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

/**
 * 表示解析后的字段变量信息的类。
 * 
 * <p>
 * 即表达式中包含的字段变量的解析结果。<br/>
 * 表达式中的字段变量（运行时转换为文档资源的字段值）表示方式为：${字段名}，如：“$fld_subject”。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ParsedField extends ParsedVariable {

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param variable String
	 * @param range VariableRange
	 */
	protected ParsedField(String variable, VariableRange range) {
		super(variable, range);
	}

	/**
	 * 获取字段变量中去掉变量指示前缀后的字段名。
	 * 
	 * <p>
	 * 如$fld_subject为变量体，则返回fld_subject，如果变量体为null、空串、不包含前缀，则返回null。
	 * </p>
	 * 
	 * @return String
	 */
	public String getFieldName() {
		String v = this.getVariable();
		if (v == null || v.length() == 0 || v.charAt(0) != FIELD_VAR_PREFIX) return null;
		return v.substring(1);
	}

}

