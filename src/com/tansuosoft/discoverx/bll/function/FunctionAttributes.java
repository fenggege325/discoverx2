/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 表示提供系统功能函数缺省配置属性信息的注释类。
 * 
 * <p>
 * 新添加的系统功能函数可以在其类定义前添加此注释，如：<br/>
 * 
 * <pre>
 * &#64;FunctionAttributes(type = 0, alias = "test", name = "测试文档操作", desc = "测试文档操作的说明" icon="test.gif")
 * public class Test extends Operation {
 * ...
 * }
 * </pre>
 * 
 * 这样在用户配置Test对应的功能函数资源时，只要将实现类全名配置好后，系统会自动设置名称、别名、类型、返回值类型、说明等属性。<br/>
 * 注意：此注释中定义的属性值都是第一次配置此函数对应的资源时使用的缺省值，具体函数资源配置的最终属性值可以不一样。
 * </p>
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FunctionAttributes {
	/**
	 * 缺省类型，包括：0、1、2、3分别表示“文档操作”、“其它操作”、“系统公式”、“通用操作”等，默认为0表示“文档操作”。
	 * 
	 * @return int
	 */
	int type() default 0;

	/**
	 * 缺省函数别名，默认为空字符串。
	 * 
	 * @return String
	 */
	String alias() default "";

	/**
	 * 缺省函数名称，默认为空字符串。
	 * 
	 * @return String
	 */
	String name() default "";

	/**
	 * 缺省函数说明，默认为空字符串。
	 * 
	 * @return String
	 */
	String desc() default "";

	/**
	 * 缺省结果类，默认为{@link com.tansuosoft.discoverx.model.OperationResult}（表示操作的返回结果）。
	 * 
	 * @return String
	 */
	String result() default "com.tansuosoft.discoverx.model.OperationResult";

	/**
	 * 缺省图标，默认为空字符串。
	 * 
	 * @return String
	 */
	String icon() default "";
}

