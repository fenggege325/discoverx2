/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;

import com.tansuosoft.discoverx.common.exception.ExpressionEvaluatorException;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 计算表达式最终结果的类。
 * 
 * <p>
 * 此类一般用于获取表达式计算的最终常数值结果。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ExpressionEvaluator extends Evaluator {
	private Object m_objectValue = null;
	private String m_stringValue = null;
	private int m_intValue = 0;
	private boolean m_intValueFlag = false;
	private long m_longValue = 0;
	private boolean m_longValueFlag = false;
	private float m_floatValue = 0.0F;
	private boolean m_floatValueFlag = false;
	private double m_doubleValue = 0.0D;
	private boolean m_doubleValueFlag = false;
	private boolean m_booleanValue = false;
	private boolean m_booleanValueFlag = false;

	private String m_jstypename = null; // 表达式结果对应的js数据类型，如“string”、“number”、“boolean”等。

	private String m_expression = null;
	private Session m_session = null;
	private Resource m_resource = null;

	protected static final String JST_STRING = "string";
	protected static final String JST_BOOLEAN = "boolean";
	protected static final String JST_NUMBER = "number";
	protected static final String JST_UNDEFINED = "undefined";
	protected static final String JST_OBJECT = "object";
	protected static final String NULL = "null";
	protected static final String TRUE = "true";
	protected static final String FALSE = "false";

	/**
	 * 接收表达式、自定义会话、指定资源的构造器。
	 * 
	 * @param expression String 表达式，必须。
	 * @param session Session 自定义会话，可以为null。
	 * @param resource Resource 和表达式上下文绑定的资源，可以为null。
	 */
	public ExpressionEvaluator(String expression, Session session, Resource resource) {
		this(expression, session, resource, null);
	}

	/**
	 * 接收表达式、自定义会话、已计算表达式等的构造器。
	 * 
	 * @param expression String 表达式，必须。
	 * @param session Session 自定义会话，可以为null。
	 * @param resource Resource 和表达式上下文绑定的资源，如果表达式包含字段型变量，则必须提供，否则可以为null。
	 * @param evaluated Map&lt;String, Evaluator&gt; 已经计算完的表达式字符串和对应的Evaluator一一对应的Map，可以为null。
	 */
	protected ExpressionEvaluator(String expression, Session session, Resource resource, Map<String, Evaluator> evaluated) {
		this.m_expression = expression;
		this.m_session = session;
		this.m_resource = resource;
		this.setEvaluated(evaluated);
		StringBuilder sb = new StringBuilder(this.m_expression);
		// 计算表达式结果
		try {
			ExpressionParser parser = new ExpressionParser(this.m_expression);
			List<ParsedVariable> list = parser.parseVariables();
			Object evalResult = null;
			boolean singleVFlag = false;
			if (list != null && !list.isEmpty()) {
				for (ParsedVariable x : list) {
					// 无效表达式或已经计算过
					if (x == null || this.checkEvaluated(x.getVariable()) != null) continue;
					if (x instanceof ParsedField) {
						ParsedField parsedField = (ParsedField) x;
						Evaluator evaluator = new FieldVariableEvaluator(parsedField, this.m_session, this.m_resource);
						setEvaluated(x.getVariable(), evaluator);
					} else if (x instanceof ParsedFormula) {
						ParsedFormula parsedFormula = (ParsedFormula) x;
						Evaluator evaluator = new FormulaVariableEvaluator(parsedFormula, this.m_session, this.m_resource, this.getEvaluated());
						setEvaluated(x.getVariable(), evaluator);
					}
				}
				int vCount = 0; // 有效的表达式个数
				int lastStart = 0;
				int lastEnd = 0;
				for (int i = (list.size() - 1); i >= 0; i--) {
					ParsedVariable x = list.get(i);
					if (x == null) continue;
					VariableRange vr = x.getVariableRange();
					int start = vr.getStart();
					int end = vr.getEnd();
					if (start > lastStart && end < lastEnd) continue; // 作为参数的表达式不替换，只替换整体表达式。
					String variable = x.getVariable();
					Evaluator evaluator = this.checkEvaluated(variable);
					if (evaluator == null) throw new RuntimeException("无法获取“" + variable + "”变量的结果");
					EvaluatorReturnType rt = evaluator.getReturnType();
					if (rt == null) throw new RuntimeException("无法获取“" + variable + "”变量的结果类型");
					evalResult = evaluator.evalObject();
					String result = null;
					if (rt == EvaluatorReturnType.String || rt == EvaluatorReturnType.Object) {
						result = (evalResult == null ? "null" : String.format("\"%1$s\"", evalResult.toString().replace("\\", "\\\\").replace("\"", "\\\"").replace("\r\n", "\\r\\n").replace("\r\n", "\\r\\n").replace("\n", "\\n")));
					} else if (rt == EvaluatorReturnType.Boolean) {
						result = (evalResult == null ? "false" : (evaluator.evalBoolean() ? "true" : "false"));
					} else {
						result = (evalResult == null ? "0" : evalResult.toString());
					}
					sb.replace(start, end, result);
					vCount++;
					lastStart = start;
					lastEnd = end;
				}
				if (vCount == 1 && lastStart == 0 && lastEnd == expression.length()) singleVFlag = true;
			}
			if (singleVFlag) { // 只有一个有效表达式且无运算符不用js来计算
				this.clearReturnType();
				this.setReturnTypeFlag(EvaluatorReturnType.Object);
				this.m_objectValue = evalResult;
			} else {
				String exp = sb.toString().trim();
				boolean isnum = StringUtil.isNumber(exp);
				boolean isf = (isnum ? false : StringUtil.isFloat(exp));
				if (exp.equalsIgnoreCase(TRUE) || exp.equalsIgnoreCase(FALSE) || isnum || isf) { // 单独布尔和数字常量直接获取
					this.m_objectValue = exp;
					this.m_jstypename = (isnum || isf ? JST_NUMBER : JST_BOOLEAN);
				} else {
					final char sep = ';';
					sb.insert(0, "(function(){try{\r\n var result=(");
					sb.append(");\r\nreturn typeof(result)+'").append(sep).append("'+result;}catch(e){print(e);}})();");
					ScriptEngine se = ScriptEngineProvider.getInstance().getScriptEngine();
					exp = sb.toString();
					Object r = se.eval(exp);
					if (r == null) throw new RuntimeException("无法获取表达式返回结果");
					String str = r.toString();
					int firstSemicolonPos = str.indexOf(sep);
					m_jstypename = str.substring(0, firstSemicolonPos);
					m_objectValue = str.substring(firstSemicolonPos + 1);
				}
				if (m_objectValue == null && StringUtil.isBlank(this.m_jstypename)) FileLogger.debug("表达式“%1$s=>%2$s”执行时没有返回有效结果！", expression, sb.toString());
				this.setReturnTypeFlag(EvaluatorReturnType.Object);
				this.clearReturnType();
				if (JST_STRING.equalsIgnoreCase(this.m_jstypename)) {
					this.setReturnTypeFlag(EvaluatorReturnType.String);
				} else if (JST_NUMBER.equalsIgnoreCase(this.m_jstypename)) {
					this.setReturnTypeFlag(EvaluatorReturnType.Double);
					String s = null;
					if (m_objectValue != null && (s = (m_objectValue.toString() + "")).endsWith(".0")) m_objectValue = s.substring(0, s.length() - 2); // 防止返回x.0格式的
					this.evalDouble();
				} else if (JST_BOOLEAN.equalsIgnoreCase(this.m_jstypename)) {
					this.setReturnTypeFlag(EvaluatorReturnType.Boolean);
					m_objectValue = this.evalBoolean();
				} else {
					this.m_objectValue = "";
				}
			}// else end
		} catch (Exception ex) {
			FileLogger.debug("计算表达式“%1$s=>%2$s”的结果时发生错误：%3$s！", expression, sb.toString(), ex.getMessage());
			if (!(ex instanceof RuntimeException)) FileLogger.error(ex);
		}
	}

	/**
	 * 重载evalBoolean
	 * 
	 * @throws ExpressionEvaluatorException
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalBoolean()
	 */
	@Override
	public boolean evalBoolean() {
		if (!m_booleanValueFlag) {
			m_booleanValueFlag = true;
			if (this.m_objectValue == null && this.m_jstypename == null) {
				m_booleanValue = false;
			} else {
				this.m_booleanValue = (m_objectValue == null ? false : StringUtil.getValueBool(this.m_objectValue.toString(), false));
			}
		}
		return this.m_booleanValue;
	}

	/**
	 * 重载evalString
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalString()
	 */
	@Override
	public String evalString() {
		if (this.m_stringValue == null) this.m_stringValue = (m_objectValue != null ? m_objectValue.toString() : null);
		return this.m_stringValue;
	}

	/**
	 * 重载evalInt
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalInt()
	 */
	@Override
	public int evalInt() {
		if (!this.m_intValueFlag) {
			m_intValueFlag = true;
			if (this.m_objectValue == null && this.m_jstypename == null) {
				m_intValue = 0;
			} else {
				double d = this.evalDouble();
				this.m_intValue = (int) d;
			}
		}
		return this.m_intValue;
	}

	/**
	 * 重载evalLong
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalLong()
	 */
	@Override
	public long evalLong() {
		if (!this.m_longValueFlag) {
			m_longValueFlag = true;
			if (this.m_objectValue == null && this.m_jstypename == null) {
				m_longValue = 0L;
			} else {
				double d = this.evalDouble();
				this.m_longValue = (long) d;
			}
		}
		return this.m_longValue;
	}

	/**
	 * 重载evalFloat
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalFloat()
	 */
	@Override
	public float evalFloat() {
		if (!this.m_floatValueFlag) {
			m_floatValueFlag = true;
			if (this.m_objectValue == null && this.m_jstypename == null) {
				m_floatValue = 0.0F;
			} else {
				double d = this.evalDouble();
				this.m_floatValue = (float) d;
			}
		}
		return this.m_floatValue;
	}

	/**
	 * 重载evalDouble
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalDouble()
	 */
	@Override
	public double evalDouble() {
		if (!this.m_doubleValueFlag) {
			m_doubleValueFlag = true;
			if (this.m_objectValue == null && this.m_jstypename == null) {
				m_doubleValue = 0.0D;
			} else {
				this.m_doubleValue = (m_objectValue == null ? 0.0D : StringUtil.getValueDouble(this.m_objectValue.toString(), 0.0D));
			}
		}
		return this.m_doubleValue;
	}

	/**
	 * 重载evalObject
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalObject()
	 */
	@Override
	public Object evalObject() {
		return m_objectValue;
	}

	/**
	 * 重载evalEnum：未实现，总是返回null。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalEnum()
	 */
	@Override
	public Enum<?> evalEnum() {
		return null;
	}

}

