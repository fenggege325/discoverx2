/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

/**
 * 描述解析后的某个变量在整个表达式中的位置信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class VariableRange implements Comparable<VariableRange> {
	/**
	 * 缺省构造器。
	 */
	protected VariableRange() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param start
	 * @param end
	 */
	protected VariableRange(int start, int end) {
		this.m_start = start;
		this.m_end = end;
	}

	private int m_start = -1; // 表达式在整体表达式中的开始位置。
	private int m_end = -1; // 表达式在整体表达式中的结束位置。
	private boolean m_atomic = true; // 此表达式是否为原子级表达式。
	private boolean m_independent = true; // 此表达式是否为独立表达式。

	/**
	 * 返回表达式在整个表达式中的开始位置。
	 * 
	 * @return int
	 */
	public int getStart() {
		return this.m_start;
	}

	/**
	 * 设置表达式在整个表达式中的开始位置。
	 * 
	 * @param start int
	 */
	protected void setStart(int start) {
		this.m_start = start;
	}

	/**
	 * 返回表达式在整体表达式中的结束位置。
	 * 
	 * @return int
	 */
	public int getEnd() {
		return this.m_end;
	}

	/**
	 * 设置表达式在整体表达式中的结束位置。
	 * 
	 * @param end int
	 */
	protected void setEnd(int end) {
		this.m_end = end;
	}

	/**
	 * 返回此表达式是否为原子级表达式。
	 * 
	 * <p>
	 * 原子级表达式即不能再分割的最基本的表达式。<br/>
	 * 如“@Username(format:="cn")”、“$fld_subject”等为原子级别表达式，如“@F1(p1:=$fld_subject,p2:=@F2())”则不是，因为它的参数中包含下级表达式。<br/>
	 * 默认为true（表示原子级的表达式）。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getAtomic() {
		return this.m_atomic;
	}

	/**
	 * 设置此表达式是否为原子级表达式。
	 * 
	 * @see VariableRange#getContainer()
	 * @param atomic boolean
	 */
	protected void setAtomic(boolean atomic) {
		this.m_atomic = atomic;
	}

	/**
	 * 返回此表达式是否为独立表达式。
	 * 
	 * <p>
	 * 默认为true，表示此表达式为独立表达式，非独立表达式一般是作为其它表达式的参数出现的。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getIndependent() {
		return this.m_independent;
	}

	/**
	 * 设置此表达式是否为独立表达式。
	 * 
	 * @see VariableRange#getIndependent()
	 * @param independent boolean
	 */
	protected void setIndependent(boolean independent) {
		this.m_independent = independent;
	}

	/**
	 * 获取表达式长度。
	 * 
	 * @return
	 */
	public int getLength() {
		return this.m_end - this.m_start;
	}

	/**
	 * 实现compareTo方法：使用{@link VariableRangeComparator}比较并返回结果。
	 */
	@Override
	public int compareTo(VariableRange o) {
		return new VariableRangeComparator().compare(this, o);
	}

	/**
	 * 输出字符串形式。格式:[start,end]
	 */
	@Override
	public String toString() {
		return "[" + m_start + "," + m_end + "]";
	}

}

