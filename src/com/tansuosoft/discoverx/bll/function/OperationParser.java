/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.List;

import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.ParameterValueDataType;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 解析操作类型的系统功能函数以获取相关信息的类。
 * 
 * @see Operation
 * @author coca@tansuosoft.cn
 */
public class OperationParser {
	private com.tansuosoft.discoverx.bll.function.Operation m_callable = null; // 被解析的功能扩展
	private Invoker m_invoker = null;
	private ExpressionParser m_expressionParser = null;
	private Session session = null;

	/**
	 * 接收操作表达式或操作对应功能函数资源UNID或操作实现类和用户自定义会话的构造器。
	 * 
	 * @param operation
	 * @param s
	 */
	public OperationParser(String operation, Session s) {
		if (operation == null) throw new RuntimeException("没有提供有效UNID或表达式！");
		session = s;
		if (operation.startsWith("@")) { // 传入的是操作表达
			m_expressionParser = new ExpressionParser(operation);
			List<ParsedVariable> vars = m_expressionParser.parseVariables();
			if (vars == null || vars.isEmpty()) throw new RuntimeException("无法解析“" + operation + "”表达式！");
			if (m_expressionParser.getIndependentCount() != 1) throw new RuntimeException("操作表达式只能且必须包含一个操作函数！");
			ParsedVariable parsedVariable = vars.get(0);
			if (parsedVariable == null || !(parsedVariable instanceof ParsedFormula)) throw new RuntimeException("无法获取到解析后的操作表达式信息！");
			ParsedFormula parsedFormula = (ParsedFormula) parsedVariable;
			String functionAlias = parsedFormula.getFunctionAlias();
			if (functionAlias == null || functionAlias.length() == 0) throw new RuntimeException("无法获取“" + operation + "”对应的操作别名！");
			String function = FunctionHelper.getInstance().getFunctionUNIDByAlias(functionAlias);
			if (function == null || function.length() == 0) function = functionAlias;
			try {
				m_invoker = new Invoker(function);
				// 设置通过函数体传入的参数值，函数资源中预定义的参数在Invoker中先设置
				List<ParsedParameter> params = parsedFormula.getParameters();
				if (params != null && params.size() > 0) {
					for (ParsedParameter y : params) {
						if (y == null) continue;
						if (y.getValueDataType() != ParameterValueDataType.Expression) {
							m_invoker.setParameter(y.getName(), y.getValue());
						} else {
							Evaluator evaluator = new ExpressionEvaluator(y.getRawValue(), session, null, null);
							m_invoker.setParameter(y.getName(), evaluator.evalObject());
						}
					}// for end
				}// if end
			} catch (Exception ex) {
				m_invoker = null;
				FileLogger.error(ex);
			}
		} else { // 传入的是UNID或具体功能函数实现类
			try {
				m_invoker = new Invoker(operation);
			} catch (FunctionException ex) {
				m_invoker = null;
				FileLogger.error(ex);
			}
		}
		if (m_invoker == null) throw new RuntimeException("无法解析并初始化操作：" + operation + "。");
		this.m_callable = m_invoker.asOperatioin();
		if (m_callable == null) throw new RuntimeException("无法获取操作实现对象。");
	}

	/**
	 * 接收一个系统功能函数资源并构造一个虚拟操作({@link Operation})的构造器。
	 * 
	 * @param function {@link com.tansuosoft.discoverx.model.Function}
	 */
	public OperationParser(com.tansuosoft.discoverx.model.Function function) {
		try {
			m_invoker = new Invoker(function);
		} catch (FunctionException ex) {
			throw new RuntimeException(ex.getMessage());
		}
		this.m_callable = new Operation() {
			@Override
			public Object call() throws FunctionException {
				return null;
			}
		};
		this.m_callable.setFunction(function);
		m_invoker.setConcrete(m_callable);
	}

	/**
	 * 获取可供调用的表达式/UNID对应操作的{@link Invoker}对象。
	 * 
	 * @return
	 */
	public Invoker getInvoker() {
		return this.m_invoker;
	}

	/**
	 * 获取操作对应的“{@link com.tansuosoft.discoverx.bll.function.Operation}”对象。
	 * 
	 * @return {@link com.tansuosoft.discoverx.bll.function.Operation}
	 */
	public com.tansuosoft.discoverx.bll.function.Operation getOperation() {
		return this.m_callable;
	}

	/**
	 * 获取功能函数绑定的“{@link com.tansuosoft.discoverx.model.Function}”资源。
	 * 
	 * @return {@link com.tansuosoft.discoverx.model.Function}
	 */
	public com.tansuosoft.discoverx.model.Function getFunction() {
		return this.m_callable.getFunction();
	}

	/**
	 * 获取绑定的JS函数信息，如果找不到择返回defaultIfNotFound。
	 * 
	 * @param defaultIfNotFound String
	 * @return String
	 */
	public String getJSFunction(String defaultIfNotFound) {
		String ret = this.m_callable.getParameterString(Operation.BUILTIN_PARAMNAME_JSFUNCTION, null);
		if (ret == null) ret = this.m_callable.getParameterString(Operation.BUILTIN_PARAMNAME_JSFUNCTION_ALIAS, defaultIfNotFound);
		com.tansuosoft.discoverx.model.Function f = this.getFunction();
		if (ret == null && f != null) ret = f.getParamValueString(Operation.BUILTIN_PARAMNAME_JSFUNCTION, null);
		if (ret == null && f != null) ret = f.getParamValueString(Operation.BUILTIN_PARAMNAME_JSFUNCTION_ALIAS, defaultIfNotFound);
		return ret;
	}

	/**
	 * 获取绑定的下一步操作表达式信息，如果找不到择返回defaultIfNotFound。
	 * 
	 * @param defaultIfNotFound String
	 * @return String
	 */
	public String getNextOperation(String defaultIfNotFound) {
		String ret = this.m_callable.getParameterString(Operation.BUILTIN_PARAMNAME_NEXTOPERATION, null);
		if (ret == null) this.m_callable.getParameterString(Operation.BUILTIN_PARAMNAME_NEXTOPERATION_ALIAS, defaultIfNotFound);
		com.tansuosoft.discoverx.model.Function f = this.getFunction();
		if (ret == null && f != null) ret = f.getParamValueString(Operation.BUILTIN_PARAMNAME_NEXTOPERATION, null);
		if (ret == null && f != null) ret = f.getParamValueString(Operation.BUILTIN_PARAMNAME_NEXTOPERATION_ALIAS, defaultIfNotFound);
		return ret;
	}

	/**
	 * 获取绑定的结果Url地址信息，如果找不到择返回defaultIfNotFound。
	 * 
	 * @param defaultIfNotFound String
	 * @return String
	 */
	public String getResultUrl(String defaultIfNotFound) {
		return this.m_callable.getParameterString(Operation.BUILTIN_PARAMNAME_RESULTURL, defaultIfNotFound);
	}

	/**
	 * 获取绑定的操作向导Url地址信息，如果找不到择返回defaultIfNotFound。
	 * 
	 * @param defaultIfNotFound String
	 * @return String
	 */
	public String getUiPath(String defaultIfNotFound) {
		return this.m_callable.getParameterString(Operation.BUILTIN_PARAMNAME_UIPATH, defaultIfNotFound);
	}

	/**
	 * 获取绑定的WebRequestForm实现类全限定名称信息，如果找不到择返回defaultIfNotFound。
	 * 
	 * @param defaultIfNotFound String
	 * @return String
	 */
	public String getWebRequestForm(String defaultIfNotFound) {
		return this.m_callable.getParameterString(Operation.BUILTIN_PARAMNAME_WEBREQUESTFORM, defaultIfNotFound);
	}

	/**
	 * 如果解析的是操作表达式，那么返回{@link ExpressionParser#parseVariables()}的结果，否则返回null。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.ExpressionParser#parseVariables()
	 */
	public List<ParsedVariable> parseVariables() {
		if (m_expressionParser == null) return null;
		return m_expressionParser.parseVariables();
	}

	private String m_clientId = null;

	/**
	 * 获取客户端使用的唯一操作id。
	 * 
	 * @return
	 */
	public String getClientId() {
		if (m_clientId != null && m_clientId.length() > 0) return m_clientId;
		String raw = (this.m_expressionParser == null ? this.getOperation().getExpression() : this.m_expressionParser.getExpression());
		if (raw == null || raw.length() == 0) return this.getFunction().getUNID();
		return StringUtil.getMD5HashString(raw);
	}

	/**
	 * 设置客户端使用的唯一操作id。
	 * 
	 * @param clientId
	 */
	public void setClientId(String clientId) {
		m_clientId = clientId;
	}
}

