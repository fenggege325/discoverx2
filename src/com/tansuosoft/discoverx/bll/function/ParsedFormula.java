/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.ParameterValueDataType;

/**
 * 表示解析后的系统函数对应的公式信息的类。
 * 
 * <p>
 * 即表达式中包含公式变量的解析结果。<br/>
 * 表达式中的公式变量（运行时转换为公式运行结果）表示方式为：@{功能函数唯一别名}(参数列表)，如：“@Username(format:="cn")”、“@F1(num:=1,bl1:=true,bl2:=false,f2:=@F2(),fvar:=$fld_subject)”。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ParsedFormula extends ParsedVariable {
	private FormulaParser m_parser = null; // 公式解析器。
	private List<ParsedParameter> m_parameters = null; // 解析出来的参数信息集合。

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param variable String
	 * @param range VariableRange
	 */
	protected ParsedFormula(String variable, VariableRange range) {
		super(variable, range);
		m_parser = new FormulaParser(this);
		m_parser.parse();
	}

	/**
	 * 返回解析出来的公式对应的系统函数别名。
	 * 
	 * <p>
	 * 比如“@UserName(format:="cn")”这个公式中，“UserName”即为函数别名。
	 * </p>
	 * 
	 * @return String 如果变量体为null、空串、不包含前缀，则返回null。
	 */
	public String getFunctionAlias() {
		String v = this.getVariable();
		if (v == null || v.length() == 0 || v.charAt(0) != FORMULA_VAR_PREFIX) return null;
		char chars[] = v.toCharArray();
		int end = -1;
		for (int i = 1; i < chars.length; i++) {
			if (chars[i] == '(') {
				end = i;
				break;
			}
		}
		return new String(chars, 1, end - 1).trim();
	}

	/**
	 * 返回解析出来的原始参数名-参数值对应的StringPair集合。
	 * 
	 * <strong>注意是原始配置的参数名和参数值（即参数值是原始配置的值，比如用于表示常数字符串参数值的引号等）。</strong>
	 * <p>
	 * 参数按从左到右顺序保存于List中，单个参数对应的{@link com.tansuosoft.discoverx.util.StringPair}对象中，key为参数名，value为参数值（原始公式中的原始参数值）。
	 * </p>
	 * <p>
	 * 参数示例：
	 * </p>
	 * <p>
	 * 比如“@UserName(format:="cn")”这个公式中：“format”为参数名，“"cn"”为字符串类型的参数值。
	 * </p>
	 * <p>
	 * 比如“@F1(num:=1,bl1:=true,bl2:=false,f2:=@F2())”这个公式中：num为参数名，“1”为数字类型参数值；“bl1”、“bl2”为参数名，“true/false”为可用的布尔型参数值；“f2”为参数名，@F2()为公式类型的参数值。
	 * </p>
	 * 
	 * @return List&lt;StringPair&gt;
	 */
	public List<ParsedParameter> getParameters() {
		return this.m_parameters;
	}

	/**
	 * 设置参数名和对应参数值到表达式对应参数集合中。
	 * 
	 * <p>
	 * <strong>注意是原始配置的参数名和参数值。</strong>
	 * </p>
	 * 
	 * @param paramName String 参数名。
	 * @param paramValue String 参数值（原始参数值）。
	 */
	protected void setParameter(String paramName, String paramValue) {
		if (paramName == null || paramName.length() == 0 || paramValue == null) return;
		if (this.m_parameters == null) this.m_parameters = new ArrayList<ParsedParameter>();
		this.m_parameters.add(new ParsedParameter(paramName, paramValue));
	}

	/**
	 * 设置参数名、对应参数值、参数值类型到表达式对应参数集合中。
	 * 
	 * <p>
	 * <strong>注意是原始配置的参数名和参数值。</strong>
	 * </p>
	 * 
	 * @param paramName String 参数名。
	 * @param paramValue String 参数值（原始参数值）。
	 * @param dt ParameterValueDataType 参数值类型。
	 */
	protected void setParameter(String paramName, String paramValue, ParameterValueDataType dt) {
		if (paramName == null || paramName.length() == 0 || paramValue == null) return;
		if (this.m_parameters == null) this.m_parameters = new ArrayList<ParsedParameter>();
		this.m_parameters.add(new ParsedParameter(paramName, paramValue, dt));
	}

}

