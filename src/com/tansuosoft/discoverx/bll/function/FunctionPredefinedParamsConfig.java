/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.ParameterComparator;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 系统功能函数预设参数信息的配置类。
 * 
 * <p>
 * 它包含与不同函数一一对应的{@link FunctionPredefinedParams}集合。
 * </p>
 * <p>
 * 预设参数表示某个功能函数可以接收的必要/可选的有效参数。<br/>
 * 通过预设参数配置，用户配置/调用函数时，可以更直观了解函数所支持的参数信息。<br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FunctionPredefinedParamsConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private FunctionPredefinedParamsConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
		if (m_listDefinitionEntries != null) {
			m_definitionEntries = new HashMap<String, FunctionPredefinedParams>(m_listDefinitionEntries.size());
			for (FunctionPredefinedParams x : m_listDefinitionEntries) {
				if (x == null) continue;
				this.m_definitionEntries.put(x.getKey().toLowerCase(), x);
			}
		}
		m_synonyms.put(Operation.BUILTIN_PARAMNAME_JSFUNCTION, Operation.BUILTIN_PARAMNAME_JSFUNCTION_ALIAS);
		m_synonyms.put(Operation.BUILTIN_PARAMNAME_NEXTOPERATION, Operation.BUILTIN_PARAMNAME_NEXTOPERATION_ALIAS);
		m_synonyms.put(Operation.BUILTIN_PARAMNAME_JSFUNCTION_ALIAS, Operation.BUILTIN_PARAMNAME_JSFUNCTION);
		m_synonyms.put(Operation.BUILTIN_PARAMNAME_NEXTOPERATION_ALIAS, Operation.BUILTIN_PARAMNAME_NEXTOPERATION);
	}

	private static FunctionPredefinedParamsConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return FunctionPredefinedParamsConfig
	 */
	public static FunctionPredefinedParamsConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new FunctionPredefinedParamsConfig();

			}
		}
		return m_instance;
	}

	private HashMap<String, FunctionPredefinedParams> m_definitionEntries = null; // 系统不同功能函数实现类全限定类名与其对应的预设参数配置对象的映射集合。
	private List<FunctionPredefinedParams> m_listDefinitionEntries = null;
	private Map<String, String> m_synonyms = new HashMap<String, String>(2);

	/**
	 * 返回系统所有功能函数的预设参数定义列表。
	 * 
	 * @return List&lt;FunctionPredefinedParams&gt;
	 */
	public List<FunctionPredefinedParams> getDefinitionEntries() {
		return this.m_listDefinitionEntries;
	}

	/**
	 * 设置系统所有功能函数的预设参数定义列表。
	 * 
	 * @param definitionEntries List&lt;FunctionPredefinedParams&gt;
	 */
	public void setDefinitionEntries(List<FunctionPredefinedParams> definitionEntries) {
		this.m_listDefinitionEntries = definitionEntries;
	}

	/**
	 * 根据{@link FunctionPredefinedParams#getKey()}的结果获取其对应的预设参数定义。
	 * 
	 * @param key String，必须是有效的{@link FunctionPredefinedParams#getKey()}结果。
	 * @return key对应的{@link FunctionPredefinedParams}，或者null(找不到对应时）。
	 */
	protected FunctionPredefinedParams getFunctionParamsDefinition(String key) {
		if (key == null || key.length() == 0) return null;
		if (this.m_definitionEntries == null) return null;
		return this.m_definitionEntries.get(key.toLowerCase());
	}

	/**
	 * 根据{@link FunctionPredefinedParams#getKey()}的结果获取其配置的有效的预设参数集合。
	 * 
	 * <p>
	 * 获取的预设参数集合中包括本身定义的和继承所得的所有参数，参数有重名则剔除多余重名的。
	 * </p>
	 * 
	 * @param key String，必须是有效的{@link FunctionPredefinedParams#getKey()}结果。
	 * @return
	 */
	public List<Parameter> getPredefinedParameters(String key) {
		List<Parameter> result = null;
		FunctionPredefinedParams def = this.getFunctionParamsDefinition(key);
		if (def == null) return null;
		result = def.getPredefinedParams();
		String inheritance = def.getInheritance();
		if (inheritance != null && inheritance.length() > 0) {
			List<Parameter> inhResult = getPredefinedParameters(inheritance);
			if (inhResult != null && !inhResult.isEmpty()) {
				if (result == null) {
					result = new ArrayList<Parameter>(inhResult);
				} else {
					// 如果在继承的参数中碰到与本身定义的参数重名的参数，则优先使用本身定义的。
					Map<String, Parameter> map = new HashMap<String, Parameter>();
					for (Parameter x : result) {
						if (x == null || x.getName() == null) continue;
						map.put(x.getName().toLowerCase(), x);
					}
					for (Parameter x : inhResult) {
						if (x == null || x.getName() == null || this.checkParamNameExists(map, x.getName())) continue;
						result.add(x);
					}
					map.clear();
				}
			}// if end
		}// if end
		if (result != null && result.size() > 0) {
			List<Parameter> ret = new ArrayList<Parameter>(result.size());
			ret.addAll(result);
			return ret;
		}
		return result;
	}

	/**
	 * 根据系统功能函数资源（{@link com.tansuosoft.discoverx.model.Function}）获取其配置的所有有效的预设参数集合。
	 * 
	 * <p>
	 * 按先后顺序依次获取功能函数本身、实现类、表达式名（即：“@+{别名}”）、unid对应的所有参数，然后返回这些参数中所有名称不重复的参数。
	 * </p>
	 * 
	 * @param function {@link com.tansuosoft.discoverx.model.Function}
	 * @return
	 */
	public List<Parameter> getPredefinedParameters(com.tansuosoft.discoverx.model.Function function) {
		if (function == null) return null;
		List<Parameter> result = null;
		boolean isOperation = (function.getCategory() != null && function.getCategory().indexOf("操作") > 0);
		String implement = function.getImplement();
		String formula = String.format("%1$s%2$s", ParsedVariable.FORMULA_VAR_PREFIX, function.getAlias());
		String unid = function.getUNID();
		String name = null;

		Map<String, Parameter> map = new HashMap<String, Parameter>();

		result = function.getParameters();
		if (result != null) {
			for (Parameter x : result) {
				if (x == null) continue;
				name = x.getName();
				if (name == null || name.length() == 0) continue;
				map.put(name.toLowerCase(), x);
			}
			result = null;
		}

		List<Parameter> nextResult = null;
		// 如果是操作那么最先从取所有操作通用的参数
		if (isOperation) {
			nextResult = this.getPredefinedParameters(Operation.class.getName());
			if (nextResult != null && !nextResult.isEmpty()) {
				for (Parameter x : nextResult) {
					if (x == null) continue;
					name = x.getName();
					if (name == null || name.length() == 0) continue;
					if (this.checkParamNameExists(map, name)) continue;
					map.put(name.toLowerCase(), x);
				}
			}// if end
		}

		// 然后从实现类全名中取
		if (nextResult != null) nextResult.clear();
		nextResult = getPredefinedParameters(implement);
		if (nextResult != null && !nextResult.isEmpty()) {
			for (Parameter x : nextResult) {
				if (x == null) continue;
				name = x.getName();
				if (name == null || name.length() == 0) continue;
				if (this.checkParamNameExists(map, name)) continue;
				map.put(name.toLowerCase(), x);
			}
		}// if end

		// 然后从表达式中取
		if (nextResult != null) nextResult.clear();
		nextResult = getPredefinedParameters(formula);
		if (nextResult != null && !nextResult.isEmpty()) {
			for (Parameter x : nextResult) {
				if (x == null) continue;
				name = x.getName();
				if (name == null || name.length() == 0) continue;
				if (this.checkParamNameExists(map, name)) continue;
				map.put(name.toLowerCase(), x);
			}
		}// if end

		// 然后从unid中取
		if (nextResult != null) nextResult.clear();
		nextResult = getPredefinedParameters(unid);
		if (nextResult != null && !nextResult.isEmpty()) {
			for (Parameter x : nextResult) {
				if (x == null) continue;
				name = x.getName();
				if (name == null || name.length() == 0) continue;
				if (this.checkParamNameExists(map, name)) continue;
				map.put(name.toLowerCase(), x);
			}
		}// if end

		if (!map.isEmpty()) result = new ArrayList<Parameter>(map.values());
		map.clear();
		if (result != null && result.size() > 0) {
			Comparator<Parameter> c = new ParameterComparator();
			Collections.sort(result, c);
		}
		return result;
	}

	/**
	 * 检查同名参数是否存在。
	 * 
	 * @param map
	 * @param paramName
	 * @return
	 */
	private boolean checkParamNameExists(Map<String, Parameter> map, String paramName) {
		if (map == null || map.isEmpty()) return false;
		if (paramName == null || paramName.length() == 0) return true;
		String alias = m_synonyms.get(paramName.toLowerCase());
		if (map.containsKey(paramName.toLowerCase()) || map.containsKey(alias)) return true;
		return false;
	}
}

