/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.FunctionExecutionEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Function;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 用于调用系统功能函数的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Invoker extends EventSourceImpl {
	private com.tansuosoft.discoverx.bll.function.Function m_callable = null; // 被调用的系统功能扩展对象。

	/**
	 * 构造时调用的初始化方法。
	 * 
	 * @param function
	 * @throws FunctionException
	 */
	private void init(Function function) throws FunctionException {
		if (function == null) throw new FunctionException("没有提供有效的功能函数资源！");
		String impl = function.getImplement();
		com.tansuosoft.discoverx.bll.function.Function c = null;
		if ((impl == null || impl.length() == 0)) c = new BlankOperation();
		c = Instance.newInstance(impl, com.tansuosoft.discoverx.bll.function.Function.class);
		if (c == null) throw new FunctionException("无法找到或初始化“" + impl + "”指定的功能函数实现类，请检查类是否正常编译打包为jar并成功部署到WEB-INF\\lib目录下！");
		c.setFunction(function);
		this.m_callable = c;
	}

	/**
	 * 接收被调用的系统功能函数资源的构造器。
	 * 
	 * @param function
	 * @throws FunctionException
	 */
	protected Invoker(Function function) throws FunctionException {
		init(function);
	}

	/**
	 * 接收被调用的系统功能函数资源UNID、别名或具体功能函数实现类全限定类名(完整类名)的构造器。
	 * 
	 * @param function String
	 * @throws FunctionException
	 */
	public Invoker(String function) throws FunctionException {
		if (function == null || function.length() == 0) throw new FunctionException("没有提供功有效能函数资源UNID或别名！");
		Function f = null;
		if (StringUtil.isUNID(function)) f = ResourceContext.getInstance().getResource(function, Function.class);
		if (f == null) {
			String unid = ResourceAliasContext.getInstance().getUNIDByAlias(Function.class, function);
			if (unid != null) f = ResourceContext.getInstance().getResource(unid, Function.class);
		}
		if (f == null) {
			f = new Function();
			f.setImplement(function);
			String alias = StringUtil.stringRightBack(function, ".");
			f.setAlias(StringUtil.toCamelCase(StringUtil.getValueString(alias, function)));
		}
		init(f);
	}

	/**
	 * 获取绑定的扩展功能函数实现对象。
	 * 
	 * @return com.tansuosoft.discoverx.bll.function.Function
	 */
	public com.tansuosoft.discoverx.bll.function.Function getConcrete() {
		return m_callable;
	}

	/**
	 * 设置绑定的扩展功能函数实现对象。
	 * 
	 * @param concrete
	 */
	protected void setConcrete(com.tansuosoft.discoverx.bll.function.Function concrete) {
		this.m_callable = concrete;
	}

	/**
	 * 是否http请求处理函数。
	 * 
	 * <p>
	 * 即功能函数实现对象是否为{@link Operation}的实例。
	 * </p>
	 * 
	 * @return
	 */
	public boolean isOperation() {
		return (this.m_callable != null && (this.m_callable instanceof Operation));
	}

	/**
	 * 当{@link #isOperation()}返回true时以{@link com.tansuosoft.discoverx.bll.function.Operation}实例返回。
	 * 
	 * @return
	 */
	protected Operation asOperatioin() {
		if (!isOperation()) return null;
		return (Operation) m_callable;
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * <p>
	 * 如果是调用操作类型({@link com.tansuosoft.discoverx.bll.function.Operation})的功能函数，那么调用前后会触发事件。
	 * </p>
	 * 
	 * @return Object
	 * @throws FunctionException
	 * @see com.tansuosoft.discoverx.bll.function.Function#call()
	 */
	public Object call() throws FunctionException {
		if (m_callable instanceof Operation) {
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_FUNCTIONEXECUTIONBEGIN, EventHandler.EHT_FUNCTIONEXECUTIONAFTER), EventHandlerInfoProvider.getProvider(EventCategory.Operation).provide(m_callable, null));
			Operation operation = (Operation) m_callable;
			EventArgs e = new FunctionExecutionEventArgs(m_callable);
			this.callEventHandler(EventHandler.EHT_FUNCTIONEXECUTIONBEGIN, e);

			String lastRequestUrl = operation.getSourceRequestUrl();

			if (lastRequestUrl != null && lastRequestUrl.length() > 0) {
				Session s = this.getSession();
				HttpServletRequest request = operation.getHttpRequest();
				HttpSession httpSession = (request == null ? null : request.getSession());
				Session.setSessionParam(s, httpSession, Session.LASTREQUESTURL_PARAM_NAME_IN_HTTPSESSION, lastRequestUrl);
			}
		}

		Object obj = m_callable.call();

		if (m_callable instanceof Operation) {
			EventArgs e = new FunctionExecutionEventArgs(m_callable);
			this.callEventHandler(EventHandler.EHT_FUNCTIONEXECUTIONAFTER, e);
		}
		return obj;
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getCaller()
	 */
	public Object getCaller() {
		return m_callable.getCaller();
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getDocument()
	 */
	public Document getDocument() {
		return m_callable.getDocument();
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getFunction()
	 */
	public Function getFunction() {
		return m_callable.getFunction();
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterBoolean(java.lang.String, boolean)
	 */
	public boolean getParameterBoolean(String paramName, boolean defaultValue) {
		return m_callable.getParameterBoolean(paramName, defaultValue);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param paramName
	 * @param enumCls
	 * @param defaultValue
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterEnum(java.lang.String, java.lang.Class, java.lang.Enum)
	 */
	public <T> T getParameterEnum(String paramName, Class<T> enumCls, T defaultValue) {
		return m_callable.getParameterEnum(paramName, enumCls, defaultValue);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterFloat(java.lang.String, float)
	 */
	public float getParameterFloat(String paramName, float defaultValue) {
		return m_callable.getParameterFloat(paramName, defaultValue);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterInt(java.lang.String, int)
	 */
	public int getParameterInt(String paramName, int defaultValue) {
		return m_callable.getParameterInt(paramName, defaultValue);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterLong(java.lang.String, long)
	 */
	public long getParameterLong(String paramName, long defaultValue) {
		return m_callable.getParameterLong(paramName, defaultValue);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterNames()
	 */
	public Iterator<String> getParameterNames() {
		return m_callable.getParameterNames();
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param paramName
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterObject(java.lang.String)
	 */
	public Object getParameterObject(String paramName) {
		return m_callable.getParameterObject(paramName);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterString(java.lang.String, java.lang.String)
	 */
	public String getParameterString(String paramName, String defaultValue) {
		return m_callable.getParameterString(paramName, defaultValue);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getResource()
	 */
	public Resource getResource() {
		return m_callable.getResource();
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getSession()
	 */
	public Session getSession() {
		return m_callable.getSession();
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getUser()
	 */
	public User getUser() {
		return m_callable.getUser();
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param name
	 * @see com.tansuosoft.discoverx.bll.function.Function#removeParameter(java.lang.String)
	 */
	public void removeParameter(String name) {
		m_callable.removeParameter(name);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param caller
	 * @see com.tansuosoft.discoverx.bll.function.Function#setCaller(java.lang.Object)
	 */
	public void setCaller(Object caller) {
		m_callable.setCaller(caller);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param doc
	 * @see com.tansuosoft.discoverx.bll.function.Function#setDocument(com.tansuosoft.discoverx.model.Document)
	 */
	public void setDocument(Document doc) {
		m_callable.setDocument(doc);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param function
	 * @see com.tansuosoft.discoverx.bll.function.Function#setFunction(com.tansuosoft.discoverx.model.Function)
	 */
	public void setFunction(Function function) {
		m_callable.setFunction(function);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param name
	 * @param value
	 * @see com.tansuosoft.discoverx.bll.function.Function#setParameter(java.lang.String, java.lang.Object)
	 */
	public void setParameter(String name, Object value) {
		m_callable.setParameter(name, value);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param resource
	 * @see com.tansuosoft.discoverx.bll.function.Function#setResource(com.tansuosoft.discoverx.model.Resource)
	 */
	public void setResource(Resource resource) {
		m_callable.setResource(resource);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param session
	 * @see com.tansuosoft.discoverx.bll.function.Function#setSession(com.tansuosoft.discoverx.model.Session)
	 */
	public void setSession(Session session) {
		m_callable.setSession(session);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getHttpRequest()
	 */
	public HttpServletRequest getHttpRequest() {
		return m_callable.getHttpRequest();
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param <T>
	 * @param paramName
	 * @param clazz
	 * @param defaultReturn
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterObject(java.lang.String, java.lang.Class, java.lang.Object)
	 */
	public <T> T getParameterObject(String paramName, Class<T> clazz, T defaultReturn) {
		return m_callable.getParameterObject(paramName, clazz, defaultReturn);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param <T>
	 * @param paramName
	 * @param clazz
	 * @param defaultReturn
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getParameterValueAsObject(java.lang.String, java.lang.Class, java.lang.Object)
	 */
	public <T> T getParameterValueAsObject(String paramName, Class<T> clazz, T defaultReturn) {
		return m_callable.getParameterValueAsObject(paramName, clazz, defaultReturn);
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.bll.function.Function#getExpression()
	 */
	public String getExpression() {
		return m_callable.getExpression();
	}

	/**
	 * 调用{@link com.tansuosoft.discoverx.bll.function.Function}同名委托方法。
	 * 
	 * @param expression
	 * @see com.tansuosoft.discoverx.bll.function.Function#setExpression(java.lang.String)
	 */
	public void setExpression(String expression) {
		m_callable.setExpression(expression);
	}

}

