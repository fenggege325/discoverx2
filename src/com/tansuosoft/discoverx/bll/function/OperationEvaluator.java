/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.OperationResult;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 执行http请求处理并获取最终结果的类。
 * 
 * <p>
 * 此类一般用于获取{@link Operation}实现对象的最终执行结果{@link OperationResult}对象。<br/>
 * 它只返回{@link OperationResult}对象，其它返回结果未实现。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class OperationEvaluator extends Evaluator {
	private OperationParser parser = null;
	private Object m_objectValue = null;
	private String m_expression = null;
	private Invoker invoker = null;

	/**
	 * 接收表达式、自定义会话、已计算表达式等的构造器。
	 * 
	 * @param expression String 表达式，必须。
	 * @param session Session 自定义会话，可以为null。
	 * @param httpRequest HttpServletRequest，表示http请求。
	 * @param resource Resource 和表达式上下文绑定的资源，可以为null。
	 * @param caller Object，表示调用这，可以为null（表示此对象本身为调用者）。
	 * 
	 * @throws FunctionException
	 */
	public OperationEvaluator(String expression, Session session, HttpServletRequest httpRequest, Resource resource, Object caller) throws FunctionException {
		this.m_expression = expression;

		try {
			// 解析表达式
			parser = new OperationParser(this.m_expression, session);
			// 初始化执行参数
			invoker = parser.getInvoker();
			invoker.setSession(session);
			invoker.setCaller(caller == null ? this : caller);
			invoker.setExpression(m_expression);
			if (resource != null) invoker.setResource(resource);

			// 设置目标http请求
			Operation operation = parser.getOperation();
			if (operation != null && httpRequest != null) operation.setHttpRequest(httpRequest);

			// 设置请求信息封装
			String webRequestFormClassName = parser.getWebRequestForm(null);
			if (webRequestFormClassName != null && webRequestFormClassName.length() > 0) {
				CommonForm webRequestForm = Instance.newInstance(webRequestFormClassName, CommonForm.class);
				webRequestForm.fillWebRequestForm(operation.getHttpRequest());
				operation.setWebRequestForm(webRequestForm);
			} else {
				CommonDispatchForm dispatchForm = new CommonDispatchForm();
				dispatchForm.fillWebRequestForm(operation.getHttpRequest());
				operation.setWebRequestForm(dispatchForm);
			}
			this.setReturnTypeFlag(EvaluatorReturnType.Object);
		} catch (FunctionException e) {
			throw e;
		} catch (Exception e) {
			throw new FunctionException(e);
		}
	}

	/**
	 * 重载evalBoolean：未实现，总是返回false。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalBoolean()
	 */
	@Override
	public boolean evalBoolean() {
		return false;
	}

	/**
	 * 重载evalString：返回处理结果对象{@link OperationResult}的toString结果。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalString()
	 */
	@Override
	public String evalString() {
		return (this.m_objectValue == null ? null : this.m_objectValue.toString());
	}

	/**
	 * 重载evalInt：未实现，总是返回0。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalInt()
	 */
	@Override
	public int evalInt() {
		return 0;
	}

	/**
	 * 重载evalLong：未实现，总是返回0L。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalLong()
	 */
	@Override
	public long evalLong() {
		return 0L;
	}

	/**
	 * 重载evalFloat：未实现，总是返回0.0F。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalFloat()
	 */
	@Override
	public float evalFloat() {
		return 0.0F;
	}

	/**
	 * 重载evalDouble：未实现，总是返回0.0D。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalDouble()
	 */
	@Override
	public double evalDouble() {
		return 0.0D;
	}

	/**
	 * 重载evalObject：返回{@link Operation}的执行结果对象｛@link OperationResult｝。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalObject()
	 */
	@Override
	public Object evalObject() {
		return m_objectValue;
	}

	/**
	 * 重载evalEnum：未实现，总是返回null。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalEnum()
	 */
	@Override
	public Enum<?> evalEnum() {
		return null;
	}

	/**
	 * 调用操作并返回结果。
	 * 
	 * @return
	 * @throws FunctionException
	 */
	public OperationResult evalResult() throws FunctionException {
		// 执行并获取结果
		m_objectValue = invoker.call();
		if (m_objectValue == null || !(m_objectValue instanceof OperationResult)) return null;
		return (OperationResult) m_objectValue;
	}

	/**
	 * 获取绑定的{@link OperationParser}对象。
	 * 
	 * @return
	 */
	public OperationParser getOperationParser() {
		return this.parser;
	}
}

