/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.document.DocumentOpener;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 表示系统所有功能函数实现类的基类。
 * 
 * <p>
 * 一般情况下，一个或多个<strong>系统功能函数定义</strong>资源对应一个<strong>系统功能函数实现</strong>。
 * </p>
 * <p>
 * 各种类别的具体功能扩展函数实现类要继承此类并提供具体实现Call成员的代码。
 * </p>
 * <p>
 * 各种类别的具体功能扩展函数实现所必须或支持的参数在具体实现类的注释中需有相关说明。
 * </p>
 * 
 * @see com.tansuosoft.discoverx.model.Function
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class Function extends EventSourceImpl {
	/**
	 * 表示保存普通资源的参数名。
	 */
	public static final String RESOURCE_PARAM_NAME = "$$resource";

	/**
	 * 表示保存文档资源的参数名。
	 */
	public static final String DOCUMENT_PARAM_NAME = "$$document";

	private com.tansuosoft.discoverx.model.Function m_function = null; // 对应的系统功能函数资源，必须。
	private Session m_session = null; // 调用者对应的用户自定义会话信息，可选。
	private Object m_caller = null; // 调用者，可选。
	private HashMap<String, Object> m_parameters = null; // 从系统功能函数资源中继承并与调用者提供的参数合并后的参数名称(key)和参数值(value)。
	private String m_expression = null; // 原始表达式。

	/**
	 * 缺省构造器。
	 */
	public Function() {
	}

	/**
	 * 同步系统功能函数资源中配置的参数到此调用上下文环境包含的参数中。
	 */
	private void syncParameter() {
		if (this.m_parameters == null) this.m_parameters = new HashMap<String, Object>();
		if (this.m_function == null) return;
		List<Parameter> list = this.m_function.getParameters();
		if (list == null) return;
		String name = null;
		for (Parameter x : list) {
			name = x.getName();
			if (name == null || name.length() == 0) continue;
			this.m_parameters.put(name, x.getValue());
		}
	}

	/**
	 * 返回对应的系统功能函数资源。
	 * 
	 * @return {@link com.tansuosoft.discoverx.model.Function}。
	 */
	public com.tansuosoft.discoverx.model.Function getFunction() {
		return this.m_function;
	}

	/**
	 * 设置对应的系统功能函数资源。
	 * 
	 * @param function {@link com.tansuosoft.discoverx.model.Function}，必须。
	 */
	protected void setFunction(com.tansuosoft.discoverx.model.Function function) {
		this.m_function = function;
		this.syncParameter();
	}

	/**
	 * 返回调用者对应的用户自定义会话信息，可选。
	 * 
	 * @return Session 调用者对应的用户自定义会话信息，可选。
	 */
	public Session getSession() {
		if (this.m_session == null) this.m_session = SessionContext.getSession(getHttpRequest());
		return this.m_session;
	}

	/**
	 * 设置调用者对应的用户自定义会话信息，可选。
	 * 
	 * @param session Session 调用者对应的用户自定义会话信息，可选。
	 */
	public void setSession(Session session) {
		this.m_session = session;
	}

	/**
	 * 获取当前用户。
	 * 
	 * @return Participant 如果绑定的用户自定义会话对象为null，则返回系统全局唯一的匿名用户对象。
	 */
	public User getUser() {
		return Session.getUser(this.getSession());
	}

	/**
	 * 获取Session中绑定的http请求。
	 * 
	 * @return HttpServletRequest HttpServletRequest 如果没有Session或Session没有绑定请求，则返回null。
	 */
	public HttpServletRequest getHttpRequest() {
		Session s = this.m_session;
		if (s != null) return s.getHttpRequest();
		return null;
	}

	/**
	 * 返回调用者，可选。
	 * 
	 * @return Object 调用者，可选。
	 */
	public Object getCaller() {
		return this.m_caller;
	}

	/**
	 * 设置调用者，可选。
	 * 
	 * @param caller Object 调用者，可选。
	 */
	public void setCaller(Object caller) {
		this.m_caller = caller;
	}

	/**
	 * 获取绑定的当前处理的文档对象。
	 * 
	 * <p>
	 * 获取步骤：
	 * </p>
	 * <p>
	 * 1.先从当前用户自定义会话中通过{@link Session#getLastDocument()}获取文档，取到有效文档对象实例则返回。
	 * </p>
	 * <p>
	 * 2.再从名为“{@link Function#DOCUMENT_PARAM_NAME}”的参数获取绑定的文档，如果获取到有效值则返回之。
	 * </p>
	 * <p>
	 * 3.最后从{@link Function#getResource()}返回的资源判断是否文档类型资源，如果是，则返回之。
	 * </p>
	 * <p>
	 * 4.如果都取不到则返回null。
	 * </p>
	 * 
	 * @return Document
	 */
	public Document getDocument() {
		Session session = this.getSession();
		if (session != null) {
			Document doc = session.getLastDocument();
			if (doc != null) return doc;
		}

		Object obj = this.getParameterObject(Function.DOCUMENT_PARAM_NAME);
		if (obj != null && (obj instanceof Document)) return (Document) obj;

		Resource resource = this.getResource();
		if (resource != null && (resource instanceof Document)) return (Document) resource;

		return null;
	}

	/**
	 * 调用{@link DocumentOpener#open(String, Class, Session)}打开unid指定文档并返回之。
	 * 
	 * @param unid
	 * @return 如果找不到、没有权限或出错则返回null。
	 */
	public Document openDocument(String unid) {
		DocumentOpener opener = new DocumentOpener();
		Resource res = null;
		try {
			res = opener.open(unid, Document.class, this.getSession());
		} catch (ResourceException e) {
			FileLogger.error(e);
		}
		if (res == null || !(res instanceof Document)) return null;
		return (Document) res;
	}

	/**
	 * 把文档资源对象设置到名为“{@link DOCUMENT_PARAM_NAME}”的参数值中。
	 * 
	 * @param doc Document
	 */
	public void setDocument(Document doc) {
		this.setParameter(DOCUMENT_PARAM_NAME, doc);
	}

	/**
	 * 获取绑定的当前处理的资源对象。
	 * 
	 * <p>
	 * 获取步骤：
	 * </p>
	 * <p>
	 * 1.先从当前用户自定义会话中通过{@link Session#getLastResource()}获取资源，取到有效资源对象实例则返回。
	 * </p>
	 * <p>
	 * 2.再从名为“{@link Function#RESOURCE_PARAM_NAME}”的参数获取绑定的资源对象，如果获取到有效值则返回之。
	 * </p>
	 * 
	 * @return Resource
	 */
	public Resource getResource() {
		Session session = this.getSession();
		if (session != null) {
			Resource res = session.getLastResource();
			if (res != null) return res;
		}
		Object obj = this.getParameterObject(RESOURCE_PARAM_NAME);
		return (Resource) obj;
	}

	/**
	 * 把资源对象设置到名为“{@link Function#RESOURCE_PARAM_NAME}”的参数值中。
	 * 
	 * @param resource Resource。
	 */
	public void setResource(Resource resource) {
		this.setParameter(RESOURCE_PARAM_NAME, resource);
	}

	/**
	 * 获取指定名称的参数值。
	 * 
	 * @param paramName String 参数名，必须。
	 * @return Object 返回对应参数值，如果找不到则为null。
	 */
	public Object getParameterObject(String paramName) {
		if (paramName == null || paramName.length() == 0 || this.m_parameters == null || this.m_parameters.isEmpty()) return null;
		return this.m_parameters.get(paramName);
	}

	/**
	 * 获取指定名称的指定类型的参数值。
	 * 
	 * @param <T>
	 * @param paramName
	 * @param clazz
	 * @param defaultReturn
	 * @return 返回对应参数值，如果找不到则为defaultReturn。
	 */
	@SuppressWarnings("unchecked")
	public <T> T getParameterObject(String paramName, Class<T> clazz, T defaultReturn) {
		Object obj = this.getParameterObject(paramName);
		if (obj == null) return defaultReturn;
		Class<?> cls = obj.getClass();
		if (clazz.isAssignableFrom(cls)) { return (T) obj; }
		return defaultReturn;
	}

	/**
	 * 获取指定名称的参数值作为全限定类名值并调用其默认构造器构造为对应实例的结果。
	 * 
	 * @param <T>
	 * @param paramName
	 * @param clazz
	 * @param defaultReturn
	 * @return 如果没有配置值则返回defaultReturn。
	 */
	public <T> T getParameterValueAsObject(String paramName, Class<T> clazz, T defaultReturn) {
		String impl = this.getParameterString(paramName, null);
		if (impl == null || impl.length() == 0) return defaultReturn;
		T result = Instance.newInstance(impl, clazz);
		if (result != null) return result;
		return defaultReturn;
	}

	/**
	 * 获取指定参数名对应参数值的文本结果，如果获取不到或获取到的值为空字符串（长度为零的字符串）则返回defaultValue。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return String
	 */
	public String getParameterString(String paramName, String defaultValue) {
		Object obj = this.getParameterObject(paramName);
		if (obj == null) return defaultValue;
		String str = obj.toString();
		if (str.length() == 0) return defaultValue;
		return str;
	}

	/**
	 * 获取指定参数名对应参数值的int结果，如果获取不到则返回defaultValue。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return int
	 */
	public int getParameterInt(String paramName, int defaultValue) {
		String str = this.getParameterString(paramName, null);
		return StringUtil.getValueInt(str, defaultValue);
	}

	/**
	 * 获取指定参数名对应参数值的long结果，如果获取不到则返回defaultValue。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return long
	 */
	public long getParameterLong(String paramName, long defaultValue) {
		String str = this.getParameterString(paramName, null);
		return StringUtil.getValueLong(str, defaultValue);
	}

	/**
	 * 获取指定参数名对应参数值的float结果，如果获取不到则返回defaultValue。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return float
	 */
	public float getParameterFloat(String paramName, float defaultValue) {
		String str = this.getParameterString(paramName, null);
		return StringUtil.getValueFloat(str, defaultValue);
	}

	/**
	 * 获取指定参数名对应参数值的boolean结果，如果获取不到则返回defaultValue。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return boolean
	 */
	public boolean getParameterBoolean(String paramName, boolean defaultValue) {
		String str = this.getParameterString(paramName, null);
		return StringUtil.getValueBool(str, defaultValue);
	}

	/**
	 * 获取指定参数名对应参数值的Enum结果，如果获取不到则返回defaultValue。
	 * 
	 * @param &lt;T&gt; 返回的枚举类型。
	 * @param paramName
	 * @param enumCls Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return T 具体枚举结果。
	 */
	public <T> T getParameterEnum(String paramName, Class<T> enumCls, T defaultValue) {
		String str = this.getParameterString(paramName, null);
		return StringUtil.getValueEnum(str, enumCls, defaultValue);
	}

	/**
	 * 设置name指定的参数名称对应的参数值为value。
	 * <p>
	 * 如果同名的参已经存在，则其值被替换，如果value为null，则等同于调用removeParameter(name)
	 * </p>
	 * 
	 * @param name String
	 * @param value Object
	 */
	public void setParameter(String name, Object value) {
		if (name == null || name.length() == 0) return;
		if (this.m_parameters == null) this.m_parameters = new HashMap<String, Object>();
		if (value == null) {
			this.removeParameter(name);
			return;
		}
		this.m_parameters.put(name, value);
	}

	/**
	 * 删除name名称指定的参数值。
	 * 
	 * @param name String
	 */
	public void removeParameter(String name) {
		if (this.m_parameters != null && name != null && name.length() > 0) {
			this.m_parameters.remove(name);
		}
	}

	/**
	 * 获取所有参数名称字符串集合对应的迭代器对象，如果没有任何参数，则返回null。
	 * 
	 * @return Iterator&lt;String&gt;
	 */
	public Iterator<String> getParameterNames() {
		if (this.m_parameters == null || this.m_parameters.size() == 0) return null;
		Iterator<String> iterator = this.m_parameters.keySet().iterator();
		return iterator;
	}

	/**
	 * 返回原始表达式。
	 * 
	 * <p>
	 * 即触发执行此函数的原始表达式。
	 * </p>
	 * 
	 * @return String
	 */
	public String getExpression() {
		return this.m_expression;
	}

	/**
	 * 设置原始表达式。
	 * 
	 * @param expression String
	 */
	public void setExpression(String expression) {
		this.m_expression = expression;
	}

	/**
	 * 调用系统功能函数并返回结果。
	 * 
	 * <p>
	 * 具体函数功能实现类应实现此方法提供具体功能。
	 * </p>
	 * 
	 * @return Object
	 * @throws FunctionException
	 */
	public abstract Object call() throws FunctionException;
}

