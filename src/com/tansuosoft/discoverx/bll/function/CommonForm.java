/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.util.ObjectUtil;

/**
 * 表示http请求提交时包含的请求信息的基类（简称：请求参数包装类）。
 * 
 * <p>
 * 通常用于从http请求参数名中获取对应的参数值，并以setter/getter的形式通过java对象设置/读取这些参数值。<br/>
 * 不同类别或功能的请求，其参数会不同，但是如果某一类请求其参数名和参数值有一定规律和要求，则可以封装一个继承自此类的新类，用于方便获取请求信息。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class CommonForm {
	/**
	 * 缺省构造器。
	 */
	public CommonForm() {
	}

	/**
	 * 使用HttpServletRequest中的相关参数填充属性并返回此对象的实例。
	 * 
	 * @param request HttpServletRequest
	 * @return CommonForm
	 */
	public abstract CommonForm fillWebRequestForm(HttpServletRequest request);

	/**
	 * 自动按照此对象的读写属性名使用HttpServletRequest中的相关参数填充此相应的属性值。
	 * 
	 * <p>
	 * 此方法主要用于自动按照类的可读写属性名从HttpServletRequest中获取相应的参数值并把填填充到相应属性值中。
	 * </p>
	 * <p>
	 * 只支持String、int、boolean、EnumBase等类型。
	 * </p>
	 * 
	 * @param request HttpServletRequest
	 */
	public void autoFillForm(HttpServletRequest request) {
		if (request == null) return;
		Map<Method, Method> props = ObjectUtil.getInstanceRWPropMethods(this);
		if (props == null || props.isEmpty()) return;
		String paramValue = null;
		for (Method x : props.keySet()) {
			paramValue = request.getParameter(x.getName().substring(3));
			ObjectUtil.propSetValueFromText(this, x.getName(), paramValue);
		}
	}
}

