/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * 用于解析表达式的类。
 * 
 * <p>
 * 表达式表示一串由变量、常数、操作符（运算符）、括号等组合而成的文本。<br/>
 * 表达式被执行（求值）后，最终会返回某个字符串、布尔或数字结果值。</br>
 * </p>
 * <hr/>
 * <p>
 * 表达式中的变量（Variable）包括：<br/>
 * <strong>公式</strong>：Formula，对应一个系统功能函数；<br/>
 * <strong>字段</strong>：Field，对应一个通常来自文档的字段名。<br/>
 * </p>
 * 
 * <p>
 * 公式变量格式： <br/>
 * <strong>&#64;{变量名（即功能函数的别名）}([{参数名1:=参数值1（可以是常数或表达式）},...])</strong><br/>
 * 说明：<br/>
 * 在“{}”或“[]”中的为说明替代符，实际要换成具体符号；其中在“{}”中的内容为必须，在“[]”中的内容为可选，下同。
 * </p>
 * <p>
 * 公式变量示例：<br/>
 * <strong>@UserName(format:="cn")</strong>：这个公式中：<br/>
 * “format”为参数名，“cn”（不含引号）为字符串类型的参数值。<br/>
 * <strong>@F1(num:=1,bl:=true,f2:=@F2()+""+$fld_subject)</strong>：这个公式中：<br/>
 * num为参数名，“1”为数字类型参数值；<br/>
 * “bl”为参数名，“true”为可用的布尔型参数值；<br/>
 * “f2”为参数名，@F2()+""+$fld_subject 为表达式类型的参数值。<br/>
 * ...
 * </p>
 * 
 * <p>
 * 字段变量格式：<br/>
 * <strong>${字段名（英文字母、数字、下划线的组合）}</strong>
 * </p>
 * <p>
 * 字段变量示例：<br/>
 * $fld_subject<br/>
 * $fld_num<br/>
 * ...
 * </p>
 * <hr/>
 * <p>
 * 表达式中的常数类型包括：<br/>
 * <list>
 * <li>文本：包括在半角双引号中的值，如:“"test"”、“" a\"a "”等（不包括外层引号）。</li>
 * <li>数字：可以是零、正数、负数、整数、小数等，如：“0”、“100”、“-10.0”等（不包括外层引号）。</li>
 * <li>布尔：可以是<strong><i>true/false</i></strong>中的某一个。</li>
 * </list>
 * </p>
 * <p>
 * &nbsp;
 * </p>
 * <p>
 * 文本类型常数的额外注意事项：<br/>
 * 如果文本常数中包含半角双引号（“"”）字符，则需要用半角反斜杆（“\”）进行转义。<br/>
 * 如“\"”（表示一个半角双引号）。
 * </p>
 * <hr/>
 * <p>
 * 操作符（运算符）包含如下几种：<br/>
 * <list>
 * <li><strong>==</strong>：表示逻辑操作符的等于</li>
 * <li><strong>&lt;</strong>：表示逻辑操作符的小于</li>
 * <li><strong>&lt;=</strong>：表示逻辑操作符的小于等于</li>
 * <li><strong>&gt;</strong>：表示逻辑操作符的大于</li>
 * <li><strong>&gt;=</strong>：表示逻辑操作符的大于等于</li>
 * <li><strong>!</strong>：表示逻辑操作符的取反</li>
 * <li><strong>&&</strong>：表示逻辑操作符的与操作</li>
 * <li><strong>||</strong>：表示逻辑操作符的或操作</li>
 * <li><strong>+</strong>：表示数字操作符的加操作或文本操作符的文本连接操作</li>
 * <li><strong>-</strong>：表示数字操作符的减操作</li>
 * <li><strong>*</strong>：表示数字操作符的乘以操作</li>
 * <li><strong>/</strong>：表示数字操作符的除以操作</li>
 * </list>
 * </p>
 * <p>
 * &nbsp;
 * </p>
 * <p>
 * 使用操作符的额外注意事项：<br/>
 * 纯粹的常数之间请不要使用操做符，如“2+3”、“true||false”、“"aa"+"bb"”等不合法，请直接把“5”、“true”、“aabb”等对应的直接结果写到表达式中。
 * </p>
 * <hr/>
 * <p>
 * 括号用于指定表达式计算（求结果）的优先级。默认情况下优先级为从左到右，对于数字表达式来说乘除大于加减，如果有括号，则括号内表达式具有更高优先级。
 * </p>
 * <hr/>
 * <p>
 * 表达式在某些具体配置处可能会被称为<strong>“公式”</strong>、<strong>“操作”</strong>等
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ExpressionParser {
	/**
	 * 转义符：“\”。
	 */
	public static final char ESC_CHAR = '\\';

	private String m_expression = null; // 待解析的表达式文本。
	private char m_chars[] = null; // 待解析的表达式文本对应的字符数组。
	private int m_length = 0; // 待解析的表达式文本长度。
	private TreeSet<VariableRange> m_variableRanges = null; // 解析好的VariableRange集合
	private boolean m_variableRangeParsed = false; // VariableRange集合是否解析完成标记
	private List<ParsedVariable> m_parsedVariables = null; // 解析好的ParsedVariable集合
	private boolean m_variableParsed = false; // ParsedVariable集合是否解析完成标记
	private int m_independentCount = 0; // 独立表达式个数。

	/**
	 * 缺省构造器。
	 */
	protected ExpressionParser() {
	}

	/**
	 * 接收原始待解析表达式的构造器。
	 * 
	 * @param expression
	 */
	public ExpressionParser(String expression) {
		this.m_expression = expression;
		this.m_chars = (this.m_expression == null || this.m_expression.length() == 0 ? null : this.m_expression.toCharArray());
		this.m_length = (this.m_chars == null ? 0 : this.m_chars.length);
		this.m_variableRanges = new TreeSet<VariableRange>();
		this.m_variableRangeParsed = false;
		this.m_parsedVariables = new ArrayList<ParsedVariable>();
		this.m_variableParsed = false;
	}

	/**
	 * 获取原始待解析的表达式文本。
	 * 
	 * @return String
	 */
	public String getExpression() {
		return this.m_expression;
	}

	/**
	 * 按由里到外,由左到右的顺序将表达式分解为不同位置信息列表并返回。
	 * 
	 * <p>
	 * 比如表达式为：“@F1(p1:=@F2(),p2:=1,p3:=$fld_num) && (@F3() || $fld_subject=="test")”<br/>
	 * 那么应解析出五个VariableRange实例，按次序分别为：@F2()部分、$fld_num部分、@F1部分、@F3部分、$fld_subject部分。
	 * </p>
	 * 
	 * @return TreeSet&lt;ParsedVariable&gt;
	 */
	protected TreeSet<VariableRange> parseVariableRanges() {
		if (this.m_length <= 0) throw new RuntimeException("没有指定有效的待解析表达式！");

		if (this.m_variableRangeParsed) return this.m_variableRanges;

		char c = 0;
		boolean quotFlag = false;
		for (int i = 0; i < this.m_length; i++) {
			c = this.m_chars[i];
			// 引号开始：字符为非转义引号或第一个字符为引号
			if (!quotFlag && c == '\"' && ((i > 0 && m_chars[i - 1] != ESC_CHAR) || i == 0)) {
				quotFlag = true;
				continue;
			}
			// 引号结束：已经有引号开始标记且最后一个字符为引号或非转义引号
			if (quotFlag && c == '\"' && (m_chars[i - 1] != ESC_CHAR || i == (m_length - 1))) {
				quotFlag = false;
				continue;
			}

			if (!quotFlag && (c == ParsedVariable.FORMULA_VAR_PREFIX || c == ParsedVariable.FIELD_VAR_PREFIX)) {
				int baractCount = -1;
				boolean quotFlagInner = false;
				for (int j = (i + 1); j < this.m_length; j++) {
					int charj = (int) this.m_chars[j];
					// 引号开始：第一个字符为引号且非转义引号
					if (!quotFlagInner && charj == '\"' && m_chars[j - 1] != ESC_CHAR) {
						quotFlagInner = true;
						continue;
					}
					// 引号结束：已经有引号开始标记且最后一个字符为非转义引号
					if (quotFlagInner && charj == '\"' && m_chars[j - 1] != ESC_CHAR) {
						quotFlagInner = false;
						continue;
					}
					// 字段变量处理：如果有字段变量前缀且当前字符不是字符、数字、下划线结束，说明字段变量结束
					if (c == ParsedVariable.FIELD_VAR_PREFIX && !(charj == 95 || (charj >= 48 && charj <= 57) || (charj >= 65 && charj <= 90) || (charj >= 97 && charj <= 122))) {
						VariableRange pei = new VariableRange(i, j);
						addVariableRange(pei);
						break;
					}
					// 字段变量处理：如果有字段变量前缀且当前字符为最后一个字符，说明字段变量结束
					if (c == ParsedVariable.FIELD_VAR_PREFIX && j == (this.m_length - 1)) {
						VariableRange pei = new VariableRange(i, j + 1);
						addVariableRange(pei);
						break;
					}
					// 左括号开始
					if (!quotFlagInner && this.m_chars[j] == '(') {
						if (baractCount < 0)
							baractCount = 1;
						else
							baractCount++;
					}
					// 右括号开始
					if (!quotFlagInner && this.m_chars[j] == ')') baractCount--;
					// 如果括号数为0，表示公式变量结束
					if (baractCount == 0) {
						VariableRange pei = new VariableRange(i, j + 1);
						addVariableRange(pei);
						break;
					}
				}
			}// if end
		}// for end

		this.m_variableRangeParsed = true;
		return this.m_variableRanges;
	}

	/**
	 * 添加一个{@link VariableRange}
	 * 
	 * @param variableRange
	 */
	private void addVariableRange(VariableRange variableRange) {
		if (variableRange == null) return;
		if (variableRange.getIndependent()) this.m_independentCount++;
		m_variableRanges.add(variableRange);
	}

	/**
	 * 返回{@link VariableRange#getIndependent()}为true的独立表达式的个数。
	 * 
	 * @return
	 */
	public int getIndependentCount() {
		return this.m_independentCount;
	}

	/**
	 * 按顺序将表达式分解为不同解析结果列表并返回。
	 * 
	 * <p>
	 * 比如表达式为：“@F1(p1:=@F2(),p2:=1,p3:=$fld_num) && (@F3() || $fld_subject=="test")”<br/>
	 * 那么应解析出五个ParsedVariable实例，按次序分别为：@F2()部分、$fld_num部分、@F1部分、@F3部分、$fld_subject部分。
	 * </p>
	 * 
	 * @return List&lt;ParsedVariable&gt;，如果表达式为空，返回null。
	 */
	public List<ParsedVariable> parseVariables() {
		if (this.m_variableParsed) return this.m_parsedVariables;

		if (!this.m_variableRangeParsed) this.parseVariableRanges();

		ParsedVariable pvar = null;
		String var = null;
		for (VariableRange x : this.m_variableRanges) {
			var = new String(this.m_chars, x.getStart(), x.getLength());
			pvar = ParsedVariable.createVariable(var, x);
			if (pvar != null) this.m_parsedVariables.add(pvar);
		}

		this.m_variableParsed = true;
		return this.m_parsedVariables;
	}
}

