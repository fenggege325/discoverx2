/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

/**
 * 计算表达式结果的抽象类。
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class Evaluator {
	private BitSet m_valueFlag = null;
	private Map<String, Evaluator> m_evaluated = null;

	/**
	 * 缺省构造器。
	 */
	public Evaluator() {
	}

	/**
	 * 设置计算结果标记为指定类型。
	 * 
	 * @see EvaluatorReturnType
	 * @param t EvaluatorReturnType
	 */
	protected void setReturnTypeFlag(EvaluatorReturnType t) {
		if (t == null) return;
		setReturnTypeFlag(t.getIntValue());
	}

	/**
	 * 设置计算结果标记为t对应参数值指定类型。
	 * 
	 * <p>
	 * t的值只能指定0-7之间的某个值，即范围不能超过{@link EvaluatorReturnType}中规定的枚举值对应的int值。
	 * </p>
	 * 
	 * @param t int
	 */
	protected void setReturnTypeFlag(int t) {
		if (t > 7 || t < 0) return;
		if (this.m_valueFlag == null) {
			m_valueFlag = new BitSet(8);
			m_valueFlag.clear();
		}
		m_valueFlag.set(t);
	}

	/**
	 * 判断计算结果标记是否为指定类型。
	 * 
	 * @see EvaluatorReturnType
	 * @param t EvaluatorReturnType
	 * @return boolean
	 */
	protected boolean checkReturnTypeFlag(EvaluatorReturnType t) {
		if (t == null || isEmpty()) return false;
		return m_valueFlag.get(t.getIntValue());
	}

	/**
	 * 清除已经设置的所有返回结果标记。
	 * 
	 * @return EvaluatorReturnType 如果原来有标记，则返回被清除的标记，否则返回null。
	 */
	protected EvaluatorReturnType clearReturnType() {
		if (isEmpty()) return null;
		EvaluatorReturnType ret = this.getReturnType();
		this.m_valueFlag.clear();
		return ret;
	}

	/**
	 * 获取结算结果类型。
	 * 
	 * @see EvaluatorReturnType
	 * @return EvaluatorReturnType
	 */
	public EvaluatorReturnType getReturnType() {
		if (isEmpty()) return null;
		for (EvaluatorReturnType t : EvaluatorReturnType.values()) {
			if (this.checkReturnTypeFlag(t)) return t;
		}
		return null;
	}

	/**
	 * 是否结果类型标记为空（即未设置过结果类型标记）。
	 * 
	 * @return boolean
	 */
	public boolean isEmpty() {
		return (this.m_valueFlag == null || this.m_valueFlag.isEmpty());
	}

	/**
	 * 把表达式对应的Evaluator添加加到已计算的表达式集合中。
	 * 
	 * @param exp
	 * @param evaluator
	 */
	protected void setEvaluated(String exp, Evaluator evaluator) {
		if (exp == null || exp.length() == 0 || evaluator == null) return;
		if (this.m_evaluated == null) this.m_evaluated = new HashMap<String, Evaluator>();
		// if (this.m_evaluated.containsKey(exp)) return;
		this.m_evaluated.put(exp, evaluator);
	}

	/**
	 * 查找表达式对应的Evaluator并返回之，如果找不到，返回null。
	 * 
	 * @param exp
	 * @return
	 */
	protected Evaluator checkEvaluated(String exp) {
		if (this.m_evaluated == null || this.m_evaluated.isEmpty() || exp == null || exp.length() == 0) return null;
		return this.m_evaluated.get(exp);
	}

	/**
	 * 设置已处理的单个表达式与其结果计算对象一一对应的集合。
	 * 
	 * @param evaluated Map&lt;String, Evaluator&gt;
	 */
	protected void setEvaluated(Map<String, Evaluator> evaluated) {
		this.m_evaluated = evaluated;
	}

	/**
	 * 返回已处理的单个表达式与其结果计算对象一一对应的集合。
	 * 
	 * @return Map&lt;String, Evaluator&gt;
	 */
	protected Map<String, Evaluator> getEvaluated() {
		return this.m_evaluated;
	}

	/**
	 * 计算并返回表达式字符串结果。
	 * 
	 * @return String
	 */
	public abstract String evalString();

	/**
	 * 计算并返回表达式对象（Object）结果。
	 * 
	 * @return Object
	 */
	public abstract Object evalObject();

	/**
	 * 计算并返回表达式布尔结果。
	 * 
	 * @return boolean
	 */
	public abstract boolean evalBoolean();

	/**
	 * 计算并返回表达式int结果。
	 * 
	 * @return int
	 */
	public abstract int evalInt();

	/**
	 * 计算并返回表达式long结果。
	 * 
	 * @return long
	 */
	public abstract long evalLong();

	/**
	 * 计算并返回表达式float结果。
	 * 
	 * @return float
	 */
	public abstract float evalFloat();

	/**
	 * 计算并返回表达式double结果。
	 * 
	 * @return double
	 */
	public abstract double evalDouble();

	/**
	 * 计算并返回表达式枚举结果。
	 * 
	 * <p>
	 * <strong>只有实际结果为Enum类型时才有返回值（系统不会像其它类型那样尝试自动转换），否则返回null。</strong>
	 * </p>
	 * 
	 * @return Enum&lt;?&gt;
	 */
	public abstract Enum<?> evalEnum();
}

