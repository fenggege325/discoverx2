/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.common.exception.ExpressionEvaluatorException;
import com.tansuosoft.discoverx.common.exception.FunctionException;
import com.tansuosoft.discoverx.model.ParameterValueDataType;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 计算某个公式表达式变量并获取结果的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class FormulaVariableEvaluator extends Evaluator {
	private Object m_result = null;
	private Class<?> m_resultClass = null;

	/**
	 * 接收待计算的公式变量、自定义会话的构造器。
	 * 
	 * @param parsedFormula
	 * @param session
	 * @throws ExpressionEvaluatorException
	 */
	protected FormulaVariableEvaluator(ParsedFormula parsedFormula, Session session) throws ExpressionEvaluatorException {
		this(parsedFormula, session, null, null);
	}

	/**
	 * 接收待计算的公式变量、自定义会话、已计算表达式等的构造器。
	 * 
	 * @param parsedFormula ParsedFormula，必须。
	 * @param session Session，必须。
	 * @param resource Resource，可为null。
	 * @param evaluated Map&lt;String, Evaluator&gt; 已经计算完的表达式字符串和对应的Evaluator一一对应的Map，可以为null。
	 * @throws ExpressionEvaluatorException
	 */
	protected FormulaVariableEvaluator(ParsedFormula parsedFormula, Session session, Resource resource, Map<String, Evaluator> evaluated) throws ExpressionEvaluatorException {
		if (parsedFormula == null) return;
		Invoker invoker = null;
		try {
			String functionAlias = parsedFormula.getFunctionAlias();
			if (functionAlias == null || functionAlias.length() == 0) throw new FunctionException("未提供有效系统功能函数资源的别名！");
			String functionUNID = null; // 系统功能函数UNID
			functionUNID = FunctionHelper.getInstance().getFunctionUNIDByAlias(functionAlias);
			if (functionUNID == null || functionUNID.length() == 0) throw new FunctionException("无法获取“" + functionAlias + "”对应系统功能函数资源的UNID！");

			invoker = new Invoker(functionUNID);
			invoker.setSession(session);
			invoker.setCaller(this);
			invoker.setExpression(parsedFormula.getVariable());
			if (resource != null) invoker.setResource(resource);
			List<ParsedParameter> params = parsedFormula.getParameters();
			if (params != null && params.size() > 0) {
				for (ParsedParameter y : params) {
					if (y.getValueDataType() != ParameterValueDataType.Expression) {
						invoker.setParameter(y.getName(), y.getValue());
					} else {
						Evaluator evaluator = new ExpressionEvaluator(y.getRawValue(), session, null, evaluated);
						invoker.setParameter(y.getName(), evaluator.evalObject());
					}
				}// for end
			}// if end
			m_result = invoker.call();
			if (m_result != null) {
				m_resultClass = m_result.getClass();
				boolean isPrimitive = m_resultClass.isPrimitive();
				String className = m_resultClass.getName();
				if (className.equals(String.class.getName())) {
					this.setReturnTypeFlag(EvaluatorReturnType.String);
				} else if (m_resultClass.isEnum()) {
					this.setReturnTypeFlag(EvaluatorReturnType.Enum);
				} else if (className.equalsIgnoreCase(Integer.class.getName()) || (isPrimitive && m_resultClass.getName().equalsIgnoreCase("int"))) {
					this.setReturnTypeFlag(EvaluatorReturnType.Int);
				} else if (className.equalsIgnoreCase(Long.class.getName()) || (isPrimitive && m_resultClass.getName().equalsIgnoreCase("long"))) {
					this.setReturnTypeFlag(EvaluatorReturnType.Long);
				} else if (className.equalsIgnoreCase(Float.class.getName()) || (isPrimitive && m_resultClass.getName().equalsIgnoreCase("float"))) {
					this.setReturnTypeFlag(EvaluatorReturnType.Float);
				} else if (className.equalsIgnoreCase(Double.class.getName()) || (isPrimitive && m_resultClass.getName().equalsIgnoreCase("double"))) {
					this.setReturnTypeFlag(EvaluatorReturnType.Double);
				} else if (className.equalsIgnoreCase(Boolean.class.getName()) || (isPrimitive && m_resultClass.getName().equalsIgnoreCase("boolean"))) {
					this.setReturnTypeFlag(EvaluatorReturnType.Boolean);
				} else {
					this.setReturnTypeFlag(EvaluatorReturnType.Object);
				}
			}
		} catch (FunctionException ex) {
			FileLogger.debug(ex.getMessage());
		} catch (Exception ex) {
			FileLogger.debug(ex.getMessage());
			throw new ExpressionEvaluatorException(ex);
		}
	}

	/**
	 * 重载evalBoolean
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalBoolean()
	 */
	@Override
	public boolean evalBoolean() {
		if (this.m_resultClass != null && this.m_resultClass.isPrimitive() && this.m_resultClass.getName().equalsIgnoreCase("boolean")) { return boolean.class.cast(m_result); }
		if (this.m_result != null) return StringUtil.getValueBool(this.evalString(), false);
		return false;
	}

	/**
	 * 重载evalDouble
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalDouble()
	 */
	@Override
	public double evalDouble() {
		if (isNumber()) { return double.class.cast(this.m_result); }
		if (this.m_result != null) return StringUtil.getValueDouble(this.evalString(), 0.0D);
		return 0.0D;
	}

	/**
	 * 重载evalEnum
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalEnum()
	 */
	@Override
	public Enum<?> evalEnum() {
		if (this.m_resultClass != null && this.m_resultClass.isEnum()) { return Enum.class.cast(m_result); }
		return null;
	}

	/**
	 * 重载evalFloat
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalFloat()
	 */
	@Override
	public float evalFloat() {
		if (isNumber()) { return float.class.cast(this.m_result); }
		if (this.m_result != null) return StringUtil.getValueFloat(this.evalString(), 0.0F);
		return 0.0F;
	}

	/**
	 * 重载evalInt
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalInt()
	 */
	@Override
	public int evalInt() {
		if (isNumber()) { return int.class.cast(this.m_result); }
		if (this.m_result != null) return StringUtil.getValueInt(this.evalString(), 0);
		return 0;
	}

	/**
	 * 重载evalLong
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalLong()
	 */
	@Override
	public long evalLong() {
		if (isNumber()) { return long.class.cast(this.m_result); }
		if (this.m_result != null) return StringUtil.getValueLong(this.evalString(), 0L);
		return 0L;
	}

	/**
	 * 重载evalObject
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalObject()
	 */
	@Override
	public Object evalObject() {
		return m_result;
	}

	/**
	 * 重载evalString
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalString()
	 */
	@Override
	public String evalString() {
		if (this.m_result == null) return null;
		return this.m_result.toString();
	}

	/**
	 * 结果是否原始类型的数字值。
	 * 
	 * @return boolean
	 */
	private boolean isNumber() {
		boolean isNumber = (this.m_resultClass != null && this.m_resultClass.isPrimitive() && (this.m_resultClass.getName().equalsIgnoreCase("int") || this.m_resultClass.getName().equalsIgnoreCase("double") || this.m_resultClass.getName().equalsIgnoreCase("long") || this.m_resultClass.getName().equalsIgnoreCase("float")));
		return isNumber;
	}
}

