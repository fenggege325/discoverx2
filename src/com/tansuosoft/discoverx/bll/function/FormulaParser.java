/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import com.tansuosoft.discoverx.model.ParameterValueDataType;

/**
 * 解析单个公式及其参数信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
class FormulaParser {
	private ParsedFormula m_parsedFormula = null; // 解析后的公式
	private boolean m_parsed = false; // 是否已经执行过解析
	private String m_formula = null; // 公式内容的字符串
	private char chars[] = null; // 公式内容的字符数组
	private boolean m_isAtomic = false; // 是否原子级别（不包含其它变量）的公式
	private Map<VariableRange, String> vars = null; // 变量型参数对应的VariableRange对象与变量型参数具体内容对应的Map

	/**
	 * 缺省构造器。
	 */
	protected FormulaParser() {
	}

	/**
	 * 接收公式内容的构造器。
	 * 
	 * @param formula
	 */
	public FormulaParser(ParsedFormula formula) {
		m_parsedFormula = formula;
		if (m_parsedFormula == null) throw new RuntimeException("没有指定有效ParsedFormula！");
		this.m_formula = formula.getVariable();
		if (this.m_formula == null || this.m_formula.length() == 0) throw new RuntimeException("没有指定有效公式！");
		this.chars = this.m_formula.toCharArray(); // 公式内容字符数组

		this.m_isAtomic = this.m_parsedFormula.getVariableRange().getAtomic();
		if (!m_isAtomic) {
			ExpressionParser p = new ExpressionParser(this.m_formula);
			TreeSet<VariableRange> ranges = p.parseVariableRanges();
			vars = new HashMap<VariableRange, String>(ranges.size());
			for (VariableRange x : ranges) {
				if (x.getStart() > 0) vars.put(x, new String(chars, x.getStart(), x.getLength())/**/);
			}
		}

		this.m_parsed = false;
	}

	/**
	 * 返回原始公式内容。
	 * 
	 * @return
	 */
	public String getFormula() {
		return this.m_formula;
	}

	/**
	 * 解析并返回解析好的公式。
	 * 
	 * @return
	 */
	public ParsedFormula parse() {
		if (m_parsed) return this.m_parsedFormula;

		int len = chars.length; // 公式长度
		char c = 0; // 循环中的字符
		char cx = 0; // 循环中的字符
		boolean isLast = false; // 是否最后一个字符。
		boolean foundValidChar = false; // 发现有效字符的标记
		boolean quotFlag = false; // 参数值中的双引号（即文本类型参数值）开始标记。
		VariableRange subRange = null; // 是否下级变量位置及其长度。

		for (int i = 1; i < len; i++) {
			subRange = this.subRange(i);
			isLast = (i == (len - 1));
			c = chars[i];

			// 引号开始：字符为非转义引号或第一个字符为引号
			if (!quotFlag && c == '\"' && ((i > 0 && chars[i - 1] != ExpressionParser.ESC_CHAR) || i == 0)) {
				quotFlag = true;
				continue;
			}
			// 引号结束：已经有引号开始标记且最后一个字符为引号或非转义引号
			if (quotFlag && c == '\"' && (chars[i - 1] != ExpressionParser.ESC_CHAR || i == (len - 1))) {
				quotFlag = false;
				continue;
			}

			if (!quotFlag && c == ':' && !isLast && chars[i + 1] == '=' && subRange == null) {
				String paramName = null; // 参数名
				String paramValue = null; // 参数值
				int validCharPos = -1; // 有效字符开始位置。

				// 向前循环获取参数名
				foundValidChar = false;
				for (int j = (i - 1); j > 0; j--) {
					cx = chars[j];
					// 碰到第一个非空格型字符，表示参数名的结束位置
					if (!foundValidChar && cx != ' ' && cx != '\t') {
						foundValidChar = true;
						validCharPos = j;
					}
					// 第一个非空格型字符之后又碰到空格型字符或半角逗号、左括号等，表示参数名开始位置。
					if (foundValidChar && (cx == ' ' || cx == '\t' || cx == ',' || cx == '(')) {
						paramName = new String(chars, j + 1, validCharPos - j);
						break;
					}
				}// for end

				// 向后循环获取参数值
				boolean quotFlagInner = false;
				boolean isExpression = false;
				boolean isText = false;
				foundValidChar = false;
				validCharPos = -1;
				for (int j = (i + 2); j < len; j++) {
					cx = chars[j];
					// 找到有效字符
					if (!foundValidChar && cx != ' ' && cx != '\t') {
						validCharPos = j;
						foundValidChar = true;
					}

					// 找到有效字符，那么判断是否在下级公式中，如果是，那么跳过
					if (foundValidChar) {
						VariableRange vr = this.subRange(j);
						if (vr != null) {
							j += (vr.getLength() - 1);
							isExpression = true;
							continue;
						}
					}

					// 有效字符，且为文本型参数值开始的有效左双引号
					if (foundValidChar && !quotFlagInner && cx == '\"' && chars[j - 1] != ExpressionParser.ESC_CHAR) {
						quotFlagInner = true;
						continue;
					}
					// 非文本型参数值，已发现有效字符，如果碰到结尾字符（半角逗号、半角右括号），则表示非文本型参数结尾
					if (foundValidChar && !quotFlagInner && (cx == ',' || cx == ')')) {
						paramValue = new String(chars, validCharPos, j - validCharPos);
						break;
					}
					// 有效字符，且为文本型参数值结束的有效右双引号
					if (foundValidChar && quotFlagInner && cx == '\"' && chars[j - 1] != ExpressionParser.ESC_CHAR) {
						quotFlagInner = false;
						isText = true;
						continue;
					}
				}// for end

				this.m_parsedFormula.setParameter(paramName, paramValue, (isExpression ? ParameterValueDataType.Expression : (isText ? ParameterValueDataType.Text : null)));

			}// if end

		}// for end

		this.m_parsed = true;
		return this.m_parsedFormula;
	}// func end

	/**
	 * idx指定的字符位置序号是否在公式包含的下级公式范围内，如果是则返回VariableRange对象，否则返回null。
	 * 
	 * @param idx
	 * @return
	 */
	protected VariableRange subRange(int idx) {
		if (this.m_isAtomic) return null;
		for (VariableRange x : this.vars.keySet()) {
			if (idx >= x.getStart() && idx < x.getEnd()) return x;
		}
		return null;
	}
}

