/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 计算某个字段表达式变量并获取结果的类。
 * 
 * <p>
 * 如果是文档资源，从文档先字段中获取字段值字符串结果，如果没有找到，从资源额外参数中获取参数值字符串结果。<br/>
 * 如果是普通资源，先从和字段变量同名的get方法中获取结果，如果没有找到，从资源额外参数中获取参数值字符串结果。
 * </p>
 * <p>
 * <strong>注：通过字段、额外参数、属性值获取时，字段/参数/属性名通过{@link ParsedField.getFieldName}提供，其中属性名会自动加上get前缀并组合为符合属性命名规则的属性名称字符串。</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FieldVariableEvaluator extends Evaluator {
	private String m_stringValue = null;
	private Object m_objectValue = null;
	private int m_intValue = 0;
	private long m_longValue = 0;
	private float m_floatValue = 0.0F;
	private double m_doubleValue = 0.0D;
	private boolean m_booleanValue = false;
	private Enum<?> m_enumValue = null;
	private List<Object> m_values = null;

	/**
	 * 接收待计算的字段变量和要获取其字段值的资源的构造器。
	 * 
	 * @param parsedField ParsedField，必须。
	 * @param session Session，必须。
	 * @param resource Resource，可选。
	 */
	protected FieldVariableEvaluator(ParsedField parsedField, Session session, Resource resource) {
		m_values = new ArrayList<Object>(8);
		for (int i = 0; i < 8; i++) {
			m_values.add(null);
		}
		if (parsedField == null) return;
		Resource targetResource = resource;
		// 自动获取资源。
		if (session != null && targetResource == null) {
			targetResource = session.getLastResource();
			if (targetResource == null) targetResource = session.getLastDocument();
		}
		if (targetResource == null) {
			FileLogger.debug("无法获取字段表达式所处上下文资源对象！");
			this.m_stringValue = "";
			m_values.set(EvaluatorReturnType.String.getIntValue(), this.m_stringValue);
			this.setReturnTypeFlag(EvaluatorReturnType.String);
			return;
		}
		String fieldName = parsedField.getFieldName();

		// 从文档资源的字段中取
		if (targetResource instanceof Document) {
			Document doc = (Document) targetResource;
			if (doc.hasItem(fieldName)) {
				String s = doc.getItemValue(fieldName);
				this.m_stringValue = s;
				m_values.set(EvaluatorReturnType.String.getIntValue(), s);
				this.setReturnTypeFlag(EvaluatorReturnType.String);
				return;
			}
		}

		// 从属性中取
		try {
			Method m = targetResource.getClass().getMethod("get" + StringUtil.toPascalCase(fieldName));
			if (m != null) {
				Object result = m.invoke(targetResource);
				Class<?> cls = m.getReturnType();
				if (result != null && cls != null) {
					String className = cls.getName();
					boolean isPrimitive = cls.isPrimitive();
					if (className.equals(String.class.getName())) {
						this.m_stringValue = result.toString();
						m_values.set(EvaluatorReturnType.String.getIntValue(), result);
						this.setReturnTypeFlag(EvaluatorReturnType.String);
					} else if (cls.isEnum()) {
						this.m_enumValue = (Enum<?>) result;
						m_values.set(EvaluatorReturnType.Enum.getIntValue(), result);
						this.setReturnTypeFlag(EvaluatorReturnType.Enum);
					} else if (className.equalsIgnoreCase(Integer.class.getName()) || (isPrimitive && cls.getName().equalsIgnoreCase("int"))) {
						this.m_intValue = ((Integer) result).intValue();
						m_values.set(EvaluatorReturnType.Int.getIntValue(), result);
						this.setReturnTypeFlag(EvaluatorReturnType.Int);
					} else if (className.equalsIgnoreCase(Long.class.getName()) || (isPrimitive && cls.getName().equalsIgnoreCase("long"))) {
						this.m_longValue = ((Long) result).longValue();
						m_values.set(EvaluatorReturnType.Long.getIntValue(), result);
						this.setReturnTypeFlag(EvaluatorReturnType.Long);
					} else if (className.equalsIgnoreCase(Float.class.getName()) || (isPrimitive && cls.getName().equalsIgnoreCase("float"))) {
						this.m_floatValue = ((Float) result).floatValue();
						m_values.set(EvaluatorReturnType.Float.getIntValue(), result);
						this.setReturnTypeFlag(EvaluatorReturnType.Float);
					} else if (className.equalsIgnoreCase(Double.class.getName()) || (isPrimitive && cls.getName().equalsIgnoreCase("double"))) {
						this.m_doubleValue = ((Double) result).doubleValue();
						m_values.set(EvaluatorReturnType.Double.getIntValue(), result);
						this.setReturnTypeFlag(EvaluatorReturnType.Double);
					} else if (className.equalsIgnoreCase(Boolean.class.getName()) || (isPrimitive && cls.getName().equalsIgnoreCase("boolean"))) {
						this.m_booleanValue = ((Boolean) result).booleanValue();
						m_values.set(EvaluatorReturnType.Boolean.getIntValue(), result);
						this.setReturnTypeFlag(EvaluatorReturnType.Boolean);
					} else {
						this.m_objectValue = result;
						m_values.set(EvaluatorReturnType.Object.getIntValue(), result);
						this.setReturnTypeFlag(EvaluatorReturnType.Object);
					}
				}
			}
		} catch (Exception e) {
			if (CommonConfig.getInstance().getDebugable() && !(e instanceof NoSuchMethodException)) FileLogger.debug(e.getMessage());
		}
		// 找不到字段或属性值，则从额外参数中找。
		if (this.isEmpty()) {
			String pvs = targetResource.getParamValueString(fieldName, null);
			if (pvs != null) {
				this.m_stringValue = pvs;
				m_values.set(EvaluatorReturnType.String.getIntValue(), pvs);
				this.setReturnTypeFlag(EvaluatorReturnType.String);
			}
		}
		// 如果找不到任何值，则假定返回值类型为Object。
		if (this.isEmpty()) {
			this.setReturnTypeFlag(EvaluatorReturnType.Object);
		}
	}

	/**
	 * 重载evalBoolean
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalBoolean()
	 */
	@Override
	public boolean evalBoolean() {
		if (!this.checkReturnTypeFlag(EvaluatorReturnType.Boolean)) {
			for (EvaluatorReturnType t : EvaluatorReturnType.values()) {
				if (this.checkReturnTypeFlag(t)) {
					m_booleanValue = StringUtil.getValueBool(m_values.get(t.getIntValue()).toString(), false);
					break;
				}
			}
		}
		return this.m_booleanValue;
	}

	/**
	 * 重载evalDouble
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalDouble()
	 */
	@Override
	public double evalDouble() {
		if (!this.checkReturnTypeFlag(EvaluatorReturnType.Double)) {
			for (EvaluatorReturnType t : EvaluatorReturnType.values()) {
				if (this.checkReturnTypeFlag(t)) {
					int i = t.getIntValue();
					if (t == EvaluatorReturnType.Int) {
						this.m_doubleValue = (double) this.m_intValue;
					} else if (t == EvaluatorReturnType.Long) {
						this.m_doubleValue = (double) this.m_longValue;
					} else if (t == EvaluatorReturnType.Double) {
						this.m_doubleValue = (double) this.m_floatValue;
					} else {
						this.m_doubleValue = StringUtil.getValueDouble(m_values.get(i).toString(), 0.0D);
					}
					break;
				}
			}
		}
		return this.m_doubleValue;
	}

	/**
	 * 重载evalEnum
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalEnum()
	 */
	@Override
	public Enum<?> evalEnum() {
		return this.m_enumValue;
	}

	/**
	 * 重载evalFloat
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalFloat()
	 */
	@Override
	public float evalFloat() {
		if (!this.checkReturnTypeFlag(EvaluatorReturnType.Float)) {
			for (EvaluatorReturnType t : EvaluatorReturnType.values()) {
				if (this.checkReturnTypeFlag(t)) {
					int i = t.getIntValue();
					if (t == EvaluatorReturnType.Int) {
						this.m_floatValue = (float) this.m_intValue;
					} else if (t == EvaluatorReturnType.Long) {
						this.m_floatValue = (float) this.m_longValue;
					} else if (t == EvaluatorReturnType.Double) {
						this.m_floatValue = (float) this.m_doubleValue;
					} else {
						this.m_floatValue = StringUtil.getValueFloat(m_values.get(i).toString(), 0.0F);
					}
					break;
				}
			}
		}
		return this.m_floatValue;
	}

	/**
	 * 重载evalInt
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalInt()
	 */
	@Override
	public int evalInt() {
		if (!this.checkReturnTypeFlag(EvaluatorReturnType.Int)) {
			for (EvaluatorReturnType t : EvaluatorReturnType.values()) {
				if (this.checkReturnTypeFlag(t)) {
					int i = t.getIntValue();
					if (t == EvaluatorReturnType.Long) {
						this.m_intValue = (int) this.m_longValue;
					} else if (t == EvaluatorReturnType.Float) {
						this.m_intValue = (int) this.m_floatValue;
					} else if (t == EvaluatorReturnType.Double) {
						this.m_intValue = (int) this.m_doubleValue;
					} else {
						this.m_intValue = StringUtil.getValueInt(m_values.get(i).toString(), 0);
					}
					break;
				}
			}
		}
		return this.m_intValue;
	}

	/**
	 * 重载evalLong
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalLong()
	 */
	@Override
	public long evalLong() {
		if (!this.checkReturnTypeFlag(EvaluatorReturnType.Long)) {
			for (EvaluatorReturnType t : EvaluatorReturnType.values()) {
				if (this.checkReturnTypeFlag(t)) {
					int i = t.getIntValue();
					if (t == EvaluatorReturnType.Int) {
						this.m_longValue = (long) this.m_intValue;
					} else if (t == EvaluatorReturnType.Float) {
						this.m_longValue = (long) this.m_floatValue;
					} else if (t == EvaluatorReturnType.Double) {
						this.m_longValue = (long) this.m_doubleValue;
					} else {
						this.m_longValue = StringUtil.getValueLong(m_values.get(i).toString(), 0L);
					}
					break;
				}
			}
		}
		return this.m_longValue;
	}

	/**
	 * 重载evalObject
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalObject()
	 */
	@Override
	public Object evalObject() {
		if (this.m_objectValue == null) {
			for (EvaluatorReturnType t : EvaluatorReturnType.values()) {
				if (this.checkReturnTypeFlag(t)) {
					this.m_objectValue = m_values.get(t.getIntValue());
					break;
				}
			}
		}
		return this.m_objectValue;
	}

	/**
	 * 重载evalString
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.Evaluator#evalString()
	 */
	@Override
	public String evalString() {
		if (this.m_stringValue == null) {
			for (EvaluatorReturnType t : EvaluatorReturnType.values()) {
				if (this.checkReturnTypeFlag(t)) {
					this.m_stringValue = m_values.get(t.getIntValue()).toString();
					break;
				}
			}
		}
		return m_stringValue;
	}

}

