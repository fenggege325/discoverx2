/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.List;

import com.tansuosoft.discoverx.model.Parameter;

/**
 * 表示某一个系统功能函数包含的预设参数配置相关信息的类。
 * 
 * <p>
 * 它对应{@link FunctionPredefinedParamsConfig}中的某一条配置项。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FunctionPredefinedParams {
	/**
	 * 缺省构造器。
	 */
	public FunctionPredefinedParams() {
	}

	private String m_key = null;
	private String m_inheritance = null; // 表示预设参数列表将继承自另一个函数的全限定类名。
	private List<Parameter> m_predefinedParams = null; // 系统功能函数预设的参数列表。
	private String m_description = null; // 说明信息。

	/**
	 * 返回系统功能函数信息。
	 * 
	 * @return String
	 */
	public String getKey() {
		return this.m_key;
	}

	/**
	 * 设置系统功能函数信息，必须且唯一。
	 * 
	 * <p>
	 * 此信息必须是系统功能函数的全限定类名、系统功能函数对应配置资源的表达式名（即@+不带参数的别名）、系统功能函数对应配置资源的unid这三种信息中的一个。
	 * </p>
	 * 
	 * @param key String
	 */
	public void setKey(String key) {
		this.m_key = key;
	}

	/**
	 * 返回继承的key值。
	 * 
	 * <p>
	 * 表示预设参数列表来自另一个{@link FunctionPredefinedParams}对象时，这个对象的key值。
	 * </p>
	 * 
	 * @return String
	 */
	public String getInheritance() {
		return this.m_inheritance;
	}

	/**
	 * 设置继承的key值。
	 * 
	 * @param inheritance String
	 */
	public void setInheritance(String inheritance) {
		this.m_inheritance = inheritance;
	}

	/**
	 * 返回key指定的系统功能函数预设的参数列表。
	 * 
	 * @return List&lt;Parameter&gt;
	 */
	public List<Parameter> getPredefinedParams() {
		return this.m_predefinedParams;
	}

	/**
	 * 设置functionImplement指定的系统功能函数预设的参数列表。
	 * 
	 * <p>
	 * 此处包含的{@link Parameter}只要在配置中提供必要的参数名、参数说明等，其余的（预设）参数值、类型（默认为文本）等可选。
	 * </p>
	 * 
	 * @param predefinedParams List&lt;Parameter&gt;
	 */
	public void setPredefinedParams(List<Parameter> predefinedParams) {
		this.m_predefinedParams = predefinedParams;
	}

	/**
	 * 返回说明信息。
	 * 
	 * @return String
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置说明信息，可选。
	 * 
	 * @param description String
	 */
	public void setDescription(String description) {
		this.m_description = description;
	}

}

