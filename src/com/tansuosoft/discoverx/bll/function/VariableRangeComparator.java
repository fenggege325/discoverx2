/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import java.util.Comparator;

/**
 * 确定表达式变量在整个表达式中的位置先后的比较实现类。
 * 
 * @author coca
 */
public class VariableRangeComparator implements Comparator<VariableRange> {
	/**
	 * 缺省构造器。
	 */
	public VariableRangeComparator() {
	}

	/**
	 * 两个表达式变量位置的比较，比较方式：如果都为null或位置相同，则返回0，如果o1在o2的右边则返回1，o1在o2的中间或左边则返回-1。
	 */
	@Override
	public int compare(VariableRange o1, VariableRange o2) {
		if (o1 == null && o2 == null) return 0;
		if (o1 == null && o2 != null) return -1;
		if (o1 != null && o2 == null) return 1;
		int start1 = o1.getStart();
		int end1 = o1.getEnd();
		int start2 = o2.getStart();
		int end2 = o2.getEnd();
		if (start1 == start2 && end1 == end2) return 0;
		if (start1 > end2) return 1; // 在右边
		if (end1 < start2) return -1; // 在左边
		if (start1 > start2 && end1 < end2) {
			o2.setAtomic(false);
			o1.setIndependent(false);
			return -1; // o1在o2中间
		}
		if (start1 < start2 && end1 > end2) {
			o1.setAtomic(false);
			o2.setIndependent(false);
			return 1; // o2在o1中间
		}
		return 0;
	}

}

