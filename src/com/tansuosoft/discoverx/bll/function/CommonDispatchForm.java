/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;

/**
 * 系统功能函数统一处理http功能请求时，指示具体调用哪一个系统功能函数的请求参数包装类。
 * 
 * @see CommonForm
 * @author coca@tansuosoft.cn
 */
public class CommonDispatchForm extends CommonForm {

	/**
	 * 缺省构造器。
	 */
	public CommonDispatchForm() {
	}

	private String m_directory = null; // 处理的资源的目录名。
	private String m_UNID = null; // 处理的资源的UNID。
	private String m_function = null; // 要调用的系统功能函数表达式或UNID

	/**
	 * 返回处理的资源的目录名。
	 * 
	 * <p>
	 * 从名为“directory”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String 处理的资源的目录名。
	 */
	public String getDirectory() {
		return this.m_directory;
	}

	/**
	 * 设置处理的资源的目录名。
	 * 
	 * <p>
	 * 从名为“directory”的http请求参数中获取。
	 * </p>
	 * 
	 * @param directory String 处理的资源的目录名。
	 */
	public void setDirectory(String directory) {
		this.m_directory = directory;
	}

	/**
	 * 返回处理的资源的UNID。
	 * 
	 * <p>
	 * 从名为“unid”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String 处理的资源的UNID。
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置处理的资源的UNID。
	 * 
	 * <p>
	 * 从名为“unid”的http请求参数中获取。
	 * </p>
	 * 
	 * @param UNID String 处理的资源的UNID。
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回要调用的系统功能函数表达式或UNID
	 * 
	 * <p>
	 * 从名为“function”或“fx”的http请求参数中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getFunction() {
		return this.m_function;
	}

	/**
	 * 设置要调用的系统功能函数表达式或UNID
	 * 
	 * <p>
	 * 从名为“function”或“fx”的http请求参数中获取。
	 * </p>
	 * 
	 * @param function String
	 */
	public void setFunction(String function) {
		this.m_function = function;
	}

	/**
	 * 根据directory和unid属性获取资源对象实例。
	 * 
	 * @return Resource
	 */
	public Resource getResource() {
		if (this.m_directory == null || this.m_directory.length() == 0 || this.m_UNID == null || this.m_UNID.length() == 0) return null;
		Resource resource = ResourceContext.getInstance().getResource(this.m_UNID, this.m_directory);
		return resource;
	}

	/**
	 * 重载fillWebRequestForm：填充属性。
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.CommonForm#fillWebRequestForm(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public CommonForm fillWebRequestForm(HttpServletRequest request) {
		if (request == null) return null;
		String directory = request.getParameter("directory");
		String unid = request.getParameter("unid");
		String function = request.getParameter("function");
		if (function == null || function.length() == 0) function = request.getParameter("fx");
		if (function == null || function.length() == 0) function = Operation.SYSTEM_DEFAULT_OPERATION; // 默认为打开资源。
		this.setDirectory(directory);
		this.setUNID(unid);
		this.setFunction(function);
		return this;
	}
}

