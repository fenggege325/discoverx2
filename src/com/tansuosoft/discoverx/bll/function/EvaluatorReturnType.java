/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.function;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 表达式计算后获取到的直接结果的数据类型枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum EvaluatorReturnType implements EnumBase {
	/**
	 * 返回字符串（0）。
	 */
	String(0),
	/**
	 * 返回对象（1）。
	 */
	Object(1),
	/**
	 * 返回int（2）。
	 */
	Int(2),
	/**
	 * 返回long（3）。
	 */
	Long(3),
	/**
	 * 返回float（4）。
	 */
	Float(4),
	/**
	 * 返回double（5）。
	 */
	Double(5),
	/**
	 * 返回boolean（6）。
	 */
	Boolean(6),
	/**
	 * 返回Enum&lt;?&gt;（7）。
	 */
	Enum(7);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	EvaluatorReturnType(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return java.lang.String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return EvaluatorReturnType
	 */
	public EvaluatorReturnType parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (EvaluatorReturnType s : EvaluatorReturnType.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return EvaluatorReturnType.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return EvaluatorReturnType
	 */
	public static EvaluatorReturnType parse(int v) {
		for (EvaluatorReturnType s : EvaluatorReturnType.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

