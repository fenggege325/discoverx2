/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.serialization.Serializer;
import com.tansuosoft.discoverx.util.serialization.XmlSerializer;

/**
 * 根据资源对象提供资源序列化为对象实例的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceSerializerProvider {

	/**
	 * 根据资源类型信息提供资源xml序列化对象实例。
	 * 
	 * @param cls
	 * @return
	 */
	public static Serializer getResourceSerializer(Class<?> cls) {
		if (cls == null) return null;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(cls);
		if (rd == null) throw new RuntimeException("找不到对应资源描述信息！");
		String cfgClassName = rd.getSerializer();
		if (cfgClassName == null || cfgClassName.length() == 0) { return new XmlSerializer(); }
		if (cfgClassName != null && cfgClassName.length() > 0) {
			Serializer obj = Instance.newInstance(cfgClassName, Serializer.class);
			if (obj != null) {
				return obj;
			} else {
				return new XmlSerializer();
			}
		}
		return null;
	}
}

