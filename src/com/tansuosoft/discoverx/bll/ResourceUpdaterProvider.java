/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import com.tansuosoft.discoverx.bll.resource.DBResourceUpdater;
import com.tansuosoft.discoverx.bll.resource.XmlResourceUpdater;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Resource;

/**
 * 根据资源对象提供资源更新对象实例的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceUpdaterProvider {

	/**
	 * 根据资源对象提供资源更新对象实例。
	 * 
	 * @param r Resource
	 * @return
	 */
	public static ResourceUpdater getResourceUpdater(Resource r) {
		if (r == null) return null;
		Class<?> cls = r.getClass();
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(cls);
		if (rd == null) throw new RuntimeException("无法获取资源描述信息！");

		if (rd.getXmlStore()) {
			return new XmlResourceUpdater();
		} else {
			return new DBResourceUpdater();
		}
	}
}

