/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.DocumentType;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemValueComputationMethod;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 提供应用程序配置文件相关信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class ApplicationProfile {
	private Application m_application = null;
	private Document m_profile = null;
	private static Object m_lock = new Object();
	private static Map<String, ApplicationProfile> m_instances = new HashMap<String, ApplicationProfile>();
	private static final Session s = Session.getSystemSession();
	private int contextSc = OrganizationsContext.getInstance().getContextUserOrgSc();

	/**
	 * 接收{@link Application}的构造器。
	 * 
	 * @param app
	 */
	private ApplicationProfile(Application app) {
		this.m_application = app;
		if (m_application == null) throw new RuntimeException("没有提供有效的应用程序信息！");
		String profileFormUNID = m_application.getProfileForm();
		if (profileFormUNID == null || profileFormUNID.isEmpty()) return;
		final Form profileForm = ResourceContext.getInstance().getResource(profileFormUNID, Form.class);
		if (profileForm == null) {
			FileLogger.debug("无法获取“%1$s”绑定的配置文件表单！", m_application.getName());
			return;
		}
		String profile = this.m_application.getProfile();
		if (profile != null && profile.length() > 0) {
			m_profile = DocumentBuffer.getInstance().getDocument(profile);
			if (m_profile == null) m_profile = ResourceContext.getInstance().getResource(profile, Document.class);
			if (m_profile != null && !("" + contextSc).equals(m_profile.getTag())) m_profile = null;
		}
		if (m_profile == null) {
			// 先从数据库中尝试查找之前的配置文档unid
			DBRequest dbr = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper r = new SQLWrapper();
					r.setSql("select c_unid from t_document where c_formalias=? and c_oid=? order by c_created desc");
					r.setParameterized(true);
					return r;
				}
			};
			dbr.setParametersSetter(new ParametersSetter() {
				@Override
				public void setParameters(DBRequest request, CommandWrapper cw) {
					try {
						cw.setString(1, profileForm.getAlias());
						cw.setInt(2, contextSc);
					} catch (SQLException e) {
						FileLogger.error(e);
					}
				}
			});
			dbr.setResultBuilder(new ResultBuilder() {
				@Override
				public Object build(DBRequest request, Object rawResult) {
					if (rawResult == null || !(rawResult instanceof DataReader)) return null;
					DataReader dr = (DataReader) rawResult;
					String r = null;
					try {
						while (dr.next()) {
							r = dr.getString(1);
							if (r != null && r.length() > 0) break;
						}
					} catch (SQLException ex) {
						FileLogger.error(ex);
					}
					return r;
				}
			});
			dbr.sendRequest();
			profile = dbr.getResult(String.class);
			m_profile = DocumentBuffer.getInstance().getDocument(profile);
			if (m_profile == null) m_profile = ResourceContext.getInstance().getResource(profile, Document.class);

			if (m_profile == null) {// 都找不到则重建一个
				constructProfile();
				if (m_profile != null && !ResourceHelper.save(m_profile, s)) m_profile = null;
			}
			if (m_profile != null && !m_profile.getUNID().equalsIgnoreCase(m_application.getProfile())) {
				m_application.setProfile(m_profile.getUNID());
				ResourceUpdater updater = ResourceUpdaterProvider.getResourceUpdater(m_application);
				updater.update(m_application, s);
			}
		}
	}

	/**
	 * 构造应用程序对应的配置文档。
	 */
	protected void constructProfile() {
		String profileFormId = m_application.getProfileForm();
		Form profileForm = ResourceContext.getInstance().getResource(profileFormId, Form.class);
		if (profileForm == null) return;
		String unid = m_application.getUNID();
		User user = null;
		try {
			user = s.getUser();
			// 文档绑定的资源
			String configResource = unid;
			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(Application.class);

			this.m_profile = Document.newBlankDocument();

			// 构造基础属性
			this.m_profile.setState(DocumentState.New.getIntValue());
			this.m_profile.setCreator(user.getName());
			this.m_profile.setModifier(user.getName());
			this.m_profile.setPUNID(null);
			if (!configResource.startsWith(rd.getDirectory())) configResource = (rd.getDirectory() + ":" + configResource);
			this.m_profile.setConfigResource(configResource);
			this.m_profile.setType(DocumentType.Profile);
			this.m_profile.setTag(contextSc + "");

			// 构造安全属性
			Security security = this.m_profile.getSecurity();
			if (security == null) {
				security = new Security(this.m_profile.getUNID());
				this.m_profile.setSecurity(security);
			} else {
				security.setPUNID(this.m_profile.getUNID());
			}
			if (security.getSecurityEntries() == null) security.setSecurityEntries(new ArrayList<SecurityEntry>());
			security.setSecurityLevel(user.getSecurityCode(), SecurityLevel.Author.getIntValue() | SecurityLevel.View.getIntValue());
			security.setWorkflowLevel(user.getSecurityCode(), 0);

			// 构造category、title属性。
			Application app = null;
			int appCount = this.m_profile.getApplicationCount();
			for (int i = 1; i <= appCount; i++) {
				app = this.m_profile.getApplication(i);
				if (app == null) continue;
				if (i == 1) {
					this.m_profile.setCategory(app.getName());
					this.m_profile.setTitle(app.getName());
				} else {
					String category = this.m_profile.getCategory();
					this.m_profile.setCategory(String.format("%1$s%2$s%3$s", (category == null ? "" : category), (category == null || category.length() == 0 ? "" : "\\"), app.getName()));
				}
				if (i == appCount) this.m_profile.setBusinessTitle(app.getName());
			}// for end

			// 构造字段属性
			this.m_profile.setFormAlias(profileForm.getAlias());

			String defaultValueConfig = null;
			String itemName = null;
			String itemValue = null;
			List<Item> formItems = profileForm.getItems();
			if (formItems == null || formItems.isEmpty()) throw new ResourceException("文档构造错误：文档所属表单中没有字段信息！");
			for (Item x : formItems) {
				itemName = x.getItemName();
				defaultValueConfig = x.getDefaultValue();
				// 处理创建时计算
				if ((defaultValueConfig == null || defaultValueConfig.length() == 0) && x.getComputationMethod() == ItemValueComputationMethod.Create) {
					defaultValueConfig = x.getComputationExpression();
				}
				if (defaultValueConfig != null && defaultValueConfig.length() > 0) {
					try {
						ExpressionEvaluator eval = new ExpressionEvaluator(defaultValueConfig, s, this.m_profile);
						itemValue = eval.evalString();
					} catch (Exception ex) {
						itemValue = "";
					}
					this.m_profile.setFieldValue(itemName, itemValue);
				} else {
					this.m_profile.setFieldValue(itemName, null);
				}
			}
		} catch (Exception ex) {
			if (ex instanceof ResourceException) throw (ResourceException) ex;
			throw new ResourceException(ex.getMessage(), ex);
		}

		// 添加到文档缓存
		DocumentBuffer.getInstance().setDocument(this.m_profile, user.getName());
	}

	/**
	 * 根据{@link Application}对象获取其配对的{@link ApplicationProfile}对象实例。
	 * 
	 * @param app
	 * @return ApplicationProfile
	 */
	public static ApplicationProfile getInstance(Application app) {
		if (app == null) return null;
		ApplicationProfile instance = null;
		int orgsc = OrganizationsContext.getInstance().getContextUserOrgSc();
		String key = app.getUNID() + orgsc;
		synchronized (m_lock) {
			instance = m_instances.get(key);
			if (instance == null) {
				instance = new ApplicationProfile(app);
				m_instances.put(key, instance);
			}
		}
		return instance;
	}

	/**
	 * 返回应用程序对应的配置文档。
	 * 
	 * <p>
	 * 如果应用程序没有配置有效地配置文件表单，则返回null。
	 * </p>
	 * 
	 * @return Document
	 */
	public Document getProfile() {
		return m_profile;
	}

	/**
	 * 清除doc对应的配置文档缓存。
	 * 
	 * @param doc
	 * @return 返回被移除的配置文档（如果doc为null或doc非配置文档则返回null）。
	 */
	public static Document clear(Document doc) {
		if (doc == null || doc.getType() != DocumentType.Profile) return null;
		return clear(doc.getApplication(doc.getApplicationCount()));
	}

	/**
	 * 清除app指定的应用程序对应的配置文档缓存。
	 * 
	 * @param app
	 * @return 返回被移除的配置文档（如果不存在则返回null）。
	 */
	public synchronized static Document clear(Application app) {
		if (app == null) return null;
		ApplicationProfile appp = m_instances.remove(app.getUNID());
		Document result = null;
		if (appp != null) {
			result = appp.m_profile;
			appp.m_profile = null;
		}
		return result;
	}
}

