/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.util.Comparator;

import com.tansuosoft.discoverx.model.Parameter;

/**
 * 按参数名字符串升序排序的{@link Parameter}实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ParameterComparator implements Comparator<Parameter> {

	/**
	 * 重载compare:返回{@link Parameter}的比较结果。
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Parameter o1, Parameter o2) {
		if (o1 == null && o2 == null) return 0;
		if (o1 != null && o2 == null) return 1;
		if (o1 == null && o2 != null) return -1;
		if (o1.getName() == null && o2.getName() == null) return 0;
		if (o1.getName() != null && o2.getName() == null) return 1;
		if (o1.getName() == null && o2.getName() != null) return -1;
		return o1.getName().compareToIgnoreCase(o2.getName());
	}
}

