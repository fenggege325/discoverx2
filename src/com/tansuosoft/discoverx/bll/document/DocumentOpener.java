/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.document;

import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.ResourceOpener;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemValueComputationMethod;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.serialization.Deserializer;

/**
 * 打开文档资源的实现类。
 * 
 * <p>
 * 打开前后会执行事件，包括判断是否有打开权限。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DocumentOpener extends ResourceOpener {

	private Map<String, Object> m_options = null;

	/**
	 * 设置文档打开选项参数。
	 * 
	 * <p>
	 * 如果文档还没有在缓存中的话参数信息也会被传递给反序列化文档的数据库请求对象，同时提供的参数信息也可以在本对象中使用。<br/>
	 * 特别注意的是，通过{@link ResourceContext#getResource(String, Class)}等方法获取未缓存的文档资源时是不能传递额外参数的。<br/>
	 * 此方法通常用于传递文档打开(反序列化)时的选项。
	 * </p>
	 * 
	 * @param options
	 */
	public void setOpenOptions(Map<String, Object> options) {
		m_options = options;
	}

	/**
	 * 打开文档。
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceOpener#open(java.lang.String, java.lang.Class, com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Resource open(String unid, Class<?> cls, Session session) throws ResourceException {
		Document doc = null;
		try {
			DocumentBuffer buffer = DocumentBuffer.getInstance();
			User user = Session.getUser(session);
			String userFullName = user.getName();
			doc = buffer.getDocument(unid, userFullName);
			boolean isFromBuffer = true;
			if (doc == null) {
				isFromBuffer = false;
				ResourceDescriptor descriptor = ResourceDescriptorConfig.getInstance().getResourceDescriptor(Document.class);
				if (descriptor == null) throw new RuntimeException("无法获取文档反序列化对象。");
				Deserializer deserializer = (Deserializer) Instance.newInstance(descriptor.getDeserializer());
				if (m_options != null && deserializer instanceof DBRequest) {
					DBRequest dbr = (DBRequest) deserializer;
					for (String n : m_options.keySet()) {
						dbr.setParameter(n, m_options.get(n));
					}
				}
				Object obj = deserializer.deserialize(unid, Document.class);
				if (obj != null && obj instanceof Document) {
					doc = (Document) obj;
				}
				if (doc == null) return null;
			}

			// 注册事件
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Document).provide(doc, null);
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_OPEN, EventHandler.EHT_OPENED), eventHandlerInfoList);
			ResourceEventArgs arg = new ResourceEventArgs(doc, session);
			this.callEventHandler(EventHandler.EHT_OPEN, arg); // 打开开始。

			// 判断权限。
			boolean canOpen = SecurityHelper.authorize(session, doc, SecurityLevel.View);
			if (!canOpen) { throw new ResourceException("对不起，“" + userFullName + "”没有访问资源的权限！"); }

			if (!isFromBuffer) buffer.setDocument(doc, userFullName); // 如果不是来自文档缓存，则要把新打开的文档加到缓存。

			// 设置最近的文档为打开的文档。
			if (session != null) session.setLastDocument(doc);
			// 打开时计算的域值刷新。
			Form f = doc.getForm();
			List<Item> items = (f == null ? null : f.getItems());
			if (items != null) {
				String exp = null;
				ExpressionEvaluator evaluator = null;
				for (Item x : items) {
					if (x == null || x.getComputationMethod() != ItemValueComputationMethod.Open) continue;
					exp = x.getComputationExpression();
					if (exp == null || exp.length() == 0) continue;
					evaluator = new ExpressionEvaluator(exp, session, doc);
					doc.replaceItemValue(x.getItemName(), evaluator.evalString());
				}
			}

			this.callEventHandler(EventHandler.EHT_OPENED, arg); // 打开完成
		} catch (Exception ex) {
			throw new ResourceException(ex);
		}
		return doc;
	}
}

