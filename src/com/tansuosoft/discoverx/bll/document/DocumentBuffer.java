/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.document;

import java.util.ArrayList;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.ResourceContext;

/**
 * 缓存并管理所有已打开的文档的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DocumentBuffer {
	/**
	 * 文档缓存的缓存名称常数。
	 */
	protected static final String DOCUMENT_CACHE_NAME = "documentCache";

	private CacheManager m_cacheManager = null; // 缓存管理器
	private Cache m_cache = null; // 缓存。

	/**
	 * 私有构造器。
	 */
	private DocumentBuffer() {
		int capacity = CommonConfig.getInstance().getDocumentCacheCapacity();
		if (capacity == 0) capacity = 500;
		this.m_cacheManager = CacheManager.getInstance();
		this.m_cache = new Cache(DOCUMENT_CACHE_NAME, // java.lang.String name,
		capacity, // int maxElementsInMemory,
		MemoryStoreEvictionPolicy.LRU, // MemoryStoreEvictionPolicy memoryStoreEvictionPolicy,
		false, // boolean overflowToDisk,
		null, // java.lang.String diskStorePath,
		true, // boolean eternal,
		120, // long timeToLiveSeconds,
		120, // long timeToIdleSeconds,
		false, // boolean diskPersistent,
		36000, // long diskExpiryThreadIntervalSeconds,
		null, // RegisteredEventListeners registeredEventListeners,
		null, // BootstrapCacheLoader bootstrapCacheLoader,
		0, // int maxElementsOnDisk,
		30 // int diskSpoolBufferSizeMB
		);
		this.m_cacheManager.addCache(this.m_cache);
	}

	private static DocumentBuffer m_instance = null;

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return DocumentBuffer
	 */
	public static DocumentBuffer getInstance() {
		synchronized (ResourceContext.class) {
			if (m_instance == null) {
				m_instance = new DocumentBuffer();
			}
		}
		return m_instance;
	}

	/**
	 * 设置一个文档和其持有者到缓存中。
	 * 
	 * @param doc 要增加到缓存的文档，必须。
	 * @param userFullName 增加文档到缓存的用户全名（持有者）。
	 * @return CachedDocument 返回持有增加的文档和持有者信息的CachedDocument对象，如果异常则返回null。
	 */
	public boolean setDocument(Document doc, String userFullName) {
		if (doc == null) return false;
		CachedDocument cd = null;
		try {
			Element el = this.m_cache.get(doc.getUNID());
			if (el != null) cd = (CachedDocument) el.getObjectValue();
			if (cd != null) {
				cd.addHolder(userFullName);
			} else {
				cd = new CachedDocument(doc, userFullName);
				this.m_cache.put(new Element(doc.getUNID(), cd));
			}
		} catch (CacheException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
			return false;
		} catch (IllegalStateException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
			return false;
		} catch (IllegalArgumentException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
			return false;
		} catch (Exception ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
			return false;
		}
		return true;
	}

	/**
	 * 根据文档unid和用户全名获取缓存中的文档。
	 * 
	 * <p>
	 * 此方法会将用户全名记录到文档持有者集合中。
	 * </p>
	 * 
	 * @param unid 文档unid
	 * @param userFullName 用户全名。
	 * @return Document 返回缓存中的文档，如果unid为空或没有对应的缓存文档，则返回null。
	 */
	public Document getDocument(String unid, String userFullName) {
		Element el = this.m_cache.get(unid);
		if (el == null) return null;
		CachedDocument cd = (CachedDocument) el.getObjectValue();
		if (cd == null) return null;
		return cd.getDocument(userFullName);
	}

	/**
	 * 根据文档unid获取缓存中的文档。
	 * 
	 * @param unid
	 * @return
	 */
	public Document getDocument(String unid) {
		return getDocument(unid, null);
	}

	/**
	 * 返回缓存的所有文档。
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Document> getAllDocuments() {
		List<String> keys = this.m_cache.getKeys();
		if (keys == null) return null;
		List<Document> docs = new ArrayList<Document>(keys.size());
		Element el = null;
		for (String s : keys) {
			el = this.m_cache.get(s);
			if (el == null) continue;
			docs.add(((CachedDocument) el.getObjectValue()).getDocument());
		}
		return docs;
	}

	/**
	 * 为缓存的文档绑定额外信息。
	 * 
	 * @param docUNID 文档UNID，必须。
	 * @param key 额外信息名称，必须
	 * @param value 额外信息值
	 * @return 返回原来的绑定值（如果存在的话）。
	 */
	public Object setBindObject(String docUNID, String key, Object value) {
		if (docUNID == null || docUNID.length() == 0 || key == null || key.length() == 0) return null;
		Element el = this.m_cache.get(docUNID);
		if (el == null) return null;
		CachedDocument cd = (CachedDocument) el.getObjectValue();
		if (cd == null) return null;
		return cd.setBindObject(key, value);
	}

	/**
	 * 为缓存的文档获取已绑定的额外信息。
	 * 
	 * @param docUNID 文档UNID，必须。
	 * @param key 额外信息名称，必须
	 * @param value
	 * @return
	 */
	public Object getBindObject(String docUNID, String key) {
		if (docUNID == null || docUNID.length() == 0 || key == null || key.length() == 0) return null;
		Element el = this.m_cache.get(docUNID);
		if (el == null) return null;
		CachedDocument cd = (CachedDocument) el.getObjectValue();
		if (cd == null) return null;
		return cd.getBindObject(key);
	}

	/**
	 * 从unid对应的缓存的CachedDocument对象的持有者信息中删除指定用户全名对应的持有者。
	 * 
	 * <p>
	 * 如果文档没有任何持有者，那么将CachedDocument移出缓存。
	 * </p>
	 * 
	 * @param unid String 文档unid。
	 * @param userFullName String 用户全名。
	 * 
	 * @return Document 返回被移除了指定持有者的文档。
	 */
	public Document removeHolder(String unid, String userFullName) {
		if (userFullName == null || userFullName.length() == 0) return null;
		Element el = this.m_cache.get(unid);
		if (el == null) return null;
		CachedDocument cd = (CachedDocument) el.getValue();
		if (cd == null) return null;
		Document doc = cd.getDocument(userFullName);
		cd.removeHolder(userFullName);
		if (!cd.hasHolder()) this.m_cache.remove(unid);
		return doc;
	}

	/**
	 * 从缓存中移除unid对应的文档及其关联的信息。
	 * 
	 * @param unid 要移除的文档UNID，必须。
	 * @return Document 如果成功则返回被移除的文档，否则返回null。
	 */
	public Document removeDocument(String unid) {
		if (unid == null || unid.length() == 0) return null;
		Element el = this.m_cache.get(unid);
		if (el == null) return null;
		CachedDocument cd = (CachedDocument) el.getValue();
		if (cd == null) return null;
		Document doc = cd.getDocument(null);
		cd.clear();
		this.m_cache.remove(unid);
		return doc;
	}

	/**
	 * 清除所有缓存。
	 */
	public void clearCache() {
		this.m_cache.removeAll();
	}

	/**
	 * 关闭缓存。
	 */
	public void shutdown() {
		clearCache();
		this.m_cacheManager.shutdown();
		this.m_cacheManager = null;
	}

	/**
	 * 获取缓存的资源数量。
	 * 
	 * @return long
	 */
	public long getCachedCount() {
		return m_cache.getStatistics().getObjectCount();
	}
}

