/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.document;

import java.io.Serializable;
import java.util.HashMap;

import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.util.DateTime;

/**
 * 描述已打开且被缓存的文档信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
class CachedDocument implements Serializable {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 4932521649636082980L;
	private Document m_document = null; // 被缓存的文档。
	private HashMap<String, String> m_holders = null; // 打开此文档的人员全名和最近访问时间的映射集合。
	private HashMap<String, Object> m_bindObjects = null; // 缓存的文档绑定的额外信息，按额外信息的唯一名称与具体额外信息意义对应。

	/**
	 * 接收被缓存的文档资源的构造器。
	 * 
	 * @param document
	 */
	public CachedDocument(Document document, String userFullName) {
		if (document == null || userFullName == null || userFullName.length() == 0) return;
		this.m_document = document;
		m_holders = new HashMap<String, String>();
		m_holders.put(userFullName, DateTime.getNowDTString());
	}

	/**
	 * 为指定用户返回缓存的文档。
	 * 
	 * <p>
	 * 如果指定了有效的用户名，那么会把用户加到文档持有者哈希集合中。
	 * </p>
	 * 
	 * @param userFullName String
	 * @return Document 被缓存的文档。
	 */
	public Document getDocument(String userFullName) {
		addHolder(userFullName);
		return this.m_document;
	}

	/**
	 * 返回缓存的文档。
	 * 
	 * @return Document 被缓存的文档。
	 */
	protected Document getDocument() {
		return this.m_document;
	}

	/**
	 * 追加一个持有者信息。
	 * 
	 * @param userFullName
	 */
	public void addHolder(String userFullName) {
		if (userFullName == null || userFullName.length() <= 0) return;
		this.m_holders.put(userFullName, DateTime.getNowDTString());
	}

	/**
	 * 从此文档的持有者信息中删除指定用户全名对应的持有者。
	 * 
	 * @param userFullName String
	 */
	public void removeHolder(String userFullName) {
		if (userFullName == null || userFullName.length() == 0) return;
		this.m_holders.remove(userFullName);
	}

	/**
	 * 返回此文档是否还有持有者。
	 * 
	 * @return boolean
	 */
	public boolean hasHolder() {
		return !(this.m_holders == null || this.m_holders.isEmpty());
	}

	/**
	 * 获取此缓存的文档的所有的持有者（正在访问此文档的用户）信息。
	 * 
	 * <p>
	 * 用户全名保存在Hashtable的key中，最近访问时间保存在Hashtable的value中。
	 * </p>
	 * 
	 * @return Hashtable<String,String>
	 */
	public HashMap<String, String> getHolders() {
		return this.m_holders;
	}

	/**
	 * 绑定额外信息。
	 * 
	 * @param key 额外信息名称，必须
	 * @param value 额外信息值
	 * @return 返回原来的绑定值（如果存在的话）。
	 */
	protected Object setBindObject(String key, Object value) {
		if (key == null || key.length() == 0) return null;
		if (this.m_bindObjects == null) this.m_bindObjects = new HashMap<String, Object>();
		return this.m_bindObjects.put(key, value);
	}

	/**
	 * 获取绑定额外信息。
	 * 
	 * @param key 额外信息名称，必须
	 * @param value
	 * @return
	 */
	protected Object getBindObject(String key) {
		if (this.m_bindObjects == null || key == null || key.length() == 0) return null;
		return this.m_bindObjects.get(key);
	}

	/**
	 * 清理文档缓存类包含的信息。
	 */
	public void clear() {
		if (this.m_holders != null) {
			this.m_holders.clear();
			this.m_holders = null;
		}
		if (this.m_bindObjects != null) {
			this.m_bindObjects.clear();
			this.m_bindObjects = null;
		}
		this.m_document = null;
	}
}

