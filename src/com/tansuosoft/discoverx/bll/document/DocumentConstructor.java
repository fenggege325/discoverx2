/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.ResourceConstructor;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.event.impl.SetFieldOnDocumentInitialized;
import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.bll.security.AuthorizationConfig;
import com.tansuosoft.discoverx.bll.security.AuthorizationEntry;
import com.tansuosoft.discoverx.bll.security.AuthorizationItem;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.DocumentType;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemValueComputationMethod;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 文档资源构造实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentConstructor extends ResourceConstructor {
	/**
	 * 缺省构造器。
	 */
	public DocumentConstructor() {

	}

	private Document m_doc = null;

	/**
	 * 获取文档类型信息的参数名：“documenttype”。
	 */
	public static final String DOCUMENT_TYPE_PARAM_NAME = "documenttype";

	/**
	 * 重载construct：构造一个文档资源。
	 * 
	 * <p>
	 * config必须提供，其中第一个值必须是文档所属直接应用程序别名或unid。具体格式请参考{@link Document#getConfigResource()}。
	 * </p>
	 * <p>
	 * 文档类型信息通过{@link #DOCUMENT_TYPE_PARAM_NAME}常数所定义的参数名传入。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceConstructor#construct(java.lang.String, com.tansuosoft.discoverx.model.Session, java.lang.String, java.lang.String, java.util.Map)
	 */
	@Override
	public Resource construct(String directory, Session session, String config, String punid, Map<String, Object> params) throws ResourceException {
		User user = Session.getUser(session);
		try {
			this.m_doc = Document.newBlankDocument();

			// 文档所属(绑定的直接应用程序)资源和可能的其它配置信息等
			String configResource = config;
			if (configResource == null || configResource.length() == 0) throw new ResourceException("没有指定文档所属应用程序信息！");
			String directAppAlias = null;
			if (configResource.toLowerCase().startsWith("app")) {
				directAppAlias = StringUtil.stringLeft(configResource, ",");
			}
			if (configResource.toLowerCase().startsWith("application:app")) {
				directAppAlias = StringUtil.stringRight(StringUtil.stringLeft(configResource, ","), ":");
			}
			if (directAppAlias != null && directAppAlias.length() > 0) {
				String directAppUnid = ResourceAliasContext.getInstance().getUNIDByAlias(Application.class, directAppAlias);
				if (directAppUnid == null || directAppUnid.length() == 0) throw new ResourceException("无法获取" + directAppAlias + "对应的应用程序信息！");
				configResource = configResource.replace(directAppAlias, directAppUnid);
			}
			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(Application.class);
			if (!configResource.startsWith(rd.getDirectory())) configResource = (rd.getDirectory() + ":" + configResource);
			this.m_doc.setConfigResource(configResource);

			// 构造基础属性
			this.m_doc.setState(DocumentState.New.getIntValue());
			this.m_doc.setCreator(user.getName());
			this.m_doc.setModifier(user.getName());
			this.m_doc.setPUNID(punid);

			DocumentType docType = DocumentType.Normal;
			Object objDoctype = (params == null ? null : params.get(DOCUMENT_TYPE_PARAM_NAME));
			if (objDoctype != null) {
				if (objDoctype instanceof DocumentType) {
					docType = (DocumentType) objDoctype;
				} else {
					docType = DocumentType.Normal.parse(objDoctype.toString());
				}
			}
			this.m_doc.setType(docType);

			// 构造安全属性
			Application app = null;
			int appCount = this.m_doc.getApplicationCount();
			Security security = this.m_doc.getSecurity();
			if (security == null) {
				security = new Security(this.m_doc.getUNID());
				this.m_doc.setSecurity(security);
			} else {
				security.setPUNID(this.m_doc.getUNID());
			}
			if (security.getSecurityEntries() == null) security.setSecurityEntries(new ArrayList<SecurityEntry>());
			security.setSecurityLevel(user.getSecurityCode(), SecurityLevel.Author.getIntValue() | SecurityLevel.View.getIntValue());
			security.setWorkflowLevel(user.getSecurityCode(), 1);
			for (int i = 1; i <= appCount; i++) {
				app = this.m_doc.getApplication(i);
				if (app == null) continue;
				if (i == 1) {
					this.m_doc.setCategory(app.getName());
					this.m_doc.setTitle(app.getName());
				} else {
					String category = this.m_doc.getCategory();
					this.m_doc.setCategory(String.format("%1$s%2$s%3$s", (category == null ? "" : category), (category == null || category.length() == 0 ? "" : "\\"), app.getName()));
				}
				if (i == appCount) { // 如果是直接应用
					Security documentDefaultSecurity = null;
					int securityCode = 0;
					List<SecurityEntry> configSecurityEntries = null;
					this.m_doc.setBusinessTitle(app.getName());
					documentDefaultSecurity = app.getDocumentDefaultSecurity();
					configSecurityEntries = null;
					if (documentDefaultSecurity != null) configSecurityEntries = documentDefaultSecurity.getSecurityEntries();
					if (configSecurityEntries == null || configSecurityEntries.isEmpty()) continue;
					// 把可以读取的安全信息添加到文档安全信息。
					for (SecurityEntry x : configSecurityEntries) {
						if (x == null) continue;
						securityCode = x.getSecurityCode();
						if (securityCode <= 0) continue;
						if (!documentDefaultSecurity.checkSecurityLevel(securityCode, SecurityLevel.View)) continue;
						security.setSecurityLevel(securityCode, SecurityLevel.View.getIntValue());
					}// for end
					AuthorizationConfig ac = AuthorizationConfig.getInstance();
					List<AuthorizationEntry> l = ac.getAuthorizationEntries(app);
					for (AuthorizationEntry e : l) {
						if (e == null) continue;
						List<AuthorizationItem> items = e.getAuthorizationItems();
						if (items == null) continue;
						for (AuthorizationItem x : items) {
							if (x == null || x.getSecurityCode() <= 0) continue;
							if (!((x.getSecurityLevel() & SecurityLevel.View.getIntValue()) == SecurityLevel.View.getIntValue())) continue;
							security.setSecurityLevel(x.getSecurityCode(), SecurityLevel.View.getIntValue());
						}
					}// for end
				}// if end
			}// for end

			// 判断是否有新建权限
			boolean authorized = SecurityHelper.authorize(session, m_doc, SecurityLevel.Add);
			User u = Session.getUser(session);
			if (!authorized) { throw new ResourceException("对不起，" + (u == null ? "您" : "“" + u.getName() + "”") + "没有新建“" + m_doc.getCategory() + "”对应文档的权限！"); }

			// 构造字段属性
			Form form = this.m_doc.getForm();
			if (form == null) throw new ResourceException("文档构造错误：没有找到绑定的表单！");
			this.m_doc.setFormAlias(form.getAlias());

			String defaultValueConfig = null;
			String itemName = null;
			String itemValue = null;
			List<Item> formItems = form.getItems();
			if (formItems == null || formItems.isEmpty()) throw new ResourceException("文档构造错误：文档所属表单中没有字段信息！");
			for (Item x : formItems) {
				itemName = x.getItemName();
				defaultValueConfig = x.getDefaultValue();
				// 处理创建时计算
				if ((defaultValueConfig == null || defaultValueConfig.length() == 0) && x.getComputationMethod() == ItemValueComputationMethod.Create) {
					defaultValueConfig = x.getComputationExpression();
				}
				if (defaultValueConfig != null && defaultValueConfig.length() > 0) {
					try {
						ExpressionEvaluator eval = new ExpressionEvaluator(defaultValueConfig, session, this.m_doc);
						itemValue = eval.evalString();
					} catch (Exception ex) {
						itemValue = "";
					}
					this.m_doc.setFieldValue(itemName, itemValue);
				} else {
					this.m_doc.setFieldValue(itemName, null);
				}
			}

			// 文档日志
			Log log = new Log(m_doc.getUNID());
			log.setSubject(user.getName());
			log.setVerb("新建");
			m_doc.setLogs(new ArrayList<Log>());
			m_doc.getLogs().add(log);

		} catch (Exception ex) {
			if (ex instanceof ResourceException) throw (ResourceException) ex;
			throw new ResourceException(ex.getMessage(), ex);
		}

		// 注册事件
		List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Document).provide(this.m_doc, null);
		EventHandler eventHandler = Instance.newInstance("com.tansuosoft.discoverx.workflow.event.SetWorkflowLevelOnDocumentInitialized", EventHandler.class);
		if (eventHandler == null) throw new RuntimeException("无法初始化流程权限设置对象！");
		this.addHandler(EventHandler.EHT_INITIALIZED, eventHandler);
		this.addHandler(EventHandler.EHT_INITIALIZED, new SetFieldOnDocumentInitialized());
		EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_INITIALIZE, EventHandler.EHT_INITIALIZED), eventHandlerInfoList);
		ResourceEventArgs arg = new ResourceEventArgs(this.m_doc, session);

		this.callEventHandler(EventHandler.EHT_INITIALIZE, arg); // 初始化完成后触发的事件。

		// 添加到文档缓存
		DocumentBuffer.getInstance().setDocument(this.m_doc, user.getName());

		this.callEventHandler(EventHandler.EHT_INITIALIZED, arg); // 初始化完毕并添加到文档缓存后触发的事件。
		// 返回构造好的文档资源。
		return this.m_doc;
	}

	/**
	 * 构造并返回一个新文档。
	 * 
	 * @param session
	 * @param config
	 * @param punid
	 * @param params
	 * @return
	 */
	public static Document constructDocument(Session session, String config, String punid, Map<String, Object> params) {
		DocumentConstructor c = new DocumentConstructor();
		try {
			return (Document) c.construct(ResourceDescriptorConfig.DOCUMENT_DIRECTORY, session, config, punid, params);
		} catch (ResourceException e) {
			FileLogger.error(e);
		}
		return null;
	}
}

