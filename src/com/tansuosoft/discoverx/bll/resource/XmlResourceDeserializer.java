/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import java.io.File;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 以XML形式存储的资源文件的反序列化实现类。
 * 
 * 
 * @author coca@tansuosoft.cn
 */
public class XmlResourceDeserializer extends XmlDeserializer {
	/**
	 * 根据xml资源的unid反序列化为资源对象。
	 * 
	 * @param String src 资源的unid
	 * @param Class<?> cls 资源的类。
	 * @return Object 资源的实例。
	 */
	public Object deserialize(String src, Class<?> cls) {
		Object result = Instance.newInstance(cls.getName());
		this.setTarget(result);
		if (src.indexOf(File.separatorChar) < 0) {
			String resourceLocation = CommonConfig.getInstance().getResourcePath();
			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(cls);
			if (rd == null) return null;
			if (resourceLocation == null || resourceLocation.length() == 0) return null;
			// 先找使用单位本身的
			int contextOid = OrganizationsContext.getInstance().getContextUserOrgSc();
			resourceLocation = resourceLocation + rd.getDirectory() + File.separator + contextOid + File.separator + src + ".xml";
			File f = new File(resourceLocation);
			// 找不到则找通用的
			if (!f.exists()) resourceLocation = resourceLocation + rd.getDirectory() + File.separator + src + ".xml";
			result = super.deserialize(resourceLocation, cls);
		} else {
			result = super.deserialize(src, cls);
		}
		return result;
	}
}

