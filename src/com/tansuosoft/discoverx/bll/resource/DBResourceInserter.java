/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.bll.ResourceInserter;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Authority;
import com.tansuosoft.discoverx.model.AuthorityEntry;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.Opinion;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.PersistenceState;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 插入一条新的资源记录到数据库表中的实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DBResourceInserter extends ResourceInserter {

	/**
	 * 重载insert：实现保存于数据库表中的资源插入（新增）功能。
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceInserter#insert(com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.Session)
	 */
	public Resource insert(Resource resource, Session session) throws ResourceException {
		if (resource == null) return null;
		int retValue = 0;

		ResourceEventArgs e = new ResourceEventArgs(resource, session);

		ResourceDescriptorConfig resourceDescriptorConfig = ResourceDescriptorConfig.getInstance();
		ResourceDescriptor resourceDescriptor = resourceDescriptorConfig.getResourceDescriptor(resource.getDirectory());
		Document doc = null;
		try {
			if (resourceDescriptor == null) throw new Exception("资源描述未知！");
			if (resourceDescriptor.getXmlStore()) return null;

			User user = Session.getUser(session);
			// 设置更新人
			String n = (user.isSystemBuiltinAdmin() ? ResourceHelper.BUILTIN_ADMIN_USER_NAME : user.getName());
			if (StringUtil.isBlank(resource.getCreator())) resource.setCreator(n);
			resource.setModifier(n);
			boolean isDocument = (resource instanceof Document);
			if (!resourceDescriptor.getEmbeded()) {
				// 判断权限
				boolean authorized = SecurityHelper.authorize(session, resource, ((resource instanceof Accessory) ? SecurityLevel.Modify : SecurityLevel.Add));
				if (!authorized) throw new ResourceException("对不起，您没有保存此资源的权限！");

				// 添加事件处理程序。
				List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(isDocument ? EventCategory.Document : EventCategory.Resource).provide(resource, null);
				EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_SAVE, EventHandler.EHT_SAVED), eventHandlerInfoList);
				this.callEventHandler(EventHandler.EHT_SAVE, e); // 保存开始事件。
			}

			if (isDocument) {
				doc = (Document) resource;
				doc.appendState(DocumentState.Normal);
			}

			// 设置更新时间和更新人
			resource.setCreated(DateTime.getNowDTString());
			resource.setModified(DateTime.getNowDTString());

			// 构造并发送数据库访问请求
			String className = resourceDescriptor.getInserter();
			if (className == null || className.length() == 0) className = String.format("%sInserter", resource.getClass().getSimpleName());

			DBRequest dbRequest = (DBRequest) Instance.newInstance(className);
			if (dbRequest == null) throw new ResourceException(String.format("没有为“%s”配置新增记录的数据库请求实现类。", resourceDescriptor.getEntity()));

			dbRequest.setResource(resource);
			dbRequest.setUseTransaction(true);

			// 同步新增关联内容
			// --额外参数
			List<Parameter> params = resource.getParameters();
			if (params != null) {
				for (Parameter p : params) {
					if (p == null) continue;
					DBRequest dbrp = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.ParameterInserter");
					dbrp.setParameter(DBRequest.ENTITY_PARAM_NAME, p);
					dbRequest.setNextRequest(dbrp);
				}
			}

			boolean isEmbeded = resourceDescriptor.getEmbeded();
			if (!isEmbeded) {
				// --安全信息
				List<SecurityEntry> securityEntries = resource.getSecurity().getSecurityEntries();
				if (securityEntries != null) {
					for (SecurityEntry se : securityEntries) {
						// 给所有注册用户或所有人公开的信息不要保存，同时DocumentDeserializer中将公开或公布的安全编码动态添加到反序列化的文档安全信息中。
						if (se == null || se.getSecurityCode() == Role.ROLE_ANONYMOUS_SC || se.getSecurityCode() == Role.ROLE_DEFAULT_SC) continue;
						DBRequest dbrs = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.SecurityEntryInserter");
						dbrs.setParameter(DBRequest.ENTITY_PARAM_NAME, se);
						dbRequest.setNextRequest(dbrs);
					}
				}

				// --附加文件
				if (resourceDescriptor.getAccessoryContainer()) {
					List<Accessory> accs = resource.getAccessories();
					if (accs != null) {
						for (Accessory a : accs) {
							if (a == null) continue;
							DBRequest dbra = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.AccessoryInserter");
							dbra.setParameter(DBRequest.ENTITY_PARAM_NAME, a);
							dbRequest.setNextRequest(dbra);
						}
					}
				}

				// --授权
				if (resource instanceof User || resource instanceof Group) {
					User u = ((resource instanceof User) ? (User) resource : null);
					Group g = ((resource instanceof Group) ? (Group) resource : null);
					Authority a = (u != null ? u.getAuthority() : (g != null ? g.getAuthority() : null));
					List<AuthorityEntry> authorityEnties = (a == null ? null : a.getAuthorityEntries());
					if (authorityEnties != null) {
						for (AuthorityEntry x : authorityEnties) {
							if (x == null) continue;
							DBRequest dbrx = Instance.newInstance("com.tansuosoft.discoverx.dao.impl.AuthorityEntryInserter", DBRequest.class);
							dbrx.setParameter(DBRequest.ENTITY_PARAM_NAME, x);
							dbrx.setParameter(DBRequest.UNID_PARAM_NAME, resource.getUNID());
							dbrx.setParameter("directory", resource.getDirectory());
							dbRequest.setNextRequest(dbrx);
						}
					}
				}
			}// if !isEmbeded end

			if (isDocument) {
				// --意见
				List<Opinion> opns = doc.getOpinions();
				if (opns != null) {
					for (Opinion o : opns) {
						if (o == null) continue;
						DBRequest dbro = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.OpinionInserter");
						dbro.setParameter(DBRequest.ENTITY_PARAM_NAME, o);
						dbRequest.setNextRequest(dbro);
					}
				}

				// --日志
				List<Log> logs = doc.getLogs();
				if (logs != null) {
					for (Log l : logs) {
						if (l == null) continue;
						DBRequest dbrf = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.LogInserter");
						dbrf.setParameter(DBRequest.ENTITY_PARAM_NAME, l);
						dbRequest.setNextRequest(dbrf);
					}
				}

				// 把字段状态重置为原始状态。
				doc.resetFieldsPersistenceState(PersistenceState.Raw);
			}// if isDocument end

			dbRequest.sendRequest();
			retValue = (int) dbRequest.getResultLong();
		} catch (ResourceException ex) {
			throw ex;
		} catch (Exception ex) {
			if (doc != null) doc.setState(DocumentState.New.getIntValue());
			throw new ResourceException(ex);
		}
		if (retValue > 0) {
			if (resourceDescriptor.getCache()) {
				ResourceContext.getInstance().addObjectToCache(resource.getUNID(), resource);
			}
			if (!resourceDescriptor.getEmbeded()) this.callEventHandler(EventHandler.EHT_SAVED, e); // 保存完成事件。
			return resource;
		} else {
			return null;
		}
	}
}

