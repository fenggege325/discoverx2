/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import com.tansuosoft.discoverx.dao.DAOConfig;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.util.serialization.Deserializer;

/**
 * 反序列化并返回组织树资源（@link Organization）的{@link Deserializer}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OrganizationDeserializer implements Deserializer {

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	@Override
	public Object deserialize(String src, Class<?> cls) {
		DBRequest dbr = DAOConfig.getInstance().getDBRequest("OrganizationDeserializer");
		if (dbr == null) throw new RuntimeException("DAOConfig中没有配置组织树构造数据库请求信息或者配置的值无效，请联系管理员！");
		dbr.setParameter(DBRequest.UNID_PARAM_NAME, src);
		dbr.sendRequest();
		return dbr.getResult();
	}

}

