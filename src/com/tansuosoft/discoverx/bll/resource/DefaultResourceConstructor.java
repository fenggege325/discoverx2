/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.ResourceConstructor;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 默认的资源实例构造实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DefaultResourceConstructor extends ResourceConstructor {
	/**
	 * 缺省构造器。
	 */
	public DefaultResourceConstructor() {
	}

	/**
	 * 
	 * 重载construct
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceConstructor#construct(java.lang.String, com.tansuosoft.discoverx.model.Session, java.lang.String, java.lang.String, java.util.Map)
	 */
	@Override
	public Resource construct(String directory, Session session, String config, String punid, Map<String, Object> params) throws ResourceException {
		if (directory == null || directory.length() == 0) return null;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
		if (rd == null) throw new ResourceException("无法获取“" + directory + "”资源的描述对象！");
		if (rd.getSingle() && ResourceContext.getInstance().getSingleResource(rd.getDirectory()) != null) throw new ResourceException("您不能创建多个单实例资源！");
		Resource resource = null;
		resource = (Resource) Instance.newInstance(rd.getEntity());
		if (resource != null && punid != null && punid.length() == 32) resource.setPUNID(punid);
		boolean authorized = SecurityHelper.authorize(session, resource, SecurityLevel.Add);
		if (!authorized) throw new ResourceException("您没有新增“" + rd.getName() + "”的权限！");

		// 注册并执行事件
		List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(resource, null);
		EventRegister.registerEventHandler(this, String.format("%1$s", EventHandler.EHT_INITIALIZED), eventHandlerInfoList);
		ResourceEventArgs arg = new ResourceEventArgs(resource, session);
		this.callEventHandler(EventHandler.EHT_INITIALIZED, arg); // 初始化完毕后触发的事件。
		// 多租户使用时群组和角色的punid要设置为当前租户的组织的unid
		if (OrganizationsContext.getInstance().isInTenantContext() && (resource instanceof Role || resource instanceof Group)) resource.setPUNID(OrganizationsContext.getInstance().getContextUserOrgUnid());
		return resource;
	}
}

