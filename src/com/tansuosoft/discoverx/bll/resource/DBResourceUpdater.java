/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.bll.ResourceUpdater;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 更新保存于数据库表中的资源的实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DBResourceUpdater extends ResourceUpdater {
	/**
	 * 
	 * 重载update
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceUpdater#update(com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.Session)
	 */
	public Resource update(Resource resource, Session session) throws ResourceException {
		if (resource == null) return null;

		ResourceEventArgs e = new ResourceEventArgs(resource, session);

		ResourceDescriptorConfig resourceDescriptorConfig = ResourceDescriptorConfig.getInstance();
		ResourceDescriptor resourceDescriptor = resourceDescriptorConfig.getResourceDescriptor(resource.getDirectory());
		try {
			if (resourceDescriptor == null) throw new Exception("资源目录未知！");
			if (resourceDescriptor.getXmlStore()) return null;

			User user = Session.getUser(session);
			boolean isDocument = (resource instanceof Document);
			if (!resourceDescriptor.getEmbeded()) {
				// 判断权限
				boolean authorized = SecurityHelper.authorize(session, resource, SecurityLevel.Modify);
				if (!authorized) {
					// 如果没有授权，那么重置缓存中的资源
					if (isDocument) {
						DocumentBuffer.getInstance().removeDocument(resource.getUNID());
					} else {
						ResourceContext.getInstance().removeObjectFromCache(resource.getUNID());
					}
					throw new ResourceException("对不起，您没有保存此资源的权限！");
				}

				// 添加事件处理程序。
				List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(isDocument ? EventCategory.Document : EventCategory.Resource).provide(resource, null);
				EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_SAVE, EventHandler.EHT_SAVED), eventHandlerInfoList);
				this.callEventHandler(EventHandler.EHT_SAVE, e); // 保存开始事件。
			}

			// 设置更新时间和更新人
			resource.setModified(DateTime.getNowDTString());
			String n = (user.isSystemBuiltinAdmin() ? ResourceHelper.BUILTIN_ADMIN_USER_NAME : user.getName());
			resource.setModifier(n);
			// 构造并发送数据库访问请求
			String className = resourceDescriptor.getUpdater();
			if (className == null || className.length() == 0) className = String.format("%sUpdater", resource.getClass().getSimpleName());

			DBRequest dbRequest = (DBRequest) Instance.newInstance(className);
			if (dbRequest == null) throw new ResourceException(String.format("没有为“%s”配置更新记录的数据库请求实现类。", resourceDescriptor.getEntity()));

			dbRequest.setResource(resource);
			dbRequest.setParameter(DBRequest.UNID_PARAM_NAME, resource.getUNID());
			dbRequest.setUseTransaction(true);

			// 同步更新关联内容
			String unid = resource.getUNID();
			// --额外参数先删后增
			List<Parameter> params = resource.getParameters();
			DBRequest remover = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.ParameterRemover");
			remover.setParameter(DBRequest.PUNID_PARAM_NAME, unid);
			dbRequest.setNextRequest(remover);
			if (params != null) {
				for (Parameter p : params) {
					if (p == null) continue;
					DBRequest dbrp = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.ParameterInserter");
					dbrp.setParameter(DBRequest.ENTITY_PARAM_NAME, p);
					dbRequest.setNextRequest(dbrp);
				}
			}

			if (!isDocument) {
				// --安全信息先删后增
				List<SecurityEntry> securityEntries = resource.getSecurity().getSecurityEntries();
				remover = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.SecurityRemover");
				remover.setParameter(DBRequest.PUNID_PARAM_NAME, unid);
				dbRequest.setNextRequest(remover);
				if (securityEntries != null) {
					for (SecurityEntry se : securityEntries) {
						if (se == null) continue;
						DBRequest dbrs = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.SecurityEntryInserter");
						dbrs.setParameter(DBRequest.ENTITY_PARAM_NAME, se);
						dbRequest.setNextRequest(dbrs);
					}
				}
			}

			dbRequest.sendRequest();
			int retValue = (int) dbRequest.getResultLong();
			if (retValue > 0) {
				if (resourceDescriptor.getCache()) {
					if (resourceDescriptor.getCache()) ResourceContext.getInstance().removeObjectFromCache(unid); // 更新缓存。
				}
				if (!resourceDescriptor.getEmbeded()) this.callEventHandler(EventHandler.EHT_SAVED, e); // 保存完成事件。
				return resource;
			} else {
				return null;
			}
		} catch (ResourceException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new ResourceException(ex);
		}
	}
}

