/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import java.io.File;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.ResourceRemover;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.Path;

/**
 * 彻底删除保存于XML文件中的资源的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class XmlResourceRemover extends ResourceRemover {

	/**
	 * 重载remove：删除指定资源。
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceRemover#remove(com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Resource remove(Resource resource, Session session) throws ResourceException {
		if (resource == null) return null;
		String unid = resource.getUNID();

		ResourceEventArgs e = new ResourceEventArgs(resource, session);
		try {
			// 判断权限
			// User user = Session.getUser(session);
			boolean authorized = SecurityHelper.authorize(session, resource, SecurityLevel.Delete);
			if (!authorized) throw new ResourceException("对不起，您没有删除此资源的权限！");

			// 注册事件
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(resource, null);
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_DELETE, EventHandler.EHT_DELETED), eventHandlerInfoList);

			this.callEventHandler(EventHandler.EHT_DELETE, e); // 删除开始事件。
			ResourceDescriptorConfig resourceDescriptorConfig = ResourceDescriptorConfig.getInstance();
			ResourceDescriptor resourceDescriptor = resourceDescriptorConfig.getResourceDescriptor(resource.getDirectory());

			// 文件路径
			CommonConfig commonCfg = CommonConfig.getInstance();
			Path path = new Path(commonCfg.getResourcePath());
			path = path.combine(resourceDescriptor.getDirectory());
			// 如果有多单位使用
			if (OrganizationsContext.getInstance().isInTenantContext()) {
				int contextOid = OrganizationsContext.getInstance().getContextUserOrgSc();
				path = path.combine(ResourceContext.ORG_VAR_BASE + contextOid);
			}
			String fileName = path.combineFile(resource.getUNID() + ".xml");
			File file = new File(fileName);

			if (file.exists() && file.delete()) {
				ResourceContext.getInstance().removeObjectFromCache(unid);
				if (resourceDescriptor.getCache()) ResourceAliasContext.getInstance().remove(resource.getDirectory(), resource.getAlias());
				this.callEventHandler(EventHandler.EHT_DELETED, e); // 删除完成事件。
				return resource;
			}
		} catch (ResourceException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new ResourceException(ex);
		}
		return null;
	}

}
