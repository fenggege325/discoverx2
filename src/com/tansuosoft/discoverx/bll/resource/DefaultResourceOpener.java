/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceOpener;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;

/**
 * 默认的资源打开实现类。
 * 
 * <p>
 * 打开前后会执行事件，包括判断是否有打开权限。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DefaultResourceOpener extends ResourceOpener {

	/**
	 * 重载open：打开（即返回）指定资源，打开前判断查看权限，同时打开前后触发打开事件。
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceOpener#open(java.lang.String, java.lang.Class, com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Resource open(String unid, Class<?> cls, Session session) throws ResourceException {
		Resource resource = null;
		try {
			resource = ResourceContext.getInstance().getResource(unid, cls);
			if (resource == null) return null;
			ResourceEventArgs arg = new ResourceEventArgs(resource, session);

			// 添加事件处理程序。
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(resource, null);
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_OPEN, EventHandler.EHT_OPENED), eventHandlerInfoList);

			this.callEventHandler(EventHandler.EHT_OPEN, arg);
			// 判断权限。
			boolean authorized = SecurityHelper.authorize(session, resource, SecurityLevel.View);
			if (!authorized) throw new ResourceException("对不起，您没有查看此资源的权限！");
			if (session != null) session.setLastResource(resource); // 在用户自定义会话中记录最近打开的资源。
			this.callEventHandler(EventHandler.EHT_OPENED, arg);

			// 设置最近的资源为打开的资源。
			session.setLastResource(resource);
		} catch (ResourceException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new ResourceException(ex);
		}
		return resource;
	}

}

