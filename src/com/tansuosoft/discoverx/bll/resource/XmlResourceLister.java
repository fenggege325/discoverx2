/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;

/**
 * 用于返回保存于XML文件中的指定类型下的所有资源列表的实用工具类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class XmlResourceLister {
	/**
	 * 根据资源对应实体类信息获取所有此类资源的实例。
	 * 
	 * @param cls Class<?>
	 * @return List<Resource> 如果没有找到此类资源，则返回不包含元素的列表对象。
	 */
	public static List<Resource> getResources(Class<?> cls) {
		if (cls == null) return null;
		return getResources(ResourceDescriptorConfig.getInstance().getResourceDirectory(cls));
	}

	/**
	 * 根据资源对应目录名称获取所有此类资源的实例。
	 * 
	 * @param directory String
	 * @return List<Resource> 如果没有找到此类资源，则返回不包含元素的列表对象。
	 */
	public static List<Resource> getResources(String directory) {
		List<Resource> ret = new ArrayList<Resource>();
		if (directory == null || directory.length() == 0) return ret;

		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
		if (rd == null) return ret;

		Map<String, Integer> map = new HashMap<String, Integer>();
		String rl = CommonConfig.getInstance().getResourcePath();
		if (rl == null || rl.length() == 0) return ret;
		rl = rl + rd.getDirectory();
		int contextOrgSc = 0;
		if (OrganizationsContext.getInstance().isInTenantContext() && (contextOrgSc = OrganizationsContext.getInstance().getContextUserOrgSc()) > 0) addRes(ret, rl + File.separator + ResourceContext.ORG_VAR_BASE + contextOrgSc, rd, map);
		addRes(ret, rl, rd, map);
		return ret;
	}

	private static final ResourceContext RC = ResourceContext.getInstance();

	/**
	 * 添加dir指定目录下的rd所指定类型的资源到ret所指定的列表。
	 * 
	 * @param ret
	 * @param dir
	 * @param rd
	 * @param map
	 */
	protected static void addRes(List<Resource> ret, String dir, ResourceDescriptor rd, Map<String, Integer> map) {
		File files[] = null;
		Resource r = null;
		String unid = null;
		String fn = null;
		File file = new File(dir);
		if (!file.exists() || !file.isDirectory()) return;
		files = file.listFiles();
		for (File x : files) {
			if (x.isDirectory() || (fn = x.getName()).length() != 36) continue;
			unid = fn.substring(0, 32);
			if (map.containsKey(unid)) continue;
			r = RC.getResource(unid, rd);
			if (r == null) continue;
			ret.add(r);
			map.put(unid, null);
		}
	}
}

