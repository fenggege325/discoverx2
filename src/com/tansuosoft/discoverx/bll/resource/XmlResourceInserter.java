/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.bll.ResourceInserter;
import com.tansuosoft.discoverx.bll.ResourceSerializerProvider;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.Path;
import com.tansuosoft.discoverx.util.serialization.Serializer;
import com.tansuosoft.discoverx.util.serialization.XmlSerializer;

/**
 * 插入一条新的资源记录到XML文件中的实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class XmlResourceInserter extends ResourceInserter {

	/**
	 * 实现存于XML文件中的资源插入（新增）功能。
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceInserter#insert(Resource, Session)
	 * 
	 * @return Resource
	 * 
	 * @throws ResourceException
	 */
	public Resource insert(Resource resource, Session session) throws ResourceException {
		if (resource == null) return null;
		ResourceEventArgs e = new ResourceEventArgs(resource, session);

		ResourceDescriptorConfig resourceDescriptorConfig = ResourceDescriptorConfig.getInstance();
		ResourceDescriptor resourceDescriptor = resourceDescriptorConfig.getResourceDescriptor(resource.getDirectory());
		if (!resourceDescriptor.getXmlStore()) return null;
		try {
			// 判断权限
			User user = Session.getUser(session);
			boolean authorized = SecurityHelper.authorize(session, resource, SecurityLevel.Add);
			if (!authorized) { throw new ResourceException("对不起，您没有保存此资源的权限！"); }

			// 注册事件
			List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Resource).provide(resource, null);
			EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_SAVE, EventHandler.EHT_SAVED), eventHandlerInfoList);

			this.callEventHandler(EventHandler.EHT_SAVE, e); // 保存开始事件。
			// 设置更新时间和更新人
			String n = (user.isSystemBuiltinAdmin() ? ResourceHelper.BUILTIN_ADMIN_USER_NAME : user.getName());
			resource.setCreated(DateTime.getNowDTString());
			resource.setCreator(n);
			resource.setModified(DateTime.getNowDTString());
			resource.setModifier(n);
			// 序列化

			Serializer ser = ResourceSerializerProvider.getResourceSerializer(resource.getClass());
			if (ser == null) ser = new XmlSerializer();
			StringWriter writer = new StringWriter();
			ser.serialize(resource, writer);
			// 文件路径
			CommonConfig commonCfg = CommonConfig.getInstance();
			Path path = new Path(commonCfg.getResourcePath());
			path = path.combine(resourceDescriptor.getDirectory());
			// 如果有多单位使用，则保存到单位自己的子目录下
			if (OrganizationsContext.getInstance().isInTenantContext()) {
				int contextOid = OrganizationsContext.getInstance().getContextUserOrgSc();
				path = path.combine(ResourceContext.ORG_VAR_BASE + contextOid);
			}
			if (!path.checkPathExists()) path.makeDirIfNotExists();
			String fileName = path.combineFile(resource.getUNID() + ".xml");
			File file = new File(fileName);
			if (file.exists()) file.delete();

			FileWriter fw = new FileWriter(fileName);
			fw.write("<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n");
			fw.write("<resource type=\"");
			fw.write(resourceDescriptor.getEntity());
			fw.write("\">\r\n");
			fw.write(writer.toString());
			fw.write("</resource>");
			fw.flush();
			fw.close();

			if (resourceDescriptor.getCache()) ResourceAliasContext.getInstance().update(resource.getDirectory(), resource.getAlias(), resource.getUNID());

			this.callEventHandler(EventHandler.EHT_SAVED, e); // 保存完成事件。
		} catch (IOException ex) {
			throw new ResourceException("IO异常，无法创建文件。", ex);
		} catch (ResourceException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new ResourceException(ex);
		}
		return resource;
	}
}

