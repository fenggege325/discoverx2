/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.resource;

import java.util.List;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.ResourceRemover;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Securer;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 彻底删除保存于数据库表中的资源的实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DBResourceRemover extends ResourceRemover {

	/**
	 * 重载remove
	 * 
	 * @see com.tansuosoft.discoverx.bll.ResourceRemover#remove(com.tansuosoft.discoverx.model.Resource, com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Resource remove(Resource resource, Session session) throws ResourceException {
		if (resource == null) return null;
		String unid = resource.getUNID();
		ResourceEventArgs e = new ResourceEventArgs(resource, session);
		try {
			// User user = Session.getUser(session);

			boolean isDocument = (resource instanceof Document);

			ResourceDescriptorConfig resourceDescriptorConfig = ResourceDescriptorConfig.getInstance();
			ResourceDescriptor resourceDescriptor = resourceDescriptorConfig.getResourceDescriptor(resource.getDirectory());
			if (resourceDescriptor.getXmlStore()) return null;

			if (!resourceDescriptor.getEmbeded()) {
				// 判断权限
				boolean authorized = SecurityHelper.authorize(session, resource, SecurityLevel.Delete);
				if (!authorized) throw new ResourceException("对不起，您没有删除此资源的权限！");

				// 添加事件处理程序。
				List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(isDocument ? EventCategory.Document : EventCategory.Resource).provide(resource, null);
				EventRegister.registerEventHandler(this, String.format("%1$s,%2$s", EventHandler.EHT_DELETE, EventHandler.EHT_DELETED), eventHandlerInfoList);
				this.callEventHandler(EventHandler.EHT_DELETE, e); // 删除开始事件。
			}

			// 构造并发送数据库访问请求
			String className = resourceDescriptor.getRemover();
			if (className == null || className.length() == 0) className = String.format("%sRemover", resource.getClass().getSimpleName());
			if (className == null || className.length() == 0) throw new ResourceException(String.format("没有为“%s”配置删除记录的数据库请求实现类。", resourceDescriptor.getEntity()));

			DBRequest dbRequest = Instance.newInstance(className, DBRequest.class);
			if (dbRequest == null) throw new ResourceException(String.format("没有为“%s”配置删除记录的数据库请求实现类。", resourceDescriptor.getEntity()));
			dbRequest.setUseTransaction(true);
			dbRequest.setResource(resource);
			dbRequest.setParameter(DBRequest.UNID_PARAM_NAME, resource.getUNID());

			// 同步删除关联内容
			// --额外参数
			DBRequest dbrp = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.ParameterRemover");
			dbrp.setParameter(DBRequest.PUNID_PARAM_NAME, resource.getUNID());
			dbRequest.setNextRequest(dbrp);

			// --安全信息
			DBRequest dbrs = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.SecurityRemover");
			dbrs.setParameter(DBRequest.PUNID_PARAM_NAME, resource.getUNID());
			dbRequest.setNextRequest(dbrs);

			// --附加文件
			if (resourceDescriptor.getAccessoryContainer()) {
				DBRequest dbra = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.AccessoryRemover");
				dbra.setParameter(DBRequest.PUNID_PARAM_NAME, resource.getUNID());
				dbRequest.setNextRequest(dbra);
			}
			if (resource instanceof Securer) {
				// --授权
				DBRequest dbrx = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.AuthorityRemover");
				dbrx.setParameter(DBRequest.UNID_PARAM_NAME, resource.getUNID());
				dbRequest.setNextRequest(dbrx);
			}
			if (isDocument) {
				// --意见
				DBRequest dbro = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.OpinionRemover");
				dbro.setParameter(DBRequest.PUNID_PARAM_NAME, resource.getUNID());
				dbRequest.setNextRequest(dbro);

				// --日志
				DBRequest dbrl = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.LogRemover");
				dbrl.setParameter(DBRequest.PUNID_PARAM_NAME, resource.getUNID());
				dbRequest.setNextRequest(dbrl);

				// --流程控制数据
				DBRequest dbrw = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.WFDataRemover");
				dbrw.setParameter(DBRequest.PUNID_PARAM_NAME, resource.getUNID());
				dbRequest.setNextRequest(dbrw);
			}

			dbRequest.sendRequest();
			int retValue = (int) dbRequest.getResultLong();
			if (retValue > 0) {
				ResourceContext.getInstance().removeObjectFromCache(unid);
				if (isDocument) {
					// 删除对应附加文件。
					String path = AccessoryPathHelper.getAbsoluteAccessoryServerPath(resource);
					FileHelper.deleteFolder(path);
					DocumentBuffer.getInstance().removeDocument(unid);
				}
				if (!resourceDescriptor.getEmbeded()) this.callEventHandler(EventHandler.EHT_DELETED, e); // 删除完成事件。
				return resource;
			} else {
				return null;
			}
		} catch (ResourceException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new ResourceException(ex);
		}
	}

}

