/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.io.File;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;

/**
 * 附加文件路径信息帮助类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AccessoryPathHelper {
	/**
	 * 下载临时文件时使用的临时父资源UNID。
	 */
	public static final String TEMP_FILE_PARENT_UNID = "847279A18B3D4FA38C7F2E57C2643749";
	/**
	 * 下载临时文件时使用的临时附加文件资源UNID。
	 */
	public static final String TEMP_FILE_ACCESSORY_UNID = "5E2203FDFDA54AF0A236735DA2DC5633";

	/**
	 * 系统保存临时附加文件的子目录名。
	 */
	public static final String TEMP_FILE_FOLDER = "temp";

	/**
	 * 为指定的父资源获取其包含的附加文件在服务器上存放的绝对路径信息。
	 * 
	 * @param parent 附加文件所属资源，必须。
	 * 
	 * @return String，返回的路径信息中包含尾部的路径分隔符。
	 */
	public static String getAbsoluteAccessoryServerPath(Resource parent) {
		return CommonConfig.getInstance().getAccessoryPath() + getRelativeAccessoryServerPath(parent);
	}

	/**
	 * 为指定的父资源获取其包含的附加文件在服务器上存放的绝对路径信息。
	 * 
	 * @param parent 附加文件所属资源，必须。
	 * 
	 * @return String，返回的路径信息中包含尾部的路径分隔符。
	 */
	public static String getRelativeAccessoryServerPath(Resource parent) {
		if (parent == null) return File.separator;
		String subDir = null;
		String parentDirectory = parent.getDirectory();
		String punid = parent.getUNID();
		if (ResourceDescriptorConfig.DOCUMENT_DIRECTORY.equalsIgnoreCase(parentDirectory)) {
			String created = parent.getCreated();
			if (created != null && created.length() >= 7) created = created.substring(0, 7);
			subDir = created + File.separator + punid;
		} else {
			subDir = parentDirectory + File.separator + parent.getAlias();
			int contextOrgSc = 0;
			if (OrganizationsContext.getInstance().isInTenantContext() && (contextOrgSc = OrganizationsContext.getInstance().getContextUserOrgSc()) > 0) {
				subDir = subDir + File.separator + ResourceContext.ORG_VAR_BASE + contextOrgSc;
			}
		}
		String result = subDir + File.separator;
		return result;
	}

	/**
	 * 获取系统保存临时附加文件的服务器完整路径名。
	 * 
	 * <p>
	 * 返回结果包含尾部的路径分隔符。
	 * </p>
	 * 
	 * @return String
	 */
	public static String getAbsoluteTempAccessoryServerPath() {
		return String.format("%1$s%2$s%3$s", CommonConfig.getInstance().getAccessoryPath(), TEMP_FILE_FOLDER, File.separator);
	}
}

