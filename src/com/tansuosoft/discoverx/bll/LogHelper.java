/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.Resource;

/**
 * 日志实用工具类。
 * 
 * @author coca@tansuosoft.cn
 */
public class LogHelper {
	private static final String LOG_INSERTER_IMPL = "com.tansuosoft.discoverx.dao.impl.LogInserter";

	/**
	 * 缺省构造器。
	 */
	private LogHelper() {
	}

	/**
	 * 写入一条日志对象{@link Log}到数据库中。
	 * 
	 * @param log {@link Log}
	 * @return 成功则返回true，否则返回false。
	 */
	public static boolean writeLog(Log log) {
		if (log == null) return false;
		DBRequest dbr = DBRequest.getInstance(LOG_INSERTER_IMPL);
		if (dbr == null) throw new RuntimeException("无法初始化“" + LOG_INSERTER_IMPL + "”对应的日志插入数据库请求类。");
		dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, log);
		dbr.sendRequest();
		if (dbr.getResultLong() > 0) return true;
		return false;
	}

	/**
	 * 写入一条日志对象{@link Log}到数据库中并在成功写入后将日志与资源绑定（如果是文档资源的话）。
	 * 
	 * @param log {@link Log}
	 * @param resource {@link Resource}
	 * @return 成功则返回true，否则返回false。
	 */
	public static boolean writeLog(Log log, Resource resource) {
		if (log == null) return false;
		if (resource != null && !resource.getUNID().equalsIgnoreCase(log.getOwner())) log.setOwner(resource.getUNID());
		boolean successed = writeLog(log);
		if (successed && (resource instanceof Document)) {
			Document doc = (Document) resource;
			List<Log> list = doc.getLogs();
			if (list == null) {
				list = new ArrayList<Log>();
				doc.setLogs(list);
			}
			list.add(0, log);
		}
		return successed;
	}
}
