/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 根据资源类型提供资源提交内容接收对象实例的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceReceiverProvider {

	/**
	 * 根据资源类型提供资源提交内容接收对象实例。
	 * 
	 * @param cls
	 * @return
	 */
	public static ResourceReceiver getResourceReceiver(Class<?> cls) {
		if (cls == null) return null;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(cls);
		if (rd == null) throw new RuntimeException("找不到对应资源描述信息！");
		String cfgClassName = rd.getReceiver();
		if (cfgClassName != null && cfgClassName.length() > 0) {
			ResourceReceiver obj = Instance.newInstance(cfgClassName, ResourceReceiver.class);
			if (obj != null) return obj;
		}
		return null;
	}

	/**
	 * 根据资源目录提供资源提交内容接收对象实例。
	 * 
	 * @param directory
	 * @return
	 */
	public static ResourceReceiver getResourceReceiver(String directory) {
		if (directory == null) return null;
		return getResourceReceiver(ResourceDescriptorConfig.getInstance().getResourceClass(directory));
	}
}

