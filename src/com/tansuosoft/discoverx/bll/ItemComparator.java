/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.util.Comparator;

import com.tansuosoft.discoverx.model.Item;

/**
 * 按字段坐标排序字段的排序实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ItemComparator implements Comparator<Item> {

	/**
	 * 比较Item资源，比较方式：行列越靠前越小，如果行列相同则比较unid。
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Item o1, Item o2) {
		if (o1 == null && o2 == null) return 0;
		if (o1 != null && o2 == null) return 1;
		if (o1 == null && o2 != null) return -1;
		int ret = o1.getRow() - o2.getRow();
		if (ret == 0) { // 先比较行
			ret = o1.getColumn() - o2.getColumn(); // 行相同比较列
			if (ret == 0) { // 列相同比较unid
				return o1.getUNID().compareTo(o2.getUNID());
			}
		}
		return ret > 0 ? 1 : -1;
	}
}

