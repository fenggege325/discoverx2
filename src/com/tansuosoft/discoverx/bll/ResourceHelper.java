/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.db.Database;
import com.tansuosoft.discoverx.db.DatabaseFactory;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 资源实用工具类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ResourceHelper {
	/**
	 * 检查clazz和unid指定的资源是否存在。
	 * 
	 * @param clazz 资源类对应Class，必须。
	 * @param unid 资源UNID，必须。
	 * @return boolean 存在则返回true，否则返回false，嵌入型资源总是返回false。
	 * @throws RuntimeException 如果是嵌入型资源则抛出此异常。
	 */
	public static boolean exists(Class<?> clazz, String unid) {
		boolean result = false;
		if (clazz == null || unid == null || unid.length() != 32) return result;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(clazz);
		if (rd == null) return false;
		Resource r = null;
		if (rd.getEmbeded()) throw new RuntimeException("无法检查嵌入型资源是否存在！");
		if (rd.getSingle()) {
			r = ResourceContext.getInstance().getSingleResource(rd.getDirectory());
			if (r != null && unid.equalsIgnoreCase(r.getUNID())) return true;
			if (r != null && r.getChild(unid) != null) return true;
		}
		if (rd.getCache()) {
			if (ResourceContext.getInstance().getResource(unid, clazz) != null) return true;
		}
		if (rd.getXmlStore()) {
			result = checkXMLFile(rd.getDirectory(), unid);
		} else {
			result = checkDBRecord(rd.getDirectory(), unid);
		}
		return result;
	}

	/**
	 * 检查directory和unid指定的资源是否存在。
	 * 
	 * @param directory 资源目录，必须。
	 * @param unid 资源UNID，必须。
	 * @return boolean 存在则返回true，否则返回false.
	 * @throws RuntimeException 如果是嵌入型资源则抛出此异常。
	 */
	public static boolean exists(String directory, String unid) {
		return exists(ResourceDescriptorConfig.getInstance().getResourceClass(directory), unid);
	}

	/**
	 * 检查资源是否存在。
	 * 
	 * @param resource Resource 资源，必须
	 * @return boolean 存在则返回true，否则返回false.
	 * @throws RuntimeException 如果是嵌入型资源则抛出此异常。
	 */
	public static boolean exists(Resource resource) {
		if (resource == null) return false;
		Class<?> clazz = resource.getClass();
		String unid = resource.getUNID();
		return exists(clazz, unid);
	}

	/**
	 * 检查资源xml文件是否存在。
	 * 
	 * @param directory
	 * @param unid
	 * 
	 * @return boolean
	 */
	private static boolean checkXMLFile(String directory, String unid) {
		boolean result = false;
		String resourceLocation = CommonConfig.getInstance().getResourcePath();
		String filePath = String.format("%1$s%2$s%3$s%4$s.xml", resourceLocation, directory, File.separator, unid);
		File file = new File(filePath);
		result = file.exists();
		return result;
	}

	/**
	 * 检查资源UNID对应记录是否存在t_{directory}表记录中。
	 * 
	 * @param directory
	 * @param unid
	 * 
	 * @return boolean
	 */
	private static boolean checkDBRecord(String directory, String unid) {
		boolean result = false;
		Database db = DatabaseFactory.getDatabase();
		try {
			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
			String tableName = null;
			if (rd != null) tableName = rd.getDbTableName();
			if (tableName == null || tableName.length() == 0) throw new RuntimeException("无法获取资源保存的表名！");
			Object obj = db.executeScalar(String.format("select count(*) from %1$s where c_unid='%2$s'", tableName, unid));
			if (obj != null) {
				if ("1".equalsIgnoreCase(obj.toString())) return true;
			}
		} catch (SQLException e) {
			FileLogger.error(e);
		}
		return result;
	}

	/**
	 * 保存资源。
	 * 
	 * @param r 要保存的资源，必须。
	 * @param session 保存资源的用户自定义会话。
	 * @return 保存过程没有出现异常则返回true，否则返回false。
	 */
	public static boolean save(Resource r, Session session) {
		if (r == null) return false;

		ResourceInserter inserter = null;
		ResourceUpdater updater = null;
		try {
			boolean existFlag = exists(r);
			if (existFlag) {
				updater = ResourceUpdaterProvider.getResourceUpdater(r);
				updater.update(r, session);
			} else {
				inserter = ResourceInserterProvider.getResourceInserter(r);
				inserter.insert(r, session);
			}
			return true;
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		return false;
	}

	/**
	 * 获取dir资源目录下unid指定的资源的资源xml文件的完整路径。
	 * 
	 * @param dir
	 * @param unid
	 * @return 完整路径，参数不合法时返回null。
	 */
	public static String getXmlLocationPath(String dir, String unid) {
		if (dir == null || dir.length() == 0 || unid == null || unid.length() == 0) return null;
		return String.format("%1$s%2$s%3$s%4$s.xml", CommonConfig.getInstance().getResourcePath(), dir, File.separator, unid);
	}

	/**
	 * 获取r指定的资源的资源xml文件的完整路径。
	 * 
	 * @param r
	 * @return 完整路径，参数不合法时返回null。
	 */
	public static String getXmlLocationPath(Resource r) {
		if (r == null) return null;
		return getXmlLocationPath(r.getDirectory(), r.getUNID());
	}

	/**
	 * 内置超级管理员的用户名
	 */
	public static final String BUILTIN_ADMIN_USER_NAME = "管理员";
	/**
	 * 内置超级管理员的用户名
	 */
	public static final String BUILTIN_ADMIN_USER_ALIAS = "sysadmin";
	/**
	 * 内置系统用户的用户名
	 */
	public static final String BUILTIN_SYSTEM_USER_NAME = "系统";
	/**
	 * 内置系统用户的别名
	 */
	public static final String BUILTIN_SYSTEM_USER_ALIAS = "system";

	/**
	 * 系统用户
	 */
	protected static final User SYS_USER = Session.getSystemSession().getUser();

	/**
	 * 获取资源的作者对应的参与者信息。
	 * 
	 * @param r
	 * @return 如果找不到，可能返回null。
	 */
	public static Participant getAuthorParticipant(Resource r) {
		if (r == null) return null;
		// 先从作者中取(针对文档资源)
		Security s = r.getSecurity();
		boolean noSecurity = false;
		boolean noSecurityEntries = false;
		if (s == null) noSecurity = true;
		List<SecurityEntry> ses = (noSecurity ? null : s.getSecurityEntries());
		if (ses == null || ses.isEmpty()) noSecurityEntries = true;
		if (!noSecurityEntries) {
			for (SecurityEntry x : ses) {
				if (x != null && (x.getSecurityLevel() & SecurityLevel.Author.getIntValue()) == SecurityLevel.Author.getIntValue()) {
					ParticipantTree pt = ParticipantTreeProvider.getInstance().getParticipantTree(x.getSecurityCode());
					if (pt != null) return pt;
				}
			}
		}
		// 从创建者中取
		String creator = r.getCreator();
		ParticipantTree pt = ParticipantTreeProvider.getInstance().getParticipantTree(creator);
		if (pt != null) return pt;
		// 是否是系统用户
		if (creator != null && (BUILTIN_SYSTEM_USER_NAME.equalsIgnoreCase(creator) || BUILTIN_SYSTEM_USER_ALIAS.equalsIgnoreCase(creator))) {
			pt = new ParticipantTree(SYS_USER);
			return pt;
		}
		// 如果都找不到则判断是否为内置超级管理员是的话返回内置超级管理员参与者
		if (creator != null && (BUILTIN_ADMIN_USER_NAME.equalsIgnoreCase(creator) || creator.endsWith(User.SEPARATOR + BUILTIN_ADMIN_USER_NAME) || BUILTIN_ADMIN_USER_ALIAS.equalsIgnoreCase(creator))) return ParticipantTreeProvider.getInstance().getParticipantTree(User.SUPERADMIN_USER_SC);
		return null;
	}

	/**
	 * 获取资源的作者对应的用户信息。
	 * 
	 * @param r
	 * @return 如果找不到，可能返回null。
	 */
	public static User getAuthor(Resource r) {
		Participant pt = getAuthorParticipant(r);
		if (pt == null) return null;
		if (pt.getSecurityCode() == SYS_USER.getSecurityCode()) return SYS_USER;
		String unid = pt.getUNID();
		if (unid == null || unid.length() == 0) return null;
		return ResourceContext.getInstance().getResource(unid, User.class);
	}// func end
}// class end

