/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 资源访问授权验证对象实例提供类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class AuthorizationProvider {
	/**
	 * 缺省构造器。
	 */
	protected AuthorizationProvider() {
	}

	/**
	 * 根据系统配置的资源访问授权验证实现类名称获取其对应的资源访问授权验证对象实例。
	 * 
	 * @return Authorization
	 */
	public static Authorization getAuthorization() {
		CommonConfig commonConfig = CommonConfig.getInstance();
		String className = commonConfig.getAuthorization();
		Object obj = Instance.newInstance(className);
		if (obj == null || !(obj instanceof Authorization)) return new AuthorizationImpl();
		return (Authorization) obj;
	}
}

