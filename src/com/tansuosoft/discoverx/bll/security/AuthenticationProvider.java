/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 用户登录验证对象实例提供类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class AuthenticationProvider {
	/**
	 * 缺省构造器。
	 */
	protected AuthenticationProvider() {
	}

	/**
	 * 根据系统配置的用户登录验证实现类名称获取其对应的用户登录验证对象实例。
	 * 
	 * @return Authentication
	 */
	public static Authentication getAuthentication() {
		CommonConfig commonConfig = CommonConfig.getInstance();
		String className = commonConfig.getAuthentication();
		Object obj = Instance.newInstance(className);
		if (obj == null || !(obj instanceof Authentication)) return new AliasAuthentication();
		return (Authentication) obj;
	}
}

