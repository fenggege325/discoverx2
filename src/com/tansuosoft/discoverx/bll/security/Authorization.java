/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.SecurityLevel;

/**
 * 资源访问授权验证接口。
 * 
 * <p>
 * 用户访问资源前一般要判断用户对资源是否有相应访问级别的权限。 所有授权判断类都要实现此接口。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public interface Authorization {

	/**
	 * 判断用户（user）对指定的资源（
	 * 
	 * @param identity 用户身份令牌。
	 * @param resource 要访问的资源。
	 * @param securityLevel 访问级别（操作权限）。
	 * @return boolean 授权成功则返回true，否则返回false。
	 */
	public boolean authorize(Identity identity, Resource resource, SecurityLevel securityLevel);
}

