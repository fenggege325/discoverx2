/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

/**
 * 用户（登录）验证接口。
 * 
 * <p>
 * 一般用于用户登录，所有完成用户登录验证的类必须实现此接口。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public interface Authentication {
	/**
	 * 使用提供的验证（登录）凭据验证用户并返回验证（登录）结果对应的用户身份令牌。
	 * 
	 * @param credential 提供的登录凭据。
	 * @return Identity 验证（登录）成功则返回绑定了用户信息的用户身份令牌，否则返回null。
	 */
	public Identity authenticate(Credential credential);
}

