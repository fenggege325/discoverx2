/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import java.util.HashMap;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;

/**
 * 表示某一类或某一个资源包含的通用权限控制配置信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class AuthorizationEntry implements Comparable<AuthorizationEntry>, JsonSerializable {
	/**
	 * 针对具体资源目标的资源目录和资源UNID分隔符。
	 */
	protected static final String TARGET_SEP = "://";

	/**
	 * 缺省构造器。
	 */
	public AuthorizationEntry() {
	}

	private String m_target = null; // 权限控制针对的目标。
	private List<AuthorizationItem> m_authorizationItems = null; // 包含的通用权限控制配置信息列表。
	private boolean m_isdirectory = false; // 是否针对某一类资源的标记
	private String m_unid = null;
	private String m_directory = null;

	/**
	 * 返回权限控制针对的目标。
	 * 
	 * <p>
	 * 可能的值格式:
	 * <ul>
	 * <li>资源目录名，表示控制某一类资源的权限，如"application"表示定义应用程序这一整类资源的权限；</li>
	 * <li>资源目录名://应用程序UNID，表示控制某一个资源的权限，如"application://29C965A438C349E1A046564A8C00CF5B"表示定义UNID为“29C965A438C349E1A046564A8C00CF5B”的应用程序的权限。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 */
	public String getTarget() {
		if (m_target != null && m_target.endsWith(TARGET_SEP)) m_target = m_target.replace(TARGET_SEP, "");
		return this.m_target;
	}

	/**
	 * 设置权限控制针对的目标。
	 * 
	 * @param target String
	 */
	public void setTarget(String target) {
		this.m_target = target;
		m_isdirectory = true;
		m_directory = m_target;
		if (m_target != null && !m_target.isEmpty()) {
			String strs[] = StringUtil.splitString(target, TARGET_SEP);
			if (strs.length > 1 && StringUtil.isUNID(strs[1])) {
				m_directory = strs[0];
				m_isdirectory = false;
				m_unid = strs[1];
			} else {
				m_directory = strs[0].replace(TARGET_SEP, "");
				m_target = m_directory;
			}
		}
	}

	/**
	 * 返回此条目是否只针对某一类资源。
	 * 
	 * <p>
	 * 返回true表示只针对某一类资源，返回false表示针对某一个具体资源。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean isDirectory() {
		return m_isdirectory;
	}

	/**
	 * 返回针对的具体目标资源.
	 * 
	 * @return 如果{@link #isDirectory()}返回true或者找不到则返回null.
	 */
	public Resource getTargetResource() {
		if (isDirectory()) return null;
		return ResourceContext.getInstance().getResource(m_unid, m_directory);
	}

	/**
	 * 返回针对的具体某一类资源的资源描述信息。
	 * 
	 * @return 找不到则返回null.
	 */
	public ResourceDescriptor getTargetResourceDescriptor() {
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(m_directory);
		return rd;
	}

	/**
	 * 返回目标资源目录名。
	 * 
	 * @return String
	 */
	public String getDirectory() {
		return m_directory;
	}

	/**
	 * 返回包含的通用权限控制配置信息列表。
	 * 
	 * @return List&lt;AuthorizationItem&gt;
	 */
	public List<AuthorizationItem> getAuthorizationItems() {
		return this.m_authorizationItems;
	}

	/**
	 * 设置包含的通用权限控制配置信息列表。
	 * 
	 * @param authorizationItems List&lt;AuthorizationItem&gt;
	 */
	public void setAuthorizationItems(List<AuthorizationItem> authorizationItems) {
		this.m_authorizationItems = authorizationItems;
	}

	private java.util.Map<Integer, Integer> m_securityLevels = null; // 包含的安全编码与操作级别一一对应的字典集合。

	/**
	 * 同步安全信息以方便查询。
	 */
	protected void sync2Map() {
		if (this.m_authorizationItems == null || this.m_authorizationItems.isEmpty()) return;
		this.m_securityLevels = new HashMap<Integer, Integer>(this.m_authorizationItems.size());
		int securityCode = 0;
		int securityLevel = 0;
		for (AuthorizationItem x : this.m_authorizationItems) {
			if (x == null) continue;
			securityCode = x.getSecurityCode();
			securityLevel = x.getSecurityLevel();
			this.m_securityLevels.put(securityCode, securityLevel);
		}
	}

	/**
	 * 检查指定安全编码是否包含指定操作级别。
	 * 
	 * @param securityCode
	 * @param securityLevel
	 * @return boolean
	 */
	public boolean checkSecurityLevel(int securityCode, int securityLevel) {
		if (m_securityLevels == null || m_securityLevels.isEmpty()) this.sync2Map();
		Integer x = (this.m_securityLevels == null ? null : this.m_securityLevels.get(securityCode));
		if (x == null) return false;
		return ((x & securityLevel) == securityLevel);
	}

	/**
	 * 重载：比较：按照{@link #getTarget()}文本进行排序。
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AuthorizationEntry o) {
		if (o == null) return 1;
		if (StringUtil.isBlank(getTarget())) {
			if (StringUtil.isBlank(o.getTarget())) return 0;
			return -1;
		}
		return this.getTarget().compareToIgnoreCase(o.getTarget());
	}

	protected static final String UNKNOWN_NAME = "【无名称】";

	/**
	 * 重载：输出每个{@link AuthorizationItem}对象对应一个listviewitem格式的json结果。
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	@Override
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		List<AuthorizationItem> ais = this.getAuthorizationItems();
		if (ais == null || ais.isEmpty()) return "";
		int idx = 0;
		Participant pt = null;
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		Resource r = this.getTargetResource();
		int hashCode = 0;
		for (AuthorizationItem ai : ais) {
			if (ai == null) continue;
			hashCode = ai.hashCode();
			if (idx > 0) sb.append(",\r\n");
			sb.append("{");
			sb.append("id:").append("'lvi_").append(hashCode).append("'");
			sb.append(",data:").append("{}");
			sb.append(",columns:").append("[");
			sb.append("{");
			sb.append("id:").append("'lvc_").append(hashCode).append("_0").append("'");
			sb.append(",label:'").append(StringUtil.encode4Json(r == null ? "&lt;通用&gt;" : r.getName())).append("'");
			sb.append(",value:'").append(StringUtil.encode4Json(this.getTarget())).append("'");
			sb.append(",data:{}");
			sb.append("}");
			sb.append(",{");
			sb.append("id:").append("'lvc_").append(hashCode).append("_1").append("'");
			pt = ptp.getParticipantTree(ai.getSecurityCode());
			sb.append(",label:'").append(pt == null ? UNKNOWN_NAME : pt.getName()).append("'");
			sb.append(",value:").append(ai.getSecurityCode());
			sb.append(",data:{}");
			sb.append("}");
			sb.append(",{");
			sb.append("id:").append("'lvc_").append(hashCode).append("_2").append("'");
			sb.append(",label:''");
			sb.append(",value:").append(ai.getSecurityLevel());
			sb.append(",data:{}");
			sb.append("}");
			sb.append("]");
			sb.append("}");
			idx++;
		}
		return sb.toString();
	}
}

