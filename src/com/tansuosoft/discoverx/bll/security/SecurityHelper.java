/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Authority;
import com.tansuosoft.discoverx.model.AuthorityEntry;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.SecurityRange;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;

/**
 * 包含用户、安全控制等相关实用方法的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class SecurityHelper {
	/**
	 * 为指定用户判断是否具有对指定资源指定级别的授权。
	 * 
	 * @param user
	 * @param resource
	 * @param securityLevel
	 * @return boolean
	 */
	public static boolean authorize(User user, Resource resource, SecurityLevel securityLevel) {
		if (user == null) return false;
		Authorization au = AuthorizationProvider.getAuthorization();
		return au.authorize(new Identity(user), resource, securityLevel);
	}

	/**
	 * 为指定用户自定义会话包含的用户判断是否具有对指定资源指定级别的授权。
	 * 
	 * @param session
	 * @param resource
	 * @param securityLevel
	 * @return boolean
	 */
	public static boolean authorize(Session session, Resource resource, SecurityLevel securityLevel) {
		return authorize(Session.getUser(session), resource, securityLevel);
	}

	/**
	 * 为指定用户判断是否对指定嵌入型资源具有指定级别的授权。
	 * 
	 * @param session
	 * @param resource Resource 必须是嵌入型资源，否则返回false。
	 * @param parentResource Resource 必须是嵌入型资源对应的父资源，否则返回false。
	 * @param combination 资源本身安全控制和父资源安全控制的组合方式。
	 *          <ul>
	 *          <li><b>1</b>：表示使用资源本身的安全控制；</li>
	 *          <li><b>2</b>：表示使用父资源的安全控制；</li>
	 *          <li><b>3</b>：表示如果本身有定义安全控制，那么使用之，否则使用父资源的；</li>
	 *          <li><b>0</b>或其它数字表示合并所有安全控制。</li>
	 *          </ul>
	 * @param securityLevel
	 * @return boolean
	 */
	public static boolean authorize(Session session, Resource resource, Resource parentResource, int combination, SecurityLevel securityLevel) {
		if (resource == null || parentResource == null) return false;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(resource.getClass());
		ResourceDescriptor rdParent = ResourceDescriptorConfig.getInstance().getResourceDescriptor(parentResource.getClass());
		if (rd == null || !rd.getEmbeded() || rdParent == null) return false;

		// 如果判断能否查看(读取)附加文件资源的内容且上级资源不是文档资源(即要查看的是附加文件的模板文件内容)那么总是允许。
		if (securityLevel == SecurityLevel.View && (resource instanceof Accessory) && !(parentResource instanceof Document)) return true;
		Security raw = resource.getSecurity();
		switch (combination) {
		case 1: // 本身
			return SecurityHelper.authorize(session, resource, securityLevel);
		case 2: // 父资源
			return SecurityHelper.authorize(session, parentResource, securityLevel);
		case 3: // 从本身和父资源中优先选择一个
			if (raw.hasValidEntries()) {
				return SecurityHelper.authorize(session, resource, securityLevel);
			} else {
				return SecurityHelper.authorize(session, parentResource, securityLevel);
			}
		default: // 合并
			return (SecurityHelper.authorize(session, resource, securityLevel) || SecurityHelper.authorize(session, parentResource, securityLevel));
		}
	}

	/**
	 * 为指定的用户和安全信息判断是否具有指定安全范围内的指定级别权限。
	 * 
	 * @param user 用户资源，必须。
	 * @param security 安全信息，必须。
	 * @param securityLevel SecurityLevel
	 * @param securityRange SecurityRange,
	 * @return
	 */
	@SuppressWarnings("serial")
	public static boolean authorize(User user, Security security, SecurityLevel securityLevel, SecurityRange securityRange) {
		if (user == null) return false;
		Resource res = null;
		if (securityRange == SecurityRange.Document) {
			res = Document.newBlankDocument();
			Document doc = (Document) res;
			List<Resource> list = XmlResourceLister.getResources(Application.class);
			Resource app = list.get(0);
			doc.setConfigResource(app == null ? "" : app.getDirectory() + ":" + app.getUNID());
		} else {
			res = new Resource() {
			};
		}
		res.setSecurity(security);
		return authorize(user, res, securityLevel);
	}

	/**
	 * 获取用户所属的群组集合。
	 * 
	 * @param user
	 * @return
	 */
	public static List<Group> getUserGroups(User user) {
		if (user == null) return null;

		ResourceContext rc = ResourceContext.getInstance();
		List<Group> list = new ArrayList<Group>();

		Authority au = user.getAuthority();
		if (au == null) return list;
		List<AuthorityEntry> authorityEntries = au.getAuthorityEntries();
		if (authorityEntries == null || authorityEntries.isEmpty()) return list;
		Group g = null;
		String groupDirectory = ResourceDescriptorConfig.getInstance().getResourceDirectory(Group.class);

		for (AuthorityEntry x : authorityEntries) {
			if (x != null && groupDirectory.equalsIgnoreCase(x.getDirectory())) {
				g = rc.getResource(x.getUNID(), Group.class);
				if (g == null) continue;
				list.add(g);
			}
		}
		return list;
	}

	/**
	 * 检查user指定的用户是否属于groupId指定的群组。
	 * 
	 * @param user
	 * @param groupId String，群组唯一id，可以是群组的UNID、别名或名称（如果名称也唯一的话）。
	 * @return
	 */
	public static boolean checkUserGroup(User user, String groupId) {
		if (groupId == null || groupId.length() == 0) return false;
		List<Group> groups = getUserGroups(user);
		if (groups == null || groups.isEmpty()) return false;
		for (Group x : groups) {
			if (x != null && (groupId.equalsIgnoreCase(x.getUNID()) || groupId.equalsIgnoreCase(x.getAlias()) || groupId.equalsIgnoreCase(x.getName()))) return true;
		}
		return false;
	}

	/**
	 * 获取用户所有角色：包括通过配置赋予用户的角色、通过配置赋予用户所属群组的角色和登录或匿名用户固定具有的角色。
	 * 
	 * @param user Participant
	 * @return List&lt;Role&gt;
	 */
	public static List<Role> getUserRoles(User user) {
		if (user == null) return null;

		ResourceContext rc = ResourceContext.getInstance();
		List<Role> list = new ArrayList<Role>();

		if (!user.isAnonymous()) {
			Role defaultRole = Role.getDefaultRole();
			if (defaultRole != null) list.add(defaultRole);
		} else {
			Role anonymousRole = Role.getAnonymous();
			if (anonymousRole != null) list.add(anonymousRole);
		}

		Authority au = user.getAuthority();
		if (au == null) return list;
		List<AuthorityEntry> authorityEntries = au.getAuthorityEntries();
		if (authorityEntries == null || authorityEntries.isEmpty()) return list;
		Role r = null;
		String roleDirectory = ResourceDescriptorConfig.getInstance().getResourceDirectory(Role.class);
		for (AuthorityEntry x : authorityEntries) {
			if (x != null && roleDirectory.equalsIgnoreCase(x.getDirectory())) {
				r = (Role) rc.getResource(x.getUNID(), Role.class);
				if (r == null) continue;
				list.add(r);
			}
		}

		// List<Group> groups = getUserGroups(user);
		// if (groups == null || groups.isEmpty()) return list;
		// for (Group x : groups) {
		// if (x == null) continue;
		// au = x.getAuthority();
		// if (au == null) continue;
		// authorityEntries = au.getAuthorityEntries();
		// if (authorityEntries == null || authorityEntries.isEmpty()) continue;
		// for (AuthorityEntry y : authorityEntries) {
		// if (y != null && roleDirectory.equalsIgnoreCase(y.getDirectory())) {
		// r = (Role) rc.getResource(y.getUNID(), Role.class);
		// if (r == null) continue;
		// list.add(r);
		// }
		// }
		// }
		return list;
	}

	/**
	 * 判断用户是否具有指定角色。
	 * 
	 * @param user Participant
	 * @param role Role
	 * @return boolean
	 */
	public static boolean checkUserRole(User user, Role role) {
		if (role == null) return false;
		List<Role> roles = getUserRoles(user);
		if (roles == null || roles.isEmpty()) return false;
		return roles.contains(role);
	}

	/**
	 * 判断用户是否具有角色id对应的角色。
	 * 
	 * @param user Participant
	 * @param roleId String，角色唯一id，可以是角色的UNID、别名或名称（如果名称也唯一的话）。
	 * @return boolean
	 */
	public static boolean checkUserRole(User user, String roleId) {
		if (roleId == null || roleId.length() == 0) return false;
		List<Role> roles = getUserRoles(user);
		if (roles == null || roles.isEmpty()) return false;
		for (Role x : roles) {
			if (x != null && (roleId.equalsIgnoreCase(x.getUNID()) || roleId.equalsIgnoreCase(x.getAlias()) || roleId.equalsIgnoreCase(x.getName()))) return true;
		}
		return false;
	}

	/**
	 * 判断用户是否具有角色编码对应的角色。
	 * 
	 * @param user Participant
	 * @param roleCode int
	 * @return boolean
	 */
	public static boolean checkUserRole(User user, int roleCode) {
		List<Role> roles = getUserRoles(user);
		if (roles == null || roles.isEmpty()) return false;
		for (Role x : roles) {
			if (roleCode == x.getSecurityCode()) return true;
		}
		return false;
	}

	/**
	 * 获取指定用户包含的所有安全编码，包括本身、部门、群组、角色（包括内置默认角色）等的所有安全编码列表。
	 * 
	 * @param user Participant
	 * @return
	 */
	public static List<Integer> getUserSecurityCodes(User user) {
		if (user == null) return null;
		List<Integer> list = new ArrayList<Integer>();

		int sc = user.getSecurityCode();
		// 本身安全编码
		list.add(sc);

		// 固定角色安全编码
		if (!user.isAnonymous()) {
			list.add(Role.ROLE_DEFAULT_SC);
			list.add(Role.ROLE_ANONYMOUS_SC);
		} else {
			list.add(Role.ROLE_ANONYMOUS_SC);
		}

		// 获取所属部门安全编码
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree pt = ptp.getParticipantTree(sc);
		int ptsc = 0;
		while (pt != null) {
			ptsc = pt.getSecurityCode();
			if (ptsc > 0 && ptsc != ParticipantTree.ROOT_CODE && ptsc != sc) list.add(ptsc);
			pt = pt.getParent();
		}

		// 群组和角色的安全编码
		Authority au = user.getAuthority();
		if (au == null) return list;
		List<AuthorityEntry> authorityEntries = au.getAuthorityEntries();
		if (authorityEntries == null || authorityEntries.isEmpty()) return list;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (AuthorityEntry x : authorityEntries) {
			if (x == null) continue;
			int xsc = x.getSecurityCode();
			if (!map.containsKey(xsc)) {
				list.add(x.getSecurityCode());
				map.put(xsc, null);
			}
		}
		map.clear();
		map = null;

		return list;
	}
}
