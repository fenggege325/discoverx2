/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentType;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.PublishOption;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 系统默认使用的资源访问授权验证实现类。
 * 
 * <p>
 * 系统默认资源访问授权实现说明如下：<br/>
 * <table border="1" width="100%" cellspacing="0" cellpadding="2" style="table-layout:fixed;border-collapse:collapse">
 * <colgroup><col width="10%"/><col width="40%"/><col/></colgroup>
 * <tr style="background-color:#ece9d8">
 * <td>级别</td>
 * <td>文档资源</td>
 * <td>配置资源</td>
 * </tr>
 * <tr>
 * <td>查看</td>
 * <td>作者、流程参与者默认直接可以查看；如果在其所属模块中定义了额外可查看角色的话，具有这些角色的用户也可以查看。</td>
 * <td>只有具有一定角色的用户才能查看，这个角色必须在其权限控制配置中具有查看权限。</td>
 * </tr>
 * <tr>
 * <td>新建</td>
 * <td>默认所有登录用户都可以新建；如果在其所属模块中定义了需要额外角色才能新建的话，那么只有具有这些角色的用户才可以新建。</td>
 * <td>只有具有一定角色的用户才能新建，这个角色必须在其权限控制配置中具有新建权限。</td>
 * </tr>
 * <tr>
 * <td>删除</td>
 * <td>默认情况下只有作者可以删除；如果在其所属模块中定义了额外可删除角色的话，具有这些角色的用户也可以删除。</td>
 * <td>只有具有一定角色的用户才能删除，这个角色必须在其权限控制配置中具有删除权限。</td>
 * </tr>
 * <tr>
 * <td>修改</td>
 * <td>默认情况下作者和流程当前环节审批人可以修改，如果在其所属模块中定义了额外可修改角色的话，具有这些角色的用户也可以修改。</td>
 * <td>只有具有一定角色的用户才能修改，这个角色必须在其权限控制配置中具有修改权限。</td>
 * </tr>
 * <tr>
 * <td>使用</td>
 * <td>无意义</td>
 * <td>默认情况下任何用户（包括匿名用户）都可以使用此资源；除非在其权限配置中定义了额外可使用角色，此时只有具有这些角色的用户可以使用此资源。</td>
 * </tr>
 * </table>
 * 备注：<br/>
 * <ul>
 * <li>超级管理员对任意资源具有所有有效的权限；</li>
 * <li>配置管理员对配置资源具有所有有效的权限；</li>
 * <li>文档管理员对文档资源具有所有有效的权限。</li>
 * </ul>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class AuthorizationImpl implements Authorization {
	/**
	 * 要检查其访问授权的目标资源。
	 */
	protected Resource resource = null;
	/**
	 * 要检查的访问授权的权限级别。
	 */
	protected SecurityLevel securityLevel = null;
	/**
	 * 要检查对目标资源是否具有指定权限级别的目标用户。
	 */
	protected User user = null;
	/**
	 * 目标资源包含的安全信息。
	 */
	protected Security security = null;
	/**
	 * 目标用户包含的所有有效安全编码列表。
	 */
	protected List<Integer> securityCodes = null;
	/**
	 * 目标资源是否为文档资源。
	 */
	boolean isDocument = false;

	/**
	 * 重载：执行资源授权判断。
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Authorization#authorize(com.tansuosoft.discoverx.bll.security.Identity, com.tansuosoft.discoverx.model.Resource,
	 *      com.tansuosoft.discoverx.model.SecurityLevel)
	 */
	public boolean authorize(Identity identity, Resource resource, SecurityLevel securityLevel) {
		if (identity == null || resource == null) return false;
		if (securityLevel == SecurityLevel.None) return true;

		this.resource = resource;
		isDocument = (resource instanceof Document);
		this.securityLevel = securityLevel;
		user = identity.getUser();
		if (user == null) user = User.getAnonymous();
		boolean checkMultiTenancy = checkGroupAndRole();
		if (checkMultiTenancy && authorizeBuiltinRoles()) return true;
		securityCodes = SecurityHelper.getUserSecurityCodes(user);
		if (securityCodes == null) {
			securityCodes = new ArrayList<Integer>(1);
			securityCodes.add(user.getSecurityCode());
		}

		if (isDocument) {// 文档资源
			Document doc = (Document) resource;
			if (doc.getType() == DocumentType.Profile) return authorizeProfile(doc);
			security = doc.getSecurityAll();
		} else { // 非文档资源
			security = resource.getSecurity();
		}

		if (checkMultiTenancy && authorizeDefault()) return true;
		return (checkMultiTenancy && authorize());
	}

	/**
	 * 检查系统默认的增、删、改、查和使用权限。
	 * 
	 * @return boolean
	 */
	protected boolean authorizeDefault() {
		switch (securityLevel.getIntValue()) {
		case 1: // 查看
			if (user.getName().equalsIgnoreCase(resource.getCreator()) || user.getName().equalsIgnoreCase(resource.getModifier())) return true;// 作者或修改者可查看
			// 公开或公布的可以查看。
			if (security.getPublished() && (security.getPublishOption() == PublishOption.All || (security.getPublishOption() == PublishOption.User && !user.isAnonymous()))) return true;
			break;
		case 4: // 增加
			if (isDocument && authorizeDocumentAddDefault()) return true;
			break;
		case 8: // 修改
			for (int sc : securityCodes) {
				if (security.checkSecurityLevel(sc, SecurityLevel.Author)) return true; // 作者可修改
				if (security.checkWorkflowLevel(sc, 1)) return true; // 流程修改者可修改
			}
			if (user.getName().equalsIgnoreCase(resource.getCreator())) return true; // 作者可修改
			break;
		case 16: // 删除
			if (user.getName().equalsIgnoreCase(resource.getCreator())) return true;// 作者可删除
			break;
		case 32: // 使用
			if (authorizeUsageDefault()) return true;
			break;
		default:
			break;
		}
		return false;
	}

	/**
	 * 通过配置的权限绑定信息检查权限。
	 * 
	 * <p>
	 * 先通过资源本身定义的安全信息判断权限，再通过配置的全局通用权限控制信息判断权限。
	 * </p>
	 * 
	 * @return boolean
	 */
	protected boolean authorize() {
		// 通过资源本身定义的安全信息判断权限
		for (int sc : securityCodes) {
			if (security.checkSecurityLevel(sc, securityLevel)) return true;
		}
		// 通过配置的全局通用权限控制信息判断权限
		AuthorizationConfig ac = AuthorizationConfig.getInstance();
		Application app = null;
		if (isDocument) { // 如果是文档资源那么全局通用权限控制信息中要以文档所属直接应用来判断权限
			Document doc = (Document) resource;
			app = doc.getApplication(doc.getApplicationCount());
		} else { // 如果不是文档资源那么全局通用权限控制信息中只能判断是否有管理或使用权限。
			if (securityLevel != SecurityLevel.Usage) securityLevel = SecurityLevel.Admin;
		}
		Resource r = (app == null ? resource : app);
		for (int sc : securityCodes) {
			if (ac.checkSecurityLevel(r, sc, securityLevel.getIntValue())) return true;
		}
		return false;
	}

	/**
	 * 检查配置文档访问授权。
	 * 
	 * <p>
	 * 配置文档的所有权限通过所属应用中配置的应用程序配置文件管理员属性值({@link Application#getProfileAdmins()})来判断。
	 * </p>
	 * 
	 * @param profile
	 * 
	 * @return boolean
	 */
	protected boolean authorizeProfile(Document profile) {
		if (SecurityHelper.checkUserRole(user, Role.ROLE_SETTINGSADMIN_SC)) return true;
		if (SecurityHelper.checkUserRole(user, Role.ROLE_DOCUMENTADMIN_SC)) return true;
		Application app = profile.getApplication(profile.getApplicationCount());
		if (app == null) return false;
		Map<Integer, String> pas = app.getProfileAdmins();
		if (pas != null && pas.size() > 0) {
			for (int sc : securityCodes) {
				if (pas.containsKey(sc)) return true;
			}
		}
		return false;
	}

	/**
	 * 检查系统默认的文档新增权限。
	 * 
	 * <p>
	 * 没有额外定义新增角色的话则所有登录用户都可以新增。
	 * </p>
	 * 
	 * @return boolean
	 */
	protected boolean authorizeDocumentAddDefault() {
		if (security == null || user == null) return false;
		boolean isDefAddInResource = false; // 是否在资源本身中有定义新增权限
		boolean isDefAddInGlobal = false; // 是否在配置文件中有定义新增权限
		List<SecurityEntry> ses = security.getSecurityEntries();
		if (ses != null) {
			for (SecurityEntry x : ses) {
				if (x == null) continue;
				if (!((x.getSecurityLevel() & SecurityLevel.Add.getIntValue()) == SecurityLevel.Add.getIntValue())) continue;
				if ((x.getSecurityCode() == Role.ROLE_ANONYMOUS_SC)) return true; // 定义了匿名用户可以新建则所有都可以新建。
				isDefAddInResource = true;
			}
		}
		if (isDefAddInResource) return false;
		AuthorizationConfig ac = AuthorizationConfig.getInstance();
		Document doc = (Document) resource;
		Application app = doc.getApplication(doc.getApplicationCount());
		List<AuthorizationEntry> l = ac.getAuthorizationEntries(app);
		for (AuthorizationEntry e : l) {
			if (e == null) continue;
			List<AuthorizationItem> items = e.getAuthorizationItems();
			if (items == null) continue;
			for (AuthorizationItem x : items) {
				if (x == null || x.getSecurityCode() < 0) continue;
				if ((x.getSecurityLevel() & SecurityLevel.Add.getIntValue()) == SecurityLevel.Add.getIntValue()) {
					if ((x.getSecurityCode() == Role.ROLE_ANONYMOUS_SC)) return true; // 定义了匿名用户可以新建则所有都可以新建。
					isDefAddInGlobal = true;
					break;
				}
			}
			if (isDefAddInGlobal) break;
		}
		// 如果没有控制额外新增权限，则所有登录用户都可以新增
		if (!isDefAddInResource && !isDefAddInGlobal && !user.isAnonymous()) return true;
		return false;
	}

	/**
	 * 检查系统默认的资源使用权限。
	 * 
	 * <p>
	 * 如果没有定义使用权限则所有人都可以使用(查看)。
	 * </p>
	 * 
	 * @return boolean
	 */
	protected boolean authorizeUsageDefault() {
		boolean isDefUsageInResource = false; // 是否在资源本身中有定义使用权限
		boolean isDefUsageInGlobal = false; // 是否在配置文件中有定义使用权限
		List<SecurityEntry> ses = security.getSecurityEntries();
		if (ses != null) {
			for (SecurityEntry x : ses) {
				if ((x.getSecurityLevel() & SecurityLevel.Usage.getIntValue()) == SecurityLevel.Usage.getIntValue()) {
					isDefUsageInResource = true;
					break;
				}
			}
		}
		if (isDefUsageInResource) return false;
		AuthorizationConfig ac = AuthorizationConfig.getInstance();
		List<AuthorizationEntry> l = ac.getAuthorizationEntries(resource);
		for (AuthorizationEntry e : l) {
			if (e == null) continue;
			List<AuthorizationItem> items = e.getAuthorizationItems();
			if (items == null) continue;
			for (AuthorizationItem x : items) {
				if (x == null || x.getSecurityCode() < 0) continue;
				if ((x.getSecurityLevel() & SecurityLevel.Usage.getIntValue()) == SecurityLevel.Usage.getIntValue()) {
					isDefUsageInGlobal = true;
					break;
				}
			}
			if (isDefUsageInGlobal) break;
		}

		if (!isDefUsageInResource && !isDefUsageInGlobal) return true; // 如果没有定义资源使用权限，则所有用户可访问
		return false;
	}

	/**
	 * 系统内部使用的特殊角色：配置观察员。
	 * 
	 * <p>
	 * 通常为了方便做演示或测试系统时让某些用户可方便查看到所有的配置信息。
	 * </p>
	 */
	protected static final String BUILTIN_SETTINGS_OBSERVER_ROLE_NAME = "配置观察员";

	/**
	 * 检查系统默认内置的特殊角色的访问权限。
	 * 
	 * <p>
	 * <ul>
	 * <li>内置超级管理员角色具有所有权限；</li>
	 * <li>内置配置管理员角色具有配置资源的所有权限；</li>
	 * <li>内置文档管理员角色具有文档资源的所有权限；</li>
	 * <li>如果用户具有名为“配置观察员”的角色，则能够查看所有非文档资源。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return boolean
	 */
	protected boolean authorizeBuiltinRoles() {
		if (SecurityHelper.checkUserRole(user, Role.ROLE_ADMIN_SC)) return true;
		if (isDocument && SecurityHelper.checkUserRole(user, Role.ROLE_DOCUMENTADMIN_SC)) return true;
		if (!isDocument && securityLevel != SecurityLevel.Usage) {
			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(resource.getClass());
			if (rd != null && rd.getConfigurable() && SecurityHelper.checkUserRole(user, Role.ROLE_SETTINGSADMIN_SC)) return true;
			if (securityLevel == SecurityLevel.View && SecurityHelper.checkUserRole(user, BUILTIN_SETTINGS_OBSERVER_ROLE_NAME)) return true; // 具有名为“配置观察员”角色的用户可以查看所有非文档资源
		}
		return false;
	}

	/**
	 * 多租户下如果是修改、删除公用群组或角色时返回false，否则返回true。
	 * 
	 * @return
	 */
	protected boolean checkGroupAndRole() {
		if (!OrganizationsContext.getInstance().isInTenantContext()) return true;
		if (!(resource instanceof Role || resource instanceof Group)) return true;
		if (!(securityLevel == SecurityLevel.Modify || securityLevel == SecurityLevel.Delete)) return true;
		if (StringUtil.isBlank(resource.getPUNID())) return false;
		return true;
	}
}

