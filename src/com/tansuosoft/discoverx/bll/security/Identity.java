/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import com.tansuosoft.discoverx.model.User;

/**
 * 用户（登录）验证成功后返回身份令牌类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Identity {
	private User m_user = null; // 此身份令牌绑定的用户资源
	private Credential m_credential = null; // 获取此身份令牌所使用的用户验证凭据。
	private boolean m_authenticated = false; // 是否验证成功，默认为false。

	/**
	 * 缺省构造器。
	 */
	public Identity() {
	}

	/**
	 * 接收用户资源；同时设置用户登录凭据为null，是否通过验证为false。
	 * 
	 * <p>
	 * 一般用于用户授权时使用。
	 * </p>
	 * 
	 * @param user
	 * @param credential
	 */
	public Identity(User user) {
		this(user, null, false);
	}

	/**
	 * 接收用户资源和验证凭据的构造器，同时设置是否通过验证为true。
	 * 
	 * <p>
	 * 一般用于用户登录时使用。
	 * </p>
	 * 
	 * @param user
	 * @param credential
	 */
	public Identity(User user, Credential credential) {
		this(user, credential, true);
	}

	/**
	 * 接收用户资源、验证凭据、是否通过验证信息的构造器。
	 * 
	 * @param user
	 * @param credential
	 * @param authenticated
	 */
	public Identity(User user, Credential credential, boolean authenticated) {
		this.m_user = user;
		this.m_credential = credential;
		this.m_authenticated = authenticated;
	}

	/**
	 * 获取或设置此身份令牌绑定的用户资源。
	 * 
	 * @return Participant
	 */
	public User getUser() {
		return this.m_user;
	}

	/**
	 * @see Identity#getUser()
	 * @param user Participant
	 */
	public void setUser(User user) {
		this.m_user = user;
	}

	/**
	 * 返回获取此身份令牌所使用的用户验证凭据。
	 * 
	 * @return Credential 获取此身份令牌所使用的用户验证凭据。
	 */
	public Credential getCredential() {
		return this.m_credential;
	}

	/**
	 * 设置获取此身份令牌所使用的用户验证凭据。
	 * 
	 * @param credential Credential 获取此身份令牌所使用的用户验证凭据。
	 */
	public void setCredential(Credential credential) {
		this.m_credential = credential;
	}

	/**
	 * 返回是否验证成功，默认为false。
	 * 
	 * @return boolean 是否验证成功，默认为false。
	 */
	public boolean getAuthenticated() {
		return this.m_authenticated;
	}

	/**
	 * 设置是否验证成功，默认为false。
	 * 
	 * @param authenticated boolean 是否验证成功，默认为false。
	 */
	public void setAuthenticated(boolean authenticated) {
		this.m_authenticated = authenticated;
	}

	/**
	 * 获取令牌名称。
	 * 
	 * <p>
	 * 如果登录成功的话则返回登录用户的用户全名，否则返回null。
	 * </p>
	 * 
	 * @return String
	 */
	public String getName() {
		return (this.m_user == null ? null : this.m_user.getName());
	}

	/**
	 * 返回登录验证类型名称。
	 * <p>
	 * 此处返回“basic”表示基本登录验证。
	 * </p>
	 * 
	 * @return String
	 */
	public String getAuthenticationType() {
		return "basic";
	}
}

