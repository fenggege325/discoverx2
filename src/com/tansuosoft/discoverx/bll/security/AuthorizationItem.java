/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

/**
 * 表示一条通用权限控制配置信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class AuthorizationItem {
	/**
	 * 缺省构造器。
	 */
	public AuthorizationItem() {
	}

	private int m_securityCode = -1; // 安全编码。
	private int m_securityLevel = 0; // 普通权限级别，默认为0。

	/**
	 * 返回目标参与者安全编码。
	 * 
	 * <p>
	 * 通常是角色的安全编码。
	 * </p>
	 * 
	 * @return int
	 */
	public int getSecurityCode() {
		return this.m_securityCode;
	}

	/**
	 * 设置安全编码。
	 * 
	 * @param securityCode int
	 */
	public void setSecurityCode(int securityCode) {
		this.m_securityCode = securityCode;
	}

	/**
	 * 返回权限操作级别，默认为0。
	 * 
	 * @return int
	 */
	public int getSecurityLevel() {
		return this.m_securityLevel;
	}

	/**
	 * 设置权限操作级别，默认为0。
	 * 
	 * @param securityLevel int
	 */
	public void setSecurityLevel(int securityLevel) {
		this.m_securityLevel = securityLevel;
	}
}

