/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.User;

/**
 * 获取匿名用户（未登录用户）实例的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Anonymous {
	private static Anonymous m_instance = null; // 唯一实例。
	private User m_user = null; // 匿名用户资源实例。

	/**
	 * 私有构造器。
	 */
	private Anonymous() {
		DBRequest dbRequest = (DBRequest) com.tansuosoft.discoverx.dao.DAOConfig.getInstance().getInstanceFromEntryValue("UserDeserializer");
		if (dbRequest == null) com.tansuosoft.discoverx.util.logger.FileLogger.error(new ResourceException("数据库配置中没有配置名为UserDeserializer的用户反序列化数据库访问请求信息！"));
		dbRequest.setParameter(DBRequest.UNID_PARAM_NAME, User.ANONYMOUS_USER_UNID);
		dbRequest.sendRequest();
		this.m_user = (User) dbRequest.getResult();
	}

	/**
	 * 获取匿名用户资源实例。
	 * 
	 * @return Participant
	 */
	protected User getUser() {
		return this.m_user;
	}

	/**
	 * 设置匿名用户资源实例。
	 * 
	 * @param user Participant
	 */
	protected void setUser(User user) {
		this.m_user = user;
	}

	/**
	 * 获取匿名用户的唯一实例。
	 * 
	 * @return Participant
	 */
	public synchronized static User getAnonymous() {
		if (m_instance == null) {
			m_instance = new Anonymous();
		}
		return m_instance.getUser();
	}
}

