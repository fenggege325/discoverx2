/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 系统全局资源通用权限控制配置类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AuthorizationConfig extends Config implements JsonSerializable {
	/**
	 * 缺省构造器。
	 */
	private AuthorizationConfig(int orgsc) {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		String fn = String.format("%s%s%s%s.xml", CommonConfig.getInstance().getResourcePath(), ResourceContext.ORG_VAR_BASE, orgsc + File.separator, this.getClass().getSimpleName());
		File f = new File(fn);
		if (f.exists() && f.isFile()) location = fn;

		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
		if (StringUtil.isBlank(this.getName())) this.setName("系统全局资源通用权限控制配置");
	}

	private static Map<Integer, AuthorizationConfig> m_instances = new HashMap<Integer, AuthorizationConfig>();
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return AuthorizationConfig
	 */
	public static AuthorizationConfig getInstance() {
		AuthorizationConfig ac = null;
		int sc = OrganizationsContext.getInstance().getContextUserOrgSc();
		synchronized (m_lock) {
			ac = m_instances.get(sc);
			if (ac == null) {
				ac = new AuthorizationConfig(sc);
				m_instances.put(sc, ac);
			}
		}
		return ac;
	}

	private List<AuthorizationEntry> m_authorizationEntries = null; // 包含的通用权限控制条目。
	private Map<String, AuthorizationEntry> m_dict = null; // AuthorizationEntry.getTarget与AuthorizationEntry一一对应的字典集合

	/**
	 * 返回包含的通用权限控制条目。
	 * 
	 * @return List&lt;AuthorizationEntry&gt;
	 */
	public List<AuthorizationEntry> getAuthorizationEntries() {
		return this.m_authorizationEntries;
	}

	/**
	 * 设置包含的通用权限控制条目。
	 * 
	 * @param authorizationEntries List&lt;AuthorizationEntry&gt;
	 */
	public void setAuthorizationEntries(List<AuthorizationEntry> authorizationEntries) {
		this.m_authorizationEntries = authorizationEntries;
	}

	/**
	 * 获取目标资源匹配的所有{@link AuthorizationEntry}对象集合。
	 * 
	 * @param r
	 * @return List&lt;AuthorizationEntry&gt; 找不到则返回空集合。
	 */
	public List<AuthorizationEntry> getAuthorizationEntries(Resource r) {
		List<AuthorizationEntry> l = new ArrayList<AuthorizationEntry>(2);
		if (r == null) return l;
		if (m_dict == null || m_dict.isEmpty()) sync2Map();
		AuthorizationEntry self = m_dict.get(r.toString());
		if (self != null) l.add(self);
		AuthorizationEntry dir = m_dict.get(r.getDirectory() + AuthorizationEntry.TARGET_SEP);
		if (dir != null) l.add(dir);
		return l;
	}

	/**
	 * 检查securityCode对应的安全编码对target指定的目标是否具有securityLevel指定的权限级别。
	 * 
	 * @param target
	 * @param securityCode
	 * @param securityLevel
	 * @return
	 */
	public boolean checkSecurityLevel(String target, int securityCode, int securityLevel) {
		if (target == null || target.isEmpty()) return false;
		if (m_dict == null || m_dict.isEmpty()) sync2Map();
		if (target == null || m_dict == null | m_dict.isEmpty()) return false;
		AuthorizationEntry ae = m_dict.get(target);
		if (ae == null) return false;
		return ae.checkSecurityLevel(securityCode, securityLevel);
	}

	/**
	 * 检查securityCode对应的安全编码对r指定的资源是否具有securityLevel指定的权限级别。
	 * 
	 * @param r
	 * @param securityCode
	 * @param securityLevel
	 * @return
	 */
	public boolean checkSecurityLevel(Resource r, int securityCode, int securityLevel) {
		if (r == null) return false;
		String target1 = r.getDirectory();
		boolean result = checkSecurityLevel(target1, securityCode, securityLevel);
		if (result) return true;
		String target2 = (r.getDirectory() + AuthorizationEntry.TARGET_SEP + r.getUNID());
		return checkSecurityLevel(target2, securityCode, securityLevel);
	}

	/**
	 * 同步列表内容到哈希集合以加快查询。
	 */
	protected void sync2Map() {
		if (this.m_dict != null && !m_dict.isEmpty()) return;
		this.m_dict = new TreeMap<String, AuthorizationEntry>();
		if (this.m_authorizationEntries != null && this.m_authorizationEntries.size() > 0) {
			Collections.sort(m_authorizationEntries);
			for (AuthorizationEntry x : this.m_authorizationEntries) {
				if (x == null) continue;
				this.m_dict.put(x.getTarget(), x);
			}
		}
	}

	/**
	 * 返回目标资源目录名对应的{@link AuthorizationEntry}对象在包含{@link AuthorizationEntry}对象的列表集合中的起止位置序号。
	 * 
	 * @param directory
	 * @return GenericPair&lt;Integer, Integer&gt; 如果找不到则返回null，否则开始位置在key中，结束位置在value中。
	 */
	protected GenericPair<Integer, Integer> getAuthorizationEntryIndex(String directory) {
		if (directory == null || this.m_authorizationEntries == null || this.m_authorizationEntries.isEmpty()) return null;
		if (m_dict == null || m_dict.isEmpty()) sync2Map();
		GenericPair<Integer, Integer> r = new GenericPair<Integer, Integer>(-1, -1);
		for (int i = 0; i < m_authorizationEntries.size(); i++) {
			AuthorizationEntry ae = m_authorizationEntries.get(i);
			if (ae == null || ae.getTarget() == null) continue;
			if (ae.getTarget().startsWith(directory)) {
				if (r.getKey() == -1) r.setKey(i);
				if (r.getValue() < i) r.setValue(i);
			}
		}
		return r;
	}

	/**
	 * 重载：输出供前端“admin/auth_bind.jsp”页面使用的json结果。
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	@Override
	public String toJson() {
		sync2Map();
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("resourceDescriptors:[");
		List<ResourceDescriptor> rds = ResourceDescriptorConfig.getInstance().getResourceDescriptors();
		GenericPair<Integer, Integer> aeIdx = null;
		int idx = 0;
		String dir = null;
		for (ResourceDescriptor x : rds) {
			if (x == null || x.getEmbeded() || x.getSecurityRange() == 2) continue;
			dir = x.getDirectory();
			if (idx > 0) sb.append(",\r\n");
			sb.append("{");
			sb.append("id:'tvi_").append(dir).append("'");
			sb.append(",label:'").append(StringUtil.encode4Json(x.getName())).append("'");
			sb.append(",data:{");
			sb.append("directory:'").append(dir).append("'");
			sb.append(",securityRange:").append(x.getSecurityRange());
			sb.append(",xmlStore:").append(x.getXmlStore() ? "true" : "false");
			sb.append(",single:").append(x.getSingle() ? "true" : "false");
			sb.append(",entries:[\r\n");
			aeIdx = getAuthorizationEntryIndex(dir);
			if (aeIdx != null && aeIdx.getKey() >= 0 && aeIdx.getValue() >= 0) {
				int subidx = 0;
				for (int i = aeIdx.getKey(); i <= aeIdx.getValue(); i++) {
					AuthorizationEntry ae = m_authorizationEntries.get(i);
					if (subidx > 0) sb.append(",\r\n");
					sb.append(ae.toJson());
					subidx++;
				}
			}
			sb.append("]");
			sb.append("}");
			sb.append("}");
			idx++;
		}
		sb.append("]");
		sb.append(",name:'").append(StringUtil.encode4Json(this.getName())).append("'");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * 重载：
	 * 
	 * @see com.tansuosoft.discoverx.common.Config#reload()
	 */
	@Override
	public Config reload() {
		if (m_dict != null) {
			m_dict.clear();
			m_dict = null;
		}
		if (m_authorizationEntries != null) {
			m_authorizationEntries.clear();
			m_authorizationEntries = null;
		}
		Config c = null;
		synchronized (m_lock) {
			c = m_instances.remove(OrganizationsContext.getInstance().getContextUserOrgSc());
		}
		return c;
	}
}

