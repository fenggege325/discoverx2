/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

/**
 * 使用用户别名和密码登录的系统基础用户登录凭据类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class BasicCredential implements Credential {
	private String m_account = null; // 帐号名，此处为用户别名。
	private String m_password = null; // 密码
	private String m_remoteAddress = null; // 尝试登录的帐号的来网络源地址。
	private String m_remoteAgent = null; // 尝试登录的帐号的客户端名称。

	/**
	 * 获取用户登录帐号。
	 * 
	 * <p>
	 * 此处返回的登录账号为用户别名。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Credential#getAccount()
	 */
	public String getAccount() {
		return this.m_account;
	}

	/**
	 * 设置用户登录帐号。
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Credential#setAccount(java.lang.String)
	 */
	public void setAccount(String account) {
		this.m_account = account;
	}

	/**
	 * 获取登录密码。
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Credential#getPassword()
	 */
	public String getPassword() {
		return this.m_password;
	}

	/**
	 * 设置登录密码。
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Credential#setPassword(java.lang.String)
	 */
	public void setPassword(String password) {
		this.m_password = password;
	}

	/**
	 * 获取登录客户端地址。
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Credential#getRemoteAddress()
	 */
	public String getRemoteAddress() {
		return this.m_remoteAddress;
	}

	/**
	 * 设置登录客户端地址。
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Credential#setRemoteAddress(java.lang.String)
	 */
	public void setRemoteAddress(String remoteAddress) {
		this.m_remoteAddress = remoteAddress;
	}

	/**
	 * 获取登录客户端名称。
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Credential#getRemoteAgent()
	 */
	public String getRemoteAgent() {
		return this.m_remoteAgent;
	}

	/**
	 * 获取登录客户端名称。
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Credential#setRemoteAgent(java.lang.String)
	 */
	public void setRemoteAgent(String remoteAgent) {
		this.m_remoteAgent = remoteAgent;
	}

}

