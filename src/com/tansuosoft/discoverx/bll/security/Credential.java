/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

/**
 * 用户验证凭据接口。
 * <p>
 * 所有实际用户验证凭据类要实现此接口。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public interface Credential {
	/**
	 * 获取或设置用户帐号。
	 * 
	 * <p>
	 * 帐号对应用户资源对象的某个属性或用户表中的某个字段，比如别名属性。
	 * </p>
	 * 
	 * @return String
	 */
	public String getAccount();

	/**
	 * @see Credential#getAccount()
	 * @param account String
	 */
	public void setAccount(String account);

	/**
	 * 获取或设置用户密码。
	 * 
	 * <p>
	 * 此处设置的密码应为加密后的密码，用于与数据库表中的保存的加密后的密码进行比对。
	 * </p>
	 * 
	 * @return String
	 */
	public String getPassword();

	/**
	 * @see Credential#getPassword()
	 * @param password String
	 */
	public void setPassword(String password);

	/**
	 * 返回尝试登录的帐号的来网络源地址。
	 * 
	 * @return String
	 */
	public String getRemoteAddress();

	/**
	 * 设置尝试登录的帐号的来网络源地址。
	 * 
	 * @param remoteAddress String
	 */
	public void setRemoteAddress(String remoteAddress);

	/**
	 * 获取尝试登录的帐号的客户端名称。
	 * 
	 * @return String
	 */
	public String getRemoteAgent();

	/**
	 * 设置尝试登录的帐号的客户端名称。
	 * 
	 * @param remoteAgent String
	 */
	public void setRemoteAgent(String remoteAgent);
}

