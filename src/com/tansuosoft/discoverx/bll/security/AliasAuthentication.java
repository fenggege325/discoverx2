/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.security;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 通过别名和密码登录的用户登录验证实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class AliasAuthentication implements Authentication {
	protected static final String USER_LOGIN_DBREQUEST_CFG_NAME = "DefaultUserLoginDBRequest";

	/**
	 * 重载authenticate：使用用户别名和密码登录。
	 * 
	 * @see com.tansuosoft.discoverx.bll.security.Authentication#authenticate(com.tansuosoft.discoverx.bll.security.Credential)
	 */
	public Identity authenticate(Credential credential) {
		String uid = credential.getAccount();
		String pwd = credential.getPassword();
		if (checkHasInjection(credential.getAccount()) || checkHasInjection(credential.getPassword())) FileLogger.debug("账号或密码中包含无效内容！");

		DBRequest request = com.tansuosoft.discoverx.dao.DAOConfig.getInstance().getDBRequest(USER_LOGIN_DBREQUEST_CFG_NAME);
		if (request == null) request = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.UserLoginByAlias");
		request.setParameter("uid", uid);
		request.setParameter("password", pwd);
		request.sendRequest();
		User user = request.getResult(User.class);

		if (user == null) return null;
		Identity identity = new Identity(user, credential);
		return identity;
	}

	/**
	 * 检查sql查询右值是否有非法内容。
	 * 
	 * @param v
	 * @return
	 */
	public static boolean checkHasInjection(String v) {
		String pv = v.replace('\r', ' ').replace('\n', ' ').replace('\t', ' ');
		return (pv.length() > 1024 && pv.indexOf("delete ") >= 0 || pv.indexOf("insert ") >= 0 || pv.indexOf("update ") >= 0 || pv.indexOf("union ") >= 0 || pv.indexOf("t_user.") > 0 || pv.indexOf("t_user.") >= 0);
	}
}
