/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;

/**
 * （永久）删除资源（从对应的xml或者数据库表记录删除指定资源对应的持久记录）的接口。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ResourceRemover extends EventSourceImpl {
	/**
	 * 缺省构造器。
	 */
	public ResourceRemover() {
	}

	/**
	 * 删除resource指定的资源。
	 * 
	 * @param resource Resource
	 * @param session Session 表示用户自定义会话信息。
	 * @return Resource 返回被删除的资源，如果不存在或者没有权限等其它异常，则返回null。
	 * @throws ResourceException
	 */
	public abstract Resource remove(Resource resource, Session session) throws ResourceException;
}

