/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.util.HashMap;
import java.util.List;

import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 资源别名与资源UNID互取的工具类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ResourceAliasContext {
	private static ResourceAliasContext m_instance = null;

	private HashMap<String, HashMap<String, String>> m_maps = null;

	/**
	 * 私有构造器。
	 */
	private ResourceAliasContext() {
		ResourceDescriptorConfig rdc = ResourceDescriptorConfig.getInstance();
		List<ResourceDescriptor> list = rdc.getResourceDescriptors();
		this.m_maps = new HashMap<String, HashMap<String, String>>(list != null && list.size() > 0 ? list.size() : 30);
		for (ResourceDescriptor rd : list) {
			if (rd == null || !rd.getCache()) continue;
			String directory = rd.getDirectory();
			if (ResourceDescriptorConfig.DOCUMENT_DIRECTORY.equalsIgnoreCase(directory)) continue;
			m_maps.put(directory, new HashMap<String, String>());
		}
	}

	/**
	 * 获取唯一实例。
	 * 
	 * @return ResourceAliasContext
	 */
	synchronized public static ResourceAliasContext getInstance() {
		if (m_instance == null) m_instance = new ResourceAliasContext();
		return m_instance;
	}

	/**
	 * 根据保存于xml文件中的资源的别名获取其对应的UNID。
	 * 
	 * @param directory String 资源所属目录名。
	 * @param alias String 资源别名。
	 * @return String 返回UNID结果，如果找不到或者不是保存于xml文件中的资源，则返回null。
	 */
	public String getUNIDByAlias(String directory, String alias) {
		if (directory == null || directory.length() == 0) return null;
		if (alias == null || alias.length() == 0) return null;
		HashMap<String, String> map = this.m_maps.get(directory);
		if (map == null) {
			map = new HashMap<String, String>();
			this.m_maps.put(directory, map);
		}
		String result = map.get(alias.toLowerCase());
		if (result == null) {
			List<Resource> list = XmlResourceLister.getResources(directory);
			if (list != null) {
				for (Resource x : list) {
					if (x == null) continue;
					String unid = x.getUNID();
					String xAlias = x.getAlias();
					if (StringUtil.isBlank(unid) || StringUtil.isBlank(xAlias)) continue;
					map.put(xAlias.toLowerCase(), unid);
					if (alias.equalsIgnoreCase(xAlias)) result = unid;
				}
			}
		}
		return result;
	}

	/**
	 * 根据保存于xml文件中的资源的别名获取其对应的UNID。
	 * 
	 * @param cls Class 资源实体类信息。
	 * @param alias String 资源别名，必须。
	 * @return String 返回UNID结果，如果找不到或者不是保存于xml文件中的资源，则返回null。
	 */
	public String getUNIDByAlias(Class<?> cls, String alias) {
		if (cls == null) return null;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(cls);
		if (rd == null) return null;
		String directory = rd.getDirectory();
		return this.getUNIDByAlias(directory, alias);
	}

	/**
	 * 更新指定类型的资源别名和UNID对应信息。
	 * 
	 * <p>
	 * 仅供系统内部调用。
	 * </p>
	 * 
	 * @param directory
	 * @param alias
	 * @param unid
	 */
	public void update(String directory, String alias, String unid) {
		if (directory == null || directory.length() == 0 || alias == null || alias.length() == 0 || unid == null || unid.length() == 0) return;
		HashMap<String, String> map = this.m_maps.get(directory);
		if (map == null) {
			map = new HashMap<String, String>();
			this.m_maps.put(directory, map);
		}
		map.put(alias.toLowerCase(), unid);
	}

	/**
	 * 删除指定类型的资源别名和UNID对应信息。
	 * 
	 * <p>
	 * 仅供系统内部调用。
	 * </p>
	 * 
	 * @param directory
	 * @param alias
	 */
	public void remove(String directory, String alias) {
		if (directory == null || directory.length() == 0 || alias == null || alias.length() == 0) return;
		HashMap<String, String> map = this.m_maps.get(directory);
		if (map != null) map.remove(alias.toLowerCase());
	}

	/**
	 * 根据资源UNID获取资源别名。
	 * 
	 * @param directory String 资源所属目录名。
	 * @param unid String 资源UNID。
	 * @return String 返回alias结果，如果找不到则返回null。
	 */
	public static String getAliasByUNID(String directory, String unid) {
		if (directory == null || directory.length() == 0) return null;
		if (unid == null || unid.length() == 0) return null;
		String result = null;
		Resource r = ResourceContext.getInstance().getResource(unid, directory);
		if (r != null) result = r.getAlias();
		return result;
	}

	/**
	 * 根据资源UNID获取资源别名。
	 * 
	 * @param cls Class 资源实体类信息。
	 * @param unid String 资源UNID。
	 * @return String 返回alias结果，如果找不到则返回null。
	 */
	public static String getAliasByUNID(Class<?> cls, String unid) {
		if (cls == null) return null;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(cls);
		if (rd == null) return null;
		Resource r = ResourceContext.getInstance().getResource(unid, rd);
		if (r != null) return r.getAlias();
		return null;
	}

}

