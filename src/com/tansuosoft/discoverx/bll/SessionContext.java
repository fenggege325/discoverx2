/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.HttpContext;

/**
 * 用于获取当前执行环境中对应的用户自定义会话信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public final class SessionContext {

	/**
	 * 记录最近登录用户帐号名称信息的Cookie名称。
	 */
	public static final String LOGINED_UAN_COOKIE_NAME = "uan";

	private static ThreadLocal<Session> m_instance = null;

	/**
	 * 事先保存用户自定义会话信息以供后续使用。
	 * 
	 * <p>
	 * <strong>仅供系统内部使用，请勿直接调用。</strong>
	 * </p>
	 * 
	 * @param session
	 */
	public static void storeSession(Session session) {
		if (session == null) {
			if (m_instance != null) reset();
			return;
		}
		if (m_instance == null) m_instance = new ThreadLocal<Session>() {
			// @Override
			// public String toString() {
			// return StringUtil.stringRightBack(this.getClass().getName(), ".") + "@" + this.hashCode() + ",TID:" + Thread.currentThread().getId();
			// }
		};
		m_instance.set(session);
	}

	/**
	 * 获取当前用户自定义会话。
	 * 
	 * <p>
	 * 系统中要获取用户自定义会话信息({@link com.tansuosoft.discoverx.model.Session})时，应该使用此方法获取。
	 * </p>
	 * 
	 * @param request {@link javax.servlet.http.HttpServletRequest}对象，如果不提供，则尝试从事先绑定的用户自定义会话中获取。
	 * @return 返回获取到的{@link com.tansuosoft.discoverx.model.Session}对象，如果取不到则返回null。
	 */
	public static Session getSession(HttpServletRequest request) {
		HttpSession httpSession = (request == null ? null : request.getSession());
		if (httpSession == null) {
			Session session = (m_instance != null ? m_instance.get() : null);
			if (session != null && request != null) session.setHttpRequest(request);
			return session;
		}
		Session session = (Session) httpSession.getAttribute(Session.USERSESSION_PARAM_NAME_IN_HTTPSESSION);
		if (session != null && request != null) {
			session.setHttpRequest(request);
		}
		return session;
	}

	/**
	 * 重置（清理）已经存在于线程本地变量中的信息。
	 * 
	 * <p>
	 * <strong>仅供系统内部使用，请勿直接调用。</strong>
	 * </p>
	 */
	public static void reset() {
		Session s = (m_instance == null ? null : m_instance.get());
		if (s != null) {
			s.setHttpRequest(null);
			s = null;
			m_instance.remove();
		}
	}

	/**
	 * 清理所有线程对应的线程本地变量保存的信息。
	 */
	public static void shutdown() {
		Thread[] ts = HttpContext.getThreads();
		if (ts != null) {
			for (Thread t : ts) {
				HttpContext.clearReferencesThreadLocals(m_instance, t);
			}
		}
		m_instance = null;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "@" + this.hashCode() + ",TID:" + Thread.currentThread().getId();
	}
}

