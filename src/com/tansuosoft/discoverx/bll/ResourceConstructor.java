/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.util.Map;

import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;

/**
 * 初始化（新增）资源（根据必要的信息在内存中构造一个新的资源对象实例并初始化资源相关属性）的接口。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ResourceConstructor extends EventSourceImpl {
	/**
	 * 缺省构造器。
	 */
	public ResourceConstructor() {
	}

	/**
	 * 构造资源目录指定的资源对象的实例。
	 * 
	 * @param directory String 资源目录。
	 * @param session Session 用户自定义会话。
	 * @param config String 配置信息，构造文档类型时必须。
	 * @param punid String 所属资源UNID，可选。
	 * @param params Map<String,Object> 额外参数，可选
	 * @return
	 * @throws ResourceException
	 */
	public abstract Resource construct(String directory, Session session, String config, String punid, Map<String, Object> params) throws ResourceException;

	/**
	 * 构造资源类信息指定的资源对象的实例。
	 * 
	 * @param clazz
	 * @param session
	 * @param config
	 * @param punid
	 * @param params
	 * @return
	 * @throws ResourceException
	 */
	public Resource construct(Class<? extends Resource> clazz, Session session, String config, String punid, Map<String, Object> params) throws ResourceException {
		return construct(ResourceDescriptorConfig.getInstance().getResourceDirectory(clazz), session, config, punid, params);
	}
}

