/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import com.tansuosoft.discoverx.bll.function.Function;
import com.tansuosoft.discoverx.common.event.EventArgs;

/**
 * 表示系统功能扩展函数调用相关事件的事件参数类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class FunctionExecutionEventArgs extends EventArgs {

	/**
	 * 缺省构造器。
	 */
	public FunctionExecutionEventArgs() {
	}

	/**
	 * 接收{@link Function}对象的构造器。
	 * 
	 * @param callable
	 */
	public FunctionExecutionEventArgs(Function callable) {
		this.m_callable = callable;
	}

	private Function m_callable = null; // 触发事件的具体功能函数实现对象。

	/**
	 * 返回触发事件的具体功能函数实现对象。
	 * 
	 * @return Function
	 */
	public Function getCallable() {
		return this.m_callable;
	}

	/**
	 * 设置触发事件的具体功能函数实现对象。
	 * 
	 * @param callable Function
	 */
	public void setCallable(Function callable) {
		this.m_callable = callable;
	}
}

