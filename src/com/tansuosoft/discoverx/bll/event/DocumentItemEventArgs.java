/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Session;

/**
 * 表示文档字段变化相关事件的事件参数类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DocumentItemEventArgs extends EventArgs {
	private Session m_session = null; // 当前用户自定义会话。
	private Document m_document = null; // 当前处理的文档。
	private String m_itemName = null; // 当前处理的字段。
	private String m_itemValueOld = null; // 当前处理的字段的字段原有值。
	private String m_itemValueNew = null; // 当前处理的字段的字段新值。

	/*
	 * 缺省构造器。
	 */
	public DocumentItemEventArgs() {
	}

	/**
	 * 接收必要参数的缺省构造器。
	 * 
	 * @param document
	 * @param itemName
	 * @param itemValueOld
	 * @param itemValueNew
	 * @param session
	 */
	public DocumentItemEventArgs(Document document, String itemName, String itemValueOld, String itemValueNew, Session session) {
		this.m_document = document;
		this.m_itemName = itemName;
		this.m_itemValueOld = itemValueOld;
		this.m_itemValueNew = itemValueNew;
		this.m_session = session;
	}

	/**
	 * 返回当前用户自定义会话。
	 * 
	 * @return Session
	 */
	public Session getSession() {
		return this.m_session;
	}

	/**
	 * 设置当前用户自定义会话。
	 * 
	 * @param session Session
	 */
	public void setSession(Session session) {
		this.m_session = session;
	}

	/**
	 * 返回当前处理的文档。
	 * 
	 * @return Document
	 */
	public Document getDocument() {
		return this.m_document;
	}

	/**
	 * 设置当前处理的文档。
	 * 
	 * @param document Document
	 */
	public void setDocument(Document document) {
		this.m_document = document;
	}

	/**
	 * 返回当前处理的字段名称。
	 * 
	 * @return String
	 */
	public String getItemName() {
		return this.m_itemName;
	}

	/**
	 * 设置当前处理的字段名称。
	 * 
	 * @param itemName String
	 */
	public void setItemName(String itemName) {
		this.m_itemName = itemName;
	}

	/**
	 * 返回当前处理的字段的原始值。
	 * 
	 * <p>
	 * 对于{@link com.tansuosoft.discoverx.common.event.EventHandler#EHT_DOCUMENTITEMVALUECHANGE}类型的事件，此值为null。
	 * </p>
	 * 
	 * @return String
	 */
	public String getItemValueOld() {
		return this.m_itemValueOld;
	}

	/**
	 * 设置当前处理的字段的原始值。
	 * 
	 * @param itemValueOld String
	 */
	public void setItemValueOld(String itemValueOld) {
		this.m_itemValueOld = itemValueOld;
	}

	/**
	 * 返回当前处理的字段的新值。
	 * 
	 * @return String
	 */
	public String getItemValueNew() {
		return this.m_itemValueNew;
	}

	/**
	 * 设置当前处理的字段的新值。
	 * 
	 * @param itemValueNew String
	 */
	public void setItemValueNew(String itemValueNew) {
		this.m_itemValueNew = itemValueNew;
	}

}

