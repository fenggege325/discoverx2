/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;

/**
 * 表示资源相关事件的事件参数类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ResourceEventArgs extends EventArgs {
	private Session m_session = null; // 当前用户自定义会话。
	private Resource m_resource = null; // 当前处理的资源。

	/**
	 * 缺省构造器。
	 */
	public ResourceEventArgs() {
	}

	/**
	 * 接收必要参数的缺省构造器。
	 * 
	 * @param resource
	 * @param session
	 */
	public ResourceEventArgs(Resource resource, Session session) {
		this.m_resource = resource;
		this.m_session = session;
	}

	/**
	 * 返回当前用户自定义会话。
	 * 
	 * @return Session 当前用户自定义会话。
	 */
	public Session getSession() {
		return this.m_session;
	}

	/**
	 * 设置当前用户自定义会话。
	 * 
	 * @param session Session 当前用户自定义会话。
	 */
	public void setSession(Session session) {
		this.m_session = session;
	}

	/**
	 * 返回当前处理的资源。
	 * 
	 * @return Resource 当前处理的资源。
	 */
	public Resource getResource() {
		return this.m_resource;
	}

	/**
	 * 设置当前处理的资源。
	 * 
	 * @param resource Resource 当前处理的资源。
	 */
	public void setResource(Resource resource) {
		this.m_resource = resource;
	}

}

