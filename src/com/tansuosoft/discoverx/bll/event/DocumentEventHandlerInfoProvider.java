/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.event.impl.OnDocumentSaveDefault;
import com.tansuosoft.discoverx.bll.event.impl.ProfileDocumentDeleteEventHandler;
import com.tansuosoft.discoverx.bll.event.impl.SetSequenceOnDocumentSave;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentType;
import com.tansuosoft.discoverx.model.Form;

/**
 * 提供文档事件处理程序信息对象的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DocumentEventHandlerInfoProvider extends EventHandlerInfoProvider {
	/**
	 * 缺省构造器。
	 */
	public DocumentEventHandlerInfoProvider() {
	}

	/**
	 * 返回forObject指向的文档定义的所有可用事件处理程序信息对象列表集合。
	 * 
	 * @see com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider#provide(java.lang.Object, java.lang.Object)
	 */
	@Override
	public List<EventHandlerInfo> provide(Object forObject, Object tag) {
		if (forObject == null || !(forObject instanceof Document)) return null;
		// 来自资源定义的事件
		List<EventHandlerInfo> result = new ArrayList<EventHandlerInfo>();
		List<EventHandlerInfo> raw = new ResourceEventHandlerInfoProvider().provide(forObject, tag);
		this.listAddAll(result, raw);
		Document doc = (Document) forObject;
		if (doc.getType() == DocumentType.Profile) {
			result.add(new EventHandlerInfo(ProfileDocumentDeleteEventHandler.class.getName(), EventHandler.EHT_DELETED, "删除后同步删除配置文档所在应用程序的配置文档链接", result.size()));
		}
		// 所属直接应用中定义的事件
		Application app = doc.getApplication(doc.getApplicationCount());
		if (app != null) this.listAddAll(result, app.getEventHandlers());

		// 所属表单中定义的事件
		Form form = doc.getForm();
		if (form != null) {
			this.listAddAll(result, form.getEventHandlers());
		}

		EventHandlerInfo ehid = new EventHandlerInfo();
		ehid.setEventHandler(OnDocumentSaveDefault.class.getName());
		ehid.setEventHandlerName("默认保存前事件处理程序");
		ehid.setEventHandlerType(EventHandler.EHT_SAVE);
		ehid.setSort(Integer.MAX_VALUE - 100);
		result.add(ehid);

		// 文档保存前自动设置文档流水号和业务号信息的事件处理程序，确保其在最后触发。
		EventHandlerInfo ehi = new EventHandlerInfo();
		ehi.setEventHandler(SetSequenceOnDocumentSave.class.getName());
		ehi.setEventHandlerName("自动设置文档流水号和业务号信息");
		ehi.setEventHandlerType(EventHandler.EHT_SAVE);
		ehi.setSort(Integer.MAX_VALUE);
		result.add(ehi);

		EventHandlerInfo srodo = new EventHandlerInfo();
		srodo.setEventHandler("com.tansuosoft.discoverx.workflow.event.SetReadOnDocumentOpened");
		srodo.setEventHandlerName("设置已读标记");
		srodo.setEventHandlerType(EventHandler.EHT_OPENED);
		srodo.setSort(1);
		result.add(srodo);

		// 保存后保存字段组字段值到独立表
		EventHandlerInfo gvtOnSaved = new EventHandlerInfo();
		gvtOnSaved.setEventHandler("com.tansuosoft.discoverx.app.gvt.OnSaved");
		gvtOnSaved.setEventHandlerName("字段组包含的字段值保存");
		gvtOnSaved.setEventHandlerType(EventHandler.EHT_SAVED);
		gvtOnSaved.setSort(Integer.MAX_VALUE);
		result.add(gvtOnSaved);

		// 删除后从独立表删除字段组字段值
		EventHandlerInfo gvtOnRemoved = new EventHandlerInfo();
		gvtOnRemoved.setEventHandler("com.tansuosoft.discoverx.app.gvt.OnRemoved");
		gvtOnRemoved.setEventHandlerName("字段组包含的字段值删除");
		gvtOnRemoved.setEventHandlerType(EventHandler.EHT_DELETED);
		gvtOnRemoved.setSort(Integer.MAX_VALUE);
		result.add(gvtOnRemoved);

		return result;
	}
}

