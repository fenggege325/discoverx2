/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.event.impl.ApplicationDeleteEventHandler;
import com.tansuosoft.discoverx.bll.event.impl.FormViewTableRebuildEventHandler;
import com.tansuosoft.discoverx.bll.event.impl.OnFormDelete;
import com.tansuosoft.discoverx.bll.event.impl.SecurerChangedEventHandler;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Securer;

/**
 * 提供资源事件处理程序信息对象的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ResourceEventHandlerInfoProvider extends EventHandlerInfoProvider {
	/**
	 * 缺省构造器。
	 */
	public ResourceEventHandlerInfoProvider() {
	}

	/**
	 * 返回forObject指向的资源定义的所有可用事件处理程序信息对象列表集合。
	 * 
	 * @see com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider#provide(java.lang.Object, java.lang.Object)
	 */
	@Override
	public List<EventHandlerInfo> provide(Object forObject, Object tag) {
		if (forObject == null || !(forObject instanceof Resource)) return null;
		List<EventHandlerInfo> result = new ArrayList<EventHandlerInfo>();
		if (forObject instanceof Application) {
			EventHandlerInfo eventHandler = new EventHandlerInfo(ApplicationDeleteEventHandler.class.getName(), EventHandler.EHT_DELETED, "同步删除应用包含的表单、流程、视图、下级应用、附加文件等", 0);
			result.add(eventHandler);
		}
		if (forObject instanceof Securer) {
			String impl = SecurerChangedEventHandler.class.getName();
			EventHandlerInfo eventHandler = new EventHandlerInfo(impl, EventHandler.EHT_SAVED, "参与者保存完毕内置事件", 0);
			result.add(eventHandler);
			eventHandler = new EventHandlerInfo(impl, EventHandler.EHT_DELETED, "参与者删除完毕内置事件", 0);
			result.add(eventHandler);
		}
		if (forObject instanceof Form) {
			EventHandlerInfo eventHandler = new EventHandlerInfo(FormViewTableRebuildEventHandler.class.getName(), EventHandler.EHT_SAVED, "同步表单字段信息到数据表", 1);
			result.add(eventHandler);
			if (OrganizationsContext.getInstance().isInTenantContext()) {
				eventHandler = new EventHandlerInfo(FormViewTableRebuildEventHandler.class.getName(), EventHandler.EHT_DELETE, "检查多租户模式下能否删除表单", 0);
				result.add(eventHandler);
				eventHandler = new EventHandlerInfo(FormViewTableRebuildEventHandler.class.getName(), EventHandler.EHT_SAVE, "检查多租户模式下能否保存表单", 0);
				result.add(eventHandler);
			}
			eventHandler = new EventHandlerInfo(OnFormDelete.class.getName(), EventHandler.EHT_DELETED, "删除后同步删除应用程序中的配置文件信息", 2);
			result.add(eventHandler);
			eventHandler = new EventHandlerInfo("com.tansuosoft.discoverx.web.ui.document.ReloadItemsParser", EventHandler.EHT_SAVED, "更新表单字段解析缓存", 4);
			result.add(eventHandler);
		}
		GlobalEventHandlerInfoProvider gehi = new GlobalEventHandlerInfoProvider();
		this.listAddAll(result, gehi.provide("construct,constructed,open,opened,save,saved,delete,deleted,close,closed,documentsetstatebegin,documentsetstateend,documentitemvaluechange,documentitemvaluechanged", null));
		Resource resource = (Resource) forObject;
		String directory = resource.getDirectory();
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
		if (rd == null) return result;
		this.listAddAll(result, rd.getEventHandlers());
		return result;
	}
}

