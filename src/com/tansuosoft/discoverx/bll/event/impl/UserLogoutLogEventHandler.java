/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import com.tansuosoft.discoverx.bll.event.UserActivityEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.LogType;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 记录用户注销日志的事件处理程序实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class UserLogoutLogEventHandler implements EventHandler {

	/**
	 * 记录用户注销信息到系统日志中。
	 */
	public void handle(Object sender, EventArgs e) {
		Log log = new Log();
		UserActivityEventArgs userEvent = (UserActivityEventArgs) e;
		if (userEvent == null) return;
		Session s = userEvent.getSession();
		if (s == null) return;
		User u = s.getUser();
		log.setCreated(new DateTime().toString());
		log.setSubject(ParticipantHelper.getFormatValue(u, "cn"));
		log.setVerb("注销");
		log.setOwner(u.getUNID());
		log.setResult("成功");
		log.setLogType(LogType.User);
		String object = s.getLoginClient() + "[" + s.getLoginAddress() + "]";
		if (object.length() > 2000) object = object.substring(0, 1999);
		log.setObject(object);
		// 插入数据库
		DBRequest request = (DBRequest) Instance.newInstance("com.tansuosoft.discoverx.dao.impl.LogInserter");
		if (request == null) return;
		request.setParameter(DBRequest.ENTITY_PARAM_NAME, log);
		request.sendRequest();
	}
}

