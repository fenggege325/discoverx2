/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import java.io.File;
import java.util.List;

import com.tansuosoft.discoverx.bll.AccessoryPathHelper;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.bll.resource.XmlResourceRemover;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Portal;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.FileHelper;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 应用程序删除后执行包含的表单、视图、流程、下级应用等同步删除的事件处理程序类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ApplicationDeleteEventHandler implements EventHandler {
	private Session session = null;
	private XmlResourceRemover xmlResourceRemover = new XmlResourceRemover();
	private ResourceContext rc = ResourceContext.getInstance();

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	public void handle(Object sender, EventArgs e) {
		ResourceEventArgs resourceEvent = (ResourceEventArgs) e;
		session = resourceEvent.getSession();
		Application app = (Application) resourceEvent.getResource();
		deleteApplication(app);
		// 删除前端代码路径
		if (app.getParamValueBool("keep_app_program_dir", true)) return;
		String alias = app.getAlias();
		if (alias == null || alias.trim().length() == 0) return;
		List<Resource> portals = XmlResourceLister.getResources(Portal.class);
		if (portals == null || portals.isEmpty()) return;
		String base = CommonConfig.getInstance().getInstallationPath();
		for (Resource r : portals) {
			if (r == null || !(r instanceof Portal)) continue;
			Portal p = (Portal) r;
			String frame = p.getFrame();
			if (StringUtil.isBlank(frame)) continue;
			String path = base + frame + File.separator + alias;
			FileHelper.deleteFolder(path);
		}
	}

	/**
	 * 删除应用程序绑定的表单、流程、视图、下级应用程序、附加文件等。
	 * 
	 * @param app
	 */
	protected void deleteApplication(Application app) {
		if (app == null) return;
		String appUNID = app.getUNID();

		// 删除绑定的表单
		String frm = app.getForm();
		if (frm != null && frm.length() > 0 && !checkAnotherReference(appUNID, frm)) {
			xmlResourceRemover.remove(rc.getResource(frm, "form"), session);
		}

		// 删除绑定的配置文件表单
		String pfrm = app.getProfileForm();
		if (pfrm != null && pfrm.length() > 0 && !checkAnotherReference(appUNID, pfrm)) {
			xmlResourceRemover.remove(rc.getResource(pfrm, "form"), session);
		}

		// 删除绑定的流程
		String wf = app.getWorkflow();
		if (wf != null && wf.length() > 0 && !checkAnotherReference(appUNID, wf)) {
			xmlResourceRemover.remove(rc.getResource(wf, "workflow"), session);
		}

		// 删除绑定的视图
		List<Reference> views = app.getViews();
		if (views != null) {
			for (Reference x : views) {
				if (checkAnotherReference(appUNID, x.getUnid())) continue;
				xmlResourceRemover.remove(rc.getResource(x.getUnid(), "view"), session);
			}
		}

		// 删除绑定的下级应用程序
		List<Reference> subs = app.getSubApplications();
		if (subs != null) {
			Application sub = null;
			for (Reference x : subs) {
				sub = rc.getResource(x.getUnid(), Application.class);
				if (sub == null) continue;
				if (checkAnotherReference(appUNID, x.getUnid())) continue;
				xmlResourceRemover.remove(sub, session);
			}
		}

		// 删除绑定的附加文件
		List<Accessory> accs = app.getAccessories();
		if (accs != null) {
			String commonDir = AccessoryPathHelper.getAbsoluteAccessoryServerPath(app);
			String orgVarDir = null;
			int contextOrgSc = 0;
			boolean hasValOrgVarDir = false;
			if (OrganizationsContext.getInstance().isInTenantContext() && (contextOrgSc = OrganizationsContext.getInstance().getContextUserOrgSc()) > 0) {
				orgVarDir = commonDir + ResourceContext.ORG_VAR_BASE + contextOrgSc + File.separator;
			}
			for (Accessory x : accs) {
				if (checkAnotherReference(appUNID, x.getUNID())) continue;
				if (orgVarDir != null && new File(orgVarDir).exists()) {
					if (!hasValOrgVarDir) hasValOrgVarDir = true;
					deleteFile(orgVarDir + x.getFileName());
				} else {
					deleteFile(commonDir + x.getFileName());
				}
				rc.removeObjectFromCache(x.getUNID());
			}
			deleteFile(hasValOrgVarDir ? orgVarDir : commonDir);
		}// if end
	}

	/**
	 * 删除指定文件。
	 * 
	 * @param location
	 */
	protected void deleteFile(String location) {
		File f = new File(location);
		if (!f.exists()) return;
		if (!f.delete()) f.deleteOnExit();
	}

	/**
	 * 检查是否别的应用程序有引用unid指定的资源
	 * 
	 * @param deletedAppUnid
	 * @param unid
	 * @return
	 */
	protected boolean checkAnotherReference(String deletedAppUnid, String unid) {
		if (unid == null) return false;
		List<Resource> apps = XmlResourceLister.getResources(Application.class);
		if (apps == null) return false;
		Application app = null;
		List<Reference> subs = null;
		List<Accessory> accs = null;
		List<Reference> views = null;
		for (Resource x : apps) {
			app = (Application) x;
			if (app != null && app.getUNID().equalsIgnoreCase(deletedAppUnid) && !app.isCommon()) return true;
			if (app == null || app.getUNID().equalsIgnoreCase(deletedAppUnid)) continue;
			if (unid.equalsIgnoreCase(app.getForm())) return true;
			if (unid.equalsIgnoreCase(app.getProfileForm())) return true;
			if (unid.equalsIgnoreCase(app.getWorkflow())) return true;
			subs = app.getSubApplications();
			if (subs != null && subs.size() > 0) {
				for (Reference r : subs) {
					if (unid.equalsIgnoreCase(r.getUnid())) return true;
				}
			}
			accs = app.getAccessories();
			if (accs != null && accs.size() > 0) {
				for (Accessory a : accs) {
					if (unid.equalsIgnoreCase(a.getUNID())) return true;
				}
			}
			views = app.getViews();
			if (views != null && views.size() > 0) {
				for (Reference r : views) {
					if (unid.equalsIgnoreCase(r.getUnid())) return true;
				}
			}
		}
		return false;
	}
}
