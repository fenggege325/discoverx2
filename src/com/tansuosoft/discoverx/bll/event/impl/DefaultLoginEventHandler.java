/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.event.LoginEventArgs;
import com.tansuosoft.discoverx.bll.security.Credential;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.util.Escape;

/**
 * 默认用户登录完成事件处理程序实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DefaultLoginEventHandler implements EventHandler {
	/**
	 * 记录登录帐号信息。
	 */
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof LoginEventArgs)) return;
		LoginEventArgs lee = (LoginEventArgs) e;
		Credential c = lee.getCredential();
		if (c == null || c.getAccount() == null) return;
		HttpServletRequest request = lee.getHttpServletRequest();
		HttpServletResponse response = lee.getHttpServletResponse();
		if (request == null || response == null) return;
		String cookiePath = request.getContextPath();
		if (cookiePath == null || cookiePath.length() == 0) cookiePath = "/";

		// 记录登录用户帐号名称到cookie
		Cookie cookie = new Cookie(SessionContext.LOGINED_UAN_COOKIE_NAME, Escape.escape(c.getAccount()));// 用户登录帐号名
		cookie.setMaxAge(60 * 60 * 24 * 360); // cookie有效时间：一年
		cookie.setPath(cookiePath); // 在根路径下创建
		response.addCookie(cookie);
	}

}

