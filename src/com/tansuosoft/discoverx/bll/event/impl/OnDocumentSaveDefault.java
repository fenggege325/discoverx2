/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import java.util.List;

import com.tansuosoft.discoverx.bll.ApplicationProfile;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.exception.ResourceException;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.DocumentType;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemValueComputationMethod;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 文档保存前默认事件处理程序。
 * 
 * <p>
 * 执行保存前计算的公式结果并设置到字段值中。
 * </p>
 * <p>
 * 系统内部使用。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class OnDocumentSaveDefault implements EventHandler {

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof ResourceEventArgs)) return;
		ResourceEventArgs rea = ((ResourceEventArgs) e);
		Resource r = rea.getResource();
		if (r == null || !(r instanceof Document)) return;
		Document doc = (Document) r;
		Session s = rea.getSession();
		String evaledItemValue = null;
		String formulaOnSave = null;
		String itemName = null;
		try {
			List<Item> formItems = doc.getForm().getItems();
			if (formItems == null || formItems.isEmpty()) throw new ResourceException("文档保存错误：文档所属表单中没有字段信息！");
			formulaOnSave = null;
			evaledItemValue = null;
			itemName = null;
			try {
				for (Item x : formItems) {
					if (x == null) continue;
					itemName = x.getItemName();
					formulaOnSave = x.getComputationExpression();
					if (formulaOnSave == null || formulaOnSave.length() == 0) continue;
					if (x.getComputationMethod() == ItemValueComputationMethod.Save || (doc.checkState(DocumentState.New) && x.getComputationMethod() == ItemValueComputationMethod.FirstSave)) {
						ExpressionEvaluator eval = new ExpressionEvaluator(formulaOnSave, s, doc);
						evaledItemValue = eval.evalString();
						doc.replaceItemValue(itemName, evaledItemValue);
					}
				}
			} catch (Exception ex) {
				if (itemName != null && formulaOnSave != null) FileLogger.debug("处理文档“%1$s”中“%2$s”字段的保存时公式“%3$s”时出现错误：%4$s", doc.getName(), itemName, formulaOnSave, ex.getMessage());
			}
			if (doc.getType() == DocumentType.Profile) ApplicationProfile.clear(doc);
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}// func end
}// class end

