/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import java.util.List;

import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.view.FormViewTable;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;

/**
 * 表单保存后触发相应视图查询表检查重建事件处理程序类。
 * 
 * @author coca@tansuosoft.cn
 */
public class FormViewTableRebuildEventHandler implements EventHandler {

	/**
	 * 检查并重建视图查询表。
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(Object, EventArgs)
	 */
	public void handle(Object sender, EventArgs e) {
		ResourceEventArgs rea = (ResourceEventArgs) e;
		Form form = (Form) rea.getResource();
		if (form == null) return;
		if (rea.getEventHandlerType().equalsIgnoreCase(EventHandler.EHT_SAVE)) {
			if (OrganizationsContext.getInstance().isInTenantContext() && FormViewTable.checkFormViewTable(form)) throw new RuntimeException("多租户模式下无法执行增删表单字段、更改表单字段名等操作，您必须升级到独立版才能更改表单字段！");
		} else if (rea.getEventHandlerType().equalsIgnoreCase(EventHandler.EHT_DELETE)) {
			if (OrganizationsContext.getInstance().isInTenantContext() && form.isCommon()) throw new RuntimeException("多租户模式下无法删除公用表单，您必须升级到独立版才能删除表单！");
		} else if (rea.getEventHandlerType().equalsIgnoreCase(EventHandler.EHT_SAVED)) {
			String alias = form.getAlias();
			List<Item> list = form.getItems();
			if (alias == null || alias.length() == 0 || list == null || list.isEmpty()) return;
			FormViewTable.getInstance().checkAndRebuildFormViewTable(form, true);
		}
	}
}

