/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 文档保存前设置流水号、业务号相关操作。
 * 
 * <p>
 * 系统内部使用，需确保本事件处理程序在最后执行。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SetSequenceOnDocumentSave implements EventHandler {

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof ResourceEventArgs)) return;
		ResourceEventArgs rea = ((ResourceEventArgs) e);
		Resource r = rea.getResource();
		if (r == null || !(r instanceof Document)) return;
		Document doc = (Document) r;
		// 如果是新文档且配置了有效的流水号或业务号模式，那么要设置流水号和业务号
		if (doc.checkState(DocumentState.New)) {
			boolean processSequencePatternFlag = false;
			String processSequencePatternKey = null;
			Application processSequencePatternApp = null;

			boolean businessSequencePatternFlag = false;
			String businessSequencePatternKey = null;
			Application businessSequencePatternApp = null;

			Application app = null;
			int appCount = doc.getApplicationCount();
			for (int i = appCount; i >= 1; i--) {
				app = doc.getApplication(i);
				if (app == null) continue;
				if (!processSequencePatternFlag) {
					processSequencePatternFlag = (!StringUtil.isBlank(app.getProcessSequencePattern()));
					if (processSequencePatternFlag) {
						processSequencePatternKey = "PS" + app.getAlias();
						processSequencePatternApp = app;
					}
				}
				if (!businessSequencePatternFlag) {
					businessSequencePatternFlag = (!StringUtil.isBlank(app.getBusinessSequencePattern()));
					if (businessSequencePatternFlag) {
						businessSequencePatternKey = "BS" + app.getAlias();
						businessSequencePatternApp = app;
					}
				}
			} // for end
			// 初始化流水号信息
			if (processSequencePatternFlag) {
				DateTime dt = new DateTime();
				doc.setProcessYear(StringUtil.getValueInt(dt.toString("yyyy"), -1));
				int init = processSequencePatternApp.getParamValueInt("processSequenceInitValue", 0);
				int step = processSequencePatternApp.getParamValueInt("processSequenceStepValue", 0);
				doc.setProcessSequence((int) SequenceProvider.getInstance(processSequencePatternKey, init).getSequence(step));
			}
			// 初始化业务号信息
			if (businessSequencePatternFlag) {
				DateTime dt = new DateTime();
				doc.setBusinessYear(StringUtil.getValueInt(dt.toString("yyyy"), -1));
				int init = businessSequencePatternApp.getParamValueInt("businessSequenceInitValue", 0);
				int step = businessSequencePatternApp.getParamValueInt("businessSequenceStepValue", 0);
				doc.setBusinessSequence((int) SequenceProvider.getInstance(businessSequencePatternKey, init).getSequence(step));
			}
		} // if end

		// 如果文档存在用于保存流水号和业务号结果的指定名称的字段，则设置其值。
		String pprFldName = "fld_processpatternresult";
		if (doc.hasItem(pprFldName)) doc.replaceItemValue(pprFldName, doc.getProcessPatternResult());
		String bprFldName = "fld_businesspatternresult";
		if (doc.hasItem(bprFldName)) doc.replaceItemValue(bprFldName, doc.getBusinessPatternResult());
	}// func end
}// class end

