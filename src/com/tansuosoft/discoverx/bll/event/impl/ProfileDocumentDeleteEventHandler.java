/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import com.tansuosoft.discoverx.bll.ApplicationProfile;
import com.tansuosoft.discoverx.bll.ResourceUpdater;
import com.tansuosoft.discoverx.bll.ResourceUpdaterProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Session;

/**
 * 配置文档删除后同步删除应用程序中的链接的事件处理程序类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ProfileDocumentDeleteEventHandler implements EventHandler {

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	public void handle(Object sender, EventArgs e) {
		ResourceEventArgs eea = (ResourceEventArgs) e;
		Document doc = (Document) eea.getResource();
		if (doc == null) return;
		Application app = doc.getApplication(doc.getApplicationCount());
		if (app == null) return;
		ApplicationProfile.clear(app);
		app.setProfile(null);
		Session s = Session.getSystemSession();
		ResourceUpdater updater = ResourceUpdaterProvider.getResourceUpdater(app);
		updater.update(app, s);
	}

}

