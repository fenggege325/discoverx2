/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import java.util.List;

import com.tansuosoft.discoverx.bll.LogHelper;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.bll.resource.DBResourceRemover;
import com.tansuosoft.discoverx.bll.user.Profile;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.LogType;
import com.tansuosoft.discoverx.model.Organization;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Securer;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 参与者类型的资源保存后触发的事件处理程序类。
 * 
 * @author coca@tansuosoft.cn
 */
public class SecurerChangedEventHandler implements EventHandler {

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	public void handle(Object sender, EventArgs e) {
		ResourceEventArgs resourceEvent = (ResourceEventArgs) e;
		Resource r = resourceEvent.getResource();
		if (r == null || !(r instanceof Securer)) return;
		Securer securer = (Securer) resourceEvent.getResource();
		if (securer == null) return;
		String unid = r.getUNID();
		if (r instanceof Organization) {
			ResourceContext.getInstance().removeObjectFromCache(unid);
			// 删除部门时递归删除下级部门及用户
			if (sender != null && sender instanceof DBResourceRemover) {
				ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
				ParticipantTree pt = ptp.getParticipantTree(securer.getSecurityCode());
				DBRequest dbr = new DBRequest() {
					@Override
					protected SQLWrapper buildSQL() {
						return SQLWrapper.getNonRequestSQLWrapper();
					}
				};
				try {
					dbr.setUseTransaction(true);
					assembleOrganizationRemoveDBRequest(pt, dbr, pt.getSecurityCode());
					dbr.sendRequest();
				} catch (Exception ex) {
					FileLogger.error(ex);
				}
			}
			if (StringUtil.isBlank(r.getPUNID())) OrganizationsContext.getInstance().add(r.getUNID(), securer.getSecurityCode());
			ResourceContext.getInstance().removeObjectFromCache(OrganizationsContext.getInstance().getOrgUnid(resourceEvent.getSession()));
		} else if (r instanceof User && sender != null && sender instanceof DBResourceRemover) {
			// 删除用户的同时删除其配置文档
			try {
				User u = (User) r;
				Profile p = Profile.getInstance(u);
				Document profiledoc = (p == null ? null : p.getDocument());
				if (profiledoc != null) {
					DBResourceRemover rm = new DBResourceRemover();
					if (rm.remove(profiledoc, resourceEvent.getSession()) != null) {
						Profile.clear(u);
					}
				}
			} catch (Exception ex) {
				FileLogger.error(ex);
			}
		}
		ResourceContext.getInstance().removeObjectFromCache(unid);
		ParticipantTreeProvider.reload();

		// 日志
		Log log = new Log();
		log.setCreated(new DateTime().toString());
		User curuser = Session.getUser(resourceEvent.getSession());
		log.setSubject(ParticipantHelper.getFormatValue(curuser, "ou0\\cn"));
		log.setVerb((sender != null && sender instanceof DBResourceRemover) ? "删除" : "更改");
		log.setObject(r.toString() + "/" + r.getName());
		log.setOwner(curuser.getUNID());
		log.setResult("成功");
		log.setLogType(LogType.User);
		LogHelper.writeLog(log);
	}

	/**
	 * 构造删除部门时同步删除其下的子部门、用户及对应的配置文件文档数据库请求链。
	 * 
	 * @param pt
	 * @param dbr
	 * @param rootsc
	 */
	protected static void assembleOrganizationRemoveDBRequest(ParticipantTree pt, DBRequest dbr, int rootsc) {
		if (pt == null || pt.getParticipantType() != ParticipantType.Organization) return;
		// 包含的用户配置文档字段
		DBRequest dbrpdocitem = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper sqlw = new SQLWrapper();
				String punid = this.getParamValueString("punid", "");
				sqlw.setSql("delete from t_item where c_punid in (select c_profile from t_user where c_punid='" + punid + "')");
				sqlw.setRequestType(RequestType.NonQuery);
				return sqlw;
			}
		};
		dbrpdocitem.setParameter("punid", pt.getUNID());
		dbr.setNextRequest(dbrpdocitem);
		// 包含的用户配置文档附加文件
		DBRequest dbrpdocacc = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper sqlw = new SQLWrapper();
				String punid = this.getParamValueString("punid", "");
				sqlw.setSql("delete from t_accessory where c_punid in (select c_profile from t_user where c_punid='" + punid + "')");
				sqlw.setRequestType(RequestType.NonQuery);
				return sqlw;
			}
		};
		dbrpdocacc.setParameter("punid", pt.getUNID());
		dbr.setNextRequest(dbrpdocacc);
		// 包含的用户配置文档安全
		DBRequest dbrpdocsec = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper sqlw = new SQLWrapper();
				String punid = this.getParamValueString("punid", "");
				sqlw.setSql("delete from t_security where c_punid in (select c_profile from t_user where c_punid='" + punid + "')");
				sqlw.setRequestType(RequestType.NonQuery);
				return sqlw;
			}
		};
		dbrpdocsec.setParameter("punid", pt.getUNID());
		dbr.setNextRequest(dbrpdocsec);
		// 包含的用户配置文档额外参数
		DBRequest dbrpdocp = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper sqlw = new SQLWrapper();
				String punid = this.getParamValueString("punid", "");
				sqlw.setSql("delete from t_parameter where c_punid in (select c_profile from t_user where c_punid='" + punid + "')");
				sqlw.setRequestType(RequestType.NonQuery);
				return sqlw;
			}
		};
		dbrpdocp.setParameter("punid", pt.getUNID());
		dbr.setNextRequest(dbrpdocp);
		// 包含的用户配置文档本身
		DBRequest dbrpdoc = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper sqlw = new SQLWrapper();
				String punid = this.getParamValueString("punid", "");
				sqlw.setSql("delete from t_document where c_unid in (select c_profile from t_user where c_punid='" + punid + "')");
				sqlw.setRequestType(RequestType.NonQuery);
				return sqlw;
			}
		};
		dbrpdoc.setParameter("punid", pt.getUNID());
		dbr.setNextRequest(dbrpdoc);
		// 包含的用户和下级部门
		DBRequest dbrsub = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper sqlw = new SQLWrapper();
				String punid = this.getParamValueString("punid", "");
				sqlw.setSql("delete from t_user where c_punid='" + punid + "'");
				sqlw.setRequestType(RequestType.NonQuery);
				return sqlw;
			}
		};
		dbrsub.setParameter("punid", pt.getUNID());
		dbr.setNextRequest(dbrsub);

		List<ParticipantTree> pts = pt.getChildren();
		if (pts != null && pts.size() > 0) {
			for (ParticipantTree ptx : pts) {
				if (ptx == null || ptx.getParticipantType() != ParticipantType.Organization) continue;
				assembleOrganizationRemoveDBRequest(ptx, dbr, rootsc);
			}
		}
	}
}
