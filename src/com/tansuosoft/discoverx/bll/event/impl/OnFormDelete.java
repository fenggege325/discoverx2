/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import com.tansuosoft.discoverx.bll.ApplicationProfile;
import com.tansuosoft.discoverx.bll.ResourceUpdater;
import com.tansuosoft.discoverx.bll.ResourceUpdaterProvider;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Session;

/**
 * 表单删除后触发的事件处理程序类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OnFormDelete implements EventHandler {

	/**
	 * 事件处理实现函数。
	 * 
	 * <p>
	 * 删除配置文件表单时删除应用程序中的对应信息。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(Object, EventArgs)
	 */
	public void handle(Object sender, EventArgs e) {
		ResourceEventArgs resourceEvent = (ResourceEventArgs) e;
		Resource r = resourceEvent.getResource();
		if (r == null || !(r instanceof Form)) return;
		Form form = (Form) r;
		Session s = resourceEvent.getSession();
		String punid = form.getPUNID();
		if (punid == null) return;
		Application app = ResourceContext.getInstance().getResource(punid, Application.class);
		if (app == null) return;
		boolean needUpdate = false;
		String profileForm = app.getProfileForm();
		if (form.getUNID().equalsIgnoreCase(profileForm)) {
			app.setProfileForm(null);
			needUpdate = true;
		}
		if (needUpdate) {
			ApplicationProfile.clear(app);
			String profile = app.getProfile();
			if (profile != null && profile.length() > 0) {
				app.setProfile(null);
				// 是否要删除数据库中的配置文档？
			}
			ResourceUpdater updater = ResourceUpdaterProvider.getResourceUpdater(app);
			updater.update(app, s);
		}
	}
}
