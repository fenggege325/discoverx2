/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.HttpContext;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 文档构造完成后根据用户自定义会话重的同名参数值和http请求中的同名请求参数值设置文档字段值。
 * 
 * @author coca@tansuosoft.cn
 */
public class SetFieldOnDocumentInitialized implements EventHandler {
	protected static final String PN_ALIAS = "alias";
	protected static final String PN_TAG = "tag";
	protected static final String PN_DESC = "description";

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		try {
			if (e == null || !(e instanceof ResourceEventArgs)) return;
			ResourceEventArgs rea = ((ResourceEventArgs) e);
			Resource r = rea.getResource();
			if (r == null || !(r instanceof Document)) return;
			Document doc = (Document) r;
			Session session = rea.getSession();
			HttpServletRequest request = (session == null ? null : session.getHttpRequest());
			if (request == null) request = HttpContext.getRequest();

			String alias = getValue(PN_ALIAS, request, session);
			if (alias != null) doc.setAlias(alias);
			String tag = getValue(PN_TAG, request, session);
			if (tag != null) doc.setTag(tag);
			String desc = getValue(PN_DESC, request, session);
			if (desc != null) doc.setDescription(desc);

			Form f = doc.getForm();
			if (f == null) return;
			List<Item> items = f.getItems();
			if (items == null || items.isEmpty()) return;
			String itemName = null;
			String v = null;
			for (Item item : items) {
				if (item == null) continue;
				itemName = item.getItemName();
				v = getValue(itemName, request, session);
				if (v != null) doc.replaceItemValue(itemName, v);
			}
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}

	/**
	 * 获取要额外设置到文档属性值或字段值的参数值。
	 * 
	 * <p>
	 * 先从http请求中获取，如果没有则尝试从自定义会话中获取。
	 * </p>
	 * 
	 * @param name
	 * @param request
	 * @param session
	 * @return String
	 */
	protected static String getValue(final String name, final HttpServletRequest request, final Session session) {
		String v = null;
		if (request != null) v = request.getParameter(name);
		if (v == null && session != null) v = session.getParamValueString(name, null);
		return v;
	}
}

