/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.tansuosoft.discoverx.common.EventHandlerConfig;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;

/**
 * 提供全局配置的事件处理程序信息对象的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class GlobalEventHandlerInfoProvider extends EventHandlerInfoProvider {
	/**
	 * 缺省构造器。
	 */
	public GlobalEventHandlerInfoProvider() {
	}

	/**
	 * 返回全局配置（通过{@link EventHandlerConfig#getEventHandlers()}获取的事件处理程序）的所有可用事件处理程序信息对象列表集合。
	 * 
	 * <p>
	 * forObject参数可以提供过滤选项。<br/>
	 * forObject过滤选项的类型为字符串，格式为：“[+/-]eventHandlerType1,[+/-]eventHandlerType2...”<br/>
	 * 如“+userlogin;userlogout;-save” 表示返回的是包含用户登录完成、用户注销完成且不包含保存的所有事件处理程序信息。<br/>
	 * 如果forObject为null或空字符串，则返回所有配置的事件处理程序信息。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider#provide(java.lang.Object, java.lang.Object)
	 */
	@Override
	public List<EventHandlerInfo> provide(Object forObject, Object tag) {
		boolean filterFlag = false;
		HashMap<String, Boolean> filterMap = new HashMap<String, Boolean>();
		if (forObject != null && forObject instanceof String) {
			String filterString = (String) forObject;
			if (filterString.length() > 0) {
				filterFlag = true;
				String filterStrings[] = null;
				filterStrings = filterString.split("[,|;]");
				if (filterStrings != null && filterStrings.length > 0) {
					for (String x : filterStrings) {
						if (x == null || x.length() == 0) continue;
						if (x.startsWith("-")) {
							filterMap.put(x.substring(1), false);
						} else {
							if (x.startsWith("+"))
								filterMap.put(x.substring(1), true);
							else
								filterMap.put(x, true);
						}
					}// for end
				}// if end
			}// if end
		}// if end
		List<EventHandlerInfo> filtered = new ArrayList<EventHandlerInfo>();
		EventHandlerConfig ehc = EventHandlerConfig.getInstance();
		List<EventHandlerInfo> list = ehc.getEventHandlers();
		if (list != null && !list.isEmpty()) {
			Boolean bl = null;
			for (EventHandlerInfo x : list) {
				if (x == null) continue;
				if (filtered.contains(x)) continue;
				if (filterFlag) {
					bl = filterMap.get(x.getEventHandlerType());
					if (bl != null && bl.booleanValue()) {
						filtered.add(x);
					}
				} else {
					filtered.add(x);
				}
			}
		}

		return filtered;
	}

}

