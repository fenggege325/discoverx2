/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import java.util.List;

import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 提供事件处理程序信息对象的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class EventHandlerInfoProvider {
	/**
	 * 缺省构造器。
	 */
	public EventHandlerInfoProvider() {

	}

	/**
	 * 为指定目标对象提供事件处理程序信息对象列表集合的抽象方法。
	 * 
	 * @param forObject Object 目标对象。
	 * @param tag Object 额外对象。
	 * @return List&lt;EventHandlerInfo&gt;，如果没有匹配的事件处理程序信息则可能返回空列表或者null。
	 */
	public abstract List<EventHandlerInfo> provide(Object forObject, Object tag);

	/**
	 * 把sub集合包含的项逐个追加到raw集合中。
	 * 
	 * @param raw List<EventHandlerInfo> 如果为null，则直接返回。
	 * @param sub List<EventHandlerInfo> 如果为null或不包含项，则直接返回。
	 */
	protected void listAddAll(List<EventHandlerInfo> raw, List<EventHandlerInfo> sub) {
		if (raw == null) return;
		if (sub == null || sub.isEmpty()) return;
		String cls = null;
		for (EventHandlerInfo x : sub) {
			if (x == null) continue;
			cls = x.getEventHandler();
			if (cls == null || cls.length() == 0) continue;
			if (!raw.contains(x)) raw.add(x);
		}
	}

	/**
	 * 根据传入的系统支持的事件分类返回相应的提供对象实例。
	 * 
	 * @param eventCategory EventCategory
	 * @return EventHandlerInfoProvider 如果没有匹配的提供对象实现，则可能返回null。
	 */
	public static EventHandlerInfoProvider getProvider(EventCategory eventCategory) {
		EventHandlerInfoProvider provider = new EventHandlerInfoProvider() {
			@Override
			public List<EventHandlerInfo> provide(Object forObject, Object tag) {
				return null;
			}
		};
		switch (eventCategory.getIntValue()) {
		case 1:
			provider = new DocumentEventHandlerInfoProvider();
			break;
		case 2:
			provider = new ResourceEventHandlerInfoProvider();
			break;
		case 3:
			provider = new OperationEventHandlerInfoProvider();
			break;
		case 4:
			provider = new UserEventHandlerInfoProvider();
			break;
		case 5:
			String impl = "com.tansuosoft.discoverx.workflow.event.WorkflowEventHandlerInfoProvider";
			provider = Instance.newInstance(impl, EventHandlerInfoProvider.class);
			break;
		case 6:
			provider = new LoginEventHandlerInfoProvider();
			break;
		default:
			provider = new EventHandlerInfoProvider() {
				@Override
				public List<EventHandlerInfo> provide(Object forObject, Object tag) {
					return null;
				}
			};
			break;
		}
		return provider;
	}
}

