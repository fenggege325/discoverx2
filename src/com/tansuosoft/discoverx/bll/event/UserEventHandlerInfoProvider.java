/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import java.util.List;

import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;

/**
 * 提供用户活动事件处理程序信息对象的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class UserEventHandlerInfoProvider extends EventHandlerInfoProvider {
	/**
	 * 缺省构造器。
	 */
	public UserEventHandlerInfoProvider() {
	}

	/**
	 * 返回用户活动相关的所有可用事件处理程序信息对象列表集合。
	 * 
	 * <p>
	 * forObject传入null。 <br/>
	 * tag传入null。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider#provide(java.lang.Object, java.lang.Object)
	 */
	@Override
	public List<EventHandlerInfo> provide(Object forObject, Object tag) {
		EventHandlerInfoProvider provider = new GlobalEventHandlerInfoProvider();
		List<EventHandlerInfo> result = provider.provide(String.format("%1$s;%2$s", EventHandler.EHT_USERLOGIN, EventHandler.EHT_USERLOGOUT), null);
		return result;
	}
}

