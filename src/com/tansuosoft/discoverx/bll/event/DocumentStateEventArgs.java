/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Session;

/**
 * 表示文档状态变化相关事件的事件参数类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DocumentStateEventArgs extends EventArgs {
	private Session m_session = null; // 当前用户自定义会话。
	private Document m_document = null; // 当前处理的文档。
	private int m_state = 0; // 要设置的新状态。
	private int m_oldState = 0;

	/*
	 * 缺省构造器。
	 */
	public DocumentStateEventArgs() {
	}

	/**
	 * 接收必要参数的缺省构造器。
	 * 
	 * @param document Document
	 * @param state int
	 * @param session Session
	 */
	public DocumentStateEventArgs(Document document, int state, Session session) {
		this.m_document = document;
		this.m_state = state;
		this.m_session = session;
	}

	/**
	 * 返回当前用户自定义会话。
	 * 
	 * @return Session 当前用户自定义会话。
	 */
	public Session getSession() {
		return this.m_session;
	}

	/**
	 * 设置当前用户自定义会话。
	 * 
	 * @param session Session 当前用户自定义会话。
	 */
	public void setSession(Session session) {
		this.m_session = session;
	}

	/**
	 * 返回当前处理的文档。
	 * 
	 * @return Document 当前处理的文档。
	 */
	public Document getDocument() {
		return this.m_document;
	}

	/**
	 * 设置当前处理的文档。
	 * 
	 * @param document Document 当前处理的文档。
	 */
	public void setDocument(Document document) {
		this.m_document = document;
	}

	/**
	 * 返回新状态值。
	 * 
	 * @return int
	 */
	public int getState() {
		return this.m_state;
	}

	/**
	 * 设置新状态值。
	 * 
	 * @param state int
	 */
	public void setState(int state) {
		this.m_state = state;
	}

	/**
	 * 返回老的状态值。
	 * 
	 * @return int
	 */
	public int getOldState() {
		return this.m_oldState;
	}

	/**
	 * 设置老的状态值。
	 * 
	 * @param state int
	 */
	public void setOldState(int state) {
		this.m_oldState = state;
	}
}

