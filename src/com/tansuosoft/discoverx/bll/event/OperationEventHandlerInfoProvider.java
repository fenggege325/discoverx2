/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.function.Operation;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.model.Function;

/**
 * 提供功能操作事件处理程序信息对象的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class OperationEventHandlerInfoProvider extends EventHandlerInfoProvider {
	/**
	 * 缺省构造器。
	 */
	public OperationEventHandlerInfoProvider() {
	}

	/**
	 * 返回forObject指向的功能扩展操作({@link Operation})配置的所有可用事件处理程序信息对象列表集合。
	 * 
	 * @see com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider#provide(java.lang.Object, java.lang.Object)
	 */
	@Override
	public List<EventHandlerInfo> provide(Object forObject, Object tag) {
		if (forObject == null || !(forObject instanceof Operation)) return null;
		EventHandlerInfoProvider globalProvider = new GlobalEventHandlerInfoProvider();
		List<EventHandlerInfo> result = globalProvider.provide(String.format("%1$s;%2$s", EventHandler.EHT_FUNCTIONEXECUTIONBEGIN, EventHandler.EHT_FUNCTIONEXECUTIONAFTER), null);
		if (result == null) result = new ArrayList<EventHandlerInfo>();
		Operation operation = (Operation) forObject;
		Function f = operation.getFunction();
		if (f == null) return result;
		this.listAddAll(result, f.getEventHandlers());
		String before = operation.getParameterStringFromAll(EventHandler.EHT_FUNCTIONEXECUTIONBEGIN, null);
		if (before != null && before.length() > 0) {
			EventHandlerInfo ehi = new EventHandlerInfo(before, EventHandler.EHT_FUNCTIONEXECUTIONBEGIN, "运行时通过web端指定", result.size());
			result.add(ehi);
		}
		String after = operation.getParameterStringFromAll(EventHandler.EHT_FUNCTIONEXECUTIONAFTER, null);
		if (after != null && after.length() > 0) {
			EventHandlerInfo ehi = new EventHandlerInfo(after, EventHandler.EHT_FUNCTIONEXECUTIONAFTER, "运行时通过web端指定", result.size());
			result.add(ehi);
		}
		return result;
	}

}

