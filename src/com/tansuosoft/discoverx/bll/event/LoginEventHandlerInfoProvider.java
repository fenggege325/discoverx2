/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.event.impl.DefaultLoginEventHandler;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;

/**
 * 提供用户登录事件处理程序信息对象的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class LoginEventHandlerInfoProvider extends EventHandlerInfoProvider {
	/**
	 * 缺省构造器。
	 */
	public LoginEventHandlerInfoProvider() {
	}

	/**
	 * 返回用户登录事件相关的所有可用事件处理程序信息对象列表集合。
	 * 
	 * <p>
	 * forObject传入null。 <br/>
	 * tag传入null。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider#provide(java.lang.Object, java.lang.Object)
	 */
	@Override
	public List<EventHandlerInfo> provide(Object forObject, Object tag) {
		EventHandlerInfoProvider provider = new GlobalEventHandlerInfoProvider();
		List<EventHandlerInfo> result = provider.provide(String.format("%1$s", EventHandler.EHT_ONLOGIN), null);
		if (result == null) result = new ArrayList<EventHandlerInfo>(1);
		result.add(0, new EventHandlerInfo(DefaultLoginEventHandler.class.getName(), EventHandler.EHT_ONLOGIN, "系统默认登录完成事件", 0));
		result.add(1, new EventHandlerInfo("com.tansuosoft.discoverx.web.app.tsim.UserLoginEventHandler", EventHandler.EHT_ONLOGIN, "广播小助手通知", 1));
		return result;
	}
}

