/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tansuosoft.discoverx.bll.security.Credential;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.model.Session;

/**
 * 表示用户登录事件的事件参数类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class LoginEventArgs extends EventArgs {
	private Session m_session = null; // 执行登录、注销等用户活动的用户自定义会话信息。

	/**
	 * 缺省构造器。
	 */
	public LoginEventArgs() {
	}

	/**
	 * 接收必要参数的构造器。
	 * 
	 * @param request
	 * @param response
	 * @param c
	 * @param s
	 */
	public LoginEventArgs(HttpServletRequest request, HttpServletResponse response, Credential c, Session s) {
		this.m_httpServletRequest = request;
		this.m_httpServletResponse = response;
		this.m_credential = c;
		this.m_session = s;
	}

	private HttpServletRequest m_httpServletRequest = null; // 用户登录对应的http请求对象。
	private HttpServletResponse m_httpServletResponse = null; // 用户登录对应的http响应对象。
	private Credential m_credential = null; // 用户登录的登录凭据

	/**
	 * 返回用户登录对应的http请求对象。
	 * 
	 * @return HttpServletRequest
	 */
	public HttpServletRequest getHttpServletRequest() {
		return this.m_httpServletRequest;
	}

	/**
	 * 设置用户登录对应的http请求对象。
	 * 
	 * @param httpServletRequest HttpServletRequest
	 */
	public void setHttpServletRequest(HttpServletRequest httpServletRequest) {
		this.m_httpServletRequest = httpServletRequest;
	}

	/**
	 * 返回用户登录对应的http响应对象。
	 * 
	 * @return HttpServletResponse
	 */
	public HttpServletResponse getHttpServletResponse() {
		return this.m_httpServletResponse;
	}

	/**
	 * 设置用户登录对应的http响应对象。
	 * 
	 * @param httpServletResponse HttpServletResponse
	 */
	public void setHttpServletResponse(HttpServletResponse httpServletResponse) {
		this.m_httpServletResponse = httpServletResponse;
	}

	/**
	 * 返回用户登录的登录凭据
	 * 
	 * @return Credential
	 */
	public Credential getCredential() {
		return this.m_credential;
	}

	/**
	 * 设置用户登录的登录凭据
	 * 
	 * @param credential Credential
	 */
	public void setCredential(Credential credential) {
		this.m_credential = credential;
	}

	/**
	 * 返回执行登录、注销等用户活动的用户自定义会话信息。
	 * 
	 * <p>
	 * 登录前事件此属性可能返回null，登录前事件中应通过getTag/setTag获取/设置登录的Credential对象。
	 * </p>
	 * 
	 * @return Participant
	 */
	public Session getSession() {
		return this.m_session;
	}

	/**
	 * 设置执行登录、注销等用户活动的用户自定义会话信息。
	 * 
	 * @param session Session
	 */
	public void setSession(Session session) {
		this.m_session = session;
	}

}

