/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.event;

import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.model.Session;

/**
 * 表示用户活动相关（登录、注销等）事件的事件参数类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class UserActivityEventArgs extends EventArgs {
	private Session m_session = null; // 执行登录、注销等用户活动的用户自定义会话信息。

	/**
	 * 缺省构造器。
	 */
	public UserActivityEventArgs() {
	}

	/**
	 * 接收必要参数的构造器。
	 * 
	 * @param s Session
	 */
	public UserActivityEventArgs(Session s) {
		this.m_session = s;
	}

	/**
	 * 返回执行登录、注销等用户活动的用户自定义会话信息。
	 * 
	 * <p>
	 * 登录前事件此属性可能返回null。
	 * </p>
	 * 
	 * @return Participant
	 */
	public Session getSession() {
		return this.m_session;
	}

	/**
	 * 设置执行登录、注销等用户活动的用户自定义会话信息。
	 * 
	 * @param session Session
	 */
	public void setSession(Session session) {
		this.m_session = session;
	}

}

