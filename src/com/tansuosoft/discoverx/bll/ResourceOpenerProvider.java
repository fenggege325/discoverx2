/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import com.tansuosoft.discoverx.bll.document.DocumentOpener;
import com.tansuosoft.discoverx.bll.resource.DefaultResourceOpener;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 根据资源类型提供资源打开对象实例的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceOpenerProvider {

	/**
	 * 根据资源类型提供资源打开对象实例。
	 * 
	 * @param cls
	 * @return
	 */
	public static ResourceOpener getResourceOpener(Class<?> cls) {
		if (cls == null) return null;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(cls);
		if (rd == null) throw new RuntimeException("找不到对应资源描述信息！");
		String cfgClassName = rd.getOpener();
		if (cfgClassName != null && cfgClassName.length() > 0) {
			ResourceOpener obj = Instance.newInstance(cfgClassName, ResourceOpener.class);
			if (obj != null) return obj;
		}
		String dir = rd.getDirectory();
		String docdir = ResourceDescriptorConfig.DOCUMENT_DIRECTORY;
		if (dir == null || docdir == null) return null;
		boolean isDocument = (dir == null ? docdir.equalsIgnoreCase(dir) : (dir.equalsIgnoreCase(docdir)));
		if (isDocument) {
			return new DocumentOpener();
		} else {
			return new DefaultResourceOpener();
		}
	}
}

