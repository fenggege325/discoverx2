/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.model.ViewColumn;

/**
 * 表示解析后的视图数据列信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ParsedColumn extends ParsedViewQueryConfig {
	private ViewColumn m_column = null;

	/**
	 * 接收视图数据列对象的构造器。
	 * 
	 * @param column
	 */
	public ParsedColumn(ViewColumn column) {
		super(column);
		this.m_column = column;
	}

	/**
	 * 返回原始的视图数据列对象。
	 * 
	 * @return ViewColumn
	 */
	public ViewColumn getColumn() {
		return this.m_column;
	}

	private boolean m_UNIDColumnFlag = false; // 是否资源UNID列，默认为false。

	/**
	 * 返回是否资源UNID列，默认为false。
	 * 
	 * @return boolean
	 */
	public boolean getUNIDColumnFlag() {
		return this.m_UNIDColumnFlag;
	}

	/**
	 * 设置是否资源UNID列，默认为false。
	 * 
	 * @param UNIDColumnFlag boolean
	 */
	public void setUNIDColumnFlag(boolean UNIDColumnFlag) {
		this.m_UNIDColumnFlag = UNIDColumnFlag;
	}

	/**
	 * 重载getSQLElementResult：返回包含的{@link ViewColumn}对象对应的sql片段结果，如 “t_document.c_creator as c_creator”。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ParsedViewQueryConfig#getSQLElementResult(com.tansuosoft.discoverx.bll.view.ViewQuery)
	 */
	@Override
	public String getSQLElementResult(ViewQuery viewQuery) {
		if (this.m_column == null) return null;
		String expression = this.m_column.getExpression();
		String tableName = this.m_column.getTableName();
		String columnName = this.m_column.getColumnName();
		String columnAlias = this.getColumnAlias();

		String tblDotCol = this.formatTableDotColumnSQL(tableName, columnName);
		if (expression == null || expression.length() == 0) {
			return String.format("%1$s %2$s", tblDotCol, columnAlias);
		} else {
			String exp = expression.replace("{0}", tblDotCol);
			return String.format("%1$s %2$s", exp, columnAlias);
		}
	}

	/**
	 * 返回实际使用的有效的列别名。
	 * 
	 * <p>
	 * 参考{@link ViewColumn#getColumnAlias()}。
	 * </p>
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.model.ViewColumn#getColumnAlias()
	 */
	public String getColumnAlias() {
		String alias = m_column.getColumnAlias();
		if (alias == null || alias.length() == 0) alias = m_column.getColumnName();
		return alias;
	}
}

