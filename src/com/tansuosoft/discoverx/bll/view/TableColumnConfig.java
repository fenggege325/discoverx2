/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 视图配置时可用的表和表字段信息配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class TableColumnConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private TableColumnConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static TableColumnConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return TableColumnConfig
	 */
	public static TableColumnConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new TableColumnConfig();
			}
		}
		return m_instance;
	}

	private List<ConfigurableTable> m_configurableTables = null; // 视图配置时可用的所有表和表字段信息集合。

	/**
	 * 返回视图配置时可用的所有表和表字段信息集合。
	 * 
	 * @return List<ConfigurableTable> 视图配置时可用的表和表字段信息集合。
	 */
	public List<ConfigurableTable> getConfigurableTables() {
		return this.m_configurableTables;
	}

	/**
	 * 设置视图配置时可用的所有表和表字段信息集合。
	 * 
	 * @param configurableTables List<ConfigurableTable> 视图配置时可用的表和表字段信息集合。
	 */
	public void setConfigurableTables(List<ConfigurableTable> configurableTables) {
		this.m_configurableTables = configurableTables;
	}

	/**
	 * 为指定视图类型的视图返回可用的所有表和表字段信息集合。
	 * 
	 * @param viewType ViewType
	 * @return List<ConfigurableTable> 如果找不到，则可能返回null。
	 */
	public List<ConfigurableTable> getConfigurableTables(ViewType viewType) {
		if (this.m_configurableTables == null || this.m_configurableTables.isEmpty()) return null;
		List<ConfigurableTable> ret = new ArrayList<ConfigurableTable>();
		for (ConfigurableTable t : this.m_configurableTables) {
			if (t == null || t.getForTargetViewType() != viewType) continue;
			ret.add(t);
		}
		return ret;
	}

	/**
	 * 为指定视图返回可用的所有表和表字段信息集合。
	 * 
	 * @param view View
	 * @return List<ConfigurableTable> 如果找不到，则可能返回null。
	 */
	public List<ConfigurableTable> getConfigurableTables(View view) {
		if (view == null) return null;
		if (this.m_configurableTables == null || this.m_configurableTables.isEmpty()) return null;
		List<ConfigurableTable> ret = new ArrayList<ConfigurableTable>();
		String forView = null;
		for (ConfigurableTable t : this.m_configurableTables) {
			if (t == null || t.getForTargetViewType() != view.getViewType()) continue;
			forView = t.getForTargetViewUNID();
			if (forView != null && forView.length() > 0 && !forView.equalsIgnoreCase(view.getUNID())) continue;
			ret.add(t);
		}
		return ret;
	}
}

