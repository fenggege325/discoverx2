/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.model.ViewOrder;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 视图查询语句解析基类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ViewParser {
	/**
	 * 是否多租户
	 */
	protected static final boolean IsMultipleOrgs = OrganizationsContext.getInstance().isMultipleOrgs();

	/**
	 * 缺省构造器。
	 */
	public ViewParser() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param view
	 * @param session缺省构造器。
	 */
	public ViewParser(View view, Session session) {
		this.m_view = view;
		this.m_session = session;
	}

	private View m_view = null; // 被解析的视图。
	private Session m_session = null; // 当前用户自定义会话。
	List<ViewCondition> m_appendedViewConditions = null;
	List<ParsedCondition> m_parsedConditions = null;
	List<ParsedColumn> m_parsedColumns = null;
	List<ParsedOrder> m_parsedOrders = null;

	/**
	 * 返回被解析的视图，必须。
	 * 
	 * @return View
	 */
	public View getView() {
		return this.m_view;
	}

	/**
	 * 设置被解析的视图，必须。
	 * 
	 * @param view
	 */
	public void setView(View view) {
		this.m_view = view;
		m_parsedConditions = null;
		m_parsedColumns = null;
		m_parsedOrders = null;
	}

	/**
	 * 返回当前用户自定义会话，必须。
	 * 
	 * @return
	 */
	public Session getSession() {
		return this.m_session;
	}

	/**
	 * 设置当前用户自定义会话，必须。
	 * 
	 * @param session
	 */
	protected void setSession(Session session) {
		this.m_session = session;
	}

	/**
	 * 解析并返回解析后的视图条件集合。
	 * 
	 * @return List&lt;ParsedCondition&gt;
	 */
	public List<ParsedCondition> parseConditions() {
		if (m_parsedConditions != null) return m_parsedConditions;
		View view = this.getView();
		if (view == null) throw new RuntimeException("未知视图！");
		List<ViewCondition> conditions = view.getConditions();
		if (conditions == null || conditions.isEmpty()) {
			conditions = new ArrayList<ViewCondition>();
		} else {
			List<ViewCondition> newlist = new ArrayList<ViewCondition>();
			try {
				for (ViewCondition vc : conditions) {
					newlist.add((ViewCondition) vc.clone());
				}
			} catch (Exception ex) {
			}
			conditions = newlist;
		}
		if (m_appendedViewConditions != null && m_appendedViewConditions.size() > 0) {
			conditions.addAll(m_appendedViewConditions);
		}
		if (conditions == null || conditions.isEmpty()) {
			m_parsedConditions = new ArrayList<ParsedCondition>();
			return m_parsedConditions;
		}
		ParsedCondition parsed = null;
		m_parsedConditions = new ArrayList<ParsedCondition>(conditions.size());
		try {
			for (ViewCondition x : conditions) {
				if (x == null || (StringUtil.isBlank(x.getTableName()) && StringUtil.isBlank(x.getColumnName()))) continue;
				parsed = new ParsedCondition((ViewCondition) x.clone());
				m_parsedConditions.add(parsed);
			}
		} catch (CloneNotSupportedException e) {
			FileLogger.error(e);
		}
		return m_parsedConditions;
	}

	/**
	 * 追加一个选择条件（{@link ViewCondition}）。
	 * 
	 * @param viewCondition
	 */
	public void appendCondition(ViewCondition viewCondition) {
		if (viewCondition == null) return;
		View view = this.getView();
		if (view == null) throw new RuntimeException("未知视图！");
		if (m_appendedViewConditions == null) m_appendedViewConditions = new ArrayList<ViewCondition>();
		m_appendedViewConditions.add(viewCondition);
	}

	/**
	 * 返回是否有额外搜索条件。
	 * 
	 * <p>
	 * 即除了本身配置的条件外还额外动态追加了一个或多个搜索条件时返回true。
	 * </p>
	 * <p>
	 * 供系统内部使用。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean hasExtraCondition() {
		return (m_appendedViewConditions != null && m_appendedViewConditions.size() > 0);
	}

	/**
	 * 返回所有额外搜索条件组合出来唯一数字编码。
	 * 
	 * <p>
	 * 供系统内部使用。
	 * </p>
	 * 
	 * @return long
	 */
	public long getExtraConditionsUniqueCode() {
		if (!hasExtraCondition()) return 0;
		long result = 0;
		for (ViewCondition vc : m_appendedViewConditions) {
			if (vc == null) continue;
			result += vc.getStringResultHashCode();
		}
		return result;
	}

	/**
	 * 解析并返回视图数据列信息集合。
	 * 
	 * @return List&lt;ParsedColumn&gt;
	 */
	public List<ParsedColumn> parseColumns() {
		if (m_parsedColumns != null) return m_parsedColumns;
		View view = this.getView();
		if (view == null) throw new RuntimeException("未知视图！");
		List<ViewColumn> columns = view.getColumns();
		if (columns == null || columns.isEmpty()) {
			m_parsedColumns = new ArrayList<ParsedColumn>();
			return m_parsedColumns;
		}
		ParsedColumn parsed = null;
		m_parsedColumns = new ArrayList<ParsedColumn>(columns.size());
		try {
			for (ViewColumn x : columns) {
				if (x == null || !x.isValid()) continue;
				parsed = new ParsedColumn((ViewColumn) x.clone());
				m_parsedColumns.add(parsed);
			}
		} catch (CloneNotSupportedException e) {
			FileLogger.error(e);
		}
		return m_parsedColumns;
	}

	/**
	 * 解析并返回视图排序信息集合。
	 * 
	 * @return List&lt;ParsedOrder&gt;
	 */
	public List<ParsedOrder> parseOrders() {
		if (m_parsedOrders != null) return m_parsedOrders;
		View view = this.getView();
		if (view == null) throw new RuntimeException("未知视图！");
		List<ViewOrder> orders = view.getOrders();
		if (orders == null || orders.isEmpty()) {
			m_parsedOrders = new ArrayList<ParsedOrder>();
			return m_parsedOrders;
		}
		ParsedOrder parsed = null;
		m_parsedOrders = new ArrayList<ParsedOrder>(orders.size());
		try {
			for (ViewOrder x : orders) {
				if (x == null || (StringUtil.isBlank(x.getTableName()) && StringUtil.isBlank(x.getColumnName()))) continue;
				parsed = new ParsedOrder((ViewOrder) x.clone());
				m_parsedOrders.add(parsed);
			}
		} catch (CloneNotSupportedException e) {
			FileLogger.error(e);
		}
		return m_parsedOrders;
	}

	/**
	 * 解析并返回解析后的视图条目数据来源表集合。
	 * 
	 * <p>
	 * 对于数据条目来源于数据库表的视图，返回的字符串集合中应包含所有相关表名。
	 * </p>
	 * <p>
	 * 对于数据条目来源于XML文件的的资源视图，返回的字符串集合中应包含一个资源目录名称。
	 * </p>
	 * 
	 * @return List&lt;String&gt;
	 */
	public List<String> parseTables() {
		View view = this.getView();
		if (view == null) throw new RuntimeException("未知视图！");
		String tbl = view.getTable();
		if (tbl != null && tbl.length() > 0) {
			List<String> result = new ArrayList<String>();
			int currentIndex = 0;
			int nextSeparator;
			String tmp = tbl.replace(';', ',');
			while ((nextSeparator = tmp.indexOf(',', currentIndex)) != -1) {
				result.add(tmp.substring(currentIndex, nextSeparator));
				currentIndex = nextSeparator + 1;
			}
			result.add(tmp.substring(currentIndex));
			return result;
		}
		return new ArrayList<String>();
	}

	/**
	 * 返回视图中配置的名为“ignore_multiple_orgs”的额外参数的布尔值结果。
	 * 
	 * <p>
	 * 如果参数值的结果为true，那么忽略自动追加的多租户额外查询条件及其校验过程。否则多租户使用的默认情况下会自动追加多租户额外查询条件及其校验过程。
	 * </p>
	 * 
	 * @return boolean
	 */
	protected boolean ignoreMultipleOrgs() {
		View view = this.getView();
		return view.getParamValueBool("ignore_multiple_orgs", false);
	}
}

