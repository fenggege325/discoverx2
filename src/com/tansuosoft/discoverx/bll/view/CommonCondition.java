/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 描述视图通用查询条件相关信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class CommonCondition {
	/**
	 * 缺省构造器。
	 */
	public CommonCondition() {
	}

	private String m_name = null; // 通用查询条件名称，必须且唯一。
	private String m_provider = null; // 条件列表提供类全限定类名。
	private String m_inheritance = null; // 继承自另一个固定条件的名称。
	private List<ViewCondition> m_conditions = null; // 通用查询条件包含的查询条件集合。
	private String m_description = null; // 通用查询条件说明。

	/**
	 * 返回通用查询条件名称，必须且唯一。
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置通用查询条件名称，必须且唯一。
	 * 
	 * @param name String
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回条件列表提供类全限定类名。
	 * 
	 * <p>
	 * 如果提供了有效的类信息（实现{@link ConditionsProvider}接口的实现类的全限定类名）则优先使用以获取查询条件列表；否则通过{@link CommonCondition.getConditions}获取查询条件列表信息。
	 * </p>
	 * 
	 * @return String
	 */
	public String getProvider() {
		return this.m_provider;
	}

	/**
	 * 设置条件列表提供类全限定类名。
	 * 
	 * @param provider String
	 */
	public void setProvider(String provider) {
		this.m_provider = provider;
	}

	/**
	 * 返回继承自另一个固定条件的名称。
	 * 
	 * @return String
	 */
	public String getInheritance() {
		return this.m_inheritance;
	}

	/**
	 * 设置继承自另一个固定条件的名称。
	 * 
	 * @param inheritance String
	 */
	public void setInheritance(String inheritance) {
		this.m_inheritance = inheritance;
	}

	/**
	 * 返回通用查询条件包含的查询条件集合。
	 * 
	 * @see CommonCondition#getProvider()
	 * @return List<ViewCondition>
	 */
	public List<ViewCondition> getConditions() {
		return this.m_conditions;
	}

	/**
	 * 设置通用查询条件包含的查询条件集合。
	 * 
	 * @param conditions List<ViewCondition>
	 */
	public void setConditions(List<ViewCondition> conditions) {
		this.m_conditions = conditions;
	}

	/**
	 * 返回通用查询条件说明。
	 * 
	 * @return String
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置通用查询条件说明。
	 * 
	 * @param description String
	 */
	public void setDescription(String description) {
		this.m_description = description;
	}

	/**
	 * 从继承的通用查询条件配置中获取具体查询条件并返回。
	 * 
	 * @param commonCondition
	 * @param view
	 * @param session
	 * @return
	 */
	@SuppressWarnings("null")
	public static List<ViewCondition> getConditionsFromInheritance(CommonCondition commonCondition, View view, Session session) {
		if (commonCondition == null) return null;
		String inheritance = commonCondition.getInheritance();
		List<ViewCondition> result = null;
		if (inheritance != null && inheritance.length() > 0) {
			CommonCondition c = CommonConditionConfig.getInstance().getCommonCondition(inheritance);
			if (c != null) {
				String provider = c.getProvider();
				if (provider != null && provider.length() > 0) {
					Object obj = Instance.newInstance(provider);
					if (obj == null || obj instanceof ConditionsProvider) result = ((ConditionsProvider) obj).provide(view, session);
				} else {
					result = c.getConditions();
				}
				List<ViewCondition> sub = getConditionsFromInheritance(c, view, session);
				if (sub != null) {
					if (result != null) {
						result = new ArrayList<ViewCondition>(sub);
					} else {
						result.addAll(sub);
					}
				}
			}
		}

		return result;
	}
}

