/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.List;

import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.bll.function.ExpressionParser;
import com.tansuosoft.discoverx.bll.function.ParsedVariable;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.ViewQueryConfig;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 表示某一条视图查询配置项解析结果的基类。
 * 
 * <p>
 * 每一个实例对应一个{@link com.tansuosoft.discoverx.model.ViewQueryConfig}实例。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ParsedViewQueryConfig {
	private boolean m_dynamic = false; // 此查询配置项是否动态添加的。
	private ViewQueryConfig m_viewQueryConfig = null; // 包含的原始视图查询查询配置项。

	/**
	 * 缺省构造器。
	 */
	public ParsedViewQueryConfig() {
	}

	/**
	 * 接收原始视图查询查询配置项的构造器。
	 * 
	 * @param viewQueryConfig
	 */
	public ParsedViewQueryConfig(ViewQueryConfig viewQueryConfig) {
		this.m_viewQueryConfig = viewQueryConfig;
	}

	/**
	 * 返回此查询配置项是否动态添加的。
	 * 
	 * <p>
	 * 从视图原始配置中得来的配置项解析结果为false，如果是运行时动态添加的配置项，则应设置为true。<br/>
	 * 默认为false。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getDynamic() {
		return this.m_dynamic;
	}

	/**
	 * 设置此查询配置项是否动态添加的。
	 * 
	 * @see ParsedViewQueryConfig#getDynamic()
	 * @param dynamic boolean
	 */
	public void setDynamic(boolean dynamic) {
		this.m_dynamic = dynamic;
	}

	/**
	 * 返回包含的原始视图查询查询配置项。
	 * 
	 * @return ViewQueryConfig
	 */
	public ViewQueryConfig getViewQueryConfig() {
		return this.m_viewQueryConfig;
	}

	/**
	 * 构造并返回当前查询配置项对应SQL语句中各字句下包含的某一条内容。
	 * 
	 * @param viewQuery 表示要为其返回SQL语句片段的{@link ViewQuery}
	 * @return String
	 */
	public abstract String getSQLElementResult(ViewQuery viewQuery);

	/**
	 * 传入表名和字段名并格式化为符合sql语句规范的有效的表名.字段名的组合。
	 * 
	 * @param tableName String
	 * @param columnName String
	 * @return String 如果表名不存在则返回“字段名”，否则返回“表名.字段名”。
	 */
	protected String formatTableDotColumnSQL(String tableName, String columnName) {
		if (tableName == null || tableName.trim().length() == 0) return StringUtil.getValueString(columnName, "");
		return String.format("%1$s.%2$s", tableName, columnName);
	}

	/**
	 * 将SQL查询片断（即{@link ParsedViewQueryConfig#getSQLElementResult()}的返回值）中的公式表达式提取出来，并计算其结果，然后用表达式结果替换原来公式表达式所在位置的内容并返回替换后的SQL片断。
	 * 
	 * @param sql
	 * @param viewQuery
	 * @param defaultIfExpNoReturn 如果表达式结果返回null，那么使用此值作为表达式结果。
	 * @return String
	 */
	protected static String processExpression(String sql, ViewQuery viewQuery, String defaultIfExpNoReturn) {
		if (sql == null) { return ""; }
		if (sql.trim().length() == 0) return sql;

		ExpressionParser expressionParser = new ExpressionParser(sql);
		List<ParsedVariable> list = expressionParser.parseVariables();
		if (list == null || list.isEmpty()) return sql;

		String result = new String(sql);
		Session session = viewQuery.getViewParser().getSession();

		// if (session != null) session.pushResource(view);
		String variable = null;
		ExpressionEvaluator evaluator = null;
		String evalResult = null;
		for (ParsedVariable x : list) {
			if (x == null || !x.getVariableRange().getIndependent()) continue;
			variable = x.getVariable();
			evaluator = new ExpressionEvaluator(variable, session, null);
			evalResult = evaluator.evalString();
			if (evalResult == null) evalResult = defaultIfExpNoReturn;
			result = result.replace(variable, evalResult);
		}
		// if (session != null) session.popResource();

		return result;
	}

	/**
	 * 返回两个对象toString后的字符串比较结果。
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return this.toString().equalsIgnoreCase(obj == null ? null : obj.toString());
	}

	/**
	 * 返回toString的返回结果的hashCode。
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * 返回getSQLElementResult的返回结果。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName() + ":" + this.m_viewQueryConfig.toString();
	}
}

