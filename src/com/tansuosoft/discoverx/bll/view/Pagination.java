/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.model.View;

/**
 * 表示视图条目分页信息的类。
 * 
 * <p>
 * <strong>对于一次可返回的条目数量很多的视图查询，如果不提供分页信息，则可能导致性能严重下降和大量系统资源损耗！</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Pagination {
	private int m_totalCount = 0; // 总条目数。
	private int m_pageCount = 0; // 总页数。
	private int m_currentPage = 1; // 当前显示第几页。
	private int m_displayCountPerPage = View.DEFAULT_DISPLAY_COUNT_PER_PAGE; // 每页显示多少条。

	/**
	 * 缺省构造器。
	 */
	public Pagination() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param currentPage int
	 * @param displayCountPerPage int
	 */
	public Pagination(int currentPage, int displayCountPerPage) {
		this.m_currentPage = currentPage;
		this.m_displayCountPerPage = displayCountPerPage;
	}

	/**
	 * 返回总条目数。
	 * 
	 * @return int
	 */
	public int getTotalCount() {
		return this.m_totalCount;
	}

	/**
	 * 设置总条目数。
	 * 
	 * @param totalCount int
	 */
	public void setTotalCount(int totalCount) {
		this.m_totalCount = totalCount;
	}

	/**
	 * 计算并返回总页数。
	 * 
	 * @return int
	 */
	public int getPageCount() {
		if (this.m_pageCount > 0) return this.m_pageCount;
		if (m_displayCountPerPage == 0) {
			m_pageCount = 1;
			return m_pageCount;
		}
		this.m_pageCount = this.m_totalCount / this.m_displayCountPerPage;
		int mod = this.m_totalCount % this.m_displayCountPerPage;
		if (mod > 0) this.m_pageCount++;
		return this.m_pageCount;
	}

	/**
	 * 返回当前显示第几页，默认为1（第一页）。
	 * 
	 * @return int
	 */
	public int getCurrentPage() {
		if (this.m_currentPage > this.m_pageCount && this.m_pageCount > 0) this.m_currentPage = this.m_pageCount;
		return this.m_currentPage;
	}

	/**
	 * 设置当前显示第几页。
	 * 
	 * @param currentPage int
	 */
	public void setCurrentPage(int currentPage) {
		this.m_currentPage = currentPage;
	}

	/**
	 * 返回每页显示多少条，默认为{@link View#DEFAULT_DISPLAY_COUNT_PER_PAGE}。
	 * 
	 * <p>
	 * 返回或设置为0表示不分页。
	 * </p>
	 * 
	 * @return int
	 */
	public int getDisplayCountPerPage() {
		return this.m_displayCountPerPage;
	}

	/**
	 * 设置每页显示多少条。
	 * 
	 * @param displayCountPerPage int
	 */
	public void setDisplayCountPerPage(int displayCountPerPage) {
		this.m_displayCountPerPage = displayCountPerPage;
	}

}

