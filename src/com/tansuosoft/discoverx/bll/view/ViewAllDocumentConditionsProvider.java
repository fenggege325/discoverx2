/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.security.AuthorizationConfig;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.SecurityRange;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewCondition;

/**
 * 查询所有可访问文档条件的查询条件提供实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewAllDocumentConditionsProvider implements ConditionsProvider {
	private static ViewCondition ConstConditionTrueAlways = new ViewCondition();
	private static List<ViewCondition> ConstResultTrueAlways = new ArrayList<ViewCondition>(1);
	private static ViewCondition ConstConditionCheckPublishedToUser = new ViewCondition();
	private static ViewCondition ConstConditionCheckPublishedToAll = new ViewCondition();
	static {
		ConstConditionTrueAlways.setLeftBracket("(");
		ConstConditionTrueAlways.setTableName("");
		ConstConditionTrueAlways.setColumnName("1");
		ConstConditionTrueAlways.setCompare("=");
		ConstConditionTrueAlways.setRightValue("1");
		ConstConditionTrueAlways.setNumericCompare(true);
		ConstConditionTrueAlways.setRightBracket(")");
		ConstResultTrueAlways.add(ConstConditionTrueAlways);

		ConstConditionCheckPublishedToUser.setLeftBracket("(");
		ConstConditionCheckPublishedToUser.setTableName("t_document");
		ConstConditionCheckPublishedToUser.setColumnName("c_securitycodes");
		ConstConditionCheckPublishedToUser.setCompare("=");
		ConstConditionCheckPublishedToUser.setRightValue(Security.PUBLISH_TO_USER_FLAG);
		ConstConditionCheckPublishedToUser.setNumericCompare(false);
		ConstConditionCheckPublishedToUser.setRightBracket("");
		// ConstConditionCheckPublishedToUser.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_OR);

		ConstConditionCheckPublishedToAll.setLeftBracket("");
		ConstConditionCheckPublishedToAll.setTableName("t_document");
		ConstConditionCheckPublishedToAll.setColumnName("c_securitycodes");
		ConstConditionCheckPublishedToAll.setCompare("=");
		ConstConditionCheckPublishedToAll.setRightValue(Security.PUBLISH_TO_ALL_FLAG);
		ConstConditionCheckPublishedToAll.setNumericCompare(false);
		ConstConditionCheckPublishedToAll.setRightBracket("");
	};
	private static final Application APP = new Application();

	/**
	 * 缺省构造器。
	 */
	public ViewAllDocumentConditionsProvider() {
	}

	/**
	 * 重载provide：返回当前用户所有可访问文档的查询条件集合。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ConditionsProvider#provide(com.tansuosoft.discoverx.model.View, com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public List<ViewCondition> provide(View view, Session session) {
		if (view == null) return null;

		List<ViewCondition> result = null;
		Application app = ViewHelper.getViewDirectParentApplication(view);
		User user = (session == null ? User.getAnonymous() : session.getUser());
		Security security = (app == null ? null : app.getDocumentDefaultSecurity());
		boolean viewable = false;
		viewable = SecurityHelper.authorize(user, security, SecurityLevel.View, SecurityRange.Document);
		if (!viewable) {
			AuthorizationConfig ac = AuthorizationConfig.getInstance();
			List<Integer> securityCodes = SecurityHelper.getUserSecurityCodes(user);
			if (app == null) app = APP;
			for (int sc : securityCodes) {
				if (ac.checkSecurityLevel(app, sc, SecurityLevel.View.getIntValue())) {
					viewable = true;
					break;
				}
			}
		}
		if (viewable) { // 如果定义了可以直接查看则返回一个总是为真的where表达式。
			return ConstResultTrueAlways;
		} else {
			if (user.isAnonymous()) { // 如果是匿名用户，只有公布或公开的文档才能查看。
				result = new ArrayList<ViewCondition>(1);
				result.add(ConstConditionCheckPublishedToAll);
			} else { // 否则通过t_security是否有与文档关联的用户安全编码记录进行检查。
				ViewCondition vc = new ViewCondition();
				vc.setLeftBracket("");
				vc.setTableName("t_security");
				vc.setColumnName("c_code");
				vc.setCompare("=");
				vc.setRightValue(user.getSecurityCode() + "");
				vc.setNumericCompare(true);
				vc.setRightBracket("");
				vc.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_AND);
				result = new ArrayList<ViewCondition>(3);
				result.add(vc);
			}
		}
		return result;
	}
}

