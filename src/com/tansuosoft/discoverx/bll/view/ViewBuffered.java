/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.List;

/**
 * 表示一个已缓存的视图查询结果的类。
 * 
 * <p>
 * 由系统内部使用。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
class ViewBuffered {
	private String m_viewAlias = null; // 视图别名。
	private int m_entryCount = -1; // 视图数据总条目数。
	private List<ViewCategoryEntry> m_categories = null; // 视图分类数据。
	private List<ViewEntry> m_viewEntries = null; // 视图数据（查询结果）。
	private int m_currentCachePages = Integer.MIN_VALUE; // 当前缓存对应的视图资源中配置的缓存页数。
	private int m_currentCachePage = -1; // 当前缓存对应的实际缓存数据的页数。

	/**
	 * 缺省构造器。
	 */
	public ViewBuffered() {
	}

	/**
	 * 返回视图别名。
	 * 
	 * @return String
	 */
	public String getViewAlias() {
		return this.m_viewAlias;
	}

	/**
	 * 设置视图别名。
	 * 
	 * @param viewAlias String
	 */
	public void setViewAlias(String viewAlias) {
		this.m_viewAlias = viewAlias;
	}

	/**
	 * 返回视图数据总条目数。
	 * 
	 * @return int
	 */
	public int getEntryCount() {
		return this.m_entryCount;
	}

	/**
	 * 设置视图数据总条目数。
	 * 
	 * @param entryCount int
	 */
	public void setEntryCount(int entryCount) {
		this.m_entryCount = entryCount;
	}

	/**
	 * 返回视图分类数据。
	 * 
	 * @return List&lt;ViewCategoryEntry&gt;
	 */
	public List<ViewCategoryEntry> getCategories() {
		return this.m_categories;
	}

	/**
	 * 设置视图分类数据。
	 * 
	 * @param categories List&lt;ViewCategoryEntry&gt;
	 */
	public void setCategories(List<ViewCategoryEntry> categories) {
		if (this.m_categories != null && !this.m_categories.isEmpty()) this.m_categories.clear();
		this.m_categories = categories;
	}

	/**
	 * 返回视图查询结果数据。
	 * 
	 * @return List&lt;ViewEntry&gt;
	 */
	public List<ViewEntry> getViewEntries() {
		return this.m_viewEntries;
	}

	/**
	 * 设置视图查询结果数据。
	 * 
	 * @param viewEntries List&lt;ViewEntry&gt;
	 */
	public void setViewEntries(List<ViewEntry> viewEntries) {
		if (this.m_viewEntries != null && !this.m_viewEntries.isEmpty()) this.m_viewEntries.clear();
		this.m_viewEntries = viewEntries;
	}

	/**
	 * 返回当前缓存对应的视图资源中配置的缓存页数。
	 * 
	 * @return int
	 */
	protected int getCurrentCachePages() {
		return this.m_currentCachePages;
	}

	/**
	 * 设置当前缓存对应的视图资源中配置的缓存页数。
	 * 
	 * @param currentCachePages int
	 */
	protected void setCurrentCachePages(int currentCachePages) {
		this.m_currentCachePages = currentCachePages;
	}

	/**
	 * 返回当前缓存对应的实际缓存数据的页数。
	 * 
	 * @return int
	 */
	protected int getCurrentCachePage() {
		return this.m_currentCachePage;
	}

	/**
	 * 设置当前缓存对应的实际缓存数据的页数。
	 * 
	 * @param currentCachePage int
	 */
	protected void setCurrentCachePage(int currentCachePage) {
		this.m_currentCachePage = currentCachePage;
	}
}

