/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.View;

/**
 * 用于解析保存于XML文件中的资源信息的视图查询语句解析类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class XmlViewParser extends ViewParser {
	/**
	 * 缺省构造器。
	 */
	protected XmlViewParser() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param view
	 * @param session缺省构造器。
	 */
	protected XmlViewParser(View view, Session session) {
		super(view, session);
	}

}

