/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.model.ViewOrder;

/**
 * 表示解析后的视图排序信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ParsedOrder extends ParsedViewQueryConfig {
	private ViewOrder m_order = null;

	/**
	 * 接收排序对象的构造器。
	 * 
	 * @param order
	 */
	public ParsedOrder(ViewOrder order) {
		super(order);
		this.m_order = order;
	}

	/**
	 * 获取原始排序对象。
	 * 
	 * @return ViewOrder
	 */
	public ViewOrder getOrder() {
		return this.m_order;
	}

	/**
	 * 
	 * 重载getSQLElementResult: 返回包含的{@link ViewOrder}对象对应的sql片段结果，如 “t_document.c_sort DESC”。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ParsedViewQueryConfig#getSQLElementResult(com.tansuosoft.discoverx.bll.view.ViewQuery)
	 */
	@Override
	public String getSQLElementResult(ViewQuery viewQuery) {
		if (this.m_order == null) return null;
		String expression = this.m_order.getExpression();
		String tableName = this.m_order.getTableName();
		String columnName = this.m_order.getColumnName();
		String orderBy = this.m_order.getOrderBy();
		if (expression == null || expression.length() == 0) {
			return String.format("%1$s.%2$s %3$s", tableName, columnName, orderBy);
		} else {
			String exp = expression.replaceAll("\\{0\\}", String.format("%1$s.%2$s", tableName, columnName));
			return String.format("%1$s %2$s", exp, orderBy);
		}
	}

}

