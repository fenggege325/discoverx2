/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.List;

/**
 * 描述视图分类数据条目信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ViewCategoryEntry {
	private int index = 0; // 分类索引。
	private List<String> m_values = null; // 此视图列包含的唯一分类值。

	/**
	 * 缺省构造器。
	 */
	public ViewCategoryEntry() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param index
	 * @param vals
	 */
	public ViewCategoryEntry(int index, List<String> vals) {
		this.index = index;
		this.m_values = vals;
	}

	/**
	 * 返回分类索引。
	 * 
	 * <p>
	 * 分类索引是指分类信息所在视图按顺序配置的分类列的位置，0表示第一个视图分类列，依此类推。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.model.ViewColumn
	 * @see com.tansuosoft.discoverx.model.ViewColumn#getCategorized()
	 * @see com.tansuosoft.discoverx.model.ViewColumn#setCategorized(boolean)
	 * 
	 * @return int
	 */
	public int getIndex() {
		return this.index;
	}

	/**
	 * 设置分类索引。
	 * 
	 * @see ViewCategoryEntry#getIndex()
	 * @param index int
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * 返回视图对应索引的分类列包含的唯一分类值（结果）。
	 * 
	 * @return List<String>
	 */
	public List<String> getValues() {
		return this.m_values;
	}

	/**
	 * 设置视图对应索引的分类列包含的唯一分类值（结果）。
	 * 
	 * @param values List<String>
	 */
	public void setValues(List<String> values) {
		this.m_values = values;
	}

	/**
	 * 返回结果中包含几个具体值。
	 * 
	 * @return int
	 */
	public int getValuesCount() {
		if (this.m_values == null || this.m_values.isEmpty()) return 0;
		return this.m_values.size();
	}
}

