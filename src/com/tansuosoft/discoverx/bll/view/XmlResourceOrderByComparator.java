/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.lang.reflect.Method;
import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.Comparator;
import java.util.List;

import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceComparatorDefault;
import com.tansuosoft.discoverx.util.ObjectUtil;

/**
 * 用于XML资源视图查询类中排序输出使用的资源比较器。
 * 
 * <p>
 * 它依据传入的List&lt;ParsedOrder&gt;信息进行排序，如果不提供则按排序号和名称升序排序。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class XmlResourceOrderByComparator implements Comparator<Resource> {
	// 排序控制类列表
	private List<ParsedOrder> orderList = null;
	private static final RuleBasedCollator collator = (RuleBasedCollator) Collator.getInstance(java.util.Locale.CHINA);

	/**
	 * 缺省构造器。
	 */
	public XmlResourceOrderByComparator() {

	}

	/**
	 * 获取{@link ParsedOrder}列表。
	 * 
	 * @return List&lt;ParsedOrder&gt;
	 */
	protected List<ParsedOrder> getOrders() {
		return orderList;
	}

	/**
	 * 设置{@link ParsedOrder}列表。
	 * 
	 * @param orderList List&lt;ParsedOrder&gt;
	 */
	protected void setOrders(List<ParsedOrder> orderList) {
		this.orderList = orderList;
	}

	/**
	 * 重载compare：按指定属性值排序，如果没有排序条件则默认按资源的排序号、名称、UNID升序排序(确保顺序每次都不变)。
	 * 
	 * <p>
	 * 如果属性值为空的，不管是升序还是降序都排到最后面。
	 * </p>
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Resource o1, Resource o2) {
		int result = 0;
		if (orderList == null || orderList.isEmpty()) {
			ResourceComparatorDefault c = new ResourceComparatorDefault();
			return c.compare(o1, o2);
		} else {
			String propName = null;
			Method method = null;
			Object value1 = null, value2 = null;
			String valueC1 = null, valueC2 = null;

			for (ParsedOrder order : orderList) {
				if (order != null) {

					propName = order.getOrder().getColumnName();
					propName = propName.substring(0, 1).toUpperCase().concat(propName.substring(1));

					method = ObjectUtil.getMethod(o1, "get" + propName);

					value1 = ObjectUtil.getMethodResult(o1, method);

					value2 = ObjectUtil.getMethodResult(o2, method);
					if (value1 == value2) {
						result = 0;
					} else if (value1 == null || value2 == null) {
						// 如果是空值这总是排在后面
						result = (value1 == null ? 1 : -1);
					} else {
						if (value1 instanceof String) {
							String c1 = value1.toString();
							String c2 = value2.toString();
							if ("DESC".equalsIgnoreCase(order.getOrder().getOrderBy().trim())) {
								result = collator.compare(c2, c1);
							} else {
								result = collator.compare(c1, c2);
							}
						} else {
							// 根据条件进行升降序排列
							valueC1 = String.valueOf(value1);
							valueC2 = String.valueOf(value2);

							if ("DESC".equalsIgnoreCase(order.getOrder().getOrderBy().trim())) {
								result = valueC2.compareTo(valueC1);
							} else {
								result = valueC1.compareTo(valueC2);
							}
						}
					}
					if (result != 0) {
						break;
					}
				}
			}
		}
		// 如果两条相等，则按照unid排类，以保证排序条件得出的结果的情况下，顺序一样
		if (result == 0) {
			result = o1.getUNID().compareTo(o2.getUNID());
		}
		return result;
	}

}

