/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewOrder;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 用于解析保存于数据库表中的资源信息的视图查询语句解析类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DBResourceViewParser extends ViewParser {
	private List<ParsedOrder> orders = null; // 最终解析的排序信息。
	private List<ParsedColumn> columns = null; // 最终解析的列信息

	/**
	 * 缺省构造器。
	 */
	protected DBResourceViewParser() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param view
	 * @param session缺省构造器。
	 */
	protected DBResourceViewParser(View view, Session session) {
		super(view, session);
	}

	/**
	 * 重载parseColumns
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewParser#parseColumns()
	 */
	public List<ParsedColumn> parseColumns() {
		if (this.columns == null) {
			List<String> tables = this.parseTables();
			if (tables == null || tables.isEmpty()) throw new RuntimeException("没有配置资源表信息！");
			if (tables.size() > 1) throw new RuntimeException("不能配置多个资源表信息！");
			String table = tables.get(0);

			this.columns = super.parseColumns();
			boolean foundUNIDColumn = false;
			if (this.columns != null) {
				for (ParsedColumn x : this.columns) { // 检查是否有可获取文档unid的列。
					ViewColumn c = x.getColumn();
					String tableName = c.getTableName();
					String columnName = c.getColumnName();
					if (tableName == null || tableName.length() == 0 || columnName == null || columnName.length() == 0) continue;
					if (table.equalsIgnoreCase(tableName) && "c_unid".equalsIgnoreCase(columnName)) {
						x.setUNIDColumnFlag(true);
						foundUNIDColumn = true;
						break;
					}
				}
				// 如果没有文档unid列且为非手工输入查询条件，那么添加一个UNID列。
				if (!foundUNIDColumn && StringUtil.isBlank(this.getView().getRawQuery())) {
					ViewColumn unidColumn = new ViewColumn();
					unidColumn.setTableName(table);
					unidColumn.setColumnName("c_unid");
					unidColumn.setColumnAlias(unidColumn.getColumnName());
					unidColumn.setLinkable(false);
					ParsedColumn parsedUnidColumn = new ParsedColumn(unidColumn);
					parsedUnidColumn.setUNIDColumnFlag(true);
					parsedUnidColumn.setDynamic(true);
					this.columns.add(0, parsedUnidColumn);
				}// if end
			}// if end
		}
		return this.columns;
	}

	/**
	 * 重载parseOrders
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewParser#parseOrders()
	 */
	public List<ParsedOrder> parseOrders() {
		if (this.orders == null) {
			this.orders = super.parseOrders();
			if (this.orders == null || this.orders.isEmpty()) {
				this.orders = new ArrayList<ParsedOrder>(1);
				ViewOrder o = new ViewOrder();
				o.setTableName(this.getView().getTable());
				o.setColumnName("c_sort");
				o.setOrderBy("DESC");
				ParsedOrder po = new ParsedOrder(o);
				po.setDynamic(true);
				this.orders.add(po);
			}
		}
		return this.orders;
	}

}

