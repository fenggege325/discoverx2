/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.model.ViewGroup;

/**
 * 表示解析后的视图分组信息的类。
 * 
 * <p>
 * 此对象由系统保留使用。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ParsedGroup extends ParsedViewQueryConfig {
	private ViewGroup m_group = null;

	/**
	 * 接收排序对象的构造器。
	 * 
	 * @param group
	 */
	public ParsedGroup(ViewGroup group) {
		super(group);
		this.m_group = group;
	}

	/**
	 * 获取原始分组对象。
	 * 
	 * @return ViewGroup
	 */
	public ViewGroup getGroup() {
		return this.m_group;
	}

	/**
	 * 重载getSQLElementResult:返回包含的{@link ViewGroup}对象对应的sql片段结果，如 “group by t_document.c_creator”。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ParsedViewQueryConfig#getSQLElementResult(com.tansuosoft.discoverx.bll.view.ViewQuery)
	 */
	@Override
	public String getSQLElementResult(ViewQuery viewQuery) {
		if (this.m_group == null) return null;
		String expression = this.m_group.getExpression();
		String tableName = this.m_group.getTableName();
		String columnName = this.m_group.getColumnName();
		if (expression == null || expression.length() == 0) {
			return String.format("%1$s.%2$s", tableName, columnName);
		} else {
			String exp = expression.replaceAll("\\{0\\}", String.format("%1$s.%2$s", tableName, columnName));
			return exp;
		}
	}

}

