/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.model.View;

/**
 * 用于过滤并提供可供视图配置时使用的表字段信息的接口。
 * 
 * @author coca@tansuosoft.cn
 */
public interface ConfigurableTableFilter {
	/**
	 * 为指定视图（View）过滤并返回一个包含了有效的{@link ConfigurableColumn}对象集合的新{@link ConfigurableTable}对象实例。
	 * 
	 * @param view View 必须。
	 * @param table ConfigurableTable 必须。
	 * @return ConfigurableTable 返回新的能提供有效表字段的新的{@link ConfigurableTable}对象。
	 */
	public ConfigurableTable filter(View view, ConfigurableTable table);
}

