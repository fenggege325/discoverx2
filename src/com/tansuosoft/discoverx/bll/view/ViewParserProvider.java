/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 视图解析对象提供类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ViewParserProvider {
	/**
	 * 私有构造器。
	 */
	private ViewParserProvider() {
	}

	/**
	 * 根据指定的视图和自定义会话信息获取合适的视图解析对象实例。
	 * 
	 * @param view View
	 * @param session Session
	 * @return ViewParser
	 */
	public static ViewParser getViewParser(View view, Session session) {
		if (view == null) throw new RuntimeException("未提供有效视图资源对象");
		ViewParser result = null;
		String impl = view.getViewParserImplement();
		if (impl != null && impl.length() > 0) {
			Object obj = Instance.newInstance(impl);
			if (obj == null || !(obj instanceof ViewParser)) throw new RuntimeException("无法实例化“" + impl + "”对应的视图解析实现类！");
			result = (ViewParser) obj;
			return result;
		}
		if (view.getViewType() == ViewType.XMLResourceView) {
			result = new XmlViewParser();
		} else if (view.getViewType() == ViewType.DBResourceView) {
			result = new DBResourceViewParser();
		} else if (view.getViewType() == ViewType.DocumentView) {
			result = new DocumentViewParser();
		} else {
			throw new RuntimeException("无法获取视图“" + view.getUNID() + "”对应的视图解析实现类！");
		}
		if (result != null) {
			result.setView(view);
			result.setSession(session);
		}

		return result;
	}
}

