/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.bll.security.SecurityHelper;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.Source;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 获取保存于XML文件中的资源列表的视图查询实现类。
 * 
 * <p>
 * 保存于xml中的资源的视图条件只支持“and”和“or”组合（不支持括号），其比较条件只能有等于、大于、小于、大于等于、小于等于、包含等。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * @implementBy coca@tansuosoft.cn
 */
public class XmlViewQuery extends ViewQuery {
	private int m_entryCount = 0; // 条目总数
	private List<ViewEntry> m_viewEntries = null; // 没有执行的时候为空，在getViewEntries中会进行填充
	private List<Resource> m_allViewEntries = null; // 满足条件的所有资源对象列表。
	private Map<String, Method> m_pmmaps = null; // 属性名和值对照表

	/**
	 * 缺省构造器。
	 */
	protected XmlViewQuery() {
		super();
		m_pmmaps = new HashMap<String, Method>();
	}

	/**
	 * 取指定对象指定属性名对应的属性值的字符串结果。
	 * 
	 * @param object
	 * @param propName
	 * @param defaultValue
	 * @return String 如果找到则返回属性值toString后的结果，否则返回defaultValue。
	 */
	private String getPropertyValueStr(Object object, String propName, String defaultValue) {
		String pascalCasePropName = StringUtil.toPascalCase(propName);
		String str = null;
		Method method = m_pmmaps.get(pascalCasePropName);
		if (method == null) {
			method = ObjectUtil.getMethod(object, "get" + pascalCasePropName);
			m_pmmaps.put(propName, method);
		}
		Object value = ObjectUtil.getMethodResult(object, method);
		str = (value == null ? defaultValue : value.toString());
		return str;
	}

	/**
	 * 根据视图列配置和指定资源返回填充好的ViewEntry。
	 * 
	 * @param resource
	 * @return
	 */
	private ViewEntry buildViewEntry(Resource resource) {
		ViewParser parser = this.getViewParser();
		ViewEntry entry = new ViewEntry();
		entry.setUnid(resource.getUNID());

		List<ParsedColumn> viewColumns = parser.parseColumns();
		// 如果没有定义列，则只返回unid
		if (viewColumns != null) {
			List<String> values = new ArrayList<String>();
			entry.setValues(values);
			for (ParsedColumn pColumn : viewColumns) {
				values.add(getPropertyValueStr(resource, pColumn.getColumn().getColumnName(), ""));
			}
		}
		return entry;
	}

	/**
	 * 对象指定属性值转为字符串方式后是否满足与目标值的比较。
	 * 
	 * @param propName 属性名
	 * @param targetValue 目标比较值
	 * @param comparator 比较操作符
	 * @param object 资源实例
	 * @return boolean true表示满足条件，false表示不满足条件
	 */
	private boolean compares(String propName, String targetValue, String comparator, Object object) {
		boolean result = false;

		String strValue = getPropertyValueStr(object, propName, null);
		if (comparator.trim().equalsIgnoreCase("=") && StringUtil.isBlank(strValue) && StringUtil.isBlank(targetValue)) return true;
		if (strValue == null) return result;
		String comp = comparator.trim().toLowerCase();
		if (comp.equals("=")) {
			result = strValue.equalsIgnoreCase(targetValue);
		} else if (comp.equals("<")) {// 小于
			result = strValue.compareTo(targetValue) < 0;
		} else if (comp.equals(">")) {// 大于
			result = strValue.compareTo(targetValue) > 0;
		} else if (comp.equals(">=")) {// 大于等于
			result = strValue.compareTo(targetValue) >= 0;
		} else if (comp.equals("<=")) {// 小于等于
			result = strValue.compareTo(targetValue) <= 0;
		} else if (comp.equals("like")) {// 包含
			result = strValue.toLowerCase().indexOf(targetValue.toLowerCase().replace("%", "")) >= 0;
		}

		return result;
	}

	/**
	 * 计算表达式结果。
	 * 
	 * @param expression
	 * @return
	 */
	private boolean evalExpression(String expression) {
		ExpressionEvaluator evaluator = new ExpressionEvaluator(expression, null, null);
		return evaluator.evalBoolean();
	}

	/**
	 * 初始化符合条件的所有条目列表。
	 */
	private void fetchMatchedViewEntries() {
		if (m_allViewEntries != null) return;

		ViewParser parser = this.getViewParser();
		if (parser == null) throw new RuntimeException("无法获取视图解析信息！");

		String directory = parser.parseTables().get(0);// 资源目录。
		List<Resource> resourceList = XmlResourceLister.getResources(directory);
		if (resourceList == null || resourceList.isEmpty()) {
			m_allViewEntries = new ArrayList<Resource>(0);
			m_entryCount = 0;
			return;
		}

		if (m_allViewEntries == null) this.m_allViewEntries = new ArrayList<Resource>();

		ViewCondition condition = null;
		List<ParsedCondition> conditionList = parser.parseConditions();

		boolean hasCondition = (conditionList != null && !conditionList.isEmpty());
		List<String> expressionList = new ArrayList<String>();
		int idx = 0;
		if (hasCondition) {
			String combineNextWith = null;
			String logicOperator = null;
			for (ParsedCondition pc : conditionList) {
				condition = pc.getCondition();
				combineNextWith = condition.getCombineNextWith();
				if (combineNextWith == null) combineNextWith = "and";
				logicOperator = ("or".equalsIgnoreCase(combineNextWith.trim()) ? " || " : " && ");
				expressionList.add(condition.getLeftBracket() + " false " + condition.getRightBracket() + (idx < (conditionList.size() - 1) ? logicOperator : ""));
				idx++;
			}
		}

		boolean compareResult = false;
		StringBuilder sb = null;
		Session s = this.getViewParser().getSession();
		for (Resource resource : resourceList) {
			if (resource == null || resource.getSource() == Source.System) continue;
			if (!SecurityHelper.authorize(s, resource, SecurityLevel.View)) continue;
			if (!hasCondition) {
				m_allViewEntries.add(resource);
				continue;
			}
			// 找出满足条件的资源
			idx = 0;
			sb = new StringBuilder();
			for (ParsedCondition parsedCondition : conditionList) {
				condition = parsedCondition.getCondition();
				compareResult = compares(condition.getColumnName(), condition.getRightValue(), condition.getCompare(), resource);
				sb.append(expressionList.get(idx).replace("false", compareResult ? "true" : "false"));
				idx++;
			}// for end
			if (evalExpression(sb.toString().trim())) m_allViewEntries.add(resource);
		} // for end

		m_entryCount = (m_allViewEntries == null ? 0 : m_allViewEntries.size());
		Pagination p = this.getPagination();
		if (p != null) p.setTotalCount(m_entryCount);
	}

	/**
	 * 重载getEntryCount
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getEntryCount()
	 */
	@Override
	public int getEntryCount() {
		fetchMatchedViewEntries();
		return m_entryCount;
	}

	/**
	 * 重载getViewEntries：获取符合条件的按要求排序后的资源对应的{@link ViewEntry}列表集合。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getViewEntries()
	 */
	@Override
	public List<ViewEntry> getViewEntries() {
		if (m_viewEntries != null) { return m_viewEntries; }

		this.getEntryCount();
		if (m_entryCount <= 0) {
			m_entryCount = 0;
			m_viewEntries = new ArrayList<ViewEntry>(m_entryCount);
			return m_viewEntries;
		}

		ViewParser parser = this.getViewParser();
		if (parser == null) throw new RuntimeException("无法获取视图解析信息！");

		// 排序资源
		XmlResourceOrderByComparator comparator = new XmlResourceOrderByComparator();
		comparator.setOrders(parser.parseOrders());
		Collections.sort(m_allViewEntries, comparator);

		// 生成结果，取出指定列的值
		ViewEntry entry = null;

		// 设置结果
		Pagination pagination = this.getPagination();
		int countPerPage = (pagination == null ? 0 : pagination.getDisplayCountPerPage());
		if (pagination != null && countPerPage <= 0) pagination.setDisplayCountPerPage(this.getViewParser().getView().getDisplayCountPerPage());
		if (pagination == null || countPerPage <= 0) { // 不分页返回所有
			m_viewEntries = new ArrayList<ViewEntry>(m_entryCount);
			for (Resource resource : m_allViewEntries) {
				entry = buildViewEntry(resource);
				this.callViewEntryCallbacks(entry);
				m_viewEntries.add(entry);
			}
			return m_viewEntries;
		} else { // 否则返回分页数。
			m_viewEntries = new ArrayList<ViewEntry>(countPerPage);
			pagination.setTotalCount(m_entryCount);
			int offset = ((pagination.getCurrentPage() - 1) * countPerPage);

			int count = 0;
			for (;;) {
				if (offset >= m_entryCount) break;
				entry = buildViewEntry(m_allViewEntries.get(offset++));
				this.callViewEntryCallbacks(entry);
				m_viewEntries.add(entry);
				count++;
				if (count >= countPerPage) break;
			}
		}

		return m_viewEntries;
	}

	/**
	 * 重载getCategories:总是返回null。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getCategories()
	 */
	@Override
	public List<ViewCategoryEntry> getCategories() {
		return null;
	}

	/**
	 * 重载getQueryForCategories:总是返回null
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getQueryForCategories()
	 */
	@Override
	public String getQueryForCategories() {
		return null;
	}

	/**
	 * 重载getQueryForEntryCount: 总是返回null。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getQueryForEntryCount()
	 */
	@Override
	public String getQueryForEntryCount() {
		return null;
	}

	/**
	 * 重载getQueryForViewEntries: 总是返回null。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getQueryForViewEntries()
	 */
	@Override
	public String getQueryForViewEntries() {
		return null;
	}
}

