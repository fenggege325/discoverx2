/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewCondition;

/**
 * 视图查询基础类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ViewQuery {

	protected ViewParser m_viewParser = null; // 绑定的视图解析对象，必须。
	private Pagination m_pagination = null; // 分页信息，可选。
	private List<ViewEntryCallback> m_viewEntryCallbacks = null;
	private boolean m_conditionChanged = false;
	private String m_innerRawQuery = null; // 视图直接输入的原始查询语句。

	/**
	 * 缺省构造器。
	 */
	public ViewQuery() {

	}

	/**
	 * 返回绑定的视图解析对象，必须。
	 * 
	 * @return ViewParser 绑定的视图解析对象，必须。
	 */
	public ViewParser getViewParser() {
		return this.m_viewParser;
	}

	/**
	 * 设置绑定的视图解析对象，必须。
	 * 
	 * @param viewParser ViewParser 绑定的视图解析对象，必须。
	 */
	public void setViewParser(ViewParser viewParser) {
		this.m_viewParser = viewParser;
	}

	/**
	 * 返回绑定的视图。
	 * 
	 * @return
	 */
	public View getView() {
		ViewParser vp = this.getViewParser();
		if (vp != null) {
			View v = vp.getView();
			return v;
		}
		return null;
	}

	/**
	 * 是否有有效分页信息。
	 * 
	 * @return 如果{@link #getPagination()}不为null且{@link Pagination#getCurrentPage()}和{@link Pagination#getDisplayCountPerPage()}均大于0则返回true，否则返回false。
	 */
	protected boolean hasPagination() {
		return (this.m_pagination != null && this.m_pagination.getCurrentPage() > 0 && this.m_pagination.getDisplayCountPerPage() > 0);
	}

	/**
	 * 返回是否启用了视图查询结果数据的缓存功能。
	 * 
	 * @return
	 */
	protected boolean getCache() {
		ViewParser vp = this.getViewParser();
		if (vp != null && vp.hasExtraCondition()) { return false; }
		View v = this.getView();
		boolean result = (v == null ? false : v.getCache());
		return result;
	}

	/**
	 * 返回是否启用了视图所有查询结果数据的缓存功能。
	 * 
	 * @return
	 */
	protected boolean getCacheAll() {
		View v = this.getView();
		if (v == null) return false;
		boolean result = (v.getCache() && v.getCachePages() <= 0);
		return result;
	}

	/**
	 * 返回是否启用了视图部分页面查询结果数据的缓存功能。
	 * 
	 * @return
	 */
	protected boolean getCachePages() {
		View v = this.getView();
		if (v == null) return false;
		boolean result = (v.getCache() && v.getCachePages() > 0);
		return result;
	}

	/**
	 * 获取缓存的{@link ViewQuery}查询结果的键值。
	 * 
	 * @return 不管{@link ViewQuery}绑定的视图有没有配置为打开数据缓存（即不论{@link View#getCache()}返回true或false）都有结果返回。
	 */
	protected String getPermanentCacheKey() {
		View v = this.getView();
		boolean cacheByUser = (v.getCache() ? v.getCacheByUser() : true);
		User u = null;
		ViewParser vp = this.getViewParser();
		if (cacheByUser) {
			Session s = (vp == null ? null : vp.getSession());
			u = Session.getUser(s);
			if (u == null || u.isAnonymous()) cacheByUser = false;
		}
		String extraConditionSuffix = (m_conditionChanged ? "" + vp.getExtraConditionsUniqueCode() : "");
		if (cacheByUser) {
			return String.format("%1$s@%2$s%3$s", v.getAlias(), u.getSecurityCode(), extraConditionSuffix);
		} else {
			return String.format("%1$s%2$s", v.getAlias(), extraConditionSuffix);
		}
	}

	private String m_cacheKey = null;

	/**
	 * 获取缓存的{@ViewQuery}查询结果的键值。
	 * 
	 * @return 如果{@link ViewQuery}绑定的视图没有配置为打开数据缓存（即{@link View#getCache()}返回false）或有额外搜索条件时则返回空字符串。
	 */
	protected String getCacheKey() {
		if (m_cacheKey != null) return m_cacheKey;
		if (!this.getCache() || m_conditionChanged) {
			m_cacheKey = "";
			return m_cacheKey;
		}
		View v = this.getView();
		boolean cacheByUser = v.getCacheByUser();
		User u = null;
		if (cacheByUser) {
			ViewParser vp = this.getViewParser();
			Session s = (vp == null ? null : vp.getSession());
			u = Session.getUser(s);
			if (u == null || u.isAnonymous()) cacheByUser = false;
		}
		if (cacheByUser) {
			m_cacheKey = String.format("%1$s@%2$s", v.getAlias(), u.getSecurityCode());
		} else {
			m_cacheKey = v.getAlias();
		}
		return m_cacheKey;
	}

	/**
	 * 清除所有缓存的视图数据。
	 * 
	 * <p>
	 * 此方法将从缓存中清除所有与当前{@link ViewQuery}对象相关的数据！
	 * </p>
	 */
	public void removeCache() {
		ViewBuffer vb = ViewBuffer.getInstance();
		String k = this.getPermanentCacheKey();
		vb.clearCache(k);
		k = this.getCacheKey();
		vb.clearCache(k);
	}

	/**
	 * 返回视图直接输入的原始查询语句。
	 * 
	 * @return String
	 */
	public String getRawQuery() {
		if (m_innerRawQuery == null) m_innerRawQuery = getViewParser().getView().getRawQuery();
		return m_innerRawQuery;
	}

	/**
	 * 设置视图直接输入的原始查询语句。
	 * 
	 * <p>
	 * <strong>注：此方法将使用传入的查询语句替换视图配置中直接输入的原始查询语句！</strong>
	 * </p>
	 * 
	 * @param rawQuery String
	 */
	public void setRawQuery(String rawQuery) {
		this.m_innerRawQuery = rawQuery;
	}

	/**
	 * 调用{@link ViewParser#appendCondition(ViewCondition)}同名方法以追加一个搜索条件（{@link ViewCondition}）。
	 * 
	 * <p>
	 * 手工输入的查询条件忽略通过此方法追加的搜索条件。
	 * </p>
	 * <p>
	 * <strong>需在调用此类的其它获取任何结果的方法之前调用此方法才起作用！</strong>
	 * </p>
	 * 
	 * @param viewCondition
	 */
	public void appendCondition(ViewCondition viewCondition) {
		if (viewCondition == null) return;
		this.m_viewParser.appendCondition(viewCondition);
		m_conditionChanged = true;
	}

	/**
	 * 返回分页信息，可选。
	 * 
	 * <p>
	 * 不提供分页信息则不分页。
	 * </p>
	 * 
	 * @return Pagination 分页信息，可选。
	 */
	public Pagination getPagination() {
		if (this.m_pagination != null) {
			int ec = this.getEntryCount();
			if (ec >= 0) this.m_pagination.setTotalCount(ec);
		}
		return this.m_pagination;
	}

	/**
	 * 设置分页信息，可选。
	 * 
	 * @param pagination Pagination 分页信息，可选。
	 */
	public void setPagination(Pagination pagination) {
		this.m_pagination = pagination;
	}

	/**
	 * 追加一个ViewEntryCallback回调函数到视图查询中。
	 * 
	 * <p>
	 * 必须在getViewEntries之前追加，否则追加的ViewEntryCallback可能不会被调用。
	 * </p>
	 * 
	 * @param callback ViewEntryCallback
	 */
	public void addViewEntryCallback(ViewEntryCallback callback) {
		if (callback == null) return;
		callback.setViewQuery(this);
		if (this.m_viewEntryCallbacks == null) this.m_viewEntryCallbacks = new ArrayList<ViewEntryCallback>();
		this.m_viewEntryCallbacks.add(callback);
	}

	/**
	 * 为指定的ViewEntry实例调用所有注册到此对象的回调函数。
	 * 
	 * @param viewEntry ViewEntry
	 */
	protected void callViewEntryCallbacks(ViewEntry viewEntry) {
		if (this.m_viewEntryCallbacks == null || this.m_viewEntryCallbacks.isEmpty()) return;
		for (ViewEntryCallback x : this.m_viewEntryCallbacks) {
			x.entryFetched(viewEntry, this);
		}
	}

	/**
	 * 返回获取视图数据条目的查询语句。
	 * 
	 * <p>
	 * 一般用于数据条目来源于数据库表的视图。
	 * </p>
	 * 
	 * @return String
	 */
	public abstract String getQueryForViewEntries();

	/**
	 * 返回获取视图所有分类列数据结果的查询语句集合。
	 * 
	 * <p>
	 * 一般用于数据条目来源于数据库表的视图。
	 * </p>
	 * 
	 * @return String
	 */
	public abstract String getQueryForCategories();

	/**
	 * 返回获取视图数据条目总数的查询语句。
	 * 
	 * <p>
	 * 一般用于数据条目来源于数据库表的视图。
	 * </p>
	 * 
	 * @return String
	 */
	public abstract String getQueryForEntryCount();

	/**
	 * 获取视图数据条目总记录数。
	 * 
	 * @return int
	 */
	public abstract int getEntryCount();

	/**
	 * 获取视图数据条目结果集合。
	 * 
	 * <p>
	 * 一条视图数据条目（一般对应一条表记录或一个资源）其id（如果有的话）保存于UNID属性中，条目值（对应的表记录的不同列或者资源属性值）按顺序保存于Values属性中。
	 * </p>
	 * 
	 * @see ViewEntry
	 * @return List&lt;ViewEntry&gt;
	 */
	public abstract List<ViewEntry> getViewEntries();

	/**
	 * 获取视图分类数据结果集合。
	 * 
	 * 一个视图原生分类列（原生分类列指视图中原始配置的{@link com.tansuosoft.discoverx.model.ViewColumn}）的结果对应一个{@link ViewCategoryEntry}对象。<br/>
	 * 如果视图有零个、一个或多个原生分类列，则此方法应返回包含有零个、一个或多个对应的{@link ViewCategoryEntry}的列表集合。
	 * 
	 * @see ViewCategoryEntry
	 * @see com.tansuosoft.discoverx.model.ViewColumn
	 * @see com.tansuosoft.discoverx.model.ViewColumn#getCategorized()
	 * 
	 * @return List&lt;ViewCategoryEntry&gt;
	 */
	public abstract List<ViewCategoryEntry> getCategories();

}

