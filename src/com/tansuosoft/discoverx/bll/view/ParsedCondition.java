/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.dao.SQLValueEncoderConfig;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 表示解析后的视图查询条件信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ParsedCondition extends ParsedViewQueryConfig {
	private ViewCondition m_condition = null;

	/**
	 * 接收查询条件对象的构造器。
	 * 
	 * @param condition
	 */
	public ParsedCondition(ViewCondition condition) {
		super(condition);
		this.m_condition = condition;
	}

	/**
	 * 获取原始查询条件对象。
	 * 
	 * @return ViewCondition
	 */
	public ViewCondition getCondition() {
		return this.m_condition;
	}

	/**
	 * 重载getSQLElementResult: 返回包含的{@link ViewCondition}对象对应的sql片段结果，如 “(t_document.c_created ='2010-09-01 12:30:00')”。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ParsedViewQueryConfig#getSQLElementResult(com.tansuosoft.discoverx.bll.view.ViewQuery)
	 */
	@Override
	public String getSQLElementResult(ViewQuery viewQuery) {
		if (this.m_condition == null) return "";
		if (!validateSqlElement()) return "";
		String expression = this.m_condition.getExpression();
		String tableName = this.m_condition.getTableName();
		String columnName = this.m_condition.getColumnName();
		String compareRaw = this.m_condition.getCompare();
		String leftBraket = this.m_condition.getLeftBracket() == null ? "" : this.m_condition.getLeftBracket();
		String rightBraket = this.m_condition.getRightBracket() == null ? "" : this.m_condition.getRightBracket();
		String rightValue = this.m_condition.getRightValue();
		rightValue = rightValue == null ? "" : rightValue;
		boolean numericCompare = this.m_condition.getNumericCompare();
		String tableDotColumn = this.formatTableDotColumnSQL(tableName, columnName);
		String left = null;
		if (expression == null || expression.length() == 0) {
			left = tableDotColumn;
		} else {
			String exp = expression.replaceAll("\\{0\\}", tableDotColumn);
			if (exp.indexOf('@') >= 0) exp = processExpression(exp, viewQuery, exp);
			left = exp;
		}
		String compare = null;
		int likeMode = 0; // like模式，1为begin like ；2为 end like；3为all like.
		String compareTrim = compareRaw == null ? "" : compareRaw.trim().toLowerCase();
		if (compareTrim.equals("beginlike")) {
			compare = "like";
			likeMode = 1;
		} else if (compareTrim.equals("endlike")) {
			compare = "like";
			likeMode = 2;
		} else if (compareTrim.equals("like")) {
			compare = "like";
			likeMode = 3;
		} else if (compareTrim.equals("not like")) {
			compare = "not like";
			likeMode = 3;
		} else {
			compare = compareRaw;
		}
		String likeModeBegin = "";
		String likeModeEnd = "";
		if (!checkHasPercentMark(rightValue)) {
			switch (likeMode) {
			case 1:
				likeModeEnd = "%";
				break;
			case 2:
				likeModeBegin = "%";
				break;
			case 3:
				likeModeBegin = "%";
				likeModeEnd = "%";
				break;
			default:
				likeModeBegin = "";
				likeModeEnd = "";
				break;
			}
		}
		// 非动态添加的项，其右值要计算并替换为公式表达式结果
		if (!this.getDynamic()) {
			rightValue = processExpression(rightValue, viewQuery, this.m_condition.getNumericCompare() ? "-1" : "");
		}
		// 调用系统注册的所有编码器对值进行编码。
		if (!numericCompare) rightValue = SQLValueEncoderConfig.getInstance().encode(rightValue, compare);
		String ret = String.format("%1$s%2$s %3$s %4$s%5$s%6$s%7$s%8$s%9$s", leftBraket, left, (compare == null ? "" : compare), (numericCompare ? "" : "'"), likeModeBegin, rightValue, likeModeEnd, (numericCompare ? "" : "'"), rightBraket);
		return ret;
	}

	/**
	 * 检查右边比较值是否已经配置了like查询的百分号通配符。
	 * 
	 * @param rightValue
	 * @return boolean
	 */
	protected boolean checkHasPercentMark(String rightValue) {
		if (rightValue == null || rightValue.isEmpty() || rightValue.indexOf('%') < 0) return false;
		return true;
	}

	/**
	 * 检查此条件是否能够输出有效的sql条件比较表达式。
	 * 
	 * <p>
	 * 如果表名、字段名、比较方式、sql表达式、比较右值都没有值，那么返回空串。
	 * </p>
	 * 
	 * @return boolean 如果为true那么
	 */
	public boolean validateSqlElement() {
		if (StringUtil.isBlank(m_condition.getTableName()) && StringUtil.isBlank(m_condition.getColumnName()) && StringUtil.isBlank(m_condition.getCompare()) && StringUtil.isBlank(m_condition.getExpression()) && StringUtil.isBlank(m_condition.getRightValue())) return false;
		return true;
	}
}

