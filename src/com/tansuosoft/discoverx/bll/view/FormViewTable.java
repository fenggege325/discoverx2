/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.resource.XmlResourceLister;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.dao.DAOConfig;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 封装供视图查询使用的表相关操作的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class FormViewTable {
	/**
	 * 视图查询表前缀，区别于普通表的“vt_”前缀。
	 */
	public static final String FORM_VIEW_TABLE_PREFIX = "vt_";

	/**
	 * 获取表字段信息的数据库请求实现类的配置项名称：TableColumnsFetcher
	 */
	public static final String TABLE_COLUMNS_FETCHER_DBRCE_NAME = "TableColumnsFetcher";

	/**
	 * 重建视图查询表的数据库请求实现类的配置项名称：FormViewTableBuilder
	 */
	public static final String FVT_REBUILD_DBRCE_NAME = "FormViewTableBuilder";

	/**
	 * 缺省构造器。
	 */
	private FormViewTable() {
		List<Resource> lister = XmlResourceLister.getResources(Form.class);
		this.m_tables = new HashMap<String, Boolean>(lister == null ? 16 : lister.size() * 2);
	}

	private static FormViewTable m_instance = null;

	/**
	 * 获取此对象唯一实例。
	 * 
	 * @return FormViewTable
	 */
	public synchronized static FormViewTable getInstance() {
		if (m_instance == null) {
			m_instance = new FormViewTable();
		}
		return m_instance;
	}

	private Map<String, Boolean> m_tables = null;

	/**
	 * 检查表单对应的视图查询表是否需要重建。
	 * 
	 * @param form 要检查的表单对象
	 * @return boolean ture表示需要重建，false表示不需要重建。
	 */
	@SuppressWarnings("unchecked")
	public static boolean checkFormViewTable(Form form) {
		try {
			List<Item> items = form.getItems();
			if (items == null || items.isEmpty()) return false;

			// 从数据库中获取查询表字段列信息
			com.tansuosoft.discoverx.dao.DAOConfig dbConfig = DAOConfig.getInstance();
			DBRequest dbRequest = dbConfig.getDBRequest(TABLE_COLUMNS_FETCHER_DBRCE_NAME);
			if (dbRequest == null) throw new Exception("无法初始化获取表列信息的数据库请求,请联系管理员。");

			dbRequest.setParameter("tablename", FORM_VIEW_TABLE_PREFIX + form.getAlias());
			dbRequest.sendRequest();
			Object o = dbRequest.getResult();
			if (o == null || !(o instanceof List)) return true;
			List<String> columns = (List<String>) o;
			if (columns == null || columns.isEmpty()) return true;
			for (String str : columns) {
				if ("c_punid".equalsIgnoreCase(str)) {
					columns.remove(str);
					break;
				}
			}
			for (String str : columns) {
				if ("c_oid".equalsIgnoreCase(str)) {
					columns.remove(str);
					break;
				}
			}

			// 过滤表单中不能作为查询列的Item
			List<String> comparingItems = new ArrayList<String>();
			String itemName = null;
			for (Item item : items) {
				if (item == null || !item.isFormViewTableColumn()) continue;
				itemName = item.getAlias();
				if (itemName == null || itemName.length() == 0) continue;
				comparingItems.add(itemName.toUpperCase());
			}

			// 将视图字段和表单字段对照
			if (columns.size() != comparingItems.size()) return true;
			for (int i = 0; i < comparingItems.size(); i++) {
				itemName = comparingItems.get(i);
				if (!columns.contains(itemName)) {// 如果不包含,返回需要更新标记
					return true;
				}
			}
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		return false;
	}

	/**
	 * 使用字段表中的数据同步更新表单对应的视图查询表数据。
	 * 
	 * @param form Form 要更新的表单对象
	 */
	protected void rebuildFormViewTable(Form form) {
		if (form == null) return;
		if (CommonConfig.getInstance().getDebugable()) FileLogger.debug("为表单“%1$s/%2$s/%3$s”重建视图表。", form.toString(), form.getAlias(), form.getName());
		com.tansuosoft.discoverx.dao.DAOConfig dbConfig = DAOConfig.getInstance();
		DBRequest dbRequest = dbConfig.getDBRequest(FVT_REBUILD_DBRCE_NAME);
		if (dbRequest == null) throw new RuntimeException("无法初始化重建视图查询表的数据库请求,请联系管理员。");
		dbRequest.setParameter(DBRequest.OBJECT_PARAM_NAME, form);
		dbRequest.sendRequest();
	}

	/**
	 * 检查表单对应的视图查询表是否需要重建，如果需要则重建之。
	 * 
	 * <p>
	 * 如果已经检查重建过，则什么也不做直接返回。
	 * </p>
	 * 
	 * @param form Form
	 */
	public void checkAndRebuildFormViewTable(Form form) {
		this.checkAndRebuildFormViewTable(form, false);
	}

	/**
	 * 检查表单对应的视图查询表是否需要重建，如果需要则重建之。
	 * 
	 * @param form Form
	 * @param force boolean 如果为true，则强行检查并重建，否则系统可能因为原先检查重建过而不处理。
	 */
	public void checkAndRebuildFormViewTable(Form form, boolean force) {
		if (form == null) return;
		String alias = form.getAlias();
		if (alias == null || alias.length() == 0) return;
		alias = alias.toLowerCase();
		Boolean hasRebuilt = this.m_tables.get(alias);
		if (hasRebuilt != null && hasRebuilt.booleanValue() && !force) return;
		try {
			if (checkFormViewTable(form)) {
				rebuildFormViewTable(form);
				// 如果表单字段有变动，那么删除缓存的使用此表单的文档以确保新变更的字段信息启用。
				DocumentBuffer db = DocumentBuffer.getInstance();
				List<Document> docs = db.getAllDocuments();
				if (docs != null) {
					for (Document x : docs) {
						if (x == null) continue;
						String fa = x.getFormAlias();
						if (fa == null || !fa.equalsIgnoreCase(alias)) continue;
						db.removeDocument(x.getUNID());
					}
				}
			}
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		this.m_tables.put(alias, true);
	}

	/**
	 * 检查并重建系统所有表单对应的视图查询表。
	 */
	public void checkAndRebuildAllFormViewTable() {
		try {
			List<Resource> lister = XmlResourceLister.getResources(Form.class);
			if (lister == null || lister.isEmpty()) return;
			for (Resource x : lister) {
				if (x == null || (!(x instanceof Form))) continue;
				checkAndRebuildFormViewTable((Form) x);
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private static volatile boolean monitorFlag = false;
	private static Timer timer = null;
	private static TimerTask timerTask = null;
	final static Object lock = new Object();

	/**
	 * 开始监控定时更新视图查询表任务。
	 */
	public static void startMonitor() {
		synchronized (lock) {
			if (monitorFlag) return;
			monitorFlag = true;
		}

		long current = System.currentTimeMillis();
		DateTime currentDt = new DateTime();
		DateTime dt = new DateTime(currentDt.toString("yyyy-MM-dd") + " 05:20:00");
		long currentAt520 = dt.getTimeMillis();
		long diff = (current - currentAt520);

		// 如果当前时间晚于5点20，先更新一次并设置第一次周期执行日期为第二天5点20；
		if (diff >= 0) dt.adjustDay(1);
		// 如果是第一次运行则执行一次视图查询表任务。
		boolean runFlag = true;
		File firstRun = new File(CommonConfig.getInstance().getInstallationPath() + "WEB-INF" + File.separator + "classes" + File.separator + "firstrun");
		boolean firstRunFlag = (!firstRun.exists());

		// 先执行一次
		if (runFlag) {
			try {
				List<Resource> lister = XmlResourceLister.getResources(Form.class);
				if (lister == null || lister.isEmpty()) return;
				FormViewTable formViewTable = FormViewTable.getInstance();
				if (!firstRunFlag) {
					System.out.println(DateTime.getNowDTString() + ":" + "开始更新视图查询表...");
				} else {
					System.out.println(DateTime.getNowDTString() + ":" + "请稍候，正为系统首次运行进行初始化...");
				}
				for (Resource x : lister) {
					if (x == null || (!(x instanceof Form))) continue;
					formViewTable.checkAndRebuildFormViewTable((Form) x, true);
				}
				if (!firstRunFlag) {
					System.out.println(DateTime.getNowDTString() + ":" + "完成视图查询表更新。");
				} else {
					System.out.println(DateTime.getNowDTString() + ":" + "系统首次运行进行初始化完成。");
				}
			} catch (Exception ex) {
				FileLogger.debug("更新视图查询表时出现异常:" + ex.getMessage());
				FileLogger.error(ex);
			}
		}

		// 然后设置周期执行
		timer = new Timer(FormViewTable.class.getName(), false);
		timerTask = new TimerTask() {
			@Override
			public void run() {
				try {
					List<Resource> lister = XmlResourceLister.getResources(Form.class);
					if (lister == null || lister.isEmpty()) return;
					FormViewTable formViewTable = FormViewTable.getInstance();
					System.out.println(DateTime.getNowDTString() + ":" + "开始更新视图查询表...");
					for (Resource x : lister) {
						if (x == null || (!(x instanceof Form))) continue;
						formViewTable.checkAndRebuildFormViewTable((Form) x, true);
					}
					System.out.println(DateTime.getNowDTString() + ":" + "完成视图查询表更新。");
				} catch (Exception ex) {
					FileLogger.debug("更新视图查询表时出现异常:" + ex.getMessage());
					FileLogger.error(ex);
				}
			}
		};

		timer.schedule(timerTask, (diff > 0L ? (dt.getTimeMillis() - current) : -diff), 1000L * 60L * 60L * 24L); // 每天5点20执行
	}

	/**
	 * 结束监控定时更新视图查询表任务。
	 */
	public static void stopMonitor() {
		if (timerTask != null) {
			timerTask.cancel();
		}
		if (monitorFlag && timer != null) {
			monitorFlag = false;
			timer.purge();
			timer.cancel();
		}
	}
}

