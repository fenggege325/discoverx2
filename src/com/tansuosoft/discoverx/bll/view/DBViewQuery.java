/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.GenericTriplet;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 基于数据库获取视图数据的视图查询实现类的基类。
 * 
 * <p>
 * 不同的关系数据库系统视图查询实现类应继承自此类。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class DBViewQuery extends ViewQuery {
	/**
	 * 缺省构造器。
	 */
	public DBViewQuery() {
		super();
	}

	protected List<ViewEntry> m_viewEntries = null; // 视图数据条目结果集
	protected List<ViewCategoryEntry> m_categoryEntries = null; // 视图分类信息结果集
	protected int m_entryCount = -1; // 条目总数
	protected String m_rawQuery = null; // 手工输入SQL
	private boolean m_rawQueryChecked = false; // 手工输入的SQL是否已经计算完毕标记
	protected String m_queryForCategories = null; // 获取视图分类信息的sql语句
	protected String m_queryForViewEntries = null; // 获取视图数据条目结果集信息的sql语句
	private String m_orderBy = null;
	private String m_from = null;
	private String m_where = null;

	/**
	 * 重载getQueryForCategories
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getQueryForCategories()
	 */
	@Override
	public String getQueryForCategories() {
		if (this.m_queryForCategories == null) {
			List<ParsedColumn> columns = this.getViewParser().parseColumns();
			StringBuilder sb = new StringBuilder();
			int idx = 0;
			ViewColumn vc = null;
			String rawAlias = null;
			for (ParsedColumn parsedColumn : columns) {
				if (parsedColumn == null || parsedColumn.getDynamic() || !(vc = parsedColumn.getColumn()).getCategorized()) continue;
				sb.append((sb.length() <= 0) ? "" : " union ");
				sb.append("select distinct ").append(idx++).append(" viewCategoryIdx,");
				rawAlias = vc.getColumnAlias();
				vc.setColumnAlias("viewCategoryValue");
				sb.append(parsedColumn.getSQLElementResult(this));
				vc.setColumnAlias(rawAlias);
				appendFrom(sb);
				appendWhere(sb);
			}
			if (sb.length() > 0) {
				sb.insert(0, "select * from (");
				sb.append(") tblunion order by viewCategoryIdx,viewCategoryValue");
			}
			this.m_queryForCategories = sb.toString();
		}
		View view = this.getView();
		if (view.getDebugable() && m_queryForCategories != null && m_queryForCategories.length() > 0) {
			FileLogger.debug("“%1$s”分类查询语句:“%2$s”", view.getName(), m_queryForCategories.replace("\r\n", ""));
		}
		return this.m_queryForCategories;
	}

	/**
	 * 构造ViewCategory结果集的{@link com.tansuosoft.discoverx.dao.ResultBuilder}实现类。
	 * 
	 * @author coca@tansuosoft.cn
	 */
	class ViewCategoriesBuilder implements ResultBuilder {
		/**
		 * 缺省构造器。
		 */
		public ViewCategoriesBuilder() {
		}

		/**
		 * 重载build：构造ViewCategory结果集并返回之。
		 * 
		 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
		 */
		@Override
		public Object build(DBRequest request, Object rawResult) {
			DataReader dr = (DataReader) rawResult;
			if (dr == null) return null;

			ViewCategoryEntry x = null;
			ArrayList<ViewCategoryEntry> al = null;
			ArrayList<String> vals = null;
			int lastIdx = -1;
			int idx = -1;
			String v = null;
			try {
				while (dr.next()) {
					idx = dr.getInt(1);
					if (lastIdx != idx) {
						x = new ViewCategoryEntry();
						x.setIndex(idx);
						vals = new ArrayList<String>();
						x.setValues(vals);
						if (al == null) al = new ArrayList<ViewCategoryEntry>();
						al.add(x);
						lastIdx = idx;
					}
					v = dr.getString(2);
					if (v != null && v.length() > 0) vals.add(v);
				}// while end
			} catch (SQLException ex) {
				FileLogger.error(ex);
			}
			return al;
		}
	}

	/**
	 * 通过视图中名为“database”的额外参数名获取非默认数据源的名称并赋予{@link DBRequest}对象。
	 * 
	 * @param dbr
	 */
	protected void useDatabase(DBRequest dbr) {
		if (dbr == null) return;
		View view = this.getView();
		String dbJndids = view.getParamValueString("database", null);
		if (dbJndids == null || dbJndids.trim().length() == 0) return;
		dbr.useDatabase(dbJndids);
	}

	/**
	 * 重载getCategories
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getCategories()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ViewCategoryEntry> getCategories() {
		if (m_categoryEntries != null) return m_categoryEntries;
		m_categoryEntries = ViewBuffer.getInstance().getCategories(this);
		if (m_categoryEntries != null) return m_categoryEntries;

		DBRequest dbr = new DBRequest() {
			@Override
			public SQLWrapper buildSQL() {
				SQLWrapper sqlWrapper = new SQLWrapper();
				sqlWrapper.setRequestType(RequestType.Query);
				String query = getQueryForCategories();
				if (query == null || query.trim().length() == 0) return SQLWrapper.getNonRequestSQLWrapper();
				sqlWrapper.setSql(query);
				return sqlWrapper;
			}
		};
		useDatabase(dbr);
		dbr.setResultBuilder(new ViewCategoriesBuilder());
		dbr.sendRequest();
		Object x = dbr.getResult();
		if (x != null && x instanceof List) {
			m_categoryEntries = (List<ViewCategoryEntry>) x;
		} else {
			m_categoryEntries = new ArrayList<ViewCategoryEntry>();
		}
		ViewBuffer.getInstance().setCategories(this, m_categoryEntries);
		return m_categoryEntries;
	}

	protected static final Pattern P_ORDER_BY = Pattern.compile("\\s+order\\s+by\\s+", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
	protected static final Pattern P_GROUP_BY = Pattern.compile("\\s+group\\s+by\\s+", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

	/**
	 * 返回获取视图数据总条目数的sql语句。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getQueryForEntryCount()
	 */
	@Override
	public String getQueryForEntryCount() {
		String rawQuery = (this.checkRawQuery() ? getRawQueryWithExtraCondition() : null);
		StringBuilder sb = new StringBuilder();
		String result = null;
		if (rawQuery == null || rawQuery.length() == 0) {
			sb.append("select count(*) ");
			this.appendFrom(sb);
			appendWhere(sb);
			result = sb.toString();
		} else {
			sb.append("select count(*) from (");
			String rq = new String(rawQuery).toLowerCase();
			Matcher m = P_ORDER_BY.matcher(rq);
			if (m.find()) {
				int pos = m.start();
				sb.append(pos > 0 ? rawQuery.substring(0, pos) : rawQuery);
			} else {
				sb.append(rawQuery);
			}
			sb.append(") as t0");
		}
		result = sb.toString();
		View view = this.getView();
		if (view.getDebugable() && result.length() > 0) {
			FileLogger.debug("“%1$s”条目数查询语句:“%2$s”", view.getName(), result.replace("\r\n", ""));
		}
		return result;
	}

	/**
	 * 
	 * 重载:getEntryCount
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getEntryCount()
	 */
	@Override
	public int getEntryCount() {
		if (m_entryCount >= 0) return m_entryCount;
		m_entryCount = ViewBuffer.getInstance().getEntryCount(this);
		if (m_entryCount >= 0) return m_entryCount;
		if (this.m_entryCount < 0) {
			this.m_entryCount = 0;
			DBRequest dbr = new DBRequest() {
				@Override
				public SQLWrapper buildSQL() {
					SQLWrapper sqlWrapper = new SQLWrapper();
					sqlWrapper.setRequestType(RequestType.Scalar);
					sqlWrapper.setSql(getQueryForEntryCount());
					return sqlWrapper;
				}
			};
			useDatabase(dbr);
			dbr.sendRequest();
			Object x = dbr.getResult();
			if (x != null) {
				this.m_entryCount = StringUtil.getValueInt(x.toString(), 0);
			} else {
				this.m_entryCount = 0;
			}
			Pagination p = this.getPagination();
			if (p != null) p.setTotalCount(m_entryCount);
		}

		ViewBuffer.getInstance().setEntryCount(this, m_entryCount);

		return this.m_entryCount;
	}

	/**
	 * 构造视图数据条目{@link ViewEntry}结果集的{@link com.tansuosoft.discoverx.dao.ResultBuilder}实现类。
	 * 
	 * @author coca@tansuosoft.cn
	 */
	class ViewEntriesBuilder implements ResultBuilder {
		/**
		 * 缺省构造器。
		 */
		public ViewEntriesBuilder() {
		}

		/**
		 * 重载build：构造视图数据条目{@link ViewEntry}结果集并返回之。
		 * 
		 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
		 */
		@Override
		public Object build(DBRequest request, Object rawResult) {
			DataReader dr = (DataReader) rawResult;
			if (dr == null) return null;

			ViewEntry x = null;
			ArrayList<ViewEntry> al = new ArrayList<ViewEntry>();
			ArrayList<String> vals = null;
			Object val = null;
			String valstr = null;

			try {
				int columnCount = -1;
				ResultSet rs = dr.getResultSet();
				if (rs != null) {
					ResultSetMetaData rsmd = rs.getMetaData();
					if (rsmd != null) columnCount = rsmd.getColumnCount();
				}
				List<ParsedColumn> columns = getViewParser().parseColumns();
				int viewColumnCount = columns.size();
				if (columnCount > 0 && columnCount != viewColumnCount) throw new RuntimeException("视图配置的列和实际读取的列数目不一致！");
				if (columnCount < 0) columnCount = viewColumnCount;
				boolean callViewEntryCallbacksFlag = (!getCache());
				while (dr.next()) {
					x = new ViewEntry();
					vals = new ArrayList<String>();
					x.setValues(vals);
					for (int i = 1; i <= columnCount; i++) {
						ParsedColumn parsedColumn = columns.get(i - 1);
						val = dr.getObject(i);
						valstr = (val == null ? "" : val.toString());
						if (parsedColumn.getUNIDColumnFlag()) {
							x.setUnid(valstr);
							if (!parsedColumn.getDynamic()) vals.add(valstr);
						} else if (!parsedColumn.getDynamic()) vals.add(valstr);
					}
					al.add(x);
					if (callViewEntryCallbacksFlag) callViewEntryCallbacks(x);
				}// while end
			} catch (SQLException ex) {
				FileLogger.error(ex);
			}
			return al;
		}
	}

	/**
	 * 输出sql调试信息。
	 */
	protected void outputDebugSql() {
		View view = this.getView();
		if (view.getDebugable() && m_queryForViewEntries != null && m_queryForViewEntries.length() > 0) {
			FileLogger.debug("“%1$s”数据查询语句:“%2$s”", view.getName(), m_queryForViewEntries.replace("\r\n", ""));
		}
	}

	/**
	 * 重载getQueryForViewEntries
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getQueryForViewEntries()
	 */
	@Override
	public String getQueryForViewEntries() {
		if (m_queryForViewEntries != null && m_queryForViewEntries.length() > 0) return m_queryForViewEntries;
		// 如果没有分页信息或者启用了缓存视图全数据的选项则不分页。
		if (!hasPagination() || this.getCacheAll()) {
			ViewParser parser = this.getViewParser();
			String rawQuery = (this.checkRawQuery() ? getRawQueryWithExtraCondition() : null);
			if (rawQuery != null && rawQuery.length() > 0) {
				m_queryForViewEntries = rawQuery;
				checkRawSql();
			} else {
				StringBuilder sb = new StringBuilder();
				List<ParsedColumn> columns = parser.parseColumns();
				String parsedResult = null;
				if (columns != null && columns.size() > 0) {
					for (ParsedColumn x : columns) {
						if (x == null) continue;
						parsedResult = x.getSQLElementResult(this);
						if (parsedResult == null || parsedResult.trim().length() == 0) continue;
						sb.append(sb.length() > 0 ? "," : "").append(parsedResult);
					}
				}
				if (sb.length() > 0) sb.insert(0, "select ");
				appendFrom(sb);
				appendWhere(sb);
				appendOrderBy(sb);
				m_queryForViewEntries = sb.toString();
				checkConfigSql();
			}
		}
		outputDebugSql();
		return m_queryForViewEntries;
	}

	/**
	 * 校验配置的视图查询语句。
	 */
	protected void checkConfigSql() {
		if (!ViewParser.IsMultipleOrgs) return;
		if (this.m_viewParser.ignoreMultipleOrgs()) return;
		String vname = this.getView().getName() + ":";
		if (m_queryForViewEntries == null || m_queryForViewEntries.isEmpty()) throw new RuntimeException(vname + "无有效查询语句！");
		String l = m_queryForViewEntries.toLowerCase();
		if (l.indexOf(" where ") < 0) throw new RuntimeException(vname + "视图查询语句没有包含查询条件，您必须升级到独立版才能使用不受限制的查询语句！");
		if (l.indexOf(" union ") > 0) throw new RuntimeException(vname + "视图查询语句包含非法查询条件，您必须升级到独立版才能使用不受限制的查询语句！");
	}

	/**
	 * 用来获取vt_表名的正则表达式。
	 */
	protected static final Pattern P_VT = Pattern.compile(".*(t_document|vt_[\\w]+)\\s+(as\\s[\\w]*|[\\w]*|).*", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

	/**
	 * 校验输入的视图查询语句。
	 */
	protected void checkRawSql() {
		if (!ViewParser.IsMultipleOrgs) return;
		if (this.m_viewParser.ignoreMultipleOrgs()) return;
		String vname = this.getView().getName() + ":";
		if (m_queryForViewEntries == null || m_queryForViewEntries.isEmpty()) throw new RuntimeException(vname + "无有效查询语句！");
		if (!this.getView().isCommon()) { throw new RuntimeException(vname + "您必须升级到独立版才能使用此查询条件！"); }
		if (getView().getViewType() != ViewType.DocumentView) { throw new RuntimeException(vname + "您必须升级到独立版才能使用此视图！"); }
		if (OrganizationsContext.getInstance().isInTenantContext()) {
			int orgsc = OrganizationsContext.getInstance().getOrgSc(this.m_viewParser.getSession());
			int pos = 0, tmpPos = 0;
			String l = m_queryForViewEntries.toLowerCase();
			String from = " from ";
			String where = " where ";
			String group = " group by ";
			String having = " having ";
			String union = " union ";
			String order = " order by ";
			String finds[] = { where, "JOIN", "INNER", "LEFT", "RIGHT", group, having, order };
			String oidExp = null;
			String oidTbl = null;
			String oidTblAlias = null;
			String parts[] = StringUtil.splitString(l, union);
			final int unionSepLen = union.length();
			List<GenericTriplet<String, Integer, Boolean>> gts = new ArrayList<GenericTriplet<String, Integer, Boolean>>(parts.length);
			for (int i = 0; i < parts.length; i++) {
				String sql = parts[i];
				pos = sql.lastIndexOf(from);
				if (pos < 0) throw new RuntimeException(vname + "视图查询语句没有包含数据库表，您必须升级到独立版才能使用不受限制的查询语句！");
				oidExp = null;
				oidTbl = null;
				oidTblAlias = null;
				String str = sql.substring(pos);
				Matcher m = P_VT.matcher(str);
				if (m.matches()) {
					oidTbl = m.group(1);
					oidTblAlias = m.group(2);
					if (oidTblAlias.startsWith("as ")) oidTblAlias = oidTblAlias.substring(3).trim();
					for (int j = 0; j < finds.length; j++) {
						if (!finds[j].trim().startsWith(oidTblAlias)) continue;
						oidTblAlias = null;
						break;
					}
				}
				if (oidTbl == null || oidTbl.isEmpty()) throw new RuntimeException(vname + "视图查询语句没有包含必要的数据库表，您必须升级到独立版才能使用不受限制的查询语句！");
				oidExp = (oidTblAlias != null ? oidTblAlias : oidTbl) + "." + DocumentViewParser.C_OID + DocumentViewParser.EQ;
				tmpPos = sql.indexOf(where, pos);
				if (tmpPos > 0) {
					gts.add(new GenericTriplet<String, Integer, Boolean>(oidExp, calcInsPos(parts, i, unionSepLen, tmpPos + where.length()), false));
					continue;
				}
				boolean found = false;
				for (int j = 5; j < finds.length; j++) {
					tmpPos = sql.indexOf(finds[j], pos);
					if (tmpPos > 0) {
						found = true;
						gts.add(new GenericTriplet<String, Integer, Boolean>(oidExp, calcInsPos(parts, i, unionSepLen, tmpPos), true));
						break;
					}
				}
				if (!found) gts.add(new GenericTriplet<String, Integer, Boolean>(oidExp, calcInsPos(parts, i, unionSepLen, sql.length() - 1), true));
			}// for i end
			StringBuilder sb = new StringBuilder(m_queryForViewEntries);
			for (int i = (gts.size() - 1); i >= 0; i--) {
				GenericTriplet<String, Integer, Boolean> gt = gts.get(i);
				sb.insert(gt.getSecond(), (gt.getThird() ? " where " : " ") + gt.getFirst() + orgsc + (gt.getThird() ? " " : " and "));
			}
			m_queryForViewEntries = sb.toString();
		}
	}

	/**
	 * 计算手动输入的sql语句的多租户固定查询条件插入位置
	 * 
	 * @param parts 通过“ union ”为分隔符分割后的的sql语句数组
	 * @param idx 当前处理的sql语句在sql语句数组中的序号。
	 * @param unionSepLen “ union ”的长度
	 * @param offset 插入位置偏移
	 * @return
	 */
	protected int calcInsPos(String[] parts, int idx, int unionSepLen, int offset) {
		int r = 0;
		for (int i = 0; i < idx; i++) {
			r = parts[i].length() + unionSepLen;
		}
		return r + offset;
	}

	/**
	 * 重载getViewEntries
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getViewEntries()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ViewEntry> getViewEntries() {
		View v = this.getView();
		ViewBuffered vb = ViewBuffer.getInstance().getViewBuffered(this);
		if (m_viewEntries == null) m_viewEntries = (vb == null ? null : vb.getViewEntries());
		boolean isCacheValid = (m_viewEntries != null && m_viewEntries.size() > 0); // 是否从缓存中获取到有效数据
		// 如果有获取到缓存的数据，还要判断需要的数据是否在缓存的数据范围之内
		// 有全缓存：有外部分页，无外部分页->不需额外处理分页
		// 无缓存：有外部分页，无外部分页->不需额外处理分页
		// 有部分页面数据缓存：有外部分页时需要额外处理分页，无外部分页时只显示指定缓存页面的数据。
		int realDisplayCountPerPage = -1; // 实际的每页条目数。
		int realCurrentPage = -1; // 实际的第几页。
		int currentCachePage = -1;
		int cachePages = v.getCachePages();
		Pagination p = this.getPagination();
		boolean cacheFlag = this.getCache();
		if (isCacheValid) {
			// 如果只缓存指定页数的数据
			if (cachePages > 0) {
				if (!this.hasPagination()) {
					// 如果没有分页信息则缓存不能用
					isCacheValid = false;
					p = null;
					this.setPagination(p);
				} else {
					// 如果有分页信息且分页数据范围在缓存的数据范围之外则缓存不能用
					int cachedCurrentCachePage = vb.getCurrentCachePage();
					if (cachedCurrentCachePage < 0) cachedCurrentCachePage = 1;
					if (p.getCurrentPage() > cachedCurrentCachePage * cachePages) isCacheValid = false;
					if (p.getCurrentPage() <= (cachedCurrentCachePage - 1) * cachePages) isCacheValid = false;
				}
				if (isCacheValid && vb.getCurrentCachePages() != cachePages) {
					// 如果视图配置中关闭了缓存或缓存页数修改了则缓存不能用且删除缓存
					isCacheValid = false;
					ViewBuffer.getInstance().clearCache(this);
				}
			}
			if (!isCacheValid) m_viewEntries = null;
		}
		boolean foundInCache = isCacheValid;

		if (!foundInCache) {
			if (cacheFlag && this.getCachePages() && this.hasPagination()) {
				realDisplayCountPerPage = p.getDisplayCountPerPage();
				realCurrentPage = p.getCurrentPage();
				p.setDisplayCountPerPage(realDisplayCountPerPage * cachePages);
				currentCachePage = 1 + (realCurrentPage - 1) / cachePages;
				p.setCurrentPage(currentCachePage);
				if (vb != null) vb.setCurrentCachePage(currentCachePage);
			}
			DBRequest dbr = new DBRequest() {
				@Override
				public SQLWrapper buildSQL() {
					SQLWrapper sqlWrapper = new SQLWrapper();
					sqlWrapper.setRequestType(RequestType.Query);
					sqlWrapper.setSql(getQueryForViewEntries());
					return sqlWrapper;
				}
			};
			useDatabase(dbr);
			dbr.setResultBuilder(new ViewEntriesBuilder());
			dbr.sendRequest();
			Object x = dbr.getResult();
			m_viewEntries = (x != null && x instanceof List ? (List<ViewEntry>) x : new ArrayList<ViewEntry>());

			// 没分页且缓存所有数据或没缓存所有数据且有分页则更新缓存的数据
			if ((!this.hasPagination() && this.getCacheAll()) || (cacheFlag && this.getCachePages() && this.hasPagination())) {
				ViewBuffer.getInstance().setViewEntries(this, m_viewEntries);
			}

			// 如果不是缓存所有数据那么要记录实际缓存的数据页数
			if (p != null && currentCachePage > 0 && vb == null) {
				vb = ViewBuffer.getInstance().getViewBuffered(this);
				if (vb != null) vb.setCurrentCachePage(p.getCurrentPage());
			}

			// 如果只缓存指定页数的数据，那么要恢复实际显示的页数和每页显示多少条信息
			if (this.getCachePages() && realDisplayCountPerPage > 0) {
				p.setDisplayCountPerPage(realDisplayCountPerPage);
				p.setCurrentPage(realCurrentPage);
				realDisplayCountPerPage = -1;
				realCurrentPage = -1;
			}
		}

		// 如果在缓存中找到数据或者缓存中没有找到数据但是视图启用了缓存数据。
		if (foundInCache || (!foundInCache && cacheFlag)) {
			int size = m_viewEntries.size();
			int start = 0;
			int end = size;
			// 如果有分页且缓存了视图数据。
			if (p != null) {
				if (cachePages > 0) {
					int l = p.getCurrentPage() % cachePages;
					start = ((l == 0 ? cachePages : l) - 1) * p.getDisplayCountPerPage();
				} else {
					start = (p.getCurrentPage() - 1) * p.getDisplayCountPerPage();
				}
				end = start + p.getDisplayCountPerPage();
			}
			List<ViewEntry> newlist = new ArrayList<ViewEntry>();
			for (int i = start; i < end; i++) {
				if (i >= size) break;
				ViewEntry x = m_viewEntries.get(i);
				newlist.add(x);
				callViewEntryCallbacks(x);
			}
			m_viewEntries = newlist;
			return m_viewEntries;
		}

		return m_viewEntries;
	}

	/**
	 * 检查并解析手工输入的查询条件（SQL语句）。
	 * 
	 * @return boolean 如果有手工输入条件则返回true，否则为false。
	 */
	protected boolean checkRawQuery() {
		if (m_rawQueryChecked) return (this.m_rawQuery != null && this.m_rawQuery.length() > 0);
		this.m_rawQuery = this.getRawQuery();
		if (this.m_rawQuery != null && this.m_rawQuery.length() > 0) {
			this.m_rawQuery = ParsedViewQueryConfig.processExpression(this.m_rawQuery, this, null);
		}
		m_rawQueryChecked = true;
		return (this.m_rawQuery != null && this.m_rawQuery.length() > 0);
	}

	/**
	 * 返回手工输入的sql语句结果，如果有额外提供的查询条件则尝试追加到sql语句中。
	 * 
	 * @return
	 */
	protected String getRawQueryWithExtraCondition() {
		if (this.m_rawQuery == null || this.m_rawQuery.length() == 0) return m_rawQuery;
		ViewParser vp = this.getViewParser();
		if (vp.m_appendedViewConditions == null || vp.m_appendedViewConditions.isEmpty()) return m_rawQuery;
		ViewCondition vc = vp.m_appendedViewConditions.get(0);
		if (vc == null) return m_rawQuery;
		String v = vc.getRightValue();
		if (v == null || v.isEmpty()) return m_rawQuery;
		Matcher m = P_GROUP_BY.matcher(m_rawQuery);
		if (m.find()) {
			int pos = m.start();
			if (pos > 0) return appendRawQueryExtraCondition(v, pos);
		} else {
			m = P_ORDER_BY.matcher(m_rawQuery);
			if (m.find()) {
				int pos = m.start();
				if (pos > 0) return appendRawQueryExtraCondition(v, pos);
			}
		}
		return m_rawQuery;
	}

	/**
	 * 尝试将额外条件追加进pos指定位置的sql语句中并返回结果。
	 * 
	 * @param v
	 * @param pos
	 * @return
	 */
	private String appendRawQueryExtraCondition(String v, int pos) {
		String prev = m_rawQuery.substring(0, pos);
		String r = prev + " " + (prev.indexOf(" where ") > 0 ? " and " + v : "where " + v) + " " + m_rawQuery.substring(pos);
		return r;
	}

	/**
	 * 向sb指定的sql中追加order by子句。
	 * 
	 * @param sb StringBuilder
	 */
	protected void appendOrderBy(StringBuilder sb) {
		if (m_orderBy != null) {
			sb.append(m_orderBy);
			return;
		}
		ViewParser parser = this.getViewParser();
		List<ParsedOrder> list = parser.parseOrders();
		StringBuilder orderBy = new StringBuilder();
		if (list != null && list.size() > 0) {
			orderBy.append("\r\n order by ");
			boolean hasAppended = false;
			for (ParsedOrder x : list) {
				if (x == null) continue;
				orderBy.append(hasAppended ? "," : "").append(x.getSQLElementResult(this));
				if (!hasAppended) hasAppended = true;
			}
		}
		m_orderBy = orderBy.toString();
		sb.append(m_orderBy);
	}

	/**
	 * 向sb指定的sql中追加from子句。
	 * 
	 * @param sb StringBuilder
	 */
	protected void appendFrom(StringBuilder sb) {
		if (m_from != null) {
			sb.append(m_from);
			return;
		}

		ViewParser parser = this.getViewParser();
		List<String> list = parser.parseTables();
		StringBuilder from = new StringBuilder();
		if (list != null && list.size() > 0) {
			boolean hasAppended = false;
			from.append("\r\n from ");
			for (String x : list) {
				if (x == null || x.length() == 0) continue;
				from.append(hasAppended ? "," : "").append(x);
				if (!hasAppended) hasAppended = true;
			}
		}
		m_from = from.toString();
		sb.append(m_from);
	}

	/**
	 * 向sb指定的sql中追加where子句。
	 * 
	 * @param sb StringBuilder
	 */
	protected void appendWhere(StringBuilder sb) {
		if (m_where != null) {
			sb.append(m_where);
			return;
		}

		ViewParser parser = this.getViewParser();
		List<ParsedCondition> list = parser.parseConditions();
		StringBuilder where = new StringBuilder();
		if (list != null && list.size() > 0) {
			where.append("\r\n where ");
			int idx = 0;
			String combineNext = null;
			String parsedResult = null;
			for (ParsedCondition x : list) {
				if (x == null) continue;
				parsedResult = x.getSQLElementResult(this);
				if (parsedResult == null || parsedResult.length() == 0) continue;
				where.append(parsedResult);
				if (idx < (list.size() - 1)) {
					combineNext = x.getCondition().getCombineNextWith();
					if (!combineNext.startsWith(" ")) combineNext = " " + combineNext;
					if (!combineNext.endsWith(" ")) combineNext = combineNext + " ";
					where.append(combineNext);
				}
				idx++;
			}// for end
			m_where = where.toString();
			sb.append(m_where);
		}// if end
	}// func end

}

