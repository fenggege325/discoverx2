/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.List;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewCondition;

/**
 * 提供视图查询条件列表的对象需实现的统一接口。
 * 
 * @author coca@tansuosoft.cn
 */
public interface ConditionsProvider {
	/**
	 * 为指定视图和自定义会话提供查询条件列表。
	 * 
	 * @param view View，当前视图，必须。
	 * @param session Session，当前自定义会话，必须。
	 * @return List&lt;ViewCondition&gt; 返回视图查询条件集合列表或null。
	 */
	public List<ViewCondition> provide(View view, Session session);
}

