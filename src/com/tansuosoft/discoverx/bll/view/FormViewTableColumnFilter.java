/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceAliasContext;
import com.tansuosoft.discoverx.model.DataSourceConstant;
import com.tansuosoft.discoverx.model.DataSourceParticipant;
import com.tansuosoft.discoverx.model.DataSourceResource;
import com.tansuosoft.discoverx.model.Dictionary;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 为文档视图提供主表单字段配置信息的{@link ConfigurableTableFilter}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class FormViewTableColumnFilter implements ConfigurableTableFilter {
	/**
	 * 缺省构造器。
	 */
	public FormViewTableColumnFilter() {
	}

	/**
	 * 重载filter：根据表单别名返回对应视图查询表及其包含的字段信息的新{@link ConfigurableTable}实例。
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ConfigurableTableFilter#filter(com.tansuosoft.discoverx.model.View,
	 *      com.tansuosoft.discoverx.bll.view.ConfigurableTable)
	 */
	@Override
	public ConfigurableTable filter(View view, ConfigurableTable table) {
		if (view == null || table == null) return null;

		String formAlias = view.getTable();
		String formUNID = ResourceAliasContext.getInstance().getUNIDByAlias(Form.class, formAlias);
		Form f = (Form) ResourceContext.getInstance().getResource(formUNID, Form.class);
		if (f == null) return null;
		List<Item> items = f.getItems();
		if (items == null || items.isEmpty()) return null;
		List<ConfigurableColumn> list = new ArrayList<ConfigurableColumn>(items.size());
		for (Item x : items) {
			if (x == null || !x.isFormViewTableColumn()) continue;
			ConfigurableColumn c = new ConfigurableColumn();
			c.setColumnName(x.getItemName());
			c.setColumnTitle(x.getCaption());
			switch (x.getDataSourceType().getIntValue()) {
			case 1: // Resource
				DataSourceResource rds = Instance.safeCast(x.getDataSource(), DataSourceResource.class);
				if (rds == null || !new Dictionary().getDirectory().equalsIgnoreCase(rds.getDirectory())) break;
				String dunid = rds.getUNID();
				if (dunid == null) break;
				if (dunid.startsWith("dict")) dunid = ResourceAliasContext.getInstance().getUNIDByAlias(Dictionary.class, rds.getUNID());
				Dictionary d = ResourceContext.getInstance().getResource(dunid, Dictionary.class);
				if (d != null) c.setDictionary(d.getAlias());
				break;
			case 3: // Participant
				DataSourceParticipant rdp = Instance.safeCast(x.getDataSource(), DataSourceParticipant.class);
				if (rdp == null) break;
				c.setDictionary(String.format("{datasource:'p://%1$s',value:'%2$s',label:'%3$s'}", ParticipantType.getAbbreviation(rdp.getParticipantType()), rdp.getValueFormat(), rdp.getTitleFormat()));
				break;
			case 4: // Constant
				DataSourceConstant rdc = Instance.safeCast(x.getDataSource(), DataSourceConstant.class);
				if (rdc == null) break;
				StringBuilder json = new StringBuilder();
				String configValue = rdc.getConfigValue();
				if (configValue == null || configValue.length() == 0) break;
				List<String> cfgvals = new ArrayList<String>();
				int currentIndex = 0;
				int nextSeparator;
				while ((nextSeparator = configValue.indexOf(DataSourceConstant.VALUES_DELIMITER, currentIndex)) != -1) {
					cfgvals.add(configValue.substring(currentIndex, nextSeparator));
					currentIndex = nextSeparator + 1;
				}
				cfgvals.add(configValue.substring(currentIndex));
				String title = null;
				String value = null;
				int pos = -1;
				for (String cfgval : cfgvals) {
					if (cfgval == null || cfgval.length() == 0) continue;
					pos = cfgval.indexOf(DataSourceConstant.TITLE_VALUE_DELIMITER);
					if (pos < 0) {
						title = value = cfgval;
					} else {
						title = cfgval.substring(0, pos);
						value = cfgval.substring(pos + 1);
						if (value == null || value.length() == 0) value = title;
					}
					json.append(json.length() > 0 ? "," : "");
					json.append("{");
					json.append("v:'").append(StringUtil.encode4Json(value)).append("'");
					json.append(",t:'").append(StringUtil.encode4Json(title)).append("'");
					json.append("}");
				}// for end
				if (json.length() > 0) {
					json.insert(0, "[");
					json.append("]");
					c.setDictionary(json.toString());
				}
				break;
			case 2: // View
			case 100: // Custom
			case 0: // None
			default:
				break;
			}
			list.add(c);
		}

		ConfigurableTable result = new ConfigurableTable();
		result.setTableColumns(list);
		String tblName = String.format("%1$s%2$s", FormViewTable.FORM_VIEW_TABLE_PREFIX, formAlias);
		result.setTableName(tblName);
		result.setTableTitle(f.getName());
		result.setForTargetViewType(table.getForTargetViewType());
		result.setForTargetViewUNID(table.getForTargetViewUNID());

		return result;
	}
}

