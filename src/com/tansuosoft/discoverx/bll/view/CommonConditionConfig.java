/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 常使用的视图通用查询条件配置类。
 * 
 * <p>
 * <strong>注：仅文档视图支持通用查询条件配置！</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class CommonConditionConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private CommonConditionConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static CommonConditionConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return CommonConditionConfig
	 */
	public static CommonConditionConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new CommonConditionConfig();
			}
		}
		return m_instance;
	}

	private List<CommonCondition> m_commonConditions = null; // 包含的固定条件列表。

	/**
	 * 返回含的固定条件列表。
	 * 
	 * @return List<CommonCondition>
	 */
	public List<CommonCondition> getCommonConditions() {
		return this.m_commonConditions;
	}

	/**
	 * 设置含的固定条件列表。
	 * 
	 * @param commonConditions List<CommonCondition>
	 */
	public void setCommonConditions(List<CommonCondition> commonConditions) {
		this.m_commonConditions = commonConditions;
	}

	/**
	 * 根据固定查询条件名称返回对应的CommonCondition对象，如果找不到择返回null。
	 * 
	 * @param name String 固定查询条件名称
	 * @return CommonCondition
	 */
	public CommonCondition getCommonCondition(String name) {
		if (name == null || name.length() == 0 || this.m_commonConditions == null || this.m_commonConditions.isEmpty()) return null;
		for (CommonCondition x : this.m_commonConditions) {
			if (name.equalsIgnoreCase(x.getName())) return x;
		}
		return null;
	}
}

