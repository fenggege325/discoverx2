/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.List;

import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;

/**
 * 视图实用工具类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewHelper {
	/**
	 * 缺省构造器。
	 */
	private ViewHelper() {
	}

	/**
	 * 获取视图直接父资源。
	 * 
	 * <p>
	 * 视图父资源可能是视图或应用模块。
	 * </p>
	 * 
	 * @param view
	 * @return
	 */
	public static Resource getViewDirectParent(View view) {
		if (view == null) return null;
		String PUNID = view.getPUNID();
		Resource parent = ResourceContext.getInstance().getResource(PUNID, Application.class);
		if (parent == null) parent = ResourceContext.getInstance().getResource(PUNID, View.class);
		return parent;
	}

	/**
	 * 获取视图根级别父资源。
	 * 
	 * <p>
	 * 视图父资源可能是视图或应用模块。
	 * </p>
	 * 
	 * @param view
	 * @return
	 */
	public static Resource getViewRootParent(View view) {
		String PUNID = view.getPUNID();
		Resource parent = null;
		if (PUNID != null) {
			parent = ResourceContext.getInstance().getResource(PUNID, Application.class);
			if (parent == null) parent = ResourceContext.getInstance().getResource(PUNID, View.class);
			while (parent != null) {
				PUNID = parent.getPUNID();
				if (PUNID == null || PUNID.length() == 0) return parent;
				parent = ResourceContext.getInstance().getResource(PUNID, Application.class);
				if (parent == null) parent = ResourceContext.getInstance().getResource(PUNID, View.class);
			}
		}
		return parent;
	}

	/**
	 * 获取视图所属类别为应用模块的直接父资源。
	 * 
	 * @param view
	 * @return
	 */
	public static Application getViewDirectParentApplication(View view) {
		String PUNID = view.getPUNID();
		Resource parent = null;
		if (PUNID != null) {
			parent = ResourceContext.getInstance().getResource(PUNID, Application.class);
			if (parent == null) parent = ResourceContext.getInstance().getResource(PUNID, View.class);
			while (parent != null && (parent instanceof View) && (PUNID = parent.getPUNID()) != null) {
				parent = ResourceContext.getInstance().getResource(PUNID, Application.class);
				if (parent != null && parent instanceof Application) {
					break;
				} else if (parent == null) {
					parent = ResourceContext.getInstance().getResource(PUNID, View.class);
				}
			}
		}
		if (parent != null && parent instanceof Application) return (Application) parent;
		return null;
	}

	/**
	 * 返回指定的视图是否包含分类列。
	 * 
	 * @param view View
	 * @return boolean
	 */
	public static boolean hasCategoryColumn(View view) {
		if (view == null) return false;
		List<ViewColumn> vcs = view.getColumns();
		if (vcs == null || vcs.isEmpty()) return false;
		for (ViewColumn vc : vcs) {
			if (vc != null && vc.getCategorized()) return true;
		}
		return false;
	}
}

