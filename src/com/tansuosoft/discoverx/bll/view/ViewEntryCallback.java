/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.HashMap;
import java.util.Iterator;

import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 每获取到一条视图数据条目之后自动触发执回调功能的抽象类。
 * 
 * <p>
 * 继承此类的具体子类一般称为视图数据条目（或称视图内容）的回调对象或组装对象。
 * </p>
 * 
 * <p>
 * 子类可继承此类用以按自己需要的格式组装视图数据条目的结果。比如可以把视图数据条目内容组合为一个符合前台浏览器显示的特殊html片断。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ViewEntryCallback {
	/**
	 * 缺省构造器。
	 */
	public ViewEntryCallback() {
	}

	private ViewQuery m_viewQuery = null;
	private HashMap<String, Object> m_parameters = null; // 额外参数值。

	/**
	 * 设置绑定的{@link ViewQuery}。
	 * 
	 * @param viewQuery
	 */
	protected void setViewQuery(ViewQuery viewQuery) {
		this.m_viewQuery = viewQuery;
	}

	/**
	 * 返回绑定的{@link ViewQuery}。
	 * 
	 * @return ViewQuery
	 */
	protected ViewQuery getViewQuery() {
		return this.m_viewQuery;
	}

	/**
	 * 返回绑定的视图（{@link View}）。
	 * 
	 * @return View
	 */
	protected View getView() {
		if (this.m_viewQuery != null) return this.m_viewQuery.getViewParser().getView();
		return null;
	}

	/**
	 * 每获取到一条视图数据条目时自动调用的函数（回调函数）。
	 * 
	 * @param viewEntry 表示一条组装好的{@link ViewEntry}对象。
	 * @param query ViewQuery 获取ViewEntry的ViewQuery对象。
	 * 
	 */
	public abstract void entryFetched(ViewEntry viewEntry, ViewQuery query);

	/**
	 * 获取所有视图数据条目结果组装完毕的最终结果对象。
	 * 
	 * @return Object
	 */
	public abstract Object getResult();

	/**
	 * 按指定类型获取所有视图数据条目结果组装完毕的最终结果对象。
	 * 
	 * @param <T>
	 * @param clazz
	 * @return T 如果组装结果为null或无法转换为T，则返回null。
	 */
	public <T> T getResult(Class<T> clazz) {
		return Instance.safeCast(getResult(), clazz);
	}

	/**
	 * 获取组装结果结果格式类型。
	 * 
	 * <p>
	 * 通常用于在呈现视图内容出错时根据不同格式类型输出不同格式的错误信息，系统当前支持以下格式类型：
	 * <ul>
	 * <li>listview：表示返回遵循ListView格式的json文本；</li>
	 * <li>html：表示返回html文本格式（这是系统默认返回值）；</li>
	 * <li>xml：表示返回xml文本格式；</li>
	 * <li>json：表示返回其它（区别于ListView格式）格式的json文本；</li>
	 * <li>other：表示返回其它格式；</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 */
	public String getResultFormat() {
		return "html";
	}

	/**
	 * 以Object类型获取指定名称的参数值。
	 * 
	 * @param paramName String 参数名，必须。
	 * @return Object 返回对应参数值，如果找不到则为null。
	 */
	public Object getParameter(String paramName) {
		if (paramName == null || paramName.length() == 0 || this.m_parameters == null || this.m_parameters.isEmpty()) return null;
		return this.m_parameters.get(paramName);
	}

	/**
	 * 获取指定参数名对应参数值的文本结果，如果获取不到或获取到的值为空字符串（长度为零的字符串）则返回defaultValue。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return String
	 */
	public String getParameterString(String paramName, String defaultValue) {
		Object obj = this.getParameter(paramName);
		if (obj == null) return defaultValue;
		String str = obj.toString();
		if (str.length() == 0) return defaultValue;
		return str;
	}

	/**
	 * 获取指定参数名对应参数值的int结果，如果获取不到则返回defaultValue。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return int
	 */
	public int getParameterInt(String paramName, int defaultValue) {
		return StringUtil.getValueInt(getParameterString(paramName, null), defaultValue);
	}

	/**
	 * 获取指定参数名对应参数值的boolean结果，如果获取不到则返回defaultValue。
	 * 
	 * @param paramName
	 * @param defaultValue
	 * @return boolean
	 */
	public boolean getParameterBoolean(String paramName, boolean defaultValue) {
		return StringUtil.getValueBool(getParameterString(paramName, null), defaultValue);
	}

	/**
	 * 获取指定参数名对应参数值的指定对象类型的结果，如果获取不到则返回defaultValue。
	 * 
	 * @param <T>
	 * @param paramName
	 * @param clazz
	 * @param defaultReturn
	 * @return
	 */
	public <T> T getParameterType(String paramName, Class<T> clazz, T defaultReturn) {
		Object o = this.getParameter(paramName);
		return StringUtil.getValueGeneric(o, clazz, defaultReturn);
	}

	/**
	 * 设置name指定的参数名称对应的参数值为value。
	 * <p>
	 * 如果同名的参已经存在，则其值被替换，如果value为null，则等同于调用removeParameter(name)
	 * </p>
	 * 
	 * @param name String
	 * @param value Object
	 */
	public void setParameter(String name, Object value) {
		if (name == null || name.length() == 0) return;
		if (this.m_parameters == null) this.m_parameters = new HashMap<String, Object>();
		if (value == null) {
			this.removeParameter(name);
			return;
		}
		this.m_parameters.put(name, value);
	}

	/**
	 * 删除name名称指定的参数值。
	 * 
	 * @param name String
	 */
	public void removeParameter(String name) {
		if (this.m_parameters != null && name != null && name.length() > 0) {
			this.m_parameters.remove(name);
		}
	}

	/**
	 * 获取所有参数名称字符串集合对应的迭代器对象，如果没有任何参数，则返回null。
	 * 
	 * @return Iterator<String>
	 */
	public Iterator<String> getParameterNames() {
		if (this.m_parameters == null || this.m_parameters.size() == 0) return null;
		Iterator<String> iterator = this.m_parameters.keySet().iterator();
		return iterator;
	}
}

