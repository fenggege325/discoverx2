/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 为视图配置提供最终可供配置的表和字段信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class TableColumnProvider {
	/**
	 * 为指定视图提供最终可用的所有表和表字段信息集合。
	 * 
	 * @param view {@link com.tansuosoft.discoverx.model.View}，必须。
	 * @return List&lt;ConfigurableTable&gt;
	 */
	public static List<ConfigurableTable> getTables(View view) {
		if (view == null) return null;
		TableColumnConfig tcc = TableColumnConfig.getInstance();
		List<ConfigurableTable> list = tcc.getConfigurableTables(view);
		if (list == null || list.isEmpty()) return null;
		List<ConfigurableTable> result = new ArrayList<ConfigurableTable>(list.size());
		try {
			for (int i = 0; i < list.size(); i++) {
				ConfigurableTable c = list.get(i);
				if (c == null) continue;

				// 如果是属于模块的视图，那么要判断模块是否启用了额外状态信息
				String tableName = c.getTableName();
				String appUNID = view.getPUNID();
				if (tableName.equalsIgnoreCase("t_extra") && appUNID != null && appUNID.length() > 0) {
					Application app = ResourceContext.getInstance().getResource(appUNID, Application.class);
					if (app != null && !app.getAutoRecordStateInfo()) continue;
				}

				boolean filterFlag = false;
				String clsName = c.getFilter();
				if (clsName != null && clsName.length() > 0) {
					ConfigurableTableFilter filter = Instance.newInstance(clsName, ConfigurableTableFilter.class);
					if (filter != null) {
						ConfigurableTable tbl = filter.filter(view, c);
						if (tbl != null) {
							result.add(tbl);
							filterFlag = true;
						}
					}
				}
				if (!filterFlag && c.getTableColumns() != null && c.getTableColumns().size() > 0) {
					result.add((ConfigurableTable) c.clone());
				}
			}// for end
		} catch (CloneNotSupportedException e) {
			FileLogger.error(e);
		}

		return result;
	}
}

