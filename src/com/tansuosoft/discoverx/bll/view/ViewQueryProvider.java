/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.dao.DAOConfig;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 视图查询对象提供类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ViewQueryProvider {
	/**
	 * 私有构造器。
	 */
	private ViewQueryProvider() {
	}

	/**
	 * 根据视图、用户自定义会话等信息获取ViewQuery对象实例。
	 * 
	 * <p>
	 * 根据视图分页配置自动设置分页为第一页或不分页。
	 * </p>
	 * 
	 * @param view View
	 * @param session Session
	 * @return ViewQuery
	 */
	public static ViewQuery getViewQuery(View view, Session session) {
		if (view == null) throw new RuntimeException("未提供有效视图资源对象");
		int displayCountPerPage = view.getDisplayCountPerPage();
		if (displayCountPerPage <= 0) return getViewQuery(view, session, null);
		return getViewQuery(view, session, new Pagination(1, displayCountPerPage));
	}

	/**
	 * 根据视图、用户自定义会话、分页等信息获取ViewQuery对象实例。
	 * 
	 * @param view View
	 * @param session Session
	 * @param pagination Pagination
	 * @return ViewQuery
	 */
	public static ViewQuery getViewQuery(View view, Session session, Pagination pagination) {
		if (view == null) throw new RuntimeException("未提供有效视图资源对象");
		ViewQuery viewQuery = null;

		String impl = view.getViewQueryImplement();
		if (impl != null && impl.length() > 0) {
			viewQuery = Instance.newInstance(impl, ViewQuery.class);
		} else {
			if (view.getViewType() == ViewType.XMLResourceView) {
				viewQuery = new XmlViewQuery();
			} else if (view.getViewType() == ViewType.DBResourceView || view.getViewType() == ViewType.DocumentView) {
				String systemImpl = DAOConfig.getInstance().getViewQueryImplement();
				if (systemImpl == null || systemImpl.length() == 0) throw new RuntimeException("没有配置数据库视图查询实现类，请联系系统管理员！");
				viewQuery = Instance.newInstance(systemImpl, ViewQuery.class);
			}
		}
		if (viewQuery == null) throw new RuntimeException("无法获取视图“" + view.getName() + "”对应的视图查询实现类，请检查配置！");
		ViewParser viewParser = ViewParserProvider.getViewParser(view, session);
		viewQuery.setViewParser(viewParser);
		viewQuery.setPagination(pagination);
		return viewQuery;
	}
}

