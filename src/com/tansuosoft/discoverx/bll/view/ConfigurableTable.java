/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.List;

import com.tansuosoft.discoverx.model.ViewType;
import com.tansuosoft.discoverx.util.ObjectUtil;

/**
 * 用于描述视图配置时可使用的表的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ConfigurableTable implements Cloneable {
	/**
	 * 缺省构造器。
	 */
	public ConfigurableTable() {
	}

	private String m_tableName = null; // 表名，必须。
	private String m_tableTitle = null; // 表标题，必须。
	private ViewType m_forTargetViewType = ViewType.DocumentView; // 可应用的视图类型，默认为文档视图。
	private String m_forTargetViewUNID = null; // 可应用的具体视图UNID。
	private String m_filter = null; // 表字段列表提供类全限定类名。
	private List<ConfigurableColumn> m_tableColumns = null; // 表包含的表字段信息。

	/**
	 * 返回表名，必须。
	 * 
	 * @return String 表名，必须。
	 */
	public String getTableName() {
		return this.m_tableName;
	}

	/**
	 * 设置表名，必须。
	 * 
	 * @param tableName String 表名，必须。
	 */
	public void setTableName(String tableName) {
		this.m_tableName = tableName;
	}

	/**
	 * 返回表标题，必须。
	 * 
	 * @return String 表标题，必须。
	 */
	public String getTableTitle() {
		return this.m_tableTitle;
	}

	/**
	 * 设置表标题，必须。
	 * 
	 * @param tableTitle String 表标题，必须。
	 */
	public void setTableTitle(String tableTitle) {
		this.m_tableTitle = tableTitle;
	}

	/**
	 * 返回可应用的视图类型，默认为文档视图。
	 * 
	 * @see ConfigurableTable#getForTargetViewUNID()
	 * 
	 * @return ViewType 对应的视图类型，默认为文档视图。
	 */
	public ViewType getForTargetViewType() {
		return this.m_forTargetViewType;
	}

	/**
	 * 设置可应用的视图类型，默认为文档视图。
	 * 
	 * @param forTargetViewType ViewType 对应的视图类型，默认为文档视图。
	 */
	public void setForTargetViewType(ViewType forTargetViewType) {
		this.m_forTargetViewType = forTargetViewType;
	}

	/**
	 * 返回表字段列表过滤实现类权限定名称。
	 * 
	 * <p>
	 * 如过提供了有效的过滤类信息，则调用其filter方法以提供表字段列表信息。
	 * </p>
	 * <p>
	 * 此类必须是实现{@link ConfigurableTableFilter}接口的具体类。
	 * </p>
	 * 
	 * @see ConfigurableTable#getTableColumns()
	 * @return String
	 */
	public String getFilter() {
		return this.m_filter;
	}

	/**
	 * 设置表字段列表过滤实现类权限定名称。
	 * 
	 * @param filter String
	 */
	public void setFilter(String filter) {
		this.m_filter = filter;
	}

	/**
	 * 返回表配置中原始包含的表字段信息。
	 * 
	 * @see ConfigurableTable#getFilter()
	 * @return List&lt;ConfigurableColumn&gt; 表包含的表字段信息。
	 */
	public List<ConfigurableColumn> getTableColumns() {
		return this.m_tableColumns;
	}

	/**
	 * 设置表配置中原始包含的表字段信息。
	 * 
	 * @param tableColumns List&lt;ConfigurableColumn&gt; 表包含的表字段信息。
	 */
	public void setTableColumns(List<ConfigurableColumn> tableColumns) {
		this.m_tableColumns = tableColumns;
	}

	/**
	 * 返回可应用的具体视图UNID。
	 * 
	 * <p>
	 * 如果此属性为空则所有符合{@link #getForTargetViewType()}返回值中定义的类型的视图都会应用此配置，如果此属性返回有效UNID，则只有{@link #getForTargetViewType()}返回值中定义的类型和此属性返回值同时匹配的视图才会应用此配置。
	 * </p>
	 * 
	 * @return String
	 */
	public String getForTargetViewUNID() {
		return this.m_forTargetViewUNID;
	}

	/**
	 * 设置可应用的具体视图UNID。
	 * 
	 * @param forTargetViewUNID String
	 */
	public void setForTargetViewUNID(String forTargetViewUNID) {
		this.m_forTargetViewUNID = forTargetViewUNID;
	}

	/**
	 * 重载clone
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ConfigurableTable x = new ConfigurableTable();

		x.setFilter(this.getFilter());
		x.setForTargetViewUNID(this.getForTargetViewUNID());
		x.setTableName(this.getTableName());
		x.setTableTitle(this.getTableTitle());
		x.setForTargetViewType(this.getForTargetViewType());
		x.setTableColumns(ObjectUtil.cloneList(this.getTableColumns()));

		return x;
	}

}

