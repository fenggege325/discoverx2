/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.List;

/**
 * 描述视图数据条目信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ViewEntry {
	private String m_unid = null; // 条目UNID。
	private List<String> m_values = null; // 条目包含的列值。

	/**
	 * 缺省构造器。
	 */
	public ViewEntry() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param unid
	 * @param values
	 */
	public ViewEntry(String unid, List<String> values) {
		this.m_unid = unid;
		this.m_values = values;
	}

	/**
	 * 返回条目UNID。
	 * 
	 * <p>
	 * 一般是指文档或资源的UNID。
	 * </p>
	 * 
	 * @return String
	 */
	public String getUnid() {
		return this.m_unid;
	}

	/**
	 * 设置条目UNID。
	 * 
	 * @param unid
	 */
	public void setUnid(String unid) {
		this.m_unid = unid;
	}

	/**
	 * 返回条目包含的列值。
	 * 
	 * <p>
	 * 列值的顺序和个数应与获取此数据条目的视图包含的视图列信息一一对应。
	 * </p>
	 * 
	 * @return List&lt;String&lt;
	 */
	public List<String> getValues() {
		return this.m_values;
	}

	/**
	 * 设置条目包含的列值。
	 * 
	 * @param values List&lt;String&lt;
	 */
	protected void setValues(List<String> values) {
		this.m_values = values;
	}

	/**
	 * 返回列值集合中包含几个具体值。
	 * 
	 * @return int 如果列值集合为null或空集合，则返回0。
	 */
	public int getColumnsCount() {
		if (this.m_values == null || this.m_values.isEmpty()) return 0;
		return this.m_values.size();
	}

}

