/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;

import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 缓存并管理视图数据的类。
 * 
 * <p>
 * 参考{@link View#getCache()}中对启用视图缓存的说明和注意事项。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
final class ViewBuffer {
	/**
	 * 文档缓存的缓存名称常数。
	 */
	protected static final String VIEW_CACHE_NAME = "viewCache";
	/**
	 * 默认缓存时间：600秒/10分钟。
	 */
	protected static final int DEFAULT_CACHED_SECONDS = 600;
	private CacheManager m_cacheManager = null; // 缓存管理器
	private Cache m_cache = null; // 缓存。

	/**
	 * 私有构造器。
	 */
	private ViewBuffer() {
		int capacity = 500;
		this.m_cacheManager = CacheManager.getInstance();
		this.m_cache = new Cache(VIEW_CACHE_NAME, // java.lang.String name,
				capacity, // int maxElementsInMemory,
				MemoryStoreEvictionPolicy.LRU, // MemoryStoreEvictionPolicy memoryStoreEvictionPolicy,
				false, // boolean overflowToDisk,
				null, // java.lang.String diskStorePath,
				false, // boolean eternal,
				120, // long timeToLiveSeconds,
				0, // long timeToIdleSeconds,
				false, // boolean diskPersistent,
				36000, // long diskExpiryThreadIntervalSeconds,
				null, // RegisteredEventListeners registeredEventListeners,
				null, // BootstrapCacheLoader bootstrapCacheLoader,
				0, // int maxElementsOnDisk,
				30 // int diskSpoolBufferSizeMB
		);
		this.m_cacheManager.addCache(this.m_cache);
	}

	private static ViewBuffer m_instance = null;

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * <p>
	 * 由系统内部调用。
	 * </p>
	 * 
	 * @return ViewBuffer
	 */
	public static ViewBuffer getInstance() {
		synchronized (ResourceContext.class) {
			if (m_instance == null) {
				m_instance = new ViewBuffer();
			}
		}
		return m_instance;
	}

	/**
	 * 为指定key构造一个新{@link ViewBuffered}对象。
	 * 
	 * @param key
	 * @return
	 */
	protected ViewBuffered createViewBuffered(String key) {
		if (key == null || key.length() == 0) return null;
		ViewBuffered result = new ViewBuffered();
		result.setViewAlias(key);
		return result;
	}

	/**
	 * 设置当前{@link ViewQuery}对应的{@link ViewBuffered}对象的实际缓存数据的页数(即{@link ViewBuffered#setCurrentCachePage(int)})。
	 * 
	 * <p>
	 * 由系统内部调用。
	 * </p>
	 * 
	 * @param viewQuery
	 * @param currentCachePage
	 */
	protected void setCurrentCachePage(ViewQuery viewQuery, int currentCachePage) {
		if (viewQuery == null) return;
		String key = viewQuery.getCacheKey();
		if (key == null || key.length() == 0) return;
		Element oldel = this.m_cache.get(key);
		ViewBuffered vb = (oldel == null ? null : (ViewBuffered) oldel.getObjectValue());
		if (vb != null) vb.setCurrentCachePage(currentCachePage);
	}

	/**
	 * 返回{@link ViewQuery}对应的已缓存的视图数据。
	 * 
	 * <p>
	 * 由系统内部调用。
	 * </p>
	 * 
	 * @param viewQuery
	 * @return 如果没有已缓存的视图数据，则返回null。
	 */
	protected ViewBuffered getViewBuffered(ViewQuery viewQuery) {
		if (viewQuery == null) return null;
		String key = viewQuery.getCacheKey();
		if (key == null || key.length() == 0) return null;
		Element el = this.m_cache.get(key);
		ViewBuffered result = (el == null ? null : (ViewBuffered) el.getObjectValue());
		List<ViewEntry> list = (result != null ? result.getViewEntries() : null);
		if (viewQuery.getView().getDebugable() && list != null) FileLogger.debug("ViewEntries(%d Counts) Fetched From Cache By Key:\"%2$s\".", list.size(), key);
		return result;
	}

	/**
	 * 添加{@link ViewQuery}对应的视图查询数据到缓存中。
	 * 
	 * <p>
	 * 如果{@link ViewQuery}绑定的视图没有配置为打开数据缓存（即{@link View#getCache()}返回false）则什么也不做直接返回null。
	 * </p>
	 * 
	 * @param viewQuery
	 * @param viewEntries 视图数据列表集合，如果为null，则删除缓存。
	 * @return 返回之前的数据（如果存在的话，否则返回null）。
	 */
	public List<ViewEntry> setViewEntries(ViewQuery viewQuery, List<ViewEntry> viewEntries) {
		if (viewQuery == null) return null;
		String key = viewQuery.getCacheKey();
		if (key == null || key.length() == 0 || viewEntries == null) return null;
		Element oldel = this.m_cache.get(key);
		List<ViewEntry> result = (oldel == null ? null : ((ViewBuffered) oldel.getObjectValue()).getViewEntries());
		ViewBuffered vb = (oldel == null ? createViewBuffered(key) : (ViewBuffered) oldel.getObjectValue());
		vb.setCurrentCachePages(viewQuery.getView().getCachePages());
		vb.setViewEntries(viewEntries);
		Element newel = new Element(key, vb);
		newel.setTimeToLive(viewQuery.getView().getCacheExpires() * 60);
		this.m_cache.put(newel);
		return result;
	}

	/**
	 * 添加key对应的视图视图查询数据到缓存中。
	 * 
	 * @param key
	 * @param viewEntries
	 * @return 返回之前的数据（如果存在的话，否则返回null）。
	 */
	public List<ViewEntry> setViewEntries(String key, List<ViewEntry> viewEntries) {
		if (key == null || key.length() == 0 || viewEntries == null) return null;
		Element oldel = this.m_cache.get(key);
		List<ViewEntry> result = (oldel == null ? null : ((ViewBuffered) oldel.getObjectValue()).getViewEntries());
		ViewBuffered vb = (oldel == null ? createViewBuffered(key) : (ViewBuffered) oldel.getObjectValue());
		vb.setViewEntries(viewEntries);
		Element newel = new Element(key, vb);
		this.m_cache.put(newel);
		return result;
	}

	/**
	 * 添加key对应的视图数据条目总数到缓存中。
	 * 
	 * @param key
	 * @param entryCount
	 * @return 返回之前的条目数（如果存在的话，否则返回0）。
	 */
	public int setEntryCount(String key, int entryCount) {
		if (key == null || key.length() == 0 || entryCount < 0) return -1;
		Element oldel = this.m_cache.get(key);
		int result = (oldel == null ? 0 : ((ViewBuffered) oldel.getObjectValue()).getEntryCount());
		ViewBuffered vb = (oldel == null ? createViewBuffered(key) : (ViewBuffered) oldel.getObjectValue());
		vb.setEntryCount(entryCount);
		Element newel = new Element(key, vb);
		this.m_cache.put(newel);
		return result;
	}

	/**
	 * 添加{@link ViewQuery}对应的视图数据条目总数到缓存中。
	 * 
	 * <p>
	 * 如果{@link ViewQuery}绑定的视图没有配置为打开数据缓存（即{@link View#getCache()}返回false）则什么也不做直接返回null。
	 * </p>
	 * 
	 * @param viewQuery
	 * @param entryCount
	 * @return 返回之前的条目数（如果存在的话，否则返回0）。
	 */
	public int setEntryCount(ViewQuery viewQuery, int entryCount) {
		if (viewQuery == null) return -1;
		String key = viewQuery.getPermanentCacheKey();
		if (key == null || key.length() == 0 || key.length() == 0 || entryCount < 0) return -1;
		Element oldel = this.m_cache.get(key);
		int result = (oldel == null ? 0 : ((ViewBuffered) oldel.getObjectValue()).getEntryCount());
		ViewBuffered vb = (oldel == null ? createViewBuffered(key) : (ViewBuffered) oldel.getObjectValue());
		vb.setEntryCount(entryCount);
		Element newel = new Element(key, vb);
		View v = viewQuery.getView();
		if (viewQuery.getCache()) newel.setTimeToLive(v.getCacheExpires() * 60);
		else newel.setTimeToLive(DEFAULT_CACHED_SECONDS);
		this.m_cache.put(newel);
		return result;
	}

	/**
	 * 添加key对应的分类数据到缓存中。
	 * 
	 * @param key
	 * @param categories
	 * @return 返回之前的数据（如果存在的话，否则返回null）。
	 */
	public List<ViewCategoryEntry> setCategories(String key, List<ViewCategoryEntry> categories) {
		if (key == null || key.length() == 0 || categories == null) return null;
		Element oldel = this.m_cache.get(key);
		List<ViewCategoryEntry> result = (oldel == null ? null : ((ViewBuffered) oldel.getObjectValue()).getCategories());
		ViewBuffered vb = (oldel == null ? createViewBuffered(key) : (ViewBuffered) oldel.getObjectValue());
		vb.setCategories(categories);
		Element newel = new Element(key, vb);
		this.m_cache.put(newel);
		return result;
	}

	/**
	 * 添加{@link ViewQuery}对应的分类数据到缓存中。
	 * 
	 * 
	 * <p>
	 * 如果{@link ViewQuery}绑定的视图没有配置为打开数据缓存（即{@link View#getCache()}返回false）则什么也不做直接返回null。
	 * </p>
	 * 
	 * @param viewQuery
	 * @param categories
	 * @return 返回之前的数据（如果存在的话，否则返回null）。
	 */
	public List<ViewCategoryEntry> setCategories(ViewQuery viewQuery, List<ViewCategoryEntry> categories) {
		if (viewQuery == null) return null;
		String key = viewQuery.getPermanentCacheKey();
		if (key == null || key.length() == 0 || key.length() == 0 || categories == null) return null;
		Element oldel = this.m_cache.get(key);
		List<ViewCategoryEntry> result = (oldel == null ? null : ((ViewBuffered) oldel.getObjectValue()).getCategories());
		ViewBuffered vb = (oldel == null ? createViewBuffered(key) : (ViewBuffered) oldel.getObjectValue());
		vb.setCategories(categories);
		Element newel = new Element(key, vb);
		View v = viewQuery.getView();
		if (viewQuery.getCache()) newel.setTimeToLive(v.getCacheExpires() * 60);
		else newel.setTimeToLive(DEFAULT_CACHED_SECONDS);
		this.m_cache.put(newel);
		return result;
	}

	/**
	 * 返回{@link ViewQuery}对应的已缓存的视图数据。
	 * 
	 * @param viewQuery
	 * @return 如果没有已缓存的视图数据，则返回null。
	 */
	public List<ViewEntry> getViewEntries(ViewQuery viewQuery) {
		if (viewQuery == null) return null;
		String key = viewQuery.getCacheKey();
		List<ViewEntry> list = getViewEntries(key);
		if (viewQuery.getView().getDebugable() && list != null) FileLogger.debug("ViewEntries(%d Counts) Fetched From Cache By Key:\"%2$s\".", list.size(), key);
		return list;
	}

	/**
	 * 返回key对应的已缓存的视图数据。
	 * 
	 * @param key
	 * @return 如果没有已缓存的视图数据，则返回null。
	 */
	public List<ViewEntry> getViewEntries(String key) {
		Element el = this.m_cache.get(key);
		if (el == null) return null;
		ViewBuffered vb = (ViewBuffered) el.getObjectValue();
		return vb.getViewEntries();
	}

	/**
	 * 返回{@link ViewQuery}对应的已缓存的视图数据条目数。
	 * 
	 * @param viewQuery
	 * @return 如果没有已缓存的视图数据条目数，则返回-1。
	 */
	public int getEntryCount(ViewQuery viewQuery) {
		if (viewQuery == null) return -1;
		String key = viewQuery.getPermanentCacheKey();
		int result = getEntryCount(key);
		if (viewQuery.getView().getDebugable() && result >= 0) FileLogger.debug("EntryCount(%d) Fetched From Cache By Key:\"%2$s\".", result, key);
		return result;
	}

	/**
	 * 返回key对应的已缓存的视图数据条目数。
	 * 
	 * @param key
	 * @return 如果没有已缓存的视图数据条目数，则返回-1。
	 */
	public int getEntryCount(String key) {
		Element el = this.m_cache.get(key);
		return (el == null ? -1 : ((ViewBuffered) el.getObjectValue()).getEntryCount());
	}

	/**
	 * 返回{@link ViewQuery}对应的已缓存的视图分类数据。
	 * 
	 * @param viewQuery
	 * @return 如果没有已缓存的视图分类数据，则返回null。
	 */
	public List<ViewCategoryEntry> getCategories(ViewQuery viewQuery) {
		if (viewQuery == null) return null;
		String key = viewQuery.getPermanentCacheKey();
		List<ViewCategoryEntry> list = getCategories(key);
		if (viewQuery.getView().getDebugable() && list != null) FileLogger.debug("ViewCategoryEntries(%d Counts) Fetched From Cache By Key:\"%2$s\".", list.size(), key);
		return list;
	}

	/**
	 * 返回key对应的已缓存的视图分类数据。
	 * 
	 * @param key
	 * @return 如果没有已缓存的视图分类数据，则返回null。
	 */
	public List<ViewCategoryEntry> getCategories(String key) {
		Element el = this.m_cache.get(key);
		return (el == null ? null : ((ViewBuffered) el.getObjectValue()).getCategories());
	}

	/**
	 * 删除{@link ViewQuery}对应的视图数据缓存项。
	 * 
	 * @param viewQuery
	 */
	public void clearCache(ViewQuery viewQuery) {
		if (viewQuery == null) return;
		clearCache(viewQuery.getCacheKey());
	}

	/**
	 * 删除key对应的视图数据缓存项。
	 * 
	 * @param key
	 */
	public void clearCache(String key) {
		if (key == null) return;
		this.m_cache.remove(key);
	}

	/**
	 * 清除所有缓存的视图数据。
	 */
	public void clearCache() {
		this.m_cache.removeAll();
	}

	/**
	 * 关闭缓存。
	 */
	public void shutdown() {
		clearCache();
		this.m_cacheManager.shutdown();
		this.m_cacheManager = null;
	}
}

