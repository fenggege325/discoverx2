/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.View;
import com.tansuosoft.discoverx.model.ViewColumn;
import com.tansuosoft.discoverx.model.ViewCondition;
import com.tansuosoft.discoverx.model.ViewOrder;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.JVM;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 用于解析保存于数据库表中的文档信息的视图查询语句解析类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DocumentViewParser extends ViewParser {
	/**
	 * c_unid字段名常数。
	 */
	public static final String C_UNID = "c_unid";
	/**
	 * c_punid字段名常数。
	 */
	public static final String C_PUNID = "c_punid";
	/**
	 * c_oid字段名常数(保存所属单位安全编码的字段名)。
	 */
	public static final String C_OID = "c_oid";
	/**
	 * c_sort字段名常数。
	 */
	public static final String C_SORT = "c_sort";
	/**
	 * c_sort字段名常数。
	 */
	public static final String EQ = "=";
	/**
	 * 文档表及其用于关联的字段名。
	 */
	public static final StringPair T_DOCUMENT = new StringPair("t_document", C_UNID);
	/**
	 * 安全表及其用于关联的字段名。
	 */
	public static final StringPair T_SECURITY = new StringPair("t_security", C_PUNID);
	/**
	 * 流程表及其用于关联的字段名。
	 */
	public static final StringPair T_WFDATA = new StringPair("t_wfdata", C_PUNID);

	private List<String> parseTables = null; // 最终解析的表信息。
	private List<ParsedOrder> orders = null; // 最终解析的排序信息。
	private List<ParsedCondition> conditions = null; // 最终解析的条件信息。
	private List<ParsedCondition> conditionsWithoutJoin = null;// 不包含表关联的条件信息。
	private List<ParsedColumn> columns = null; // 最终解析的列信息
	private boolean hasDocumentTable = false; // 是否有文档表
	private boolean hasVTTable = false; // 是否有vt表
	private String vtTableName = null; // vt表表名，hasVTTable为true时，必须同时设置vtTable为有效值。

	/**
	 * 缺省构造器。
	 */
	protected DocumentViewParser() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param view
	 * @param session缺省构造器。
	 */
	protected DocumentViewParser(View view, Session session) {
		super(view, session);
	}

	/**
	 * 重载parseColumns
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewParser#parseColumns()
	 */
	public List<ParsedColumn> parseColumns() {
		if (this.columns == null) {
			this.columns = super.parseColumns();
			boolean foundUNIDColumn = false;
			if (this.columns != null) {
				for (ParsedColumn x : this.columns) { // 检查是否有可获取文档unid的列。
					ViewColumn c = x.getColumn();
					String tableName = c.getTableName();
					String columnName = c.getColumnName();
					if (tableName == null || tableName.length() == 0 || columnName == null || columnName.length() == 0) continue;
					if ((T_DOCUMENT.getKey()).equalsIgnoreCase(tableName) && C_UNID.equalsIgnoreCase(columnName)) {
						x.setUNIDColumnFlag(true);
						foundUNIDColumn = true;
						break;
					} else if (C_PUNID.equalsIgnoreCase(columnName) && (tableName.toLowerCase().startsWith(FormViewTable.FORM_VIEW_TABLE_PREFIX) || true)) {
						x.setUNIDColumnFlag(true);
						foundUNIDColumn = true;
						break;
					}
				}
				// 如果没有文档unid列，那么添加一个UNID列。
				if (!foundUNIDColumn && StringUtil.isBlank(this.getView().getRawQuery())) {
					ViewColumn unidColumn = new ViewColumn();
					unidColumn.setTableName(T_DOCUMENT.getKey());
					unidColumn.setColumnName(C_UNID);
					unidColumn.setColumnAlias(unidColumn.getColumnName());
					unidColumn.setLinkable(false);
					ParsedColumn parsedUnidColumn = new ParsedColumn(unidColumn);
					parsedUnidColumn.setUNIDColumnFlag(true);
					parsedUnidColumn.setDynamic(true);
					this.columns.add(0, parsedUnidColumn);
				}// if end
			}// if end

		}
		return this.columns;
	}

	/**
	 * 解析并返回不包含计算出来的表连接条件的所有条件集合。
	 * 
	 * @return List<ParsedCondition>
	 */
	protected List<ParsedCondition> parseConditionsWithoutJoin() {
		if (this.conditionsWithoutJoin == null) {
			View view = this.getView();
			Session session = this.getSession();
			// 处理固定条件
			String commonConditionName = view.getCommonCondition();
			if (commonConditionName != null && commonConditionName.length() > 0) {
				CommonCondition commonCondition = CommonConditionConfig.getInstance().getCommonCondition(commonConditionName);
				if (commonCondition != null) {
					List<ViewCondition> commonConditions = new ArrayList<ViewCondition>();

					// 1.先从继承的通用条件中获取。
					List<ViewCondition> inheritanceConditions = CommonCondition.getConditionsFromInheritance(commonCondition, view, session);
					if (inheritanceConditions != null) commonConditions.addAll(inheritanceConditions);

					// 2.再从本身获取
					String provider = commonCondition.getProvider();
					if (provider != null && provider.length() > 0) { // a)如果存在的话，先从本身搜索条件提供类中获取
						ConditionsProvider providerInstance = Instance.newInstance(provider, ConditionsProvider.class);
						if (providerInstance != null) {
							List<ViewCondition> providedContitions = providerInstance.provide(view, session);
							if (providedContitions != null && providedContitions.size() > 0) commonConditions.addAll(providedContitions);
						}
					} else { // b)最后从配置的内置条件项获取
						List<ViewCondition> builtinContitions = commonCondition.getConditions();
						if (builtinContitions != null && builtinContitions.size() > 0) commonConditions.addAll(builtinContitions);
					}

					if (commonConditions != null && commonConditions.size() > 0) {
						for (ViewCondition condition : commonConditions) {
							if (this.conditionsWithoutJoin == null) this.conditionsWithoutJoin = new ArrayList<ParsedCondition>();
							this.conditionsWithoutJoin.add(new ParsedCondition(condition));
						}// for end
					}// if end
				}// if end
			}// if end

			// 原有条件
			List<ParsedCondition> configConditions = super.parseConditions();
			if (configConditions != null) {
				if (this.conditionsWithoutJoin == null) this.conditionsWithoutJoin = new ArrayList<ParsedCondition>();
				this.conditionsWithoutJoin.addAll(configConditions);
			}
		}
		return this.conditionsWithoutJoin;
	}

	/**
	 * 重载parseConditions
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewParser#parseConditions()
	 */
	public List<ParsedCondition> parseConditions() {
		if (this.conditions != null) return this.conditions;
		this.conditions = new ArrayList<ParsedCondition>();

		// 先添加计算出来的表连接条件
		List<String> allTables = this.parseTables();
		if (allTables != null && allTables.size() > 1) {
			String joinTable = (this.hasDocumentTable ? T_DOCUMENT.getKey() : (this.hasVTTable ? this.vtTableName : null));
			String joinColumn = (this.hasDocumentTable ? C_UNID : C_PUNID); // C_PUNID<="punid"
			ViewCondition c = null;
			ParsedCondition pc = null;
			String column = null;

			if (joinTable != null && joinTable.length() > 0) {
				for (String x : allTables) {
					if (x == null || x.length() == 0) continue;
					if (x.equalsIgnoreCase(joinTable)) continue;
					if (x.startsWith(FormViewTable.FORM_VIEW_TABLE_PREFIX)) {
						if (this.hasVTTable && !x.equalsIgnoreCase(this.vtTableName)) continue;
					}
					column = (x.equalsIgnoreCase(T_DOCUMENT.getKey()) ? C_UNID : C_PUNID);
					c = new ViewCondition();
					c.setTableName(joinTable);
					c.setColumnName(joinColumn);
					c.setCompare(EQ);
					c.setRightValue(x + "." + column);
					c.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_AND);
					c.setNumericCompare(true);
					pc = new ParsedCondition(c);
					pc.setDynamic(true);
					this.conditions.add(pc);
				}// for end
				if (this.conditions.size() > 0) {
					this.conditions.get(0).getCondition().setLeftBracket(ViewCondition.LP); // 第一个连接条件左括号
					this.conditions.get(this.conditions.size() - 1).getCondition().setRightBracket(ViewCondition.RP); // 最后一个连接条件右括号
				}
			}// if end
		}// if end
			// 最后加上单位安全编码条件(如果是多单位使用的话)
		if (IsMultipleOrgs && !ignoreMultipleOrgs()) {
			ViewCondition coid = new ViewCondition();
			if (this.hasDocumentTable) {
				coid.setTableName(T_DOCUMENT.getKey());
			} else if (this.hasVTTable) {
				coid.setTableName(this.vtTableName);
			} else {
				throw new RuntimeException("找不到必须的表，您必须升级到独立版才能使用不受限制的查询语句！");
			}
			coid.setColumnName(C_OID);
			coid.setCompare(EQ);
			coid.setRightValue(OrganizationsContext.getInstance().getOrgSc(getSession()) + "");
			coid.setCombineNextWith(ViewCondition.CONDITION_COMBINATION_AND);
			coid.setNumericCompare(true);
			ParsedCondition pc = new ParsedCondition(coid);
			pc.setDynamic(true);
			conditions.add(pc);
		}
		// 然后再加上其它条件（固定查询条件、配置查询条件、动态追加的条件）
		List<ParsedCondition> conditionsWithoutJoin = this.parseConditionsWithoutJoin();
		if (conditionsWithoutJoin != null && conditionsWithoutJoin.size() > 0) this.conditions.addAll(conditionsWithoutJoin);

		return this.conditions;
	}

	/**
	 * 重载parseOrders
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewParser#parseOrders()
	 */
	public List<ParsedOrder> parseOrders() {
		if (this.orders == null) {
			this.orders = super.parseOrders();
			if (this.orders == null || this.orders.isEmpty()) {
				this.orders = new ArrayList<ParsedOrder>(1);
				ViewOrder o = new ViewOrder();
				o.setTableName(T_DOCUMENT.getKey());
				o.setColumnName(C_SORT);
				o.setOrderBy(ViewOrder.ORDERBY_DESC);
				ParsedOrder po = new ParsedOrder(o);
				po.setDynamic(true);
				this.orders.add(po);
			}
		}
		return this.orders;
	}

	/**
	 * 检查表名是否为文档表或视图查询表，如果是则置相应标记。
	 * 
	 * @param tableName String
	 */
	protected void checkTable(String tableName) {
		if (tableName == null || tableName.length() == 0) return;
		if (tableName.startsWith(FormViewTable.FORM_VIEW_TABLE_PREFIX) && !this.hasVTTable) {
			View v = this.getView();
			if (v != null && v.getTable() != null && v.getTable().toLowerCase().indexOf(tableName.substring(3)) >= 0) {
				this.hasVTTable = true;
				this.vtTableName = tableName;
				return;
			}
		}
		if (tableName.equalsIgnoreCase(T_DOCUMENT.getKey()) && !this.hasDocumentTable) {
			this.hasDocumentTable = true;
			return;
		}
	}

	/**
	 * 重载parseTables
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewParser#parseTables()
	 */
	public List<String> parseTables() {
		if (this.parseTables != null) { return this.parseTables; }

		List<String> allTables = new ArrayList<String>();

		// 默认配置的表单别名
		List<String> configTables = super.parseTables();
		if (configTables != null) {
			String vt = null;
			for (String x : configTables) {
				if (x == null || x.length() == 0) continue;
				if (JVM.isHostWindows()) x = x.toLowerCase();
				vt = FormViewTable.FORM_VIEW_TABLE_PREFIX + x;
				if (!allTables.contains(vt)) {
					allTables.add(vt);
					checkTable(vt);
				}
			}
		}

		List<ParsedColumn> allcolumns = this.parseColumns();
		List<ParsedCondition> allconditions = this.parseConditionsWithoutJoin();
		List<ParsedOrder> allorders = this.parseOrders();

		String tableName = null;

		// 列中的表
		if (allcolumns != null) {
			for (ParsedColumn parsedColumn : allcolumns) {
				if (parsedColumn == null) continue;
				tableName = parsedColumn.getColumn().getTableName();
				if (tableName == null || tableName.length() == 0) continue;
				if (JVM.isHostWindows()) tableName = tableName.toLowerCase();
				if (!allTables.contains(tableName)) {
					allTables.add(tableName);
					checkTable(tableName);
				}
			}
		}

		// 条件中的表
		if (allconditions != null) {
			for (ParsedCondition parsedCondition : allconditions) {
				if (parsedCondition == null) continue;
				tableName = parsedCondition.getCondition().getTableName();
				if (tableName == null || tableName.length() == 0) continue;
				if (JVM.isHostWindows()) tableName = tableName.toLowerCase();
				if (!allTables.contains(tableName)) {
					allTables.add(tableName);
					checkTable(tableName);
				}
			}
		}

		// 排序中的表
		if (allorders != null) {
			for (ParsedOrder parsedOrder : allorders) {
				if (parsedOrder == null) continue;
				tableName = parsedOrder.getOrder().getTableName();
				if (tableName == null || tableName.length() == 0) continue;
				if (JVM.isHostWindows()) tableName = tableName.toLowerCase();
				if (!allTables.contains(tableName)) {
					allTables.add(tableName);
					checkTable(tableName);
				}
			}
		}
		if (!allTables.isEmpty()) {
			this.parseTables = new ArrayList<String>(allTables);
		} else {
			this.parseTables = new ArrayList<String>();
		}
		return this.parseTables;
	}

}

