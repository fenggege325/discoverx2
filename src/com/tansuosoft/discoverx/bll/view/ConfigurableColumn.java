/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.view;

import com.tansuosoft.discoverx.model.ViewColumn;

/**
 * 用于描述视图配置时可使用的表字段信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ConfigurableColumn implements Cloneable {
	/**
	 * 缺省构造器。
	 */
	public ConfigurableColumn() {
	}

	private String m_columnName = null; // 表字段名，必须。
	private String m_columnTitle = null; // 表字段标题，必须。
	private boolean m_numeric = false; // 是否数字类型。
	private String m_dictionary = null; // 绑定的字典信息。

	/**
	 * 返回表字段名，必须。
	 * 
	 * @return String 表字段名，必须。
	 */
	public String getColumnName() {
		return this.m_columnName;
	}

	/**
	 * 设置表字段名，必须。
	 * 
	 * @param columnName String 表字段名，必须。
	 */
	public void setColumnName(String columnName) {
		this.m_columnName = columnName;
	}

	/**
	 * 返回表字段标题，必须。
	 * 
	 * @return String 表字段标题，必须。
	 */
	public String getColumnTitle() {
		return this.m_columnTitle;
	}

	/**
	 * 设置表字段标题，必须。
	 * 
	 * @param columnTitle String 表字段标题，必须。
	 */
	public void setColumnTitle(String columnTitle) {
		this.m_columnTitle = columnTitle;
	}

	/**
	 * 返回字段是否数字类型。
	 * 
	 * <p>
	 * 默认为false，表示文本类型。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getNumeric() {
		return this.m_numeric;
	}

	/**
	 * 设置字段是否数字类型。
	 * 
	 * @see ConfigurableColumn#getNumeric()
	 * @param numeric boolean
	 */
	public void setNumeric(boolean numeric) {
		this.m_numeric = numeric;
	}

	/**
	 * 返回绑定的字典信息。
	 * 
	 * <p>
	 * 在配置视图列{@link ViewColumn}时，自动为视图列提供的字典信息，格式请参考“{@link ViewColumn#getDictionary()}”中的说明。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDictionary() {
		return this.m_dictionary;
	}

	/**
	 * 设置绑定的字典信息。
	 * 
	 * <p>
	 * 在配置视图列{@link ViewColumn}时，自动为视图列提供的字典信息，格式请参考“{@link ViewColumn#getDictionary()}”中的说明。
	 * </p>
	 * 
	 * @param dictionary String
	 */
	public void setDictionary(String dictionary) {
		this.m_dictionary = dictionary;
	}

	/**
	 * 重载clone
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ConfigurableColumn x = new ConfigurableColumn();

		x.setNumeric(this.getNumeric());
		x.setColumnName(this.getColumnName());
		x.setColumnTitle(this.getColumnTitle());
		x.setDictionary(this.getDictionary());

		return x;
	}
}

