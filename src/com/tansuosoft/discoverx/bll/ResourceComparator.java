/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.util.Comparator;

import com.tansuosoft.discoverx.model.Resource;

/**
 * 按资源修改日期倒序排序的比较实现类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ResourceComparator implements Comparator<Resource> {

	/**
	 * 重载compare
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Resource o1, Resource o2) {
		if (o1 == null && o2 == null) return 0;
		if (o1 != null && o2 == null) return 1;
		if (o1 == null && o2 != null) return -1;
		String modified1 = o1.getModified();
		String modified2 = o2.getModified();
		int result = 0;
		if (modified1 == null && modified2 == null) result = 0;
		if (modified1 != null && modified2 == null) result = 1;
		if (modified1 == null && modified2 != null) result = -1;
		if (result == 0) result = modified1.compareToIgnoreCase(modified2);
		if (result == 0) result = o1.getUNID().compareToIgnoreCase(o2.getUNID());
		return result;
	}

}

