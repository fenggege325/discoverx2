/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.model.Authority;
import com.tansuosoft.discoverx.model.AuthorityEntry;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.ParameterValueDataType;
import com.tansuosoft.discoverx.model.Reference;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.Source;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 从通过http提交的Web字段值中提取并设置资源对应属性值的属性接收抽象类。
 * 
 * <p>
 * 一般情况下每一个种类类的资源对应一个属性接收实现类，比如表单配置提交时调用表单对应的属性接收类，应用程序配置提交时调用应用程序对应的属性接收类。
 * </p>
 * <p>
 * 构造资源对应的Web创建/修改表单时，原则上必须以资源可读写属性名作为Web表单字段名，比如{@link com.tansuosoft.discoverx.model.Application}。<br/>
 * 它的某一个可读写属性如：“getAlias/setAlias”在对应的Web配置表单中显示的字段名应该为"published"。<br/>
 * 其余具体要求规则，请参考具体实现类中的额外说明。
 * </p>
 * <p>
 * <strong>不要在开发时直接使用此类及其派生类，这些类由系统自动调用！</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ResourceReceiver {
	/**
	 * 缺省构造器。
	 */
	public ResourceReceiver() {
	}

	private HttpServletRequest m_httpRequest = null; // http 请求，必须。
	private Resource m_resource = null; // 要设置其属性值的资源，必须。
	private String m_prefix = null; // 参数名的固定前缀。

	/**
	 * 返回http 请求，必须。
	 * 
	 * @return HttpServletRequest http 请求，必须。
	 */
	public HttpServletRequest getHttpRequest() {
		return this.m_httpRequest;
	}

	/**
	 * 设置http 请求，必须。
	 * 
	 * @param httpRequest HttpServletRequest http 请求，必须。
	 */
	public void setHttpRequest(HttpServletRequest httpRequest) {
		this.m_httpRequest = httpRequest;
	}

	/**
	 * 返回要设置其属性值的资源，必须。
	 * 
	 * <p>
	 * 调用receive()之后，才能获取到更新了属性值的资源。
	 * </p>
	 * 
	 * @return Resource 要设置其属性值的资源，必须。
	 */
	public Resource getResource() {
		return this.m_resource;
	}

	/**
	 * 设置要设置其属性值的资源，必须。
	 * 
	 * @param resource Resource 要设置其属性值的资源，必须。
	 */
	public void setResource(Resource resource) {
		this.m_resource = resource;
	}

	/**
	 * 设置参数名前缀。
	 * 
	 * @param prefix
	 */
	public void setPrefix(String prefix) {
		this.m_prefix = prefix;
	}

	/**
	 * 获取参数名前缀。
	 * 
	 * @return
	 */
	protected String getPrefix() {
		return (this.m_prefix == null ? "" : this.m_prefix);
	}

	/**
	 * 获取HttpServletRequest中name指定的参数名对应的参数值。
	 * 
	 * <p>
	 * 如果参数值有多个，则仅返回多值中的第一个值。
	 * </p>
	 * 
	 * @param name 参数名。
	 * @param defaultValue 异常情况下返回的默认值。
	 * @return String 返回参数名对应的参数值，如果没有对应参数值、参数名为null、参数名为空字符串等，则返回defaultValue。
	 */
	protected String getRequestParameterValueString(String name, String defaultValue) {
		String param = this.m_httpRequest.getParameter(name);
		return (param != null ? param : defaultValue);
	}

	/**
	 * 获取HttpServletRequest中name指定的参数名对应的参数值。
	 * 
	 * @param name 参数名。
	 * @param delimiter 多值分隔符。
	 * @param defaultValue 异常情况下返回的默认值。
	 * @return String 返回参数名对应的参数值（多值用delimiter分隔），如果没有对应参数值、参数名为null、参数名为空字符串等，则返回defaultValue。
	 */
	protected String getRequestParameterValueString(String name, String delimiter, String defaultValue) {
		String[] params = this.m_httpRequest.getParameterValues(name);
		if (params == null || params.length == 0) return defaultValue;
		if (params.length == 1) return params[0];
		StringBuilder sb = new StringBuilder();
		for (String s : params) {
			if (s == null) s = "";
			sb.append(sb.length() == 0 ? "" : delimiter).append(s);
		}
		return sb.toString();
	}

	/**
	 * 获取HttpServletRequest中name指定的参数名对应的int型参数值。
	 * 
	 * <p>
	 * 如果参数值有多个，则仅返回多值中的第一个值。
	 * </p>
	 * 
	 * @param name 参数名。
	 * @param defaultValue 异常情况下返回的默认值。
	 * @return String 返回参数名对应的参数值，如果没有对应参数值、参数名为null、参数名为空字符串等，则返回defaultValue。
	 */
	protected int getRequestParameterValueInt(String name, int defaultValue) {
		String val = this.getRequestParameterValueString(name, null);
		return StringUtil.getValueInt(val, defaultValue);
	}

	/**
	 * 获取HttpServletRequest中name指定的参数名对应的boolean型参数值。
	 * 
	 * <p>
	 * 如果参数值有多个，则仅返回多值中的第一个值。
	 * </p>
	 * 
	 * @param name 参数名。
	 * @param defaultValue 异常情况下返回的默认值。
	 * @return String 返回参数名对应的参数值，如果没有对应参数值、参数名为null、参数名为空字符串等，则返回defaultValue。
	 */
	protected boolean getRequestParameterValueBoolean(String name, boolean defaultValue) {
		String val = this.getRequestParameterValueString(name, null);
		return StringUtil.getValueBool(val, defaultValue);
	}

	/**
	 * 获取HttpServletRequest中name指定的参数名对应的Enum（枚举）型参数值。
	 * 
	 * <p>
	 * 如果参数值有多个，则仅返回多值中的第一个值。
	 * </p>
	 * 
	 * @param name 参数名。
	 * @param cls Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultValue 如果没有值或无法转换时返回的默认结果。
	 * @return T 指定类型的枚举值。
	 */
	protected <T> T getRequestParameterValueEnum(String name, Class<T> cls, T defaultValue) {
		String val = this.getRequestParameterValueString(name, null);
		return StringUtil.getValueEnum(val, cls, defaultValue);
	}

	/**
	 * 获取HttpServletRequest中name指定的参数名对应的多个参数值的数组。
	 * 
	 * @param name 参数名。
	 * @return String 返回参数名对应的参数值数组，如果没有对应参数值、参数名为null、参数名为空字符串等，则返回null。
	 */
	protected String[] getRequestParameterValueStrings(String name) {
		String[] param = this.m_httpRequest.getParameterValues(name);
		return (param != null && param.length > 0) ? param : null;
	}

	/**
	 * 接收并设置所有资源通用的属性值({@link Resource}的基础属性值)。
	 * 
	 * <p>
	 * 通过与属性值相同的请求参数名获取参数值，如{@link Resource#getName()}/{@link Resource#setName(String)}对应的请求参数名为“name”。
	 * </p>
	 */
	protected void receiveResourceCommon() {
		if (this.m_resource == null || this.m_httpRequest == null) return;
		String prefix = this.getPrefix();
		this.m_resource.setName(this.getRequestParameterValueString(prefix + "name", null));
		String alias = this.getRequestParameterValueString(prefix + "alias", null);
		if (alias != null && alias.length() > 0) this.m_resource.setAlias(alias);
		this.m_resource.setUNID(this.getRequestParameterValueString(prefix + "unid", null));
		this.m_resource.setSort(this.getRequestParameterValueInt(prefix + "sort", 0));
		this.m_resource.setCategory(this.getRequestParameterValueString(prefix + "category", null));
		this.m_resource.setCreated(this.getRequestParameterValueString(prefix + "created", new DateTime().toString()));
		this.m_resource.setCreator(this.getRequestParameterValueString(prefix + "creator", null));
		this.m_resource.setDescription(this.getRequestParameterValueString(prefix + "description", null));
		this.m_resource.setModified(this.getRequestParameterValueString(prefix + "modified", new DateTime().toString()));
		this.m_resource.setModifier(this.getRequestParameterValueString(prefix + "modifier", null));
		this.m_resource.setPUNID(this.getRequestParameterValueString(prefix + "punid", null));
		this.m_resource.setSelectable(this.getRequestParameterValueBoolean(prefix + "selectable", true));
		this.m_resource.setSource(Source.parse(this.getRequestParameterValueInt(prefix + "source", Source.UserDefine.getIntValue())));
	}

	/**
	 * 根据参数列表填充包含每个请求参数对应参数值列表的列表并返回。
	 * 
	 * @param params String[]
	 * @return List&lt;List&lt;String&gt;&gt;
	 */
	protected List<List<String>> fillListString(String params[]) {
		List<List<String>> vals = null;
		String strs[] = null;
		List<String> strvals = null;
		String prefix = this.getPrefix();
		List<String> allStrs = null;
		int rows = -1;
		int columns = params.length;
		for (int i = 0; i < params.length; i++) {
			strs = this.getRequestParameterValueStrings(prefix + params[i]);
			if (strs == null || strs.length == 0) {
				if (rows < 0) {
					return null;
				} else {
					throw new RuntimeException("接收属性时发现“" + params[i] + "”的结果个数和“" + params[i - 1] + "”的结果个数不符！");
				}
			}
			if (rows > 0 && rows != strs.length) throw new RuntimeException("接收属性时发现“" + params[i] + "”的结果个数和“" + params[i - 1] + "”的结果个数不符！");
			if (vals == null) vals = new ArrayList<List<String>>(strs.length);
			if (allStrs == null) allStrs = new ArrayList<String>(strs.length * columns);// new String[strs.length * params.length];
			for (String x : strs) {
				allStrs.add(x);
			}
			rows = strs.length;
		}
		int idx = 0;
		for (int i = 0; i < rows; i++) {
			strvals = new ArrayList<String>(columns);
			for (int j = 0; j < columns; j++) {
				idx = i + j * rows;
				strvals.add(allStrs.get(idx));
			}
			vals.add(strvals);
		}
		if (allStrs != null) allStrs.clear();
		return vals;
	}

	/**
	 * 接收并设置所有资源安全控制信息。
	 * 
	 * <p>
	 * 构造{@link Security}及其包含的{@link SecurityEntry}并调用{@link Resource#setSecurity(Security)}设置结果。
	 * </p>
	 * <p>
	 * 请求参数名及其注意事项说明：<br/>
	 * <list>
	 * <li>securityCodes参数值对应的每一个结果作为{@link SecurityEntry#setSecurityCode(int)}的属性值设置</li>
	 * <li>securityLevels参数值对应的每一个结果作为{@link SecurityEntry#setSecurityLevel(int)}的属性值设置</li>
	 * <li>workflowLevels参数值对应的每一个结果作为{@link SecurityEntry#setWorkflowLevel(int)}的属性值设置</li>
	 * </list> <br/>
	 * 每个请求参数的结果必须为数组，并且每个数组的个数必须一致，数组中每一个结果必须可以转换为对应的属性值类型。<br/>
	 * 不满足以上要求时则抛出运行时异常。
	 * </p>
	 */
	protected void receiveResourceSecurity() {
		if (this.m_resource == null || this.m_httpRequest == null) return;

		String params[] = { "security_securityCode", "security_securityLevel", "security_workflowLevel" };
		Security s = this.receiveSecurity(params);

		this.m_resource.setSecurity(s);
	}

	/**
	 * 接收并设置所有资源额外参数信息。
	 * 
	 * <p>
	 * 构造资源包含的{@link Parameter}并调用{@link Resource#setParameters(List)}设置结果。
	 * </p>
	 * <p>
	 * 请求参数名及其注意事项说明：<br/>
	 * <list>
	 * <li>param_names参数值对应的每一个结果作为{@link Parameter#setName(String)}的属性值设置</li>
	 * <li>param_values参数值对应的每一个结果作为{@link Parameter#setValue(String)}的属性值设置</li>
	 * <li>param_valueTypes参数值对应的每一个结果作为{@link Parameter#setValueType(ParameterValueDataType)}的属性值设置</li>
	 * <li>param_descriptions参数值对应的每一个结果作为{@link Parameter#setDescription(String)}的属性值设置</li>
	 * <li>param_multiples参数值对应的每一个结果作为{@link Parameter#setMultiple(boolean)}的属性值设置</li>
	 * <li>param_delimiters参数值对应的每一个结果作为{@link Parameter#setDelimiter(String)}的属性值设置</li>
	 * </list> <br/>
	 * 每个请求参数的结果必须为数组，并且每个数组的个数必须一致，数组中每一个结果必须可以转换为对应的属性值类型。<br/>
	 * 不满足以上要求时则抛出运行时异常。
	 * </p>
	 */
	protected void receiveResourceParameter() {
		if (this.m_resource == null || this.m_httpRequest == null) return;

		List<Parameter> ps = this.m_resource.getParameters();
		if (ps != null) ps.clear();

		String params[] = { "parameters_name", "parameters_value", "parameters_valueType", "parameters_description", "parameters_multiple", "parameters_delimiter" };
		Parameter p = null;

		List<List<String>> vals = this.fillListString(params);
		if (vals == null) return;
		String name = null;
		String val = null;
		ParameterValueDataType valType = null;
		String description = null;
		boolean multiple = false;
		String delimiter = null;
		for (List<String> x : vals) {
			name = StringUtil.getValueString(x.get(0), null);
			if (name == null || name.length() == 0) continue;
			val = StringUtil.getValueString(x.get(1), null);
			if (val == null || val.length() == 0) val = "";
			valType = ParameterValueDataType.parse(StringUtil.getValueInt(x.get(2), ParameterValueDataType.Text.getIntValue()));
			// (ParameterValueDataType) StringUtil.getValueEnum(x.get(2), ParameterValueDataType.class, ParameterValueDataType.Text);
			description = StringUtil.getValueString(x.get(3), null);
			multiple = StringUtil.getValueBool(x.get(4), false);
			delimiter = StringUtil.getValueString(x.get(5), null);

			p = new Parameter(this.m_resource.getUNID(), name, val);
			p.setDescription(description);
			p.setValueType(valType);
			p.setMultiple(multiple);
			p.setDelimiter(delimiter);
			if (ps == null) {
				ps = new ArrayList<Parameter>();
				this.m_resource.setParameters(ps);
			}
			ps.add(p);
		}
		if (ps != null && ps.size() > 0) {
			Comparator<Parameter> c = new ParameterComparator();
			Collections.sort(ps, c);
		}
	}

	/**
	 * 接收事件处理程序（{@link EventHandlerInfo}）信息并返回其列表。
	 * 
	 * <p>
	 * 请求参数名及其注意事项说明：<br/>
	 * <list>
	 * <li>eventHandlers参数值对应的每一个结果作为{@link EventHandlerInfo#setEventHandler(String)}的属性值设置</li>
	 * <li>eventHandlerNames参数值对应的每一个结果作为{@link EventHandlerInfo#setEventHandlerName(String)}的属性值设置</li>
	 * <li>eventHandlerTypes参数值对应的每一个结果作为{@link EventHandlerInfo#setEventHandlerType(String)}的属性值设置</li>
	 * <li>eventHandlerSorts参数值对应的每一个结果作为{@link EventHandlerInfo#setSort(int)}的属性值设置</list> <br/>
	 * 每个请求参数的结果必须为数组，并且每个数组的个数必须一致，数组中每一个结果必须可以转换为对应的属性值类型。<br/>
	 * 不满足以上要求时则抛出运行时异常。
	 * </p>
	 * 
	 * @return
	 */
	protected List<EventHandlerInfo> receiveEventHandlers() {
		if (this.m_resource == null || this.m_httpRequest == null) return null;

		List<EventHandlerInfo> result = null;

		String params[] = { "eventHandlers_eventHandler", "eventHandlers_eventHandlerName", "eventHandlers_eventHandlerType", "eventHandlers_sort" };
		List<List<String>> vals = this.fillListString(params);
		if (vals == null || vals.isEmpty()) return null;
		String eventHandler = null;
		String eventHandlerName = null;
		String eventHandlerType = null;
		int sort = 0;
		EventHandlerInfo ehi = null;
		for (List<String> x : vals) {
			eventHandler = StringUtil.getValueString(x.get(0), null);
			if (eventHandler == null || eventHandler.length() == 0) continue;
			eventHandlerName = StringUtil.getValueString(x.get(1), null);
			if (eventHandlerName == null || eventHandlerName.length() == 0) eventHandlerName = eventHandler;
			eventHandlerType = StringUtil.getValueString(x.get(2), null);
			if (eventHandlerType == null || eventHandlerType.length() == 0) continue;
			sort = StringUtil.getValueInt(x.get(3), 0);
			ehi = new EventHandlerInfo(eventHandler, eventHandlerType, eventHandlerName, sort);
			if (result == null) result = new ArrayList<EventHandlerInfo>();
			result.add(ehi);
		}
		return result;
	}

	/**
	 * 根据params中指定的参数名填充安全控制信息({@link Security})并返回。 。
	 * 
	 * <p>
	 * 构造{@link Security}及其包含的{@link SecurityEntry}并调用{@link Resource#setSecurity(Security)}设置结果。
	 * </p>
	 * <p>
	 * params参数规则：<br/>
	 * <list>
	 * <li>params中第一个参数名对应参数值对应的每一个结果作为{@link SecurityEntry#setSecurityCode(int)}的属性值设置</li>
	 * <li>params中第一个参数名对应参数值对应的每一个结果作为{@link SecurityEntry#setSecurityLevel(int)}的属性值设置</li>
	 * <li>params中第一个参数名对应参数值对应的每一个结果作为{@link SecurityEntry#setWorkflowLevel(int)}的属性值设置</li>
	 * </list> <br/>
	 * 参数顺序不能颠倒，每个请求参数的结果必须为数组，并且每个数组的个数必须一致，数组中每一个结果必须可以转换为对应的属性值类型。<br/>
	 * 不满足以上要求时则抛出运行时异常。
	 * </p>
	 * 
	 * @param params String[]
	 * @return Security
	 */
	protected Security receiveSecurity(String[] params) {
		if (this.m_resource == null || this.m_httpRequest == null) return null;

		String punid = this.m_resource.getUNID();

		Security s = new Security(punid);

		List<SecurityEntry> ses = s.getSecurityEntries();
		if (ses != null) ses.clear();

		SecurityEntry se = null;

		List<List<String>> vals = this.fillListString(params);
		if (vals == null || vals.isEmpty()) return s;
		int securityCode = 0;
		int securityLevel = 0;
		int workflowLevel = 0;
		for (List<String> x : vals) {
			securityCode = StringUtil.getValueInt(x.get(0), 0);
			if (securityCode == 0) continue;
			securityLevel = StringUtil.getValueInt(x.get(1), 0);
			workflowLevel = StringUtil.getValueInt(x.get(2), 0);
			if (securityLevel == 0 && workflowLevel == 0) continue;
			se = new SecurityEntry(punid, securityCode, securityLevel, workflowLevel);
			if (ses == null) {
				ses = new ArrayList<SecurityEntry>();
				s.setSecurityEntries(ses);
			}
			ses.add(se);
		}
		return s;
	}

	/**
	 * 根据params中指定的参数名和directory指定的资源目录信息填充{@link Reference}集合并返回之。
	 * 
	 * <p>
	 * params参数规则：<br/>
	 * <list>
	 * <li>params中第一个参数名对应参数值对应的每一个结果作为{@link Reference#setUnid(String)}的属性值设置</li>
	 * <li>params中第二个参数名对应参数值对应的每一个结果作为{@link Reference#setTitle(String)}的属性值设置</li>
	 * <li>params中第三个参数名对应参数值对应的每一个结果作为{@link Reference#setSort(int)}的属性值设置</li>
	 * </list><br/>
	 * 参数顺序不能颠倒，每个请求参数的结果必须为数组，并且每个数组的个数必须一致，数组中每一个结果必须可以转换为对应的属性值类型。<br/>
	 * 不满足以上要求时则抛出运行时异常。
	 * </p>
	 * 
	 * @param params
	 * @param directory
	 * @return List&lt;Reference&gt;
	 */
	protected List<Reference> receiveReferences(String[] params, String directory) {
		if (this.m_resource == null || this.m_httpRequest == null) return null;

		List<List<String>> vals = this.fillListString(params);
		if (vals == null || vals.isEmpty()) return null;
		List<Reference> result = null;

		String unid = null;
		String title = null;
		int sort = 0;
		Reference ref = null;
		for (List<String> x : vals) {
			unid = StringUtil.getValueString(x.get(0), null);
			if (unid == null || unid.length() == 0) continue;
			title = StringUtil.getValueString(x.get(1), null);
			sort = StringUtil.getValueInt(x.get(2), 0);
			ref = new Reference(this.m_resource.getUNID(), title, directory, sort);
			if (result == null) result = new ArrayList<Reference>();
			result.add(ref);
		}
		return result;
	}

	/**
	 * 根据params中指定的参数名信息填充{@link Authority}及其包含的{@link AuthorityEntry}并返回之。
	 * 
	 * <p>
	 * params参数规则：<br/>
	 * <list>
	 * <li>params中第一个参数名对应参数值对应的每一个结果作为{@link AuthorityEntry#setDirectory(String)}的属性值设置</li>
	 * <li>params中第二个参数名对应参数值对应的每一个结果作为{@link AuthorityEntry#setName(String)}的属性值设置</li>
	 * <li>params中第三个参数名对应参数值对应的每一个结果作为{@link AuthorityEntry#setSecurityCode(int)}的属性值设置</li>
	 * <li>params中第三个参数名对应参数值对应的每一个结果作为{@link AuthorityEntry#setUNID(String)}的属性值设置</li>
	 * </list><br/>
	 * 参数顺序不能颠倒，每个请求参数的结果必须为数组，并且每个数组的个数必须一致，数组中每一个结果必须可以转换为对应的属性值类型。<br/>
	 * 不满足以上要求时则抛出运行时异常。
	 * </p>
	 * 
	 * @param params
	 * @return Authority
	 */
	protected Authority receiveAuthority(String[] params) {
		if (this.m_resource == null || this.m_httpRequest == null) return null;

		Authority auth = new Authority(this.m_resource.getUNID());
		List<AuthorityEntry> result = null;
		List<List<String>> vals = this.fillListString(params);
		if (vals == null || vals.isEmpty()) return auth;
		AuthorityEntry ae = null;
		for (List<String> x : vals) {
			ae = new AuthorityEntry();
			ae.setDirectory(x.get(0));
			ae.setName(x.get(1));
			ae.setSecurityCode(StringUtil.getValueInt(x.get(2), 0));
			if (ae.getSecurityCode() == 0) continue;
			ae.setUNID(x.get(3));
			if (ae.getUNID() == null || ae.getUNID().length() == 0) continue;
			if (result == null) result = new ArrayList<AuthorityEntry>();
			result.add(ae);
		}
		auth.setAuthorityEntries(result);
		return auth;
	}

	/**
	 * 根据params中指定的参数名信息填充{@link com.tansuosoft.discoverx.model.Operation}集合并返回之。
	 * 
	 * <p>
	 * params参数规则：<br/>
	 * <list>
	 * <li>params中第一个参数名对应参数值对应的每一个结果作为{@link com.tansuosoft.discoverx.model.Operation#setExpression(String)}的属性值设置</li>
	 * <li>params中第二个参数名对应参数值对应的每一个结果作为{@link com.tansuosoft.discoverx.model.Operation#setVisibleExpression(String)}的属性值设置</li>
	 * <li>params中第三个参数名对应参数值对应的每一个结果作为{@link com.tansuosoft.discoverx.model.Operation#setTitle(String)}的属性值设置</li>
	 * <li>params中第四个参数名对应参数值对应的每一个结果作为{@link com.tansuosoft.discoverx.model.Operation#setIcon(String)}的属性值设置</li>
	 * <li>params中第五个参数名对应参数值对应的每一个结果作为{@link com.tansuosoft.discoverx.model.Operation#setSort(int)}的属性值设置</li>
	 * </list><br/>
	 * 参数顺序不能颠倒，每个请求参数的结果必须为数组，并且每个数组的个数必须一致，数组中每一个结果必须可以转换为对应的属性值类型。<br/>
	 * 不满足以上要求时则抛出运行时异常。
	 * </p>
	 * 
	 * @param params
	 * @return
	 */
	protected List<Operation> receiveOperations(String[] params) {
		if (this.m_resource == null || this.m_httpRequest == null) return null;

		List<List<String>> vals = this.fillListString(params);
		if (vals == null || vals.isEmpty()) return null;
		List<Operation> result = null;

		String expression = null;
		String visibleExpression = null;
		String title = null;
		String icon = null;
		int sort = 0;
		// String runtimeParameters = null;
		Operation opr = null;
		for (List<String> x : vals) {
			expression = StringUtil.getValueString(x.get(0), null);
			if (expression == null || expression.length() == 0) continue;
			visibleExpression = StringUtil.getValueString(x.get(1), null);
			title = StringUtil.getValueString(x.get(2), null);
			icon = StringUtil.getValueString(x.get(3), null);
			sort = StringUtil.getValueInt(x.get(4), 0);
			opr = new Operation();
			opr.setExpression(expression);
			opr.setVisibleExpression(visibleExpression);
			opr.setTitle(title);
			opr.setIcon(icon);
			opr.setSort(sort);
			if (result == null) result = new ArrayList<Operation>();
			result.add(opr);
		}
		return result;
	}

	/**
	 * 通过反射自动从请求参数中设置对象对应属性。
	 * 
	 * <p>
	 * 将x对应的对象中可读写属性的属性名分别加上前缀({@link ResourceReceiver#getPrefix()})作为请求参数名。<br/>
	 * 通过此参数名获取参数值，如果存在，则设置对应的属性结果（调用对应setter）。<br/>
	 * 此方法只适合设置基础属性（包括基本内置原始数据类型、字符串、系统枚举类型等）参数值。
	 * </p>
	 * 
	 * @param x
	 */
	protected void setPropertiesByReflect(Object x) {
		if (x == null) return;
		String prefix = this.getPrefix();
		String paramName = null;
		Map<Method, Method> map = ObjectUtil.getInstanceRWPropMethods(x);
		if (map == null || map.isEmpty()) return;
		String propName = null;
		String paramValue = null;
		Class<?> cls = null;
		Enum<?> enumx = null;
		Method set = null;
		for (Method m : map.keySet()) {
			propName = StringUtil.toCamelCase(m.getName().substring(3));
			paramName = String.format("%1$s%2$s", prefix, propName);
			paramValue = this.getRequestParameterValueString(paramName, null);
			if (paramValue == null) return;
			cls = m.getReturnType();
			set = map.get(m);
			try {
				if (cls.isEnum()) {
					enumx = StringUtil.getValueEnumCommon(paramValue, cls, (Enum<?>) cls.getEnumConstants()[0]);
					set.invoke(x, enumx);
				} else {
					set.invoke(x, ObjectUtil.getResult(cls, paramValue));
				}
			} catch (IllegalArgumentException e) {
				FileLogger.error(e);
			} catch (IllegalAccessException e) {
				FileLogger.error(e);
			} catch (InvocationTargetException e) {
				FileLogger.error(e);
			}
		}// for end
	}

	/**
	 * 接收不同类型的资源对应的差异性属性结果。
	 * 
	 * <p>
	 * <strong>必须实现此方法以便用于不同类型资源的差异性属性接收。</strong>
	 * </p>
	 */
	protected abstract void receiveResourceDifference();

	/**
	 * 用HttpServletRequest提交的字段内容设置资源对应的属性值。
	 * 
	 * <p>
	 * 此方法依次调用{@link ResourceReceiver#receiveResourceSecurity()}、{@link ResourceReceiver#receiveResourceCommon()}<br/>
	 * {@link ResourceReceiver#receiveResourceDifference()}、{@link ResourceReceiver#receiveResourceParameter()}
	 * </p>
	 */
	public void receive() {
		this.receiveResourceSecurity();
		this.receiveResourceCommon();
		this.receiveResourceDifference();
		this.receiveResourceParameter();
	}

}

