/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import com.tansuosoft.discoverx.bll.document.DocumentConstructor;
import com.tansuosoft.discoverx.bll.resource.DefaultResourceConstructor;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 根据资源目录提供资源构造对象实例的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceConstructorProvider {

	/**
	 * 根据资源目录提供资源构造对象实例。
	 * 
	 * @param dir String 资源目录。
	 * @return
	 */
	public static ResourceConstructor getResourceConstructor(String dir) {
		if (dir == null || dir.length() == 0) return null;

		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(dir);
		if (rd == null) throw new RuntimeException("找不到对应资源描述信息！");
		String cfgClassName = rd.getConstructor();
		if (cfgClassName != null && cfgClassName.length() > 0) {
			ResourceConstructor obj = Instance.newInstance(cfgClassName, ResourceConstructor.class);
			if (obj != null) return obj;
		}

		String docdir = ResourceDescriptorConfig.getInstance().getResourceDirectory(Document.class);
		if (docdir == null) return null;
		boolean isDocument = (dir.equalsIgnoreCase(docdir));
		if (isDocument) {
			return new DocumentConstructor();
		} else {
			return new DefaultResourceConstructor();
		}
	}

	/**
	 * 根据资源类信息提供资源构造对象实例。
	 * 
	 * @param clazz
	 * @return
	 */
	public static ResourceConstructor getResourceConstructor(Class<?> clazz) {
		return getResourceConstructor(ResourceDescriptorConfig.getInstance().getResourceDirectory(clazz));
	}
}

