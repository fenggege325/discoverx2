/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll;

import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.serialization.Deserializer;

/**
 * 根据资源类型提供资源反序列化对象实例的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ResourceDeserializerProvider {

	/**
	 * 根据资源类型提供资源反序列化对象实例。
	 * 
	 * @param cls
	 * @return
	 */
	public static Deserializer getResourceDeserializer(Class<?> cls) {
		if (cls == null) return null;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(cls);
		if (rd == null) throw new RuntimeException("找不到对应资源描述信息！");
		String cfgClassName = rd.getDeserializer();
		if (cfgClassName != null && cfgClassName.length() > 0) {
			Deserializer obj = Instance.newInstance(cfgClassName, Deserializer.class);
			if (obj != null) return obj;
		} else {
			throw new RuntimeException("资源描述中没有获取到有效反序列化类信息！");
		}
		return null;
	}
}

