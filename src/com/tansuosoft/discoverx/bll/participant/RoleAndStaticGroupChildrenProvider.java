/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.participant;

import java.util.List;

import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.impl.ChildParticipantsOfRoleOrGroupFetcher;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;

/**
 * 返回角色或静态群组对应的下级参与者的参与者提供实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class RoleAndStaticGroupChildrenProvider implements ParticipantsProvider {

	/**
	 * 重载provide。
	 * 
	 * <p>
	 * bindResource必须为对应的角色或者静态群组资源对象。
	 * </p>
	 * <p>
	 * 如果bindResource是静态群组，那么返回属于此群组的用户参与者。<br/>
	 * 如果bindResource是角色，那么返回属于此角色的用户参与者和群组参与者。<br/>
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.participant.ParticipantsProvider#provide(com.tansuosoft.discoverx.model.Session)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Participant> provide(Session session, Resource bindResource) {
		if (bindResource == null) return null;
		DBRequest dbr = new ChildParticipantsOfRoleOrGroupFetcher();
		dbr.setParameter(DBRequest.PUNID_PARAM_NAME, bindResource.getUNID());
		dbr.setParameter(DBRequest.UNID_PARAM_NAME, OrganizationsContext.getInstance().getOrgUnid(session));
		dbr.sendRequest();
		List<Participant> list = dbr.getResult(List.class);
		return list;
	}
}

