/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.participant;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 返回当前打开的文档作者的参与者提供实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AuthorProvider implements ParticipantsProvider {

	/**
	 * 重载provide：如果正常，则返回包含一个文档作者对应参与者信息的列表，否则返回null。
	 * 
	 * @see com.tansuosoft.discoverx.bll.participant.ParticipantsProvider#provide(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public List<Participant> provide(Session session, Resource bindResource) {
		if (session == null) {
			FileLogger.debug("无法获取当前用户自定义会话信息！");
			return null;
		}
		Document res = session.getLastDocument();
		if (res == null) {
			FileLogger.debug("无法获取当前打开的文档！");
			return null;
		}
		String author = res.getCreator();
		ParticipantTree root = ParticipantTreeProvider.getInstance().getRoot();
		ParticipantTree result = ParticipantTreeProvider.getParticipantTree(root, author);
		if (result != null) {
			List<Participant> ret = new ArrayList<Participant>(1);
			ret.add(result);
			return ret;
		}
		return null;
	}
}

