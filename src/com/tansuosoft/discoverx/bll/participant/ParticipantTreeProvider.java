/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.participant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.dao.DAOConfig;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.GroupType;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 提供系统所有参与者树信息及相关操作的类。
 * 
 * @author coca@tansuosoft.cn
 */
public final class ParticipantTreeProvider {
	private static final Map<Integer, ParticipantTreeProvider> m_instances = new HashMap<Integer, ParticipantTreeProvider>(OrganizationsContext.getInstance().getOrgsCount() + 100); // 唯一实例.
	private static final Object m_lock = new Object();

	private ParticipantTree m_root = null;
	private int m_participantCount = 0; // 系统有效参与者总数。
	private Map<Integer, Integer[]> m_mapOfSecurityCodeAndIndex = null; // 参与者安全编码与其在参与者根节点树中的位置索引数组一一对应的Map集合。
	private Map<String, Integer> m_mapOfUnidAndSecurityCode = null; // 参与者unid与安全编码索引一一对应的Map集合。
	private Map<String, Integer> m_mapOfAliasAndSecurityCode = null; // 参与者别名与安全编码索引一一对应的Map集合。
	private Map<String, Integer> m_mapOfUserNameAndSecurityCode = null; // 人员参与者全名与安全编码索引一一对应的Map集合。
	private Map<Integer, Integer[]> m_mapOfRGSCAndPSC = null; // 角色和静态群组安全编码与其包含的用户安全编码数组一一对应的Map集合。
	private List<ParticipantTree> m_units = null;

	/**
	 * 同步安全编码-位置索引、unid-安全编码、别名-安全编码等对应的Map缓存。
	 * 
	 * @param parent
	 * @param parentIndex
	 */
	private void syncMap(ParticipantTree parent, List<Integer> parentIndex) {
		if (parent == null) return;
		List<ParticipantTree> list = parent.getChildren();
		m_mapOfUnidAndSecurityCode.put(parent.getUNID(), parent.getSecurityCode());
		m_mapOfAliasAndSecurityCode.put(parent.getAlias(), parent.getSecurityCode());
		if (parent.getParticipantType() == ParticipantType.Person) m_mapOfUserNameAndSecurityCode.put(parent.getName(), parent.getSecurityCode());
		if (list != null && !list.isEmpty()) {
			Integer[] arr = null;

			int idx = 0;
			for (ParticipantTree x : list) {
				if (parentIndex == null) {
					parentIndex = new ArrayList<Integer>();
					arr = new Integer[1];
				} else {
					arr = new Integer[parentIndex.size() + 1];
					parentIndex.toArray(arr);
				}
				arr[arr.length - 1] = idx;
				parentIndex.add(idx);
				this.m_mapOfSecurityCodeAndIndex.put(x.getSecurityCode(), arr);
				syncMap(x, parentIndex);
				parentIndex.remove(parentIndex.size() - 1);
				idx++;
			}
		}
	}

	/**
	 * 接收根组织UNID的构造器。
	 * 
	 * @param punid
	 */
	private ParticipantTreeProvider(String punid) {
		DBRequest dbr = DAOConfig.getInstance().getDBRequest("ParticipantTreeFetcher");
		if (dbr == null) throw new RuntimeException("DAOConfig中必须配置名为“ParticipantTreeFetcher”的数据库请求实现类且能够正常初始化。");
		dbr.setParameter(DBRequest.PUNID_PARAM_NAME, punid);
		dbr.sendRequest();
		m_root = dbr.getResult(ParticipantTree.class);
		if (m_root == null) throw new RuntimeException("无法获取参与者信息。");

		// 保存通过根节点的sort传递的参与者总数信息
		m_participantCount = m_root.getSort();
		m_root.setSort(0);

		// 加入固定角色
		if (!OrganizationsContext.getInstance().isInTenantContext()) {
			ParticipantTree ptAdmin = new ParticipantTree(Role.getAdmin());
			this.m_root.addChild(ptAdmin);
			ParticipantTree ptDocumentAdmin = new ParticipantTree(Role.getDocumentAdmin());
			this.m_root.addChild(ptDocumentAdmin);
		}
		ParticipantTree ptSettingsAdmin = new ParticipantTree(Role.getSettingsAdmin());
		this.m_root.addChild(ptSettingsAdmin);
		ParticipantTree ptDefault = new ParticipantTree(Role.getDefaultRole());
		this.m_root.addChild(ptDefault);
		ParticipantTree ptAnonymous = new ParticipantTree(Role.getAnonymous());
		this.m_root.addChild(ptAnonymous);

		// 如果没有自定义的群组则加入群组根节点
		boolean hasGroupRoot = false;
		List<ParticipantTree> children = m_root.getChildren();
		if (children != null && children.size() > 0) {
			for (ParticipantTree x : children) {
				if (x == null) continue;
				if (!hasGroupRoot && x.getSecurityCode() == ParticipantTree.GROUP_ROOT_CODE) hasGroupRoot = true;
			}
		}
		if (!hasGroupRoot) {
			m_root.addChild(new ParticipantTree(ParticipantTree.GROUP_ROOT_CODE, ParticipantTree.GROUP_ROOT_NAME, ParticipantTree.GROUP_ROOT_UNID, ParticipantTree.GROUP_ROOT_ALIAS, ParticipantType.Group, false));
		}

		// 初始化并同步快速查询字典
		this.m_mapOfSecurityCodeAndIndex = new HashMap<Integer, Integer[]>(this.m_participantCount + 1);
		this.m_mapOfUnidAndSecurityCode = new HashMap<String, Integer>(this.m_participantCount + 1);
		this.m_mapOfAliasAndSecurityCode = new HashMap<String, Integer>(this.m_participantCount + 1);
		this.m_mapOfUserNameAndSecurityCode = new HashMap<String, Integer>(this.m_participantCount + 1);
		syncMap(this.m_root, null);
	}

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return ParticipantTreeProvider
	 */
	public static ParticipantTreeProvider getInstance() {
		ParticipantTreeProvider ptp = null;
		synchronized (m_lock) {
			int orgsc = OrganizationsContext.getInstance().getContextUserOrgSc();
			ptp = m_instances.get(orgsc);
			if (ptp == null) {
				String orgUnid = OrganizationsContext.getInstance().getContextUserOrgUnid();
				ptp = new ParticipantTreeProvider(orgUnid);
				m_instances.put(orgsc, ptp);
			}
		}
		return ptp;
	}

	/**
	 * 让系统从持久层（数据库表）中重新装载参与者树信息。
	 * 
	 * <p>
	 * 一般用于持久更改了用户、部门、群组、角色及其关系等信息后调用。
	 * </p>
	 */
	public static void reload() {
		synchronized (m_lock) {
			int orgsc = OrganizationsContext.getInstance().getContextUserOrgSc();
			m_instances.remove(orgsc);
		}
	}

	/**
	 * 重载缓存的群组或角色包含的下级参与者信息。
	 * 
	 * <p>
	 * 通常在更新了群组或角色包含的下级参与者信息时调用。
	 * </p>
	 * 
	 * @param securityCode
	 */
	public void reloadRoleOrGroupParticipants(int securityCode) {
		if (m_mapOfRGSCAndPSC != null) this.m_mapOfRGSCAndPSC.remove(securityCode);
	}

	/**
	 * 获取有效参与者总数。
	 * 
	 * @return
	 */
	public int getParticipantCount() {
		return this.m_participantCount;
	}

	/**
	 * 获取指定类型的有效参与者总数。
	 * 
	 * @return
	 */
	public int getParticipantCount(ParticipantType pt) {
		if (m_root == null) return 0;
		return getParticipantCount(m_root, pt);
	}

	/**
	 * 获取p及p包含的指定类型的有效参与者总数。
	 * 
	 * @param p
	 * @param pt
	 * @return
	 */
	protected int getParticipantCount(ParticipantTree p, ParticipantType pt) {
		if (p == null) return 0;
		int r = 0;
		if (p.getParticipantType() == pt) r++;
		List<ParticipantTree> children = p.getChildren();
		if (children == null || children.isEmpty()) return r;
		for (ParticipantTree x : children) {
			r += getParticipantCount(x, pt);
		}
		return r;
	}

	/**
	 * 返回系统所有参与者树对应的根节点对象。
	 * 
	 * <p>
	 * 此根节点对象以树结构的方式包含系统所有有效参与者的信息。
	 * </p>
	 * 
	 * @return ParticipantTree
	 */
	public ParticipantTree getRoot() {
		return m_root;
	}

	/**
	 * 根据clue指定的线索在根节点下查找符合此线索的参与者树并返回。
	 * 
	 * <p>
	 * 如果已知安全编码，应该优先使用{@link ParticipantTreeProvider#getParticipantTree(int)}方法（因为它比此方法具有更好的执行性能）。
	 * </p>
	 * 
	 * @param clue String，必须，线索可以是用户、组织、群组、角色的唯一全名、别名、unid中的某一个值。
	 * @return ParticipantTree
	 */
	public ParticipantTree getParticipantTree(String clue) {
		int sc = StringUtil.getValueInt(clue, -1);
		if (sc > 0) { return getParticipantTree(sc); }
		Integer securityCode = this.m_mapOfUserNameAndSecurityCode.get(clue);
		if (securityCode == null) securityCode = this.m_mapOfAliasAndSecurityCode.get(clue);
		if (securityCode == null) securityCode = this.m_mapOfUnidAndSecurityCode.get(clue);
		if (securityCode != null) {
			return this.getParticipantTree(securityCode);
		} else { // 可能是名称
			return ParticipantTreeProvider.getParticipantTree(this.m_root, clue);
		}
	}

	/**
	 * 根据securityCode指定的安全编码在根节点下查找符合此编码的参与者树并返回。
	 * 
	 * <p>
	 * 如果已知安全编码，应该优先使用此方法（因为此方法比{@link ParticipantTreeProvider#getParticipantTree(String)}具有更好的执行性能）。
	 * </p>
	 * 
	 * @param securityCode int,安全编码，必须。
	 * @return ParticipantTree
	 */
	public ParticipantTree getParticipantTree(int securityCode) {
		if (m_root.getSecurityCode() == securityCode) return m_root;
		Integer[] idx = this.m_mapOfSecurityCodeAndIndex.get(securityCode);
		if (idx == null) return null;
		ParticipantTree pt = this.m_root;
		for (int i : idx) {
			pt = pt.getChildren().get(i);
		}
		return pt;
	}

	/**
	 * 获取根节点包含的所有下级节点中参与者类型为pt的子节点的列表集合。
	 * 
	 * @param pt
	 * @return
	 */
	public List<ParticipantTree> getChildren(ParticipantType pt) {
		return ParticipantTreeProvider.getChildren(this.m_root, pt);
	}

	/**
	 * 获取{@link Participant}对应的参与者包含的所有下级参与者列表。
	 * 
	 * <p>
	 * 如果p是部门，那么此方法将获取其包含的所有用户参与者；如果p是群组或者角色类型参与者，那么此方法会获取其包含的具体下级用户/群组参与者。
	 * </p>
	 * 
	 * @param parent Participant，参与者，必须
	 * @param session Session，自定义会话，必须。
	 * @return List&lt;Participant&gt;
	 */
	public List<Participant> getParticipants(Participant parent, Session session) {
		if (parent == null) return null;
		List<Participant> result = null;
		ParticipantType pt = parent.getParticipantType();
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree ptree = null;
		List<ParticipantTree> list = null;
		Group g = null;
		boolean isDynamicGroup = true;
		if (pt == ParticipantType.Group) {
			g = (Group) ResourceContext.getInstance().getResource(parent.getUNID(), Group.class);
			if (g == null) return null;
			isDynamicGroup = (g.getGroupType() == GroupType.Dynamic);
		}
		if (pt == ParticipantType.Person) { // 人员
			result = new ArrayList<Participant>(1);
			result.add(parent);
		} else if (pt == ParticipantType.Organization) { // 组织
			ptree = ptp.getParticipantTree(parent.getSecurityCode());
			if (ptree == null) return null;
			list = ParticipantTreeProvider.getChildren(ptree, ParticipantType.Person);
			if (list == null || list.isEmpty()) return null;
			result = new ArrayList<Participant>(list);
		} else if (pt == ParticipantType.Role || !isDynamicGroup) { // 静态群组和角色
			boolean foundInMap = false;
			Participant participant = null;
			if (m_mapOfRGSCAndPSC != null) { // 先从缓存中找静态群组或角色对应的用户安全编码数组结果
				foundInMap = m_mapOfRGSCAndPSC.containsKey(parent.getSecurityCode());
			}// if end
			if (foundInMap) {
				Integer[] scs = m_mapOfRGSCAndPSC.get(parent.getSecurityCode());
				if (scs != null && scs.length > 0) {
					for (int sc : scs) {
						participant = this.getParticipantTree(sc);
						if (participant == null) continue;
						if (result == null) result = new ArrayList<Participant>(scs.length);
						result.add(participant);
					}// for end
				}// if end
			}// if end
			if (!foundInMap) { // 如果在缓存中找不到，那么从数据库中取并加到缓存。
				ParticipantsProvider provider = new RoleAndStaticGroupChildrenProvider();
				if (pt == ParticipantType.Role) {
					Resource r = ResourceContext.getInstance().getResource(parent.getUNID(), Role.class);
					result = provider.provide(session, r);
				} else {
					result = provider.provide(session, g);
				}
				if (result != null && !result.isEmpty()) {
					if (m_mapOfRGSCAndPSC == null) m_mapOfRGSCAndPSC = new HashMap<Integer, Integer[]>();
					Integer[] arr = new Integer[result.size()];
					int idx = 0;
					for (Participant x : result) {
						arr[idx++] = new Integer(x.getSecurityCode());
					}
					m_mapOfRGSCAndPSC.put(parent.getSecurityCode(), arr);
				}
			}
		} else if (pt == ParticipantType.Group && isDynamicGroup) {
			String impl = g.getGroupImplement();
			if (impl == null || impl.length() == 0) {
				FileLogger.debug("动态群组“%1$s”没有配置群组公式！", g.getUNID());
				return null;
			}
			ParticipantsProvider pp = Instance.newInstance(impl, ParticipantsProvider.class);
			if (pp == null) {
				FileLogger.debug("无法初始化动态群组“%1$s”对应的效群组公式“%2$s”，请检查类是否存在！", g.toString(), impl);
				return null;
			}
			result = pp.provide(session, g);
		}
		return result;
	}

	/**
	 * 获取只包含所有部门信息的根{@link ParticipantTree}对象。
	 * 
	 * @return
	 */
	public ParticipantTree getAllSubOrg() {
		if (m_units == null) m_units = ParticipantTreeProvider.getChildren(m_root, ParticipantType.Organization);
		ParticipantTree root = new ParticipantTree(m_root);
		root.setChildren(m_units);
		return root;
	}

	/**
	 * 根据clue指定的线索在parent中查找符合此线索的参与者。
	 * 
	 * <p>
	 * 此方法速度可能更慢，因此请考虑使用场合。
	 * </p>
	 * 
	 * @param parent ParticipantTree，必须，表示开始查找的ParticipantTree。
	 * @param clue String，必须，线索可以是用户、组织、群组、角色的唯一名称、别名、unid、安全编码（需先转为字符串格式）中的某一个值。
	 * @return
	 */
	public static ParticipantTree getParticipantTree(ParticipantTree parent, String clue) {
		ParticipantTree result = null;
		if (parent == null) return result;
		String name = parent.getName();
		String alias = parent.getAlias();
		String sc = "" + parent.getSecurityCode();
		String unid = parent.getUNID();
		if (clue.equalsIgnoreCase(name) || clue.equalsIgnoreCase(alias) || clue.equals(sc) || clue.equalsIgnoreCase(unid)) {
			result = parent;
		} else {
			List<ParticipantTree> list = parent.getChildren();
			if (list != null && !list.isEmpty()) {
				for (ParticipantTree x : list) {
					result = getParticipantTree(x, clue);
					if (result != null) return result;
				}
			}
		}
		return result;
	}

	/**
	 * 获取parent包含的所有下级节点中参与者类型为pt的子节点的列表集合。
	 * 
	 * @param parent
	 * @param pt
	 * @return
	 */
	public static List<ParticipantTree> getChildren(ParticipantTree parent, ParticipantType pt) {
		if (parent == null) return null;
		List<ParticipantTree> result = new ArrayList<ParticipantTree>();
		List<ParticipantTree> subResult = null;
		List<ParticipantTree> list = parent.getChildren();
		if (list == null || list.isEmpty()) return null;
		for (ParticipantTree x : list) {
			if (x == null || x.getParticipantType() != pt) continue;
			result.add(x);
			subResult = getChildren(x, pt);
			if (subResult != null && !subResult.isEmpty()) {
				for (ParticipantTree y : subResult) {
					result.add(y);
				}
			}
		}
		return result;
	}
}

