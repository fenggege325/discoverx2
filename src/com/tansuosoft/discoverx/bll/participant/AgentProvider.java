/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.participant;

import java.util.HashMap;
import java.util.Map;

import com.tansuosoft.discoverx.dao.DAOConfig;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 提供用户对应的代理用户参与者信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AgentProvider {
	private Map<Integer, Integer> m_clientToAgentMap = new HashMap<Integer, Integer>();

	/**
	 * 缺省构造器。
	 */
	@SuppressWarnings("unchecked")
	private AgentProvider() {
		DBRequest dbr = DAOConfig.getInstance().getDBRequest("AgentFetcher");
		if (dbr == null) throw new RuntimeException("无法初始化数据请求对象！。");
		dbr.sendRequest();
		Map<String, String> map = (Map<String, String>) dbr.getResult(Map.class);
		if (map == null) return;
		int clientSC = 0;
		int agentSC = 0;
		for (String k : map.keySet()) {
			clientSC = StringUtil.getValueInt(k, -1);
			if (clientSC <= 0) continue;
			agentSC = StringUtil.getValueInt(map.get(k), -1);
			if (agentSC <= 0) continue;
			m_clientToAgentMap.put(clientSC, agentSC);
		}
		map.clear();
		map = null;
	}

	private static AgentProvider m_instance = null;

	/**
	 * 获取此对象唯一实例。
	 * 
	 * @return
	 */
	public synchronized static AgentProvider getInstance() {
		if (m_instance == null) {
			m_instance = new AgentProvider();
		}
		return m_instance;
	}

	/**
	 * 获取并返回securityCode指定安全编码对应的用户的有效代理用户参与者信息。
	 * 
	 * @param securityCode
	 * @return {@link Participant}，返回代理用户参与者信息，如果没有指定代理用户或者当前日期时间不在用户的离开办公室日期时间范围内，那么返回null。
	 */
	public Participant getAgent(int securityCode) {
		Integer agentSC = this.m_clientToAgentMap.get(securityCode);
		if (agentSC == null || agentSC.intValue() <= 0) return null;
		return ParticipantTreeProvider.getInstance().getParticipantTree(agentSC);
	}
}

