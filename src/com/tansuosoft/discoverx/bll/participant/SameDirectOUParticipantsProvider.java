/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.participant;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 返回与当前自定义会话用户同一个直接部门的所与参与者的参与者提供类。
 * 
 * @author coca@tansuosoft.cn
 */
public class SameDirectOUParticipantsProvider implements ParticipantsProvider {

	/**
	 * 重载provide：返回与当前自定义会话用户同一个直接部门的所与参与者的参
	 * 
	 * @see com.tansuosoft.discoverx.bll.participant.ParticipantsProvider#provide(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public List<Participant> provide(Session session, Resource bindResource) {
		if (session == null) {
			FileLogger.debug("无法获取当前用户自定义会话信息！");
			return null;
		}
		User u = session.getUser();
		String name = u.getName();
		name = StringUtil.stringLeftBack(name, User.SEPARATOR);
		ParticipantTree root = ParticipantTreeProvider.getInstance().getRoot();
		ParticipantTree pt = ParticipantTreeProvider.getParticipantTree(root, name);
		List<ParticipantTree> list = ParticipantTreeProvider.getChildren(pt, ParticipantType.Person);
		if (list == null || list.isEmpty()) return null;
		return new ArrayList<Participant>(list);
	}

}

