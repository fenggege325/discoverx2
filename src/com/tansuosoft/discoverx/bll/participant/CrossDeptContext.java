/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.participant;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;

/**
 * 用于获取用户交叉部门信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class CrossDeptContext implements JsonSerializable {
	private static Object m_lock = new Object();
	private static CrossDeptContext m_instance = null;

	private final Map<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();
	private static final ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();

	/**
	 * 私有构造器。
	 */
	private CrossDeptContext() {
		init();
	}

	private void init() {
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql("select c_unid,c_authunid from t_authority where c_authdirectory='organization'");
				return result;
			}
		};

		dbr.setResultBuilder(new ResultBuilder() {
			@Override
			public Object build(DBRequest request, Object rawResult) {
				DataReader dr = (DataReader) rawResult;
				if (dr == null) return null;

				ParticipantTree ptUser = null;
				ParticipantTree ptDept = null;
				String userUnid = null;
				String deptUnid = null;
				int userSc = 0;
				int deptSc = 0;
				List<Integer> list = null;
				try {
					while (dr.next()) {
						userUnid = dr.getString(1);
						deptUnid = dr.getString(2);
						ptUser = ptp.getParticipantTree(userUnid);
						ptDept = ptp.getParticipantTree(deptUnid);
						if (ptUser != null && ptDept != null) {
							userSc = ptUser.getSecurityCode();
							deptSc = ptDept.getSecurityCode();
							list = map.get(userSc);
							if (list == null) {
								list = new ArrayList<Integer>();
								map.put(userSc, list);
							}
							list.add(deptSc);
						}
					}
				} catch (SQLException ex) {
					FileLogger.error(ex);
				}
				return null;
			}
		});

		dbr.sendRequest();
	}

	/**
	 * 获取本对象的唯一实例。
	 * 
	 * @return CrossDeptContext
	 */
	public static CrossDeptContext getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) m_instance = new CrossDeptContext();
		}
		return m_instance;
	}

	/**
	 * 获取安全编码指定的用户所属的所有交叉部门参与者安全编码列表。
	 * 
	 * @param usersc
	 * @return List&lt;Integer&gt;
	 */
	public List<Integer> getCrossDeptList(int usersc) {
		return map.get(usersc);
	}

	/**
	 * 获取安全编码指定的用户所属的所有交叉部门参与者列表。
	 * 
	 * @param usersc
	 * @return List&lt;Participant&gt;
	 */
	public List<Participant> getCrossDeptParticipantList(int usersc) {
		List<Integer> list = this.getCrossDeptList(usersc);
		if (list == null || list.isEmpty()) return null;
		ParticipantTree p = null;
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		List<Participant> result = new ArrayList<Participant>();
		for (int sc : list) {
			p = ptp.getParticipantTree(sc);
			if (p == null) continue;
			result.add(p);
		}
		return result;
	}

	/**
	 * 追加安全编码指定的用户对应的单个交叉部门参与者安全编码信息。
	 * 
	 * @param usersc
	 * @param p
	 */
	public void addCrossDept(int usersc, int p) {
		List<Integer> list = map.get(usersc);
		if (list == null) {
			list = new ArrayList<Integer>();
			map.put(usersc, list);
		}
		list.add(p);
	}

	/**
	 * 追加安全编码指定的用户对应的多个交叉部门参与者安全编码信息。
	 * 
	 * @param usersc
	 * @param plist
	 */
	public void addCrossDeptList(int usersc, List<Integer> plist) {
		if (plist == null) return;
		List<Integer> list = map.get(usersc);
		if (list == null) {
			list = new ArrayList<Integer>(plist);
			map.put(usersc, list);
			return;
		}
		list.addAll(plist);
	}

	/**
	 * 删除安全编码指定的用户对应的单个交叉部门参与者安全编码信息。
	 * 
	 * @param usersc
	 * @param p
	 */
	public void removeCrossDept(int usersc, int p) {
		List<Integer> list = map.get(usersc);
		if (list == null || list.isEmpty()) return;
		List<Integer> newList = new ArrayList<Integer>(list.size());
		for (int sc : list) {
			if (p == sc) continue;
			newList.add(sc);
		}
		map.put(usersc, newList);
	}

	/**
	 * 删除安全编码指定的用户对应的多个交叉部门参与者安全编码信息。
	 * 
	 * @param usersc
	 * @param plist
	 */
	public void removeCrossDeptList(int usersc, List<Integer> plist) {
		if (plist == null || plist.isEmpty()) return;
		List<Integer> list = map.get(usersc);
		if (list == null || list.isEmpty()) return;
		boolean canAdd = true;
		List<Integer> newList = new ArrayList<Integer>(list.size());
		for (int i = 0; i < list.size(); i++) {
			int x = list.get(i);
			canAdd = true;
			for (int p : plist) {
				if (p == x) {
					canAdd = false;
					break;
				}
			}
			if (canAdd) newList.add(x);
		}
		map.put(usersc, newList);
	}

	private static final String NO_DATA = "{error:true,status:0}";

	/**
	 * 重载：输出json格式的所有具有交叉部门的用户对应的交叉部门信息。
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	@Override
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		if (map.isEmpty()) { return NO_DATA; }
		List<Integer> list = null;
		ParticipantTree p = null;
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		for (int sc : map.keySet()) {
			list = map.get(sc);
			if (list == null || list.isEmpty()) continue;
			sb.append(sb.length() > 0 ? "," : "").append("\r\n");
			sb.append("{");
			sb.append("u:").append(sc);
			sb.append(",").append("v:").append("[");
			int pidx = 0;
			for (int scx : list) {
				p = ptp.getParticipantTree(scx);
				if (p == null) continue;
				sb.append(pidx > 0 ? "," : "").append("\r\n");
				sb.append("{");
				sb.append("sc:").append(p.getSecurityCode());
				sb.append(",").append("alias:").append("'").append(p.getAlias()).append("'");
				sb.append(",").append("name:").append("'").append(StringUtil.encode4Json(p.getName())).append("'");
				sb.append(",").append("unid:").append("'").append(p.getUNID()).append("'");
				sb.append("}");
				pidx++;
			}
			sb.append("]");
			sb.append("}");
		}
		if (sb.length() > 0) {
			sb.insert(0, "[");
			sb.append("\r\n]");
		} else {
			return NO_DATA;
		}
		return sb.toString();
	}
}

