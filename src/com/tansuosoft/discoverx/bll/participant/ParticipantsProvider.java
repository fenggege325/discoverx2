/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.bll.participant;

import java.util.List;

import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;

/**
 * 提供用户（参与者）列表的类需继承的统一接口。
 * 
 * <p>
 * 比如获取动态群组包含的用户信息的类需实现此接口。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public interface ParticipantsProvider {
	/**
	 * 提供具体用户列表。
	 * 
	 * <p>
	 * 实现类需实现此方法以提供动态群组包含的具体参与者。
	 * </p>
	 * 
	 * @param session {@link com.tansuosoft.discoverx.model.Session}，表示当前用户自定义会话。
	 * @param bindResource {@link com.tansuosoft.discoverx.model.Resource}，表示绑定的资源，比如为动态群组获取，则bindResource为绑定的群组资源对象实例。
	 * @return List&lt;Participant&gt; 返回获取到的参与者列表。
	 */
	public List<Participant> provide(Session session, Resource bindResource);
}

