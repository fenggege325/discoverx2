/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 根据sql返回的结果集({@link DataReader})构造{@link java.util.Map}对象的{@link ResultBuilder}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class MapResultBuilder implements ResultBuilder {

	/**
	 * 重载build：返回构造好的{@link java.util.Map}对象，如果结果集为null或没有记录，则返回null。
	 * 
	 * <p>
	 * key为第一列的值（必须为有效非空字符串值，否则被忽略），value为第二列的值，如果结果集只有一列则会导致错误，如果有多余列则被忽略。<br/>
	 * 以Map&lt;String,String&gt;的结果类型返回。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		if (rawResult == null || !(rawResult instanceof DataReader)) return null;
		DataReader dr = (DataReader) rawResult;
		Map<String, String> map = null;
		try {
			String key = null;
			String value = null;
			while (dr.next()) {
				key = dr.getString(1);
				value = dr.getString(2);
				if (key == null || key.length() == 0) continue;
				if (map == null) map = new HashMap<String, String>();
				map.put(key, value);
			}// while end
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return map;
	}
}
