/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.User;

/**
 * 发送数据库请求后构造最终结果的接口。
 * 
 * <p>
 * 数据库请求处理完成后，根据SQLWrapper中的语句执行的返回结果（原始结果）构造最终需要的结果对象（最终结果）时，须实现的接口。
 * </p>
 * 
 * @see DBRequest#getResultBuilder()
 * @author coca@tansuosoft.cn
 * 
 */
public interface ResultBuilder {
	/**
	 * 根据提供的参数构造并返回最终结果。
	 * 
	 * <p>
	 * 最终结果构造过程通常用于对查询返回的结果集（{@link DataReader}）进行循环处理以读取其中的字段值组合成特定类型的输出结果。<br/>
	 * </p>
	 * 
	 * @param request 对应的数据库请求对象。
	 * @param rawResult 对应的原始结果对象，通常{@link RequestType#Query}类型的请求传入的是{@link DataReader}对象。<br/>
	 *          通过{@link DataReader}对象返回构造好的结果对象的一般规则为：<br/>
	 *          如果结果集只包含一条结果，则返回构造的结果对象本身；否则返回包含构造的结果对象的列表对象作为结果对象。<br/>
	 *          比如构造方法将查询结果集构造为{@link User}对象，此时如果只有查询结果集只有一条记录，则返回单个{@link User}对象，如果查询结果集包含多条记录，则返回包含多个{@link User}对象的列表（{@link java.util.List}）。
	 * 
	 * @return Object 返回构造好的结果对象（最终结果），如果出现异常等则返回null。
	 */
	public Object build(DBRequest request, Object rawResult);
}

