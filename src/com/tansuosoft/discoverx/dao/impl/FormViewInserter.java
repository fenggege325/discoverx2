/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.view.FormViewTable;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 插入文档资源对象{@link Document}对应的一条新记录到视图查询表的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#ENTITY_PARAM_NAME}获取{@link Document}对象以提供插入信息。<br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FormViewInserter extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public FormViewInserter() {
	}

	protected List<String> m_itemValues = null; // 字段值列表。

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		Document x = (Document) this.getResource();
		if (x == null) throw new RuntimeException("没有提供文档对象！");
		Form f = x.getForm();
		if (f == null) throw new RuntimeException("找不到文档所属表单！");
		String vtName = String.format("%1$s%2$s", FormViewTable.FORM_VIEW_TABLE_PREFIX, x.getFormAlias());
		List<Item> items = f.getItems();
		if (items == null || items.isEmpty()) throw new RuntimeException("找不到文档表单字段！");
		String itemName = null;
		String itemValue = null;
		StringBuilder insertpart = new StringBuilder();
		StringBuilder valpart = new StringBuilder();
		valpart.append("?");
		this.m_itemValues = new ArrayList<String>(items.size() + 1);
		this.m_itemValues.add(x.getUNID());
		for (Item o : items) {
			if (o == null || !o.isFormViewTableColumn()) continue;
			itemName = o.getAlias();
			itemValue = x.getItemValue(itemName);
			this.m_itemValues.add(itemValue == null ? "" : itemValue);
			sql.append(sql.length() > 0 ? "," : "").append(itemName);
			valpart.append(valpart.length() > 0 ? "," : "").append("?");
		}

		insertpart.append("insert into ").append(vtName).append(" (c_punid,");
		sql.insert(0, insertpart);
		sql.append(",c_oid) values (").append(valpart.toString()).append(",?)");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		if (this.m_itemValues == null || this.m_itemValues.isEmpty()) throw new RuntimeException("没有提供字段值！");
		int idx = 0;
		try {
			byte bs[] = null;
			final int ivsbl = CommonConfig.getInstance().getItemValueStroageBoundaryLength();
			for (String v : this.m_itemValues) {
				bs = (v == null ? null : v.getBytes("utf-8"));
				if (bs != null && bs.length >= ivsbl) {
					cw.setString((++idx), v.substring(0, ivsbl / 2));
				} else {
					cw.setString((++idx), v);
				}
			}
			cw.setInt((++idx), OrganizationsContext.getInstance().getContextUserOrgSc());
		} catch (Exception e) {
			FileLogger.error(e);
		}
	}// func end
}

