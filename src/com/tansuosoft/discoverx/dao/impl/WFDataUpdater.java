/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.WFData;

/**
 * 更新WFData对象到数据库表的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#ENTITY_PARAM_NAME}获取要更新的{@link WFData}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class WFDataUpdater extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public WFDataUpdater() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("update t_wfdata set c_punid=?,c_activity=?,c_workflow=?,c_instance=?,c_parentInstance=?,c_data=?,c_parentData=?,c_level=?,c_sublevel=?,c_sort=?,c_created=?,c_expect=?,c_notify=?,c_accomplished=?,c_done=?,c_derivative=?,c_params=? where c_unid=?");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		WFData x = (WFData) this.getParameterObject(ENTITY_PARAM_NAME);

		try {
			if (x == null) throw new RuntimeException("没有提供有效流程控制数据！");
			cw.setString(1, x.getPUNID());
			cw.setString(2, x.getActivity());
			cw.setString(3, x.getWorkflow());
			cw.setInt(4, x.getInstance());
			cw.setInt(5, x.getParentInstance());
			cw.setInt(6, x.getData());
			cw.setInt(7, x.getParentData());
			cw.setInt(8, x.getLevel());
			cw.setInt(9, x.getSubLevel());
			cw.setInt(10, x.getSort());
			cw.setString(11, x.getCreated());
			cw.setString(12, x.getExpect());
			cw.setString(13, x.getNotify());
			cw.setString(14, x.getAccomplished());
			cw.setString(15, x.getDone() ? "y" : "n");
			cw.setString(16, x.getDerivative() ? "y" : "n");
			cw.setString(17, x.getParams());

			cw.setString(18, x.getUNID());
		} catch (SQLException e) {
			FileLogger.error(e);
		} catch (Exception e) {
			FileLogger.error(e);
		}
	}// func end
}

