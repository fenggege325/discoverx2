/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.SQLWrapper;

/**
 * 从表中删除对应WFData对象的数据库请求类。
 * 
 * <p>
 * 如果提供“unid”参数，则删除“unid”对应的对象，如果提供了“punid”参数，则删除所有所属文档unid为“punid”对应值的对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class WFDataRemover extends DBRequest {

	/**
	 * 缺省构造器。
	 */
	public WFDataRemover() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (unid != null) {
			sql.append("delete from t_wfdata where c_unid='").append(unid).append("'");
		} else if (punid != null) {
			sql.append("delete from t_wfdata where c_punid='").append(punid).append("'");
		} else {
			throw new RuntimeException("没有提供必要参数！");
		}
		result.setSql(sql.toString());

		return result;
	}
}

