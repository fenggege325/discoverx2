/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 插入SecurityEntry对象到数据库表的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#ENTITY_PARAM_NAME}参数名获取要插入的{@link SecurityEntry}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SecurityEntryInserter extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public SecurityEntryInserter() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("insert into t_security (c_id,c_punid,c_code,c_securityLevel,c_workflowLevel,c_securityLevel1,c_securityLevel2,c_securityLevel3,c_securityLevel4,c_securityLevel5,c_securityLevel6,c_securityLevel7,c_securityLevel8,c_workflowLevel1,c_workflowLevel2,c_workflowLevel3,c_workflowLevel4,c_workflowLevel5,c_workflowLevel6,c_workflowLevel7,c_workflowLevel8)");
		sql.append(" values (");
		sql.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?");
		sql.append(")");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		SecurityEntry x = (SecurityEntry) this.getParameterObject(ENTITY_PARAM_NAME);
		if (x == null) throw new RuntimeException("没有提供必要的SecurityEntry对象。");
		try {
			cw.setLong(1, SequenceProvider.getInstance("t_security").getSequence());
			cw.setString(2, x.getPUNID());
			int sl = x.getSecurityLevel();
			int wl = x.getWorkflowLevel();
			cw.setInt(3, x.getSecurityCode());
			cw.setInt(4, sl);
			cw.setInt(5, wl);
			int v = 0;
			int idx = 5;
			for (int i = 0; i < 8; i++) {
				idx++;
				v = (int) java.lang.Math.pow(2, i);
				if ((sl & v) == v) {
					cw.setInt(idx, 1);
				} else {
					cw.setInt(idx, 0);
				}
			}

			for (int i = 0; i < 8; i++) {
				idx++;
				v = (int) java.lang.Math.pow(2, i);
				if ((wl & v) == v) {
					cw.setInt(idx, 1);
				} else {
					cw.setInt(idx, 0);
				}
			}
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end

}

