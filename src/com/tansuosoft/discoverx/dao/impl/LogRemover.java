/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.Log;

/**
 * 从表中删除对应Log对象的数据库请求类。
 * 
 * <p>
 * 先通过{@link DBRequest.ENTITY_PARAM_NAME}获取要删除的{@link Log}对象；<br/>
 * 如果获取不到则从{@link DBRequest#UNID_PARAM_NAME}获取要删除日志的unid以删除指定日志；<br/>
 * 如果还获取不到则从{@link DBRequest#PUNID_PARAM_NAME}获取要删除日志所属资源的unid以删除所有此类日志。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class LogRemover extends DBRequest {

	/**
	 * 缺省构造器。
	 */
	public LogRemover() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		Log l = (Log) this.getParameterObject(ENTITY_PARAM_NAME);
		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (l != null) {
			sql.append("delete from t_log where c_unid='").append(l.getUNID()).append("'");
		} else if (unid != null) {
			sql.append("delete from t_log where c_unid='").append(unid).append("'");
		} else if (punid != null) {
			sql.append("delete from t_log where c_owner='").append(punid).append("'");
		} else {
			throw new RuntimeException("没有提供必要参数！");
		}
		result.setSql(sql.toString());

		return result;
	}
}

