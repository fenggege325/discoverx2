/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.List;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Field;
import com.tansuosoft.discoverx.model.FieldType;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.PersistenceState;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 更新{@link Document}资源对象到数据库表的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#getResource()}获取要更新的文档资源。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentUpdater extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public DocumentUpdater() {
	}

	private Document m_document = null;
	private String m_abstract = null;

	/**
	 * 获取参数传递的文档。
	 * 
	 * @return
	 */
	protected Document getDocument() {
		if (m_document != null) return m_document;
		Resource r = this.getResource();
		if (r != null && r instanceof Document) m_document = (Document) r;
		return m_document;
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		Document doc = this.getDocument();
		if (doc == null) throw new RuntimeException("未提供文档对象！");
		m_abstract = doc.getAbstract();

		sql.append("update t_document set c_unid=?,c_source=?,c_creator=?,c_created=?,c_modified=?,c_modifier=?,c_sort=?,c_description=?,c_name=?,c_alias=?,c_category=?,c_abstract=?,c_clickCount=?,c_title=?,c_businessTitle=?,c_state=?,c_processSequence=?,c_businessSequence=?,c_configResource=?,c_punid=?,c_type=?,c_formAlias=?,c_processYear=?,c_businessYear=?,c_rankCount=?,c_rankValue=?,c_securityCodes=?,c_tag=? where c_unid=?");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		boolean updaterFieldFlag = false;
		boolean inserterFieldFlag = false;
		FieldUpdater fieldUpdater = null;
		FieldInserter fieldInserter = null;
		List<Field> fields = doc.getFields();
		Item item = null;
		// 同步更新t_item表
		for (Field x : fields) {
			if (x == null || x.getFieldType() == FieldType.Builtin || x.getPersistenceState() == PersistenceState.Raw) continue;
			item = x.getItem();
			if (item == null || !item.canSave()) continue;
			if (x.getPersistenceState() == PersistenceState.Update) {
				fieldUpdater = new FieldUpdater();
				fieldUpdater.setParameter(DBRequest.ENTITY_PARAM_NAME, x);
				fieldUpdater.setParameter(DBRequest.PARENT_PARAM_NAME, doc);
				this.setNextRequest(fieldUpdater);
				if (!updaterFieldFlag) updaterFieldFlag = true;
			} else if (x.getPersistenceState() == PersistenceState.New) {
				fieldInserter = new FieldInserter();
				fieldInserter.setParameter(DBRequest.ENTITY_PARAM_NAME, x);
				fieldInserter.setParameter(DBRequest.PARENT_PARAM_NAME, doc);
				this.setNextRequest(fieldInserter);
				if (!inserterFieldFlag) inserterFieldFlag = true;
			}
		}
		// 同步更新vt_...表
		if (updaterFieldFlag || inserterFieldFlag) {
			DBRequest formViewUpdater = new FormViewUpdater();
			formViewUpdater.setParameter(ENTITY_PARAM_NAME, doc);
			this.setNextRequest(formViewUpdater);

			this.setUseTransaction(true);
		}

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		Document x = this.getDocument();

		if (x.getSort() == 0) x.setSort((int) SequenceProvider.getSortInstance().getSequence());

		try {
			cw.setString(1, x.getUNID());
			cw.setInt(2, x.getSource().getIntValue());
			cw.setString(3, x.getCreator());
			cw.setString(4, x.getCreated());
			cw.setString(5, x.getModified());
			cw.setString(6, x.getModifier());
			cw.setInt(7, x.getSort());
			cw.setString(8, x.getDescription());
			cw.setString(9, x.getName());
			cw.setString(10, x.getAlias());
			cw.setString(11, x.getCategory());
			cw.setString(12, m_abstract);
			cw.setInt(13, x.getClickCount());
			cw.setString(14, x.getTitle());
			cw.setString(15, x.getBusinessTitle());
			cw.setInt(16, x.getState());
			cw.setInt(17, x.getProcessSequence());
			cw.setInt(18, x.getBusinessSequence());
			cw.setString(19, x.getConfigResource());
			cw.setString(20, x.getPUNID());
			cw.setInt(21, x.getType().getIntValue());
			cw.setString(22, x.getFormAlias());
			cw.setInt(23, x.getProcessYear());
			cw.setInt(24, x.getBusinessYear());
			cw.setInt(25, x.getRankCount());
			cw.setInt(26, x.getRankValue());
			cw.setString(27, x.getSecurity() == null ? "" : x.getSecurity().toString());
			cw.setString(28, x.getTag());
			cw.setString(29, x.getUNID());
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end
}

