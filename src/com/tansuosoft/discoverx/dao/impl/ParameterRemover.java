/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 从表中删除对应{@link Parameter}对象的数据库请求类。
 * 
 * <p>
 * 优先通过{@link DBRequest#ENTITY_PARAM_NAME}参数名获取要删除的{@link Parameter}对象。<br/>
 * 其次如果提供了名为“punid”的参数，则删除所有属于指定参数值对应的资源的{@link Parameter}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ParameterRemover extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public ParameterRemover() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		Parameter p = (Parameter) this.getParameterObject(ENTITY_PARAM_NAME);
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (p == null && (punid == null || punid.length() == 0)) throw new RuntimeException("没有提供必要的要删除的参数的信息！");
		if (p != null) {
			sql.append("delete from t_parameter where c_name=? and c_punid=?");
		} else if (punid != null && punid.length() > 0) {
			sql.append("delete from t_parameter where c_punid=?");
		}
		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		try {
			Parameter p = (Parameter) this.getParameterObject(ENTITY_PARAM_NAME);
			String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
			if (p != null) {
				cw.setString(1, p.getName());
				cw.setString(2, p.getPUNID());
			} else if (punid != null && punid.length() > 0) {
				cw.setString(1, punid);
			}
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}
}

