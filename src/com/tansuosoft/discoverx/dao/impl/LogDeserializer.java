/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.LogLevel;
import com.tansuosoft.discoverx.model.LogType;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.Deserializer;

/**
 * 从表中反序列化{@link Log}对象的数据库请求类。
 * 
 * <p>
 * 如果从{@link DBRequest#UNID_PARAM_NAME}中获取到要反序列化的日志的unid则返回unid指定的{@link Log}对象；<br/>
 * 如果从{@link DBRequest#PUNID_PARAM_NAME}中获取到要反序列化的日志所属资源的unid则返回同属此unid的{@link Log}对象列表。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class LogDeserializer extends DBRequest implements ResultBuilder, Deserializer {

	/**
	 * 缺省构造器。
	 */
	public LogDeserializer() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (unid != null) {
			sql.append("select c_unid,c_owner,c_subject,c_created,c_verb,c_object,c_result,c_logLevel,c_logType,c_start,c_expect,c_message from t_log where c_unid='").append(unid).append("'");
		} else if (punid != null) {
			sql.append("select c_unid,c_owner,c_subject,c_created,c_verb,c_object,c_result,c_logLevel,c_logType,c_start,c_expect,c_message from t_log where c_owner='").append(punid).append("' order by c_created desc,c_start asc");
		}
		result.setSql(sql.toString());

		return result;
	}

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	@Override
	public Object deserialize(String src, Class<?> cls) {
		Object o = this.getResult();
		if (o == null) {
			this.setParameter(UNID_PARAM_NAME, src);
			this.sendRequest();
			o = this.getResult();
		}
		return o;
	}

	/**
	 * 重载build：返回包含字段名和字段值文本一一对应的哈希表。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		int count = 0;

		Log x = null;
		ArrayList<Log> al = null;

		try {
			while (dr.next()) {
				count++;
				if (count > 1) {
					if (al == null) al = new ArrayList<Log>();
					al.add(x);
				}

				x = new Log();

				x.setUNID(dr.getString(1));
				x.setOwner(dr.getString(2));
				x.setSubject(dr.getString(3));
				x.setCreated(dr.getString(4));
				x.setVerb(dr.getString(5));
				x.setObject(dr.getString(6));
				x.setResult(dr.getString(7));
				x.setLogLevel((LogLevel) dr.getEnum(8, LogLevel.Information));
				x.setLogType((LogType) dr.getEnum(9, LogType.Normal));
				x.setStart(dr.getString(10));
				x.setExpect(dr.getString(11));
				x.setMessage(dr.getString(12));

			}// while end
			if (al != null) al.add(x);
			return (count == 1 ? x : (count > 1 ? al : null));
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return null;
	}
}

