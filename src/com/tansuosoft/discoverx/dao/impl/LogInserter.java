/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 插入{@link Log}对象到数据库表的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#ENTITY_PARAM_NAME}获取要插入的{@link Log}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class LogInserter extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public LogInserter() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("insert into t_log (c_unid,c_owner,c_subject,c_created,c_verb,c_object,c_result,c_logLevel,c_logType,c_start,c_expect,c_message)");
		sql.append(" values (");
		sql.append("?,?,?,?,?,?,?,?,?,?,?,?");
		sql.append(")");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		Log x = (Log) this.getParameterObject(ENTITY_PARAM_NAME);
		if (x == null) throw new RuntimeException("没有提供有效的日志对象！");
		try {
			cw.setString(1, x.getUNID());
			cw.setString(2, x.getOwner());
			String subject = x.getSubject();
			if (subject == null) subject = "";
			cw.setString(3, subject);
			// StringReader sr = new StringReader(subject);
			// InputStreamReader sr = new InputStreamReader(new ByteArrayInputStream(subject.getBytes()));
			// cw.setCharacterStream(3, sr, subject.length());
			cw.setString(4, x.getCreated());
			cw.setString(5, x.getVerb());
			String object = x.getObject();
			if (object == null) object = "";
			cw.setString(6, object);
			cw.setString(7, x.getResult());
			cw.setInt(8, x.getLogLevel().getIntValue());
			cw.setInt(9, x.getLogType().getIntValue());
			cw.setString(10, x.getStart());
			cw.setString(11, x.getExpect());
			cw.setString(12, x.getMessage());
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end
}

