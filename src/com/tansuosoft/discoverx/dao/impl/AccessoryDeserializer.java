/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.Source;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.Deserializer;

/**
 * 从表中反序列化{@link Accessory}资源对象的数据库请求类。
 * 
 * <p>
 * 如果提供名为{@link DBRequest#UNID_PARAM_NAME}的参数，则获取参数值对应的对象并返回，如果提供了名为{@link DBRequest#PUNID_PARAM_NAME}的参数，则获取所有参数值对应的对象并返回包含这些对象的列表。
 * </p>
 * <p>
 * 返回列表时，其中包含的每一项不包含绑定的安全、额外参数等内容。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class AccessoryDeserializer extends DBRequest implements ResultBuilder, Deserializer {

	/**
	 * 缺省构造器。
	 */
	public AccessoryDeserializer() {
	}

	private boolean m_unidFlag = false;
	private boolean includeSecurity = false;

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		if (unid != null) {
			sql.append("select c_unid,c_creator,c_modified,c_modifier,c_sort,c_source,c_created,c_description,c_category,c_name,c_alias,c_punid,c_parentDirectory,c_fileName,c_template,c_codeTemplate,c_accessoryType,c_size,c_state from t_accessory where c_unid='").append(unid).append("'");
		} else {
			sql.append("select c_unid,c_creator,c_modified,c_modifier,c_sort,c_source,c_created,c_description,c_category,c_name,c_alias,c_punid,c_parentDirectory,c_fileName,c_template,c_codeTemplate,c_accessoryType,c_size,c_state from t_accessory where c_punid=? order by c_sort,c_modified desc");
			result.setParameterized(true);
			this.setParametersSetter(SecurityDeserializer.PUNIDSETTER);
		}
		result.setSql(sql.toString());
		includeSecurity = this.getParamValueBool("includeSecurity", false);
		if (unid != null) {
			m_unidFlag = true;
			// 安全
			DBRequest sder = new SecurityDeserializer();
			sder.setParameter(PUNID_PARAM_NAME, unid);
			this.setNextRequest(sder);

			// 额外参数
			DBRequest pder = new ParameterDeserializer();
			pder.setParameter(PUNID_PARAM_NAME, unid);
			this.setNextRequest(pder);
		} else if (includeSecurity) {
			DBRequest dbrss = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper result = new SQLWrapper();
					StringBuilder sql = new StringBuilder();
					sql.append("select a.c_punid,a.c_code,a.c_securityLevel,a.c_workflowLevel");
					sql.append(" from t_security a,t_accessory b");
					sql.append(" where a.c_punid=b.c_unid and b.c_punid=?");
					sql.append(" order by a.c_punid");
					result.setSql(sql.toString());
					result.setParameterized(true);
					this.setParametersSetter(SecurityDeserializer.PUNIDSETTER);
					return result;
				}
			};
			dbrss.setResultBuilder(new ResultBuilder() {
				@Override
				public Object build(DBRequest request, Object rawResult) {
					DataReader dr = (DataReader) rawResult;
					if (dr == null) return null;

					SecurityEntry x = null;
					Map<String, Security> result = new HashMap<String, Security>();
					Security s = null;
					String punid = "";
					String lastPunid = "1";
					try {
						while (dr.next()) {
							punid = dr.getString(1);
							if (!lastPunid.equalsIgnoreCase(punid)) {
								s = new Security(punid);
								s.setSecurityEntries(new ArrayList<SecurityEntry>());
								result.put(punid, s);
							}
							lastPunid = punid;
							x = new SecurityEntry();
							x.setPUNID(punid);
							x.setSecurityCode(dr.getInt(2));
							x.setSecurityLevel(dr.getInt(3));
							x.setWorkflowLevel(dr.getInt(4));
							s.getSecurityEntries().add(x);
						}// while end
						return result;
					} catch (SQLException ex) {
						FileLogger.error(ex);
					}
					return null;
				}
			});
			this.setNextRequest(dbrss);
		}
		return result;
	}

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	@Override
	public Object deserialize(String src, Class<?> cls) {
		this.setParameter(UNID_PARAM_NAME, src);

		this.sendRequest();
		return this.getResult();
	}

	/**
	 * 重载build
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		int count = 0;
		Resource p = this.getResource();
		Accessory x = null;
		ArrayList<Accessory> al = null;
		List<DBRequest> reqs = request.getChainChildren();
		try {
			while (dr.next()) {
				count++;
				if (count > 1) {
					if (al == null) al = new ArrayList<Accessory>();
					al.add(x);
				}

				x = new Accessory();
				x.setUNID(dr.getString(1));
				x.setCreator(dr.getString(2));
				x.setModified(dr.getString(3));
				x.setModifier(dr.getString(4));
				x.setSort(dr.getInt(5));
				x.setSource(dr.getEnumGeneric(6, Source.class, Source.UserDefine));
				x.setCreated(dr.getString(7));
				x.setDescription(dr.getString(8));
				x.setCategory(dr.getString(9));
				x.setName(dr.getString(10));
				x.setAlias(dr.getString(11));
				x.setPUNID(dr.getString(12));
				x.setParentDirectory(dr.getString(13));
				x.setFileName(dr.getString(14));
				x.setStyleTemplate(dr.getString(15));
				x.setCodeTemplate(dr.getString(16));
				x.setAccessoryType(dr.getString(17));
				x.setSize(dr.getInt(18, 0));
				x.setState(dr.getString(19));

				if (m_unidFlag && reqs != null) {
					Object result = null;
					for (DBRequest r : reqs) {
						if (r == null) continue;
						result = r.getResult();
						if (r instanceof SecurityDeserializer) {
							if (result != null && result instanceof Security) x.setSecurity((Security) result);
						} else if (r instanceof ParameterDeserializer) {
							if (result != null && result instanceof List) {
								x.setParameters((List<Parameter>) r.getResult());
							} else if (result != null && result instanceof Parameter) {
								List<Parameter> list = new ArrayList<Parameter>();
								list.add((Parameter) result);
								x.setParameters(list);
							}
						}
					}// for end
				}
			}// while end
			if (m_unidFlag) return x;
			if (x != null) {
				if (al != null) {
					al.add(x);
				} else {
					al = new ArrayList<Accessory>();
					al.add(x);
				}
			}
			// 需要设置附加文件列表的安全信息
			if (al != null && al.size() > 0 && reqs != null && includeSecurity) {
				for (DBRequest r : reqs) {
					if (r == null) return al;
					Map<String, Security> m = r.getResult(Map.class);
					if (m == null || m.isEmpty()) continue;
					for (Accessory acc : al) {
						if (acc == null) continue;
						Security s = m.get(acc.getUNID());
						if (s != null) acc.setSecurity(s);
					}
				}
			}
			if (p != null) p.setAccessories(al);
			return al;
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return null;
	}
}

