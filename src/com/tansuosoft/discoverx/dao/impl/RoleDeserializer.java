/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.Source;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.Deserializer;

/**
 * 从表中反序列化Role资源对象的数据库请求类。
 * 
 * <p>
 * 如果提供unid参数，则获取unid对应的对象并返回，如果提供了punid参数，则获取所有punid对应的对象并返回包含这些对象的列表。
 * </p>
 * <p>
 * 返回列表时，其中包含的每一项不包含绑定的安全、额外参数等内容。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class RoleDeserializer extends DBRequest implements ResultBuilder, Deserializer {

	/**
	 * 缺省构造器。
	 */
	public RoleDeserializer() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (unid != null) {
			sql.append("select c_unid,c_name,c_alias,c_description,c_created,c_creator,c_modified,c_modifier,c_sort,c_source,c_securityCode,c_punid,c_securityRange from t_role where c_unid='").append(unid).append("'");
		} else if (punid != null) {
			sql.append("select c_unid,c_name,c_alias,c_description,c_created,c_creator,c_modified,c_modifier,c_sort,c_source,c_securityCode,c_punid,c_securityRange from t_role where c_punid='").append(punid).append("'");
		}
		result.setSql(sql.toString());

		if (unid != null) {
			// 安全
			DBRequest sder = new SecurityDeserializer();
			sder.setParameter(PUNID_PARAM_NAME, unid);
			this.setNextRequest(sder);

			// 额外参数
			DBRequest pder = new ParameterDeserializer();
			pder.setParameter(PUNID_PARAM_NAME, unid);
			this.setNextRequest(pder);
		}
		return result;
	}

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	@Override
	public Object deserialize(String src, Class<?> cls) {
		this.setParameter(UNID_PARAM_NAME, src);

		this.sendRequest();
		return this.getResult();
	}

	/**
	 * 重载build
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		int count = 0;

		Role x = null;
		ArrayList<Role> al = null;

		try {
			while (dr.next()) {
				count++;
				if (count > 1) {
					if (al == null) al = new ArrayList<Role>();
					al.add(x);
				}

				x = new Role();

				x.setUNID(dr.getString(1));
				x.setName(dr.getString(2));
				x.setAlias(dr.getString(3));
				x.setDescription(dr.getString(4));
				x.setCreated(dr.getString(5));
				x.setCreator(dr.getString(6));
				x.setModified(dr.getString(7));
				x.setModifier(dr.getString(8));
				x.setSort(dr.getInt(9));
				x.setSource(dr.getEnumGeneric(10, Source.class, Source.UserDefine));
				x.setSecurityCode(dr.getInt(11));
				x.setPUNID(dr.getString(12));
				x.setSecurityRange(dr.getInt(13));

				List<DBRequest> reqs = request.getChainChildren();
				Object result = null;
				for (DBRequest r : reqs) {
					if (r == null) continue;
					result = r.getResult();
					if (r instanceof SecurityDeserializer) {
						if (result != null && result instanceof Security) x.setSecurity((Security) result);
					} else if (r instanceof AccessoryDeserializer) {
						if (result != null && result instanceof List) {
							x.setAccessories((List<Accessory>) result);
						} else if (result != null && result instanceof Accessory) {
							List<Accessory> list = new ArrayList<Accessory>();
							list.add((Accessory) result);
							x.setAccessories(list);
						}
					} else if (r instanceof ParameterDeserializer) {
						if (result != null && result instanceof List) {
							x.setParameters((List<Parameter>) r.getResult());
						} else if (result != null && result instanceof Parameter) {
							List<Parameter> list = new ArrayList<Parameter>();
							list.add((Parameter) result);
							x.setParameters(list);
						}
					}
				}// for end
			}// while end
			if (al != null) al.add(x);
			return (count == 1 ? x : (count > 1 ? al : null));
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return null;
	}
}

