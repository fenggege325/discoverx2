/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 从表中删除授权信息的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#UNID_PARAM_NAME}获取要删除其所有授权信息的资源的UNID。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class AuthorityRemover extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public AuthorityRemover() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("delete from t_authority where c_unid=?");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		if (unid == null) throw new RuntimeException("无法获取资源的UNID。");
		try {
			cw.setString(1, unid);
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end
}

