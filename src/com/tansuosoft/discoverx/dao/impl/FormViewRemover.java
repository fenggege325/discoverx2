/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.bll.view.FormViewTable;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 从视图查询表中删除指定文档对应记录的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#getResource()}获取要删除的文档资源。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FormViewRemover extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public FormViewRemover() {
	}

	private Document doc = null;

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		doc = (Document) this.getResource();
		if (doc == null) throw new RuntimeException("无法获取文档对象。");
		String formAlias = doc.getFormAlias();
		if (formAlias == null || formAlias.length() == 0) throw new RuntimeException("无法获取文档所属表单别名。");
		sql.append("delete from ").append(FormViewTable.FORM_VIEW_TABLE_PREFIX).append(formAlias).append(" where c_punid=?");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		String punid = doc.getUNID();

		try {
			cw.setString(1, punid);
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end
}

