/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentType;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.Opinion;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Source;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.Deserializer;

/**
 * 从表中反序列化{@link Document}资源对象的数据库请求类。
 * 
 * <p>
 * 如果提供名为{@link DBRequest#UNID_PARAM_NAME}的参数，则获取unid值对应的文档资源对象并返回，如果提供了名为{@link DBRequest#PUNID_PARAM_NAME}的参数，则获取所有punid对应的文档资源对象并返回包含这些对象的列表。
 * </p>
 * <p>
 * 返回文档资源列表时，其中包含的每一个文档资源对象没有反序列化其绑定的安全、附加文件、额外参数等内容。
 * </p>
 * <p>
 * 文档包含的附加文件列表中的附加文件资源包含了从数据库中获取的各自可能存在的安全信息({@link Security})。
 * </p>
 * <p>
 * 当获取unid对应的文档资源对象时，可以通过以下参数名与其对应值来指示系统不要反序列化文档的日志、审批意见(或评论等)、附加文件等信息。
 * <table cellpadding="2" border="1" cellspacing="0" style="table-layout:fixed;border:1px solid #cccccc;border-collapse:collapse;">
 * <tr>
 * <td width="200">参数名</td>
 * <td width="200">参数值</td>
 * <td>说明</td>
 * </tr>
 * <tr>
 * <td>EXCLUDE_LOGS</td>
 * <td>true</td>
 * <td>不包含日志信息。</td>
 * </tr>
 * <tr>
 * <td>EXCLUDE_ACCESSORIES</td>
 * <td>true</td>
 * <td>不包含附加文件信息。</td>
 * </tr>
 * <tr>
 * <td>EXCLUDE_OPINIONS</td>
 * <td>true</td>
 * <td>不包含审批意见(或评论)信息。</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentDeserializer extends DBRequest implements ResultBuilder, Deserializer {
	/**
	 * 指示是否反序列化文档所属日志的参数名。
	 */
	public static final String PN_EXCLUDE_LOGS = "EXCLUDE_LOGS";
	/**
	 * 指示是否反序列化文档所属附加文件的参数名。
	 */
	public static final String PN_EXCLUDE_ACCESSORIES = "EXCLUDE_ACCESSORIES";
	/**
	 * 指示是否反序列化文档所属审批意见(或评论等)的参数名。
	 */
	public static final String PN_EXCLUDE_OPINIONS = "EXCLUDE_OPINIONS";

	/**
	 * 缺省构造器。
	 */
	public DocumentDeserializer() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (unid != null) {
			sql.append("select c_unid,c_source,c_creator,c_created,c_modified,c_modifier,c_sort,c_description,c_name,c_alias,c_category,'',c_clickCount,c_title,c_businessTitle,c_state,c_processSequence,c_businessSequence,c_configResource,c_punid,c_type,c_formAlias,c_processYear,c_businessYear,c_rankCount,c_rankValue,c_tag,c_securitycodes from t_document where c_unid='").append(unid).append("'");
		} else if (punid != null) {
			sql.append("select c_unid,c_source,c_creator,c_created,c_modified,c_modifier,c_sort,c_description,c_name,c_alias,c_category,'',c_clickCount,c_title,c_businessTitle,c_state,c_processSequence,c_businessSequence,c_configResource,c_punid,c_type,c_formAlias,c_processYear,c_businessYear,c_rankCount,c_rankValue,c_tag,c_securitycodes from t_document where c_punid='").append(punid).append("'");
		}
		result.setSql(sql.toString());

		if (unid != null) {
			// 安全
			DBRequest sder = new SecurityDeserializer();
			sder.setParameter(PUNID_PARAM_NAME, unid);
			this.setNextRequest(sder);

			// 字段
			DBRequest fder = new FieldDeserializer();
			fder.setParameter(PUNID_PARAM_NAME, unid);
			this.setNextRequest(fder);

			if (!this.getParamValueBool(PN_EXCLUDE_ACCESSORIES, false)) {
				// 附加文件
				DBRequest ader = new AccessoryDeserializer();
				ader.setParameter(PUNID_PARAM_NAME, unid);
				ader.setParameter("includeSecurity", true);
				this.setNextRequest(ader);
			}

			if (!this.getParamValueBool(PN_EXCLUDE_OPINIONS, false)) {
				// 意见
				DBRequest oder = new OpinionDeserializer();
				oder.setParameter(PUNID_PARAM_NAME, unid);
				this.setNextRequest(oder);
			}

			if (!this.getParamValueBool(PN_EXCLUDE_LOGS, false)) {
				// 日志
				DBRequest lder = new LogDeserializer();
				lder.setParameter(PUNID_PARAM_NAME, unid);
				this.setNextRequest(lder);
			}

			// 额外参数
			DBRequest pder = new ParameterDeserializer();
			pder.setParameter(PUNID_PARAM_NAME, unid);
			this.setNextRequest(pder);
		}
		return result;
	}

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	@Override
	public Object deserialize(String src, Class<?> cls) {

		this.setParameter(UNID_PARAM_NAME, src);

		this.sendRequest();
		return this.getResult();

	}

	/**
	 * 重载build
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		int count = 0;

		Document x = null;
		ArrayList<Document> al = null;

		try {
			while (dr.next()) {
				count++;
				if (count > 1) {
					if (al == null) al = new ArrayList<Document>();
					al.add(x);
				}

				x = Document.newBlankDocument();

				x.setUNID(dr.getString(1));
				x.setSource(dr.getEnumGeneric(2, Source.class, Source.UserDefine));
				x.setCreator(dr.getString(3));
				x.setCreated(dr.getString(4));
				x.setModified(dr.getString(5));
				x.setModifier(dr.getString(6));
				x.setSort(dr.getInt(7));
				x.setDescription(dr.getString(8));
				x.setName(dr.getString(9));
				x.setAlias(dr.getString(10));
				x.setCategory(dr.getString(11));
				// x.setAbstract(dr.getString(12));
				x.setClickCount(dr.getInt(13));
				x.setTitle(dr.getString(14));
				x.setBusinessTitle(dr.getString(15));
				x.setState(dr.getInt(16));
				x.setProcessSequence(dr.getInt(17));
				x.setBusinessSequence(dr.getInt(18));
				x.setConfigResource(dr.getString(19));
				x.setPUNID(dr.getString(20));
				x.setType(dr.getEnumGeneric(21, DocumentType.class, DocumentType.Normal));
				x.setFormAlias(dr.getString(22));
				x.setProcessYear(dr.getInt(23));
				x.setBusinessYear(dr.getInt(24));
				x.setRankCount(dr.getInt(25));
				x.setRankValue(dr.getInt(26));
				x.setTag(dr.getString(27));
				String scs = dr.getString(28);

				List<DBRequest> reqs = request.getChainChildren();
				Object result = null;
				for (DBRequest r : reqs) {
					if (r == null) continue;
					result = r.getResult();
					if (r instanceof SecurityDeserializer) {
						if (result != null && result instanceof Security) x.setSecurity((Security) result);
						if (scs != null && scs.equalsIgnoreCase(Security.PUBLISH_TO_ALL_FLAG)) {
							x.getSecurity().setSecurityLevel(Role.ROLE_ANONYMOUS_SC, SecurityLevel.View.getIntValue());
						} else if (scs != null && scs.equalsIgnoreCase(Security.PUBLISH_TO_USER_FLAG)) {
							x.getSecurity().setSecurityLevel(Role.ROLE_DEFAULT_SC, SecurityLevel.View.getIntValue());
						}
					} else if (r instanceof ParameterDeserializer) {
						if (result != null && result instanceof List) {
							x.setParameters((List<Parameter>) result);
						} else if (result != null && result instanceof Parameter) {
							List<Parameter> list = new ArrayList<Parameter>();
							list.add((Parameter) result);
							x.setParameters(list);
						}
					} else if (r instanceof AccessoryDeserializer) {
						if (result != null && result instanceof List) {
							x.setAccessories((List<Accessory>) result);
						} else if (result != null && result instanceof Accessory) {
							List<Accessory> list = new ArrayList<Accessory>();
							list.add((Accessory) result);
							x.setAccessories(list);
						}
					} else if (r instanceof OpinionDeserializer) {
						if (result != null && result instanceof List) {
							x.setOpinions((List<Opinion>) result);
						} else if (result != null && result instanceof Opinion) {
							List<Opinion> list = new ArrayList<Opinion>();
							list.add((Opinion) result);
							x.setOpinions(list);
						}
					} else if (r instanceof FieldDeserializer) {
						if (result != null && result instanceof Map) {
							Map<String, String> m = (Map<String, String>) result;
							for (String k : m.keySet()) {
								x.setFieldValue(k, m.get(k));
							}
						}
					} else if (r instanceof LogDeserializer) {
						if (result != null && result instanceof List) {
							x.setLogs((List<Log>) result);
						} else if (result != null && result instanceof Log) {
							List<Log> list = new ArrayList<Log>();
							list.add((Log) result);
							x.setLogs(list);
						}
					}
				}// for end

			}// while end
			if (al != null) al.add(x);
			return (count == 1 ? x : (count > 1 ? al : null));
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return null;
	}
}

