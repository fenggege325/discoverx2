/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 插入Role资源对象到数据库表的数据库请求类。
 * 
 * <p>
 * 通过getResource获取要插入的资源。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class RoleInserter extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public RoleInserter() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("insert into t_role (c_unid,c_name,c_alias,c_description,c_created,c_creator,c_modified,c_modifier,c_sort,c_source,c_securityCode,c_punid,c_securityRange)");
		sql.append(" values (");
		sql.append("?,?,?,?,?,?,?,?,?,?,?,?,?");
		sql.append(")");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		Role x = (Role) this.getResource();

		try {
			cw.setString(1, x.getUNID());
			cw.setString(2, x.getName());
			cw.setString(3, x.getAlias());
			cw.setString(4, x.getDescription());
			cw.setString(5, x.getCreated());
			cw.setString(6, x.getCreator());
			cw.setString(7, x.getModified());
			cw.setString(8, x.getModifier());
			cw.setInt(9, x.getSort());
			cw.setInt(10, x.getSource().getIntValue());
			cw.setInt(11, x.getSecurityCode());
			cw.setString(12, StringUtil.getValueString(x.getPUNID(), ""));
			cw.setInt(13, x.getSecurityRange());
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end
}

