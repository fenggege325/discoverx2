/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 获取系统所有非系统角色参与者信息的数据库请求类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ParticipantsOfRoleFetcher extends DBRequest implements ResultBuilder {

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (punid == null || punid.isEmpty()) {
			sql.append("select c_unid,c_name,c_alias,c_sort,c_securitycode from t_role where c_source<4 order by c_sort,c_name");
		} else {
			sql.append("select c_unid,c_name,c_alias,c_sort,c_securitycode from t_role where c_source<4 and (c_punid='" + punid + "' or c_punid is null or c_punid='') order by c_sort,c_name");
		}
		result.setSql(sql.toString());

		return result;
	}

	/**
	 * 重载build：返回角色参与者（{@link ParticipantTree}）列表。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		ArrayList<ParticipantTree> al = null;
		int count = 0;

		ParticipantTree x = null;
		try {
			while (dr.next()) {
				count++;

				if (al == null) al = new ArrayList<ParticipantTree>();
				if (count > 1) {
					al.add(x);
				}

				// 顺序：c_unid,c_name,c_alias,c_sort,c_securitycode
				x = new ParticipantTree();
				x.setUNID(dr.getString(1));
				x.setName(dr.getString(2));
				x.setAlias(dr.getString(3));
				x.setSort(dr.getInt(4));
				x.setSecurityCode(dr.getInt(5));
				x.setParticipantType(ParticipantType.Role);
				x.setSortId("0|" + x.getSort());
			}// while end
			if (al != null) al.add(x);
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}

		return al;
	}
}

