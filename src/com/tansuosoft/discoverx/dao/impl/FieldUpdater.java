/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Field;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 更新{@link Field}对象到数据库表的数据库请求类。
 * 
 * <p>
 * 只能更新与表单字段关联的Field对象。<br/>
 * 通过getParameterObject(ENTITY_PARAM_NAME)获取要更新的对象。<br/>
 * 通过getParameterObject(PARENT_PARAM_NAME)获取要更新的对象所属文档资源。<br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FieldUpdater extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public FieldUpdater() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("update t_item set c_value=?,c_clobvalue=? where c_name=? and c_punid=?");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		Field x = (Field) this.getParameterObject(ENTITY_PARAM_NAME);
		if (x == null) throw new RuntimeException("无法获取字段Field对象。");
		Item item = x.getItem();
		if (item == null) throw new RuntimeException("无法获取字段Field对象绑定的表单字段Item对象。");
		Resource parent = this.getParameterResource(PARENT_PARAM_NAME);
		if (parent == null || !(parent instanceof Document)) throw new RuntimeException("无法获取字段所属文档资源。");
		String punid = parent.getUNID();

		try {
			String val = StringUtil.getValueString(x.getValue(), ((Document) parent).getItemValue(x.getName()));
			if (val == null) val = "";
			byte bs[] = val.getBytes("utf-8");
			if (bs.length >= CommonConfig.getInstance().getItemValueStroageBoundaryLength()) {
				cw.setString(1, "");
				cw.setString(2, val);
			} else {
				cw.setString(1, val);
				cw.setString(2, "");
			}
			cw.setString(3, x.getName());
			cw.setString(4, punid);
		} catch (SQLException e) {
			FileLogger.error(e);
		} catch (UnsupportedEncodingException e) {
			FileLogger.error(e);
		}
	}// func end
}

