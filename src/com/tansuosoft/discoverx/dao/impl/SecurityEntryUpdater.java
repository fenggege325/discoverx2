/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.UpdateFailConditionalDBRequestAdapter;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 更新SecurityEntry对象到数据库表的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#ENTITY_PARAM_NAME}参数名获取要更新的{@link SecurityEntry}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SecurityEntryUpdater extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public SecurityEntryUpdater() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("update t_security set c_securityLevel=?,c_workflowLevel=?,c_securityLevel1=?,c_securityLevel2=?,c_securityLevel3=?,c_securityLevel4=?,c_securityLevel5=?,c_securityLevel6=?,c_securityLevel7=?,c_securityLevel8=?,c_workflowLevel1=?,c_workflowLevel2=?,c_workflowLevel3=?,c_workflowLevel4=?,c_workflowLevel5=?,c_workflowLevel6=?,c_workflowLevel7=?,c_workflowLevel8=? where c_code=? and c_punid=?");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		SecurityEntryInserter inserter = new SecurityEntryInserter();
		UpdateFailConditionalDBRequestAdapter adapter = new UpdateFailConditionalDBRequestAdapter(inserter);
		adapter.setParameter(ENTITY_PARAM_NAME, this.getParameterObject(ENTITY_PARAM_NAME));
		this.setNextRequest(adapter);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		SecurityEntry x = (SecurityEntry) this.getParameterObject(ENTITY_PARAM_NAME);

		try {
			int sl = x.getSecurityLevel();
			int wl = x.getWorkflowLevel();
			cw.setInt(1, sl);
			cw.setInt(2, wl);
			int v = 0;
			int idx = 2;
			for (int i = 0; i < 8; i++) {
				idx++;
				v = (int) java.lang.Math.pow(2, i);
				if ((sl & v) == v) {
					cw.setInt(idx, 1);
				} else {
					cw.setInt(idx, 0);
				}
			}

			for (int i = 0; i < 8; i++) {
				idx++;
				v = (int) java.lang.Math.pow(2, i);
				if ((wl & v) == v) {
					cw.setInt(idx, 1);
				} else {
					cw.setInt(idx, 0);
				}
			}

			cw.setInt(idx + 1, x.getSecurityCode());
			cw.setString(idx + 2, x.getPUNID());
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end
}

