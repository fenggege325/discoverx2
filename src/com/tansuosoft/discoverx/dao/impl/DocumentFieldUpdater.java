/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.dao.UpdateSuccessConditionalDBRequestAdapter;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.ItemType;
import com.tansuosoft.discoverx.model.PersistenceState;
import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 更新文档某个字段值的数据库请求类。
 * 
 * <p>
 * 通过名为“{@link DBRequest#ENTITY_PARAM_NAME}”的参数获取要更改其字段值的文档对象。<br/>
 * 通过名为“{@link #FIELD_NAME_PARAM_NAME}”的参数获取要更改的文档字段的名称。<br/>
 * 通过名为“{@link #FIELD_VALUE_PARAM_NAME}”的参数获取要更改的文档字段的值。<br/>
 * </p>
 * <p>
 * 注：文档必须已经保存于数据库中才能使用此对象，如果写入成功，将同步修改文档对象的字段值。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentFieldUpdater extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public DocumentFieldUpdater() {
	}

	/**
	 * 用于获取字段名的额外参数名称：“FIELD_NAME”。
	 */
	public static final String FIELD_NAME_PARAM_NAME = "FIELD_NAME";
	/**
	 * 用于获取字段值的额外参数名称：“FIELD_VALUE”。
	 */
	public static final String FIELD_VALUE_PARAM_NAME = "FIELD_VALUE";

	private boolean isBuiltinField = false;
	private boolean insertFlag = false;
	private Document m_document = null;
	private String fn = null;

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		fn = this.getParamValueString(FIELD_NAME_PARAM_NAME, null);
		if (fn == null || fn.length() == 0) throw new RuntimeException("没有提供有效的要更新的文档字段名称！");
		m_document = (Document) this.getResource();
		if (m_document == null) throw new RuntimeException("没有提供有效的目标文档对象！");

		this.isBuiltinField = Document.isBuiltinField(fn);
		if (this.isBuiltinField) {
			sql.append("update t_document set c_").append(fn).append("=? where c_unid=?");

			// 写入数据库成功后更新文档属性值
			DBRequest dbrend = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					Document doc = (Document) this.getResource();
					String fn = this.getParamValueString(FIELD_NAME_PARAM_NAME, null);
					Object fv = this.getParameter(FIELD_VALUE_PARAM_NAME);
					ObjectUtil.propSetValue(doc, fn, fv);
					doc.setFieldPersistenceState(fn, PersistenceState.Raw);
					return SQLWrapper.getNonRequestSQLWrapper();
				}

				/**
				 * 重载getResult
				 * 
				 * @see com.tansuosoft.discoverx.dao.DBRequest#getResult()
				 */
				@Override
				public Object getResult() {
					DBRequest parent = this.getParentRequest();
					return (parent == null ? 0 : parent.getResult());
				}
			};
			DBRequest dbra = new UpdateSuccessConditionalDBRequestAdapter(dbrend);
			this.copyParametersTo(dbra);
			this.setNextRequest(dbra);
		} else {
			String fa = m_document.getFormAlias();
			if (fa == null || fa.length() == 0) throw new RuntimeException("没有提供有效的文档所属表单别名！");

			PersistenceState ps = m_document.getFieldPersistenceState(fn);
			insertFlag = (ps == PersistenceState.New);
			if (insertFlag) {
				sql.append("insert into t_item (c_value,c_clobvalue,c_punid,c_name,c_id) values (?,?,?,?,?)");
			} else {
				sql.append("update t_item set c_value=?,c_clobvalue=? where c_punid=? and c_name=?");
			}
			Item item = m_document.getItem(fn);
			if (item == null) throw new RuntimeException("无法获取文档中“" + fn + "”对应的字段信息！");
			ItemType itemType = item.getType();
			if (itemType.isFormViewTableColumn()) {
				DBRequest next = new FormViewColumnUpdater();
				this.copyParametersTo(next);
				this.setNextRequest(next);
				this.setUseTransaction(true);
			} // if end

			// 写入数据库成功后更新文档字段值
			DBRequest dbrend = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					Document doc = (Document) this.getResource();
					String fn = this.getParamValueString(FIELD_NAME_PARAM_NAME, null);
					String fv = this.getParamValueString(FIELD_VALUE_PARAM_NAME, null);
					doc.replaceItemValue(fn, fv);
					doc.setFieldPersistenceState(fn, PersistenceState.Raw);
					return SQLWrapper.getNonRequestSQLWrapper();
				}

				/**
				 * 重载getResult
				 * 
				 * @see com.tansuosoft.discoverx.dao.DBRequest#getResult()
				 */
				@Override
				public Object getResult() {
					DBRequest parent = this.getParentRequest();
					return (parent == null ? 0 : parent.getResult());
				}

			};
			DBRequest dbra = new UpdateSuccessConditionalDBRequestAdapter(dbrend);
			this.copyParametersTo(dbra);
			this.setNextRequest(dbra);
		} // else end

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		String unid = m_document.getUNID();
		String fv = request.getParamValueString(FIELD_VALUE_PARAM_NAME, null);
		if (fv == null) fv = "";
		try {
			if (this.isBuiltinField) {
				if (Document.isBuiltinFieldPrimitive(fn)) {
					long l = StringUtil.getValueLong(fv, 0);
					cw.setLong(1, l);
				} else {
					cw.setString(1, fv);
				}
				cw.setString(2, unid);
			} else {
				if (fv != null && fv.length() > 2048) {
					cw.setString(1, "");
					cw.setString(2, fv);
					// Reader reader = new BufferedReader(new StringReader(fv));
					// cw.setCharacterStream(2, reader, fv.length());
				} else {
					cw.setString(1, fv);
					cw.setString(2, "");
				}
				cw.setString(3, unid);
				cw.setString(4, fn);
				if (this.insertFlag) cw.setLong(5, SequenceProvider.getInstance("t_item").getSequence());
			}
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}
}

