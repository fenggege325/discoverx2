/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Resource;

/**
 * 从表中删除文档资源{@link Document}对象的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#getResource()}获取要删除的文档资源。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentRemover extends DBRequest {

	/**
	 * 缺省构造器。
	 */
	public DocumentRemover() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		Resource r = this.getResource();
		if (r == null || !(r instanceof Document)) throw new RuntimeException("没有提供文档资源！");
		Document doc = (Document) r;
		String unid = doc.getUNID();

		sql.append("delete from t_document where c_unid='").append(unid).append("'");
		result.setSql(sql.toString());

		// --字段
		DBRequest dbrf = new FieldRemover();
		dbrf.setParameter(DBRequest.PUNID_PARAM_NAME, unid);
		this.setNextRequest(dbrf);

		// --视图查询表
		DBRequest dbrv = new FormViewRemover();
		this.copyParametersTo(dbrv);
		this.setNextRequest(dbrv);

		this.setUseTransaction(true);

		return result;
	}
}

