/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 从表中反序列化{@link Security}对象的数据库请求类。
 * 
 * <p>
 * 必须提供名为{@link DBRequest#PUNID_PARAM_NAME}的参数，此参数表示要反序列化的{@link Security}对象及其包含的{@link SecurityEntry}对象所属资源的UNID。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SecurityDeserializer extends DBRequest implements ResultBuilder {

	/**
	 * 缺省构造器。
	 */
	public SecurityDeserializer() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();
		sql.append("select c_code,c_securityLevel,c_workflowLevel from t_security where c_punid=? order by c_code");
		result.setSql(sql.toString());
		result.setParameterized(true);
		this.setParametersSetter(PUNIDSETTER);
		return result;
	}

	/**
	 * 重载build：返回构造好的{@link Security}对象实例。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		int count = 0;

		SecurityEntry x = null;
		ArrayList<SecurityEntry> al = null;

		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		Resource r = this.getResource();
		try {
			while (dr.next()) {
				count++;
				if (al == null) al = new ArrayList<SecurityEntry>();
				if (count > 1) {
					al.add(x);
				}

				x = new SecurityEntry();
				x.setPUNID(punid);
				x.setSecurityCode(dr.getInt(1));
				x.setSecurityLevel(dr.getInt(2));
				x.setWorkflowLevel(dr.getInt(3));

			}// while end
			if (al != null) al.add(x);
			Security ret = new Security(punid);
			ret.setSecurityEntries(al);
			if (r != null) r.setSecurity(ret);
			return ret;
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return null;
	}

	/**
	 * 从名为“punid”的参数名中获取参数值并将参数值结果设置给sql语句的第一个动态参数的{@link ParametersSetter}实现对象。
	 */
	protected static final ParametersSetter PUNIDSETTER = new ParametersSetter() {
		@Override
		public void setParameters(DBRequest request, CommandWrapper cw) {
			String punid = request.getParamValueString(PUNID_PARAM_NAME, "");
			try {
				cw.setString(1, punid);
			} catch (SQLException e) {
				FileLogger.error(e);
			}
		}
	};
}

