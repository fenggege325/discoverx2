/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Accessory;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 更新Accessory资源对象到数据库表的数据库请求类。
 * 
 * <p>
 * 通过getResource获取要更新的资源。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class AccessoryUpdater extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public AccessoryUpdater() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("update t_accessory set c_unid=?,c_creator=?,c_modified=?,c_modifier=?,c_sort=?,c_source=?,c_created=?,c_description=?,c_category=?,c_name=?,c_alias=?,c_punid=?,c_parentDirectory=?,c_fileName=?,c_template=?,c_codeTemplate=?,c_accessoryType=?,c_size=?,c_state=? where c_unid=?");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		Accessory x = (Accessory) this.getResource();

		try {
			cw.setString(1, x.getUNID());
			cw.setString(2, x.getCreator());
			cw.setString(3, x.getModified());
			cw.setString(4, x.getModifier());
			cw.setInt(5, x.getSort());
			cw.setInt(6, x.getSource().getIntValue());
			cw.setString(7, x.getCreated());
			cw.setString(8, x.getDescription());
			cw.setString(9, x.getCategory());
			cw.setString(10, x.getName());
			cw.setString(11, x.getAlias());
			cw.setString(12, x.getPUNID());
			cw.setString(13, x.getParentDirectory());
			cw.setString(14, x.getFileName());
			cw.setString(15, x.getStyleTemplate());
			cw.setString(16, x.getCodeTemplate());
			cw.setString(17, x.getAccessoryType());
			cw.setInt(18, x.getSize());
			cw.setString(19, x.getState());
			cw.setString(20, x.getUNID());
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end
}

