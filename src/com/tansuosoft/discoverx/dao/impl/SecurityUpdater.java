/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.Role;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 更新{@link Security}对象包含的内容到数据库文档表中相关字段的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#ENTITY_PARAM_NAME}参数名获取要更新的{@link Security}对象。<br/>
 * 如果通过名为“state”的参数名设置了要更新的文档的文档状态值（参考{@link com.tansuosoft.discoverx.model.Document#getState()}），则会同步更新文档状态值到数据库表。<br/>
 * 如果state的值中没有公开标记，则删除t_security中的对应匿名角色、所有注册用户角色信息。<br/>
 * 同时会从名为“c_others”的参数获取值并写入t_document.c_others，取不到值则写入空串。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SecurityUpdater extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public SecurityUpdater() {
	}

	private int m_state = -1;

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		m_state = this.getParamValueInt("state", -1);
		if (m_state > 0) {
			sql.append("update t_document set c_securityCodes=?,c_state=?,c_others=? where c_unid=?");
			final Security x = (Security) this.getParameterObject(ENTITY_PARAM_NAME);
			if (x != null) {
				if ((m_state & DocumentState.Published.getIntValue()) != DocumentState.Published.getIntValue()) {
					DBRequest delAllUserSecurityEntry = new DBRequest() {
						@Override
						protected SQLWrapper buildSQL() {
							SQLWrapper result = new SQLWrapper();
							result.setRequestType(RequestType.NonQuery);
							result.setSql(String.format("delete from t_security where c_punid='%1$s' and c_code=%2$d", x.getPUNID(), Role.ROLE_DEFAULT_SC));
							return result;
						}
					};
					this.setNextRequest(delAllUserSecurityEntry);
					DBRequest delAnonymousSecurityEntry = new DBRequest() {
						@Override
						protected SQLWrapper buildSQL() {
							SQLWrapper result = new SQLWrapper();
							result.setRequestType(RequestType.NonQuery);
							result.setSql(String.format("delete from t_security where c_punid='%1$s' and c_code=%2$d", x.getPUNID(), Role.ROLE_ANONYMOUS_SC));
							return result;
						}
					};
					this.setNextRequest(delAnonymousSecurityEntry);
					this.setUseTransaction(true);
				}
			}
		} else {
			sql.append("update t_document set c_securityCodes=?,c_others where c_unid=?");
		}

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		Security x = (Security) this.getParameterObject(ENTITY_PARAM_NAME);
		if (x == null) throw new RuntimeException("没有提供有效的Security对象！");
		String others = request.getParamValueString("c_others", "");
		try {
			String scs = x.toString();
			String unid = x.getPUNID();
			if (m_state > 0) {
				cw.setString(1, scs);
				cw.setInt(2, m_state);
				cw.setString(3, others);
				cw.setString(4, unid);
			} else {
				cw.setString(1, scs);
				cw.setString(2, others);
				cw.setString(2, unid);
			}
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end
}

