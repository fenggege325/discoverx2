/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.bll.view.FormViewTable;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 更新文档某个字段值到视图查询表的数据库请求类。
 * 
 * <p>
 * 通过名为“{@link DBRequest#ENTITY_PARAM_NAME}”的参数获取要更改其字段值的文档对象。<br/>
 * 通过名为“{@link DocumentFieldUpdater#FIELD_NAME_PARAM_NAME}”的参数获取要更改的文档字段的名称。<br/>
 * 通过名为“{@link DocumentFieldUpdater#FIELD_VALUE_PARAM_NAME}”的参数获取要更改的文档字段的值。<br/>
 * </p>
 * <p>
 * 注：文档必须已经保存于数据库中才能使用此对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FormViewColumnUpdater extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public FormViewColumnUpdater() {
	}

	private Document m_document = null;

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		m_document = (Document) this.getResource();
		if (m_document == null) throw new RuntimeException("没有提供有效的目标文档对象！");

		String fa = m_document.getFormAlias();
		if (fa == null || fa.length() == 0) throw new RuntimeException("文档所属表单别名未知！");

		String fn = this.getParamValueString(DocumentFieldUpdater.FIELD_NAME_PARAM_NAME, null);
		if (fn == null || fn.length() == 0) throw new RuntimeException("无法获取要更改的字段名");

		sql.append("update ").append(FormViewTable.FORM_VIEW_TABLE_PREFIX).append(fa).append(" set ").append(fn).append("=? where c_punid=?");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		String unid = m_document.getUNID();
		String fv = request.getParamValueString(DocumentFieldUpdater.FIELD_VALUE_PARAM_NAME, null);
		if (fv == null) fv = "";
		try {
			if (fv.length() > 2048) fv = "";
			cw.setString(1, fv);
			cw.setString(2, unid);
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end
}

