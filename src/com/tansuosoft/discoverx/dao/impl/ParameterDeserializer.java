/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.ParameterValueDataType;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 从表中反序列化Parameter对象的数据库请求类。
 * 
 * <p>
 * 必须提供punid参数用于获取所有punid对应的对象并返回包含Parameter对象集合的列表对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ParameterDeserializer extends DBRequest implements ResultBuilder {

	/**
	 * 缺省构造器。
	 */
	public ParameterDeserializer() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();
		sql.append("select c_punid,c_name,c_description,c_value,c_valueType,c_multiple,c_multipleValueDelima from t_parameter where c_punid=? order by c_name");
		result.setParameterized(true);
		this.setParametersSetter(SecurityDeserializer.PUNIDSETTER);
		result.setSql(sql.toString());

		return result;
	}

	/**
	 * 重载build：返回包含Parameter对象集合的列表对象。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		int count = 0;

		Parameter x = null;
		ArrayList<Parameter> al = null;
		Resource r = this.getResource();
		try {
			while (dr.next()) {
				count++;
				if (al == null) al = new ArrayList<Parameter>();
				if (count > 1) {
					al.add(x);
				}

				x = new Parameter();

				x.setPUNID(dr.getString(1));
				x.setName(dr.getString(2));
				x.setDescription(dr.getString(3));
				x.setValue(dr.getString(4));
				x.setValueType(dr.getEnumGeneric(5, ParameterValueDataType.class, ParameterValueDataType.Text));
				x.setMultiple(dr.getBool(6, false));
				x.setDelimiter(dr.getString(7));

			}// while end
			if (al != null) al.add(x);
			if (r != null) r.setParameters(al);
			return al;
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return null;
	}
}

