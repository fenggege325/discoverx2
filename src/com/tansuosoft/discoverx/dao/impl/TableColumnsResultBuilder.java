/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 构造表格字段信息列表并返回列表的ResultBuilder实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class TableColumnsResultBuilder implements ResultBuilder {

	/**
	 * 重载build：返回表包含的字段名(大写)列表。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		if (request == null || rawResult == null || !(rawResult instanceof DataReader)) return null;

		DataReader dr = (DataReader) rawResult;
		List<String> al = new ArrayList<String>();
		String colName = null;
		try {
			while (dr.next()) {
				colName = dr.getString(1);
				if (colName == null) continue;
				al.add(colName.toUpperCase());
			}
			return al;
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}

		return null;
	}

}

