/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.ResultBuildSequence;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 根据用户别名和密信息从表中反序列化User资源对象的数据库请求类。
 * 
 * <p>
 * 用于用户登录时验证是否允许登录并返回通过验证的User对象。
 * </p>
 * <p>
 * 必须通过名为“uid”和“password”的参数提供用户别名和密码信息。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class UserLoginByAlias extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public UserLoginByAlias() {
	}

	/**
	 * 重载：实现。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper r = new SQLWrapper();
		r.setSql("select c_unid,c_name,c_category,c_description,c_created,c_creator,c_modified,c_modifier,c_sort,c_source,c_alias,c_punid,c_password,c_accountType,c_securityCode,c_duty,c_profile from t_user where c_alias=? and c_password=? and c_accounttype=3");
		r.setParameterized(true);
		this.setResultBuildSequence(ResultBuildSequence.Queue);
		this.setResultBuilder(new UserDeserializer());
		// 安全
		DBRequest sder = new SecurityDeserializer();
		this.setNextRequest(sder);

		// 授权
		DBRequest xder = new AuthorityDeserializer();
		this.setNextRequest(xder);

		// 附加文件
		DBRequest ader = new AccessoryDeserializer();
		this.setNextRequest(ader);

		// 额外参数
		DBRequest pder = new ParameterDeserializer();
		this.setNextRequest(pder);

		return r;
	}

	/**
	 * 重载：实现。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		try {
			String alias = this.getParamValueString("uid", null);
			String pwd = this.getParamValueString("password", null);
			if (alias == null || alias.length() == 0) throw new RuntimeException("没有提供用户登录帐号！");
			if (pwd == null || pwd.length() == 0) throw new RuntimeException("没有提供用户登录密码！");
			cw.setString(1, alias);
			cw.setString(2, pwd);
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}
}

