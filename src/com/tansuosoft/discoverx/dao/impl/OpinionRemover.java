/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.Opinion;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 从表中删除对应{@link Opinion}资源对象的数据库请求类。
 * 
 * <p>
 * 如果提供名为“unid”的参数，则删除unid对应的意见记录，如果提供了名为“punid”的参数，则删除所有punid对应的意见记录。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class OpinionRemover extends DBRequest {

	/**
	 * 缺省构造器。
	 */
	public OpinionRemover() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String sqlSecurity = null;
		String sqlParameter = null;
		String sqlAccessory = null;
		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (StringUtil.isUNID(unid)) {
			sql.append("delete from t_opinion where c_unid='").append(unid).append("'");
			sqlSecurity = String.format("delete from t_security where c_punid='%1$s'", unid);
			sqlParameter = String.format("delete from t_parameter where c_punid='%1$s'", unid);
			sqlAccessory = String.format("delete from t_accessory where c_punid='%1$s'", unid);
		} else if (StringUtil.isUNID(punid)) {
			sql.append("delete from t_opinion where c_punid='").append(punid).append("'");
			sqlSecurity = String.format("delete from t_security where c_punid in (select c_unid from t_opinion where c_punid='%1$s')", punid);
			sqlParameter = String.format("delete from t_parameter where c_punid in (select c_unid from t_opinion where c_punid='%1$s')", punid);
			sqlAccessory = String.format("delete from t_accessory where c_punid in (select c_unid from t_opinion where c_punid='%1$s')", punid);
		}
		result.setSql(sql.toString());
		result.setRequestType(RequestType.NonQuery);
		DBRequest dbrSecurity = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql(this.getParamValueString("sql", null));
				result.setRequestType(RequestType.NonQuery);
				return result;
			}
		};
		dbrSecurity.setParameter("sql", sqlSecurity);
		this.setNextRequest(dbrSecurity);
		DBRequest dbrParameter = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql(this.getParamValueString("sql", null));
				result.setRequestType(RequestType.NonQuery);
				return result;
			}
		};
		dbrParameter.setParameter("sql", sqlParameter);
		this.setNextRequest(dbrParameter);
		DBRequest dbrAccessory = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql(this.getParamValueString("sql", null));
				result.setRequestType(RequestType.NonQuery);
				return result;
			}
		};
		dbrAccessory.setParameter("sql", sqlAccessory);
		this.setNextRequest(dbrAccessory);

		return result;
	}
}

