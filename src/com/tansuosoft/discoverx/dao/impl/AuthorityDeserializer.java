/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.Authority;
import com.tansuosoft.discoverx.model.AuthorityEntry;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.Deserializer;

/**
 * 从表中反序列化{@link Authority}对象的数据库请求类。
 * 
 * <p>
 * 必须提供名为{@link DBRequest#PUNID_PARAM_NAME}的参数（表示用户或群组的UNID）用于获取所有其对应的{@link AuthorityEntry}对象并返回包含这些{@link AuthorityEntry}对象的{@link Authority}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class AuthorityDeserializer extends DBRequest implements ResultBuilder, Deserializer {

	/**
	 * 缺省构造器。
	 */
	public AuthorityDeserializer() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("select a.c_authunid,a.c_authdirectory,b.c_securitycode,b.c_name from t_authority a,t_role b where a.c_authunid=b.c_unid and a.c_unid=?");
		sql.append(" union ");
		sql.append("select a.c_authunid,a.c_authdirectory,b.c_securitycode,b.c_name from t_authority a,t_group b where a.c_authunid=b.c_unid and a.c_unid=?");
		sql.append(" union ");
		sql.append("select a.c_authunid,a.c_authdirectory,b.c_securitycode,b.c_name from t_authority a,t_role b where a.c_authunid=b.c_unid and a.c_unid in (select a.c_authunid from t_authority a,t_group b where a.c_authunid=b.c_unid and a.c_unid=?)");
		result.setParameterized(true);
		this.setParametersSetter(new ParametersSetter() {
			@Override
			public void setParameters(DBRequest request, CommandWrapper cw) {
				String punid = request.getParamValueString(PUNID_PARAM_NAME, "");
				try {
					cw.setString(1, punid);
					cw.setString(2, punid);
					cw.setString(3, punid);
				} catch (SQLException e) {
					FileLogger.error(e);
				}
			}
		});
		result.setSql(sql.toString());
		return result;
	}

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	@Override
	public Object deserialize(String src, Class<?> cls) {
		Object o = this.getResult();
		if (o == null) {
			this.setParameter(UNID_PARAM_NAME, src);
			this.sendRequest();
			o = this.getResult();
		}
		return o;
	}

	/**
	 * 重载build：返回包含AuthorityEntry信息的Authority对象。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		int count = 0;

		AuthorityEntry x = null;
		ArrayList<AuthorityEntry> al = null;

		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		Resource r = this.getResource();
		try {
			while (dr.next()) {
				count++;
				if (al == null) al = new ArrayList<AuthorityEntry>();
				if (count > 1) {
					al.add(x);
				}

				x = new AuthorityEntry();
				x.setUNID(dr.getString(1));
				x.setDirectory(dr.getString(2));
				x.setSecurityCode(dr.getInt(3));
				x.setName(dr.getString(4));
			}// while end
			if (al != null) al.add(x);
			Authority ret = new Authority(punid);
			ret.setAuthorityEntries(al);
			if (r != null && r instanceof User) ((User) r).setAuthority(ret);
			return ret;
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return null;
	}
}

