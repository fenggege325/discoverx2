/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.UpdateFailConditionalDBRequestAdapter;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 更新{@link Parameter}对象到数据库表的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#ENTITY_PARAM_NAME}参数名获取要更新的{@link Parameter}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ParameterUpdater extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public ParameterUpdater() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("update t_parameter set c_description=?,c_value=?,c_valueType=?,c_multiple=?,c_multipleValueDelima=? where c_name=? and c_punid=?");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		ParameterInserter inserter = new ParameterInserter();
		UpdateFailConditionalDBRequestAdapter adapter = new UpdateFailConditionalDBRequestAdapter(inserter);
		adapter.setParameter(ENTITY_PARAM_NAME, this.getParameterObject(ENTITY_PARAM_NAME));
		this.setNextRequest(adapter);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		Parameter x = (Parameter) this.getParameterObject(ENTITY_PARAM_NAME);

		try {
			cw.setString(1, x.getDescription());
			cw.setString(2, x.getValue());
			cw.setInt(3, x.getValueType().getIntValue());
			cw.setString(4, x.getMultiple() ? "y" : "n");
			cw.setString(5, x.getDelimiter());

			cw.setString(6, x.getName());
			cw.setString(7, x.getPUNID());
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}// func end

}

