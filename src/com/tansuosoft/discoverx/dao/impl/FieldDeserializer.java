/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 从表中反序列化文档字段信息的数据库请求类。
 * 
 * <p>
 * 必须提供punid参数用于获取所有punid对应的字段信息并返回包含这些信息的哈希集合。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FieldDeserializer extends DBRequest implements ResultBuilder {

	/**
	 * 缺省构造器。
	 */
	public FieldDeserializer() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (punid != null) {
			sql.append("select c_punid,c_name,c_value,c_clobvalue from t_item where c_punid='").append(punid).append("'");
		}
		result.setSql(sql.toString());

		return result;
	}

	/**
	 * 重载build：返回包含字段名和字段值文本一一对应的{@link java.util.Map}映射。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		int count = 0;

		Map<String, String> ht = null;

		try {
			String name = null;
			String val = null;
			while (dr.next()) {
				count++;
				if (ht == null) ht = new HashMap<String, String>();
				if (count > 1) {
					ht.put(name, val);
				}

				name = dr.getString(2);
				val = dr.getString(3);
				if (val == null || val.length() == 0) {
					val = dr.getString(4);
				}
			}// while end
			if (ht != null) ht.put(name, val);
			return ht;
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return null;
	}
}

