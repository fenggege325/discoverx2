/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Field;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 插入{@link Field}对象到数据库表的数据库请求类。
 * 
 * <p>
 * 只能插入与表单字段关联的{@link Field}对象。<br/>
 * 通过getParameterObject(ENTITY_PARAM_NAME)获取要插入的{@link Field}对象。<br/>
 * 通过getParameterObject(PARENT_PARAM_NAME)获取要更新的对象所属文档{@link Document}资源。<br/>
 * </p>
 * 
 * 
 * @author coca@tansuosoft.cn
 */
public class FieldInserter extends DBRequest implements ParametersSetter {
	/**
	 * 缺省构造器。
	 */
	public FieldInserter() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("insert into t_item (c_id,c_punid,c_name,c_value,c_clobvalue)");
		sql.append(" values (");
		sql.append("?,?,?,?,?");
		sql.append(")");

		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		Field x = (Field) this.getParameterObject(ENTITY_PARAM_NAME);
		if (x == null) throw new RuntimeException("无法获取文档字段Field对象。");
		Item item = x.getItem();
		if (item == null) throw new RuntimeException("无法获取文档字段Field对象绑定的表单字段Item对象。");
		Resource parent = this.getParameterResource(PARENT_PARAM_NAME);
		if (parent == null || !(parent instanceof Document)) throw new RuntimeException("无法获取字段所属资源。");
		String punid = parent.getUNID();
		try {
			cw.setLong(1, SequenceProvider.getInstance("t_item").getSequence());
			cw.setString(2, punid);
			cw.setString(3, x.getName());
			String val = StringUtil.getValueString(x.getValue(), ((Document) parent).getItemValue(x.getName()));
			if (val == null) val = "";
			byte bs[] = val.getBytes("utf-8");
			if (bs.length >= CommonConfig.getInstance().getItemValueStroageBoundaryLength()) {
				cw.setString(4, "");
				cw.setString(5, val);
			} else {
				cw.setString(4, val);
				cw.setString(5, "");
			}
		} catch (SQLException e) {
			FileLogger.error(e);
		} catch (UnsupportedEncodingException e) {
			FileLogger.error(e);
		}
	}// func end
}

