/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.SQLWrapper;

/**
 * 从表中删除对应Group资源对象的数据库请求类。
 * 
 * <p>
 * 如果提供unid参数，则删除unid对应的对象，如果提供了punid参数，则删除所有punid对应的对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class RoleRemover extends DBRequest {

	/**
	 * 缺省构造器。
	 */
	public RoleRemover() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (unid != null) {
			sql.append("delete from t_role where c_unid='").append(unid).append("'");
		} else if (punid != null) {
			sql.append("delete from t_role where c_punid='").append(punid).append("'");
		}
		result.setSql(sql.toString());

		return result;
	}
}

