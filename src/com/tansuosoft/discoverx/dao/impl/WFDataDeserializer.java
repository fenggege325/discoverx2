/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.WFData;

/**
 * 从表中反序列化{@link WFData}对象的数据库请求类。
 * 
 * <p>
 * 如果提供了名为{@link DBRequest#UNID_PARAM_NAME}的参数，则获取参数值对应的{@link WFData}对象并返回；<br/>
 * 如果提供了名为{@link DBRequest#PUNID_PARAM_NAME}的参数，则获取所有所属文档UNID为提供的参数值的{@link WFData}对象的列表。<br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class WFDataDeserializer extends DBRequest implements ResultBuilder {
	private boolean m_isSingle = false;

	/**
	 * 缺省构造器。
	 */
	public WFDataDeserializer() {
	}

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (unid != null) {
			m_isSingle = true;
			sql.append("select c_unid,c_punid,c_activity,c_workflow,c_instance,c_parentInstance,c_data,c_parentData,c_level,c_sublevel,c_sort,c_created,c_expect,c_notify,c_accomplished,c_done,c_derivative,c_params from t_wfdata where c_unid='").append(unid).append("'");
		} else if (punid != null) {
			sql.append("select c_unid,c_punid,c_activity,c_workflow,c_instance,c_parentInstance,c_data,c_parentData,c_level,c_sublevel,c_sort,c_created,c_expect,c_notify,c_accomplished,c_done,c_derivative,c_params from t_wfdata where c_punid='").append(punid).append("' order by c_created desc,c_sort asc");
		} else {
			throw new IllegalArgumentException("没有提供必要的请求参数信息!");
		}

		result.setSql(sql.toString());

		return result;
	}

	/**
	 * 重载build：返回包含WFData对象集合的列表对象。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		int count = 0;

		WFData x = null;
		ArrayList<WFData> al = null;

		try {
			while (dr.next()) {
				count++;
				if (al == null) al = new ArrayList<WFData>();
				if (count > 1) {
					al.add(x);
				}

				x = new com.tansuosoft.discoverx.workflow.WFData();

				x.setUNID(dr.getString(1));
				x.setPUNID(dr.getString(2));
				x.setActivity(dr.getString(3));
				x.setWorkflow(dr.getString(4));
				x.setInstance(dr.getInt(5));
				x.setParentInstance(dr.getInt(6));
				x.setData(dr.getInt(7));
				x.setParentData(dr.getInt(8));
				x.setLevel(dr.getInt(9));
				x.setSubLevel(dr.getInt(10));
				x.setSort(dr.getInt(11));
				x.setCreated(dr.getString(12));
				x.setExpect(dr.getString(13));
				x.setNotify(dr.getString(14));
				x.setAccomplished(dr.getString(15));
				x.setDone(dr.getBool(16, false));
				x.setDerivative(dr.getBool(17, false));
				x.setParams(dr.getString(18));
			}// while end
			if (al != null) al.add(x);
			return (m_isSingle ? (al == null ? null : al.get(0)) : al);
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		return null;
	}
}

