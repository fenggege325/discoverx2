/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.AccountType;
import com.tansuosoft.discoverx.model.Organization;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Source;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.Deserializer;

/**
 * 获取并构造指定单位及其部门树({@link Organization})的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#UNID_PARAM_NAME}参数名获取根节点UNID，必须。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class OrganizationDeserializerCommon extends DBRequest implements ResultBuilder, Deserializer {

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		if (!StringUtil.isUNID(unid)) throw new RuntimeException("没有指定有效的组织机构唯一标识(UNID)。");

		sql.append("select a.c_unid,a.c_name,a.c_category,a.c_description,a.c_created,a.c_creator,a.c_modified,a.c_modifier,a.c_sort,a.c_source,a.c_alias,a.c_punid,a.c_accountType,a.c_securityCode,a.c_sort from t_user a where a.c_unid='").append(unid).append("'");
		result.setSql(sql.toString());

		return result;
	}

	/**
	 * 重载build:根据查询结果构造单位及其部门树({@link Organization})并返回之。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		Organization root = null;
		try {
			while (dr.next()) {
				// 顺序：c_unid,c_name,c_category,c_description,c_created,c_creator,c_modified,c_modifier,c_sort,c_source,c_alias,c_punid,c_accountType,c_securityCode,level
				root = new Organization();
				root.setUNID(dr.getString(1));
				root.setFullName(dr.getString(2));
				root.setName(root.getFullName());
				root.setCategory(dr.getString(3));
				root.setDescription(dr.getString(4));
				root.setCreated(dr.getString(5));
				root.setCreator(dr.getString(6));
				root.setModified(dr.getString(7));
				root.setModifier(dr.getString(8));
				root.setSort(dr.getInt(9));
				root.setSource((Source) dr.getEnum(10, Source.UserDefine));
				root.setAlias(dr.getString(11));
				root.setPUNID(dr.getString(12));
				root.setAccountType(dr.getInt(13));
				root.setSecurityCode(dr.getInt(14));
				break;
			}// while end
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}
		if (root == null) return root;
		if (StringUtil.isBlank(root.getPUNID())) {
			subOrgFromPT(root, ParticipantTreeProvider.getInstance().getRoot());
		} else {
			ParticipantTree ptRoot = ParticipantTreeProvider.getInstance().getParticipantTree(root.getSecurityCode());
			subOrgFromPT(root, ptRoot);
		}
		return root;
	}

	/**
	 * 构造根组织包含的下级组织。
	 * 
	 * @param root
	 * @param ptRoot
	 */
	protected void subOrgFromPT(Organization root, ParticipantTree ptRoot) {
		if (root == null) return;
		List<ParticipantTree> ptChildren = null;
		if (ptRoot == null || (ptChildren = ptRoot.getChildren()) == null || ptChildren.isEmpty()) return;
		for (ParticipantTree x : ptChildren) {
			if (x == null || x.getParticipantType() != ParticipantType.Organization) continue;
			Organization orgChild = new Organization();
			orgChild.setCreated(null);
			orgChild.setModified(null);
			orgChild.setUNID(x.getUNID());
			orgChild.setName(x.getName());
			orgChild.setAlias(x.getAlias());
			orgChild.setAccountType(AccountType.Unit.getIntValue() + AccountType.InvalidAccount.getIntValue());
			orgChild.setSecurityCode(x.getSecurityCode());
			orgChild.setSort(x.getSort());
			root.addChild(orgChild);
			subOrgFromPT(orgChild, x);
		}
	}

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	@Override
	public Object deserialize(String src, Class<?> cls) {
		this.setParameter(UNID_PARAM_NAME, src);
		this.sendRequest();
		return this.getResult();
	}
}
