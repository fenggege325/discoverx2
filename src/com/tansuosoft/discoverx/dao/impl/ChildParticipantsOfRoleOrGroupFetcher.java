/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 获取指定静态群组或角色包含的用户/群组（仅针对角色）参与者的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#PUNID_PARAM_NAME}获取对应的静态群组或角色的UNID，必须。<br/>
 * 通过{@link DBRequest#UNID_PARAM_NAME}获取目标单位UNID，必须。
 * </p>
 * 
 * <p>
 * 如果如果参数值对应的unid是静态群组unid，那么返回属于此群组的用户参与者。<br/>
 * 如果如果参数值对应的unid是角色unid，那么返回属于此角色的用户参与者和群组参与者。<br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ChildParticipantsOfRoleOrGroupFetcher extends DBRequest implements ResultBuilder {
	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		if (punid == null) throw new RuntimeException("没有提供静态群组或者角色UNID。");
		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		if (unid == null) throw new RuntimeException("没有提供单位UNID。");
		sql.append("select c_unid,c_name,c_alias,c_sort,c_securitycode,pt from (");
		sql.append("select a.c_unid,a.c_name,a.c_alias,a.c_sort,a.c_securitycode,").append(ParticipantType.Person.getIntValue()).append(" pt from t_user a ,t_authority b where a.c_unid=b.c_unid and a.c_punid='").append(unid).append("' and b.c_authunid='").append(punid).append("'");
		sql.append(" union ");
		sql.append("select a.c_unid,a.c_name,a.c_alias,a.c_sort,a.c_securitycode,").append(ParticipantType.Group.getIntValue()).append(" pt from t_group a ,t_authority b where a.c_unid=b.c_unid and a.c_punid='").append(unid).append("' and b.c_authunid='").append(punid).append("'");
		sql.append(") t order by pt,c_sort,c_name");
		result.setSql(sql.toString());
		return result;
	}

	/**
	 * 重载build：返回群组参与者（{@link ParticipantTree}）列表。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		DataReader dr = (DataReader) rawResult;
		if (dr == null) return null;

		ArrayList<ParticipantTree> al = null;
		int count = 0;

		ParticipantTree x = null;
		try {
			while (dr.next()) {
				count++;

				if (al == null) al = new ArrayList<ParticipantTree>();
				if (count > 1) {
					al.add(x);
				}

				// 顺序：c_unid,c_name,c_alias,c_sort,c_securitycode
				x = new ParticipantTree();
				x.setUNID(dr.getString(1));
				x.setName(dr.getString(2));
				x.setAlias(dr.getString(3));
				x.setSort(dr.getInt(4));
				x.setSecurityCode(dr.getInt(5));
				x.setParticipantType(dr.getEnumGeneric(6, ParticipantType.class, ParticipantType.Person));
				x.setSortId("0|" + x.getSort());
			}// while end
			if (al != null && x != null) al.add(x);
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}

		return al;
	}
}

