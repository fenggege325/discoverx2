/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.impl;

import java.sql.SQLException;

import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.SequenceProvider;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.AuthorityEntry;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 插入{@link AuthorityEntry}对象到数据库表的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#ENTITY_PARAM_NAME}获取要插入的对象，必须同时提供名为{@link DBRequest#UNID_PARAM_NAME} 的参数和名为“directory”的参数（不提供“directory”参数则默认为"user"，即默认给用户授权），<br/>
 * 分别表示传入的 {@link AuthorityEntry}属于哪个资源unid和资源目录。<br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class AuthorityEntryInserter extends DBRequest implements ParametersSetter {

	/**
	 * 缺省构造器。
	 */
	public AuthorityEntryInserter() {
	}

	protected static final String DIR_PARAM_NAME = "directory";

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		AuthorityEntry p = (AuthorityEntry) this.getParameterObject(ENTITY_PARAM_NAME);
		String unid = this.getParamValueString(UNID_PARAM_NAME, null);
		if (p != null && unid != null && unid.length() > 0) {
			sql.append("insert into t_authority (c_id,c_unid,c_directory,c_authunid,c_authdirectory)");
			sql.append(" values (");
			sql.append("?,?,?,?,?");
			sql.append(")");
		} else {
			throw new RuntimeException("数据库请求参数错误：没有提供要必要的参数！");
		}
		result.setSql(sql.toString());
		result.setParameterized(true);
		result.setRequestType(RequestType.NonQuery);

		return result;
	}

	/**
	 * 重载SetParameter
	 * 
	 * @see com.tansuosoft.discoverx.dao.ParametersSetter#setParameters(com.tansuosoft.discoverx.dao.DBRequest, com.tansuosoft.discoverx.db.CommandWrapper)
	 */
	@Override
	public void setParameters(DBRequest request, CommandWrapper cw) {
		try {
			AuthorityEntry x = (AuthorityEntry) this.getParameterObject(ENTITY_PARAM_NAME);
			String unid = this.getParamValueString(UNID_PARAM_NAME, null);
			String directory = this.getParamValueString(DIR_PARAM_NAME, ResourceDescriptorConfig.getInstance().getResourceDirectory(User.class));
			if (x != null && unid != null && unid.length() > 0) {
				cw.setLong(1, SequenceProvider.getInstance("t_authority", 100).getSequence());
				cw.setString(2, unid);
				cw.setString(3, directory);
				cw.setString(4, x.getUNID());
				cw.setString(5, x.getDirectory());
			}
		} catch (SQLException e) {
			FileLogger.error(e);
		}
	}
}

