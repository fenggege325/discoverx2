/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 同时发送多条数据库请求时的请求结果构造顺序枚举值。
 * 
 * @see DBRequest#setResultBuildSequence(ResultBuildSequence)
 * @author coca@tansuosoft.cn
 */
public enum ResultBuildSequence implements EnumBase {
	/**
	 * 堆栈方式：先构造后续请求的结果（0）。
	 */
	Stack(0),
	/**
	 * 队列方式：后构造后续请求的结果（1）。
	 */
	Queue(1);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	ResultBuildSequence(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return ResultBuildSequence
	 */
	public ResultBuildSequence parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (ResultBuildSequence s : ResultBuildSequence.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return ResultBuildSequence.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return ResultBuildSequence
	 */
	public static ResultBuildSequence parse(int v) {
		for (ResultBuildSequence s : ResultBuildSequence.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

