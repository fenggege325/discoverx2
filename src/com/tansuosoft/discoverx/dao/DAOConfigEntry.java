/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

/**
 * 数据库请求信息描述配置项类。
 * 
 * <p>
 * 用于描述保存于配置文件中的数据访问请求类相关信息的类。
 * </p>
 * <p>
 * DBConfig配置文件中，可以获取到包含多条此类的实例的列表。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DAOConfigEntry extends SQLWrapper {

	/**
	 * 缺省构造器。
	 */
	public DAOConfigEntry() {
		super();
	}

	private String m_implement = null; // 实现类的全限定类名，必须。
	private String m_type = null; // 类型名，可选，默认为“com.tansuosoft.discoverx.dao.DBRequest”。
	private String m_resultBuilder = null; // 构造最终结果的实现类的全限定类名，可选。
	private String m_parametersSetter = null; // 为带参数SQL语句自动设置参数的实现类的全限定类名。
	private String m_description = null; // 说明。

	/**
	 * 返回实现类的全限定类名，必须。
	 * 
	 * @return String
	 */
	public String getImplement() {
		return this.m_implement;
	}

	/**
	 * 设置实现类的全限定类名，必须。
	 * 
	 * @param implement String
	 */
	public void setImplement(String implement) {
		this.m_implement = implement;
	}

	/**
	 * 返回类型名，可选。
	 * <p>
	 * 默认为“com.tansuosoft.discoverx.dao.DBRequest”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getType() {
		return this.m_type;
	}

	/**
	 * 设置类型名，可选。
	 * 
	 * @param type String
	 */
	public void setType(String type) {
		this.m_type = type;
	}

	/**
	 * 返回构造最终结果的实现类的全限定类名，可选。
	 * 
	 * @see DAOConfigEntry#setResultBuilder(String)
	 * @return String
	 */
	public String getResultBuilder() {
		return this.m_resultBuilder;
	}

	/**
	 * 设置构造最终结果的实现类的全限定类名，可选。
	 * 
	 * <p>
	 * 配置的类必须实现com.tansuosoft.discoverx.dao.ResultBuilder接口。
	 * </p>
	 * 
	 * @see ResultBuilder
	 * @param resultBuilder String
	 */
	public void setResultBuilder(String resultBuilder) {
		this.m_resultBuilder = resultBuilder;
	}

	/**
	 * 返回为带参数SQL语句自动设置参数结果的{@link ParametersSetter}实现类的全限定类名。
	 * 
	 * @return String
	 */
	public String getParametersSetter() {
		return this.m_parametersSetter;
	}

	/**
	 * 设置为带参数SQL语句自动设置参数结果的{@link ParametersSetter}实现类的全限定类名。
	 * 
	 * <p>
	 * 如果sql语句是带参数的，那么必须提供一个有效的实现类，否则可能导致出错。
	 * </p>
	 * 
	 * @param parametersSetter String
	 */
	public void setParametersSetter(String parametersSetter) {
		this.m_parametersSetter = parametersSetter;
	}

	/**
	 * 返回说明信息，可选。
	 * 
	 * @return String
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置说明信息。
	 * 
	 * @param description String
	 */
	public void setDescription(String description) {
		this.m_description = description;
	}
}

