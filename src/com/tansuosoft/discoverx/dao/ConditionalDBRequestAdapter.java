/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import java.sql.Connection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.db.Database;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;

/**
 * 依条件而执行的{@link DBRequest}对应的{@link ConditionalDBRequest}实现基类 。
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class ConditionalDBRequestAdapter extends DBRequest implements ConditionalDBRequest {
	private DBRequest m_dbrequest = null;

	/**
	 * 接收原始{@link DBRequest}对象的构造器。
	 * 
	 * @param dbrequest
	 */
	public ConditionalDBRequestAdapter(DBRequest dbrequest) {
		m_dbrequest = dbrequest;
		if (m_dbrequest == null) throw new IllegalArgumentException("必须提供有效的DBRequest对象。");
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#close()
	 */
	@Override
	public void close() {
		m_dbrequest.close();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param another
	 * @see com.tansuosoft.discoverx.dao.DBRequest#copyParametersTo(com.tansuosoft.discoverx.dao.DBRequest)
	 */
	@Override
	public void copyParametersTo(DBRequest another) {
		m_dbrequest.copyParametersTo(another);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param raw
	 * @param compare
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#encode(java.lang.String, java.lang.String)
	 */
	@Override
	public String encode(String raw, String compare) {
		return m_dbrequest.encode(raw, compare);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getChainChildren()
	 */
	@Override
	public List<DBRequest> getChainChildren() {
		return m_dbrequest.getChainChildren();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getDatabase()
	 */
	@Override
	public Database getDatabase() {
		return m_dbrequest.getDatabase();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getNextRequest()
	 */
	@Override
	public DBRequest getNextRequest() {
		return m_dbrequest.getNextRequest();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param name
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParameter(java.lang.String)
	 */
	@Override
	public Object getParameter(String name) {
		return m_dbrequest.getParameter(name);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParameterNames()
	 */
	@Override
	public Iterator<String> getParameterNames() {
		return m_dbrequest.getParameterNames();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param name
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParameterObject(java.lang.String)
	 */
	@Override
	public Object getParameterObject(String name) {
		return m_dbrequest.getParameterObject(name);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param name
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParameterResource(java.lang.String)
	 */
	@Override
	public Resource getParameterResource(String name) {
		return m_dbrequest.getParameterResource(name);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParameters()
	 */
	@Override
	public Map<String, Object> getParameters() {
		return m_dbrequest.getParameters();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParametersSetter()
	 */
	@Override
	public ParametersSetter getParametersSetter() {
		return m_dbrequest.getParametersSetter();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param paramName
	 * @param defaultReturn
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParamValueBool(java.lang.String, boolean)
	 */
	@Override
	public boolean getParamValueBool(String paramName, boolean defaultReturn) {
		return m_dbrequest.getParamValueBool(paramName, defaultReturn);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param paramName
	 * @param defaultReturn
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParamValueDouble(java.lang.String, double)
	 */
	@Override
	public double getParamValueDouble(String paramName, double defaultReturn) {
		return m_dbrequest.getParamValueDouble(paramName, defaultReturn);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param <T>
	 * @param paramName
	 * @param enumCls
	 * @param defaultReturn
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParamValueEnum(java.lang.String, java.lang.Class, java.lang.Object)
	 */
	@Override
	public <T> T getParamValueEnum(String paramName, Class<T> enumCls, T defaultReturn) {
		return m_dbrequest.getParamValueEnum(paramName, enumCls, defaultReturn);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param paramName
	 * @param defaultReturn
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParamValueFloat(java.lang.String, float)
	 */
	@Override
	public float getParamValueFloat(String paramName, float defaultReturn) {
		return m_dbrequest.getParamValueFloat(paramName, defaultReturn);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param paramName
	 * @param defaultReturn
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParamValueInt(java.lang.String, int)
	 */
	@Override
	public int getParamValueInt(String paramName, int defaultReturn) {
		return m_dbrequest.getParamValueInt(paramName, defaultReturn);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param paramName
	 * @param defaultReturn
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParamValueLong(java.lang.String, long)
	 */
	@Override
	public long getParamValueLong(String paramName, long defaultReturn) {
		return m_dbrequest.getParamValueLong(paramName, defaultReturn);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param paramName
	 * @param defaultReturn
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParamValueString(java.lang.String, java.lang.String)
	 */
	@Override
	public String getParamValueString(String paramName, String defaultReturn) {
		return m_dbrequest.getParamValueString(paramName, defaultReturn);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getParentRequest()
	 */
	@Override
	public DBRequest getParentRequest() {
		return m_dbrequest.getParentRequest();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getResource()
	 */
	@Override
	public Resource getResource() {
		return m_dbrequest.getResource();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getResult()
	 */
	@Override
	public Object getResult() {
		return m_dbrequest.getResult();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param <T>
	 * @param clazz
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getResult(java.lang.Class)
	 */
	@Override
	public <T> T getResult(Class<T> clazz) {
		return m_dbrequest.getResult(clazz);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getResultBuilder()
	 */
	@Override
	public ResultBuilder getResultBuilder() {
		return m_dbrequest.getResultBuilder();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getResultBuildSequence()
	 */
	@Override
	public ResultBuildSequence getResultBuildSequence() {
		return m_dbrequest.getResultBuildSequence();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getResultLong()
	 */
	@Override
	public long getResultLong() {
		return m_dbrequest.getResultLong();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getSession()
	 */
	@Override
	public Session getSession() {
		return m_dbrequest.getSession();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getUseTransaction()
	 */
	@Override
	public boolean getUseTransaction() {
		return m_dbrequest.getUseTransaction();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#isRequestChainFirst()
	 */
	@Override
	public boolean isRequestChainFirst() {
		return m_dbrequest.isRequestChainFirst();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#isRequestChainLast()
	 */
	@Override
	public boolean isRequestChainLast() {
		return m_dbrequest.isRequestChainLast();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @return
	 * @see com.tansuosoft.discoverx.dao.DBRequest#isRequestChainSingle()
	 */
	@Override
	public boolean isRequestChainSingle() {
		return m_dbrequest.isRequestChainSingle();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param name
	 * @see com.tansuosoft.discoverx.dao.DBRequest#removeParameter(java.lang.String)
	 */
	@Override
	public void removeParameter(String name) {
		m_dbrequest.removeParameter(name);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#sendRequest()
	 */
	@Override
	public void sendRequest() {
		m_dbrequest.sendRequest();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param nextRequest
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setNextRequest(com.tansuosoft.discoverx.dao.DBRequest)
	 */
	@Override
	public void setNextRequest(DBRequest nextRequest) {
		m_dbrequest.setNextRequest(nextRequest);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param name
	 * @param value
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setParameter(java.lang.String, java.lang.Object)
	 */
	@Override
	public void setParameter(String name, Object value) {
		m_dbrequest.setParameter(name, value);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param parametersSetter
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setParametersSetter(com.tansuosoft.discoverx.dao.ParametersSetter)
	 */
	@Override
	public void setParametersSetter(ParametersSetter parametersSetter) {
		m_dbrequest.setParametersSetter(parametersSetter);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param resource
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setResource(com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	public void setResource(Resource resource) {
		m_dbrequest.setResource(resource);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param resultBuilder
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setResultBuilder(com.tansuosoft.discoverx.dao.ResultBuilder)
	 */
	@Override
	public void setResultBuilder(ResultBuilder resultBuilder) {
		m_dbrequest.setResultBuilder(resultBuilder);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param resultBuildSequence
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setResultBuildSequence(com.tansuosoft.discoverx.dao.ResultBuildSequence)
	 */
	@Override
	public void setResultBuildSequence(ResultBuildSequence resultBuildSequence) {
		m_dbrequest.setResultBuildSequence(resultBuildSequence);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param session
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setSession(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public void setSession(Session session) {
		m_dbrequest.setSession(session);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @param useTransaction
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setUseTransaction(boolean)
	 */
	@Override
	public void setUseTransaction(boolean useTransaction) {
		m_dbrequest.setUseTransaction(useTransaction);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		return m_dbrequest.buildSQL();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#getConnection()
	 */
	@Override
	protected Connection getConnection() {
		return m_dbrequest.getConnection();
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#parseSQL(java.lang.String)
	 */
	@Override
	protected String parseSQL(String sql) {
		return m_dbrequest.parseSQL(sql);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setConnection(java.sql.Connection)
	 */
	@Override
	protected void setConnection(Connection connection) {
		m_dbrequest.setConnection(connection);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setDatabase(com.tansuosoft.discoverx.db.Database)
	 */
	@Override
	protected void setDatabase(Database database) {
		m_dbrequest.setDatabase(database);
	}

	/**
	 * 原始{@link DBRequest}对象的同名方法委托。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#setParentRequest(com.tansuosoft.discoverx.dao.DBRequest)
	 */
	@Override
	protected void setParentRequest(DBRequest parentRequest) {
		m_dbrequest.setParentRequest(parentRequest);
	}

}

