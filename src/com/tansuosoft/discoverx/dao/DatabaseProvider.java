/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import javax.naming.NamingException;

import com.tansuosoft.discoverx.db.Database;
import com.tansuosoft.discoverx.db.DatabaseFactory;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 提供线程一致的数据库对象的类。
 * 
 * @author coca@tensosoft.com
 */
class DatabaseProvider {
	private static class ThreadLocalConnection extends ThreadLocal<Database> {
		/**
		 * 获取默认数据库。
		 * 
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected Database initialValue() {
			Database db = DatabaseFactory.getDatabase();
			return db;
		}

	};

	protected static ThreadLocalConnection m_instance = new ThreadLocalConnection();

	/**
	 * 获取系统使用的默认主数据库对象。
	 * 
	 * @return
	 */
	static Database getDatabase() {
		return m_instance.get();
	}

	/**
	 * 获取dbName数据库数据源名称指定的数据库对象。
	 * 
	 * @param dbName
	 * @return
	 */
	static Database getDatabase(String dbName) {
		reset();
		Database db = null;
		try {
			db = DatabaseFactory.getDatabase(dbName);
		} catch (NamingException e) {
			FileLogger.error(e);
		}
		m_instance.set(db);
		return m_instance.get();
	}

	/**
	 * 移除线程保留的数据库信息。
	 */
	static void reset() {
		Database old = m_instance.get();
		if (old != null) old.close();
		m_instance.remove();
	}
}

