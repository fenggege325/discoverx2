/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import com.tansuosoft.discoverx.db.CommandWrapper;

/**
 * 为{@link CommandWrapper}自动设置参数结果的类须实现的统一接口。
 * 
 * @author coca@tansuosoft.cn
 */
public interface ParametersSetter {
	/**
	 * 为指定数据库请求（request）设置带参数SQL语句（通过cw获取）的参数值。
	 * 
	 * @param request {@link DBRequest}
	 * @param cw {@link CommandWrapper}
	 */
	public void setParameters(DBRequest request, CommandWrapper cw);
}

