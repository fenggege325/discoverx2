/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import java.sql.SQLException;

import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Organization;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 初始化数据库。
 * 
 * @author coca@tensosoft.com
 */
public class InitDB extends DBRequest {

	/**
	 * 重载：实现功能。
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		final String orgName = CommonConfig.getInstance().getBelong();
		final String rootUnid = Organization.ROOT_ORGANIZATION_UNID;
		SQLWrapper result = new SQLWrapper();
		result.setSql("update t_user set c_name=? where c_unid='" + rootUnid + "'");
		this.setParametersSetter(new ParametersSetter() {
			@Override
			public void setParameters(DBRequest request, CommandWrapper cw) {
				try {
					cw.setString(1, orgName);
				} catch (SQLException e) {
					FileLogger.error(e);
				}
			}
		});
		result.setRequestType(RequestType.NonQuery);
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql("update t_user set c_name=? where c_unid='" + User.SUPERADMIN_USER_UNID + "' and c_punid='" + rootUnid + "'");
				result.setRequestType(RequestType.NonQuery);
				return result;
			}
		};
		dbr.setParametersSetter(new ParametersSetter() {
			@Override
			public void setParameters(DBRequest request, CommandWrapper cw) {
				try {
					cw.setString(1, orgName + User.SEPARATOR + ResourceHelper.BUILTIN_ADMIN_USER_NAME);
				} catch (SQLException e) {
					FileLogger.error(e);
				}
			}
		});
		this.setNextRequest(dbr);
		this.setUseTransaction(true);
		return result;
	}
}
