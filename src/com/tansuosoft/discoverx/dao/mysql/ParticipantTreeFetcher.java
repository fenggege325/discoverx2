/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.mysql;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.impl.ParticipantsOfGroupFetcher;
import com.tansuosoft.discoverx.dao.impl.ParticipantsOfRoleFetcher;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.AccountType;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 获取并构造系统所有参与者信息树({@link ParticipantTree})的数据库请求类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ParticipantTreeFetcher extends DBRequest implements ResultBuilder {

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		String punid = this.getParamValueString(PUNID_PARAM_NAME, null);
		ParticipantsOfGroupFetcher gf = new ParticipantsOfGroupFetcher();
		ParticipantsOfRoleFetcher rf = new ParticipantsOfRoleFetcher();
		if (punid == null || punid.isEmpty()) {
			result.setSql("select c_unid,c_name,c_alias,c_sort,c_securitycode,c_accountType from t_user order by c_name,c_sort");
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append("select c_unid,c_name,c_alias,c_sort,c_securitycode,c_accountType from ( ");
			sb.append("select c_unid,c_name,c_alias,c_sort,c_securitycode,c_accountType from t_user where c_unid='" + punid + "'");
			sb.append(" union ");
			sb.append("select c_unid,c_name,c_alias,c_sort,c_securitycode,c_accountType from t_user where c_securitycode!=0 and c_punid='" + punid + "'");
			sb.append(") t order by c_name,c_sort");
			result.setSql(sb.toString());
			gf.setParameter(PUNID_PARAM_NAME, punid);
			rf.setParameter(PUNID_PARAM_NAME, punid);
		}
		this.setNextRequest(gf);
		this.setNextRequest(rf);
		return result;
	}

	/**
	 * 重载build:根据查询结果构造参与者信息树({@link ParticipantTree})并返回之。
	 * 
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object build(DBRequest request, Object rawResult) {
		if (rawResult == null || !(rawResult instanceof DataReader)) return null;
		DataReader dr = (DataReader) rawResult;
		ParticipantTree root = null;
		int count = 0;
		ParticipantTree x = null;
		ParticipantTree parent = null;
		int accountType = 0;
		Map<String, ParticipantTree> map4QueryParent = new TreeMap<String, ParticipantTree>();
		try {
			while (dr.next()) {
				// 顺序：c_unid,c_name,c_alias,c_sort,c_securitycode,c_accountType,c_punid
				x = new ParticipantTree();
				x.setUNID(dr.getString(1));
				x.setName(dr.getString(2));
				x.setAlias(dr.getString(3));
				x.setSort(dr.getInt(4));
				x.setSecurityCode(dr.getInt(5));
				accountType = dr.getInt(6);
				if ((accountType & AccountType.User.getIntValue()) == AccountType.User.getIntValue()) {
					x.setParticipantType(ParticipantType.Person);
				} else if ((accountType & AccountType.Unit.getIntValue()) == AccountType.Unit.getIntValue()) {
					x.setParticipantType(ParticipantType.Organization);
					if (x.getLevel() == 1) x.setParticipantType(ParticipantType.Root);
					map4QueryParent.put(x.getName(), x);
				}
				if (root == null && x.getLevel() == 1) {
					root = x;
				} else {
					parent = map4QueryParent.get(StringUtil.stringLeftBack(x.getName(), User.SEPARATOR));
					if (parent != null) parent.addChild(x);
				}
				count++;
			}// while end
		} catch (SQLException ex) {
			FileLogger.error(ex);
		}

		// 角色和群组参与者
		List<DBRequest> reqs = request.getChainChildren();
		Object result = null;
		List<ParticipantTree> list = null;
		for (DBRequest r : reqs) {
			if (r == null) continue;
			result = r.getResult();
			if (result != null && result instanceof List) {
				list = (List<ParticipantTree>) result;
				for (ParticipantTree y : list) {
					if (y == null) continue;
					count++;
					root.addChild(y);
				}
			}
		}
		root.setSort(count);
		return root;
	}
}

