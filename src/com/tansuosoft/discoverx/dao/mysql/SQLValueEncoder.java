/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.mysql;

import com.tansuosoft.discoverx.dao.DAOConfig;
import com.tansuosoft.discoverx.dao.DAOConfigEntry;

/**
 * 针对mysql实现的{@link com.tansuosoft.discoverx.dao.SQLValueEncoder}实现类。
 * 
 * <p>
 * 注意，mysql调用时必须提供比较方式。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SQLValueEncoder implements com.tansuosoft.discoverx.dao.SQLValueEncoder {
	private static boolean targetDbIsMySql = true; // 目标数据库是否为mysql
	static {
		// 检测目标数据库是否为mysql
		DAOConfig daoc = DAOConfig.getInstance();
		DAOConfigEntry daoConfigEntry = daoc.getDAOConfigEntry("TableColumnsFetcher"); // 用于辅助判断目标数据库，因为DAOConfig.xml中有配置这个名称的数据库实现类。
		String tcfImpl = (daoConfigEntry == null ? null : daoConfigEntry.getImplement());
		targetDbIsMySql = (tcfImpl != null && tcfImpl.startsWith("com.tansuosoft.discoverx.dao.mysql."));
	}

	/**
	 * 缺省构造器。
	 */
	public SQLValueEncoder() {

	}

	/**
	 * 重载encode
	 * 
	 * @see com.tansuosoft.discoverx.dao.SQLValueEncoder#encode(java.lang.String, java.lang.String)
	 */
	@Override
	public String encode(String raw, String compare) {
		if (raw == null || raw.trim().length() == 0) return raw;
		if (!targetDbIsMySql) return raw;
		// 有LIKE比较时,对于MySql一个反斜杆必须有四个反斜杆表示，否则一个反斜杆用两个反斜杆表示。
		String ret = raw.replace("\\", "\\\\");
		if (compare != null && compare.toString().toLowerCase().indexOf("like") >= 0) ret = ret.replace("\\", "\\\\");
		return ret;
	}

}

