/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.mysql;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.impl.OrganizationDeserializerCommon;
import com.tansuosoft.discoverx.model.Organization;

/**
 * 获取并构造指定单位及其部门树({@link Organization})的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest#UNID_PARAM_NAME}参数名获取根节点UNID，必须。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * @deprecated 所有组织机构的反序列化可以使用的通用的{@link OrganizationDeserializerCommon}类。
 */
@Deprecated
public class OrganizationDeserializer extends OrganizationDeserializerCommon {
}
