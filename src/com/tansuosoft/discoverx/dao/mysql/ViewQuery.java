/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.mysql;

import java.util.List;

import com.tansuosoft.discoverx.bll.view.DBViewQuery;
import com.tansuosoft.discoverx.bll.view.Pagination;
import com.tansuosoft.discoverx.bll.view.ParsedColumn;
import com.tansuosoft.discoverx.bll.view.ViewParser;

/**
 * 读取保存于数据库表中的视图条目数据的视图查询实现类。
 * 
 * @author coca@tansuosoft.cn
 * @implementBy coca@tansuosoft.cn
 */
public class ViewQuery extends DBViewQuery {
	/**
	 * 缺省构造器。
	 */
	public ViewQuery() {
		super();
	}

	/**
	 * 重载getQueryForViewEntries
	 * 
	 * @see com.tansuosoft.discoverx.bll.view.ViewQuery#getQueryForViewEntries()
	 */
	@Override
	public String getQueryForViewEntries() {
		super.getQueryForViewEntries();
		if (m_queryForViewEntries != null && m_queryForViewEntries.length() > 0) return m_queryForViewEntries;

		ViewParser parser = this.getViewParser();

		String rawQuery = (this.checkRawQuery() ? getRawQueryWithExtraCondition() : null);

		Pagination pagination = this.getPagination();
		int offset = (pagination.getCurrentPage() - 1) * pagination.getDisplayCountPerPage();
		if (rawQuery != null && rawQuery.length() > 0) { // 手工输入
			StringBuilder sb = new StringBuilder();
			sb.append(rawQuery);
			sb.append("\r\n\tLIMIT ").append(offset).append(",").append(pagination.getDisplayCountPerPage());
			m_queryForViewEntries = sb.toString();
			checkRawSql();
		} else { // 组合sql
			StringBuilder sb = new StringBuilder();
			List<ParsedColumn> columns = parser.parseColumns();
			if (columns != null && columns.size() > 0) {
				for (ParsedColumn x : columns) {
					if (x == null) continue;
					sb.append(sb.length() > 0 ? "," : "").append(x.getSQLElementResult(this));
				}
			}
			if (sb.length() > 0) sb.insert(0, "select ");
			appendFrom(sb);
			appendWhere(sb);
			appendOrderBy(sb);
			sb.append("\r\n\tLIMIT ").append(offset).append(",").append(pagination.getDisplayCountPerPage());
			m_queryForViewEntries = sb.toString();
			checkConfigSql();
		}
		outputDebugSql();

		return m_queryForViewEntries;
	}
}

