/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.mysql;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.dao.impl.TableColumnsResultBuilder;
import com.tansuosoft.discoverx.util.JVM;

/**
 * 获取表包含的所有字段信息的数据库请求类。
 * 
 * <p>
 * 通过“tablename”获取表名。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class TableColumnsFetcher extends DBRequest {

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		this.setResultBuilder(new TableColumnsResultBuilder());
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		String tableName = this.getParamValueString("tablename", null);
		if (tableName == null || tableName.length() == 0) throw new RuntimeException("没有提供有效表名。");
		if (!TableExistsChecker.getInstance().tableExists(tableName)) {
			sql.append("select c_unid from t_user where c_unid='FC11D014B0C4454BA3F9C919478B8FEE'"); // 确保没有数据返回。
		} else {
			sql.append("describe ").append(JVM.isHostWindows() ? tableName.toUpperCase() : tableName); // linux下表会区分大小写
		}
		result.setSql(sql.toString());

		return result;
	}
}

