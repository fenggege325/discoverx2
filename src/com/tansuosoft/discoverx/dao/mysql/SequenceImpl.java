/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.mysql;

import com.tansuosoft.discoverx.dao.DBHelper;
import com.tansuosoft.discoverx.dao.Sequence;

/**
 * 基于MySql数据库实现的自增长序号提供类。
 * 
 * @author coca@tansuosoft.cn
 */
public class SequenceImpl implements Sequence {
	/**
	 * 缺省构造器。
	 */
	public SequenceImpl() {
	}

	private String m_key = null;
	private long m_initialization = 1;

	/**
	 * 重载getInitialization
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getInitialization()
	 */
	@Override
	public long getInitialization() {
		return this.m_initialization;
	}

	/**
	 * 重载getKey
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getKey()
	 */
	@Override
	public String getKey() {
		return this.m_key;
	}

	/**
	 * 重载getPreviewSequence
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getPreviewSequence()
	 */
	@Override
	public long getPreviewSequence() {
		if (this.m_key == null || this.m_key.length() == 0) return -1;

		String sql = String.format("select curval('%1$s')", this.m_key);
		Object currval = DBHelper.executeScalar(sql);
		if (currval == null) return 0;
		return Long.parseLong(currval.toString());
	}

	/**
	 * 重载getSequence
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getSequence()
	 */
	@Override
	public long getSequence() {
		if (this.m_key == null || this.m_key.length() == 0) return -1;

		String sql = String.format("select nextval('%1$s')", this.m_key);
		Object nextval = DBHelper.executeScalar(sql);
		if (nextval == null) return 0;
		return Long.parseLong(nextval.toString());
	}

	/**
	 * 重载getSequence
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getSequence(long)
	 */
	@Override
	public long getSequence(long step) {
		if (this.m_key == null || this.m_key.length() == 0) return -1;

		String sql = String.format("select nextval('%1$s')", this.m_key);
		Object nextval = DBHelper.executeScalar(sql);
		if (nextval == null) return 0;
		return Long.parseLong(nextval.toString()) + step;
	}

	/**
	 * 重载setInitialization
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#setInitialization(long)
	 */
	@Override
	public void setInitialization(long initialization) {
		this.m_initialization = initialization;
	}

	/**
	 * 重载setKey
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#setKey(java.lang.String)
	 */
	@Override
	public void setKey(String key) {
		this.m_key = key;
		this.autoCreateSequence();
	}

	/**
	 * 判断key对应的记录是否存在，不存在则自动创建。
	 */
	private synchronized void autoCreateSequence() {
		boolean isExist = false;
		String sql = String.format("select count(*) from t_sequence where c_name='%s'", this.m_key);
		Object retval = DBHelper.executeScalar(sql);
		if (retval != null) {
			int cnt = Integer.parseInt(retval.toString());
			if (cnt > 0) isExist = true;
		}
		if (!isExist) {
			StringBuilder sb = new StringBuilder("insert into t_sequence (c_name,c_value,c_init,c_step,c_max) values(");
			sb.append("'").append(this.m_key).append("'");
			sb.append(",").append(this.m_initialization);
			sb.append(",").append(this.m_initialization);
			sb.append(",").append(1);
			sb.append(",").append(0);
			sb.append(")");
			sql = sb.toString();
			if (DBHelper.executeNonQuery(sql) <= 0) { throw new RuntimeException("无法初始化“" + this.m_key + "”对应的自增长序号！"); }
		}
	}

	/**
	 * 重载setSequence
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#setSequence(long)
	 */
	@Override
	public void setSequence(long sequence) {
		if (this.m_key == null || this.m_key.length() == 0) return;

		String sql = String.format("select setval('%1$s',%2$s)", this.m_key, sequence + "");
		Object newval = DBHelper.executeScalar(sql);
		if (newval == null) {
			throw new RuntimeException("无法设置“" + this.m_key + "”对应的自增长序号值！");
		} else {
			long ret = Long.parseLong(newval.toString());
			if (ret != sequence) throw new RuntimeException("无法设置“" + this.m_key + "”对应的自增长序号值！");
		}
	}

}

