/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.mysql;

import java.sql.SQLException;
import java.util.HashMap;

import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.db.Database;
import com.tansuosoft.discoverx.db.DatabaseFactory;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 检查某个表是否存在的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class TableExistsChecker {
	private HashMap<String, Integer> m_map = null;
	private static TableExistsChecker m_instance = null;

	/**
	 * 缺省构造器。
	 */
	private TableExistsChecker() {
		m_map = new HashMap<String, Integer>();
		String sql = "show tables";
		Database db = null;
		DataReader dr = null;
		try {
			db = DatabaseFactory.getDatabase();
			dr = db.executeQuery(sql);
			String t = null;
			while (dr.next()) {
				t = dr.getString(1, null);
				if (t == null || t.length() == 0) continue;
				m_map.put(t.toLowerCase(), 1);
			}
		} catch (SQLException e) {
			FileLogger.error(e);
		} finally {
			if (db != null) db.close();
		}
	}

	/**
	 * 获取此对象唯一实例。
	 * 
	 * @return
	 */
	public synchronized static TableExistsChecker getInstance() {
		if (m_instance == null) {
			m_instance = new TableExistsChecker();
		}
		return m_instance;
	}

	/**
	 * 检查指定表是否存在。
	 * 
	 * @param tableName
	 * @return
	 */
	public boolean tableExists(String tableName) {
		if (tableName == null || tableName.trim().length() == 0) return false;
		Integer x = m_map.get(tableName.toLowerCase());
		if (x != null && x.intValue() == 1) return true;
		return false;
	}

	/**
	 * 追加一个表名。
	 * 
	 * @param tableName
	 */
	public void addTableName(String tableName) {
		if (tableName == null || tableName.trim().length() == 0) return;
		this.m_map.put(tableName, 1);
	}

	/**
	 * 重载表信息缓存。
	 */
	public synchronized void reload() {
		m_instance = null;
	}
}

