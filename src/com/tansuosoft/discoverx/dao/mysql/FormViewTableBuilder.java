/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.mysql;

import java.util.List;

import com.tansuosoft.discoverx.bll.view.FormViewTable;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.model.Form;
import com.tansuosoft.discoverx.model.Item;
import com.tansuosoft.discoverx.model.Resource;

/**
 * 生成视图查询表的数据库请求类。
 * 
 * <p>
 * 通过{@link DBRequest.OBJECT_PARAM_NAME}获取表单对象资源。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class FormViewTableBuilder extends DBRequest implements ResultBuilder {

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		Resource r = this.getParameterResource(OBJECT_PARAM_NAME);
		if (r == null || !(r instanceof Form)) throw new RuntimeException("没有提供表单资源！");
		Form f = (Form) r;
		List<Item> items = f.getItems();
		if (items == null || items.isEmpty()) throw new RuntimeException("表单不包含字段！");

		String tableName = String.format("%1$s%2$s", FormViewTable.FORM_VIEW_TABLE_PREFIX, f.getAlias());
		this.setParameter("__tableName", tableName);

		// 删表语句
		sql.append("drop table if exists ").append(tableName);

		// 建表语句
		StringBuilder createSql = new StringBuilder();
		createSql.append("create table ").append(tableName).append("(c_punid char(32) primary key,c_oid INT default 0");
		for (Item item : items) {
			if (item == null || !item.isFormViewTableColumn()) continue;
			createSql.append(",").append(item.getItemName()).append(" TEXT");
		}
		createSql.append(")");

		DBRequest create = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql(this.getParamValueString("sql", null));
				result.setRequestType(RequestType.NonQuery);
				return result;
			}
		};
		create.setParameter("sql", createSql.toString());
		this.setNextRequest(create);

		// Insert语句
		StringBuilder insertSql = new StringBuilder();
		insertSql.append("insert into ").append(tableName).append(" (c_punid) ").append("select c_unid from t_document where c_formalias='").append(f.getAlias()).append("'");
		DBRequest insert = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper result = new SQLWrapper();
				result.setSql(this.getParamValueString("sql", null));
				result.setRequestType(RequestType.NonQuery);
				return result;
			}
		};
		insert.setParameter("sql", insertSql.toString());
		this.setNextRequest(insert);

		// Update语句
		StringBuilder updateSql = null;
		for (Item item : items) {
			if (item == null || !item.isFormViewTableColumn()) continue;
			String itemName = item.getItemName();
			updateSql = new StringBuilder();
			updateSql.append("update ").append(tableName).append(",t_item").append(" set ");
			updateSql.append(tableName).append(".").append(itemName).append("=t_item.c_value");
			updateSql.append(" where ").append(tableName).append(".c_punid=t_item.c_punid and t_item.c_name='").append(itemName).append("'");
			DBRequest update = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper result = new SQLWrapper();
					result.setSql(this.getParamValueString("sql", null));
					result.setRequestType(RequestType.NonQuery);
					return result;
				}
			};
			update.setParameter("sql", updateSql.toString());
			this.setNextRequest(update);
		}

		this.setUseTransaction(true);
		result.setRequestType(RequestType.NonQuery);
		result.setSql(sql.toString());

		return result;
	}

	/**
	 * 重载build
	 * 
	 * @see com.tansuosoft.discoverx.dao.ResultBuilder#build(com.tansuosoft.discoverx.dao.DBRequest, java.lang.Object)
	 */
	@Override
	public Object build(DBRequest request, Object rawResult) {
		TableExistsChecker.getInstance().addTableName(this.getParamValueString("__tableName", null));
		return this.getResult();
	}
}

