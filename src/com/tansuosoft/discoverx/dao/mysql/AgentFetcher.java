/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao.mysql;

import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.MapResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;

/**
 * 获取所有有效用户代理信息的数据库请求实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AgentFetcher extends DBRequest {

	/**
	 * 重载buildSQL
	 * 
	 * @see com.tansuosoft.discoverx.dao.DBRequest#buildSQL()
	 */
	@Override
	protected SQLWrapper buildSQL() {
		SQLWrapper result = new SQLWrapper();
		StringBuilder sql = new StringBuilder();

		sql.append("select a.fld_parentsc,a.fld_agentsc from vt_frmuserprofile a where sysdate()>=STR_TO_DATE(a.fld_leave,'%Y-%m-%d %T') and sysdate()<=STR_TO_DATE(a.fld_return,'%Y-%m-%d %T')");
		result.setSql(sql.toString());
		this.setResultBuilder(new MapResultBuilder());

		return result;
	}

}

