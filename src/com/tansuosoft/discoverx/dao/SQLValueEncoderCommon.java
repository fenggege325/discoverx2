/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

/**
 * 用于编码通用文本结果的SQL值编码器类。
 * 
 * @author coca@tansuosoft.cn
 */
public class SQLValueEncoderCommon implements SQLValueEncoder {

	/**
	 * 重载encode：将原始值中包含的半角单引号替换为两个半角单引号。
	 * 
	 * @see com.tansuosoft.discoverx.dao.SQLValueEncoder#encode(java.lang.String, java.lang.String)
	 */
	@Override
	public String encode(String raw, String compare) {
		if (raw == null) return null;
		return raw.replace("'", "''");
	}

}

