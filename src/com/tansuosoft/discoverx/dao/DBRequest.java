/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tansuosoft.discoverx.common.exception.DBException;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.db.Database;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.HttpContext;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 用于发送数据库访问请求并获取结果的通用抽象类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class DBRequest implements SQLValueEncoder {
	/**
	 * 常用参数名：内置的名为“unid”的参数名。
	 */
	public static final String UNID_PARAM_NAME = "unid";

	/**
	 * 常用参数名：内置的名为“punid”的参数名。
	 */
	public static final String PUNID_PARAM_NAME = "punid";

	/**
	 * 常用参数名：内置的名为“entity”（表对应的主实体对象）的参数名。
	 */
	public static final String ENTITY_PARAM_NAME = "entity";

	/**
	 * 常用参数名：内置的名为“object”（普通对象）的参数名。
	 */
	public static final String OBJECT_PARAM_NAME = "object";

	/**
	 * 常用参数名：内置的名为“parent”（上级对象）的参数名。
	 */
	public static final String PARENT_PARAM_NAME = "parent";

	/**
	 * 缺省构造器。
	 */
	public DBRequest() {
	}

	private ResultBuildSequence m_resultBuildSequence = ResultBuildSequence.Stack; // 请求链中有多个请求时请求结果构造方式。
	private Session m_session = null; // 用户自定义会话，用于获取请求用户信息等。
	private Database m_database = null; // 当前使用的数据库访问对象，必须。
	private Connection m_connection = null; // 当前使用的数据库连接。
	private boolean m_useTransaction = false; // 是否使用事务模式（默认为false）。
	private ResultBuilder m_resultBuilder = null; // 构造最终返回结果的对象。
	private ParametersSetter m_parametersSetter = null; // 设置带参数SQL语句参数值的对象。
	private DBRequest m_parentRequest = null; // 请求链中的上一个请求。
	private DBRequest m_nextRequest = null; // 请求链中的下一个请求。
	private Object m_result = null; // 结果
	private Map<String, Object> m_params = new HashMap<String, Object>(); // 额外参数

	/**
	 * 返回包含多个请求的请求链中各请求的结果构造顺序。
	 * 
	 * <p>
	 * 此属性影响{@link DBRequest#getResultBuilder()}执行{@link ResultBuilder#build(DBRequest, Object)}的顺序。默认为：{@link ResultBuildSequence#Stack}（堆栈方式）。<br/>
	 * 如果只有单个请求则忽略此属性。只需设置请求链中根请求的结果即可，其它下级请求将自动继承根请求的结果。
	 * </p>
	 * <p>
	 * 如果是队列方式,那么在本级获取结果之后再设置下级的结果（越前面的越先构造结果）。
	 * </p>
	 * <p>
	 * 如果是堆栈方式,那么在下级获取结果之后再设置本身的结果（越后面的越先构造结果）。
	 * </p>
	 * 
	 * @return ResultBuildSequence
	 */
	public ResultBuildSequence getResultBuildSequence() {
		return this.m_resultBuildSequence;
	}

	/**
	 * 设置包含多个请求的请求链中各请求的结果构造顺序。
	 * 
	 * @see ResultBuildSequence
	 * @param resultBuildSequence ResultBuildSequence
	 */
	public void setResultBuildSequence(ResultBuildSequence resultBuildSequence) {
		this.m_resultBuildSequence = resultBuildSequence;
	}

	/**
	 * 返回用户自定义会话，用于获取请求用户信息等。
	 * 
	 * @return Session
	 */
	public Session getSession() {
		return this.m_session;
	}

	/**
	 * 设置用户自定义会话，用于获取请求用户信息等。
	 * 
	 * @param session Session
	 */
	public void setSession(Session session) {
		this.m_session = session;
	}

	/**
	 * 指定当前数据库请求将使用dbName对应的数据源名称指定的数据库连接。
	 * 
	 * <p>
	 * 如果需要使用非系统默认数据库连接，请在根{@link DBRequest}对象发送请求之前调用此方法指定数据库。
	 * </p>
	 * 
	 * @param dbName
	 */
	public void useDatabase(String dbName) {
		this.m_database = DatabaseProvider.getDatabase(dbName);
		if (this.m_database != null) this.m_database.setAutoClose(false);
	}

	/**
	 * 返回当前使用的数据库访问对象。
	 * 
	 * <p>
	 * 返回的{@link Database}对象的{@link Database#getAutoClose()}总是被设置为返回false。
	 * </p>
	 * 
	 * @return Database
	 */
	public Database getDatabase() {
		if (this.m_database == null) {
			this.m_database = DatabaseProvider.getDatabase();
			if (this.m_database != null) this.m_database.setAutoClose(false);
		}
		return this.m_database;
	}

	/**
	 * 设置当前使用的数据库访问对象。
	 * 
	 * <p>
	 * 可选，如果不设置，那么系统会自动获取。<br/>
	 * 请求链中的后续请求会根据根请求的结果自动设置，不用手工额外设置此结果。
	 * </p>
	 * <p>
	 * 设置的{@link Database}对象的{@link Database#getAutoClose()}总是被设置为返回false。
	 * </p>
	 * 
	 * @param database Database
	 */
	protected void setDatabase(Database database) {
		this.m_database = database;
		if (this.m_database != null) this.m_database.setAutoClose(false);
	}

	/**
	 * 返回可用的数据库连接。
	 * 
	 * @return Connection
	 */
	protected Connection getConnection() {
		try {
			if (this.m_connection == null || this.m_connection.isClosed()) {
				Database db = this.getDatabase();
				if (db != null) {
					this.m_connection = db.getConnection();
				} else {
					throw new SQLException("无法获取数据库信息，请联系系统管理员！");
				}
			}
		} catch (SQLException e) {
			FileLogger.error(e);
		}
		return this.m_connection;
	}

	/**
	 * 设置可用的数据库连接。
	 * 
	 * <p>
	 * 如果当前没有可用的数据库连接且{@link #getDatabase()}有效，则系统会自动通过{@link Database#getConnection()}获取连接。
	 * </p>
	 * 
	 * @param connection Connection
	 */
	protected void setConnection(Connection connection) {
		this.m_connection = connection;
	}

	/**
	 * 返回是否使用事务模式（默认为false）。
	 * 
	 * <p>
	 * 启用事务模式后，{@link Database#getAutoCommit()}将被设置为总是返回false。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getUseTransaction() {
		return this.m_useTransaction;
	}

	/**
	 * 设置是否使用事务模式。
	 * 
	 * @param useTransaction boolean
	 */
	public void setUseTransaction(boolean useTransaction) {
		this.m_useTransaction = useTransaction;
	}

	/**
	 * 返回构造最终返回结果的对象。
	 * 
	 * <p>
	 * 获取的步骤：<br/>
	 * 1.先检查是否有设置进来的ResultBuilder实例，如果有实用之，否则继续下一步获取。<br/>
	 * 2.再建查配置项中是否有配置有效的ResultBuilder实现类，如果有实用之，否则继续下一步获取。<br/>
	 * 3.如果此对象实现了ParameterSetter接口，则使用之。
	 * </p>
	 * <p>
	 * 如果以上步骤都获取不到，说明没有构造最终结果的对象，那么最终结果将返回原始结果。
	 * </p>
	 * 
	 * @see ResultBuilder
	 * @see DAOConfigEntry#setResultBuilder(String)
	 * @see DAOConfig#getResultBuilder(Class)
	 * @see DAOConfig#getResultBuilder(String)
	 * @return ResultBuilder SQL语句对应的构造最终返回结果的对象。
	 */
	public ResultBuilder getResultBuilder() {
		if (this.m_resultBuilder == null) {
			this.m_resultBuilder = DAOConfig.getInstance().getResultBuilder(this.getClass());
		}
		if (this.m_resultBuilder == null) {
			if (this instanceof ResultBuilder) this.m_resultBuilder = (ResultBuilder) this;
		}
		return this.m_resultBuilder;
	}

	/**
	 * 设置构造最终返回结果的对象。
	 * 
	 * @see DBRequest#getResultBuilder()
	 * @param resultBuilder ResultBuilder SQL语句对应的构造最终返回结果的对象。
	 */
	public void setResultBuilder(ResultBuilder resultBuilder) {
		this.m_resultBuilder = resultBuilder;
	}

	/**
	 * 返回为带参数SQL语句设置参数值结果的{@link ParametersSetter}实现对象。
	 * 
	 * <p>
	 * 获取的步骤：<br/>
	 * 1.先检查是否有设置进来的{@link ParametersSetter}实例，如果有实用之，否则继续下一步获取。<br/>
	 * 2.再建查配置项中是否有配置有效的{@link ParametersSetter}实现类，如果有实用之，否则继续下一步获取。<br/>
	 * 3.如果当前请求对象本身实现了{@link ParametersSetter}接口，则使用之。
	 * </p>
	 * <p>
	 * 如果以上步骤都获取不到，则返回null。如果SQL语句是带参数的，那么发送请求时会触发错误。
	 * </p>
	 * 
	 * @see DAOConfigEntry#setParametersSetter(String)
	 * @see DAOConfig#getParametersSetter(Class)
	 * @see DAOConfig#getParametersSetter(String)
	 * @return ParametersSetter
	 */
	public ParametersSetter getParametersSetter() {
		if (this.m_parametersSetter == null) {
			this.m_parametersSetter = DAOConfig.getInstance().getParametersSetter(this.getClass());
		}
		if (this.m_parametersSetter == null) {
			if (this instanceof ParametersSetter) this.m_parametersSetter = (ParametersSetter) this;
		}
		return this.m_parametersSetter;
	}

	/**
	 * 设置为带参数SQL语句设置参数值结果的{@link ParametersSetter}实现对象。
	 * 
	 * @param parametersSetter {@link ParametersSetter}
	 */
	public void setParametersSetter(ParametersSetter parametersSetter) {
		this.m_parametersSetter = parametersSetter;
	}

	/**
	 * 返回请求链中的上一个请求。
	 * 
	 * <p>
	 * 如果返回null则表示当前请求为根请求（即请求链中的第一个请求）。
	 * </p>
	 * 
	 * @return DBRequest 请求链中的上一个请求。
	 */
	public DBRequest getParentRequest() {
		return this.m_parentRequest;
	}

	/**
	 * 设置请求链中的上一个请求。
	 * 
	 * @param parentRequest DBRequest
	 */
	protected void setParentRequest(DBRequest parentRequest) {
		this.m_parentRequest = parentRequest;
	}

	/**
	 * 返回请求链中的下一个请求。
	 * 
	 * <p>
	 * 如果返回null则表示当前请求为请求链中的最后一个请求。
	 * </p>
	 * 
	 * @return DBRequest 请求链中的下一个请求。
	 */
	public DBRequest getNextRequest() {
		return this.m_nextRequest;
	}

	/**
	 * 设置请求链中的下一个请求。
	 * 
	 * <p>
	 * 如果当前对象已经包含有效的下一个请求对象，则作如下处理：<br/>
	 * 如果nextRequest是{@link ConditionalDBRequest}类型的请求对象实例，则把nextRequest插入到与当前请求直接相邻的下一个请求位置，其余原有后续请求位置顺延；<br/>
	 * 如果nextRequest是普通的{@link DBRequest}类型的请求对象实例，则把nextRequest追加到原有请求链的最末端。
	 * </p>
	 * 
	 * @param nextRequest DBRequest 请求链中的下一个请求。
	 */
	public void setNextRequest(DBRequest nextRequest) {
		if (nextRequest == null) return;
		if (this.m_nextRequest != null) {
			if (nextRequest instanceof ConditionalDBRequest) {
				DBRequest old = this.getNextRequest();
				nextRequest.setNextRequest(old);
				this.m_nextRequest = nextRequest;
				this.m_nextRequest.setParentRequest(this);
			} else {
				this.m_nextRequest.setNextRequest(nextRequest);
			}
		} else {
			this.m_nextRequest = nextRequest;
			this.m_nextRequest.setParentRequest(this);
		}
	}

	/**
	 * 返回是否请求链中的第一个请求。
	 * 
	 * @return boolean
	 */
	public boolean isRequestChainFirst() {
		return (this.m_parentRequest == null);
	}

	/**
	 * 返回是否请求链中的最后一个请求。
	 * 
	 * @return boolean
	 */
	public boolean isRequestChainLast() {
		return (this.m_nextRequest == null);
	}

	/**
	 * 返回是否请求链中只有一个请求。
	 * 
	 * @return boolean
	 */
	public boolean isRequestChainSingle() {
		return (this.isRequestChainFirst() && this.isRequestChainLast());
	}

	/**
	 * 获取当前请求的请求链中包含的所有下级请求对象（{@link DBRequest}）的列表。
	 * 
	 * @return List&lt;DBRequest&gt;
	 */
	public List<DBRequest> getChainChildren() {
		List<DBRequest> list = new ArrayList<DBRequest>();
		DBRequest next = this.getNextRequest();
		while (next != null) {
			list.add(next);
			next = next.getNextRequest();
		}
		return list;
	}

	/**
	 * 构造此次数据库访问请求的SQL语句包装对象的实例。
	 * 
	 * <p>
	 * 实现类必须实现此方法以提供必要的SQL请求，如果此方法返回null，那么系统从DBConfig中读取sql模板，并替换掉动态参数值，然后作为sql语句使用。
	 * </p>
	 * 
	 * @return SQLWrapper
	 */
	protected abstract SQLWrapper buildSQL();

	protected String parseSQL(String sql) {
		if (sql == null || sql.length() == 0) return sql;
		String parsed = sql;
		Pattern pattern = Pattern.compile("\\{\\#([^#{}]+)\\}", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(parsed);
		String param = null;
		String paramName = null;
		Object paramValue = null;
		int propDot = -1;

		while (matcher.find()) {
			param = matcher.group(1);
			propDot = param.indexOf('.');
			paramName = (propDot < 0 ? param : param.substring(0, propDot));
			paramValue = this.getParameter(paramName);
			String replacement = "";
			if (propDot < 0) {
				replacement = (paramValue == null ? "" : paramValue.toString());
			} else if (paramValue != null) {
				String propName = null;
				param = param.substring(propDot + 1);
				propDot = 0;
				int nextSeparator = 0;
				Method m = null;
				while (paramValue != null && (nextSeparator = param.indexOf('.', propDot)) != -1) {
					propName = param.substring(propDot, nextSeparator);
					m = ObjectUtil.getMethod(paramValue, String.format("get%1$s", StringUtil.toPascalCase(propName)));
					paramValue = (m == null ? null : ObjectUtil.getMethodResult(paramValue, m));
					propDot = nextSeparator + 1;
				}// while end
				propName = param.substring(propDot);
				if (paramValue != null) {
					m = ObjectUtil.getMethod(paramValue, String.format("get%1$s", StringUtil.toPascalCase(propName)));
					paramValue = ObjectUtil.getMethodResult(paramValue, m);
				}
				replacement = (paramValue == null ? "" : paramValue.toString());
			}// while end
			parsed = parsed.replace(matcher.group(), replacement);
		}

		return parsed;
	}

	/**
	 * 向数据库服务器发送数据库访问请求并构造请求结果。
	 * 
	 * @see DBRequest#buildSQL()
	 */
	public void sendRequest() {
		CommandWrapper cw = null;
		Connection conn = null;
		DataReader dr = null;
		ResultBuilder resultBuilder = null;
		SQLWrapper sqlWrapper = null;
		RequestType requestType = null;
		String sql = null;
		Database db = null;
		try {
			sqlWrapper = this.buildSQL();
			// buildSQL返回null，那么检查对应配置项。
			if (sqlWrapper == null) sqlWrapper = DAOConfig.getInstance().getDAOConfigEntry(this.getClass());
			if (sqlWrapper == null) throw new Exception("无法获取SQL语句包装类信息，请确认buildSQL方法有有效的返回值或DBConfig中配置了对应的SQL模板！");
			requestType = sqlWrapper.getRequestType();
			sql = sqlWrapper.getSql();
			sql = this.parseSQL(sql);
			if (requestType != RequestType.NonRequest && (sql == null || sql.trim().length() == 0)) throw new Exception("无法获取SQL语句信息！");
			db = this.getDatabase();
			// 如果是带参数sql，那么
			ParametersSetter parametersSetter = this.getParametersSetter();
			if (sqlWrapper.getParameterized()) {
				if (parametersSetter == null) throw new Exception("没有配置为带参数SQL语句设置参数的参数设置对象！");
				cw = db.createCommandWrapper(sql);
				parametersSetter.setParameters(this, cw);
			}

			conn = this.getConnection();
			// 如果是请求链中第一个请求且开启了事务模式。
			if (this.isRequestChainFirst() && this.m_useTransaction) {
				conn.setAutoCommit(false);
				if (db != null) {
					db.setAutoClose(false);
					db.setAutoCommit(false);
				}
			}
			switch (requestType.getIntValue()) {
			case 1: // 普通查询语句类型
				if (cw != null) {
					if (cw.execute()) dr = cw.getDataReader();
				} else {
					dr = db.executeQuery(sql);
				}
				this.m_result = dr;
				break;
			case 2: // 普通非查询语句类型，包括Update、Insert、Delete等
				int affectedCount = 0;
				if (cw != null) {
					if (!cw.execute()) affectedCount = cw.getUpdateCount();
				} else {
					affectedCount = db.executeNonQuery(sql);
				}
				this.m_result = affectedCount;
				break;
			case 3: // 只需要数据库返回的结果集中的第一行第一列的标量值对应的的普通查询语句类型
				if (cw != null) {
					if (cw.execute()) {
						dr = cw.getDataReader();
						if (dr != null) {
							while (dr.next()) {
								this.m_result = dr.getObject(1);
								break;
							}
						}
					}
				} else {
					this.m_result = db.executeScalar(sql);
				}
				break;
			case 0:
			default:
				break;
			}// switch end

			resultBuilder = this.getResultBuilder();

			// 如果是队列方式,那么在构造本级结果之后再构造下级的结果（越前面的越先设置结果）。
			if (this.getResultBuildSequence() == ResultBuildSequence.Queue && resultBuilder != null) {
				// 把初始结果构造为最终结果并把最终构造结果重新记录到m_result中
				this.m_result = resultBuilder.build(this, this.m_result);
			}

			// 如果请求链中包含下一个请求，则执行请求链中的下一个请求。
			DBRequest next = this.getNextRequest();
			if (next != null) {
				next.setResultBuildSequence(getResultBuildSequence());
				boolean nextFlag = true;
				if (next instanceof ConditionalDBRequest) nextFlag = ((ConditionalDBRequest) next).check();
				while (!nextFlag) {
					next = next.getNextRequest();
					if (next == null) {
						nextFlag = true;
						break;
					}
					if (next instanceof ConditionalDBRequest) {
						nextFlag = ((ConditionalDBRequest) next).check();
					} else {
						nextFlag = true;
					}
				}
				if (nextFlag && next != null) {
					next.setUseTransaction(this.getUseTransaction());
					next.setResultBuildSequence(this.getResultBuildSequence());
					next.setSession(this.getSession());
					next.setDatabase(db);
					next.sendRequest();
				}
			}

			// 如果是堆栈方式,那么在下级结果构造之后再构造本身的结果（越后面的越先设置结果）。
			if (this.getResultBuildSequence() == ResultBuildSequence.Stack && resultBuilder != null) {
				// 把初始结果构造为最终结果并把最终构造结果重新记录到m_result中
				this.m_result = resultBuilder.build(this, this.m_result);
			}

			// 事务提交
			if (this.isRequestChainFirst() && this.m_useTransaction) {
				conn.commit();
			}
		} catch (Exception ex) {
			if (ex instanceof SQLException && sql != null && sql.length() > 0) {
				FileLogger.debug("SQL执行错误：“%1$s”，SQL语句=“%2$s”", ex.getMessage(), sql);
				FileLogger.error(ex);
			}
			try {
				if (this.isRequestChainFirst() && conn != null && this.m_useTransaction) {
					conn.rollback();
				}
			} catch (SQLException e) {
				if (sql != null && sql.length() > 0) FileLogger.debug("SQL执行错误并触发事务回滚时出现新错误：\r\nsql:%1$s\r\n错误消息:%2$s", sql, e.getMessage());
			}
			throw new DBException(ex);
		} finally {
			if (dr != null) dr.close();
			if (cw != null) cw.close();
			this.close();
		}
	}// func end

	/**
	 * 关闭数据库连接资源。
	 * 
	 * <p>
	 * 仅根请求才执行实际的链接关闭。
	 * </p>
	 */
	public void close() {
		if (this.isRequestChainFirst()) {
			Database db = this.getDatabase();
			if (db != null) db.close();
			DatabaseProvider.reset();
		}
	}

	/**
	 * 获取本次请求的返回结果。
	 * 
	 * <p>
	 * 需执行过{@link DBRequest#sendRequest()}之后才有可能存在返回结果。<br/>
	 * </p>
	 * 
	 * @return Object 返回结果对象。<br/>
	 *         如果{@link SQLWrapper#getRequestType()}的结果为{@link RequestType#Scalar}，则返回第一行第一列的查询结果对象。<br/>
	 *         如果{@link SQLWrapper#getRequestType()}的结果为{@link RequestType#NonQuery}，则通常返回受增、删、改等语句影响的行数数字。<br/>
	 *         如果{@link SQLWrapper#getRequestType()}的结果为{@link RequestType#NonRequest}，则通常返回null。<br/>
	 *         如果提供了有效的{@link ResultBuilder}则返回{@link ResultBuilder#build(DBRequest, Object)}的处理结果。<br/>
	 *         如果没有提供有效的{@link ResultBuilder}则返回数据库请求的查询语句执行的原始结果（通常封装为{@link DataReader}对象返回)。<br/>
	 *         如果没有发送过请求或没有结果或出现异常时则通常返回null。
	 */
	public Object getResult() {
		return this.m_result;
	}

	/**
	 * 获取本次请求的具体类型的返回结果。
	 * 
	 * <p>
	 * 通过{@link DBRequest#getResult()}获取。
	 * </p>
	 * <p>
	 * 需执行过{@link DBRequest#sendRequest()}之后才有可能存在返回结果。<br/>
	 * </p>
	 * 
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T getResult(Class<T> clazz) {
		if (clazz == null) throw new IllegalArgumentException("未指定有效Class信息。");
		Object result = this.getResult();
		if (result == null) return null;
		if (clazz.isInstance(result)) return (T) result;
		if (clazz.getName().equals(String.class.getName())) return (T) result.toString();

		T ret = null;
		Class<?> cls = result.getClass();
		if (clazz.isAssignableFrom(cls)) ret = (T) result;
		return ret;
	}

	/**
	 * 获取数据更新、插入、删除、返回单条数字结果等sql语句执行的最终影响的行对应的数字。
	 * 
	 * <p>
	 * 通过{@link DBRequest#getResult()}获取。
	 * </p>
	 * <p>
	 * 需执行过{@link DBRequest#sendRequest()}之后才有可能存在返回结果。<br/>
	 * </p>
	 * 
	 * @return long，如果执行结果不是返回有效数字，则返回-1。
	 */
	public long getResultLong() {
		Object obj = this.getResult();
		if (obj != null) { return StringUtil.getValueLong(obj.toString(), -1); }
		return 0;
	}

	/**
	 * 获取name指定参数名称对应的参数值，如果找不到，则返回null。
	 * 
	 * @param name
	 * @return Object
	 */
	public Object getParameter(String name) {
		if (name == null || name.trim().length() == 0) return null;
		if (this.m_params == null) return null;
		return this.m_params.get(name);
	}

	/**
	 * 获取所有参数名称字符串的集合对应的迭代器对象，如果没有任何参数，则返回null。
	 * 
	 * @return Iterator<String>
	 */
	public Iterator<String> getParameterNames() {
		if (this.m_params == null || this.m_params.size() == 0) return null;
		Iterator<String> iterator = this.m_params.keySet().iterator();
		return iterator;
	}

	/**
	 * 获取所有参数的参数名-参数值一一对应哈希集合。
	 * 
	 * @return Hashtable<String, Object>
	 */
	public Map<String, Object> getParameters() {
		return this.m_params;
	}

	/**
	 * 把此对象包含的额外参数拷贝到另一个请求。
	 * 
	 * @param another
	 */
	public void copyParametersTo(DBRequest another) {
		if (another == null) return;
		if (this.m_params == null || this.m_params.isEmpty()) return;
		for (String k : this.m_params.keySet()) {
			another.setParameter(k, this.m_params.get(k));
		}
	}

	/**
	 * 设置name指定的参数名称对应的参数值为value。
	 * <p>
	 * 如果同名的参数值已经存在则替换，如果value为null，则等同于调用removeParameter(name)
	 * </p>
	 * 
	 * @param name String
	 * @param value Object
	 */
	public void setParameter(String name, Object value) {
		if (name == null || name.trim().length() == 0) return;
		if (this.m_params == null) this.m_params = new Hashtable<String, Object>();
		if (value == null) {
			this.removeParameter(name);
		} else {
			this.m_params.put(name, value);
		}
	}

	/**
	 * 删除name指定名称的参数值。
	 * 
	 * @param name String
	 */
	public void removeParameter(String name) {
		if (this.m_params != null && name != null && name.trim().length() > 0) {
			this.m_params.remove(name);
		}
	}

	/**
	 * 返回参数名对应参数值的String类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果参数值为null时返回的默认结果。
	 * @return String
	 */
	public String getParamValueString(String paramName, String defaultReturn) {
		Object obj = this.getParameter(paramName);
		if (obj == null) return defaultReturn;
		return obj.toString();
	}

	/**
	 * 返回参数名对应的参数值的int类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return int
	 */
	public int getParamValueInt(String paramName, int defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueInt(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的long类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return long
	 */
	public long getParamValueLong(String paramName, long defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueLong(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的float类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return float
	 */
	public float getParamValueFloat(String paramName, float defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueFloat(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的double类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return double
	 */
	public double getParamValueDouble(String paramName, double defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueDouble(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的boolean类型结果。
	 * 
	 * <p>
	 * 参数值（不区分大小写）配置为“true”、“yes”、“y”、“1”等情况时返回true，否则返回false。
	 * </p>
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return boolean
	 */
	public boolean getParamValueBool(String paramName, boolean defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueBool(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值转换为enumCls定义的枚举中某一个具体枚举值类型之后的结果。
	 * 
	 * @param &lt;T&gt; 返回的枚举类型。
	 * @param paramName String 参数名，必须。
	 * @param enumCls Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return T 具体枚举结果。
	 */
	public <T> T getParamValueEnum(String paramName, Class<T> enumCls, T defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueEnum(this.getParamValueString(paramName, null), enumCls, defaultReturn);
	}

	/**
	 * 获取指定名称(name)对应参数值的java对象实例表示形式。
	 * 
	 * @param name
	 * @return Object 返回指定名称(name)对应参数值的java对象实例，如果不存在则返回null。
	 */
	public Object getParameterObject(String name) {
		Object obj = this.getParameter(name);
		return obj;
	}

	/**
	 * 获取指定名称(name)对应参数值的资源对象实例表示形式。
	 * 
	 * @param name
	 * @return Resource 返回指定名称(name)对应参数值的资源对象实例，如果不存在则返回null。
	 */
	public Resource getParameterResource(String name) {
		Object obj = this.getParameter(name);
		if (obj == null || !(obj instanceof Resource)) return null;
		return (Resource) obj;
	}

	/**
	 * 从实体参数名(DBRequest.ENTITY_PARAM_NAME)对应的参数值中返回资源对象实例。
	 * 
	 * @return Resource 资源对象实例或null（如果找不到对应参数值或对应参数值类型不是资源类型）。
	 */
	public Resource getResource() {
		Resource r = this.getParameterResource(ENTITY_PARAM_NAME);
		return r;
	}

	/**
	 * 设置资源对象实例到实体参数名(DBRequest.ENTITY_PARAM_NAME)对应的参数值中。
	 * 
	 * @see DBRequest#ENTITY_PARAM_NAME
	 * @param resource Resource 使用的资源对象实例。
	 */
	public void setResource(Resource resource) {
		if (resource != null) this.setParameter(ENTITY_PARAM_NAME, resource);
	}

	/**
	 * 调用系统注册的所有编码器将输入值编码为最终结果并返回之。
	 * 
	 * <p>
	 * 参考{@link SQLValueEncoder}
	 * </p>
	 * 
	 * @param raw String，原始值。
	 * @param compare String
	 * @return String，如果系统没有注册编码器，则返回原始值。
	 */
	@Override
	public String encode(String raw, String compare) {
		if (raw == null) return null;
		if (raw.length() == 0) return raw;
		return SQLValueEncoderConfig.getInstance().encode(raw, compare);
	}

	/**
	 * 通过全限定类名获取对应的DBRequest对象实例。
	 * 
	 * @param className
	 * @return
	 */
	public static DBRequest getInstance(String className) {
		if (className == null || className.length() == 0) return null;
		DBRequest result = Instance.newInstance(className, DBRequest.class);
		return result;
	}

	/**
	 * 清理所有线程对应的线程本地变量保存的信息。
	 * 
	 * <p>
	 * 由系统内部调用。
	 * </p>
	 */
	public static void shutdown() {
		Thread[] ts = HttpContext.getThreads();
		if (ts != null) {
			for (Thread t : ts) {
				HttpContext.clearReferencesThreadLocals(DatabaseProvider.m_instance, t);
			}
		}
		DatabaseProvider.m_instance = null;
	}

	/**
	 * 重载：返回此请求对应的sql语句等信息。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SQLWrapper sql = this.buildSQL();
		if (sql == null) return super.toString();
		return String.format("%1$s=%2$s", StringUtil.getValueString(this.getClass().getSimpleName(), "DBRequest"), sql.getSql());
	}
}

