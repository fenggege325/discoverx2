/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import java.util.HashMap;
import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.common.DBType;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 通过数据访问请求名称获取对应数据访问请求对象的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DAOConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private DAOConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
		if (this.m_DAOConfigEntries != null && !this.m_DAOConfigEntries.isEmpty()) {
			this.m_cfgdict = new HashMap<String, DAOConfigEntry>(this.m_DAOConfigEntries.size());
			for (DAOConfigEntry x : this.m_DAOConfigEntries) {
				this.m_cfgdict.put(x.getName(), x);
			}
		}
	}

	private static DAOConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return DAOConfig
	 */
	public static DAOConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new DAOConfig();
			}
		}
		return m_instance;
	}

	private List<DAOConfigEntry> m_DAOConfigEntries = null; // 包含的数据库请求信息描述配置项列表。
	private HashMap<String, DAOConfigEntry> m_cfgdict = null; // 数据库请求信息描述配置项对象的配置项名称和对象本身一一对应的Hashtable。

	/**
	 * 返回包含的数据库请求信息描述配置项列表。
	 * 
	 * @return List<DAOConfigEntry>
	 */
	public List<DAOConfigEntry> getDAOConfigEntries() {
		return this.m_DAOConfigEntries;
	}

	/**
	 * 设置包含的数据库请求信息描述配置项列表。
	 * 
	 * @param DAOConfigEntries List&lt;DAOConfigEntry&gt;
	 */
	public void setDAOConfigEntries(List<DAOConfigEntry> DAOConfigEntries) {
		this.m_DAOConfigEntries = DAOConfigEntries;
	}

	/**
	 * 获取指定类名（配置名）对应的数据库请求类的参数设置类实例。
	 * 
	 * @param className String，数据库请求唯一名称，可以使用简单类名（不包含package信息的类名）或全限定类名（包含完整package和类名本身）作为配置时候的名称。<br/>
	 *          下面获取其它配置项结果的className参数也使用同样的约定。
	 * @return ParametersSetter 如果没有配置或其它异常，则返回null.
	 */
	public ParametersSetter getParametersSetter(String className) {
		DAOConfigEntry entry = this.getDAOConfigEntry(className);
		if (entry == null) return null;
		String clsName = entry.getParametersSetter();
		if (clsName == null || clsName.length() == 0) return null;
		return Instance.newInstance(clsName, ParametersSetter.class);
	}

	/**
	 * 获取指定类信息对应的数据库请求类的参数设置类实例。
	 * 
	 * @param clazz Class&lt;?&gt;，先后从其全限定类名或简单类名尝试获取。
	 * @return
	 */
	public ParametersSetter getParametersSetter(Class<?> clazz) {
		if (clazz == null) return null;
		ParametersSetter result = this.getParametersSetter(clazz.getName());
		if (result == null) result = this.getParametersSetter(clazz.getSimpleName());
		return result;
	}

	/**
	 * 获取指定类名（配置名）对应的数据库请求类的最终结果构造对象实例。
	 * 
	 * @param className
	 * @return ResultBuilder 如果没有配置或其它异常，则返回null.
	 */
	public ResultBuilder getResultBuilder(String className) {
		DAOConfigEntry entry = this.getDAOConfigEntry(className);
		if (entry == null) return null;
		String clsName = entry.getResultBuilder();
		if (clsName == null || clsName.length() == 0) return null;
		return Instance.newInstance(clsName, ResultBuilder.class);
	}

	/**
	 * 获取指定类信息对应的数据库请求类的最终结果构造对象实例。
	 * 
	 * @param clazz
	 * @return
	 */
	public ResultBuilder getResultBuilder(Class<?> clazz) {
		if (clazz == null) return null;
		ResultBuilder result = this.getResultBuilder(clazz.getName());
		if (result == null) result = this.getResultBuilder(clazz.getSimpleName());
		return result;
	}

	/**
	 * 获取类名（配置名）对应的数据库请求类的配置信息。
	 * 
	 * @param className
	 * @return
	 */
	public DAOConfigEntry getDAOConfigEntry(String className) {
		if (className == null || className.length() == 0 || this.m_cfgdict == null) return null;
		return this.m_cfgdict.get(className);
	}

	/**
	 * 获取指定类信息对应的数据库请求类的配置信息。
	 * 
	 * @param clazz
	 * @return
	 */
	public DAOConfigEntry getDAOConfigEntry(Class<?> clazz) {
		if (clazz == null || this.m_cfgdict == null) return null;
		DAOConfigEntry result = this.getDAOConfigEntry(clazz.getName());
		if (result == null) result = this.getDAOConfigEntry(clazz.getSimpleName());
		return result;
	}

	/**
	 * 获取指类名（配置名）对应的数据库请求类的实例。
	 * 
	 * @param className
	 * @return
	 */
	public DBRequest getDBRequest(String className) {
		if (className == null || className.length() == 0) return null;
		DAOConfigEntry daocf = getDAOConfigEntry(className);
		if (daocf == null) return null;
		String implement = daocf.getImplement();
		if (implement == null || implement.length() == 0) throw new RuntimeException("没有配置名为“" + className + "”的数据库请求实现类信息！");
		return Instance.newInstance(implement, DBRequest.class);
	}

	/**
	 * 获取系统当前使用的视图查询实现对象（{@link com.tansuosoft.discoverx.bll.view.ViewQuery}）对应的实现类的全限定类名。
	 * 
	 * <p>
	 * DAOConfig.xml中必须配置名为“ViewQuery”的实现类信息。
	 * </p>
	 * 
	 * @return String
	 */
	public String getViewQueryImplement() {
		DAOConfigEntry daocf = getDAOConfigEntry("ViewQuery");
		String impl = (daocf == null ? null : daocf.getImplement());
		if (impl == null || impl.length() == 0) impl = "com.tansuosoft.discoverx.dao.mysql.ViewQuery";
		return impl;
	}

	/**
	 * 猜测系统默认使用的目标数据库类型。
	 * 
	 * <p>
	 * 此方法从配置的数据库访问接口实现中尝试获取系统默认使用的主数据库。
	 * </p>
	 * <p>
	 * <strong>如果要判断系统默认使用的数据库类型，建议优先使用此方法。</strong>
	 * </p>
	 * 
	 * @param defaultIfNoResult 如果猜不到则返回defaultIfNoResult。
	 * @return DBType
	 */
	public static DBType guessSystemDefaultDBType(DBType defaultIfNoResult) {
		DAOConfig daoc = DAOConfig.getInstance();
		DAOConfigEntry daoConfigEntry = daoc.getDAOConfigEntry("TableColumnsFetcher"); // 用于辅助判断目标数据库，因为DAOConfig.xml中有配置这个名称的数据库实现类。
		String tcfImpl = (daoConfigEntry == null ? null : daoConfigEntry.getImplement());
		if (tcfImpl != null && tcfImpl.startsWith("com.tansuosoft.discoverx.dao.mssql.")) { // sql server
			return DBType.Sqlserver;
		} else if (tcfImpl != null && tcfImpl.startsWith("com.tansuosoft.discoverx.dao.oracle.")) { // oracle
			return DBType.Oracle;
		} else if (tcfImpl != null && tcfImpl.startsWith("com.tansuosoft.discoverx.dao.mysql.")) { // mysql
			return DBType.Mysql;
		}
		return defaultIfNoResult; // 猜不到则返回默认的。
	}
}

