/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

/**
 * 下级请求链中依条件执行的{@link DBRequest}对象需要实现的接口。
 * 
 * @author coca@tansuosoft.cn
 */
public interface ConditionalDBRequest {
	/**
	 * 判断请求链中当前{@link DBRequest}是否能够执行。
	 * 
	 * <p>
	 * 一般根据上级{@link DBRequest}的请求结果（此时上级{@link DBRequest#sendRequest()}已经执行过了，但是结果根据上级{@link DBRequest#getResultBuildSequence()结果的不同，可能还没有组装最终结果}）来判断。<br/>
	 * 可以通过{@link DBRequest#getParentRequest()}来获取上级{@link DBRequest}。<br/>
	 * <strong>如果当前{@link DBRequest}不能执行，那么会继续执行请求链中的下一个{@link DBRequest}（如果存在的话）。</strong>
	 * </p>
	 * 
	 * @return true/false 如果当前{@link DBRequest}可以执行则返回true，否则返回false。
	 */
	public boolean check();
}

