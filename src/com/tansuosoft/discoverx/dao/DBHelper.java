/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import java.sql.SQLException;

import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.db.DatabaseFactory;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 用于直接访问数据库的辅助类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DBHelper {
	/**
	 * 缺省私有构造器。
	 */
	private DBHelper() {
	}

	/**
	 * 执行sql查询语句并返回结果集。
	 * 
	 * <p>
	 * 请确保使用完毕后调用DataReader.close关闭数据库连接!
	 * </p>
	 * 
	 * @param sql String-要查询的sql语句。
	 * @return DataReader
	 */
	public static DataReader executeQuery(String sql) {
		try {
			return DatabaseFactory.getDatabase().executeQuery(sql);
		} catch (SQLException e) {
			FileLogger.error(e);
		}
		return null;
	}

	/**
	 * 执行sql查询语句并返回结果集中的第一行第一列的标量值。
	 * 
	 * @param sql String-要执行的sql语句。
	 * @return Object 返回JDBC规范中SQL数据类型对应的java对象，如果查询返回空列值，则返回null。
	 */
	public static Object executeScalar(String sql) {
		try {
			return DatabaseFactory.getDatabase().executeScalar(sql);
		} catch (SQLException e) {
			FileLogger.error(e);
		}
		return null;
	}

	/**
	 * 执行非查询语句（更新、插入、删除等）。
	 * 
	 * @param sql 返回受语句影响的行数，0表示没有更改任何行。
	 * @return int
	 */
	public static int executeNonQuery(String sql) {
		try {
			return DatabaseFactory.getDatabase().executeNonQuery(sql);
		} catch (SQLException e) {
			FileLogger.error(e);
		}
		return 0;
	}

	/**
	 * 批量执行多条SQL语句并返回每一条SQL语句对应的处理结果影响的数据行数据数组。
	 * 
	 * @param sqls String数组，表示要执行的多条SQL语句
	 * @return int[]
	 * @throws SQLException
	 */
	public int[] executeBatch(String[] sqls) throws SQLException {
		try {
			return DatabaseFactory.getDatabase().executeBatch(sqls);
		} catch (SQLException e) {
			FileLogger.error(e);
		}
		return null;
	}
}

