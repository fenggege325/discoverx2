/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 数据库访问请求类型枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum RequestType implements EnumBase {
	/**
	 * 普通查询语句类型（1）。
	 */
	Query(1),
	/**
	 * 普通非查询语句类型，包括Update、Insert、Delete等（2）。
	 */
	NonQuery(2),
	/**
	 * 只需要数据库返回的结果集中的第一行第一列的标量值对应的的普通查询语句类型（4）。
	 */
	Scalar(3),
	/**
	 * 非数据库请求类型，{@link DBRequest#sendRequest()}将忽略具有此类型的{@link SQLWrapper}对象。
	 * 
	 * <p>
	 * 可用此类型来执行数据库请求后续操作。
	 * </p>
	 */
	NonRequest(0);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	RequestType(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return RequestType
	 */
	public RequestType parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (RequestType s : RequestType.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return RequestType.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return RequestType
	 */
	public static RequestType parse(int v) {
		for (RequestType s : RequestType.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

