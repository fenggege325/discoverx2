/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

/**
 * 如果上级请求更新成功则执行的{@link DBRequest}对应的{@link ConditionalDBRequest}实现类 。
 * 
 * <p>
 * 如果请求链中上级请求为{@link RequestType#NonQuery}类型的请求且结果返回大于0（表示写入成功），则可以执行当前{@link DBRequest}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class UpdateSuccessConditionalDBRequestAdapter extends ConditionalDBRequestAdapter {
	/**
	 * 接收原始{@link DBRequest}对象的构造器。
	 * 
	 * @param dbrequest
	 */
	public UpdateSuccessConditionalDBRequestAdapter(DBRequest dbrequest) {
		super(dbrequest);
	}

	/**
	 * 重载check：如果{@link #getParentRequest()}的{@link DBRequest#getResultLong()}返回大于0的值，则返回true，否则返回false。
	 * 
	 * @see com.tansuosoft.discoverx.dao.ConditionalDBRequest#check()
	 */
	@Override
	public boolean check() {
		DBRequest parent = this.getParentRequest();
		if (parent == null) return false;
		if (parent.getResultLong() > 0) return true;
		return false;
	}
}

