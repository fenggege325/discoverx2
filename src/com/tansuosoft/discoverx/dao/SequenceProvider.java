/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import java.util.Hashtable;

import com.tansuosoft.discoverx.model.Securer;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 根据不同的关键字提供自动增长累计的序列号的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class SequenceProvider implements Sequence {
	/**
	 * 系统全局使用的自增长序列号对应的Key值：SEQ_DEFAULT。
	 */
	public static final String DEFAULT_SEQUENCE_KEY = "SEQ_DEFAULT";

	/**
	 * 系统排序号使用的自增长序列号对应的Key值：SEQ_SORT。
	 */
	public static final String SORT_SEQUENCE_KEY = "SEQ_SORT";

	/**
	 * 为需要唯一安全编码的资源提供一个唯一安全编码的自增长序号对应的Key：SEQ_SECURITYCODE。
	 * <p>
	 * 请勿在非安全编码范围使用此序列号。
	 * </p>
	 */
	public static final String SECURITY_CODE_SEQUENCE_KEY = "SEQ_SECURITYCODE";

	/**
	 * DBConfig配置信息中配置的系统默认使用的Sequence实现类名称对应的参数值：SequenceImplement。
	 * 
	 * <p>
	 * DBConfig必须有此参数名对应的参数值（实现类名称），否则无法获取自增长序列号。
	 * </p>
	 */
	public static final String DEFAULT_SEQUENCE_IMPL_PARAM_NAME = "SequenceImplement";
	/**
	 * 包含所有关键字-序列号对象的哈希集合。
	 */
	private static Hashtable<String, SequenceProvider> m_instances = new Hashtable<String, SequenceProvider>();
	private Sequence m_sequence = null; // 绑定的Sequence对象实例。

	/**
	 * 接收Sequence对象实例的构造器。
	 * 
	 * @param sequence Sequence
	 */
	protected SequenceProvider(Sequence sequence) {
		this.m_sequence = sequence;
	}

	/**
	 * 获取{@link SequenceProvider#SECURITY_CODE_SEQUENCE_KEY}指定的指定的关键字对应的自增长序号提供对象，其初始值默认为{@link Securer#MIN_AVAILABLE_CODE}。
	 * 
	 * <p>
	 * <strong>使用此方法为需要唯一安全编码的资源提供一个唯一安全编码，其余地方不能使用！</strong>
	 * </p>
	 * 
	 * @return SequenceProvider 返回对应的自增长序号提供对象，如果没有在DBConfig中配置正确的自增长序号实现类名，则可能返回null。
	 */
	public static synchronized SequenceProvider getSecurityCodeInstance() {
		// 先从3个表中获取最大安全编码，如果大于等于Securer.MIN_AVAILABLE_CODE，则使用获取到的最大安全编码作为初始值。
		StringBuilder sb = new StringBuilder();
		sb.append("select max(sc) from (SELECT max(c_securitycode) sc FROM t_role");
		sb.append(" union ");
		sb.append("SELECT max(c_securitycode) sc FROM t_user");
		sb.append(" union ");
		sb.append("SELECT max(c_securitycode) sc FROM t_group");
		sb.append(") t");
		long initval = StringUtil.getValueLong(StringUtil.getValueString(DBHelper.executeScalar(sb.toString()), null), Securer.MIN_AVAILABLE_CODE);
		return getInstance(SECURITY_CODE_SEQUENCE_KEY, initval < Securer.MIN_AVAILABLE_CODE ? Securer.MIN_AVAILABLE_CODE : initval + 1);
	}

	/**
	 * 获取{@link SequenceProvider#DEFAULT_SEQUENCE_KEY}指定的关键字对应的自增长序号提供对象，其初始值默认为100。
	 * 
	 * @return SequenceProvider 返回对应的自增长序号提供对象，如果没有在DBConfig中配置正确的自增长序号实现类名，则可能返回null。
	 */
	public static synchronized SequenceProvider getDefaultInstance() {
		return getInstance(DEFAULT_SEQUENCE_KEY, 100);
	}

	/**
	 * 获取{@link SequenceProvider#DEFAULT_SEQUENCE_KEY}指定的关键字对应的自增长序号提供对象，其初始值默认为1。
	 * 
	 * @return SequenceProvider 返回对应的自增长序号提供对象，如果没有在DBConfig中配置正确的自增长序号实现类名，则可能返回null。
	 */
	public static synchronized SequenceProvider getSortInstance() {
		return getInstance(SORT_SEQUENCE_KEY, 1);
	}

	/**
	 * 获取key指定的关键字对应的自增长序号提供对象，其初始值默认为1。
	 * 
	 * @param key 自增长序号提供对象的Key值。
	 * @return SequenceProvider 返回对应的自增长序号提供对象，如果没有在DBConfig中配置正确的自增长序号实现类名，则可能返回null。
	 */
	public static synchronized SequenceProvider getInstance(String key) {
		return getInstance(key, 1);
	}

	/**
	 * 获取key指定的关键字对应的自增长序号提供对象，其初始值由initialization指定。
	 * 
	 * @param key 自增长序号提供对象的Key值。
	 * @param initialization key指定的关键字对应的自增长序号提供对象的初始值。
	 * @return SequenceProvider 返回对应的自增长序号提供对象，如果没有在DBConfig中配置正确的自增长序号实现类名，则可能返回null。
	 */
	public static synchronized SequenceProvider getInstance(String key, long initialization) {
		if (key == null || key.length() == 0) return null;
		SequenceProvider instance = null;
		instance = m_instances.get(key);
		if (instance != null) return instance;
		DAOConfig dbConfig = DAOConfig.getInstance();
		if (dbConfig == null) throw new RuntimeException("无法获取数据访问配置！");
		DAOConfigEntry entry = dbConfig.getDAOConfigEntry(DEFAULT_SEQUENCE_IMPL_PARAM_NAME);
		if (entry == null) throw new RuntimeException("取数据访问配置中没有配置序列提供器信息。");
		String impl = entry.getImplement();
		if (impl == null || impl.length() == 0) throw new RuntimeException("取数据访问配置中没有配置序列提供器实现类信息。");
		Sequence sequence = Instance.newInstance(impl, Sequence.class);
		if (sequence == null) throw new RuntimeException("取数据访问配置中没有配置序列提供器实现类。");
		sequence.setInitialization(initialization);
		sequence.setKey(key);
		instance = new SequenceProvider(sequence);
		m_instances.put(key, instance);
		return instance;
	}

	/**
	 * 重载getInitialization
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getInitialization()
	 */
	public long getInitialization() {
		return this.m_sequence.getInitialization();
	}

	/**
	 * 重载getKey
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getKey()
	 */
	public String getKey() {
		return this.m_sequence.getKey();
	}

	/**
	 * 重载getPreviewSequence
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getPreviewSequence()
	 */
	public long getPreviewSequence() {
		return this.m_sequence.getPreviewSequence();
	}

	/**
	 * 重载getSequence
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getSequence()
	 */
	public long getSequence() {
		return this.m_sequence.getSequence();
	}

	/**
	 * 重载getSequence
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#getSequence(long)
	 */
	public long getSequence(long step) {
		return this.m_sequence.getSequence(step);
	}

	/**
	 * 重载setInitialization
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#setInitialization(long)
	 */
	public void setInitialization(long initialization) {
		this.m_sequence.setInitialization(initialization);
	}

	/**
	 * 重载setKey
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#setKey(java.lang.String)
	 */
	public void setKey(String key) {
		this.m_sequence.setKey(key);
	}

	/**
	 * 重载setSequence
	 * 
	 * @see com.tansuosoft.discoverx.dao.Sequence#setSequence(long)
	 */
	public void setSequence(long sequence) {
		this.m_sequence.setSequence(sequence);
	}

}

