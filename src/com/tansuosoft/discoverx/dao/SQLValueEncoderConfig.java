/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.common.ConfigEntry;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 系统注册的所有SQL值编码器相关信息的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class SQLValueEncoderConfig extends Config implements SQLValueEncoder {
	/**
	 * 缺省构造器。
	 */
	private SQLValueEncoderConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static SQLValueEncoderConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return SQLValueEncoderConfig
	 */
	public static SQLValueEncoderConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new SQLValueEncoderConfig();
			}
		}
		return m_instance;
	}

	private List<SQLValueEncoder> m_encoderResult = null;

	/**
	 * 返回包含系统注册的所有{@link SQLValueEncoder}的实例的列表。
	 * 
	 * @return List&lt;SQLValueEncoder&gt;
	 */
	protected synchronized List<SQLValueEncoder> getRegisteredSQLValueEncoders() {
		if (m_encoderResult != null) return m_encoderResult;
		List<ConfigEntry> entries = this.getEntries();
		if (entries == null || entries.isEmpty()) return null;
		SQLValueEncoder encoder = null;
		m_encoderResult = new ArrayList<SQLValueEncoder>(entries.size());
		for (ConfigEntry x : entries) {
			if (x == null || x.getValue() == null) continue;
			encoder = Instance.newInstance(x.getValue(), SQLValueEncoder.class);
			if (encoder != null) m_encoderResult.add(encoder);
		}
		return m_encoderResult;
	}

	/**
	 * 调用系统注册的所有编码器将输入值编码为最终结果并返回之。
	 * 
	 * <p>
	 * 参考{@link SQLValueEncoder}
	 * </p>
	 * 
	 * @param raw String
	 * @param compare String
	 * @return String，如果系统没有注册编码器，则返回原始值。
	 * 
	 * @see com.tansuosoft.discoverx.dao.SQLValueEncoder#encode(java.lang.String, java.lang.String)
	 */
	@Override
	public String encode(String raw, String compare) {
		if (raw == null) return null;
		if (raw.length() == 0) return raw;
		List<SQLValueEncoder> list = this.getRegisteredSQLValueEncoders();
		if (list == null || list.isEmpty()) return raw;
		String result = new String(raw);
		for (SQLValueEncoder x : list) {
			if (x == null) continue;
			result = x.encode(result, compare);
		}
		return result;
	}
}

