/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

/**
 * 将sql比较右值编码为符合SQL规范的结果的类需实现的统一接口。
 * 
 * <p>
 * 此接口对应的实现类统称：“SQL右值编码器”。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public interface SQLValueEncoder {
	/**
	 * 将输入值编码为符合SQL规范的结果并返回之。
	 * 
	 * @param raw 原始输入值。
	 * @param compare String 比较方式。
	 * @return 如果raw为null或空字符串，则直接返回，否则返回编码后的结果（如果有需要编码的字符的话），如果没有需要编码的字符，则返回结果与raw相同。
	 */
	public String encode(String raw, String compare);
}

