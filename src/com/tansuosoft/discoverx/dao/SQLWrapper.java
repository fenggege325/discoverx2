/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.dao;

/**
 * 封装发送数据库访问请求时所需相关信息的类。
 * 
 * <p>
 * 此对象通常包含sql语句及其请求类型{@link RequestType}等内容。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class SQLWrapper {
	/**
	 * 缺省构造器。
	 */
	public SQLWrapper() {
	}

	private String m_sql = null; // SQL（或模板）语句，必须。
	private RequestType m_requestType = RequestType.Query; // SQL语句对应的数据访问请求类型，默认为：RequestType.Query。
	private boolean m_parameterized = false; // sql语句是否包含待设置的参数。
	private String m_name = null; // 当前sql语句的名称，可选。

	/**
	 * 返回数据库请求所使用的SQL语句。
	 * 
	 * @return String
	 */
	public String getSql() {
		return this.m_sql;
	}

	/**
	 * 设置数据库请求所使用的SQL语句，必须。
	 * 
	 * @param sql String
	 */
	public void setSql(String sql) {
		this.m_sql = sql;
		if (this.m_sql != null && (this.m_sql.trim().startsWith("create") || this.m_sql.trim().startsWith("drop") || this.m_sql.trim().startsWith("delete") || this.m_sql.trim().startsWith("update") || this.m_sql.trim().startsWith("insert"))) {
			this.m_requestType = RequestType.NonQuery;
		}
		if (this.m_sql != null && (this.m_sql.indexOf(",?,") > 0 || this.m_sql.indexOf("=?") > 0 || this.m_sql.indexOf(" =?") > 0 || this.m_sql.indexOf(" = ?") > 0 || this.m_sql.indexOf("= ?") > 0 || this.m_sql.indexOf("(?") > 0 || this.m_sql.indexOf("( ?") > 0)) this.m_parameterized = true;
	}

	/**
	 * 返回SQL语句对应的数据访问请求类型，默认为：{@link RequestType#Query}。
	 * 
	 * @return {@link RequestType#Query}
	 */
	public RequestType getRequestType() {
		return this.m_requestType;
	}

	/**
	 * 设置SQL语句对应的数据访问请求类型。
	 * 
	 * <p>
	 * <strong>必须设置正确的请求类型，如果请求类型和{@link SQLWrapper#setSql(String)}传入的sql信息不匹配，则会导致异常！</strong>
	 * </p>
	 * 
	 * @param requestType
	 */
	public void setRequestType(RequestType requestType) {
		this.m_requestType = requestType;
	}

	/**
	 * 返回sql语句是否包含待设置的参数。
	 * 
	 * @see SQLWrapper#setParameterized(boolean)
	 * @return boolean
	 */
	public boolean getParameterized() {
		return this.m_parameterized;
	}

	/**
	 * 设置sql语句是否包含待设置的参数。
	 * 
	 * <p>
	 * 带参数的sql用半角问号作为具体参数值的替代符，运行时通过{@link ParametersSetter}。
	 * </p>
	 * <p>
	 * 默认为false。设置带参数sql语句时，要同步设置此属性为true，否则可能出错，反之，如果设置了不带参数的sql语句而设置了此属性为true，也将导致出错。
	 * </p>
	 * 
	 * @param parameterized boolean
	 */
	public void setParameterized(boolean parameterized) {
		this.m_parameterized = parameterized;
	}

	/**
	 * 返回当前sql语句的名称,可选。
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置当前sql语句的名称,可选。
	 * 
	 * @param name String
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	private static SQLWrapper m_instance = new SQLWrapper();
	static {
		m_instance.setRequestType(RequestType.NonRequest);
	}

	/**
	 * 返回一个类型为“{@link RequestType#NonRequest}”的{@link SQLWrapper}对象。
	 * 
	 * @return
	 */
	public static SQLWrapper getNonRequestSQLWrapper() {
		return m_instance;
	}
}

