/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 包含在资源中的指向其它资源的资源引用类
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Reference implements Cloneable {
	private String m_directory = null; // 引用所指目标资源的目录，有子目录（下级分类时），请用“\”(反斜杠)分隔，必须。
	private String m_unid = null; // 引用所指目标资源的UNID
	private String m_title = null; // 引用所指目标资源的标题
	private int m_sort = 0; // 引用所指目标资源的排序号。

	/**
	 * 缺省构造器。
	 */
	public Reference() {
	}

	/**
	 * unid接收构造器
	 * 
	 */
	public Reference(String unid, String title, String directory, int sort) {
		this.m_unid = unid;
		this.m_title = title;
		this.m_directory = directory;
		this.m_sort = sort;
	}

	/**
	 * 返回引用所指目标资源的目录，可选。
	 * 
	 * @return String
	 */
	public String getDirectory() {
		return this.m_directory;
	}

	/**
	 * 设置引用所指目标资源的目录，可选。
	 * 
	 * @param directory String
	 */
	public void setDirectory(String directory) {
		this.m_directory = directory;
	}

	/**
	 * 返回引用所指目标资源的UNID。
	 * 
	 * @return String 引用所指目标资源的UNID
	 */
	public String getUnid() {
		return this.m_unid;
	}

	/**
	 * 设置引用所指目标资源的UNID。
	 * 
	 * @param unid String 引用所指目标资源的UNID
	 */
	public void setUnid(String unid) {
		this.m_unid = unid;
	}

	/**
	 * 返回引用所指目标资源的标题。
	 * 
	 * @return String 引用所指目标资源的标题
	 */
	public String getTitle() {
		return this.m_title;
	}

	/**
	 * 设置引用所指目标资源的标题。
	 * <p>
	 * 如果设置title属性为null或空字符串，则unid将同时被设置为null。
	 * </p>
	 * 
	 * @param title String 引用所指目标资源的标题
	 */
	public void setTitle(String title) {
		this.m_title = title;
		if (this.m_title == null || this.m_title.trim().length() == 0) {
			this.m_unid = null;
		}
	}

	/**
	 * 返回引用所指目标资源的排序号（包含同一类型的多个不同引用时才有意义）。
	 * 
	 * @return int 引用所指目标资源的排序号。
	 */
	public int getSort() {
		return this.m_sort;
	}

	/**
	 * 设置引用所指目标资源的排序号（包含同一类型的多个不同引用时才有意义）。
	 * 
	 * @param sort int 引用所指目标资源的排序号。
	 */
	public void setSort(int sort) {
		this.m_sort = sort;
	}

	/**
	 * 判断是否有效引用。
	 * 
	 * <p>
	 * 有效引用必须确保unid属性有合法值。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean isValidReference() {
		return (this.m_unid != null && this.m_unid.length() == 32);
	}

	/**
	 * 返回toString()结果是否相等。
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return toString().equals(obj == null ? null : obj.toString());
	}

	/**
	 * 返回toString()结果的hashCode。
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * toString返回格式：getDirectories()://getUnid()
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.m_directory == null ? "" : this.m_directory);
		sb.append("://");
		sb.append(this.m_unid == null ? "" : this.m_unid);
		return sb.toString();
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Reference x = new Reference();
		x.setDirectory(this.getDirectory());
		x.setSort(this.getSort());
		x.setTitle(this.getTitle());
		x.setUnid(this.getUnid());
		return x;
	}
}

