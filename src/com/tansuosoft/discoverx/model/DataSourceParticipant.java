/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示参与者信息的数据源的类。
 * 
 * <p>
 * {@link DataSource#getTitleFormat()}默认值为“cn”（表示返回用户姓名作为显示用），配置时必须提供有效的格式，否则可能出错。
 * </p>
 * <p>
 * {@link DataSource#getValueFormat()}默认值为“cn”（表示返回用户姓名作为结果用），配置时必须提供有效的格式，否则可能出错。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DataSourceParticipant extends DataSource {
	private ParticipantType m_participantType = ParticipantType.Person;

	/**
	 * 缺省构造器。
	 */
	public DataSourceParticipant() {
		super();
		this.setTitleFormat("cn");
		this.setValueFormat("cn");
	}

	/**
	 * 返回参与者类型。
	 * 
	 * <p>
	 * 默认为用户参与者：{@link ParticipantType#Person}。
	 * </p>
	 * 
	 * @return ParticipantType
	 */
	public ParticipantType getParticipantType() {
		return this.m_participantType;
	}

	/**
	 * 设置参与者类型。
	 * 
	 * @param participantType ParticipantType
	 */
	public void setParticipantType(ParticipantType participantType) {
		this.m_participantType = participantType;
	}
}

