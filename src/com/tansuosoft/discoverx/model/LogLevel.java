/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 日志级别枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum LogLevel implements EnumBase {
	/**
	 * 消息级别
	 */
	Information(0),
	/**
	 * 警告级别
	 */
	Warning(1),
	/**
	 * 错误级别
	 */
	Error(2),
	/**
	 * 其它
	 */
	Other(9);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	LogLevel(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return LogLevel
	 */
	public LogLevel parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (LogLevel s : LogLevel.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return LogLevel.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return LogLevel
	 */
	public static LogLevel parse(int v) {
		for (LogLevel s : LogLevel.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

	/**
	 * 根据枚举对象返回相关文字信息
	 * 
	 * @param logType
	 * @return String
	 */
	public static String getStringValue(LogLevel logLevel) {
		String value = "";
		switch (logLevel.getIntValue()) {
		case 0:
			value = "错误级别";
			break;
		case 1:
			value = "警告级别";
			break;
		case 2:
			value = "消息级别";
			break;
		case 9:
			value = "其它";
			break;
		default:
			break;
		}
		return value;
	}
}

