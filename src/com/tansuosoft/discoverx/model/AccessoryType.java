/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.VerbotenUploadFileTypeConfig;

/**
 * 附加文件类型资源类。
 * 
 * <p>
 * 由于附加文件可能有不同的类型，比如正文、附件、阅办单（办理单）、正文模板等，附加文件类型用于区分这些类型。
 * <p>
 * 
 * @author coca@tansuosoft.cn
 */
public class AccessoryType extends Resource {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 3102342215499937728L;

	/**
	 * 图片类型{@link ItemType#Image}的字段上传的图片所对应的附加文件类型。
	 */
	public static final String IMAGE_FIELD_TYPE = "actImage";

	/**
	 * 缺省构造器。
	 */
	public AccessoryType() {
	}

	private boolean m_visible = true; // 是否在容器的附加文件操作区域中显示此附加文件类型对应的附加文件。

	/**
	 * 返回是否在容器的附加文件操作区域中显示此附加文件类型对应的附加文件，缺省为true。
	 * 
	 * @return boolean
	 */
	public boolean getVisible() {
		return this.m_visible;
	}

	/**
	 * 设置是否在容器的附加文件操作区域中显示此附加文件类型对应的附加文件，缺省为true。
	 * 
	 * @param visible boolean
	 */
	public void setVisible(boolean visible) {
		this.m_visible = visible;
	}

	private boolean m_restrictToConfig = false; // 是否限制在配置中使用，默认为false。

	/**
	 * 返回是否限制在配置中使用，默认为false。
	 * 
	 * <p>
	 * 为true则表示只能用于配置中使用，无法在文档中创建此类附加文件。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getRestrictToConfig() {
		return this.m_restrictToConfig;
	}

	/**
	 * 设置是否限制在配置中使用，默认为false。
	 * 
	 * @param restrictToConfig boolean
	 */
	public void setRestrictToConfig(boolean restrictToConfig) {
		this.m_restrictToConfig = restrictToConfig;
	}

	private String m_fileNameExtension = null; // 上传此类型的文件时允许的扩展名。
	private int m_minCount = 0; // 允许上传此类型文件的最大个数。
	private int m_maxCount = 0; // 允许上传此类型文件的最小个数。
	private int m_maxSize = 0; // 允许上传此类型文件最大字节数。

	/**
	 * 返回上传此类型的文件时允许的扩展名。
	 * 
	 * @return String
	 */
	public String getFileNameExtension() {
		return this.m_fileNameExtension;
	}

	/**
	 * 设置上传此类型的文件时允许的扩展名。
	 * 
	 * <p>
	 * 如果不设置，则默认可以选择所有文件，但是{@link VerbotenUploadFileTypeConfig}中的配置项仍会限制可上传文件的扩展名。<br/>
	 * 如果设置，格式为“.ext1,.ext2...”，（请注意半角逗号分割符分隔多个扩展名且每个扩展名必须以半角点号开始），此时只能上传在此指定的扩展名的附加文件。
	 * </p>
	 * 
	 * @param fileNameExtension String
	 */
	public void setFileNameExtension(String fileNameExtension) {
		this.m_fileNameExtension = fileNameExtension;
	}

	/**
	 * 返回允许上传此类型文件的最小个数。
	 * 
	 * <p>
	 * 默认为0，表示可以不上传。
	 * </p>
	 * 
	 * @return int
	 */
	public int getMinCount() {
		return this.m_minCount;
	}

	/**
	 * 设置允许上传此类型文件的最小个数。
	 * 
	 * @param minCount int
	 */
	public void setMinCount(int minCount) {
		this.m_minCount = minCount;
	}

	/**
	 * 返回允许上传此类型文件的最大个数。
	 * 
	 * <p>
	 * 默认为0，表示无上限。
	 * </p>
	 * 
	 * @return int
	 */
	public int getMaxCount() {
		return this.m_maxCount;
	}

	/**
	 * 设置允许上传此类型文件的最大个数。
	 * 
	 * @param maxCount int
	 */
	public void setMaxCount(int maxCount) {
		this.m_maxCount = maxCount;
	}

	/**
	 * 返回允许上传此类型文件的最大字节数。
	 * 
	 * <p>
	 * 默认为0，表示由{@link CommonConfig#getMaxUploadSize()}决定。
	 * </p>
	 * 
	 * @return int
	 */
	public int getMaxSize() {
		return this.m_maxSize;
	}

	/**
	 * 设置允许上传此类型文件的最大字节数。
	 * 
	 * @param maxSize int
	 */
	public void setMaxSize(int maxSize) {
		this.m_maxSize = maxSize;
	}

}

