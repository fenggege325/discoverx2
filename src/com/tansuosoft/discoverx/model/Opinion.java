/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示一条意见（或评论、答复等）资源的类。
 * 
 * <p>
 * 意见(评论、答复)资源通常表示用户在审批、阅读、使用文档（{@link Document}）过程中填写的各种类型的审批意见、评论、答复等内容的一个系统抽象，它必须包含于某个文档资源，孤立存在的意见资源没有意义。
 * </p>
 * <p>
 * 审批意见的名称、说明、别名、分类等的属性值可以根据具体需求保存、解析并为不同的信息。<br/>
 * 其中系统默认在名称中填入的内容为“意见作者直接部门名称\意见作者姓名”。<br/>
 * 如果不设置分类，那么getCategory()默认返回意见类别名称。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Opinion extends Resource {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -5076621229106073723L;

	/**
	 * 缺省构造器
	 */
	public Opinion() {
		super();
	}

	/**
	 * 接收所属资源UNID的构造器。
	 * 
	 * <p>
	 * 比如一个正文属于某个文档，那么这个属性保存文档的UNID。
	 * </p>
	 * 
	 * @param parentUNID
	 */
	public Opinion(String parentUNID) {
		this();
		this.setPUNID(parentUNID);
	}

	private String m_opinionType = OpinionType.DEFAULT_OPINION_TYPE_UNID; // 意见类别引用，必须。
	private String m_agent = null; // 代填意见的人员，可选。
	private String m_body = null; // 具体填写的意见内容，可选。
	private String m_state = null; // 哪个状态对应的意见，可选。
	private boolean m_passed = true; // 通过与否。
	private String m_signature = null; // 签名信息。
	private OpinionView m_opinionView = OpinionView.ViewByDocumentViewer; // 意见查看选项。

	/**
	 * 返回意见类别资源UNID，必须。
	 * 
	 * <p>
	 * 默认为DEFAULT_OPINION_TYPE_UNID
	 * </p>
	 * 
	 * @see Opinion#DEFAULT_OPINION_TYPE_UNID
	 * @return String 意见类别引用，必须。
	 */
	public String getOpinionType() {
		return m_opinionType;
	}

	/**
	 * 设置意见类别资源UNID，必须。
	 * 
	 * <p>
	 * 默认为DEFAULT_OPINION_TYPE_UNID
	 * </p>
	 * 
	 * @see Opinion#DEFAULT_OPINION_TYPE_UNID
	 * @param opinionType String 意见类别引用，必须。
	 */
	public void setOpinionType(String opinionType) {
		this.m_opinionType = opinionType;
	}

	/**
	 * 返回代填意见的人员，可选。
	 * 
	 * @return String 代填意见的人员，可选。
	 */
	public String getAgent() {
		return m_agent;
	}

	/**
	 * 设置代填意见的人员，可选。
	 * 
	 * @param agent 代填意见的人员，可选。
	 */
	public void setAgent(String agent) {
		this.m_agent = agent;
	}

	/**
	 * 返回具体填写的意见内容。
	 * 
	 * @return String
	 */
	public String getBody() {
		return m_body;
	}

	/**
	 * 设置具体填写的意见内容。
	 * 
	 * @param body
	 */
	public void setBody(String body) {
		this.m_body = body;
	}

	/**
	 * 返回哪个流程状态对应的意见。
	 * 
	 * <p>
	 * 格式为：“流程unid/流程环节unid”，如果流程unid省略，则表示文档当前流程。
	 * </p>
	 * 
	 * @return String
	 */
	public String getState() {
		return m_state;
	}

	/**
	 * 设置哪个流程状态对应的意见，可选。
	 * 
	 * @param state
	 */
	public void setState(String state) {
		this.m_state = state;
	}

	/**
	 * 返回通过与否。
	 * 
	 * <p>
	 * 默认为ture。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getPassed() {
		return this.m_passed;
	}

	/**
	 * 设置通过与否。
	 * 
	 * @param passed boolean
	 */
	public void setPassed(boolean passed) {
		this.m_passed = passed;
	}

	/**
	 * 返回签名信息，可选。
	 * 
	 * @return String
	 */
	public String getSignature() {
		return this.m_signature;
	}

	/**
	 * 设置签名信息。
	 * 
	 * @param signature String
	 */
	public void setSignature(String signature) {
		this.m_signature = signature;
	}

	/**
	 * 返回意见查看选项。
	 * 
	 * <p>
	 * 默认为{@link OpinionView#ViewByDocumentViewer}，表示所有可查看文档的人员均可以查看意见。
	 * </p>
	 * 
	 * @return OpinionView
	 */
	public OpinionView getOpinionView() {
		return this.m_opinionView;
	}

	/**
	 * 设置意见查看选项。
	 * 
	 * @param opinionView OpinionView
	 */
	public void setOpinionView(OpinionView opinionView) {
		this.m_opinionView = opinionView;
	}

	/**
	 * 获取所属意见类别资源。
	 * 
	 * @return String
	 */
	public OpinionType getOpinionTypeResource() {
		if (this.m_opinionType == null || this.m_opinionType.length() == 0) return null;
		OpinionType r = ResourceContext.getInstance().getResource(this.m_opinionType, OpinionType.class);
		if (r == null || !(r instanceof OpinionType)) return null;
		return r;
	}

	/**
	 * 获取所属意见类别名称。
	 * 
	 * @return String
	 */
	public String getOpinionTypeName() {
		OpinionType r = this.getOpinionTypeResource();
		return (r == null ? null : r.getName());
	}

	/**
	 * 获取所属意见类别别名。
	 * 
	 * @return 如果没有值则返回空字符串。
	 */
	public String getOpinionTypeAlias() {
		OpinionType r = this.getOpinionTypeResource();
		return (r == null ? null : r.getAlias());
	}

	/**
	 * 重载getCategory：默认返回意见类型名称（即没有有效值时返回{@link Opinion#getOpinionTypeName()}的结果）。
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#getCategory()
	 */
	public String getCategory() {
		String category = super.getCategory();
		if (category == null || category.length() == 0) return this.getOpinionTypeName();
		else return category;
	}

}

