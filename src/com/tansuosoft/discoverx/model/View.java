/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 定义获取系统数据列表的相关信息的资源类。
 * 
 * <p>
 * 视图主要用于按一定条件访问文档等资源的数据列表。<br/>
 * 它包括数据源来自哪些表/资源、有哪些列（视图列）、选择条件如何、怎样排序数据等信息。
 * </p>
 * 
 * <p>
 * <strong>视图资源的{@link Resource#getPUNID()}结果为所属应用模块的UNID。</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class View extends Resource implements Cloneable {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -4863456126939573765L;

	/**
	 * 系统默认的分页视图每页显示记录条目数：30。
	 */
	public static final int DEFAULT_DISPLAY_COUNT_PER_PAGE = 30;

	/**
	 * 缺省构造器
	 */
	public View() {
	}

	private String m_table = null; // 查询数据源的表，必须。
	private List<ViewColumn> m_columns = new ArrayList<ViewColumn>(); // 视图数据列配置。
	private String m_commonCondition = null; // 视图绑定的通用选择（查询）条件配置名称，可选。
	private List<ViewCondition> m_conditions = new ArrayList<ViewCondition>(); // 视图选择（查询）条件配置。
	private List<ViewOrder> m_orders = new ArrayList<ViewOrder>(); // 视图排序依据配置。
	private int m_displayCountPerPage = DEFAULT_DISPLAY_COUNT_PER_PAGE; // （分页）视图每页显示记录条目数，默认为20条。
	private String m_rawQuery = null; // 直接提供给视图查询用的原始SQL语句。
	private ViewType m_viewType = ViewType.DocumentView; // 视图类型，默认为文档视图。
	private boolean m_showSelector = false; // 视图呈现时是否可以在显示出来的每一条数据前显示选择框，缺省为false。
	private boolean m_portalDatasource = false; // 视图数据是否可以作为门户数据源，默认为false。
	private boolean m_applicationOutline = true; // 视图是否自动作为所属模块的导航大纲。
	private String m_display = null; // 自定义视图内容显示的视图呈现的信息。
	private boolean m_debugable = false; // 是否调试状态（调试状态为true时会输出SQL语句日志），默认为false。
	private List<Operation> m_operations = null; // 包含的视图操作集合，可选。
	private String m_viewParserImplement = null; // 视图解析对象实现类的全限定类名。
	private String m_viewQueryImplement = null; // 视图查询对象实现类的全限定类名。
	private SizeMeasurement m_measurement = SizeMeasurement.Percentage; // 相关尺寸配置值的度量单位。
	private boolean m_cache = false; // 是否缓存视图数据。
	private int m_cacheExpires = 60; // 缓存过期时间（分钟）。
	private boolean m_cacheByUser = true; // 是否按用户缓存视图查询结果。
	private boolean m_numberIndicator = false; // 是否显示视图条目数字。
	private int m_cachePages = 3; // 缓存的页数。

	/**
	 * 返回视图数据条目来源表名称，必须。
	 * 
	 * <p>
	 * 对于数据条目来源于数据库表的视图，如果是文档视图，则应返回文档所属表单别名，否则为具体表名。
	 * </p>
	 * <p>
	 * 对于数据条目来源于XML文件的的资源视图，应返回资源目录名称。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTable() {
		return m_table;
	}

	/**
	 * 设置视图数据条目来源表名称，必须。
	 * 
	 * @param table String
	 */
	public void setTable(String table) {
		this.m_table = table;
	}

	/**
	 * 返回视图数据列配置。
	 * 
	 * @return List&lt;ViewColumn&gt;
	 */
	public List<ViewColumn> getColumns() {
		return m_columns;
	}

	/**
	 * 设置视图数据列配置。
	 * 
	 * <p>
	 * 如果视图数据来字手工输入，则必须配置对应占位符列。
	 * </p>
	 * 
	 * @see View#setRawQuery(String)
	 * @param columns
	 */
	public void setColumns(List<ViewColumn> columns) {
		this.m_columns = columns;
	}

	/**
	 * 返回视图选择（查询）条件配置。
	 * 
	 * @see View#getCommonCondition()
	 * 
	 * @return List&lt;ViewCondition&gt;
	 */
	public List<ViewCondition> getConditions() {
		return m_conditions;
	}

	/**
	 * 设置视图选择（查询）条件配置。
	 * 
	 * @see View#getConditions()
	 * 
	 * @param conditions
	 */
	public void setConditions(List<ViewCondition> conditions) {
		this.m_conditions = conditions;
	}

	/**
	 * 返回视图排序依据配置。
	 * 
	 * @return List&lt;ViewOrder&lt;
	 */
	public List<ViewOrder> getOrders() {
		return m_orders;
	}

	/**
	 * 设置视图排序依据配置。
	 * 
	 * @param orders
	 */
	public void setOrders(List<ViewOrder> orders) {
		this.m_orders = orders;
	}

	/**
	 * 返回视图每页显示记录条目数，默认为20条。
	 * 
	 * <p>
	 * 为0表示不分页，显示所有内容。
	 * </p>
	 * 
	 * @return int
	 */
	public int getDisplayCountPerPage() {
		return m_displayCountPerPage;
	}

	/**
	 * 设置视图每页显示记录条目数，默认为20条。
	 * 
	 * @see View#getDisplayCountPerPage()
	 * @param displayCountPerPage
	 */
	public void setDisplayCountPerPage(int displayCountPerPage) {
		this.m_displayCountPerPage = displayCountPerPage;
	}

	/**
	 * 返回直接提供给视图查询用的原始SQL语句。
	 * 
	 * @see View#setRawQuery(String)
	 * @return String
	 */
	public String getRawQuery() {
		return m_rawQuery;
	}

	/**
	 * 设置直接提供给视图查询用的原始SQL语句。
	 * 
	 * <p>
	 * 视图数据来自数据库表的视图才能够手工输入sql语句。
	 * </p>
	 * <p>
	 * 手工输入查询SQL语句时，请注意不同的数据库平台的SQL语法等可能不一样。
	 * </p>
	 * <p>
	 * 在输入sql语句时，可以使用符合系统规范的表达式，比如输入：<br/>
	 * “select c_unid,c_name from t_document where c_created='@Datetime(format:="yyyy-MM-dd")'”。<br/>
	 * 则此语句中“@Datetime(format:="yyyy-MM-dd")”部分表示一个公式。<br/>
	 * 假设这个公式返回结果为：“2009-09-27”，那么最终sql语句为：<br/>
	 * “select c_unid,c_name from t_document where c_created='2009-09-27'”。 <br/>
	 * </p>
	 * <p>
	 * 如果有手工输入的原始SQL语句，那么配置的各种条件、排序等信息都将被忽略。<br/>
	 * 但是视图包含的视图列（{@link ViewColumn}）集合中必须配置与查询语句中选择出来的数据列一一对应的视图列信息。<br/>
	 * 如上述例子中“c_unid,c_name”为sql语句的数剧列，那么视图列配置中必须配置两个对应的视图列<br/>
	 * 视图列配置时，表名和字段名使用占位符，别名、宽度、标题等必须提供，否则可能无法正常显示查询结果对应的视图数据条目内容。<br/>
	 * </p>
	 * <p>
	 * 如果要求列出视图原始SQL语句查询出来的结果数据条目时可以使每一条数据条目对应一个资源的链接，请在第一个选择列中选择各资源主表的c_unid列，如“select t_document.c_unid...”。<br/>
	 * </p>
	 * <p>
	 * 如果需要分页，那么原始sql查询语句中必须包含order by字句，否则将导致无法分页或每次显示某页分页记录时内容不明确！<br/>
	 * 对于SQLServer2005及其以上版本如果要分页，则必须在SQL语句中提供“row_number() over(order by ...) as rn_”子句作为分页依据。
	 * </p>
	 * 
	 * @param rawQuery
	 */
	public void setRawQuery(String rawQuery) {
		this.m_rawQuery = rawQuery;
	}

	/**
	 * 返回视图类型。
	 * 
	 * <p>
	 * 默认为文档视图：{@link ViewType#DocumentView}。
	 * </p>
	 * 
	 * @return ViewType
	 */
	public ViewType getViewType() {
		return this.m_viewType;
	}

	/**
	 * 设置视图类型。
	 * 
	 * @param viewType ViewType
	 */
	public void setViewType(ViewType viewType) {
		this.m_viewType = viewType;
	}

	/**
	 * 返回视图呈现时是否可以在显示出来的每一条数据前显示选择框，缺省为false。
	 * 
	 * @return boolean
	 */
	public boolean getShowSelector() {
		return m_showSelector;
	}

	/**
	 * 设置视图呈现时是否可以在显示出来的每一条数据前显示选择框。
	 * 
	 * @param showSelector
	 */
	public void setShowSelector(boolean showSelector) {
		this.m_showSelector = showSelector;
	}

	/**
	 * 返回视图数据是否可以作为门户数据源，默认为false。
	 * 
	 * <p>
	 * 如果在新建视图时，设置此属性为true，那么保存新建的视图时，将自动创建一个指向视图的Ajax类型的门户小窗口数据源资源({@link PortletDataSource})。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getPortalDatasource() {
		return m_portalDatasource;
	}

	/**
	 * 设置视图数据是否可以作为门户数据源。
	 * 
	 * @param portalDatasource
	 */
	public void setPortalDatasource(boolean portalDatasource) {
		this.m_portalDatasource = portalDatasource;
	}

	/**
	 * 返回文档视图内容自定义呈现信息。
	 * 
	 * <p>
	 * 如果需要自定义呈现视图内容，则可以配置为以下两种自定义呈现信息：<br/>
	 * <ul>
	 * <li>视图数据后台组装信息，表示{@link com.tansuosoft.discoverx.web.ui.view.ViewEntryCallbackConfig}的某一个配置项名称，用来指定视图内容在后台返回给前台时的信息组装格式。<br/>
	 * 比如配置为“calendarview”、“listview”、“xml”等，也可以配置为实现{@link com.tansuosoft.discoverx.bll.view.ViewEntryCallback}接口的全限定类名。</li>
	 * <li>视图数据前台组装信息，用来指定组装视图后台返回的数据组装内容（一般是json格式的数据）为最终html的javascript引用和具体呈现对象。<br/>
	 * 配置个格式为：“[{js引用路径}]:{组装对象名}”，其中“{js引用路径}”为可选，“:{组装对象名}”为必须。<br/>
	 * 比如：“<strong>{frame}js/calendarview.js:calendarView</strong>”这个配置中：<br/>
	 * “<strong>{frame}js/calendarview.js</strong>” 为js引用路径，其中路径变量的配置方式请参考{@link com.tansuosoft.discoverx.web.PathContext#parseUrl(String)}中的说明；<br/>
	 * “<strong>calendarView</strong>”为具体前台组装对象，注意要严格区分大小写。</li>
	 * </ul>
	 * 如果没有配置此属性值，则默认的后台数据组装配置项名称为“listview”、默认的前台html组装对象为“{frame}js/view.js”中定义的viewListView。<br/>
	 * 如果配置为视图数据前台组装信息，那么视图数据后台组装信息可以通过{@link com.tansuosoft.discoverx.web.ui.view.ViewForm#setViewEntryCallback(String)}设置。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDisplay() {
		return m_display;
	}

	/**
	 * 设置文档视图内容自定义呈现信息。
	 * 
	 * @param display
	 */
	public void setDisplay(String display) {
		this.m_display = display;
	}

	/**
	 * 返回是否调试状态（调试状态为true时会输出SQL语句日志），默认为false。
	 * 
	 * @return boolean
	 */
	public boolean getDebugable() {
		return this.m_debugable;
	}

	/**
	 * 设置是否调试状态。
	 * 
	 * @param debugable boolean
	 */
	public void setDebugable(boolean debugable) {
		this.m_debugable = debugable;
	}

	/**
	 * 返回视图绑定的通用选择（查询）条件配置名称，可选。
	 * 
	 * <p>
	 * 实际查询时，视图配置的通用查询条件对应的语句位于视图直接配置的查询条件对应的语句之前。
	 * </p>
	 * 
	 * @see View#getConditions()
	 * 
	 * @return String
	 */
	public String getCommonCondition() {
		return this.m_commonCondition;
	}

	/**
	 * 设置视图绑定的通用选择（查询）条件配置名称，可选。
	 * 
	 * @see View#getCommonCondition()
	 * 
	 * @param commonCondition String
	 */
	public void setCommonCondition(String commonCondition) {
		this.m_commonCondition = commonCondition;
	}

	/**
	 * 返回包含的视图自定义操作集合，可选。
	 * 
	 * <p>
	 * 根据视图的不同属性组合，系统会自动提供一些默认操作，此处配置的是出了这些默认操作之外的额外操作。
	 * </p>
	 * 
	 * @return List&lt;Operation&gt;
	 */
	public List<Operation> getOperations() {
		return this.m_operations;
	}

	/**
	 * 设置包含的视图自定义操作集合。
	 * 
	 * @param operations List&lt;Operation&gt;
	 */
	public void setOperations(List<Operation> operations) {
		this.m_operations = operations;
	}

	/**
	 * 返回视图解析对象实现类的全限定类名。
	 * 
	 * @see View#setViewParserImplement(String)
	 * @return String
	 */
	public String getViewParserImplement() {
		return this.m_viewParserImplement;
	}

	/**
	 * 设置视图解析对象实现类的全限定类名。
	 * 
	 * <p>
	 * 可选，默认会由系统自动提供，对于其它类型（即{@link ViewType}为Others的类型）的视图，必须提供。
	 * </p>
	 * <p>
	 * 配置的类名必须是{@link com.tansuosoft.discoverx.bll.view.ViewParser}的实现类。
	 * </p>
	 * 
	 * @param viewParserImplement String
	 */
	public void setViewParserImplement(String viewParserImplement) {
		this.m_viewParserImplement = viewParserImplement;
	}

	/**
	 * 返回视图查询对象实现类的全限定类名。
	 * 
	 * @see View#setViewQueryImplement(String)
	 * @return String
	 */
	public String getViewQueryImplement() {
		return this.m_viewQueryImplement;
	}

	/**
	 * 设置视图查询对象实现类的全限定类名。
	 * 
	 * <p>
	 * 可选，默认会由系统自动提供，对于其它类型（即{@link ViewType}为Others的类型）的视图，必须提供。
	 * </p>
	 * <p>
	 * 配置的类名必须是{@link com.tansuosoft.discoverx.bll.view.ViewQuery}的实现类。
	 * </p>
	 * 
	 * @param viewQueryImplement String
	 */
	public void setViewQueryImplement(String viewQueryImplement) {
		this.m_viewQueryImplement = viewQueryImplement;
	}

	/**
	 * 返回相关尺寸配置值的度量单位。
	 * 
	 * <p>
	 * 默认为“{@link SizeMeasurement#Percentage}”
	 * </p>
	 * 
	 * @return SizeMeasurement
	 */
	public SizeMeasurement getMeasurement() {
		return this.m_measurement;
	}

	/**
	 * 设置相关尺寸配置值的度量单位。
	 * 
	 * @param measurement SizeMeasurement
	 */
	public void setMeasurement(SizeMeasurement measurement) {
		this.m_measurement = measurement;
	}

	/**
	 * 返回视图是否自动作为所属模块的导航大纲。
	 * 
	 * <p>
	 * 默认为true。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getApplicationOutline() {
		return this.m_applicationOutline;
	}

	/**
	 * 设置视图是否自动作为所属模块的导航大纲。
	 * 
	 * @param applicationOutline boolean
	 */
	public void setApplicationOutline(boolean applicationOutline) {
		this.m_applicationOutline = applicationOutline;
	}

	/**
	 * 返回是否缓存视图查询结果数据。
	 * 
	 * <p>
	 * 默认为false，如果为true，则视图查询结果数据(包括分类信息、条目数、视图数据条目等)将被缓存在内存中，这样可以加快视图数据访问。
	 * </p>
	 * <p>
	 * 启用视图数据缓存是否请谨慎权衡以下优缺点：改善视图显示时间；用户量大时降低数据库并发访问量；获取到的视图数据可能和数据库中的实际数据不一致；对于包含大量数据的视图进行缓存需要较多内存。<br/>
	 * 通常缓存视图数据主要为了改善性能或降低并发数据库连接。通常启用缓存基于以下前提：视图数据不经常改变或对改变不敏感；同一份视图数据可供不同用户同时使用；视图数据不会很大而占用过多内存。
	 * </p>
	 * <p>
	 * 只有数据来自数据库表的视图且视图数据获取时没有追加额外搜索条件时才有意义。<br/>
	 * 视图分类信息和视图数据总条目数总是会被缓存，如果此属性返回false，则它们缓存的时间默认为60分钟且为不同用户缓存不同数据。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getCache() {
		return this.m_cache;
	}

	/**
	 * 设置是否缓存视图查询结果数据。
	 * 
	 * @param cache boolean
	 */
	public void setCache(boolean cache) {
		this.m_cache = cache;
	}

	/**
	 * 返回缓存过期时间（分钟）。
	 * 
	 * <p>
	 * 单位为分钟，默认为60。缓存过期后，将重新从数据库获取新数据。
	 * </p>
	 * 
	 * @return int
	 */
	public int getCacheExpires() {
		return this.m_cacheExpires;
	}

	/**
	 * 设置缓存过期时间（分钟）。
	 * 
	 * <p>
	 * 只有{@link #getCache()}返回true时才有意义。
	 * </p>
	 * 
	 * @param cacheExpires int
	 */
	public void setCacheExpires(int cacheExpires) {
		this.m_cacheExpires = cacheExpires;
	}

	/**
	 * 返回是否按用户缓存视图查询结果。
	 * 
	 * <p>
	 * 默认为true表示为每个访问此视图查询数据的用户缓存数据（适用于每个用户查询出来的数据不同的情况，更耗内存），如果为false则表示为所有用户缓存一份视图查询数据（适用于每个用户查询出来的数据相同的情况，更省内存）。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getCacheByUser() {
		return this.m_cacheByUser;
	}

	/**
	 * 设置是否按用户缓存视图查询结果。
	 * 
	 * <p>
	 * 只有{@link #getCache()}返回true时才有意义。
	 * </p>
	 * 
	 * @param cacheByUser boolean
	 */
	public void setCacheByUser(boolean cacheByUser) {
		this.m_cacheByUser = cacheByUser;
	}

	/**
	 * 返回缓存的数据的页数。
	 * 
	 * <p>
	 * 为0表示缓存所有页（即缓存所有数据，可能严重消耗内存）。<br/>
	 * 默认为3。
	 * </p>
	 * 
	 * @return int
	 */
	public int getCachePages() {
		return this.m_cachePages;
	}

	/**
	 * 设置缓存的数据的页数。
	 * 
	 * @param cachePages int
	 */
	public void setCachePages(int cachePages) {
		this.m_cachePages = cachePages;
	}

	/**
	 * 返回是否显示视图条目数字。
	 * 
	 * <p>
	 * 默认为false表示视图数据列表呈现时不显示数字。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getNumberIndicator() {
		return this.m_numberIndicator;
	}

	/**
	 * 设置是否显示视图条目数字。
	 * 
	 * @param numberIndicator boolean
	 */
	public void setNumberIndicator(boolean numberIndicator) {
		this.m_numberIndicator = numberIndicator;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		View x = (View) super.clone();

		x.setApplicationOutline(this.getApplicationOutline());
		x.setCache(this.getCache());
		x.setDebugable(this.getDebugable());
		x.setPortalDatasource(this.getPortalDatasource());
		x.setShowSelector(this.getShowSelector());
		x.setDisplayCountPerPage(this.getDisplayCountPerPage());
		x.setCommonCondition(this.getCommonCondition());
		x.setDisplay(this.getDisplay());
		x.setRawQuery(this.getRawQuery());
		x.setTable(this.getTable());
		x.setViewParserImplement(this.getViewParserImplement());
		x.setViewQueryImplement(this.getViewQueryImplement());
		x.setMeasurement(this.getMeasurement());
		x.setViewType(this.getViewType());
		x.setNumberIndicator(this.getNumberIndicator());
		x.setCacheByUser(this.getCacheByUser());
		x.setCacheExpires(this.getCacheExpires());
		x.setCachePages(this.getCachePages());

		x.setColumns(Resource.cloneList(this.getColumns()));
		x.setConditions(Resource.cloneList(this.getConditions()));
		x.setOperations(Resource.cloneList(this.getOperations()));
		x.setOrders(Resource.cloneList(this.getOrders()));

		return x;
	}
}

