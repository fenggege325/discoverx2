/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 包含数据源的字段的字段值输入方式枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum ItemDataSourceInput implements EnumBase {
	/**
	 * 由系统自动设置。
	 */
	Default(0),
	/**
	 * 显示可供单击的热点以显示数据源输入框（1）。
	 * 
	 * <p>
	 * 可能是链接或按钮等形式。
	 * </p>
	 */
	SelectorHotspot(1),
	/**
	 * 字段获取焦点时自动显示数据源输入框（2）。
	 */
	SelectorAuto(2),
	/**
	 * 自动完成。
	 * 
	 * <p>
	 * 即根据用户输入自动过滤并下拉显示输入的建议项。
	 * </p>
	 */
	AutoComplete(3);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	ItemDataSourceInput(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return ItemDataSourceInput
	 */
	public ItemDataSourceInput parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (ItemDataSourceInput s : ItemDataSourceInput.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return ItemDataSourceInput.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return ItemDataSourceInput
	 */
	public static ItemDataSourceInput parse(int v) {
		for (ItemDataSourceInput s : ItemDataSourceInput.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

