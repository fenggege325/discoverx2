/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 用于描述显示于门户中的小窗口信息的门户小窗口(Portlet)资源类。
 * 
 * <p>
 * 本资源包含于门户({@link Portal})资源中。
 * </p>
 * 
 * <p>
 * 它的{@link Resource#getPUNID()}结果为所属门户资源的UNID。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Portlet extends Resource implements Cloneable {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -143235976470188325L;

	private int m_column = 0; // 小窗口所在页面列位置，必须。
	private int m_row = 0; // 小窗口所在页面行位置，必须，缺省为1
	private int m_displayCount = 10; // 小窗口最大可显示信息的条目数，缺省为10条
	private boolean m_showTitle = true; // 是否显示小窗口标题，缺省为true
	private int m_height = 120; // 小窗口高度，像素，默认为130，某些情况下可能被忽略。
	private int m_width = 0; // 小窗口在容器中的宽度，百分比，默认为100，某些情况下可能被忽略
	private String m_titleIcon = null; // 小窗口标题图标信息，可选
	private Scroll m_portletScroll = Scroll.NoScroll; // 小窗口小窗口内容滚动方式，缺省为不滚动。
	private Reference m_dataSource = null; // 小窗口数据源，必须。

	/**
	 * 缺省构造器。
	 */
	public Portlet() {
	}

	/**
	 * 获取小窗口所在页面列（栏目）位置。
	 * 
	 * <p>
	 * 默认为0。
	 * </p>
	 * 
	 * @return int
	 */
	public int getColumn() {
		return m_column;
	}

	/**
	 * 设置小窗口所在页面列（栏目）位置（）。
	 * 
	 * <p>
	 * 必须提供/配置；结果不能小于1或大于门户配置的最大支持的栏目数（{@link Portal#getColumnCount()}）。
	 * </p>
	 * 
	 * @param column int
	 */
	public void setColumn(int column) {
		this.m_column = column;
	}

	/**
	 * 获取小窗口所在页面行位置。
	 * 
	 * <p>
	 * 默认为0。
	 * </p>
	 * 
	 * @return int
	 */
	public int getRow() {
		return m_row;
	}

	/**
	 * 设置小窗口所在页面行位置。
	 * 
	 * <p>
	 * 必须提供/配置。
	 * </p>
	 * 
	 * @param row int
	 */
	public void setRow(int row) {
		this.m_row = row;
	}

	/**
	 * 获取小窗口最大可显示信息的条目数。
	 * 
	 * <p>
	 * 缺省为10条，0表示所有数据（请谨慎使用0，如果内容很多可能导致性能严重下降或界面效果被破坏）。<br/>
	 * 有些类型的数据源中可能被忽略或被重载。
	 * </p>
	 * 
	 * @return int
	 */
	public int getDisplayCount() {
		return m_displayCount;
	}

	/**
	 * 设置小窗口最大可显示信息的条目数。
	 * 
	 * @param displayCount int
	 */
	public void setDisplayCount(int displayCount) {
		this.m_displayCount = displayCount;
	}

	/**
	 * 获取是否显示小窗口标题。
	 * 
	 * <p>
	 * 缺省为true。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getShowTitle() {
		return m_showTitle;
	}

	/**
	 * 设置是否显示小窗口标题。
	 * 
	 * @param showTitle boolean
	 */
	public void setShowTitle(boolean showTitle) {
		this.m_showTitle = showTitle;
	}

	/**
	 * 获取小窗口内容高度。
	 * 
	 * <p>
	 * 单位为像素，默认为120，某些情况下此配置值可能被忽略或被重载。
	 * </p>
	 * 
	 * @return int
	 */
	public int getHeight() {
		return m_height;
	}

	/**
	 * 设置小窗口内容高度。
	 * 
	 * @param height int
	 */
	public void setHeight(int height) {
		this.m_height = height;
	}

	/**
	 * 获取小窗口在容器中的宽度。
	 * 
	 * <p>
	 * 单位为百分比，默认为0（表示自适应宽度），某些情况下此配置值可能被忽略或被重载。
	 * </p>
	 * 
	 * @return int
	 */
	public int getWidth() {
		return m_width;
	}

	/**
	 * 设置小窗口在容器中的宽度。
	 * 
	 * @param width int
	 */
	public void setWidth(int width) {
		this.m_width = width;
	}

	/**
	 * 获取小窗口标题图标路径信息。
	 * 
	 * @return String
	 */
	public String getTitleIcon() {
		return m_titleIcon;
	}

	/**
	 * 设置小窗口标题图标路径信息。
	 * 
	 * @param titleIcon String
	 */
	public void setTitleIcon(String titleIcon) {
		this.m_titleIcon = titleIcon;
	}

	/**
	 * 获取小窗口内容的滚动方式。
	 * 
	 * <p>
	 * 缺省为不滚动：“{@link Scroll#NoScroll}”。
	 * </p>
	 * 
	 * @return Scroll
	 */
	public Scroll getPortletScroll() {
		return m_portletScroll;
	}

	/**
	 * 设置小窗口内容的滚动方式。
	 * 
	 * @param portletScroll Scroll
	 */
	public void setPortletScroll(Scroll portletScroll) {
		this.m_portletScroll = portletScroll;
	}

	/**
	 * 返回小窗口数据源。
	 * 
	 * @return Reference
	 */
	public Reference getDataSource() {
		return this.m_dataSource;
	}

	/**
	 * 设置小窗口数据源。
	 * 
	 * @param dataSource Reference
	 */
	public void setDataSource(Reference dataSource) {
		this.m_dataSource = dataSource;
	}

	/**
	 * 重载getAlias：获取小窗口别名，如果没有设置有效别名，则返回：“ptt{@link #getColumn()}{@link #getRow()}”格式别名。
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#getAlias()
	 */
	@Override
	public String getAlias() {
		String alias = super.getAlias();
		if (alias == null || alias.length() == 0) {
			alias = ("ptt" + this.getColumn()) + this.getRow();
			this.setAlias(alias);
		}
		return alias;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Portlet x = (Portlet) super.clone();

		x.setShowTitle(this.getShowTitle());
		x.setColumn(this.getColumn());
		x.setDisplayCount(this.getDisplayCount());
		x.setHeight(this.getHeight());
		x.setRow(this.getRow());
		x.setWidth(this.getWidth());
		x.setTitleIcon(this.getTitleIcon());
		x.setPortletScroll(this.getPortletScroll());
		x.setDataSource(this.getDataSource());

		return x;
	}
}

