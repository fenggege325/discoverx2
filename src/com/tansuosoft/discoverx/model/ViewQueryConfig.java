/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.Instance;

/**
 * 表示描述系统所有视图查询配置项的基类。
 * 
 * <p>
 * 视图包含的列、条件、排序、分组等类（配置项）统称视图查询配置项。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class ViewQueryConfig implements Cloneable {
	/**
	 * 缺省构造器。
	 */
	public ViewQueryConfig() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param tableName
	 * @param columnName
	 */
	public ViewQueryConfig(String tableName, String columnName) {
		this.m_tableName = tableName;
		this.m_columnName = columnName;
	}

	private String m_tableName = null; // 表名，必须。
	private String m_columnName = null; // 字段名，必须。
	private String m_expression = null; // 表达式，可选。

	/**
	 * 返回表名，必须。
	 * 
	 * @return String 表名，必须。
	 */
	public String getTableName() {
		return this.m_tableName;
	}

	/**
	 * 设置表名，必须。
	 * 
	 * @param tableName String 表名，必须。
	 */
	public void setTableName(String tableName) {
		this.m_tableName = tableName;
	}

	/**
	 * 返回字段名，必须。
	 * 
	 * @return String 字段名，必须。
	 */
	public String getColumnName() {
		return this.m_columnName;
	}

	/**
	 * 设置字段名，必须。
	 * 
	 * @param columnName String 字段名，必须。
	 */
	public void setColumnName(String columnName) {
		this.m_columnName = columnName;
	}

	/**
	 * 返回SQL表达式，可选。
	 * 
	 * @see ViewQueryConfig#setExpression(String)
	 * @return String 表达式，可选。
	 */
	public String getExpression() {
		return this.m_expression;
	}

	/**
	 * 设置SQL表达式，可选。
	 * 
	 * <p>
	 * 表示可以调用的来自关系数据库系统的内置函数、自定义函数或其它有效表达式。<br/>
	 * 注意它和系统所基于的不同关系数据库平台有关，不同数据库系统可能有区别，不能通用！<br/>
	 * 表达式中如果要引“表名.字段名”（{@link TableName}.{@link ColumnName}），则以{0}作为“表名.字段名”的占位符。
	 * </p>
	 * <p>
	 * 配置举例：<br/>
	 * 比如在视图列配置中，表达式为“len({0})”，表名为“t_document”，字段名为“c_creator”，字段列别名为“creator”，则表示运行时，sql语句为“len(t_document.c_creator) as creator”。<br/>
	 * 同理，在条件配置中“len({0})&gt;10”在运行时为“len(t_document.c_creator)&gt;10”。
	 * </p>
	 * <p>
	 * 只对来自数据来数据库表记录的视图有效。
	 * </p>
	 * 
	 * @param expression String 表达式，可选。
	 */
	public void setExpression(String expression) {
		this.m_expression = expression;
	}

	/**
	 * 重载equals:比较toString的结果是否相等。
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		return this.toString().equals(obj.toString());
	}

	/**
	 * 重载hashCode
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * 重载toString。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.m_tableName);
		sb.append(".");
		sb.append(this.m_columnName);
		sb.append(" ").append(this.m_expression);
		return super.toString();
	}

	private int stringResultHashCode = -1;

	/**
	 * 获取{@link #toString()}返回的字符串对应的hashCode结果。
	 * 
	 * <p>
	 * 供系统内部使用。
	 * </p>
	 * 
	 * @return int
	 */
	public int getStringResultHashCode() {
		if (stringResultHashCode < 0) {
			stringResultHashCode = this.toString().hashCode();
		}
		return stringResultHashCode;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ViewQueryConfig x = Instance.newInstanceT(this.getClass());

		x.setColumnName(this.getColumnName());
		x.setExpression(this.getExpression());
		x.setTableName(this.getTableName());

		return x;
	}
}

