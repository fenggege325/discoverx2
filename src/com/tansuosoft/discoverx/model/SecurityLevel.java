/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 权限控制对应的操作级别枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum SecurityLevel implements EnumBase {
	/**
	 * 无任何权限（0）
	 */
	None(0),
	/**
	 * 查看权限（1）
	 */
	View(1),
	/**
	 * 文档作者权限（2）。
	 */
	Author(2),
	/**
	 * 增加权限（4）
	 */
	Add(4),
	/**
	 * 修改权限（8）
	 */
	Modify(8),
	/**
	 * 删除权限（16）
	 */
	Delete(16),
	/**
	 * 使用权限（32）
	 */
	Usage(32),
	/**
	 * 管理权限（64）
	 * 
	 * <p>
	 * 由系统内部使用。
	 * </p>
	 */
	Admin(64),
	/**
	 * 所有内级别总和。
	 */
	AllBuiltinLevel(127);
	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	SecurityLevel(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举值对应的数字返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return SecurityLevel
	 */
	public static SecurityLevel parse(int v) {
		for (SecurityLevel s : SecurityLevel.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字格式字符串返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return SecurityLevel
	 */
	public SecurityLevel parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (SecurityLevel s : SecurityLevel.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return SecurityLevel.valueOf(v);
		}
		return null;
	}

}

