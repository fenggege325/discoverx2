/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;


/**
 * 用于描述系统角色信息的资源类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Role extends Resource implements Securer {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -2552037732832370662L;

	/**
	 * 系统内置超级管理员角色UNID。
	 */
	public static final String ROLE_ADMIN = "091D0B8430C347AB94F77F52349BFFEF";
	/**
	 * 系统内置超级管理员角色安全编码。
	 */
	public static final int ROLE_ADMIN_SC = 3;
	/**
	 * 系统内置文档管理员角色UNID。
	 */
	public static final String ROLE_DOCUMENTADMIN = "0B2AAD5A061D4F4BB9D22D979F7FC1FA";
	/**
	 * 系统内置文档管理员角色安全编码。
	 */
	public static final int ROLE_DOCUMENTADMIN_SC = 4;
	/**
	 * 系统内置配置管理员角色UNID。
	 */
	public static final String ROLE_SETTINGSADMIN = "0A4ED9EA79EA4590A1B36F19BDAAA8AF";
	/**
	 * 系统内置配置管理员角色安全编码。
	 */
	public static final int ROLE_SETTINGSADMIN_SC = 5;
	/**
	 * 系统内置所有合法登录用户所具有的默认角色UNID。
	 */
	public static final String ROLE_DEFAULT = "0614F44BAB7241D1B5EDD558A41003F9";
	/**
	 * 系统内置所有合法登录用户所具有的默认角色安全编码。
	 */
	public static final int ROLE_DEFAULT_SC = 6;

	/**
	 * 系统内置所有匿名用户所具有的默认角色UNID。
	 */
	public static final String ROLE_ANONYMOUS = "0A1A8066509B48F4B7CB370973B4471A";
	/**
	 * 系统内置所有匿名用户所具有的默认角色安全编码。
	 */
	public static final int ROLE_ANONYMOUS_SC = 7;

	//
	/**
	 * 缺省构造器
	 */
	public Role() {
		super();
	}

	private int m_securityCode = 0; // 角色的安全编码

	/**
	 * 重载getSecurityCode：获取角色安全编码。
	 * 
	 * @see com.tansuosoft.discoverx.model.Securer#getSecurityCode()
	 */
	public int getSecurityCode() {
		return this.m_securityCode;
	}

	/**
	 * 重载setSecurityCode：设置角色安全编码。
	 * 
	 * @see com.tansuosoft.discoverx.model.Securer#setSecurityCode(int)
	 */
	public void setSecurityCode(int code) {
		this.m_securityCode = code;
	}

	private int m_securityRange = 1; // 此安全条目的安全控制范围。

	/**
	 * 获取安全控制范围。
	 * 
	 * <p>
	 * 默认为{@link SecurityRange#Config}。<br/>
	 * 多个{@link SecurityRange}枚举值对应的数字值可以想加以进行组合，一般继承自资源的描述对象中。
	 * </p>
	 * 
	 * @return int
	 */
	public int getSecurityRange() {
		return this.m_securityRange;
	}

	/**
	 * 设置安全控制范围。
	 * 
	 * @param securityRange
	 */
	public void setSecurityRange(int securityRange) {
		this.m_securityRange = securityRange;
	}

	private static Role m_admin = null;

	/**
	 * 获取并返回内置超级管理员角色。
	 * 
	 * @return
	 */
	public static Role getAdmin() {
		if (m_admin == null) {
			m_admin = ResourceContext.getInstance().getResource(ROLE_ADMIN, Role.class);
		}
		return m_admin;
	}

	private static Role m_documentAdmin = null;

	/**
	 * 获取并返回内置文档管理员角色。
	 * 
	 * @return
	 */
	public static Role getDocumentAdmin() {
		if (m_documentAdmin == null) {
			m_documentAdmin = ResourceContext.getInstance().getResource(ROLE_DOCUMENTADMIN, Role.class);
		}
		return m_documentAdmin;
	}

	private static Role m_settingsAdmin = null;

	/**
	 * 获取并返回内置配置管理员角色。
	 * 
	 * @return
	 */
	public static Role getSettingsAdmin() {
		if (m_settingsAdmin == null) {
			m_settingsAdmin = ResourceContext.getInstance().getResource(ROLE_SETTINGSADMIN, Role.class);
		}
		return m_settingsAdmin;
	}

	private static Role m_default = null;

	/**
	 * 获取并返回登录用户默认角色。
	 * 
	 * @return
	 */
	public static Role getDefaultRole() {
		if (m_default == null) {
			m_default = ResourceContext.getInstance().getResource(ROLE_DEFAULT, Role.class);
		}
		return m_default;
	}

	private static Role m_anonymous = null;

	/**
	 * 获取并返回匿名用户默认角色。
	 * 
	 * @return
	 */
	public static Role getAnonymous() {
		if (m_anonymous == null) {
			m_anonymous = ResourceContext.getInstance().getResource(ROLE_ANONYMOUS, Role.class);
			if (m_anonymous == null) {
				m_anonymous = new Role();
				m_anonymous.setUNID(Role.ROLE_ANONYMOUS);
				m_anonymous.setSecurityCode(ROLE_ANONYMOUS_SC);
				m_anonymous.setName("匿名用户");
				m_anonymous.setDescription("匿名用户所具有的角色。");
				m_anonymous.setSource(Source.System);
				m_anonymous.setSort(5);
			}
		}
		return m_anonymous;
	}

}

