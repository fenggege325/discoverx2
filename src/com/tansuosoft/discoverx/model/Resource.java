/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.UNIDProvider;
import com.tansuosoft.discoverx.util.serialization.SerializationFilter;
import com.tansuosoft.discoverx.util.serialization.SerializationFilterable;
import com.tansuosoft.discoverx.util.serialization.SerializationType;

/**
 * 系统所有资源的统一基类。
 * 
 * <p>
 * 资源是指描述系统业务或提供系统功能的模型对应的数据结构，一般从以xml文件或者数据库表记录的方式持久化保存。 系统运行时可以反序列化为对应的Resource实现对象。
 * </p>
 * <p>
 * 比如对于一份<strong>文档</strong>，他可能属于不同的类别（模块），绑定不同<strong>流程</strong>，包含不同的<strong>字段</strong>、<strong>附件</strong>等，因此文档、模块、字段、附件、流程等都抽象为分属某种类别的资源。
 * </p>
 * <p>
 * 系统所有资源的相关配置信息应在资源描述中注册。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class Resource implements java.io.Serializable, SerializationFilterable {
	/**
	 * 序列化版本uid
	 */
	private static final long serialVersionUID = 1L;

	private List<Parameter> parameters = null;
	private java.util.List<Resource> children = null;
	private java.util.List<Accessory> accessories = null;
	private String name = null;
	private String alias = null;
	private String UNID = null;
	private String PUNID = null; // 所属父资源UNID。
	private String category = null;
	private String directory = null;
	private String creator = null;
	private String created = null;
	private String modifier = null;
	private String modified = null;
	private String description = null;
	private Security security = null;
	private int sort = 0;
	private Source source = Source.UserDefine;
	private boolean selectable = true;
	private boolean m_common = true;
	private String[] m_categories = null;

	/**
	 * 缺省构造器。
	 */
	public Resource() {
		this.UNID = UNIDProvider.getUNID();
		this.created = new DateTime().toString();
		this.modified = this.created;
	}

	/**
	 * 返回资源目录名称。
	 * 
	 * <p>
	 * 同一类型的资源其资源目录必须相同且必须由英文字母组成。<br/>
	 * </p>
	 * 
	 * @return String
	 */
	public String getDirectory() {
		if (this.directory == null || this.directory.length() == 0) {
			Class<?> cls = this.getClass();
			this.directory = ResourceDescriptorConfig.getInstance().getResourceDirectory(cls);
			if (this.directory == null || this.directory.length() == 0) this.directory = StringUtil.toCamelCase(this.getClass().getSimpleName());
		}
		return this.directory;
	}

	/**
	 * 返回资源名称。
	 * 
	 * <p>
	 * 此属性针对不用类别资源有不同意义，使用时请参考具体资源类的说明。
	 * </p>
	 * 
	 * @return String
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * 设置资源名称，必须。
	 * 
	 * <p>
	 * 资源名称在不同类别的资源中可能有不同的意义，设置时请参考具体资源类的说明。
	 * </p>
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 返回资源别名。
	 * 
	 * @return String
	 */
	public String getAlias() {
		return this.alias;
	}

	/**
	 * 设置资源别名，必须。
	 * 
	 * <p>
	 * 资源别名必须全局唯一（特殊说明的除外），别名必须以英文字母开头且只能由英文字母和数字组成，总长度不超过24个字符。
	 * </p>
	 * <p>
	 * 系统内置的部分资源别名命名规范要求为：固定的别名前缀加别名名称。
	 * </p>
	 * <p>
	 * 以下为常见资源别名命名规范说明：
	 * <ul>
	 * <li>用户:没具体要求，用户别名可能不同单位会有不同意义，比如工号等</li>
	 * <li>群组：别名前缀:grp，比如grpLeader</li>
	 * <li>角色：别名前缀:role，比如roleAdmin</li>
	 * <li>组织机构：别名前缀:org，比如orgMyCompany，如果用户有特殊要求，别名可以作为用户单位或部门的编码按照用户要求的格式保存</li>
	 * <li>应用模块：别名前缀:app，比如appReceive</li>
	 * <li>流程：别名前缀:wf，比如wfReceive</li>
	 * <li>流程环节：别名前缀:wn，比如wn1、wn2，所属不同流程的环节，别名可以相同。</li>
	 * <li>表单：别名前缀:frm，比如frmReceive（建议别名非前缀部分和使用这个表单的应用模块相同）</li>
	 * <li>字段：别名前缀:fld_，比如fld_subject，所属不同表单的字段，别名可以相同。</li>
	 * <li>视图：别名前缀:view，比如viewToReceive</li>
	 * <li>门户：别名前缀:ptl，比如ptlDefault</li>
	 * <li>门户小窗口：别名前缀:ptt，比如ptt1、ptt2</li>
	 * <li>门户小窗口数据源资源类：别名前缀:pds，比如pdsReceive</li>
	 * <li>门户菜单大纲：别名前缀:menu，比如menuDefault</li>
	 * <li>附加文件类型：别名前缀:act，比如actAttachment</li>
	 * <li>字典：别名前缀:dict，比如dictDocType</li>
	 * <li>意见类型：别名前缀:opt，比如optOpinion</li>
	 * <li>意见输入助手：别名前缀:opa，比如opaOpinion</li>
	 * <li>功能扩展函数：建议与其对应的实现类名称一致，如果实现类重名，那么要提供一个唯一英文名。</li>
	 * </ul>
	 * <p>
	 * 别名命名注意事项：
	 * </p>
	 * <ul>
	 * <li>除非特殊说明，别名必须在同一类资源中唯一且建议用英文字母开头，包含英文字母、数字、下划线等；</li>
	 * <li>除非特殊说明，请按下面有提供前缀的资源别名加上前缀；</li>
	 * <li>别名请用单个英文、英文简称或英文单词组合。</li>
	 * </ul>
	 * 
	 * @param alias
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * 返回资源的类别字符串。
	 * 
	 * <p>
	 * 多个用半角空格、半角逗号、半角分号等分隔，层次分类用半角反斜杆分隔。
	 * </p>
	 * 
	 * @return String
	 */
	public String getCategory() {
		return this.category;
	}

	/**
	 * 设置资源类别字符串。
	 * 
	 * <p>
	 * 多个用半角空格、半角逗号、半角分号等分隔，同一个分类中有分层次的使用半角反斜杆分隔各层次。
	 * </p>
	 * 
	 * @param category String
	 */
	public void setCategory(String category) {
		this.category = category;
		if (this.m_categories != null) this.m_categories = null;
	}

	/**
	 * 获取分类个数。
	 * 
	 * @return int
	 */
	public int getCategoryCount() {
		String[] strs = getCategories();
		if (strs == null) return 0;
		return strs.length;
	}

	/**
	 * 获取所有分类的字符串数组。
	 * 
	 * @return String[]
	 */
	public String[] getCategories() {
		if (this.category == null || this.category.length() == 0) return null;
		if (this.m_categories == null) {
			this.m_categories = StringUtil.splitString(this.category.replace(';', ',').replace(' ', ','), ',');
		}
		return this.m_categories;
	}

	/**
	 * 返回资源UNID（唯一32位标识）。
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.UNID;
	}

	/**
	 * 设置资源UNID（唯一32位标识）。
	 * 
	 * <p>
	 * 资源实例化时，会自动获取一个唯一unid。
	 * </p>
	 * 
	 * @param unid
	 */
	public void setUNID(String unid) {
		this.UNID = unid;
	}

	/**
	 * 返回资源描述。
	 * 
	 * <p>
	 * 此属性针对不用类别资源有不同意义，使用时要注意区分。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * 设置资源描述，可选。
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 返回资源创建者（作者）。
	 * 
	 * <p>
	 * 以用户层次名（全名）方式返回。<br/>
	 * </p>
	 * 
	 * @return String
	 */
	public String getCreator() {
		return this.creator;
	}

	/**
	 * 设置资源创建者（作者）。
	 * 
	 * <p>
	 * 此属性针对不同类别资源有不同意义，同时有些资源中为必须，另一些可能为可选，因此使用时要注意区分并查看具体资源类中有无特殊说明。<br/>
	 * 比如对于文档资源，其创建者就是文档的作者。
	 * </p>
	 * 
	 * @param creator String
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * 返回资源创建时间，可选。
	 * 
	 * <p>
	 * 返回格式为“yyyy-mm-dd hh:nn:ss”的日期时间文本。<br/>
	 * 此属性针对不同类别资源有不同意义，同时有些资源中为必须，另一些可能为可选，因此使用时要注意区分并查看具体资源类中有无特殊说明。<br/>
	 * </p>
	 * 
	 * @return String
	 */
	public String getCreated() {
		return this.created;
	}

	/**
	 * 设置资源创建时间。
	 * 
	 * <p>
	 * 默认为资源实例化时的日期时间。
	 * </p>
	 * 
	 * @param created
	 */
	public void setCreated(String created) {
		this.created = created;
	}

	/**
	 * 返回资源最近修改者。
	 * 
	 * <p>
	 * 以用户层次名（全名）方式返回。<br/>
	 * 此属性针对不同类别资源有不同意义，同时有些资源中为必须，另一些可能为可选，因此使用时要注意区分并查看具体资源类中有无特殊说明。<br/>
	 * 比如对于文档资源，其最近修改者就是最后一次保存文档的人。
	 * </p>
	 * 
	 * @return String
	 */
	public String getModifier() {
		return this.modifier;
	}

	/**
	 * 设置资源最近修改者。
	 * 
	 * @param modifier
	 */
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	/**
	 * 返回资源最近修改时间。
	 * 
	 * <p>
	 * 返回格式为“yyyy-mm-dd hh:nn:ss”的日期时间文本。<br/>
	 * 此属性针对不同类别资源有不同意义，同时有些资源中为必须，另一些可能为可选，因此使用时要注意区分并查看具体资源类中有无特殊说明。<br/>
	 * </p>
	 * 
	 * @return String
	 */
	public String getModified() {
		return this.modified;
	}

	/**
	 * 设置资源最近修改时间。
	 * 
	 * <p>
	 * 默认为资源实例化时的日期时间。
	 * </p>
	 * 
	 * @param modified
	 */
	public void setModified(String modified) {
		this.modified = modified;
	}

	/**
	 * 返回资源排序号。
	 * 
	 * @return int
	 */
	public int getSort() {
		return this.sort;
	}

	/**
	 * 设置资源排序号。
	 * 
	 * <p>
	 * 此属性针对不同类别资源有不同意义，同时有些资源中为必须，另一些可能为可选，因此使用时要注意区分并查看具体资源类中有无特殊说明。<br/>
	 * </p>
	 * 
	 * @param sort
	 */
	public void setSort(int sort) {
		this.sort = sort;
	}

	/**
	 * 返回资源来源类别。
	 * 
	 * <p>
	 * 资源来源类别包括：系统内置资源、用户自定义资源等，参考“{@link SerializationType}”。<br/>
	 * 它主要用于区分资源来自哪里。
	 * </p>
	 * 
	 * @return SerializationType
	 */
	public Source getSource() {
		return this.source;
	}

	/**
	 * 设置资源来源类别。
	 * 
	 * <p>
	 * 默认为用户自定义类别（{@link SerializationType#UserDefine}）
	 * </p>
	 * 
	 * @param source
	 */
	public void setSource(Source source) {
		this.source = source;
	}

	/**
	 * 返回资源包含的额外参数列表集合。
	 * 
	 * <p>
	 * 额外参数可以用来动态地为某个资源增加配置、属性等。
	 * </p>
	 * 
	 * @return List&lt;Parameter&gt;
	 */
	public List<Parameter> getParameters() {
		return this.parameters;
	}

	/**
	 * 设置资源包含的额外参数列表集合。
	 * 
	 * <p>
	 * 默认不包含任何额外参数。
	 * </p>
	 * 
	 * @param params
	 */
	public void setParameters(List<Parameter> params) {
		this.parameters = params;
	}

	/**
	 * 增加指定参数，如果同名参数已经存在，则替换其值，如果参数值为null，则删除之。
	 * 
	 * @param name String，参数名，必须。
	 * @param value String，参数值，可选。
	 */
	public void setParameter(String name, String value) {
		if (name != null && name.length() > 0) {
			Parameter p = new Parameter(this.getUNID(), name, value);
			this.setParameter(p);
		}
	}

	/**
	 * 增加指定参数，如果参数名重复，则替换其值，如果参数值为null，则删除之。
	 * 
	 * @param param Parameter
	 */
	public void setParameter(Parameter param) {
		if (param == null || param.getName() == null || param.getName().length() == 0) return;
		String v = param.getValue();
		if (v == null) {
			this.removeParameter(param);
			return;
		}
		if (this.parameters == null || this.parameters.isEmpty()) {
			List<Parameter> list = new ArrayList<Parameter>();
			list.add(param);
			this.setParameters(list);
			return;
		} else {
			int idx = this.parameters.indexOf(param);
			if (idx >= 0) {
				this.parameters.set(idx, param);
			} else {
				this.parameters.add(param);
			}
		}
	}

	/**
	 * 删除指定参数。
	 * 
	 * @param param
	 */
	public void removeParameter(Parameter param) {
		if (param == null || param.getName() == null || param.getName().length() == 0) return;
		if (this.parameters == null) return;
		this.parameters.remove(param);
	}

	/**
	 * 根据参数名获取参数对象。
	 * 
	 * @param paramName
	 * @return Parameter<String, Object>
	 */
	public Parameter getParameter(String paramName) {
		if (paramName == null || parameters == null || parameters.isEmpty()) return null;
		for (Parameter p : parameters) {
			if (p != null && paramName.equalsIgnoreCase(p.getName())) return p;
		}
		return null;
	}

	/**
	 * 根据参数名获取对应参数中指定的参数值类型的结果。
	 * 
	 * @param paramName
	 * @return Object
	 */
	public Object getParameterTypeValue(String paramName) {
		Parameter p = this.getParameter(paramName);
		return (p == null ? null : p.getTypeValue());
	}

	/**
	 * 返回参数名对应参数值作为完整类名并调用完整类名对应的类的缺省公共构造器，然后构造好的对象。
	 * 
	 * @param &lt;T&gt; 返回的对象的目标泛型。
	 * @param paramName 参数名，其参数值必须是所定义的完整类名。
	 * @param clazz 指定返回的对象的目标类型。
	 * @param defaultReturn 如果类无效，或者无法构造，或者类型不符，则返回此结果。
	 * @return 返回构造好的对象。
	 */
	public <T> T getParamValueObject(String paramName, Class<T> clazz, T defaultReturn) {
		String clsName = this.getParamValueString(paramName, null);
		if (clsName == null || clsName.length() == 0) return defaultReturn;
		T ret = Instance.newInstance(clsName, clazz);
		if (ret == null) return defaultReturn;
		return ret;
	}

	/**
	 * 返回参数名对应参数值的String类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果参数值为null时返回的默认结果。
	 * @return String
	 */
	public String getParamValueString(String paramName, String defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueString(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的int类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return int
	 */
	public int getParamValueInt(String paramName, int defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueInt(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的long类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return long
	 */
	public long getParamValueLong(String paramName, long defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueLong(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的float类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return float
	 */
	public float getParamValueFloat(String paramName, float defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueFloat(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的double类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return double
	 */
	public double getParamValueDouble(String paramName, double defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueDouble(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的boolean类型结果。
	 * 
	 * <p>
	 * 参数值（不区分大小写）配置为“true”、“yes”、“y”、“1”等情况时返回true，否则返回false。
	 * </p>
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return boolean
	 */
	public boolean getParamValueBool(String paramName, boolean defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueBool(defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值转换为enumCls定义的枚举中某一个具体枚举值类型之后的结果。
	 * 
	 * @param &lt;T&gt; 返回的枚举类型。
	 * @param paramName String 参数名，必须。
	 * @param enumCls Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return T 具体枚举结果。
	 */
	public <T> T getParamValueEnum(String paramName, Class<T> enumCls, T defaultReturn) {
		Parameter param = this.getParameter(paramName);
		if (param == null) return defaultReturn;
		return param.getValueEnum(enumCls, defaultReturn);
	}

	/**
	 * 返回资源安全控制信息。
	 * 
	 * @return Security
	 */
	public Security getSecurity() {
		if (this.security == null) this.setSecurity(new Security(this.getUNID()));
		return this.security;
	}

	/**
	 * 设置资源安全控制信息。
	 * 
	 * <p>
	 * 此属性用于控制本资源的访问授权。<br/>
	 * 如果不提供（不配置），则系统会有默认的授权机制。<br/>
	 * 信息请参考{@link Security}中的说明。
	 * </p>
	 * 
	 * @param security
	 */
	public void setSecurity(Security security) {
		this.security = security;
		if (this.security != null && (this.security.getPUNID() == null || this.security.getPUNID().length() == 0)) {
			this.security.setPUNID(this.getUNID());
		}
	}

	private boolean m_childrenSorted = false;

	/**
	 * 返回资源包含的下级资源列表。
	 * 
	 * <p>
	 * 如果只能保存同一类资源，需在不同类别资源类中进行说明。<br/>
	 * 除非明确资源实例的类型，否则在递归/迭代调用时请按照普通资源({@link Resource})类别处理下级资源。
	 * </p>
	 * <p>
	 * 返回结果按照{@link ResourceComparatorDefault}中的排序规则进行排序。
	 * </p>
	 * 
	 * @return java.util.List&lt;Resource&gt;
	 */
	public java.util.List<Resource> getChildren() {
		if (!m_childrenSorted) {
			m_childrenSorted = true;
			if (this.children != null && this.children.size() > 0) {
				ResourceComparatorDefault c = new ResourceComparatorDefault();
				Collections.sort(this.children, c);
			}
		}
		return this.children;
	}

	/**
	 * 设置资源包含的下级资源列表。
	 * 
	 * <p>
	 * 资源可以按照树型结构方式存储用以包含下级资源，树可以支持一级或多级分支。
	 * </p>
	 * 
	 * @param resources java.util.List&lt;Resource&gt;
	 */
	public void setChildren(java.util.List<Resource> children) {
		this.children = children;
		if (this.children != null) {
			for (Resource r : this.children) {
				if (r == null) continue;
				r.setPUNID(this.UNID);
			}
		}
		if (m_childrenSorted) m_childrenSorted = false;
	}

	/**
	 * 追加一个所属下级资源。
	 * 
	 * @param child
	 * @return
	 */
	public boolean addChild(Resource child) {
		if (child == null) return false;
		if (this.children == null) this.children = new ArrayList<Resource>();
		child.setPUNID(this.getUNID());
		if (m_childrenSorted) m_childrenSorted = false;
		return this.children.add(child);
	}

	/**
	 * 获取资源是否包含下级资源。
	 * 
	 * @return boolean
	 */
	public boolean hasChildren() {
		return (this.children != null && !this.children.isEmpty());
	}

	/**
	 * 获取资源树的下级资源的最大深度。
	 * 
	 * @return int
	 */
	public int getTreeLevel() {
		return ResourceTreeHelper.getMaxLevel(this, 0);
	}

	/**
	 * 获取资源树包含的下级资源对象。
	 * 
	 * <p>
	 * 下级资源的UNID必需与参数指定的UNID一致，如果找不到一致或者不包含下级资源，则返回null
	 * </p>
	 * 
	 * @param unid
	 * @return Resource
	 */
	public Resource getChild(String unid) {
		return ResourceTreeHelper.getChildByUNID(this, unid);
	}

	/**
	 * 获取资源作为文档字段绑定的数据源时是否可选择。
	 * 
	 * @return boolean
	 */
	public boolean getSelectable() {
		return this.selectable;
	}

	/**
	 * 设置资源作为文档字段绑定的数据源时是否可选择。
	 * 
	 * <p>
	 * 默认为true。
	 * </p>
	 * 
	 * @param selectable
	 */
	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}

	/**
	 * 返回资源包含的附加文件资源列表集合。
	 * 
	 * <p>
	 * 如果此类资源的描述信息中配置了资源不能包含附加文件（即{@link ResourceDescriptor#getAccessoryContainer()}返回false），则此属性被忽略。
	 * </p>
	 * 
	 * @return java.util.List&lt;Accessory&gt;
	 */
	public java.util.List<Accessory> getAccessories() {
		return this.accessories;
	}

	/**
	 * 设置资源包含的附加文件资源列表集合。
	 * 
	 * @param accessories java.util.List&lt;Accessory&gt;
	 */
	public void setAccessories(java.util.List<Accessory> accessories) {
		this.accessories = accessories;
	}

	/**
	 * 获取资源所属父（直接上级）资源的UNID。
	 * 
	 * @return String
	 */
	public String getPUNID() {
		return this.PUNID;
	}

	protected String m_path = null; // 完整路径

	/**
	 * 设置完整路径名。
	 * 
	 * <p>
	 * 一般只针对顶层资源调用一次即可。
	 * </p>
	 * 
	 * @param parentPath 上级路径，调用时通常传入空字符串。
	 * @param pathSeparator 路径分隔符，默认为“\”。
	 */
	public void setPath(String parentPath, String pathSeparator) {
		List<Resource> list = this.getChildren();
		boolean hasValidParentPath = (parentPath != null && parentPath.trim().length() > 0);
		this.m_path = (hasValidParentPath ? parentPath : "") + (hasValidParentPath ? (pathSeparator == null ? "\\" : pathSeparator) : "") + name;
		if (list != null && list.size() > 0) {
			for (Resource r : list) {
				if (r == null) continue;
				r.setPath(m_path, pathSeparator);
			}
		}
	}

	/**
	 * 获取树形资源的完整路径名。
	 * 
	 * <p>
	 * 格式为：根名称\1级名称\2级名称...\本身名称
	 * </p>
	 */
	public String getPath() {
		return (m_path == null ? name : m_path);
	}

	/**
	 * 设置资源所属父（直接上级）资源的UNID。
	 * 
	 * <p>
	 * 此属性针对不同类别资源有不同意义，同时有些资源中为必须，另一些可能为可选，因此使用时要注意区分并查看具体资源类中有无特殊说明。<br/>
	 * </p>
	 * 
	 * @param punid String
	 */
	public void setPUNID(String punid) {
		this.PUNID = punid;
	}

	/**
	 * 重载getSerializationFilter：从资源描述{@link ResourceDescriptor}中获取配置有效的{@link SerializationFilter}对象。
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.SerializationFilterable#getSerializationFilter(com.tansuosoft.discoverx.util.serialization.SerializationType)
	 */
	@Override
	public SerializationFilter getSerializationFilter(SerializationType serializationType) {
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(this.getClass());
		if (serializationType == null || rd == null) return null;
		String impl = null;
		switch (serializationType.getIntValue()) {
		case 1: // xml
			impl = rd.getXmlSerializerFilter();
			break;
		case 2: // json
			impl = rd.getJsonSerializerFilter();
			break;
		default:
			return null;
		}
		if (impl != null && impl.length() > 0) return Instance.newInstance(impl, SerializationFilter.class);
		return null;
	}

	/**
	 * 克隆列表中的对象并返回包含克隆对象的列表。
	 * 
	 * <p>
	 * 列表中包含的对象必须实现了{@link java.lang.Cloneable}接口并提供了重载后的{@link Object#clone()}公共方法。
	 * </p>
	 * 
	 * @param <T>
	 * @param list
	 * @return
	 */
	protected static <T> List<T> cloneList(List<T> list) {
		return ObjectUtil.cloneList(list);
	}

	/**
	 * 重载clone：返回一个克隆的新资源（此方法返回的克隆资源只包括所有资源的通用属性）。
	 * 
	 * <p>
	 * 包含的下级资源如果没有实现{@link java.lang.Cloneable}接口，则会被忽略。
	 * </p>
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Resource r = null;
		if (this instanceof Document) {
			r = Document.newBlankDocument();
		} else {
			r = Instance.newInstanceT(this.getClass());
		}
		r.setAlias(alias);
		r.setCategory(category);
		r.setCreated(created);
		r.setCreator(creator);
		r.setDescription(description);
		r.setModified(modified);
		r.setModifier(modifier);
		r.setName(name);
		r.setPUNID(this.PUNID);
		Security s = this.getSecurity();
		Security snew = (Security) (s == null ? null : s.clone());
		r.setSecurity(snew);
		r.setSelectable(selectable);
		r.setSort(sort);
		r.setSource(source);
		r.setUNID(UNID);

		List<Accessory> accs = this.getAccessories();
		if (accs != null) {
			List<Accessory> newaccs = new ArrayList<Accessory>(accs.size());
			for (Accessory x : accs) {
				if (x == null) continue;
				newaccs.add((Accessory) x.clone());
			}
			r.setAccessories(newaccs);
		}

		List<Parameter> params = this.getParameters();
		if (params != null) {
			List<Parameter> newparams = new ArrayList<Parameter>(params.size());
			for (Parameter x : params) {
				if (x == null) continue;
				newparams.add((Parameter) x.clone());
			}
			r.setParameters(newparams);
		}

		List<Resource> reses = this.getChildren();
		if (reses != null) {
			List<Resource> newreses = new ArrayList<Resource>(reses.size());
			for (Resource x : reses) {
				if (x == null || !(x instanceof Cloneable)) continue;
				newreses.add((Resource) (r.clone()));
			}
			r.setChildren(newreses);
		}

		return r;
	}

	/**
	 * 是否多租户使用环境下的通用资源。
	 * 
	 * <p>
	 * 默认为true，如果为false，那么表示这个是某个租户的特定资源。<br/>
	 * 单独使用版总是返回true。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean isCommon() {
		if (OrganizationsContext.getInstance().isMultipleOrgs()) {
			ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(this.getClass());
			if (rd == null || rd.getXmlStore()) return m_common;
			if (this instanceof Document) return false;
			else if (this instanceof User || this instanceof Organization) return false;
			else if (this instanceof Role || this instanceof Group) return StringUtil.isBlank(PUNID);
			return m_common;
		}
		return m_common;
	}

	/**
	 * 设置是否是否多租户使用环境下的通用资源。
	 * 
	 * @param common
	 */
	protected void setCommon(boolean common) {
		m_common = common;
	}

	/**
	 * 重载equals：unid相同则返回true，否则返回false。
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Resource)) return false;
		Resource r = (Resource) obj;
		return this.getUNID().equalsIgnoreCase(r.getUNID());
	}

	/**
	 * 重载hashCode
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * 重载toString：返回格式为“{directory}://{unid}”。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("%1$s://%2$s", this.getDirectory(), this.getUNID());
	}

}

