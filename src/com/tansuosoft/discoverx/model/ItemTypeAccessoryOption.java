/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.List;

/**
 * 字段值数据类型为附加文件类型的额外选项类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemTypeAccessoryOption extends ItemTypeOption {
	/**
	 * 缺省构造器。
	 */
	public ItemTypeAccessoryOption() {
	}

	private List<Reference> m_accessoryTypes = null; // 可以包含的附加文件类型。

	/**
	 * 返回可以包含的附加文件类型。
	 * 
	 * @return List<Reference>
	 */
	public List<Reference> getAccessoryTypes() {
		return this.m_accessoryTypes;
	}

	/**
	 * 设置可以包含的附加文件类型。
	 * 
	 * @param accessoryTypes List<Reference>
	 */
	public void setAccessoryTypes(List<Reference> accessoryTypes) {
		this.m_accessoryTypes = accessoryTypes;
	}
}

