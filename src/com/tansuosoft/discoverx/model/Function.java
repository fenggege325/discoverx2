/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.event.EventHandlerInfo;

/**
 * 用于描述可以执行某些功能并返回指定类型的执行结果对象的系统功能函数对应的的配置资源类。
 * 
 * <p>
 * 简称系统功能<strong>函数配置资源</strong>或系统功能<strong>函数定义</strong>。
 * </p>
 * <p>
 * 系统使用统一的类似函数调用的方式来为系统提供常见的功能并使用同样的方式来增加、扩展系统功能。<br/>
 * 系统功能函数的具体实现总体上可以分为两大类。<br/>
 * <strong>公式</strong>：返回一些常数值（如文本、布尔、数字等）的功能扩展函数实现。<br/>
 * <strong>操作</strong>：执行某些（一般是http请求处理）功能，并统一返回OperationResult的功能扩展函数实现。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Function extends Resource implements Cloneable {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 6190716214843830236L;

	/**
	 * 缺省构造器
	 */
	public Function() {
		super();
		this.setCategory("文档操作");
		this.setResult(OperationResult.class.getName());
	}

	private String m_implement = null; // 实现具体功能的函数类的全限定类名。
	private String m_result = null; // 实现具体功能的函数类返回的结果类名。

	/**
	 * 返回实现具体功能的函数类的全限定类名。
	 * 
	 * @return String
	 */
	public String getImplement() {
		return m_implement;
	}

	/**
	 * 设置实现具体功能的函数类的全限定类名。
	 * 
	 * @param implement
	 */
	public void setImplement(String implement) {
		this.m_implement = implement;
	}

	/**
	 * 返回实现具体功能的函数类返回的结果类名。
	 * 
	 * @return String
	 */
	public String getResult() {
		return m_result;
	}

	/**
	 * 设置实现具体功能的函数类返回的结果类名。
	 * 
	 * <p>
	 * 比如返回StringBuilder，则为”java.lang.StringBuilder”，如果是标量值， 如执行结果返回整数，则为“int”、“long”等。
	 * </p>
	 * 
	 * @param result
	 */
	public void setResult(String result) {
		this.m_result = result;
	}

	private List<EventHandlerInfo> m_eventHandlers = null; // 包含的操作执行前后触发的事件处理程序信息，可选。

	/**
	 * 返回包含的操作执行前后触发的事件处理程序信息，可选。
	 * 
	 * @return List<EventHandlerInfo> 包含的操作执行前后触发的事件处理程序信息，可选。
	 */
	public List<EventHandlerInfo> getEventHandlers() {
		return this.m_eventHandlers;
	}

	/**
	 * 设置包含的操作执行前后触发的事件处理程序信息，可选。
	 * 
	 * @param eventHandlers List<EventHandlerInfo> 包含的操作执行前后触发的事件处理程序信息，可选。
	 */
	public void setEventHandlers(List<EventHandlerInfo> eventHandlers) {
		this.m_eventHandlers = eventHandlers;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Function x = (Function) super.clone();

		x.setImplement(this.getImplement());
		x.setResult(this.getResult());
		x.setEventHandlers(this.getEventHandlers());

		List<EventHandlerInfo> ehilist = this.getEventHandlers();

		if (ehilist != null) {
			List<EventHandlerInfo> ehilistNew = new ArrayList<EventHandlerInfo>(ehilist.size());
			for (EventHandlerInfo y : ehilist) {
				if (y == null) continue;
				ehilistNew.add((EventHandlerInfo) y.clone());
			}
			x.setEventHandlers(ehilistNew);
		}

		return x;
	}
}

