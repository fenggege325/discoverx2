/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 字段显示方式枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum ItemDisplay implements EnumBase {
	/**
	 * 单行文本框(1)。
	 */
	SingleLineText(1),
	/**
	 * 多行文本框(2)。
	 */
	MultipleLineText(2),
	/**
	 * 下拉组合框(3)。
	 */
	SingleCombo(3),
	/**
	 * 多选组合框(4)。
	 */
	MultipleCombo(4),
	/**
	 * 多选框(5)。
	 */
	CheckBox(5),
	/**
	 * 单选框(6)。
	 */
	Radio(6),
	/**
	 * 图片默认控件(7)。
	 */
	Image(7),
	/**
	 * 链接默认控件(8)。
	 */
	Link(8),
	/**
	 * 富文本编辑器默认控件(9)。
	 */
	RTF(9),
	/**
	 * 附加文件默认控件(10)。
	 */
	Accessory(10),
	/**
	 * 字段分组默认控件(11)。
	 */
	Group(11),
	/**
	 * 默认，表示由系统自动根据字段配置进行显示(-1)。
	 */
	Default(-1),
	/**
	 * 自定义显示方式（0）。
	 */
	Custom(0);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	ItemDisplay(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return ItemDisplay
	 */
	public ItemDisplay parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (ItemDisplay s : ItemDisplay.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return ItemDisplay.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return ItemDisplay
	 */
	public static ItemDisplay parse(int v) {
		for (ItemDisplay s : ItemDisplay.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

