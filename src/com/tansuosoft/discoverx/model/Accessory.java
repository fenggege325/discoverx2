/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.common.MIMEContentTypeConfig;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 附加文件资源类。
 * <p>
 * 对于文档等类别的资源，可能会包含一些额外的文件，比如正文、附件或其它类型的二进制文件等，此资源用于描述这些文件。
 * <p>
 * <p>
 * 附加资源的PUNID属性为此附加文件资源所属资源的UNID，是必须属性。
 * </p>
 * <p>
 * 属于同一个资源的附加文件资源的名称（{@link Resource#getName()}）不能重复！
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Accessory extends Resource implements Cloneable {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -5076621229106073723L;

	/**
	 * 缺省构造器。
	 */
	public Accessory() {
	}

	/**
	 * 接收所属资源UNID的构造器。
	 * 
	 * <p>
	 * 比如一个正文属于某个文档，那么这个属性保存文档的UNID。
	 * </p>
	 * 
	 * @param parentUNID
	 */
	public Accessory(String parentUNID) {
		this.setPUNID(parentUNID);
	}

	/**
	 * 接收所属资源UNID和所属资源目录名的构造器。
	 * 
	 * @param getBelongTo
	 */
	public Accessory(String parentUNID, String parentDirectory) {
		this.setPUNID(parentUNID);
		this.m_parentDirectory = parentDirectory;
	}

	private String m_parentDirectory = "document"; // 附加文件所属资源的目录名，可选，默认为文档资源目录名。
	private String m_styleTemplate = null; // 附加文件基于的模板附加文件资源对应的UNID，可选。
	private String m_codeTemplate = null; // 代码模板，可选。
	private int m_size = 0; // 附加文件大小，以字节计，可选。
	private String m_state = null; // 附件添加时的流程状态（格式：流程UNID\环节UNID），可选。
	private String m_fileName = null; // 附加原始文件文件名（不带路径）。

	/**
	 * 返回附加文件大小，以千字节(KB)计。
	 * 
	 * @return int 附加文件大小
	 */
	public int getSize() {
		return this.m_size;
	}

	/**
	 * 设置附加文件大小，以千字节(KB)计，可选。
	 * 
	 * @param size int 附加文件大小
	 */
	public void setSize(int size) {
		this.m_size = size;
	}

	/**
	 * 返回附加文件所属资源的目录名，可选，默认为文档资源目录名。
	 * 
	 * @return String 附加文件所属资源的目录名，可选，默认为文档资源目录名。
	 */
	public String getParentDirectory() {
		return this.m_parentDirectory;
	}

	/**
	 * 设置附加文件所属资源的目录名，可选，默认为文档资源目录名。
	 * 
	 * @param parentDirectory 附加文件所属资源的目录名，可选，默认为文档资源目录名。
	 */
	public void setParentDirectory(String parentDirectory) {
		this.m_parentDirectory = parentDirectory;
	}

	/**
	 * 返回附加文件基于的样式模板附加文件资源对应的UNID，可选。
	 * 
	 * <p>
	 * 比如一个正文来源于某份模板，那么此UNID保存此模板附加文件资源的UNID。
	 * </p>
	 * 
	 * @return String
	 */
	public String getStyleTemplate() {
		return this.m_styleTemplate;
	}

	/**
	 * 设置附加文件基于的样式模板附加文件资源对应的UNID
	 * 
	 * @param styleTemplate String
	 */
	public void setStyleTemplate(String styleTemplate) {
		this.m_styleTemplate = styleTemplate;
	}

	/**
	 * 返回附加文件基于的代码模板附加文件资源对应的UNID。
	 * 
	 * <p>
	 * 在集成word、wps进行正文、办理单处理时，需要使用到代码模板。
	 * </p>
	 * <p>
	 * 默认从所属直接模块中获取，逐级向上获取直到取到有效的代码模板信息，如果都取不到则从系统全局配置的模板中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getCodeTemplate() {
		return this.m_codeTemplate;
	}

	/**
	 * 设置附加文件基于的代码模板附加文件资源对应的UNID。
	 * 
	 * @param codeTemplate String
	 */
	public void setCodeTemplate(String codeTemplate) {
		this.m_codeTemplate = codeTemplate;
	}

	/**
	 * 返回附件添加时的流程状态（格式：流程UNID\环节UNID），可选。
	 * 
	 * @return String
	 */
	public String getState() {
		return this.m_state;
	}

	/**
	 * 设置附件添加时的流程状态（格式：流程UNID\环节UNID），可选。
	 * 
	 * @param state String
	 */
	public void setState(String state) {
		this.m_state = state;
	}

	private String m_accessoryType = null;

	/**
	 * 获取所属的附加文件类型资源的UNID。
	 * 
	 * <p>
	 * 参考{@link AccessoryType}
	 * </p>
	 * 
	 * @return Reference
	 */
	public String getAccessoryType() {
		return this.m_accessoryType;
	}

	/**
	 * 设置所属的附加文件类型资源的UNID。
	 * 
	 * @see Accessory#getAccessoryType()
	 * @param accessoryType String
	 */
	public void setAccessoryType(String accessoryType) {
		this.m_accessoryType = accessoryType;
	}

	/**
	 * 获取所属附加文件类别资源。
	 * 
	 * @return String
	 */
	public AccessoryType getAccessoryTypeResource() {
		if (!StringUtil.isUNID(m_accessoryType)) return ResourceContext.getInstance().getResource("6A8ACF76DC9DD59DE30E5C91B7FC6F73", AccessoryType.class);
		return ResourceContext.getInstance().getResource(this.m_accessoryType, AccessoryType.class);
	}

	/**
	 * 获取所属附加文件类别名称。
	 * 
	 * @return String
	 */
	public String getAccessoryTypeName() {
		AccessoryType at = this.getAccessoryTypeResource();
		return (at == null ? null : at.getName());
	}

	/**
	 * 获取所属附加文件类别别名。
	 * 
	 * @return String 如果没有值则返回空字符串。
	 */
	public String getAccessoryTypeAlias() {
		AccessoryType at = this.getAccessoryTypeResource();
		return (at == null ? "" : StringUtil.getValueString(at.getAlias(), ""));
	}

	/**
	 * 重载getCategory，如果Category属性有有效值，则返回之，否则返回所属附加文件类别名称。
	 * 
	 * @see Accessory#getAccessoryTypeName()
	 * @see com.tansuosoft.discoverx.model.Resource#getCategory()
	 */
	public String getCategory() {
		String category = super.getCategory();
		if (category == null || category.length() == 0) return this.getAccessoryTypeName();
		else return category;
	}

	/**
	 * 返回附加原始文件文件名。
	 * 
	 * <p>
	 * 属于同一个资源的附加文件资源的文件名不能重复！
	 * </p>
	 * 
	 * @return String
	 */
	public String getFileName() {
		return this.m_fileName;
	}

	/**
	 * 设置附加原始文件文件名。
	 * 
	 * <p>
	 * 属于同一个资源的附加文件资源的文件名不能重复！
	 * </p>
	 * 
	 * @param fileName String
	 */
	public void setFileName(String fileName) {
		this.m_fileName = fileName;
	}

	/**
	 * 通过附加文件文件名判断并返回附加文件是否是常见的图片文件。
	 * 
	 * <p>
	 * 常见图片文件包括：“jpg、gif、png、bmp、tif”等。
	 * </p>
	 * 
	 * @return
	 */
	public boolean isImage() {
		return MIMEContentTypeConfig.isImage(m_fileName);
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Accessory a = (Accessory) super.clone();
		a.setAccessoryType(getAccessoryType());
		a.setFileName(getFileName());
		a.setParentDirectory(this.getParentDirectory());
		a.setStyleTemplate(this.getStyleTemplate());
		a.setCodeTemplate(this.getCodeTemplate());
		a.setSize(this.getSize());
		a.setState(this.getState());
		return a;
	}
}

