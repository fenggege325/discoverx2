/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.GenericTriplet;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 用于维护系统登录用户对应的用户自定义会话信息的类。
 * 
 * <p>
 * 用户自定义会话通常作为属性值保存于登录用户对应的{@link javax.servlet.http.HttpSession}http会话（区别于用户会话或用户自定义会话）对象中。<br/>
 * 通常一个登录用户对应一个自定义会话，如果用户用不同客户端多次登录，或登录后没有注销关掉浏览器又再次登录，则这些http会话会绑定同一个用户自定义会话实例。<br/>
 * 用户注销或http会话超时的时候，会减少绑定的用户会话的引用计数，如果引用计数小于等于0，则用户自定义会话被从缓存中删除。<br/>
 * 应优先使用用户会话存储运行时需要传递到前端的对象，如果用户没有登录则可以使用http会话传递对象。<br/>
 * 未登录系统会分配一个匿名用户对应的自定义会话供其使用，但是建议匿名用户使用http会话传递对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Session implements Serializable, HttpSessionActivationListener {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 8675169519510887407L;

	/**
	 * 用户自定义会话对象实例在httpsession中保存的参数名：$$user_session。
	 */
	public static final String USERSESSION_PARAM_NAME_IN_HTTPSESSION = "$$user_session";

	/**
	 * 最近一次http请求的执行导致的异常对象实例在httpsession中保存的参数名：$$last_exception。
	 * <p>
	 * 一般在用户自定义会话中设置，如果自定义会话不存在则在httpsession中保存。
	 * </p>
	 */
	public static final String LASTERROR_PARAM_NAME_IN_HTTPSESSION = "$$last_exception";

	/**
	 * 最近一次http请求执行结果对应的消息文本在httpsession中保存的参数名：$$last_message。
	 * <p>
	 * 一般在用户自定义会话中设置，如果自定义会话不存在则在httpsession中保存。
	 * </p>
	 */
	public static final String LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION = "$$last_message";

	/**
	 * 最近一次http请求执行结果对应的操作信息在httpsession中保存的参数名：$$last_operation。
	 * <p>
	 * 一般在用户自定义会话中设置，如果自定义会话不存在则在httpsession中保存。
	 * </p>
	 */
	public static final String LASTOPERATION_PARAM_NAME_IN_HTTPSESSION = "$$last_operation";

	/**
	 * 最近一次http请求执行结果对应的操作处理结果文本在httpsession中保存的参数名：$$last_operationresult。
	 * <p>
	 * 一般在用户自定义会话中设置，如果自定义会话不存在则在httpsession中保存。
	 * </p>
	 */
	public static final String LASTOPERATIONRESULT_PARAM_NAME_IN_HTTPSESSION = "$$last_operationresult";

	/**
	 * 最近一次http请求执行结果返回资源对象httpsession中保存的参数名：$$last_resource。
	 * <p>
	 * 一般在用户自定义会话中设置，如果自定义会话不存在则在httpsession中保存。
	 * </p>
	 */
	public static final String LASTRESOURCE_PARAM_NAME_IN_HTTPSESSION = "$$last_resource";

	/**
	 * 最近一次http请求执行结果返回文档资源对象httpsession中保存的参数名：$$last_document。
	 * <p>
	 * 一般在用户自定义会话中设置，如果自定义会话不存在则在httpsession中保存。
	 * </p>
	 */
	public static final String LASTDOCUMENT_PARAM_NAME_IN_HTTPSESSION = "$$last_document";

	/**
	 * 最近一次http请求执行后需重定向的URL地址在httpsession中保存的参数名：$$last_redirecturl。
	 * <p>
	 * 一般在用户自定义会话中设置如果自定义会话不存在则在httpsession中保存。
	 * </p>
	 */
	public static final String LASTREQUESTURL_PARAM_NAME_IN_HTTPSESSION = "$$last_redirecturl";

	/**
	 * 最近一次http请求执行后的{@link AJAXResponse}对象在httpsession中保存的参数名：$$last_ajaxresponse。。
	 * <p>
	 * 一般在用户自定义会话中设置如果自定义会话不存在则在httpsession中保存。
	 * </p>
	 */
	public static final String LASTAJAXRESPONSE_PARAM_NAME_IN_HTTPSESSION = "$$last_ajaxresponse";

	/**
	 * 多个登录地址和客户端的分隔字符。
	 */
	public static final char ADDRESS_AND_CLIENT_SEP = '|';
	/**
	 * 多个登录地址和客户端的分隔字符串。
	 */
	public static final String ADDRESS_AND_CLIENT_SEP_STR = "|";
	/**
	 * 单个客户端名称中的{@link #ADDRESS_AND_CLIENT_SEP}字符的替换符。
	 */
	public static final char ADDRESS_AND_CLIENT_SEP_ALT = '│';

	/**
	 * 设置指定参数名对应参数值到用户自定义会话（如果不为null的话优先）或http会话。
	 * 
	 * @param userSession 用户自定义会话对象，可为null。
	 * @param httpSession http会话对象，可为null。
	 * @param name String 参数名，必须。
	 * @param val Object 参数值，可以为null。
	 * 
	 */
	public static void setSessionParam(Session userSession, javax.servlet.http.HttpSession httpSession, String name, Object val) {
		if (name == null || name.length() == 0) return;
		if (userSession != null) {
			if (name.equalsIgnoreCase(LASTERROR_PARAM_NAME_IN_HTTPSESSION)) {
				userSession.setLastError((val instanceof Exception) ? (Exception) val : null);
			} else if (name.equalsIgnoreCase(LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION)) {
				userSession.setLastMessage(val == null ? null : val.toString());
			} else if (name.equalsIgnoreCase(LASTOPERATION_PARAM_NAME_IN_HTTPSESSION)) {
				userSession.setLastOperation(val == null ? null : val);
			} else if (name.equalsIgnoreCase(LASTOPERATIONRESULT_PARAM_NAME_IN_HTTPSESSION)) {
				userSession.setLastOperationResult((val instanceof OperationResult) ? (OperationResult) val : null);
			} else if (name.equalsIgnoreCase(LASTRESOURCE_PARAM_NAME_IN_HTTPSESSION)) {
				userSession.setLastResource((val instanceof Resource) ? (Resource) val : null);
			} else if (name.equalsIgnoreCase(LASTDOCUMENT_PARAM_NAME_IN_HTTPSESSION)) {
				userSession.setLastDocument((val instanceof Document) ? (Document) val : null);
			} else if (name.equalsIgnoreCase(LASTREQUESTURL_PARAM_NAME_IN_HTTPSESSION)) {
				userSession.setLastRedirectUrl(val == null ? null : val.toString());
			} else {
				userSession.setParameter(name, val);
			}
		} else if (httpSession != null) {
			httpSession.setAttribute(name, val);
		}
	}

	/**
	 * 从用户自定义会话（如果不为null的话优先）或http会话中会指定名称参数值。
	 * 
	 * @param userSession 用户自定义会话对象，可为null。
	 * @param httpSession http会话对象，可为null。
	 * @param name String 参数名，必须。
	 * @param defaultReturn Object 默认找不到时的返回值，可以为null。
	 * @param removeAfterGet boolean 获取后是否删除参数名对应参数值。
	 * 
	 * @return Object 获取的参数值，如果找不到择返回defaultReturn的值。
	 */
	public static Object getSessionParam(Session userSession, javax.servlet.http.HttpSession httpSession, String name, Object defaultReturn, boolean removeAfterGet) {
		if (name == null || name.length() == 0) return null;
		Object result = defaultReturn;
		if (userSession != null) {
			if (name.equalsIgnoreCase(LASTERROR_PARAM_NAME_IN_HTTPSESSION)) {
				result = userSession.getLastError();
				if (removeAfterGet) userSession.setLastError(null);
			} else if (name.equalsIgnoreCase(LASTMESSAGE_PARAM_NAME_IN_HTTPSESSION)) {
				result = userSession.getLastMessage();
				if (removeAfterGet) userSession.setLastMessage(null);
			} else if (name.equalsIgnoreCase(LASTOPERATION_PARAM_NAME_IN_HTTPSESSION)) {
				result = userSession.getLastOperation();
				if (removeAfterGet) userSession.setLastOperation(null);
			} else if (name.equalsIgnoreCase(LASTOPERATIONRESULT_PARAM_NAME_IN_HTTPSESSION)) {
				result = userSession.getLastOperationResult();
				if (removeAfterGet) userSession.setLastOperationResult(null);
			} else if (name.equalsIgnoreCase(LASTRESOURCE_PARAM_NAME_IN_HTTPSESSION)) {
				result = userSession.getLastResource();
				if (removeAfterGet) userSession.setLastResource(null);
			} else if (name.equalsIgnoreCase(LASTDOCUMENT_PARAM_NAME_IN_HTTPSESSION)) {
				result = userSession.getLastDocument();
				if (removeAfterGet) userSession.setLastDocument(null);
			} else if (name.equalsIgnoreCase(LASTREQUESTURL_PARAM_NAME_IN_HTTPSESSION)) {
				result = userSession.getLastRedirectUrl();
				if (removeAfterGet) userSession.setLastRedirectUrl(null);
			} else {
				result = userSession.getParameter(name);
				if (removeAfterGet) userSession.removeParameter(name);
			}
		}
		if (result == null && httpSession != null) {
			result = httpSession.getAttribute(name);
			if (removeAfterGet) httpSession.removeAttribute(name);
		}
		if (result == null) result = defaultReturn;
		return result;
	}

	/**
	 * 从用户自定义会话（如果不为null的话优先）或http会话中会指定名称参数值，并删除获取后的参数名对应的参数值。
	 * 
	 * @param userSession 用户自定义会话对象，可为null。
	 * @param httpSession http会话对象，可为null。
	 * @param name String 参数名，必须。
	 * @param defaultReturn Object 默认找不到时的返回值，可以为null。
	 * 
	 * @return Object 获取的参数值，如果找不到择返回defaultReturn的值。
	 */
	public static Object getSessionParam(Session userSession, javax.servlet.http.HttpSession httpSession, String name, Object defaultReturn) {
		return getSessionParam(userSession, httpSession, name, defaultReturn, true);
	}

	private Stack<Document> m_lastDocument = null;
	private Stack<Resource> m_lastResource = null;
	private HttpServletRequest m_httpRequest = null;
	private Exception m_lastError = null;
	private String m_lastMessage = null;
	private Object m_lastOperation = null;
	private OperationResult m_lastOperationResult = null;
	private String m_lastRedirectUrl = null;
	private Map<String, Object> m_params = null; // 保存参数名和参数值映射的参数集合。
	private User m_user = null;
	private int m_counter = 0; // 会话计数。
	private List<GenericTriplet<String, String, String>> m_logininfo = new ArrayList<GenericTriplet<String, String, String>>();
	private GenericTriplet<String, Integer, Boolean> m_tsimlogininfo = new GenericTriplet<String, Integer, Boolean>(null, 0, false);
	private Object m_lock = new Object();

	private static Map<String, Session> m_sessions = new HashMap<String, Session>();
	private static Object m_lock4Static = new Object();

	/**
	 * 缺省构造器。
	 */
	protected Session() {
		m_lastResource = new Stack<Resource>();
		m_lastDocument = new Stack<Document>();
	}

	/**
	 * 接收用户的构造器。
	 * 
	 * @param user
	 */
	protected Session(User user) {
		this();
		this.setUser(user);
	}

	/**
	 * 会话引用计数加1。
	 * 
	 * <p>
	 * 此方法需与{@link #decreaseRefCounter()}配对使用，{@link #removeSession(User)}、{@link Session#removeSession(String)}等会间接调用{@link #decreaseRefCounter()}。
	 * </p>
	 * <p>
	 * 通常与登录客户端个数同步，如同时通过tsim或浏览器登录，则计数为2，如果只有某一个客户端登录，则计数为1。
	 * </p>
	 */
	protected void increaseRefCounter() {
		synchronized (m_lock) {
			m_counter++;
		}
	}

	/**
	 * 会话引用计数减1，如果会话引用计数小于等于0，则从自定义会话集合中移除之。
	 * 
	 * <p>
	 * 每次调用{@link Session#removeSession(String)}或{@link Session#removeSession(Participant)}时会自动调用本方法。
	 * </p>
	 */
	protected void decreaseRefCounter() {
		synchronized (m_lock) {
			m_counter--;
			if (m_counter <= 0) m_sessions.remove(this.getSessionUNID());
		}
		if (m_counter <= 0) {
			CommonConfig cc = CommonConfig.getInstance();
			if (cc.getDebugable()) {
				User u = this.getUser();
				if (u != null) {
					String un = ParticipantHelper.getFormatValue(u, "ou0\\cn").replace(cc.getBelong() + User.SEPARATOR, "");
					System.out.println(String.format("%1$s:“%2$s”对应的用户会话被“%3$s”回收。", DateTime.getNowDTString(), un, cc.getSystemNameAbbr()));
				}
			}
		}// if end
	}

	/**
	 * 重置用户自定义会话以清除其中包含的所有信息。
	 */
	public void reset() {
		if (this.m_params != null) {
			this.m_params.clear();
			this.m_params = null;
		}
		this.setHttpRequest(null);
		this.setLastDocument(null);
		this.setLastError(null);
		this.setLastMessage(null);
		this.setLastOperation(null);
		this.setLastOperationResult(null);
		this.setLastRedirectUrl(null);
		this.setLastResource(null);
		this.m_logininfo = null;
		this.m_tsimlogininfo = null;
		this.setUser(null);
	}

	/**
	 * 获取服务器当前环境下的所有用户会话信息。
	 * 
	 * <p>
	 * key为会话id，value为对应用户会话。
	 * </p>
	 * 
	 * @return Map&lt;String, Session&gt;
	 */
	public static Map<String, Session> getAllSession() {
		return m_sessions;
	}

	/**
	 * 为指定的用户创建用户自定义会话并返回之。
	 * 
	 * <p>
	 * 此方法等同于{@link #newSession(User, boolean)}且第二个参数值为false。
	 * </p>
	 * 
	 * @param user
	 * @return Session 与指定的用户绑定的用户自定义会话对象。
	 */
	public static Session newSession(User user) {
		return newSession(user, false);
	}

	/**
	 * 为指定的用户创建用户自定义会话并返回之。
	 * 
	 * @param user
	 * @param increaseCounterFlag 是否增加会话引用计数，如果为true，则会增加，否则不增加。
	 * @return Session 与指定的用户绑定的用户自定义会话对象。
	 */
	public static Session newSession(User user, boolean increaseCounterFlag) {
		if (user == null) return null;
		Session session = null;
		synchronized (m_lock4Static) {
			session = m_sessions.get(user.getUNID());
			if (session == null) {
				session = new Session(user);
				m_sessions.put(user.getUNID(), session);
			}
		}
		session.setUser(user);
		if (increaseCounterFlag) session.increaseRefCounter();
		return session;
	}

	/**
	 * 删除用户对应的用户自定义会话。
	 * 
	 * @param user 用户对象。
	 * @return Session 返回删除的Session，如果找不到则返回null。
	 */
	public static Session removeSession(User user) {
		if (user == null) return null;
		return removeSession(user.getUNID());
	}

	/**
	 * 删除指定的用户自定义会话。
	 * 
	 * @param s 要删除的用户自定义会话。
	 * @return Session 返回删除的Session，如果找不到则返回null。
	 */
	public static Session removeSession(Session s) {
		if (s == null) return null;
		return removeSession(s.getSessionUNID());
	}

	/**
	 * 删除用户unid对应的用户自定义会话。
	 * 
	 * @param sessionUNID 用户unid（即用户自定义会话UNID）。
	 * @return Session 返回删除的Session，如果找不到则返回null。
	 */
	public static Session removeSession(String sessionUNID) {
		if (sessionUNID == null || sessionUNID.length() != 32) return null;
		if (m_sessions == null || m_sessions.isEmpty()) return null;
		Session s = m_sessions.get(sessionUNID);
		if (s != null) s.decreaseRefCounter();
		return s;
	}

	/**
	 * 获取当前用户自定义会话个数。
	 * 
	 * @return int
	 */
	public static int getSessionCount() {
		return m_sessions.size();
	}

	/**
	 * 根据提供的用户唯一信息线索获取对应的用户自定义会话。
	 * 
	 * <p>
	 * 如果对应自定义会话不存在，则返回null。
	 * </p>
	 * 
	 * @param uniqueUserClue String 将把此参数值分别作为用户UNID，用户别名，用户全名、安全编码依次查找。
	 * @return Session
	 */
	public static Session getSession(String uniqueUserClue) {
		if (uniqueUserClue == null || uniqueUserClue.length() == 0) return null;
		// 先判断是否可以通过unid获取。
		Session session = m_sessions.get(uniqueUserClue);
		// 如果找不到，那么循环查找每一个登录用户对应的自定义会话。
		if (session == null) {
			User user = null;
			for (Session s : m_sessions.values()) {
				if (s == null) continue;
				user = s.getUser();
				if (user == null) continue;
				// 如果用户自定义会话包含的用户别名和提供的参数值相等，则返回。
				if (uniqueUserClue == user.getAlias()) session = s;
				// 或者如果用户自定义会话包含的用户全名和提供的参数值相等，则返回。
				if (session == null && uniqueUserClue == user.getName()) session = s;
				if (session == null && StringUtil.getValueInt(uniqueUserClue, -1) == user.getSecurityCode()) session = s;
				if (session != null) break;
			}
		}
		return session;
	}

	/**
	 * 根据提供的用户唯一信息线索获取对应的用户自定义会话，其包含的用户的密码必须和指定的密码匹配，否则即使找到符合提供的线索的自定义会话也会返回null。
	 * 
	 * @param uniqueUserClue String 将把此参数值分别作为用户UNID，用户别名，用户全名、安全编码依次查找。
	 * @param password String 用户密码（加密后的形式），必须。
	 * @return Session
	 */
	public static Session getSession(String uniqueUserClue, String password) {
		if (password == null) return null;
		Session s = getSession(uniqueUserClue);
		if (s == null) return null;
		User user = s.getUser();
		if (user == null || !password.equals(user.getPassword())) return null;
		return s;
	}

	/**
	 * 获取当前用户自定义会话对应的最近一次http请求。
	 * 
	 * @return {@link javax.servlet.http.HttpServletRequest}
	 */
	public HttpServletRequest getHttpRequest() {
		return this.m_httpRequest;
	}

	/**
	 * 设置当前用户自定义会话对应的最近一次http请求。
	 * 
	 * @param request
	 */
	public void setHttpRequest(javax.servlet.http.HttpServletRequest request) {
		this.m_httpRequest = request;
	}

	/**
	 * 获取当前用户自定义会话的UNID（即绑定的用户的UNID）。
	 * 
	 * @return String
	 */
	public String getSessionUNID() {
		if (this.m_user == null) return User.ANONYMOUS_USER_UNID;
		return this.m_user.getUNID();
	}

	/**
	 * 获取自定义会话绑定的用户对象。
	 * 
	 * @return Participant
	 */
	public User getUser() {
		return (this.m_user == null ? User.getAnonymous() : this.m_user);
	}

	/**
	 * 设置自定义会话绑定的用户对象。
	 * 
	 * @return Document
	 */
	protected void setUser(User user) {
		this.m_user = user;
	}

	/**
	 * 返回最近一次打开的文档。
	 * 
	 * @return
	 */
	public Document getLastDocument() {
		return (this.m_lastDocument.empty() ? null : this.m_lastDocument.peek());
	}

	/**
	 * 设置用户最近一次打开的文档。
	 * 
	 * @param document Document
	 */
	public void setLastDocument(Document document) {
		if (!this.m_lastDocument.empty()) this.m_lastDocument.clear();
		if (document != null) this.m_lastDocument.push(document);
	}

	/**
	 * 获取最近一次打开的资源。
	 * 
	 * @param resource Resource
	 */
	public Resource getLastResource() {
		return (this.m_lastResource.empty() ? null : this.m_lastResource.peek());
	}

	/**
	 * 设置用户最近一次打开的资源。
	 * 
	 * @param resource Resource
	 */
	public void setLastResource(Resource resource) {
		if (!this.m_lastResource.empty()) this.m_lastResource.clear();
		if (resource != null) this.m_lastResource.push(resource);
	}

	/**
	 * 获取最近一次http请求执行的结果对应的消息文本。
	 * 
	 * @return String，如果最近一次http请求执行时没有产生信息文本则返回null。
	 */
	public String getLastMessage() {
		return this.m_lastMessage;
	}

	/**
	 * 设置最近一次http请求执行的结果对应的消息文本。
	 * 
	 * @param message
	 */
	public void setLastMessage(String message) {
		this.m_lastMessage = message;
	}

	/**
	 * 获取最近一次http请求执行时产生的异常信息。
	 * 
	 * @return Exception，如果最近一次http请求执行时没有产生异常信息则返回null。
	 */
	public Exception getLastError() {
		return this.m_lastError;
	}

	/**
	 * 设置最近一次http请求执行时产生的异常信息。
	 * 
	 * @param ex void
	 */
	public void setLastError(Exception ex) {
		this.m_lastError = ex;
	}

	/**
	 * 获取最近一次http请求执行后需要重定向的URL地址。
	 * 
	 * @return String
	 */
	public String getLastRedirectUrl() {
		return this.m_lastRedirectUrl;
	}

	/**
	 * 设置最近一次http请求执行后需要重定向的URL地址。
	 * 
	 * @param lastRedirectUrl void
	 */
	public void setLastRedirectUrl(String lastRedirectUrl) {
		this.m_lastRedirectUrl = lastRedirectUrl;
	}

	/**
	 * 获取最近一次http请求执行后的操作表达式解析对象。
	 * 
	 * <p>
	 * 返回“{@link com.tansuosoft.discoverx.bll.function.OperationParser}”的实例。
	 * </p>
	 * 
	 * @return Object
	 */
	public Object getLastOperation() {
		return this.m_lastOperation;
	}

	/**
	 * 设置最近一次http请求执行后的操作表达式解析对象。
	 * 
	 * @param lastOperation Object
	 */
	public void setLastOperation(Object lastOperation) {
		this.m_lastOperation = lastOperation;
	}

	/**
	 * 获取最近一次http请求执行后的操作结果对象。
	 * 
	 * <p>
	 * 即{@link com.tansuosoft.discoverx.model.OperationResult}对象的实例。
	 * </p>
	 * 
	 * @return OperationResult
	 */
	public OperationResult getLastOperationResult() {
		return this.m_lastOperationResult;
	}

	/**
	 * 设置最近一次http请求执行后的操作结果对象。
	 * 
	 * @param lastOperationResult OperationResult
	 */
	public void setLastOperationResult(OperationResult lastOperationResult) {
		this.m_lastOperationResult = lastOperationResult;
	}

	/**
	 * 追加登录信息。
	 * 
	 * @param client 登录客户端全名（长名称）
	 * @param address 登录地址
	 * @return List&lt;GenericTriplet&lt;String, String, String&gt;&gt;
	 */
	public List<GenericTriplet<String, String, String>> appendLoginInfo(String client, String address) {
		if (m_logininfo == null) m_logininfo = new ArrayList<GenericTriplet<String, String, String>>();
		m_logininfo.add(new GenericTriplet<String, String, String>(client, address, DateTime.getNowDTString()));
		return m_logininfo;
	}

	/**
	 * 移除登录客户端客户端信息。
	 * 
	 * @param client
	 * @param address
	 * @return
	 */
	public List<GenericTriplet<String, String, String>> removeLoginInfo(String client, String address) {
		if ((client == null && address == null) || m_logininfo == null) return null;
		int cidx = -1;
		int aidx = -1;
		int idx = -1;
		for (int i = 0; i < m_logininfo.size(); i++) {
			GenericTriplet<String, String, String> gt = m_logininfo.get(i);
			if (address != null && address.indexOf(gt.getFirst()) >= 0) {
				aidx = i;
			}
			if (client != null && client.indexOf(gt.getFirst()) >= 0) {
				cidx = i;
			}
			if (aidx != -1 && cidx == aidx) {
				idx = cidx;
				break;
			}
		}
		if (idx >= 0) {
			m_logininfo.remove(idx);
		} else if (aidx >= 0) {
			m_logininfo.remove(aidx);
		} else if (cidx >= 0) {
			m_logininfo.remove(cidx);
		}
		return m_logininfo;
	}

	/**
	 * 设置小助手登录信息。
	 * 
	 * @param address 有效客户端地址
	 * @param presence 小助手状态信息
	 * @param logined 是否登录成功标记
	 * @return GenericTriplet&lt;String, Integer, Boolean&gt;
	 */
	public GenericTriplet<String, Integer, Boolean> setTsimLoginInfo(String address, int presence, boolean logined) {
		if (m_tsimlogininfo == null) m_tsimlogininfo = new GenericTriplet<String, Integer, Boolean>(null, 0, false);
		m_tsimlogininfo.setFirst(address);
		m_tsimlogininfo.setSecond(presence);
		m_tsimlogininfo.setThird(logined);
		return m_tsimlogininfo;
	}

	/**
	 * 设置小助手状态。
	 * 
	 * <p>
	 * 仅供内部使用。
	 * </p>
	 * 
	 * @param p
	 */
	public void setTsimPresence(String p) {
		if (m_tsimlogininfo == null) return;
		m_tsimlogininfo.setSecond(StringUtil.getValueInt(p, 0));
	}

	/**
	 * 获取使用此用户自定义会话的所有登录地址信息。
	 * 
	 * <p>
	 * 如果多次或多个客户端登录，则用{@link Session#ADDRESS_AND_CLIENT_SEP_STR}分隔多个地址不同的信息。
	 * </p>
	 * 
	 * @return String
	 */
	public String getLoginAddress() {
		if (m_logininfo == null) return null;
		StringBuilder sb = new StringBuilder();
		try {
			for (Iterator<GenericTriplet<String, String, String>> itr = m_logininfo.iterator(); itr.hasNext();) {
				GenericTriplet<String, String, String> gt = itr.next();
				if (gt == null) continue;
				sb.append(sb.length() > 0 ? Session.ADDRESS_AND_CLIENT_SEP_STR : "").append(gt.getSecond());
			}
		} catch (ConcurrentModificationException ex) {
			FileLogger.debug("并发异常：获取登录客户端IP地址！");
		}
		return sb.toString();
	}

	/**
	 * 获取使用此用户自定义会话的所有登录客户端信息。
	 * 
	 * <p>
	 * 如果多次登录，则用{@link Session#ADDRESS_AND_CLIENT_SEP_STR}分隔多个客户端信息。
	 * </p>
	 * 
	 * @return String
	 */
	public String getLoginClient() {
		if (m_logininfo == null) return null;
		StringBuilder sb = new StringBuilder();
		try {
			Object[] objs = m_logininfo.toArray();
			for (Object obj : objs) {
				@SuppressWarnings("unchecked")
				GenericTriplet<String, String, String> gt = (GenericTriplet<String, String, String>) obj;
				if (gt == null) continue;
				sb.append(sb.length() > 0 ? Session.ADDRESS_AND_CLIENT_SEP_STR : "").append(gt.getFirst());
			}
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		return sb.toString();
	}

	/**
	 * 返回小助手在线状态。
	 * 
	 * <p>
	 * 只有客户端通过小助手登录成功后才能返回有效值。
	 * </p>
	 * 
	 * @return int
	 */
	public int getTsimPresence() {
		if (m_tsimlogininfo == null) return 0;
		Integer i = m_tsimlogininfo.getSecond();
		return (i == null ? 0 : i);
	}

	/**
	 * 返回小助手实际登录地址。
	 * 
	 * <p>
	 * 区别于{@link #getLoginAddress()}，此地址保存登录用户的完整可用的IP地址信息，它通常有以下几种格式：
	 * <ul>
	 * <li>直接IP地址，如“192.168.1.10”（内网地址）、“121.204.105.38”（外网地址）等，表示当前用户通过此ip地址直接连接到服务器。</li>
	 * <li>间接IP地址，如“121.204.105.38-192.168.1.10”表示当前用户通过“121.204.105.38”这个网关连接到服务器，实际的内部地址是“192.168.1.10”。</li>
	 * </ul>
	 * </p>
	 * <p>
	 * 只有客户端通过小助手登录成功后才能返回有效值。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTsimAddress() {
		if (m_tsimlogininfo == null) return "";
		return m_tsimlogininfo.getFirst();
	}

	/**
	 * 获取是否已通过小助手登录。
	 * 
	 * <p>
	 * 只有客户端通过小助手登录成功后才能返回true。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getTsimLogin() {
		if (m_tsimlogininfo == null) return false;
		Boolean b = m_tsimlogininfo.getThird();
		return (b == null ? false : b.booleanValue());
	}

	/**
	 * 获取name指定参数名称对应的参数值，如果找不到，则返回null。
	 * 
	 * @param name
	 * @return Object
	 */
	public Object getParameter(String name) {
		if (name == null || name.trim().length() == 0) return null;
		if (this.m_params == null) return null;
		return this.m_params.get(name);
	}

	/**
	 * 返回参数名对应参数值作为指定类型的结果。
	 * 
	 * <p>
	 * &lt;T&gt; 返回的对象的目标泛型。
	 * </p>
	 * 
	 * @param name 参数名，其参数值必须是所定义的完整类名。
	 * @param clazz 指定返回的对象的目标类型。
	 * @param defaultReturn 如果类无效，或者无法构造，或者类型不符，则返回此结果。
	 * @return 返回目标对象。
	 */
	@SuppressWarnings("unchecked")
	public <T> T getParamValueObject(String name, Class<T> clazz, T defaultReturn) {
		Object obj = this.getParameter(name);
		if (obj == null) return defaultReturn;
		T result = null;
		try {
			result = (T) obj;
		} catch (Exception ex) {
			result = defaultReturn;
		}
		return result;
	}

	/**
	 * 获取所有参数名称字符串的集合对应的迭代器对象，如果没有任何参数，则返回null。
	 * 
	 * @return Iterator<String>
	 */
	public Iterator<String> getParameterNames() {
		if (this.m_params == null || this.m_params.size() == 0) return null;
		Iterator<String> iterator = this.m_params.keySet().iterator();
		return iterator;
	}

	/**
	 * 设置name指定的参数名称对应的参数值为value。
	 * <p>
	 * 如果同名的参数值已经存在则替换，如果value为null，则等同于调用removeParameter(name)
	 * </p>
	 * 
	 * @param name String
	 * @param value Object
	 */
	public void setParameter(String name, Object value) {
		if (name == null || name.trim().length() == 0) return;
		if (this.m_params == null) this.m_params = new HashMap<String, Object>();
		if (value == null) this.removeParameter(name);
		this.m_params.put(name, value);
	}

	/**
	 * 删除name指定名称的参数值。
	 * 
	 * @param name String
	 */
	public void removeParameter(String name) {
		if (this.m_params != null && name != null && name.trim().length() > 0) {
			this.m_params.remove(name);
		}
	}

	/**
	 * 获取name指定参数名称对应的参数值并在获取后删除name指定名称的参数值。
	 * 
	 * @param name
	 * @return Object 如果找不到对应参数名，则返回null。
	 */
	public Object getAndRemoveParameter(String name) {
		if (this.m_params == null || name == null || name.trim().length() == 0) return null;
		if (this.m_params.get(name) == null) return null;
		Object obj = this.m_params.get(name);
		this.removeParameter(name);
		return obj;
	}

	/**
	 * 返回参数名对应参数值的String类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果参数值为null时返回的默认结果。
	 * @return String
	 */
	public String getParamValueString(String paramName, String defaultReturn) {
		Object obj = this.getParameter(paramName);
		if (obj == null) return defaultReturn;
		return obj.toString();
	}

	/**
	 * 返回参数名对应的参数值的int类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return int
	 */
	public int getParamValueInt(String paramName, int defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueInt(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的long类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return long
	 */
	public long getParamValueLong(String paramName, long defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueLong(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的float类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return float
	 */
	public float getParamValueFloat(String paramName, float defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueFloat(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的double类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return double
	 */
	public double getParamValueDouble(String paramName, double defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueDouble(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的boolean类型结果。
	 * 
	 * <p>
	 * 参数值（不区分大小写）配置为“true”、“yes”、“y”、“1”等情况时返回true，否则返回false。
	 * </p>
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return boolean
	 */
	public boolean getParamValueBool(String paramName, boolean defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueBool(this.getParamValueString(paramName, null), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值转换为enumCls定义的枚举中某一个具体枚举值类型之后的结果。
	 * 
	 * @param &lt;T&gt; 返回的枚举类型。
	 * @param paramName String 参数名，必须。
	 * @param enumCls Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return T 具体枚举结果。
	 */
	public <T> T getParamValueEnum(String paramName, Class<T> enumCls, T defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueEnum(this.getParamValueString(paramName, null), enumCls, defaultReturn);
	}

	/**
	 * 获取指定会话绑定的用户。
	 * 
	 * <p>
	 * 如果会话不存在，则返回一个匿名用户。
	 * </p>
	 * 
	 * @param session Session
	 * @return Participant
	 */
	public static User getUser(Session session) {
		User ua = User.getAnonymous();
		User u = (session == null ? null : session.getUser());
		return (u == null ? ua : u);
	}

	/**
	 * 获取表示系统本身作为用户时其对应的自定义会话对象的一个新实例。
	 * 
	 * <p>
	 * 系统用户的安全编码为-1，名称为“系统”，别名为“system”，具有超级管理员权限。
	 * </p>
	 * 
	 * @return
	 */
	public static Session getSystemSession() {
		Session s = new Session();
		s.setUser(User.getSystemUser());
		return s;
	}

	/**
	 * 重载toString：返回绑定用户全名。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.m_user.getName();
	}

	/**
	 * 自定义序列化写操作。
	 * 
	 * @param out
	 * @throws IOException
	 */
	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeUTF(this.getLoginClient());
		out.writeUTF(this.getLoginAddress());
		out.writeObject(this.m_user == null ? "" : this.m_user.getUNID());
		out.writeObject(this.m_params);
		out.writeInt(m_counter);
	}

	/**
	 * 自定义序列化读操作。
	 * 
	 * @param in
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		String[] clients = StringUtil.splitString(in.readUTF(), Session.ADDRESS_AND_CLIENT_SEP);
		String[] addresses = StringUtil.splitString(in.readUTF(), Session.ADDRESS_AND_CLIENT_SEP);
		if (clients != null && addresses != null) {
			for (int i = 0; i < clients.length; i++) {
				if (addresses.length > i) this.appendLoginInfo(clients[i], addresses[i]);
			}
		}
		String userunid = (String) in.readObject();
		if (userunid != null && userunid.length() > 0) {
			User user = ResourceContext.getInstance().getResource(userunid, User.class);
			if (user != null) this.setUser(user);
		}
		m_params = (Map<String, Object>) in.readObject();
		m_lastResource = new Stack<Resource>();
		m_lastDocument = new Stack<Document>();
		m_counter = in.readInt();
		m_lock = new Object();
		m_sessions.put(userunid, this);
	}

	protected static HashMap<String, HttpSession> m_smap = new HashMap<String, HttpSession>();

	/**
	 * 获取当前所有http会话信息。
	 * 
	 * @return Map&lt;String, HttpSession&gt;
	 */
	public static Map<String, HttpSession> getAllHttpSession() {
		return m_smap;
	}

	/**
	 * @see javax.servlet.http.HttpSessionActivationListener#sessionDidActivate(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionDidActivate(HttpSessionEvent se) {
		HttpSession s = se.getSession();
		m_smap.put(s.getId(), s);
	}

	/**
	 * @see javax.servlet.http.HttpSessionActivationListener#sessionWillPassivate(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionWillPassivate(HttpSessionEvent se) {
		HttpSession s = se.getSession();
		m_smap.remove(s.getId());
	}
}

