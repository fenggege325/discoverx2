/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述文档在浏览器中显示时相关显示选项信息的类。
 * 
 * <p>
 * 统称“文档显示选项”。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentDisplay implements Cloneable {
	/**
	 * 缺省构造器。
	 */
	public DocumentDisplay() {
	}

	private String m_expression = null; // 条件表达式。
	private int m_maxWidth = 0; // 最大显示内容区宽度（像素）。
	private String m_display = null; // 显示方式，可选。
	private List<Tab> m_tabs = null; // Tab配置，可选。
	private List<HtmlAttribute> m_htmlAttributes = null; // 表单内容在文档中显示时连带输出的html属性。
	private String m_readonlyExpression = null; // 文档只读公式。

	/**
	 * 返回条件表达式。
	 * 
	 * <p>
	 * 如果配置了表达式，那么必须在表达式返回true时才会应用此配置。<br/>
	 * 如果不配置表达式，则表示直接使用此配置，而忽略系统默认的显示配置。
	 * </p>
	 * 
	 * @return String
	 */
	public String getExpression() {
		return this.m_expression;
	}

	/**
	 * 设置条件表达式，可选。
	 * 
	 * @param expression String
	 */
	public void setExpression(String expression) {
		this.m_expression = expression;
	}

	/**
	 * 返回最大显示内容区宽度（像素）。
	 * 
	 * <p>
	 * 系统默认为{@link com.tansuosoft.discoverx.common.CommonConfig#DEFAULT_DOCUMENT_CONTENT_WIDTH}中定义的值，除非在
	 * {@link com.tansuosoft.discoverx.common.CommonConfig#getMaxDocumentContentWidth()}属性中做了具体定义。
	 * </p>
	 * <p>
	 * 必须考虑所有客户端的最大宽度，否则设置只过大可能导致内容显示不完全或因滚动条破坏整体页面效果。
	 * </p>
	 * 
	 * @return int
	 */
	public int getMaxWidth() {
		return this.m_maxWidth;
	}

	/**
	 * 设置最大显示内容区宽度（像素）。
	 * 
	 * @see DocumentDisplay#getMaxWidth()
	 * @param maxWidth int
	 */
	public void setMaxWidth(int maxWidth) {
		this.m_maxWidth = maxWidth;
	}

	/**
	 * 返回显示方式。
	 * 
	 * <p>
	 * 显示方式用于实际呈现文档字段内容，如果不提供，则系统会根据文档所属表单表单中的字段定义方式使用默认渲染引擎显示文档字段内容。<br/>
	 * 只有当系统默认显示效果无法符合实际需要时才需要配置自定义的显示方式。
	 * </p>
	 * <p>
	 * 显示方式可以配置为自定义输出文档字段内容的类的全限定类名（此类必须继承自{@link com.tansuosoft.discoverx.web.ui.document.ItemsRender}）或某个用来显示的文档内容的页面文件名（保留使用）。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDisplay() {
		return this.m_display;
	}

	/**
	 * 设置显示方式，可选。
	 * 
	 * @param display String
	 */
	public void setDisplay(String display) {
		this.m_display = display;
	}

	/**
	 * 返回Tab配置，可选。
	 * 
	 * <p>
	 * 不提供则使用系统默认的Tab配置。
	 * </p>
	 * 
	 * @return List&lt;Tab&gt;
	 */
	public List<Tab> getTabs() {
		return this.m_tabs;
	}

	/**
	 * 设置Tab配置，可选。
	 * 
	 * @see DocumentDisplay#getTabs()
	 * @param tabs List&lt;Tab&gt;
	 */
	public void setTabs(List<Tab> tabs) {
		this.m_tabs = tabs;
	}

	/**
	 * 返回文档内容在文档中显示时body节点连带输出的html属性，可选。
	 * 
	 * <p>
	 * 可以用此属性来配置文档在浏览器中显示时html中body元素的额外属性，如javascript事件处理等。
	 * </p>
	 * <p>
	 * 请注意属性值的配置方式，因为输出按照“&lt;body {属性名}="{属性值}"...”的方式，所以引号不正常使用，可能导致html脚本错误。
	 * </p>
	 * 
	 * @return List&lt;HtmlAttribute&gt;
	 */
	public List<HtmlAttribute> getHtmlAttributes() {
		return this.m_htmlAttributes;
	}

	/**
	 * 设置文档内容在文档中显示时body节点连带输出的html属性，可选。
	 * 
	 * @see DocumentDisplay#getHtmlAttributes()
	 * @param htmlAttributes List&lt;HtmlAttribute&gt;
	 */
	public void setHtmlAttributes(List<HtmlAttribute> htmlAttributes) {
		this.m_htmlAttributes = htmlAttributes;
	}

	/**
	 * 返回文档只读公式。
	 * 
	 * @see DocumentDisplay#setReadonlyExpression(String)
	 * @return boolean
	 */
	public String getReadonlyExpression() {
		return this.m_readonlyExpression;
	}

	/**
	 * 设置文档只读公式，可选。
	 * 
	 * <p>
	 * 新增的文档忽略此配置，总是为可编辑状态。
	 * </p>
	 * <p>
	 * 文档只读状态比字段只读状态优先级高。
	 * </p>
	 * <p>
	 * 不提供或配置的表达式返回false则表示为可编辑状态，否则如果配置的表达式返回true则表示只读状态。
	 * </p>
	 * <p>
	 * 保存按钮等是根据是否只读状态自动显示或隐藏的不需要通过手工配置。
	 * </p>
	 * 
	 * @param readonlyExpression
	 */
	public void setReadonlyExpression(String readonlyExpression) {
		this.m_readonlyExpression = readonlyExpression;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		DocumentDisplay dd = new DocumentDisplay();
		dd.setDisplay(this.getDisplay());
		dd.setExpression(this.getExpression());
		dd.setMaxWidth(this.getMaxWidth());
		dd.setReadonlyExpression(this.getReadonlyExpression());
		List<HtmlAttribute> atts = this.getHtmlAttributes();
		if (atts != null && atts.size() > 0) {
			dd.setHtmlAttributes(new ArrayList<HtmlAttribute>(atts.size()));
			for (HtmlAttribute x : atts) {
				if (x == null) continue;
				dd.getHtmlAttributes().add((HtmlAttribute) x.clone());
			}
		}
		List<Tab> tabs = this.getTabs();
		if (tabs != null && tabs.size() > 0) {
			dd.setTabs(new ArrayList<Tab>(tabs.size()));
			for (Tab x : tabs) {
				if (x == null) continue;
				dd.getTabs().add((Tab) x.clone());
			}
		}
		return dd;
	}
}

