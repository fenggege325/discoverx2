/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 表示系统支持的签名方式配置的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class SignatureConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private SignatureConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static SignatureConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return {@link SignatureConfig}
	 */
	public static SignatureConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new SignatureConfig();
			}
		}
		return m_instance;
	}

	private int m_supportSignature = 1; // 支持的签名方式。
	private String m_signatureTitleDictionary = null; // 签名方式标记和签名方式名称对应信息。
	private SortedMap<Integer, String> m_map = null;

	/**
	 * 获取支持的签名方式。
	 * 
	 * @return int
	 */
	public int getSupportSignature() {
		return m_supportSignature;
	}

	/**
	 * 设置支持的签名方式。
	 * 
	 * <p>
	 * 必须是{@link Signature}中各值的有效组合。
	 * </p>
	 * 
	 * @param supportSignature int
	 */
	public void setSupportSignature(int supportSignature) {
		m_supportSignature = supportSignature;
	}

	/**
	 * 获取签名方式标记和签名方式名称对应信息。
	 * 
	 * @return String
	 */
	public String getSignatureTitleDictionary() {
		return m_signatureTitleDictionary;
	}

	/**
	 * 设置签名方式标记和签名方式名称对应信息。
	 * 
	 * <p>
	 * 用半角分号（“;”）分隔每一个签名，签名中用半角竖线(“|”)符号分隔标记（在前，同时标记必须为{@link Signature}中某个具体枚举值的数字）和标题（在后）。
	 * </p>
	 * 
	 * @param signatureTitleDictionary String
	 */
	public void setSignatureTitleDictionary(String signatureTitleDictionary) {
		m_signatureTitleDictionary = signatureTitleDictionary;
		if (m_signatureTitleDictionary != null && m_signatureTitleDictionary.length() > 0) {
			String strs[] = StringUtil.splitString(m_signatureTitleDictionary, ';');
			if (strs != null && strs.length > 0) {
				if (m_map == null) m_map = new TreeMap<Integer, String>();
				for (String str : strs) {
					m_map.put(StringUtil.getValueInt(StringUtil.stringLeft(str, "|"), -1), StringUtil.stringRight(str, "|"));
				}
			}
		}
	}

	/**
	 * 获取配置的所有签名枚举值数字与标题一一对应的字典集合。
	 * 
	 * @return
	 */
	public Map<Integer, String> getSignatureTitles() {
		return this.m_map;
	}

	/**
	 * 根据签名枚举信息获取签名标题。
	 * 
	 * @param signature {@link Signature}
	 * @return
	 */
	public String getSignatureTitle(Signature signature) {
		if (signature == null || m_map == null) return null;
		return m_map.get(signature.getIntValue());
	}

}

