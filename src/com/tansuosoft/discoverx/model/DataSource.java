/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 用于描述字段数据源相关选项的基类。
 * 
 * <p>
 * 数据源主要用于为字段的输入提供约束数据或输入便利，数据源的结果一般包含一个标题（显示给用户）和值（后台保存）。
 * </p>
 * <p>
 * 所有具体类型的数据源（参见：{@link DataSourceType}）选项类必须派生自此基类。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DataSource {
	/**
	 * 缺省构造器。
	 */
	public DataSource() {
	}

	private String m_valueFormat = null; // 保存值格式。
	private String m_titleFormat = null; // 显示值格式。

	/**
	 * 返回保存值格式。
	 * 
	 * <p>
	 * 在有些类型的数据源中可能被忽略。
	 * </p>
	 * 
	 * @return String
	 */
	public String getValueFormat() {
		return this.m_valueFormat;
	}

	/**
	 * 设置保存值格式。
	 * 
	 * @param valueFormat String
	 */
	public void setValueFormat(String valueFormat) {
		this.m_valueFormat = valueFormat;
	}

	/**
	 * 返回显示值格式。
	 * 
	 * <p>
	 * 在有些类型的数据源中可能被忽略。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTitleFormat() {
		return this.m_titleFormat;
	}

	/**
	 * 设置显示值格式。
	 * 
	 * @param titleFormat String
	 */
	public void setTitleFormat(String titleFormat) {
		this.m_titleFormat = titleFormat;
	}
}

