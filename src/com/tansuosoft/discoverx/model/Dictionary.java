/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 系统字典信息资源类。
 * 
 * <p>
 * 字典最常见的用法是用于给字段或数据源等提供可选择的输入信息， 比如一个名为"文种"的字段，引用了定义了14个标准文种的字典资源， 那么"文种"字段在输入时，可以从下拉的14个文种中选择； 字典还可以用来映射关键字和实际值，比如“发布”这个关键字对应"1"这个实际值。
 * <p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Dictionary extends Resource {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 2268286591113169993L;

	/**
	 * 缺省构造器
	 */
	public Dictionary() {
	}

	/**
	 * 获取字典值，如果字典包含别名，则返回别名值，否则返回字典名称。
	 * 
	 * @return String
	 */
	public String getValue() {
		String alias = this.getAlias();
		if (alias == null || alias.length() == 0) return this.getName();
		return alias;
	}

	private String m_belong = null; // 所属应用程序。

	/**
	 * 返回所属应用程序。
	 * 
	 * <p>
	 * 默认为null或空字符串时表示通用(即它不是特定某个应用程序使用的)，如果为应用程序UNID则表示输于其对应的应用程序。 此属性仅在分类和应用程序导出时使用，设置属于资源某个应用并不会导致其它在地方无法使用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getBelong() {
		return this.m_belong;
	}

	/**
	 * 设置所属应用程序。
	 * 
	 * @param belong String
	 */
	public void setBelong(String belong) {
		this.m_belong = belong;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Dictionary x = (Dictionary) super.clone();

		x.setBelong(this.getBelong());

		return x;
	}
}

