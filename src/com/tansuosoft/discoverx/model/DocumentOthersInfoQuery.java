/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 用于查询文档列表信息时获取一些额外显示信息的类。
 * 
 * <p>
 * 额外显示信息保存在文档表(t_document)的c_others字段中。<br/>
 * 典型的如获取文档流程状态，当前处理人信息等。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class DocumentOthersInfoQuery {
	/**
	 * 目标文档对象。
	 */
	protected Document document = null;

	/**
	 * 发起状态统称常量值：发起状态。
	 */
	public static final String BEGIN_STATE = "发起状态";
	/**
	 * 一个空格。
	 */
	public static final String SPACE = " ";
	/**
	 * 常量值：无
	 */
	public static final String NA = "无";
	/**
	 * 常量值：流程结束
	 */
	public static final String WFEND = "流程结束";

	/**
	 * 接收文档资源对象的构造器。
	 * 
	 * @param doc
	 */
	public DocumentOthersInfoQuery(Document doc) {
		document = doc;
	}

	/**
	 * 重载：返回保存于t_document.c_others字段的文档额外信息值。
	 * 
	 * <p>
	 * 系统默认为返回“w:'{当前流程名称}',a:'发起阶段',p:['{当前用户名}']”。
	 * </p>
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (document == null) return "";
		Application app = document.getApplication(document.getApplicationCount());
		if (app == null) return "";
		Resource r = ResourceContext.getInstance().getResource(app.getWorkflow(), "workflow");
		if (r == null) return "";
		return String.format("w:'%1$s',s:'%2$s',p:'%3$s'", r.getName(), BEGIN_STATE, document.getState() > DocumentState.Signed.getIntValue() ? SPACE : StringUtil.getValueString(StringUtil.stringRightBack(document.getCreator(), User.SEPARATOR), document.getCreator()));
	}

	/**
	 * 检查是否所属直接应用中配置的记录额外状态信息为true。
	 * 
	 * @return boolean
	 */
	public boolean isAutoRecordStateInfo() {
		if (document == null) return false;
		Application app = document.getApplication(document.getApplicationCount());
		if (app == null) return false;
		return app.getAutoRecordStateInfo();
	}
}

