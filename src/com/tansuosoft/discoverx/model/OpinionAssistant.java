/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.List;

/**
 * 用于在用户填写意见时，定义易于输入的意见填写助手相关信息的资源类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OpinionAssistant extends Resource {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -8820496486745088728L;

	/**
	 * 缺省构造器
	 */
	public OpinionAssistant() {
		super();
		this.setName("意见填写助手信息配置。");
	}

	private List<String> m_imperatives = null; // 常见祈使词。
	private List<String> m_entitles = null; // 常见称呼词。
	private List<String> m_actions = null; // 常见动作词。
	private List<String> m_opinions = null; // 常见意见内容。

	/**
	 * 返回常见祈使词。
	 * 
	 * @return List<String>
	 */
	public List<String> getImperatives() {
		return this.m_imperatives;
	}

	/**
	 * 设置常见祈使词。
	 * 
	 * @param imperatives List<String>
	 */
	public void setImperatives(List<String> imperatives) {
		this.m_imperatives = imperatives;
	}

	/**
	 * 返回常见称呼词。
	 * 
	 * @return List<String>
	 */
	public List<String> getEntitles() {
		return this.m_entitles;
	}

	/**
	 * 设置常见称呼词。
	 * 
	 * @param entitles List<String>
	 */
	public void setEntitles(List<String> entitles) {
		this.m_entitles = entitles;
	}

	/**
	 * 返回常见动作词。
	 * 
	 * @return List<String>
	 */
	public List<String> getActions() {
		return this.m_actions;
	}

	/**
	 * 设置常见动作词。
	 * 
	 * @param actions List<String>
	 */
	public void setActions(List<String> actions) {
		this.m_actions = actions;
	}

	/**
	 * 返回常见意见内容。
	 * 
	 * @return List<String>
	 */
	public List<String> getOpinions() {
		return this.m_opinions;
	}

	/**
	 * 设置常见意见内容。
	 * 
	 * @param opinions List<String>
	 */
	public void setOpinions(List<String> opinions) {
		this.m_opinions = opinions;
	}
}

