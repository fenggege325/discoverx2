/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 表示根据指定视图提供数据源的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DataSourceView extends DataSource {

	/**
	 * 缺省构造器。
	 */
	public DataSourceView() {
		super();
		this.setTitleFormat("1");
		this.setValueFormat("1");
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param unid
	 */
	public DataSourceView(String unid) {
		this();
		m_UNID = unid;
	}

	private String m_UNID = null; // 视图资源UNID，必须。

	/**
	 * 返回视图资源UNID，必须。
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置视图资源UNID，必须。
	 * 
	 * @param UNID String
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回用于获取数据源标题的视图列索引。
	 * 
	 * <p>
	 * 默认为1，表示视图第一列作为选择条目的显示结果，必须配置为有效的序号，否则可能出错。
	 * </p>
	 * <p>
	 * 0表示视图条目的UNID。
	 * </p>
	 * 
	 * @return int
	 */
	public int getTitleColumnIndex() {
		return StringUtil.getValueInt(this.getTitleFormat(), 1);
	}

	/**
	 * 返回用于获取数据源值的视图列索引。
	 * 
	 * <p>
	 * 默认为1，表示视图第一列作为选择条目的保存结果，必须配置为有效的序号，否则可能出错。
	 * </p>
	 * <p>
	 * 0表示视图条目的UNID。
	 * </p>
	 * 
	 * @return int
	 */
	public int getValueColumnIndex() {
		return StringUtil.getValueInt(this.getValueFormat(), 1);
	}

}

