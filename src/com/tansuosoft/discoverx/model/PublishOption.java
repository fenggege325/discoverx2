/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 文档公布选项枚举类型
 * 
 * @author coca@tansuosoft.cn
 */
public enum PublishOption implements EnumBase {
	/**
	 * 表示对所有人公开（0）
	 */
	All(0),
	/**
	 * 表示对所有注册登录用户公开（1）
	 */
	User(1),
	/**
	 * 表示对同属直接部门的用户公开（2）
	 */
	DirectOU(2),
	/**
	 * 表示对同属一级部门的用户公开（3）
	 * 
	 * <p>
	 * 如果直接部门与一级部门相同，则此选项与{@link #DirectOU}相同。
	 * </p>
	 */
	OU1(3),
	/**
	 * 表示对指定范围的人员公开（9）。
	 * 
	 * <p>
	 * {@link Application#getPublishOption()}返回此值时表示在发布时选择公开人员。
	 * </p>
	 */
	Custom(9);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	PublishOption(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return PublishOption
	 */
	public PublishOption parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (PublishOption s : PublishOption.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return PublishOption.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return PublishOption
	 */
	public static PublishOption parse(int v) {
		for (PublishOption s : PublishOption.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

