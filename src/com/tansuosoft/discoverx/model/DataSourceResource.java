/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示根据指定某个资源提供数据源的类。
 * 
 * <p>
 * 此数据源类为从资源树中提取具体数据源结果的类，因此资源必须是包含下级资源的树型结构的资源（常见的如字典资源{@link Dictionary}等）。
 * <p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DataSourceResource extends DataSource {

	/**
	 * 缺省构造器。
	 */
	public DataSourceResource() {
		super();
		this.setTitleFormat("Name");
		this.setValueFormat("Name");
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param directory
	 * @param unid
	 */
	public DataSourceResource(String directory, String unid) {
		super();
		this.m_directory = directory;
		m_UNID = unid;
	}

	private String m_directory = null; // 资源目录，必须。
	private String m_UNID = null; // 资源UNID，必须。
	private boolean m_fullTree = true; // 是否整个资源树。

	/**
	 * 返回资源目录，必须。
	 * 
	 * @return String
	 */
	public String getDirectory() {
		return this.m_directory;
	}

	/**
	 * 设置资源目录，必须。
	 * 
	 * @param directory String
	 */
	public void setDirectory(String directory) {
		this.m_directory = directory;
	}

	/**
	 * 返回资源别名或UNID，必须。
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置资源别名或UNID，必须。
	 * 
	 * @param UNID String
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回用于获取数据源标题的资源属性名称。
	 * 
	 * @see DataSourceResource#setTitlePropName(String)
	 * @return String
	 */
	public String getTitlePropName() {
		return this.getTitleFormat();
	}

	/**
	 * 返回用于获取数据源值的资源属性名称。
	 * 
	 * @see DataSourceResource#setValuePropName(String)
	 * @return String
	 */
	public String getValuePropName() {
		return this.getValueFormat();
	}

	/**
	 * 返回是否整个资源树可以作为数据源。
	 * 
	 * <p>
	 * 默认为true，表示所有下级资源都可以作为数据源。 <br/>
	 * 如果为false，则表示只有资源树的直接下级资源可以作为数据源。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getFullTree() {
		return this.m_fullTree;
	}

	/**
	 * 设置是否整个资源树可以作为数据源。
	 * 
	 * @param fullTree boolean
	 */
	public void setFullTree(boolean fullTree) {
		this.m_fullTree = fullTree;
	}

}

