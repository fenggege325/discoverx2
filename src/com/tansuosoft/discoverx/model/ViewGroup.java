/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 用于描述视图分组条件的类（查询配置项）。
 * 
 * <p>
 * 此对象由系统保留使用。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewGroup extends ViewQueryConfig implements java.io.Serializable {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -6812241954484226872L;

	/**
	 * 缺省构造器。
	 */
	public ViewGroup() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param tableName
	 * @param columnName
	 */
	public ViewGroup(String tableName, String columnName) {
		super(tableName, columnName);
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ViewGroup x = (ViewGroup) super.clone();

		return x;
	}
}

