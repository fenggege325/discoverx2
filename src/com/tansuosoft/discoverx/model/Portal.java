/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于描述门户信息的门户资源类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Portal extends Resource implements Cloneable {
	/**
	 * 默认页面框架名称。
	 * 
	 * <p>
	 * 系统必须至少有一个页面框架且名称为此处定义的名称。参考{@link #getFrame()}。
	 * </p>
	 */
	public static final String DEFAULT_FRAME_NAME = "frame";
	/**
	 * 页面框架下的默认主题名称。
	 * 
	 * <p>
	 * 每个页面框架必须至少有一个主题且名称为此处定义的名称。参考{@link #getTheme()}。
	 * </p>
	 */
	public static final String DEFAULT_THEME_NAME = "theme";
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -6210350592517471588L;

	private String m_frame = DEFAULT_FRAME_NAME; // 门户绑定的框架（相对）路径名称
	private String m_theme = DEFAULT_THEME_NAME; // 门户绑定的样式（相对）路径名称
	private String m_menu = null; // 门户绑定的菜单资源UNID。
	private List<Portlet> m_portlets = null; // 门户绑定的小窗口集合。
	private int m_columnCount = 3; // 最大栏目数
	private List<Integer> m_columnWidth = null; // 各栏目宽度。
	private String m_portalLevel1Js = null; // 门户首页额外引用的JS
	private String m_portalLevel2Js = null; // 门户二级页额外引用的JS
	private String m_portalLevel1Css = null; // 门户首页额外引用的CSS。
	private String m_portalLevel2Css = null; // 门户二级页额外引用的CSS。

	/**
	 * 缺省构造器。
	 */
	public Portal() {
		super();
		this.m_columnCount = 3;
		m_columnWidth = new ArrayList<Integer>(3);
		m_columnWidth.add(30);
		m_columnWidth.add(40);
		m_columnWidth.add(30);
	}

	/**
	 * 获取门户绑定的框架（相对）路径名称。
	 * 
	 * <p>
	 * 默认框架名称为：“frame”，参考{@link #DEFAULT_FRAME_NAME}。
	 * </p>
	 * 
	 * @return String
	 */
	public String getFrame() {
		return m_frame;
	}

	/**
	 * 设置门户绑定的框架（相对）路径名称
	 * 
	 * @param frame String
	 */
	public void setFrame(String frame) {
		this.m_frame = frame;
	}

	/**
	 * 获取门户绑定的样式主题（相对）路径名称。
	 * 
	 * <p>
	 * 默认样式主题名称为：“theme”，参考{@link #DEFAULT_THEME_NAME}。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTheme() {
		return m_theme;
	}

	/**
	 * 设置门户绑定的样式主题（相对）路径名称。
	 * 
	 * @param theme String
	 */
	public void setTheme(String theme) {
		this.m_theme = theme;
	}

	/**
	 * 获取门户绑定的菜单资源UNID。
	 * 
	 * @return String
	 */
	public String getMenu() {
		return m_menu;
	}

	/**
	 * 设置门户绑定的菜单资源UNID。
	 * 
	 * @param menu String
	 */
	public void setMenu(String menu) {
		this.m_menu = menu;
	}

	/**
	 * 获取门户绑定的小窗口集合
	 * 
	 * @return List<Portlet>
	 */
	public List<Portlet> getPortlets() {
		return m_portlets;
	}

	/**
	 * 设置门户绑定的小窗口集合
	 * 
	 * @param portlets void
	 */
	public void setPortlets(List<Portlet> portlets) {
		this.m_portlets = portlets;
	}

	/**
	 * 返回小窗口显示区最大栏目数。
	 * 
	 * <p>
	 * 默认为3栏。
	 * </p>
	 * 
	 * @return int
	 */
	public int getColumnCount() {
		return this.m_columnCount;
	}

	/**
	 * 设置小窗口显示最大栏目数。
	 * 
	 * @param columnCount int
	 */
	public void setColumnCount(int columnCount) {
		this.m_columnCount = columnCount;
	}

	/**
	 * 返回小窗口显示区各栏目宽度。
	 * 
	 * <p>
	 * 单位为百分比，默认为30、40、30。
	 * </p>
	 * 
	 * @return List<Integer>
	 */
	public List<Integer> getColumnWidth() {
		return this.m_columnWidth;
	}

	/**
	 * 设置小窗口显示区各栏目宽度。
	 * 
	 * @param columnWidth List<Integer>
	 */
	public void setColumnWidth(List<Integer> columnWidth) {
		this.m_columnWidth = columnWidth;
	}

	/**
	 * 返回门户首页额外引用的JS。
	 * 
	 * <p>
	 * 返回的js文件的url地址将在门户首页的body底部被自动引用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getPortalLevel1Js() {
		return this.m_portalLevel1Js;
	}

	/**
	 * 设置门户首页额外引用的JS。
	 * 
	 * <p>
	 * 配置时可以使用url变量，如“~”（表示应用根路径）、“{frame}”（表示当前页面框架路径）。
	 * </p>
	 * 
	 * @param portalLevel1Js String
	 */
	public void setPortalLevel1Js(String portalLevel1Js) {
		this.m_portalLevel1Js = portalLevel1Js;
	}

	/**
	 * 返回门户二级页额外引用的JS。
	 * 
	 * <p>
	 * 返回的js文件的url地址将在门户二级页面的body底部被自动引用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getPortalLevel2Js() {
		return this.m_portalLevel2Js;
	}

	/**
	 * 设置门户二级页额外引用的JS。
	 * 
	 * <p>
	 * 配置时可以使用url变量，如“~”（表示应用根路径）、“{frame}”（表示当前页面框架路径）。
	 * </p>
	 * 
	 * @param portalLevel2Js String
	 */
	public void setPortalLevel2Js(String portalLevel2Js) {
		this.m_portalLevel2Js = portalLevel2Js;
	}

	/**
	 * 返回门户首页额外引用的CSS。
	 * 
	 * <p>
	 * 返回的css文件的url地址将在门户首页的head区被自动引用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getPortalLevel1Css() {
		return this.m_portalLevel1Css;
	}

	/**
	 * 设置门户首页额外引用的CSS。
	 * 
	 * <p>
	 * 配置时可以使用url变量，如“~”（表示应用根路径）、“{frame}”（表示当前页面框架路径）。
	 * </p>
	 * 
	 * @param portalLevel1Css String
	 */
	public void setPortalLevel1Css(String portalLevel1Css) {
		this.m_portalLevel1Css = portalLevel1Css;
	}

	/**
	 * 返回门户二级页额外引用的CSS。
	 * 
	 * <p>
	 * 返回的css文件的url地址将在二级页面的head区被自动引用。
	 * </p>
	 * 
	 * @return String
	 */
	public String getPortalLevel2Css() {
		return this.m_portalLevel2Css;
	}

	/**
	 * 设置门户二级页额外引用的CSS。
	 * 
	 * <p>
	 * 配置时可以使用url变量，如“~”（表示应用根路径）、“{frame}”（表示当前页面框架路径）。
	 * </p>
	 * 
	 * @param portalLevel2Css String
	 */
	public void setPortalLevel2Css(String portalLevel2Css) {
		this.m_portalLevel2Css = portalLevel2Css;
	}

	/**
	 * 根据小窗口UNID获取门户包含的对应小窗口对象。
	 * 
	 * @param portletUNID
	 * @return Portlet 如果找不到，则返回null。
	 */
	public Portlet getPortletByUNID(String portletUNID) {
		if (this.m_portlets == null || this.m_portlets.isEmpty() || portletUNID == null || portletUNID.length() == 0) return null;
		for (Portlet x : this.m_portlets) {
			if (portletUNID.equalsIgnoreCase(x.getUNID())) return x;
		}
		return null;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Portal x = (Portal) super.clone();

		x.setColumnCount(this.getColumnCount());
		x.setFrame(this.getFrame());
		x.setMenu(this.getMenu());
		x.setTheme(this.getTheme());
		x.setPortalLevel1Js(this.getPortalLevel1Js());
		x.setPortalLevel2Js(this.getPortalLevel2Js());
		x.setPortalLevel1Css(this.getPortalLevel1Css());
		x.setPortalLevel2Css(this.getPortalLevel2Css());

		x.setPortlets(Resource.cloneList(this.getPortlets()));

		List<Integer> cw = this.getColumnWidth();
		if (cw != null) {
			List<Integer> cwnew = new ArrayList<Integer>();
			for (Integer y : this.getColumnWidth()) {
				if (y == null) continue;
				cwnew.add(y.intValue());
			}
			x.setColumnWidth(cwnew);
		}

		return x;
	}
}

