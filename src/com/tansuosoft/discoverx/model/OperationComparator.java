/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.Comparator;

/**
 * {@link com.tansuosoft.discoverx.model.Operation}对象的排序比较实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OperationComparator implements Comparator<Operation> {
	/**
	 * 缺省构造器。
	 */
	public OperationComparator() {
	}

	/**
	 * 重载compare：比较它们的sort结果，如果sort相同，则比较标题。
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Operation o1, Operation o2) {
		int result = 0;
		if (o1 == null && o2 == null) return 0;
		if (o1 == null && o2 != null) return -1;
		if (o1 != null && o2 == null) return 1;
		int sort1 = o1.getSort();
		int sort2 = o2.getSort();
		result = sort1 - sort2;
		if (result == 0) {
			String title1 = o1.getTitle();
			String title2 = o2.getTitle();
			if (title1 == null) title1 = "";
			if (title2 == null) title2 = "";
			result = title1.compareTo(title2);
			if (result == 0) {
				int x = (o1.hashCode() - o2.hashCode());
				return (x > 0 ? 1 : (x < 0 ? -1 : x));
			}
		}
		return result;
	}
}

