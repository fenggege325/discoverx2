/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示定义字段分组相关属性的类。
 * 
 * <p>
 * 分组类型的字段的{@link Item#getType()}总是返回{@link ItemType#Group}。<br/>
 * 某些属性在分组字段中可能有不同含义或者被忽略。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemGroup extends Item implements Cloneable {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 7827552453425233855L;

	/**
	 * 缺省构造器。
	 */
	public ItemGroup() {
	}

	private ItemGroupType m_groupType = ItemGroupType.Table; // 字段组合的类型。
	private int m_minCount = 0; // 分组最低条目数，默认为0（表示不是必需的）。
	private int m_maxCount = 0; // 分组最低条目数，默认为0（表示不限制上限）。
	private SaveToTableOption m_saveToTableOption = SaveToTableOption.NoSave; // 是否在文档保存时同步(插入、更新或删除)字段组信息到独立的数据库表。
	private String m_createTableImplement = null; // 建表实现类。
	private String m_saveToTableImplement = null; // 保存记录实现类。
	private String m_removeFromTableImplement = null; // 删除记录实现类。

	/**
	 * 返回分组名称。
	 * 
	 * <p>
	 * 分组名称即{@link Item#getName()}的返回结果。<br/>
	 * 在同一个表单中配置的分组名称必须唯一。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTitle() {
		return this.getName();
	}

	/**
	 * 返回字段组合的方式。
	 * 
	 * <p>
	 * 默认为{@link ItemGroupType#Table}（表格方式）。
	 * </p>
	 * <p>
	 * <strong>如果类型为{@link ItemGroupType#Control}，则此字段组的行属性必须为0！</strong>
	 * </p>
	 * 
	 * @see ItemGroupType
	 * @return ItemGroupType
	 */
	public ItemGroupType getGroupType() {
		return this.m_groupType;
	}

	/**
	 * 设置字段组合的方式。
	 * 
	 * @see ItemGroup#getGroupType()
	 * @param groupType ItemGroupType
	 */
	public void setGroupType(ItemGroupType groupType) {
		this.m_groupType = groupType;
	}

	/**
	 * 返回分组最低条目数，默认为0（表示不是必需的）。
	 * 
	 * <p>
	 * 分组条目数指的是指定分组，可以在文档中出现几次。对于表格型的，表示表格至少或至多有几行，对于段落型的，表示至少或至多有几个段落。
	 * </p>
	 * <p>
	 * 为0表示此分组可有可无，如果设置为1，则表示至少要求有一行或一段，依此类推。
	 * </p>
	 * <p>
	 * <strong>用于控制一组字段行为的分组（即GroupType属性值为ItemGroupType.Behaviour的分组）类型忽略此属性。</strong>
	 * </p>
	 * 
	 * @return int
	 */
	public int getMinCount() {
		return this.m_minCount;
	}

	/**
	 * 设置分组最低条目数，默认为0（表示不是必需的）。
	 * 
	 * @see ItemGroup#getMinCount()
	 * @param minCount int
	 */
	public void setMinCount(int minCount) {
		this.m_minCount = minCount;
	}

	/**
	 * 返回分组最低条目数，默认为0（表示不限制上限）。
	 * 
	 * <p>
	 * 0表示此分组条目数不设置上限，其它数字则表示数字指定的上限（比如表格型的分组即表示最多有几行）。
	 * </p>
	 * <p>
	 * <strong>用于控制一组字段行为的分组（即GroupType属性值为ItemGroupType.Behaviour的分组）类型忽略此属性。</strong>
	 * </p>
	 * 
	 * @see ItemGroup#getMinCount()
	 * @return int
	 */
	public int getMaxCount() {
		return this.m_maxCount;
	}

	/**
	 * 设置分组最低条目数，默认为0（表示不限制上限）。
	 * 
	 * @see ItemGroup#getMaxCount()
	 * @param maxCount int
	 */
	public void setMaxCount(int maxCount) {
		this.m_maxCount = maxCount;
	}

	/**
	 * 重载getRequired：返回是否必须的分组。
	 * 
	 * <p>
	 * 即getMinCount返回值是否大于0。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.model.Item#getRequired()
	 */
	@Override
	public boolean getRequired() {
		return (this.m_minCount > 0);
	}

	/**
	 * 重载getType:总是返回“{@link ItemType#Group}”。
	 * 
	 * @see com.tansuosoft.discoverx.model.Item#getType()
	 */
	@Override
	public ItemType getType() {
		return ItemType.Group;
	}

	/**
	 * 返回是否在文档保存时同步(插入、更新或删除)字段组信息到独立的数据库表。
	 * 
	 * <p>
	 * 默认为{@link SaveToTableOption#NoSave}。
	 * </p>
	 * 
	 * @return SaveToTableOption
	 */
	public SaveToTableOption getSaveToTableOption() {
		return this.m_saveToTableOption;
	}

	/**
	 * 设置是否在文档保存时同步(插入、更新或删除)字段组信息到独立的数据库表。
	 * 
	 * @param saveToTable boolean
	 */
	public void setSaveToTableOption(SaveToTableOption saveToTableOption) {
		this.m_saveToTableOption = saveToTableOption;
	}

	/**
	 * 返回建表实现类。
	 * 
	 * <p>
	 * {@link #getSaveToTableOption()}返回非{@link SaveToTableOption#NoSave}时才有意义，如果不配置系统会使用默认值。
	 * </p>
	 * 
	 * @return String
	 */
	public String getCreateTableImplement() {
		return this.m_createTableImplement;
	}

	/**
	 * 设置建表实现类。
	 * 
	 * <p>
	 * {@link #getSaveToTableOption()}返回非{@link SaveToTableOption#NoSave}时才有意义，如果不配置系统会使用默认值。
	 * </p>
	 * 
	 * @param createTableImplement String
	 */
	public void setCreateTableImplement(String createTableImplement) {
		this.m_createTableImplement = createTableImplement;
	}

	/**
	 * 返回保存记录实现类。
	 * 
	 * <p>
	 * {@link #getSaveToTableOption()}返回非{@link SaveToTableOption#NoSave}时才有意义，如果不配置系统会使用默认值。
	 * </p>
	 * 
	 * @return String
	 */
	public String getSaveToTableImplement() {
		return this.m_saveToTableImplement;
	}

	/**
	 * 设置保存记录实现类。
	 * 
	 * <p>
	 * {@link #getSaveToTableOption()}返回非{@link SaveToTableOption#NoSave}时才有意义，如果不配置系统会使用默认值。
	 * </p>
	 * 
	 * @param saveToTableImplement String
	 */
	public void setSaveToTableImplement(String saveToTableImplement) {
		this.m_saveToTableImplement = saveToTableImplement;
	}

	/**
	 * 返回删除记录实现类。
	 * 
	 * <p>
	 * {@link #getSaveToTableOption()}返回非{@link SaveToTableOption#NoSave}时才有意义，如果不配置系统会使用默认值。
	 * </p>
	 * 
	 * @return String
	 */
	public String getRemoveFromTableImplement() {
		return this.m_removeFromTableImplement;
	}

	/**
	 * 设置删除记录实现类。
	 * 
	 * <p>
	 * {@link #getSaveToTableOption()}返回非{@link SaveToTableOption#NoSave}时才有意义，如果不配置系统会使用默认值。
	 * </p>
	 * 
	 * @param removeFromTableImplement String
	 */
	public void setRemoveFromTableImplement(String removeFromTableImplement) {
		this.m_removeFromTableImplement = removeFromTableImplement;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ItemGroup x = (ItemGroup) super.clone();

		x.setMinCount(this.getMinCount());
		x.setMaxCount(this.getMaxCount());
		x.setGroupType(this.getGroupType());
		x.setSaveToTableOption(this.getSaveToTableOption());
		x.setCreateTableImplement(this.getCreateTableImplement());
		x.setSaveToTableImplement(this.getSaveToTableImplement());
		x.setRemoveFromTableImplement(this.getRemoveFromTableImplement());

		return x;
	}
}

