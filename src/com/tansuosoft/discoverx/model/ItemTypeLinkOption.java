/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 字段值数据类型为链接额外选项类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemTypeLinkOption extends ItemTypeOption {

	/**
	 * 缺省构造器。
	 */
	public ItemTypeLinkOption() {
	}

	private String m_target = "_blank"; // 链接打开的目标窗口。

	/**
	 * 返回链接打开的目标窗口。
	 * 
	 * <p>
	 * 默认为“_blank”，表示在新窗口中打开链接。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTarget() {
		return this.m_target;
	}

	/**
	 * 设置链接打开的目标窗口。
	 * 
	 * @param target String
	 */
	public void setTarget(String target) {
		this.m_target = target;
	}
}

