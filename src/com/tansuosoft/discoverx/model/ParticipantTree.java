/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 表示系统参与者信息树的类。
 * 
 * <p>
 * 它以树型数据结构保存包括系统所有用户（{@link Participant}）、组织（{@link Organization}）、群组（{@link Group}）、角色（{@link Role}）等资源的所有公共属性数据（Participant对象）。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ParticipantTree extends Participant implements Comparable<ParticipantTree> {
	/**
	 * 系统参与者树默认根节点的UNID，与{@link Organization#ROOT_ORGANIZATION_UNID}相同。
	 */
	public static final String ROOT_UNID = Organization.ROOT_ORGANIZATION_UNID;
	/**
	 * 系统参与者树根节点的名称。
	 */
	public static final String ROOT_NAME = CommonConfig.getInstance().getBelong();
	/**
	 * 系统参与者树根节点的别名。
	 */
	public static final String ROOT_ALIAS = "participanttreeroot";
	/**
	 * 系统默认参与者树根节点（系统根组织）的安全编码（2）。
	 */
	public static final int ROOT_CODE = 2;
	/**
	 * 系统参与者树根节点的级别（1）。
	 */
	public static final int ROOT_LEVEL = 1;

	/**
	 * 系统群组参与者树根节点的UNID。
	 */
	public static final String GROUP_ROOT_UNID = "0447F2C80BB32A834B143D2687C6B35E";
	/**
	 * 系统群组参与者树根节点的名称。
	 */
	public static final String GROUP_ROOT_NAME = "[群组]";
	/**
	 * 系统群组参与者树根节点的别名。
	 */
	public static final String GROUP_ROOT_ALIAS = "participanttreegrouproot";
	/**
	 * 系统群组参与者树根节点的安全编码（-2）。
	 */
	public static final int GROUP_ROOT_CODE = -2;

	/**
	 * 系统角色参与者树根节点的UNID。
	 */
	public static final String ROLE_ROOT_UNID = "0AFD9E55781FA82554AA1BE02A7C4AB3";
	/**
	 * 系统群组参与者树根节点的名称。
	 */
	public static final String ROLE_ROOT_NAME = "[角色]";
	/**
	 * 系统角色参与者树根节点的别名。
	 */
	public static final String ROLE_ROOT_ALIAS = "participanttreeroleroot";
	/**
	 * 系统角色参与者树根节点的安全编码（-1）。
	 */
	public static final int ROLE_ROOT_CODE = -1;

	/**
	 * 系统流程参与者树根节点的安全编码（-3）。
	 */
	public static final int WORKFLOW_ROOT_CODE = -3;

	private ParticipantTree parent = null; // 所属直接上级节点。
	private List<ParticipantTree> children = null; // 下属子节点。
	private boolean childrenSorted = false;
	private String m_sortId = null; // 排序依据
	private int m_level = 0; // 所在级别数。
	private int m_sort = Integer.MAX_VALUE; // 排序号。

	/**
	 * 缺省构造器。
	 */
	public ParticipantTree() {
		super();
	}

	/**
	 * 缺省构造器。
	 * 
	 * @param securityCode
	 * @param name
	 * @param unid
	 * @param alias
	 * @param participantType
	 * @param selected
	 */
	public ParticipantTree(int securityCode, String name, String unid, String alias, ParticipantType participantType, boolean selected) {
		super(securityCode, name, unid, alias, participantType, selected);
	}

	/**
	 * 构造器。
	 * 
	 * @param securityCode
	 * @param name
	 * @param unid
	 * @param alias
	 * @param participantType
	 */
	public ParticipantTree(int securityCode, String name, String unid, String alias, ParticipantType participantType) {
		super(securityCode, name, unid, alias, participantType);
	}

	/**
	 * 构造器。
	 * 
	 * @param securityCode
	 * @param name
	 * @param unid
	 * @param alias
	 * @param participantType
	 * @param level
	 * @param sortId
	 * @param sort
	 * @param parent
	 */
	public ParticipantTree(int securityCode, String name, String unid, String alias, ParticipantType participantType, int level, String sortId, int sort, ParticipantTree parent) {
		super(securityCode, name, unid, alias, participantType);
		this.m_level = level;
		this.m_sortId = sortId;
		this.m_sort = sort;
		this.parent = parent;
	}

	/**
	 * 构造器。
	 * 
	 * @param securityCode
	 * @param name
	 * @param unid
	 * @param alias
	 * @param participantType
	 * @param level
	 * @param sortId
	 * @param sort
	 */
	public ParticipantTree(int securityCode, String name, String unid, String alias, ParticipantType participantType, int level, String sortId, int sort) {
		super(securityCode, name, unid, alias, participantType);
		this.m_level = level;
		this.m_sortId = sortId;
		this.m_sort = sort;
	}

	/**
	 * 构造器。
	 * 
	 * @param securityCode
	 * @param name
	 * @param unid
	 * @param alias
	 */
	public ParticipantTree(int securityCode, String name, String unid, String alias) {
		super(securityCode, name, unid, alias);
	}

	/**
	 * 构造器。
	 * 
	 * @param p
	 */
	public ParticipantTree(Participant p) {
		if (p == null) return;
		this.setSecurityCode(p.getSecurityCode());
		this.setName(p.getName());
		this.setAlias(p.getAlias());
		this.setParticipantType(p.getParticipantType());
		this.setUNID(p.getUNID());
	}

	/**
	 * 构造器。
	 * 
	 * @param s
	 */
	public ParticipantTree(Securer s) {
		super(s);
	}

	/**
	 * 返回所属直接上级节点。
	 * 
	 * @return ParticipantTree
	 */
	public ParticipantTree getParent() {
		return this.parent;
	}

	/**
	 * 设置所属直接上级节点。
	 * 
	 * @param parent ParticipantTree
	 */
	public void setParent(ParticipantTree parent) {
		this.parent = parent;
	}

	/**
	 * 返回下属子节点。
	 * 
	 * @return List&lt;ParticipantTree&gt;
	 */
	public List<ParticipantTree> getChildren() {
		if (!childrenSorted) {
			childrenSorted = true;
			if (children != null) Collections.sort(children);
		}
		return this.children;
	}

	/**
	 * 设置下属子节点，可选。
	 * 
	 * @param children List&lt;ParticipantTree&gt;
	 */
	public void setChildren(List<ParticipantTree> children) {
		childrenSorted = false;
		this.children = children;
	}

	private ParticipantTree m_groupRoot = null;
	private ParticipantTree m_roleRoot = null;

	/**
	 * 追加一个子节点。
	 * 
	 * @param child ParticipantTree
	 * @return boolean
	 */
	public boolean addChild(ParticipantTree child) {
		if (child == null) return false;

		if (this.getParticipantType() == ParticipantType.Root) {
			ParticipantTree parent = null;
			if (child.getParticipantType() == ParticipantType.Group) {
				if (m_groupRoot == null) {
					m_groupRoot = new ParticipantTree(ParticipantTree.GROUP_ROOT_CODE, ParticipantTree.GROUP_ROOT_NAME, ParticipantTree.GROUP_ROOT_UNID, ParticipantTree.GROUP_ROOT_ALIAS, ParticipantType.Group, false);
					if (this.children == null) this.children = new ArrayList<ParticipantTree>();
					m_groupRoot.setParent(this);
					this.children.add(m_groupRoot);
				}
				parent = m_groupRoot;
			} else if (child.getParticipantType() == ParticipantType.Role) {
				if (m_roleRoot == null) {
					m_roleRoot = new ParticipantTree(ParticipantTree.ROLE_ROOT_CODE, ParticipantTree.ROLE_ROOT_NAME, ParticipantTree.ROLE_ROOT_UNID, ParticipantTree.ROLE_ROOT_ALIAS, ParticipantType.Role, false);
					if (this.children == null) this.children = new ArrayList<ParticipantTree>();
					m_roleRoot.setParent(this);
					this.children.add(m_roleRoot);
				}
				parent = m_roleRoot;
			}
			if (parent != null) {
				List<ParticipantTree> children = parent.getChildren();
				if (children == null) {
					children = new ArrayList<ParticipantTree>();
					parent.setChildren(children);
				}
				child.setParent(parent);
				return children.add(child);
			}
		}

		if (this.children == null) this.children = new ArrayList<ParticipantTree>();
		child.setParent(this);
		childrenSorted = false;
		return this.children.add(child);
	}

	/**
	 * 返回排序依据
	 * 
	 * @return String
	 */
	public String getSortId() {
		return this.m_sortId;
	}

	/**
	 * 设置排序依据
	 * 
	 * @param sortId String
	 */
	public void setSortId(String sortId) {
		this.m_sortId = sortId;
	}

	/**
	 * 返回所在级别数。
	 * 
	 * <p>
	 * 根节点为1，根下的第一级为2，依此类推。
	 * </p>
	 * 
	 * @return int
	 */
	public int getLevel() {
		if (m_level <= 0 && !StringUtil.isBlank(this.getName())) {
			m_level = StringUtil.splitString(this.getName(), User.SEPARATOR_CHAR).length;
		}
		return this.m_level;
	}

	/**
	 * 返回排序号。
	 * 
	 * @return int
	 */
	public int getSort() {
		return this.m_sort;
	}

	/**
	 * 设置排序号。
	 * 
	 * @param sort int
	 */
	public void setSort(int sort) {
		this.m_sort = sort;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ParticipantTree x = new ParticipantTree();

		x.setSelected(this.getSelected());
		x.setSecurityCode(this.getSecurityCode());
		x.setSort(this.getSort());
		x.setAlias(this.getAlias());
		x.setName(this.getName());
		x.setSortId(this.getSortId());
		x.setUNID(this.getUNID());
		x.setParticipantType(this.getParticipantType());
		x.setChildren(Resource.cloneList(this.getChildren()));
		x.setParent(this.getParent());

		return x;
	}

	/**
	 * 重载：输出全名树列表。
	 * 
	 * @see com.tansuosoft.discoverx.model.Participant#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int l = this.getLevel();
		for (int i = 1; i < l; i++) {
			sb.append("-");
		}
		sb.append(this.getName()).append("\r\n");
		List<ParticipantTree> list = this.getChildren();
		if (list != null) {
			for (ParticipantTree x : list) {
				if (x == null) continue;
				sb.append(x.toString());
			}
		}
		return sb.toString();
	}

	/**
	 * 重载：实现功能。
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ParticipantTree o) {
		if (o == null) return 1;
		int sort1 = this.getSort();
		int sort2 = o.getSort();
		int r = sort1 - sort2;
		if (r == 0) r = (this.getName() == null ? 0 : this.getName().compareTo(o.getName()));
		if (r == 0) r = (this.getUNID() == null ? 0 : this.getUNID().compareTo(o.getUNID()));
		return r;
	}
}
