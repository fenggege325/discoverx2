/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示云平台上一个使用单位相关信息的资源。
 * 
 * @author coca@tensosoft.com
 * 
 */
public class Enterprise extends Resource {

	/**
	 * 序列化id。
	 */
	private static final long serialVersionUID = -5387302191480794073L;

	private int m_id = 0; // 单位Id。
	private int m_from = 0; // 推荐单位的Id。
	private String m_resourcePath = null; // 资源目录名。
	private String m_accessoryPath = null; // 附加文件目录名。
	private String m_dbName = null; // 使用的目标数据库名。

	/**
	 * 返回唯一单位Id。
	 * 
	 * @return int
	 */
	public int getId() {
		return this.m_id;
	}

	/**
	 * 设置唯一单位Id。
	 * 
	 * @param id int
	 */
	public void setId(int id) {
		this.m_id = id;
	}

	/**
	 * 返回推荐单位的Id。
	 * 
	 * @return int
	 */
	public int getFrom() {
		return this.m_from;
	}

	/**
	 * 设置推荐单位的Id。
	 * 
	 * @param from int
	 */
	public void setFrom(int from) {
		this.m_from = from;
	}

	/**
	 * 返回资源目录名。
	 * 
	 * @return String
	 */
	public String getResourcePath() {
		return this.m_resourcePath;
	}

	/**
	 * 设置资源目录名。
	 * 
	 * @param resourcePath String
	 */
	public void setResourcePath(String resourcePath) {
		this.m_resourcePath = resourcePath;
	}

	/**
	 * 返回附加文件目录名。
	 * 
	 * @return String
	 */
	public String getAccessoryPath() {
		return this.m_accessoryPath;
	}

	/**
	 * 设置附加文件目录名。
	 * 
	 * @param accessoryPath String
	 */
	public void setAccessoryPath(String accessoryPath) {
		this.m_accessoryPath = accessoryPath;
	}

	/**
	 * 返回使用的目标数据库名。
	 * 
	 * @return String
	 */
	public String getDbName() {
		return this.m_dbName;
	}

	/**
	 * 设置使用的目标数据库名。
	 * 
	 * @param dbName String
	 */
	public void setDbName(String dbName) {
		this.m_dbName = dbName;
	}
}

