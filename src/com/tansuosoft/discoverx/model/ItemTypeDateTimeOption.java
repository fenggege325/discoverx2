/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 字段值数据类型为日期时间类型的额外选项类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemTypeDateTimeOption extends ItemTypeOption {

	/**
	 * 缺省构造器。
	 */
	public ItemTypeDateTimeOption() {
	}

	private String m_titleFormat = "yyyy-MM-dd HH:mm:ss";
	private String m_valueFormat = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 返回标题格式。
	 * 
	 * <p>
	 * 默认为“yyyy-MM-dd HH:mm:ss”。<br/>
	 * 其它合法格式请参考“{@link com.tansuosoft.discoverx.util.DateTime}”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTitleFormat() {
		return this.m_titleFormat;
	}

	/**
	 * 设置标题格式。
	 * 
	 * @param format String
	 */
	public void setTitleFormat(String format) {
		this.m_titleFormat = format;
	}

	/**
	 * 返回结果格式。
	 * 
	 * <p>
	 * 默认为“yyyy-MM-dd HH:mm:ss”。<br/>
	 * 其它合法格式请参考“{@link com.tansuosoft.discoverx.util.DateTime}”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getValueFormat() {
		return this.m_valueFormat;
	}

	/**
	 * 设置结果格式。
	 * 
	 * @param format String
	 */
	public void setValueFormat(String format) {
		this.m_valueFormat = format;
	}
}

