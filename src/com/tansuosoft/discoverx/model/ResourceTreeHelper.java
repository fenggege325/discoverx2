/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.List;

/**
 * 资源树实用工具类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ResourceTreeHelper {
	/**
	 * 私有构造器。
	 */
	private ResourceTreeHelper() {
	}

	/**
	 * 从root指定的树资源中获取与unid指定的资源匹配的资源对象（本级或下级）。
	 * 
	 * @param root
	 * @param unid
	 * @return Resource
	 */
	public static Resource getChildByUNID(Resource root, String unid) {
		if (root == null || unid == null || unid.length() != 32) return null;

		if (unid.equalsIgnoreCase(root.getUNID())) return root;
		List<? extends Resource> list = root.getChildren();
		if (list == null || list.size() == 0) return null;
		Resource ret = null;
		for (Resource x : list) {
			ret = getChildByUNID(x, unid);
			if (ret != null) return ret;
		}
		return null;
	}

	/**
	 * 从root指定的树资源中获取与别名指定的资源匹配的资源对象（本级或下级）。
	 * 
	 * @param root
	 * @param alias
	 * @return Resource
	 */
	public static Resource getChildByAlias(Resource root, String alias) {
		if (root == null || alias == null || alias.length() == 0) return null;
		if (alias.equalsIgnoreCase(root.getAlias())) return root;
		List<? extends Resource> list = root.getChildren();
		if (list == null || list.size() == 0) return null;
		Resource ret = null;
		for (Resource x : list) {
			ret = getChildByAlias(x, alias);
			if (ret != null) return ret;
		}
		return null;
	}

	/**
	 * 从root指定的树资源中获取与名称指定的资源匹配的第一个资源对象（本级或下级）。
	 * 
	 * @param root
	 * @param name
	 * @return Resource
	 */
	public static Resource getFirstChildByName(Resource root, String name) {
		if (root == null || name == null || name.length() == 0) return null;
		if (name.equalsIgnoreCase(root.getAlias())) return root;
		List<? extends Resource> list = root.getChildren();
		if (list == null || list.size() == 0) return null;
		Resource ret = null;
		for (Resource x : list) {
			ret = getFirstChildByName(x, name);
			if (ret != null) return ret;
		}
		return null;
	}

	/**
	 * 从root指定的树资源中获取与完整路径名称指定的资源匹配的资源对象（本级或下级）。
	 * 
	 * <p>
	 * 需先针对根资源调用其{@link Resource#setPath(String, String)}方法才能使用。
	 * </p>
	 * 
	 * @param root
	 * @param path
	 * @return Resource
	 */
	public static Resource getChildByPath(Resource root, String path) {
		if (root == null || path == null || path.length() == 0) return null;
		if (path.equalsIgnoreCase(root.getPath())) return root;
		List<? extends Resource> list = root.getChildren();
		if (list == null || list.size() == 0) return null;
		Resource ret = null;
		for (Resource x : list) {
			ret = getChildByPath(x, path);
			if (ret != null) return ret;
		}
		return null;
	}

	/**
	 * 从root中删除id（可以是unid或别名）对应的下级资源。
	 * 
	 * @param root
	 * @param id
	 * @return
	 */
	public static Resource removeChild(Resource root, String id) {
		if (root == null || id == null || id.length() == 0) return null;
		List<? extends Resource> list = root.getChildren();
		if (list == null || list.size() == 0) return null;
		Resource x = null;
		for (int i = 0; i < list.size(); i++) {
			x = list.get(i);
			if (x == null) continue;
			if (id.equalsIgnoreCase(x.getUNID()) || id.equalsIgnoreCase(x.getAlias())) {
				list.remove(i);
				return x;
			}
			x = removeChild(x, id);
			if (x != null) return x;
		}
		return null;
	}

	/**
	 * 获取root指定的树状资源包含的下级同类资源的最大级数。
	 * 
	 * @param root
	 * @param idx int-初始级别计数，一般指定为0。
	 * @return int
	 */
	public static int getMaxLevel(Resource root, int idx) {
		if (root == null) return 0;
		int ret = idx;
		List<? extends Resource> list = root.getChildren();
		if (list == null || list.isEmpty()) return 0;
		int i = 0;
		for (Resource x : list) {
			i = getMaxLevel(x, (idx + 1));
			ret = (ret < i ? i : ret);
		}
		return ret;
	}

}// class end

