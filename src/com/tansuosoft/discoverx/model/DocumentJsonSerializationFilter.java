/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.serialization.SerializationFilter;

/**
 * 文档序列化为json时使用的{@link SerializationFilter}实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentJsonSerializationFilter implements SerializationFilter {

	/**
	 * 重载filter
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.SerializationFilter#filter(java.lang.String)
	 */
	@Override
	public boolean filter(String propName) {
		if (propName == null) return false;
		if (propName.equalsIgnoreCase("security") || propName.equalsIgnoreCase("accessories") || propName.equalsIgnoreCase("logs") || propName.equalsIgnoreCase("opinions")) return false;
		return true;
	}

}

