/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.Comparator;

/**
 * 默认使用的资源引用排序比较器类。
 * 
 * <p>
 * 按资源引用的排序号、标题、UNID升序排序(确保顺序每次都不变)。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ReferenceComparatorDefault implements Comparator<Reference> {
	private RuleBasedCollator collator = null;

	/**
	 * 缺省构造器。
	 */
	public ReferenceComparatorDefault() {
		collator = (RuleBasedCollator) Collator.getInstance(java.util.Locale.CHINA);
	}

	/**
	 * 重载compare：默认按资源引用的排序号、标题、UNID升序排序(确保顺序每次都不变)。
	 * 
	 * <p>
	 * 如果属性值为空的，不管是升序还是降序都排到最后面。
	 * </p>
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Reference o1, Reference o2) {
		int result = 0;

		int sort1 = o1.getSort();
		int sort2 = o2.getSort();
		result = sort1 - sort2;
		if (result != 0) return result;
		// 如果sort相等,按title的升序排列
		String value1 = o1.getTitle();
		String value2 = o2.getTitle();
		if (value1 == null || value2 == null) { // 如果是空值总是排在后面
			result = (value1 == null ? 1 : -1);
		} else if (value1 != null && value1.equalsIgnoreCase(value2)) {
			result = 0;
		} else {// 升序
			result = collator.compare(value1, value2);
		}
		// 如果两条相等，则按照unid排类，以保证排序条件得出的结果的情况下，顺序一样
		if (result == 0) {
			result = o1.getUnid().compareTo(o2.getUnid());
		}
		return result;
	}

}

