/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示html节点的属性名和属性值配对信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class HtmlAttribute implements Cloneable {
	/**
	 * 表示文档呈现时html页面head中引用额外样式文件的属性名。
	 * 
	 * <p>
	 * 其属性值必须配置为有效的css文件引用url路径。<br/>
	 * 配置的值可以带上级路径如：“{frame}app_app1/special.css”或者（如果文件在应用程序特定功能目录下则可以）不带路径，如“special.css”。
	 * </p>
	 */
	public static final String CSS_ATTRIBUTE_NAME = "css";
	/**
	 * 表示文档呈现时html页面中引用额外js文件的属性名。
	 * 
	 * <p>
	 * 其属性值必须配置为有效的js文件引用url路径。<br/>
	 * 配置的值可以带上级路径如：“{frame}app_app1/special.js”或者（如果文件在应用程序特定功能目录下则可以）不带路径，如“special.js”。
	 * </p>
	 */
	public static final String JS_ATTRIBUTE_NAME = "js";
	/**
	 * 表示档呈现时html页面head节点中的meta属性名的前缀；
	 * 
	 * <p>
	 * 其属性值必须配置为有效的meta值，如“http-equiv="content-type" content="text/html; charset=GBK"”。
	 * </p>
	 * <p>
	 * 多个meta值请用meta1,meta2...等顺序配置。
	 * </p>
	 */
	public static final String META_ATTRIBUTE_NAME = "meta";

	/**
	 * 表示字段呈现时提供自定义选择框信息的属性名。
	 * 
	 * <p>
	 * 其属性值必须配置为有效的json对象，如“{datasource:'p://user',value:'sc',label:'cn'}”。
	 * </p>
	 * <p>
	 * 用于给需要的字段配置选择框标记，用以方便用户选择或输入字段值，此配置项的优先级低于字段内置的选择框配置。
	 * </p>
	 */
	public static final String SELECTOR_ATTRIBUTE_NAME = "selector";

	/**
	 * 缺省构造器。
	 */
	public HtmlAttribute() {
	}

	private String m_name = null;
	private String m_value = null;

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param name
	 * @param value
	 */
	public HtmlAttribute(String name, String value) {
		this.m_name = name;
		this.m_value = value;
	}

	/**
	 * 返回属性名。
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置属性名，必须。
	 * 
	 * <p>
	 * 必须是对应html节点支持的有效属性名。
	 * </p>
	 * 
	 * @param name String
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回属性值。
	 * 
	 * @return String
	 */
	public String getValue() {
		return this.m_value;
	}

	/**
	 * 设置属性值，必须。
	 * 
	 * <p>
	 * 必须是对应html节点支持的有效属性值，否则可能导致错误。
	 * </p>
	 * 
	 * @param value String
	 */
	public void setValue(String value) {
		this.m_value = value;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		HtmlAttribute x = new HtmlAttribute();
		x.setName(this.getName());
		x.setValue(this.getValue());
		return x;
	}
}

