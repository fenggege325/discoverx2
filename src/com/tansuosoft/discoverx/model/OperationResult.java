/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示http请求被后台操作（{@link com.discoverx.bll.function.Operation}）处理完成后返回的包含处理结果信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class OperationResult {
	/**
	 * 缺省构造器。
	 */
	public OperationResult() {
	}

	/**
	 * 接收相关属性的构造器。
	 * 
	 * @param status
	 * @param resultUrl
	 * @param isForward
	 */
	public OperationResult(int status, String resultUrl, boolean isForward) {
		this.m_status = status;
		this.m_resultUrl = resultUrl;
		this.m_isForward = isForward;
	}

	/**
	 * 接收相关属性的构造器。
	 * 
	 * @param resultUrl
	 */
	public OperationResult(String resultUrl) {
		this.m_status = 0;
		this.m_resultUrl = resultUrl;
		this.m_isForward = false;
	}

	/**
	 * 接收相关属性的构造器。
	 * 
	 * @param status
	 * @param resultUrl
	 */
	public OperationResult(int status, String resultUrl) {
		this.m_status = status;
		this.m_resultUrl = resultUrl;
		this.m_isForward = false;
	}

	/**
	 * 接收相关属性的构造器。
	 * 
	 * @param resultUrl
	 * @param isForward
	 */
	public OperationResult(String resultUrl, boolean isForward) {
		this.m_status = 0;
		this.m_resultUrl = resultUrl;
		this.m_isForward = isForward;
	}

	private int m_status = 0; // 处理结果状态。
	private String m_resultUrl = null; // 显示处理结果的URL地址。
	private boolean m_isForward = false; // 处理结果地址是否以转发方式重定向。

	private String m_title = null; // 消息框标题，可选。
	private String m_message = null; // 消息框内容，可选。
	private String m_icon = null; // 消息框图标URL地址，可选。
	private LogLevel m_level = LogLevel.Information; // 消息级别，默认为LogLevel.Information。
	private String m_sourceUrl = null; // 生成此结果的原始URL地址，可选。
	private Object m_tag = null; // 用于保存额外结果信息的对象，可选。

	/**
	 * 返回处理结果消息框标题。
	 * 
	 * @return String
	 */
	public String getTitle() {
		return this.m_title;
	}

	/**
	 * 设置处理结果消息框标题，可选。
	 * 
	 * @param title String
	 */
	public void setTitle(String title) {
		this.m_title = title;
	}

	/**
	 * 返回处理结果消息框内容。
	 * 
	 * @return String
	 */
	public String getMessage() {
		return this.m_message;
	}

	/**
	 * 设置处理结果消息框内容，可选。
	 * 
	 * @param message String
	 */
	public void setMessage(String message) {
		this.m_message = message;
	}

	/**
	 * 返回处理结果消息框图标URL地址。
	 * 
	 * @return String
	 */
	public String getIcon() {
		return this.m_icon;
	}

	/**
	 * 设置处理结果消息框图标URL地址，可选。
	 * 
	 * @param icon String
	 */
	public void setIcon(String icon) {
		this.m_icon = icon;
	}

	/**
	 * 返回处理结果消息级别。
	 * 
	 * <p>
	 * 默认为{@link LogLevel#Information}。
	 * </p>
	 * 
	 * @return {@link LogLevel}
	 */
	public LogLevel getLevel() {
		return this.m_level;
	}

	/**
	 * 设置处理结果消息级别，可选。
	 * 
	 * @param level
	 */
	public void setLevel(LogLevel level) {
		this.m_level = level;
	}

	/**
	 * 返回原始http请求的URL地址。
	 * 
	 * @return String 除非有设置url地址，否则默认为null。
	 */
	public String getSourceUrl() {
		return this.m_sourceUrl;
	}

	/**
	 * 设置原始http请求的URL地址，可选。
	 * 
	 * @param sourceUrl String
	 */
	public void setSourceUrl(String sourceUrl) {
		this.m_sourceUrl = sourceUrl;
	}

	/**
	 * 返回处理结果中额外包含的信息对应的对象。
	 * 
	 * @return Object 除非有设置额外信息，否则默认为null。
	 */
	public Object getTag() {
		return this.m_tag;
	}

	/**
	 * 设置处理结果中额外包含的信息对应的对象，可选。
	 * 
	 * @param tag Object
	 */
	public void setTag(Object tag) {
		this.m_tag = tag;
	}

	/**
	 * 返回处理结果状态。
	 * 
	 * <p>
	 * 默认为0，处理结果状态为0表示处理成功，其它数字表示处理过程出现异常。
	 * </p>
	 * 
	 * @return int
	 */
	public int getStatus() {
		return this.m_status;
	}

	/**
	 * 设置处理结果状态。
	 * 
	 * @param status int
	 */
	public void setStatus(int status) {
		this.m_status = status;
	}

	/**
	 * 返回处理结果的URL地址。
	 * 
	 * <p>
	 * 返回的url地址中，可以包含“~”、“{frame}”等动态路径字符串，分别表示“web应用根路径”、“框架路径”的具体值。
	 * </p>
	 * <p>
	 * 比如在http://oa.acme.com/discoverx2/这个web应用根路径下当前用户使门户方案的样式框架路径名为“frame”，<br/>
	 * 则返回“{frame}document.jsp”时，实际是“/discoverx2/frame/document.jsp”；<br/>
	 * 则返回“~message/xml_message.jsp”时，实际是“/discoverx2/message/xml_message.jsp”。
	 * </p>
	 * 
	 * @return String。
	 */
	public String getResultUrl() {
		return this.m_resultUrl;
	}

	/**
	 * 设置处理结果的URL地址，必须。
	 * 
	 * @param resultUrl String
	 */
	public void setResultUrl(String resultUrl) {
		this.m_resultUrl = resultUrl;
	}

	/**
	 * 返回是否以转发方式继续将原始请求发送给{@link #getResultUrl()}的返回结果进行处理。
	 * 
	 * <p>
	 * 默认为false，表示使用http重定向的方式显示{@link #getResultUrl()}的返回结果。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getIsForward() {
		return this.m_isForward;
	}

	/**
	 * 设置是否以转发方式继续将原始请求发送给{@link #getResultUrl()}的返回结果进行处理，可选。
	 * 
	 * @param isForward boolean
	 */
	public void setIsForward(boolean isForward) {
		this.m_isForward = isForward;
	}

}

