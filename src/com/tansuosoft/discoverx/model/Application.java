/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.common.event.EventHandlerInfo;

/**
 * 应用程序（也可称模块、应用模块等）资源类。
 * 
 * <p>
 * 系统用于划分不同类型功能模块的资源称为<strong>应用程序</strong>或<strong>应用模块</strong>，比如收文管理、发文管理等都是不同的应用程序/模块。
 * </p>
 * <p>
 * 每个应用程序可以包含各自的表单（根据需要也可以从上级应用程序继承）、视图、流程（根据需要也可以从上级应用程序继承）、模板附加文件等信息；应用程序根据需要还可以包含树状的下级应用程序，这些下级应用程序可以绑定自有的表单、视图、流程、权限等，或者从其上级继承表单、流程等。
 * </p>
 * <p>
 * 如果系统默认提供的内容不足以支撑完整应用程序功能，那么可以进行二次开发，并将应用程序特定功能对应文件（除jar外）保存在应用程序特定功能目录下。<br/>
 * 应用程序特定功能目录命名规则为：当前页面框架（{frame}）+转化为小写字母的应用程序别名作为子目录的路径。<br/>
 * 如应用程序别名为“appTest”，当前页面框架路径为“frame”，则：<br/>
 * web引用的url为：“/discoverx2/frame/apptest/”（假设整个web应用的根目录为“/discoverx2/”）；<br/>
 * 服务器物理地址为：{@link com.tansuosoft.discoverx.common.CommonConfig#getInstallationPath()}+"\frame\apptest\"。<br/>
 * 应用程序特定功能对应的jar文件应以“应用程序别名小写.jar”保存于“WEB-INF\lib”下；<br/>
 * 导出完整应用程序时，系统会自动将这些文件和应用程序及其包含的下级应用程序、表单、视图、附加文件资源一起打包。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Application extends Resource implements Cloneable {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -1527302290463917819L;

	/**
	 * 缺省构造器
	 */
	public Application() {
	}

	private String m_form = null; // 包含的表单资源UNID。
	private String m_workflow = null; // 包含的流程资源UNID。
	private List<Reference> m_views = null; // 包含的视图列表集合。
	private List<Reference> m_subApplications = null; // 包含的下级文档定义列表集合。
	private String m_businessSequencePattern = null; // 模块所包含文档的业务序号模式。
	private String m_processSequencePattern = null; // 模块所包含文档的流水序号（流水号）模式。
	private boolean m_publish = true; // 发布选项
	private PublishOption m_publishOption = PublishOption.Custom; // 公开选项
	private List<Operation> m_operations = null; // 包含文档通用的操作集合，可选。
	private List<Operation> m_newStateOperations = null; // 包含文档新增状态时的操作集合，可选。
	private Security m_documentDefaultSecurity = null; // 所定义文档的默认安全控制信息。
	private String m_documentNameProviderClassName = null; // 所定义文档名称提供对象全名。
	private String m_documentAbstractProviderClassName = null; // 所定义文档摘要提供对象全名。
	private int m_textAccessoryCount = 0; // 模块定义的文档可包含的正文个数，默认为0。
	private DocumentDisplay m_documentDisplay = null; // 文档显示选项，可选。
	private boolean m_autoRecordStateInfo = true; // 此模块对应文档是否需要同步文档状态信息以供视图查询。
	private List<EventHandlerInfo> m_eventHandlers = null; // 包含所定义的文档的事件处理程序信息，可选。
	private String m_profile = null; // 配置文档资源UNID。
	private String m_profileForm = null; // 配置文档所使用的表单资源UNID。
	private Map<Integer, String> m_profileAdmins = null; // 可管理配置文档的参与者安全编码列表。
	private String m_version = null; // 版本信息，由系统内部使用。

	/**
	 * 返回包含的表单资源UNID。
	 * 
	 * @see Application#setForm(String)
	 * @return String
	 */
	public String getForm() {
		return this.m_form;
	}

	/**
	 * 设置包含的表单资源UNID。
	 * 
	 * <p>
	 * 独立应用模块必须包含表单，下级应用模块资源可以使用上级应用模块资源绑定的表单，因此可以不配置，也就是说，在一个完整的应用模块资源树中，必须至少包含一个表单。
	 * </p>
	 * 
	 * @param form String
	 */
	public void setForm(String form) {
		this.m_form = form;
	}

	/**
	 * 返回包含的流程资源UNID。
	 * 
	 * @see Application#setWorkflow(String)
	 * @return String
	 */
	public String getWorkflow() {
		return this.m_workflow;
	}

	/**
	 * 设置包含的流程资源UNID。
	 * 
	 * <p>
	 * 如果模块不配置流程，那么系统会自动使用自由流转流程。
	 * </p>
	 * 
	 * @param workflow String
	 */
	public void setWorkflow(String workflow) {
		this.m_workflow = workflow;
	}

	/**
	 * 返回包含的视图列表集合。
	 * 
	 * @return java.util.List&lt;Reference&gt; Views
	 */
	public java.util.List<Reference> getViews() {
		return this.m_views;
	}

	/**
	 * 设置包含的视图列表集合。
	 * 
	 * @param views java.util.List&lt;Reference&gt; views
	 */
	public void setViews(java.util.List<Reference> views) {
		this.m_views = views;
	}

	/**
	 * 返回包含的下级文档定义列表集合。
	 * 
	 * @return List&lt;Reference&gt;
	 */
	public List<Reference> getSubApplications() {
		return this.m_subApplications;
	}

	/**
	 * 设置包含的下级文档定义列表集合，可选。
	 * 
	 * @param subApplications List&lt;Reference&gt;
	 */
	public void setSubApplications(List<Reference> subApplications) {
		this.m_subApplications = subApplications;
	}

	/**
	 * 返回模块所包含文档的业务号模式。
	 * 
	 * @return String
	 */
	public String getBusinessSequencePattern() {
		return this.m_businessSequencePattern;
	}

	/**
	 * 设置模块所包含文档的业务号模式。
	 * 
	 * <p>
	 * 对于公文类型的文档来说业务序号通常是公文的文号，对于其它类型文档来说，使用者可以根据实际情况对业务号做不同的解释。
	 * </p>
	 * <p>
	 * 模式配置时可包含如下内容：
	 * <ul>
	 * <li>字段变量（包括所属表单字段和文档内置字段），把字段名写在“{”与“}”之间，如{fld_subject}、{created}；</li>
	 * <li>常量，可以包含任意字符；</li>
	 * <li>格式化符，格式为：“/截取长度,长度替代符/”，如“/4,0/”表示长度为从左到右取4个字符，不足在前面补零，“/-2,0/”表示从右到左取2位，不足在前名补零</li>
	 * </ul>
	 * 示例：<br/>
	 * 示例中假设：<br/>
	 * {@link Document#getBusinessYear()}返回“2009”<br/>
	 * {@link Document#getBusinessTitle()} 返回“国办文”<br/>
	 * {@link Document#getBusinessSequence()}返回"100”<br/>
	 * {@link Document#getCreated()}返回"2009-12-12 14:30:45”<br/>
	 * 名为“fld_subject”的字段值为“文件题名1”<br/>
	 * <table cellspacing="0" cellpadding="0" width="100%" border="1" style="table-layout:fixed;border-collapse:collapse">
	 * <tr style="background:#ccc none;">
	 * <td width="66%">模式</td>
	 * <td width="34%">结果</td>
	 * </tr>
	 * <tr>
	 * <td>
	 * {businessYear}[{businessTitle}]{businessSequence}号</td>
	 * <td>2009[国办文]100号</td>
	 * </tr>
	 * <tr>
	 * <td>
	 * {businessYear}[{businessTitle}]{businessSequence}/4,0/号</td>
	 * <td>2009[国办文]0100号</td>
	 * </tr>
	 * <tr>
	 * <td>
	 * {fld_subject}/6,&nbsp;/[{created}]</td>
	 * <td>文件题名1&nbsp;&nbsp;[2009]</td>
	 * </tr>
	 * <tr>
	 * <td>
	 * {businessYear}/-2,0/S{businessSequence}/4,0/</td>
	 * <td>09S0100</td>
	 * </tr>
	 * </table>
	 * </p>
	 * <p>
	 * 只有在所属模块中配置了有效的模式后其对应的文档在第一次保存时才会自动累计序号并记录年份。<br/>
	 * 用户也可以通过事件或操作修改或自定义这些值。
	 * </p>
	 * <p>
	 * 可以通过名为“businessSequenceInitValue”的额外参数设置自增长序列号的初始值（默认为0，表示从1开始计数）；<br/>
	 * 可以通过名为“businessSequenceStepValue”的额外参数设置自增长序列号的步长值（默认为0，表示每次的步长累计为加1）；<br/>
	 * </p>
	 * 
	 * @param businessSequencePattern String
	 */
	public void setBusinessSequencePattern(String businessSequencePattern) {
		this.m_businessSequencePattern = businessSequencePattern;
	}

	/**
	 * 返回模块所包含文档的流水号模式。
	 * 
	 * @return String
	 */
	public String getProcessSequencePattern() {
		return this.m_processSequencePattern;
	}

	/**
	 * 设置模块所包含文档的流水号模式。
	 * 
	 * <p>
	 * 配置方法请参考{@link Application#setBusinessSequencePattern(String)}的说明。
	 * </p>
	 * <p>
	 * 只有在所属模块中配置了有效的模式后其对应的文档在第一次保存时才会自动累计序号并记录年份。<br/>
	 * 用户也可以通过事件或操作修改或自定义这些值。
	 * </p>
	 * <p>
	 * 可以通过名为“processSequenceInitValue”的额外参数设置自增长序列号的初始值（默认为0，表示从1开始计数）；<br/>
	 * 可以通过名为“processSequenceStepValue”的额外参数设置自增长序列号的步长值（默认为0，表示每次的步长累计为加1）；<br/>
	 * </p>
	 * 
	 * @param processSequencePattern String
	 */
	public void setProcessSequencePattern(String processSequencePattern) {
		this.m_processSequencePattern = processSequencePattern;
	}

	/**
	 * 返回所包含文档的发布选项。
	 * 
	 * <p>
	 * 默认为true，则表示发布后，还可对一定范围人员公开。<br/>
	 * 如果为false，表示仅发布文档（即只将文档设置为发布状态）。
	 * </p>
	 * 
	 * @return boolean。
	 */
	public boolean getPublish() {
		return this.m_publish;
	}

	/**
	 * 设置所包含文档的发布选项。
	 * 
	 * @param publish boolean
	 */
	public void setPublish(boolean publish) {
		this.m_publish = publish;
	}

	/**
	 * 返回所包含文档的公开范围。
	 * 
	 * <p>
	 * 必须在{@link #getPublish()}返回true时才有意义。<br/>
	 * 默认为{@link PublishOption#Custom}。<br/>
	 * </p>
	 * 
	 * @return {@link PublishOption}
	 */
	public PublishOption getPublishOption() {
		return this.m_publishOption;
	}

	/**
	 * 设置所包含文档的公开范围。
	 * 
	 * @param publishOption {@link PublishOption}
	 */
	public void setPublishOption(PublishOption publishOption) {
		this.m_publishOption = publishOption;
	}

	/**
	 * 返回包含所定义的文档的通用操作集合，可选。
	 * 
	 * <p>
	 * 通用操作集合是指文档保存后，不管文档处于哪个流程环节，都可以显示的操作（当然前提是必需显示条件公式返回true）。
	 * </p>
	 * <p>
	 * 区别于流程环节内定义的操作集合。
	 * </p>
	 * <p>
	 * 区别于文档新增状态时的操作集合。
	 * </p>
	 * 
	 * @see Application#getNewStateOperations()
	 * @return List&lt;Operation&gt;
	 */
	public List<Operation> getOperations() {
		if (this.m_operations != null) Collections.sort(this.m_operations, new OperationComparator());
		return this.m_operations;
	}

	/**
	 * 设置包含所定义的文档的通用操作集合，可选。
	 * 
	 * <p>
	 * 所有新增状态的资源默认会有关闭和保存按钮，如果需要，可通过此属性提供其它按钮。
	 * </p>
	 * 
	 * @param operations List&lt;Operation&gt;
	 */
	public void setOperations(List<Operation> operations) {
		this.m_operations = operations;
	}

	/**
	 * 返回包含文档新增状态时的操作集合，可选。
	 * 
	 * <p>
	 * 关闭和保存按钮由系统默认会自动提供，需要显示其它非系统默认按钮才需要通过此处进行配置。
	 * </p>
	 * 
	 * @return List&lt;Operation&gt;
	 */
	public List<Operation> getNewStateOperations() {
		if (this.m_newStateOperations != null) Collections.sort(this.m_newStateOperations, new OperationComparator());
		return this.m_newStateOperations;
	}

	/**
	 * 设置包含文档新增状态时的操作集合，可选。
	 * 
	 * @param newStateOperations List&lt;Operation&gt;
	 */
	public void setNewStateOperations(List<Operation> newStateOperations) {
		this.m_newStateOperations = newStateOperations;
	}

	/**
	 * 返回包含所定义的文档的事件处理程序信息，可选。
	 * 
	 * @return List&lt;EventHandlerInfo&gt;
	 */
	public List<EventHandlerInfo> getEventHandlers() {
		return this.m_eventHandlers;
	}

	/**
	 * 设置包含所定义的文档的事件处理程序信息，可选。
	 * 
	 * @param eventHandlers List&lt;EventHandlerInfo&gt;
	 */
	public void setEventHandlers(List<EventHandlerInfo> eventHandlers) {
		this.m_eventHandlers = eventHandlers;
	}

	/**
	 * 返回所定义文档的默认安全控制信息。
	 * 
	 * @return Security
	 */
	public Security getDocumentDefaultSecurity() {
		return this.m_documentDefaultSecurity;
	}

	/**
	 * 设置所定义文档的默认安全控制信息。
	 * 
	 * @param documentDefaultSecurity Security
	 */
	public void setDocumentDefaultSecurity(Security documentDefaultSecurity) {
		this.m_documentDefaultSecurity = documentDefaultSecurity;
	}

	/**
	 * 返回所定义文档名称提供类名。
	 * 
	 * @return String
	 */
	public String getDocumentNameProviderClassName() {
		return this.m_documentNameProviderClassName;
	}

	/**
	 * 设置所定义文档名称提供类名。
	 * 
	 * <p>
	 * 可以是自定义的提供文档名称的全限定类(实现{@link DocumentPropertyValueProvider}的具体类)或字段值格式化模板。<br/>
	 * 字段值格式化模板示例：<br/>
	 * <i>{fld_subject}({category})表示名称格式为：“{文档fld_subject字段值}({文档所属模块名})”。</i><br/>
	 * <strong>请勿在此属性中再配置文档名称对应的字段值格式化模板，否则可能导致死循环！</strong>
	 * </p>
	 * 
	 * @param documentNameProviderClassName String
	 */
	public void setDocumentNameProviderClassName(String documentNameProviderClassName) {
		this.m_documentNameProviderClassName = documentNameProviderClassName;
	}

	/**
	 * 返回所定义文档摘要提供对象全名。
	 * 
	 * <p>
	 * 自定义的提供文档摘要的类。
	 * </p>
	 * 
	 * @see Document#getAbstract()
	 * @see com.tansuosoft.discoverx.model.DocumentPropertyValueProvider
	 * @return String
	 */
	public String getDocumentAbstractProviderClassName() {
		return this.m_documentAbstractProviderClassName;
	}

	/**
	 * 设置所定义文档摘要提供对象全名。
	 * 
	 * @param documentAbstractProviderClassName String
	 */
	public void setDocumentAbstractProviderClassName(String documentAbstractProviderClassName) {
		this.m_documentAbstractProviderClassName = documentAbstractProviderClassName;
	}

	/**
	 * 返回属于此应用程序的文档可包含的正文个数，默认为0。
	 * 
	 * <p>
	 * 0表示不允许有正文类型的附加文件，-1表示继承上级应用程序的正文个数配置，1表示一个正文，2表示2个，依此类推。
	 * </p>
	 * 
	 * @return int
	 */
	public int getTextAccessoryCount() {
		return this.m_textAccessoryCount;
	}

	/**
	 * 设置属于此应用程序的文档可包含的正文个数。
	 * 
	 * @param textAccessoryCount int
	 */
	public void setTextAccessoryCount(int textAccessoryCount) {
		this.m_textAccessoryCount = textAccessoryCount;
	}

	/**
	 * 返回文档显示选项，可选。
	 * 
	 * <p>
	 * 如果不配置，那么会使用系统全局统一的默认显示方式来显示。
	 * </p>
	 * 
	 * @return DocumentDisplay
	 */
	public DocumentDisplay getDocumentDisplay() {
		return this.m_documentDisplay;
	}

	/**
	 * 设置文档显示选项，可选。
	 * 
	 * @see Application#getDocumentBrowserDisplay();
	 * @param documentDisplay DocumentDisplay
	 */
	public void setDocumentDisplay(DocumentDisplay documentDisplay) {
		this.m_documentDisplay = documentDisplay;
	}

	/**
	 * 返回此模块对应文档视图是否需要查询文档额外状态信息。
	 * 
	 * <p>
	 * 文档额外状态信息是指文档当前具有的一些可选的额外状态信息，通常是一些比如当前流程、当前环节之类的信息。<br/>
	 * 文档额外状态信息保存于“t_extra”表中且通过文档unid与文档记录一一对应。
	 * </p>
	 * <p>
	 * 如果为false，表示不会提供文档查询时的额外状态信息（即配置视图时不能使用“文档额外状态信息”有关的表，否则可能导致无法查询到数据）。<br/>
	 * 如果为true，那么系统会在流程状态变化时自动记录文档流程状态有关的额外状态信息，如果需要其它额外信息，可以根据需要在“t_extra”表中添加新字段并在合适的地方编写并启用事件处理程序以填充或更新相应记录。<br/>
	 * 默认为true，对于没有流转过程（即只有一个环节的流程）的应用程序，通常应设置为false。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getAutoRecordStateInfo() {
		return this.m_autoRecordStateInfo;
	}

	/**
	 * 设置此模块对应文档视图是否需要查询文档额外状态信息。
	 * 
	 * @param autoRecordStateInfo boolean
	 */
	public void setAutoRecordStateInfo(boolean autoRecordStateInfo) {
		this.m_autoRecordStateInfo = autoRecordStateInfo;
	}

	/**
	 * 返回配置文档资源UNID。
	 * 
	 * <p>
	 * 参考{@link #getProfileForm()}中的说明。
	 * </p>
	 * 
	 * @return String
	 */
	public String getProfile() {
		return this.m_profile;
	}

	/**
	 * 设置配置文档资源UNID。
	 * 
	 * <p>
	 * 参考{@link #getProfileForm()}中的说明。
	 * </p>
	 * 
	 * @param profile String
	 */
	public void setProfile(String profile) {
		this.m_profile = profile;
	}

	/**
	 * 返回配置文档所使用的表单资源UNID。
	 * 
	 * <p>
	 * 应用程序配置文档（或配置文件）通常用于保存应用程序运行时的一些特殊属性值； 如考勤管理中，上下班时间可以通过考勤管理的应用程序配置文件设置并读取。<br/>
	 * 通常可以使用应用程序额外参数或启用配置文件来保存应用程序特定属性。一般来说如果配置项少，或者配置项之间没有逻辑关系，可以使用额外参数保存，反之则可以使用配置文件。<br/>
	 * 通常通过{@link com.tansuosoft.discoverx.bll.ApplicationProfile}来获取配置文档，然后通过{@link Document#getItemValue(String)}等方法来获取配置项字段值。
	 * </p>
	 * <p>
	 * 只有在应用管理配置了其配置文档所使用的“配置文件表单”资源后才能启用应用程序配置文件功能，否则{@link #setProfile(String)}、{@link #setProfileAdmins(Map)}等均没有意义。
	 * </p>
	 * 
	 * @return String
	 */
	public String getProfileForm() {
		return this.m_profileForm;
	}

	/**
	 * 设置配置文档所使用的表单资源UNID。
	 * 
	 * <p>
	 * 参考{@link #getProfileForm()}中的说明。
	 * </p>
	 * 
	 * @param profileForm String
	 */
	public void setProfileForm(String profileForm) {
		this.m_profileForm = profileForm;
	}

	/**
	 * 返回可管理配置文档的参与者安全编码列表。
	 * 
	 * <p>
	 * key为安全编码列表，value为安全编码对应的参与者名称，只有包含于此方法返回的参与者集合指定的用户才能才、删、改配置文件。如果不配置，则默认只有超级管理员才能增、删、改配置文件。
	 * </p>
	 * <p>
	 * 参考{@link #getProfileForm()}中的说明。
	 * </p>
	 * 
	 * @return Map&lt;Integer,String&gt;
	 */
	public Map<Integer, String> getProfileAdmins() {
		return this.m_profileAdmins;
	}

	/**
	 * 设置可管理配置文档的参与者安全编码列表。
	 * 
	 * <p>
	 * 参考{@link #getProfileForm()}中的说明。
	 * </p>
	 * 
	 * @param profileAdmins Map&lt;Integer,String&gt;
	 */
	public void setProfileAdmins(Map<Integer, String> profileAdmins) {
		this.m_profileAdmins = profileAdmins;
	}

	/**
	 * 返回版本信息，由系统内部使用。
	 * 
	 * @return String
	 */
	public String getVersion() {
		return this.m_version;
	}

	/**
	 * 设置版本信息，由系统内部使用。
	 * 
	 * @param version String
	 */
	public void setVersion(String version) {
		this.m_version = version;
	}

	/**
	 * 返回是否有需要作为模块大纲树显示的视图。
	 * 
	 * @return boolean
	 */
	public boolean hasOutlineView() {
		if (this.m_views == null || this.m_views.isEmpty()) return false;
		for (Reference ref : this.m_views) {
			if (ref == null) continue;
			View view = ResourceContext.getInstance().getResource(ref.getUnid(), View.class);
			if (view != null && view.getApplicationOutline()) return true;
		}
		return false;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Application a = (Application) super.clone();
		a.setAlias(this.getAlias());
		a.setAutoRecordStateInfo(this.getAutoRecordStateInfo());
		a.setBusinessSequencePattern(this.getBusinessSequencePattern());
		a.setDocumentAbstractProviderClassName(this.getDocumentAbstractProviderClassName());
		Security s = this.getDocumentDefaultSecurity();
		if (s != null) a.setDocumentDefaultSecurity((Security) s.clone());
		DocumentDisplay dd = this.getDocumentDisplay();
		if (dd != null) a.setDocumentDisplay((DocumentDisplay) dd.clone());
		a.setDocumentNameProviderClassName(this.getDocumentNameProviderClassName());
		List<EventHandlerInfo> ehis = this.getEventHandlers();
		if (ehis != null && ehis.size() > 0) {
			a.setEventHandlers(new ArrayList<EventHandlerInfo>());
			for (EventHandlerInfo x : ehis) {
				if (x == null) continue;
				a.getEventHandlers().add((EventHandlerInfo) x.clone());
			}
		}
		a.setForm(this.getForm());
		List<Operation> oprs = this.getNewStateOperations();
		if (oprs != null && oprs.size() > 0) {
			a.setNewStateOperations(new ArrayList<Operation>());
			for (Operation x : oprs) {
				if (x == null) continue;
				a.getNewStateOperations().add((Operation) x.clone());
			}
		}
		oprs = this.getOperations();
		if (oprs != null && oprs.size() > 0) {
			a.setOperations(new ArrayList<Operation>());
			for (Operation x : oprs) {
				if (x == null) continue;
				a.getOperations().add((Operation) x.clone());
			}
		}

		a.setProcessSequencePattern(this.getProcessSequencePattern());
		a.setPublish(this.getPublish());
		a.setPublishOption(this.getPublishOption());
		a.setSelectable(this.getSelectable());
		a.setTextAccessoryCount(this.getTextAccessoryCount());
		a.setWorkflow(this.getWorkflow());

		List<Reference> refs = this.getSubApplications();
		if (refs != null && refs.size() > 0) {
			a.setSubApplications(new ArrayList<Reference>(refs.size()));
			for (Reference r : refs) {
				if (r == null) continue;
				a.getSubApplications().add((Reference) r.clone());
			}
		}
		refs = this.getViews();
		if (refs != null && refs.size() > 0) {
			a.setViews(new ArrayList<Reference>(refs.size()));
			for (Reference r : refs) {
				if (r == null) continue;
				a.getViews().add((Reference) r.clone());
			}
		}

		a.setProfileForm(this.getProfileForm());
		Map<Integer, String> pas = this.getProfileAdmins();
		if (pas != null && pas.size() > 0) {
			a.setProfileAdmins(new HashMap<Integer, String>(pas.size()));
			for (Integer sc : pas.keySet()) {
				a.getProfileAdmins().put(sc, pas.get(sc));
			}
		}

		a.setVersion(this.getVersion());
		return a;
	}
}

