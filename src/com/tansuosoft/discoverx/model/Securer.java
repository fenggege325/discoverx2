/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 可提供安全编码的类实现的接口。
 * 
 * <p>
 * 它用于指示某类资源可以用于在系统中控制访问权限。
 * </p>
 * <p>
 * 用户（{@link User}）、组织（{@linkOrganization}）、群组（{@link Group}）、角色（{@link Role}）等参与者{@link Participant}类型的资源需实现此接口以提供一个全局唯一正整数安全编码。<br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public interface Securer {
	/**
	 * 安全编码起始序号：100。
	 * 
	 * <p>
	 * 0-99为系统保留的序号。
	 * </p>
	 * <p>
	 * 以下为常用系统保留安全编码或编码段说明。
	 * <ul>
	 * <li>0：系统匿名用户使用的安全编码；</li>
	 * <li>1：系统内置超级管理员使用的安全编码；</li>
	 * <li>2：系统根组织安全编码；</li>
	 * <li>3：系统内置超级管理员安全编码；</li>
	 * <li>4：系统内置文档管理员安全编码；</li>
	 * <li>5：系统内置配置管理员安全编码；</li>
	 * <li>6：所有登录用户自动拥有的角色安全编码；</li>
	 * <li>7：所有匿名用户自动拥有的角色安全编码；</li>
	 * <li>8：系统本身作为用户时所对应的安全编码；</li>
	 * <li>10-20：系统内置流程群组安全编码，参考{@link com.tansuosoft.discoverx.workflow.WorkflowGroupsProvider}中的说明；</li>
	 * <li>21-99：为每个具体应用系统运行所预设的安全编码所保留。</li>
	 * </ul>
	 * </p>
	 */
	public static final int MIN_AVAILABLE_CODE = 100;

	/**
	 * 获取系统全局唯一安全编码值。
	 * 
	 * <p>
	 * <strong>注意此属性值设置后就不能改变且必须全局唯一，否则可能导致系统不正常。</strong>
	 * </p>
	 * <p>
	 * 编码有效范围为：{@link #MIN_AVAILABLE_CODE}到2147483647（即Integer.MAX_VALUE：2^32-1），0-（{@link #MIN_AVAILABLE_CODE}-1）为系统保留使用的特殊编码段。
	 * </p>
	 * 
	 * @return int
	 */
	public int getSecurityCode();

	/**
	 * 设置系统全局唯一安全编码值。
	 * 
	 * <p>
	 * 编码一旦设置后就不能更改。
	 * </p>
	 * 
	 * @param code
	 */
	public void setSecurityCode(int code);
}

