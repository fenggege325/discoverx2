/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 表示文档状态标记的枚举。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public enum DocumentState implements EnumBase {
	/**
	 * 新建状态（0）
	 */
	New(0),
	/**
	 * 正常状态（1）
	 */
	Normal(1),
	/**
	 * 生效状态（2）
	 * 
	 * <p>
	 * 通常用于表示签发之后的文档状态。
	 * </p>
	 */
	Signed(2),
	/**
	 * 发布状态（4）
	 */
	Issued(4),
	/**
	 * 公开状态（8）
	 */
	Published(8),
	/**
	 * 归档状态（16）
	 */
	Archived(16),
	/**
	 * 删除状态（32）
	 */
	Deleted(32);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	DocumentState(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return DocumentState
	 */
	public DocumentState parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (DocumentState s : DocumentState.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return DocumentState.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return DocumentState
	 */
	public static DocumentState parse(int v) {
		for (DocumentState s : DocumentState.values())
			if (s.getIntValue() == v) return s;
		return null;
	}
}

