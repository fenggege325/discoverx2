/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 用于描述系统门户中显示的菜单项、大纲项及其关系等信息的菜单资源类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Menu extends Resource {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 3985698529297851081L;

	private String m_target = null;
	private MenuTarget m_menuTarget = MenuTarget.Category;
	private String m_targetDirectory = null;
	private boolean m_renderResourceTree = false;

	/**
	 * 缺省构造器
	 */
	public Menu() {
	}

	/**
	 * 获取此大纲项指向的目标数据源。
	 * 
	 * <p>
	 * 根据{@link MenuTarget}的配置，返回结果格式说明如下：
	 * <ul>
	 * <li>{@link MenuTarget#Href}：url地址。</li>
	 * <li>{@link MenuTarget#Resource}：资源UNID或别名。</li>
	 * <li>{@link MenuTarget#TreeViewProvider}：提供类全限定类名，此类必须继承自{@link com.tansuosoft.discoverx.web.ui.menu.MenuOutlineRender}。</li>
	 * <li>{@link MenuTarget#Category}：不需要配置值或值被忽略。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 */
	public String getTarget() {
		return this.m_target;
	}

	/**
	 * 设置此大纲项指向的目标数据源。
	 * 
	 * <p>
	 * 需根据{@link Menu#getMenuTarget()}的结果设置符合格式的值，参考“{@link Menu#getTarget()}”中的说明。
	 * </p>
	 * 
	 * @param target String
	 */
	public void setTarget(String target) {
		this.m_target = target;
	}

	/**
	 * 返回资源数据源选项。
	 * 
	 * <p>
	 * 如果{@link #getMenuTarget()}为{@link MenuTarget#Resource}，那么此方法返回资源目录名。<br/>
	 * 如果资源目录名不提供，则默认为应用程序资源。
	 * </p>
	 * <p>
	 * 如果{@link #getMenuTarget()}为{@link MenuTarget#Href}，那么此方法返回链接打开的目标窗口。<br/>
	 * 如果不提供则默认为“_right”，如果配置为“_right”则表示在页面右边显示区打开链接所指内容（以iframe方式）。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTargetDirectory() {
		return this.m_targetDirectory;
	}

	/**
	 * 设置资源数据源选项。
	 * 
	 * @param targetDirectory String
	 */
	public void setTargetDirectory(String targetDirectory) {
		this.m_targetDirectory = targetDirectory;
	}

	/**
	 * 获取此大纲项指向的目标数据源的类型。
	 * 
	 * <p>
	 * 默认返回{@link MenuTarget#Category}。
	 * </p>
	 * 
	 * @return {@link MenuTarget}
	 */
	public MenuTarget getMenuTarget() {
		return this.m_menuTarget;
	}

	/**
	 * 设置此大纲项指向的目标数据源的类型。
	 * 
	 * @param menuTarget {@link MenuTarget}
	 */
	public void setMenuTarget(MenuTarget menuTarget) {
		this.m_menuTarget = menuTarget;
	}

	/**
	 * 返回是否输出目标资源树。
	 * 
	 * <p>
	 * 如果为true，则呈现时将自动输出指向的目标资源的树，只有{@link #getMenuTarget()}返回{@link MenuTarget#Resource}时才有意义。<br/>
	 * 默认为false，表示不展开指向的目标资源（而是输出菜单项条目本身）。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getRenderResourceTree() {
		return this.m_renderResourceTree;
	}

	/**
	 * 设置是否输出目标资源树。
	 * 
	 * @param renderResourceTree boolean
	 */
	public void setRenderResourceTree(boolean renderResourceTree) {
		this.m_renderResourceTree = renderResourceTree;
	}
}

