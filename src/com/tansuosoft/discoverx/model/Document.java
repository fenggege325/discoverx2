/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 表示系统文档资源信息的基类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Document extends Resource {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -6428879457157117165L;

	/**
	 * 内置unid字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_UNID = "UNID";
	/**
	 * 内置名称字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_NAME = "name";
	/**
	 * 内置分类字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_CATEGORY = "category";
	/**
	 * 内置资源描述字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_DESCRIPTION = "description";
	/**
	 * 内置别名字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_ALIAS = "alias";
	/**
	 * 内置作者字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_CREATOR = "creator";
	/**
	 * 内置创建时间字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_CREATED = "created";
	/**
	 * 内置最近更新人员字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_MODIFIER = "modifier";
	/**
	 * 内置最近更新时间字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_MODIFIED = "modified";
	/**
	 * 内置父UNID字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_PUNID = "PUNID";
	/**
	 * 内置排序字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_SORT = "sort";
	/**
	 * 内置资源来源字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_SOURCE = "source";
	/**
	 * 内置资源是否可选字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_SELECTABLE = "selectable";
	/**
	 * 内置文档状态字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_STATE = "state";
	/**
	 * 内置文档类型字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_TYPE = "type";
	/**
	 * 内置文档搜索摘要字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_ABSTRACT = "abstract";
	/**
	 * 内置文档标题字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_TITLE = "title";
	/**
	 * 内置文档业务标题字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_BUSINESSTITLE = "businessTitle";
	/**
	 * 内置文档流水序号字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_PROCESSSEQUENCE = "processSequence";
	/**
	 * 内置文档业务序号字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_BUSINESSSEQUENCE = "businessSequence";
	/**
	 * 内置文档流水年份序号字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_PROCESSYEAR = "processYear";
	/**
	 * 内置文档业务年份字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_BUSINESSYEAR = "businessYear";
	/**
	 * 内置文档点击量字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_CLICKCOUNT = "clickCount";
	/**
	 * 内置文档评分人数字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_RANKCOUNT = "rankCount";
	/**
	 * 内置文档评分总数字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_RANKVALUE = "rankValue";
	/**
	 * 内置文档所属表单别名字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_FORMALIAS = "formAlias";
	/**
	 * 内置文档分类标记字段名。
	 */
	protected static final String BUILTIN_FIELD_NAME_TAG = "tag";

	/**
	 * 文档内置字段名数组。
	 */
	protected static final String[] BUILTIN_ITEM_NAMES = { "UNID", "name", "category", "description", "alias", "creator", "created", "modifier", "modified", "PUNID", "sort", "source", "selectable", "state", "type", "abstract", "title", "businessTitle", "processSequence", "businessSequence", "processYear", "businessYear", "clickCount", "rankCount", "rankValue", "formAlias", "tag" };
	/**
	 * 文档内置字段标题数组。
	 */
	protected static final String[] BUILTIN_ITEM_CAPTIONS = { "UNID", "文档名称", "文档所属模块", "文档说明", "文档别名", "文档作者", "文档创建时间", "文档最近修改者", "文档最近修改时间", "PUNID", "文档排序号", "文档资源来源", "文档是否可选择", "文档状态", "文档类型", "文档摘要", "文档所属完整模块路径", "文档所属直接模块名称", "文档流水序号", "文档业务序号", "文档流水号年份", "文档业务号年份", "文档点击量", "文档评分人数", "文档评分结果", "文档表单别名", "文档额外标记" };
	/**
	 * 文档内置字段对应的get方法。
	 */
	protected static final Map<String, Method> BUILTIN_ITEM_METHODS = new HashMap<String, Method>(BUILTIN_ITEM_NAMES.length);
	static {
		Method m = null;
		for (String s : BUILTIN_ITEM_NAMES) {
			try {
				m = Document.class.getMethod("get" + StringUtil.toPascalCase(s));
			} catch (SecurityException e) {
				FileLogger.error(e);
			} catch (NoSuchMethodException e) {
				FileLogger.error(e);
			}
			BUILTIN_ITEM_METHODS.put(s, m);
		}
	}

	/**
	 * 检查指定的字段名称是否文档内置字段名称。
	 * 
	 * <p>
	 * 内置字段是指文档对象的原生属性，如名称{@link Document#getName()}、摘要{@link Document#getAbstract()}等。<br/>
	 * 系统将这些固有属性也作为文档的字段，其类型为内置类型{@link FieldType#Builtin}，具有这种类型的字段称为内置字段。<br/>
	 * 内置字段的字段名（即文档的属性名），其规则为相应的getter方法去掉get前缀并将其首字母小写（如果属性名不是全大写的话），此名称有别于文档表单字段名（表单字段名通常以“fld_”作为前缀）。<br/>
	 * 如文档名称对应的getter为“{@link #getName()}”，则内置字段名或属性名为“name”；如文档UNID对应的getter为“{@link #getUNID()}”，则内置字段名或属性名为“UNID”；其余依此类推。<br/>
	 * </p>
	 * 
	 * @param fieldName
	 * @return
	 */
	public static boolean isBuiltinField(String fieldName) {
		for (int i = 0; i < BUILTIN_ITEM_NAMES.length; i++) {
			if (BUILTIN_ITEM_NAMES[i].equalsIgnoreCase(fieldName)) return true;
		}
		return false;
	}

	/**
	 * 检查指定的字段名称是否文档内置字段名称且字段值类型为原始值类型，如int、boolean等。
	 * 
	 * @param fieldName
	 * @return
	 */
	public static boolean isBuiltinFieldPrimitive(String fieldName) {
		for (int i = 0; i < BUILTIN_ITEM_NAMES.length; i++) {
			if (BUILTIN_ITEM_NAMES[i].equalsIgnoreCase(fieldName)) {
				Method m = BUILTIN_ITEM_METHODS.get(BUILTIN_ITEM_NAMES[i]);
				if (m != null) {
					return m.getReturnType().isPrimitive();
				} else {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * 构造一个系统默认空白文档对象实例。
	 * 
	 * <p>
	 * 应采用此方法返回一个空白文档而不是使用“new Document();”获取文档实例。
	 * </p>
	 * 
	 * @return {@link Document}
	 */
	public static Document newBlankDocument() {
		Document doc = new Document();
		return doc;
	}

	private Map<String, Field> m_items = null; // 文档字段名和文档字段对象一一对应的字典集合。
	private List<StringPair> m_configResources = null; // 文档绑定的应用程序信息
	private List<Log> m_logs = new ArrayList<Log>(); // 文档包含的日志列表
	private List<Opinion> m_opinions = new ArrayList<Opinion>(); // 文档包含的意见列表
	private String m_configResource = null; // 文档原始绑定的文档配置信息
	private int m_state = DocumentState.New.getIntValue(); // 文档状态值
	private DocumentType m_type = DocumentType.Normal; // 文档类型
	private String m_abstract = null; // 文档搜索摘要信息
	private String m_title = null; // 文档所属模块下级分类的标题名称
	private String m_businessTitle = null; // 文档所属业务标题名称
	private int m_processSequence = 0; // 文档当前的流水序号数值
	private int m_businessSequence = 0; // 文档当前的业务序号数值
	private int m_processYear = 0; // 文档流水号年份
	private int m_businessYear = 0; // 文档业务号年份s
	private int m_clickCount = 0; // 文档点击数
	private int m_rankCount = 0; // 文档评分的人数
	private int m_rankValue = 0; // 文档评分的总分数
	private String m_formAlias = null; // 表单别名
	private String m_tag = null; // 分类标记

	/**
	 * 缺省构造器。
	 */
	protected Document() {
		super();
		this.setSecurity(new Security(this.getUNID()));
		int year = 0;
		String created = this.getCreated();
		if (created != null && created.length() > 4) year = StringUtil.getValueInt(created.substring(0, 4), 0);
		if (year == 0) year = StringUtil.getValueInt(new DateTime().getYear(), 0);
		if (this.m_businessYear == 0) this.setBusinessYear(year);
		if (this.m_processYear == 0) this.setProcessYear(year);
	}

	/**
	 * 初始化文档字段。
	 */
	protected void initItems() {
		if (this.m_items == null) {
			// 设置表单字段
			Form f = this.getForm();
			if (f == null) throw new RuntimeException("无法获取文档所属表单信息！");
			List<Item> formItems = f.getItems();
			if (formItems == null || formItems.isEmpty()) throw new RuntimeException("文档表单中不包含任何字段！");
			this.m_items = new HashMap<String, Field>(formItems.size() + BUILTIN_ITEM_NAMES.length);
			for (Item x : formItems) {
				Field field = new Field(x, FieldType.Form, null);
				field.setPersistenceState(PersistenceState.New);
				this.m_items.put(x.getItemName().toLowerCase(), field);
			}
			// 设置内置字段
			String builtinItemName = null;
			String builtinItemValue = null;
			for (int i = 0; i < BUILTIN_ITEM_NAMES.length; i++) {
				builtinItemName = BUILTIN_ITEM_NAMES[i];
				this.m_items.put(builtinItemName.toLowerCase(), new Field(builtinItemName, FieldType.Builtin, builtinItemValue, BUILTIN_ITEM_CAPTIONS[i]));
			}
		}
	}

	/**
	 * 获取文档包含的所有文档字段信息列表。
	 * 
	 * @return List&lt;Field&gt;
	 */
	public List<Field> getFields() {
		List<Field> list = new ArrayList<Field>(this.m_items.values());
		java.util.Collections.sort(list);
		return list;
	}

	private static final String CheckInnerCallClsName = "com.tansuosoft.discoverx.";
	private static final String thisClsName = Document.class.getName();

	/**
	 * 检查方法调用是否为内部调用。
	 * 
	 * @param methodName
	 * @return boolean
	 */
	protected boolean checkInnerCall(String methodName) {
		StackTraceElement stack[] = Thread.currentThread().getStackTrace();
		StackTraceElement el = null;
		for (int i = 3; i < stack.length; i++) {
			el = stack[i];
			if (!el.getClassName().startsWith(thisClsName)) { return el.getClassName().startsWith(CheckInnerCallClsName); }
		}
		return false;
	}

	/**
	 * 获取指定字段名对应字段的{@link PersistenceState}。
	 * 
	 * <p>
	 * <strong>此方法仅用于系统内部使用。</strong>
	 * </p>
	 * 
	 * @param fieldName
	 * @return {@link PersistenceState}中的某一个枚举值或null（找不到对应字段时）。
	 */
	public PersistenceState getFieldPersistenceState(String fieldName) {
		if (!this.checkInnerCall("getFieldPersistenceState")) throw new RuntimeException("此方法只能由系统内部调用！");

		if (fieldName == null || fieldName.length() == 0) return null;
		Field f = this.m_items.get(fieldName.toLowerCase());
		return (f == null ? null : f.getPersistenceState());
	}

	/**
	 * 设置指定字段名对应字段的{@link PersistenceState}结果。
	 * 
	 * <p>
	 * <strong>此方法仅用于系统内部使用。</strong>
	 * </p>
	 * 
	 * @param fieldName
	 * @param ps
	 */
	public void setFieldPersistenceState(String fieldName, PersistenceState ps) {
		if (!this.checkInnerCall("setFieldPersistenceState")) throw new RuntimeException("此方法只能由系统内部调用！");

		if (fieldName == null || fieldName.length() == 0) return;
		Field f = this.m_items.get(fieldName.toLowerCase());
		if (f != null) f.setPersistenceState(ps);
	}

	/**
	 * 重置所有字段{@link PersistenceState}为指定结果。
	 * 
	 * <p>
	 * <strong>此方法仅用于系统内部使用。</strong>
	 * </p>
	 * 
	 * @param ps
	 */
	public void resetFieldsPersistenceState(PersistenceState ps) {
		if (!this.checkInnerCall("resetFieldsPersistenceState")) throw new RuntimeException("此方法只能由系统内部调用！");

		for (Field f : this.m_items.values()) {
			if (f == null) continue;
			f.setPersistenceState(ps);
		}
	}

	/**
	 * 根据文档包含的字段名称设置其对应的字段值。
	 * 
	 * <p>
	 * <strong>此方法仅用于系统内部新建或者反序列化文档时使用且不设置内置字段值，请使用{@link Document#replaceItemValue(String, String)}来更改文档字段值！</strong>
	 * </p>
	 * 
	 * @param itemName
	 * @param itemValue
	 */
	public void setFieldValue(String itemName, String itemValue) {
		if (!this.checkInnerCall("setFieldValue")) throw new RuntimeException("此方法只能由系统内部调用！");

		if (itemName == null || itemName.length() == 0) return;

		initItems();

		String itemNameLowerCase = itemName.toLowerCase();
		Field field = null;
		Item item = null;

		field = this.m_items.get(itemNameLowerCase);
		// 内置字段不处理
		if (field != null && field.getFieldType() == FieldType.Builtin) return;

		if (field == null) { // 找不到Field对象时可能是原先存在的字段后来表单配置更改的时候被删除。
			Item tmpItem = new Item();
			tmpItem.setAlias(itemName);
			field = new Field(tmpItem, FieldType.Unknown, itemValue);
			field.setPersistenceState(PersistenceState.Raw);
			m_items.put(itemNameLowerCase, field);
		}

		item = field.getItem();
		if (item == null) { // 找不到Item，则说明有异常。
			throw new RuntimeException("找不到“" + itemName + "”对应的文档表单字段信息！");
		}

		boolean isNew = this.checkState(DocumentState.New);
		field.setPersistenceState(isNew ? PersistenceState.New : PersistenceState.Raw);
		field.setValue(itemValue);
	}

	/**
	 * 用新的字段值(itemValue)替换文档中(itemName)指定的表单字段的字段值。
	 * 
	 * <p>
	 * 如果表单中没有指定的字段则会新建一个未知类型({@link FieldType#Unknown})的字段并设置其值为指定的值，这个新建的字段将在文档保存时保存到数据库且在文档从数据库中反序列化时被读取出来。
	 * </p>
	 * <p>
	 * 调用此方法后，如果字段值改变，将导致字段（{@link Field}的{@link PersistenceState}变更。
	 * </p>
	 * 
	 * @param itemName String 字段名，必须。
	 * @param itemValue String 新设置的字段值的文本表示方式。
	 * @return String 返回字段原来的字段值，如果找不到与itemName匹配的字段名，则返回null。
	 */
	public String replaceItemValue(String itemName, String itemValue) {
		if (this.m_items == null || itemName == null || itemName.length() == 0) return null;
		Field old = this.m_items.get(itemName.toLowerCase());
		if (old == null) {
			Item item = new Item();
			item.setAlias(itemName);
			Field newone = new Field(item, FieldType.Unknown, itemValue);
			newone.setPersistenceState(PersistenceState.New);
			this.m_items.put(itemName.toLowerCase(), newone);
			return null;
		}

		String oldValue = old.getValue();

		// 判断是否字段值实际内容有发生变化
		boolean changed = true;
		if (oldValue == null && itemValue == null) {
			changed = false;
		} else {
			if (oldValue == null) oldValue = "";
			if (itemValue == null) itemValue = "";
			changed = !(oldValue.equals(itemValue));
		}

		Field newone = (old.getFieldType() == FieldType.Builtin ? new Field(old.getName(), old.getFieldType(), itemValue, old.getCaption()) : new Field(old.getItem(), old.getFieldType(), itemValue));
		if (changed) {
			newone.setPersistenceState(old.getPersistenceState() == PersistenceState.New ? PersistenceState.New : PersistenceState.Update);
		}
		this.m_items.put(itemName.toLowerCase(), newone);

		return oldValue;
	}

	/**
	 * 追加字段名指定的单个字段值到原有字段值。
	 * 
	 * <p>
	 * 如果表单中没有指定的字段则会新建一个未知类型({@link FieldType#Unknown})的字段并设置其值为指定的值，这个新建的字段将在文档保存时保存到数据库且在文档从数据库中反序列化时被读取出来。
	 * </p>
	 * <p>
	 * 追加时使用字段定义的多值分隔符作为多个字段值的分隔符。
	 * </p>
	 * 
	 * @param itemName String，字段名，必须。
	 * @param singleItemValue String，单个字段值的文本表示，必须。
	 */
	public void appendItemValue(String itemName, String singleItemValue) {
		if (this.m_items == null || itemName == null || itemName.length() == 0) return;
		Field field = this.m_items.get(itemName.toLowerCase());
		if (field != null && field.getFieldType() == FieldType.Builtin) return;
		if (field == null) {
			Item item = new Item();
			item.setAlias(itemName);
			item.setDelimiter(",");
			Field newone = new Field(item, FieldType.Unknown, singleItemValue);
			newone.setPersistenceState(PersistenceState.New);
			this.m_items.put(itemName.toLowerCase(), newone);
			return;
		}

		String oldValue = field.getValue();
		String newValue = null;
		if (oldValue == null || oldValue.length() == 0) {
			newValue = singleItemValue;
		} else {
			newValue = String.format("%1$s%2$s%3$s", oldValue, field.getItem().getDelimiter(), singleItemValue);
		}

		field.setValue(newValue);
		field.setPersistenceState(field.getPersistenceState() == PersistenceState.New ? PersistenceState.New : PersistenceState.Update);
	}

	/**
	 * 判断文档当前是否存在名为itemName指定的字段。
	 * 
	 * <p>
	 * 对于已经新建或保存过的历史文档，如果之后其所属表单中的字段信息有新建或改名过，则可能返回false。
	 * </p>
	 * 
	 * @param itemName 字段名。
	 * @return boolean 包含则返回true，否则返回false。
	 */
	public boolean hasItem(String itemName) {
		if (itemName == null || itemName.length() == 0) return false;
		Field f = this.m_items.get(itemName.toLowerCase());
		return (f != null);
	}

	/**
	 * 获取当前文档中指定字段名称对应的字段资源。
	 * 
	 * <p>
	 * 对于已经新建或保存过的历史文档，如果之后其所属表单中的字段信息有新建或改名过，则可能返回null。
	 * </p>
	 * 
	 * @param itemName 字段名。
	 * @return {@link Item} 如果没有匹配的字段或者传入的是内置字段名，则返回null。
	 */
	public Item getItem(String itemName) {
		if (itemName == null || itemName.length() == 0 || this.m_items == null) return null;
		Field f = this.m_items.get(itemName.toLowerCase());
		return (f == null ? null : f.getItem());
	}

	/**
	 * 获取当前文档中指定字段名称对应的字段值的字符串表现形式。
	 * 
	 * <p>
	 * 对于已经新建或保存过的历史文档，如果之后其所属表单中的字段信息有新建或改名过，则可能返回null。
	 * </p>
	 * <p>
	 * 如果是内置字段，则返回内置字段值的字符串表现形式。
	 * </p>
	 * 
	 * @param itemName
	 * @return String 如果没有匹配的字段或字段值为null，则返回null。
	 */
	public String getItemValue(String itemName) {
		if (this.m_items == null || itemName == null || itemName.length() == 0) return null;
		Field f = this.m_items.get(itemName.toLowerCase());
		if (f == null) return null;
		if (f.getFieldType() == FieldType.Builtin) {
			Object obj = null;
			Method m = BUILTIN_ITEM_METHODS.get(itemName);
			try {
				obj = (m == null ? null : m.invoke(this));
			} catch (Exception e) {
				FileLogger.error(e);
			}
			return StringUtil.getValueString(obj, null);
		}
		return f.getValue();
	}

	/**
	 * 获取当前文档中指定字段名称对应的字段值的字符串数组表现形式，字段必须是多值。
	 * 
	 * <p>
	 * 对于已经新建或保存过的历史文档，如果之后其所属表单中的字段信息有新建或改名过，则可能返回null。
	 * </p>
	 * <p>
	 * 如果是内置字段，则返回一个值的数组，其值为内置字段值的字符串表现形式。
	 * </p>
	 * 
	 * @param itemName
	 * @return String[] 如果没有匹配的字段或字段值为null，则返回null，如果字段只有一个值，则返回包含一个值的数组。
	 */
	public String[] getItemValues(String itemName) {
		if (this.m_items == null || itemName == null || itemName.length() == 0) return null;
		Field f = this.m_items.get(itemName.toLowerCase());
		if (f == null) return null;
		if (f.getFieldType() == FieldType.Builtin) {
			Object obj = null;
			Method m = BUILTIN_ITEM_METHODS.get(itemName);
			try {
				obj = (m == null ? null : m.invoke(this));
			} catch (Exception e) {
				FileLogger.error(e);
			}
			String s = StringUtil.getValueString(obj, null);
			if (s == null) return null;
			String[] strs = new String[1];
			strs[0] = s;
			return strs;
		}
		String raw = f.getValue();
		if (raw == null) return null;
		if (raw.length() == 0) {
			String[] strs = new String[1];
			strs[0] = raw;
			return strs;
		}
		Item item = f.getItem();
		String delimiter = (item == null || item.getDelimiter() == null || item.getDelimiter().length() == 0 ? Item.DEFAULT_DELIMITER : item.getDelimiter());
		String[] strs = StringUtil.splitString(raw, delimiter);
		return strs;
	}

	/**
	 * 获取文档包含的日志列表。
	 * 
	 * @return List&lt;Log&gt;
	 */
	public List<Log> getLogs() {
		return this.m_logs;
	}

	/**
	 * 设置文档包含的日志列表。
	 * 
	 * @param logs List&lt;Log&gt;
	 */
	public void setLogs(List<Log> logs) {
		this.m_logs = logs;
	}

	/**
	 * 获取文档包含的意见列表。
	 * 
	 * @return List&lt;Opinion&gt;
	 */
	public List<Opinion> getOpinions() {
		return this.m_opinions;
	}

	/**
	 * 设置文档包含的意见列表。
	 * 
	 * @param opinions List&lt;Opinion&gt;
	 */
	public void setOpinions(List<Opinion> opinions) {
		this.m_opinions = opinions;
	}

	/**
	 * 获取文档原始绑定的文档配置信息。
	 * 
	 * <p>
	 * 原始绑定的文档配置信息必须以“application:{所属直接应用的别名或UNID}”开头(默认“application:”部分可以省略)，完整格式可以为“application:对应资源UNID,资源目录2:对应UNID...”(开头的“application:”部分可以省略)。<br/>
	 * 原始绑定的文档配置信息一般在文档构造(创建或初始化)时由调用者提供。
	 * </p>
	 * 
	 * @return String
	 */
	public String getConfigResource() {
		return this.m_configResource;
	}

	/**
	 * 设置文档原始绑定的文档配置信息。
	 * 
	 * <p>
	 * 在构造新文档时，需设置此属性的值，对于已有文档，随意修改此值可能导致异常。
	 * </p>
	 * 
	 * @param configResource String
	 */
	public void setConfigResource(String configResource) {
		this.m_configResource = configResource;
		if (this.m_configResource == null || this.m_configResource.length() == 0) return;
		List<String> resstrs = new ArrayList<String>();
		int currentIndex = 0;
		int nextSeparator = 0;
		char separator = ',';
		while ((nextSeparator = this.m_configResource.indexOf(separator, currentIndex)) != -1) {
			resstrs.add(this.m_configResource.substring(currentIndex, nextSeparator));
			currentIndex = nextSeparator + 1;
		}
		resstrs.add(this.m_configResource.substring(currentIndex));
		String dir = null;
		String unidstr = null;
		int pos = -1;
		this.m_configResources = new ArrayList<StringPair>();
		for (String s : resstrs) {
			if (s == null || s.length() == 0) continue;
			pos = s.indexOf(':');
			if (pos > 0) {
				dir = s.substring(0, pos);
				unidstr = s.substring(pos + 1);
				this.m_configResources.add(new StringPair(dir, unidstr));
			} else {
				dir = ResourceDescriptorConfig.getInstance().getResourceDirectory(Application.class);
				this.m_configResources.add(new StringPair(dir, unidstr));
			} // else end
		} // for end
	} // func end

	/**
	 * 获取与文档绑定的配置信息列表集合。
	 * 
	 * @return List&lt;StringPair&gt; 返回{@link StringPair}列表集合，文档配置信息中包含的资源的资源目录作为Key、资源UNID作为Value。
	 */
	public List<StringPair> getConfigResources() {
		return this.m_configResources;
	}

	/**
	 * 获取并返回与文档绑定的配置信息列表集合中资源目录名为directory指定的资源。
	 * 
	 * @param directory
	 * @return Resource，如果没有匹配的结果则返回null。
	 */
	public Resource getConfigResource(String directory) {
		if (directory == null || directory.isEmpty() || m_configResources == null || m_configResources.isEmpty()) return null;
		for (StringPair sp : m_configResources) {
			if (sp == null || directory.equalsIgnoreCase(sp.getKey())) continue;
			return ResourceContext.getInstance().getResource(sp.getValue(), directory);
		}
		return null;
	}

	/**
	 * 获取文档状态值。
	 * 
	 * <p>
	 * 状态值可能是{@link DocumentState}中某一个或几个值的组合。<br/>
	 * 新增文档的状态值必须为:{@link DocumentState#New}，保存后的文档状态必须包含{@link DocumentState#Normal}，多个状态值可以相加。
	 * </p>
	 * 
	 * @return int
	 */
	public int getState() {
		return this.m_state;
	}

	/**
	 * 设置文档状态值。
	 * 
	 * @param state int
	 */
	public void setState(int state) {
		this.m_state = state;
	}

	/**
	 * 检查文档是否具有指定的文档状态。
	 * 
	 * @param state
	 * @return boolean 如果具有则返回true，否则返回false。
	 */
	public boolean checkState(DocumentState state) {
		if (state.equals(DocumentState.New)) {
			if (this.m_state == 0) return true;
			return (DocumentState.Normal.getIntValue() & this.m_state) != DocumentState.Normal.getIntValue();
		}
		return (state.getIntValue() & this.m_state) == state.getIntValue();
	}

	/**
	 * 追加一个state对应的文档状态到文档已有状态中。
	 * 
	 * @param state DocumentState
	 */
	public void appendState(DocumentState state) {
		this.m_state |= state.getIntValue();
	}

	/**
	 * 获取文档类型。
	 * 
	 * <p>
	 * 默认为普通文档:{@link DocumentType#Normal}。
	 * </p>
	 * 
	 * @return DocumentType
	 */
	public DocumentType getType() {
		return this.m_type;
	}

	/**
	 * 设置文档类型。
	 * 
	 * @param type DocumentType
	 */
	public void setType(DocumentType type) {
		this.m_type = type;
	}

	/**
	 * 获取文档搜索摘要信息。
	 * 
	 * <p>
	 * 摘要信息获取规则：<br/>
	 * 首先，如果文档所属直接模块中有配置自定义文档摘要提供类信息({@link Application#getDocumentAbstractProviderClassName()})，则使用那个类对应的对象提供的结果作为搜索摘要结果。<br/>
	 * 如果所属应用中没有配置摘要提供类，那么检查是否有系统全局摘要提供类，它配置在CommonConfig.xml中且参数名为“global.documentAbstractProviderClassName”，参数值为摘要提供实现类全限定类名。<br/>
	 * 如果通过以上方式获取不到有效摘要值，则使用文档所包含表单中设置为可摘要信息的所有字段（{@link Item#getSummarizable()}）的字段值串接为搜索摘要结果；<br/>
	 * 如果都获取不到有效结果，则文档摘要值为空串。
	 * </p>
	 * <p>
	 * 摘要实现类要实现{@link DocumentPropertyValueProvider}接口。
	 * </p>
	 * <p>
	 * 可以使用提供类并在其中挂接第三方分词系统以提供更准确、更高效的关键词搜索体验。
	 * </p>
	 * 
	 * @return String
	 */
	public String getAbstract() {
		String providerResult = null;
		int appCount = this.getApplicationCount();
		Application app = null;

		app = this.getApplication(appCount);

		String providerClassName = (app == null ? null : app.getDocumentAbstractProviderClassName());
		if (providerClassName == null || providerClassName.isEmpty()) providerClassName = CommonConfig.getInstance().getValue("global.documentAbstractProviderClassName");
		if (providerClassName != null && providerClassName.length() > 0) {
			try {
				DocumentPropertyValueProvider provider = Instance.newInstance(providerClassName, DocumentPropertyValueProvider.class);
				if (provider != null) providerResult = provider.providePropertyValue(this);
			} finally {
			}
		}
		if (providerResult != null) {
			this.m_abstract = providerResult;
		} else {
			Form form = this.getForm();
			StringBuffer str = new StringBuffer();
			if (form == null) return this.m_abstract;
			List<Item> items = form.getItems();
			if (items == null || items.isEmpty()) return this.m_abstract;
			for (Item item : items) {
				if (item.getSummarizable()) {
					String v = this.getItemValue(item.getItemName());
					if (v == null || v.length() == 0) continue;
					str.append(str.length() > 0 ? " " : "").append(v);
				}
			}
			if (str.length() > 1) this.m_abstract = str.substring(0, str.length() - 1);
		}
		if (this.m_abstract == null || this.m_abstract.trim().length() == 0) this.m_abstract = "";
		return this.m_abstract;
	}

	/**
	 * 获取文档所属根模块（主模块）的名称。
	 * 
	 * <p>
	 * 如果要获取模块完整层次名称，可以通过{@link Resource#getCategory()}获取。<br/>
	 * 如果文档属于层次化的多个模块，则使用半角反斜杆“\”分隔多层模块名称（按根模块到直接模块从左到右的顺序）。
	 * </p>
	 * <p>
	 * 比如文档主模块名称为“发文管理”，下级分类模块名称为“部门发文”，文件字（直接业务模块：{@link #getBusinessTitle()}）名称为“发字”，那么：<br/>
	 * {@link Resource#getCategory()}返回：“发文管理\部门发文\发字”；<br/>
	 * {@link #getTitle()}返回：“发文管理”；<br/>
	 * {@link #getBusinessTitle()}返回：“发字”。<br/>
	 * </p>
	 * <p>
	 * 文档所属直接模块名称名称通过{@link Document#getBusinessTitle()}属性获取。
	 * </p>
	 * 
	 * @see Document#getBusinessTitle()
	 * 
	 * @return String
	 */
	public String getTitle() {
		return this.m_title;
	}

	/**
	 * 设置文档所属根模块（主模块）的名称。
	 * 
	 * @param title String
	 */
	public void setTitle(String title) {
		this.m_title = title;
	}

	/**
	 * 获取文档所属直接模块名称。
	 * 
	 * <p>
	 * 文档所属根模块（主模块）名称通过{@link #getTitle()}获取。
	 * </p>
	 * <p>
	 * 文档所属完整模块层次名称通过{@link Resource#getCategory()}获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getBusinessTitle() {
		return this.m_businessTitle;
	}

	/**
	 * 设置文档所属直接模块名称。
	 * 
	 * @param title String
	 */
	public void setBusinessTitle(String title) {
		this.m_businessTitle = title;
	}

	/**
	 * 获取文档当前的流水序号数值。
	 * 
	 * @return int
	 */
	public int getProcessSequence() {
		return this.m_processSequence;
	}

	/**
	 * 设置文档当前的流水序号数值。
	 * 
	 * @param seq int
	 */
	public void setProcessSequence(int seq) {
		this.m_processSequence = seq;
	}

	/**
	 * 获取文档当前的业务序号数值。
	 * 
	 * <p>
	 * 对于公文一般为文号中的序号数值。
	 * </p>
	 * 
	 * @return int
	 */
	public int getBusinessSequence() {
		return this.m_businessSequence;
	}

	/**
	 * 设置文档当前的业务序号数值。
	 * 
	 * @param seq void
	 */
	public void setBusinessSequence(int seq) {
		this.m_businessSequence = seq;
	}

	/**
	 * 获取或设置文档流水号年份。
	 * 
	 * @return int
	 */
	public int getProcessYear() {
		return this.m_processYear;
	}

	/**
	 * 设置文档流水号年份。
	 * 
	 * @param year int
	 */
	public void setProcessYear(int year) {
		this.m_processYear = year;
	}

	/**
	 * 获取文档业务号年份。
	 * 
	 * <p>
	 * 对于公文一般为文号中的年份数值。
	 * </p>
	 * 
	 * @return int
	 */
	public int getBusinessYear() {
		return this.m_businessYear;
	}

	/**
	 * 设置文档业务号年份。
	 * 
	 * @param year int
	 */
	public void setBusinessYear(int year) {
		this.m_businessYear = year;
	}

	/**
	 * 根据文档所属应用程序中配置的业务号模式({@link Application#getBusinessSequencePattern()})获取其最终结果。
	 * 
	 * <p>
	 * 从直接应用程序到根应用程序逐级向上查找，并返回第一个找到的有效模式的结果。<br/>
	 * 如果文档存在名为“fld_businesspatternresult”的字段，那么文档保存前会自动将此字段值设置为此方法返回的结果。
	 * </p>
	 * 
	 * @return String
	 */
	public String getBusinessPatternResult() {
		int appCnt = this.getApplicationCount();
		for (int i = appCnt; i > 0; i--) {
			Application app = this.getApplication(i);
			if (app != null) {
				String templet = app.getBusinessSequencePattern();
				if (templet != null && templet.length() > 0) return this.getFormatItemValue(templet, "");
			}
		}
		return null;
	}

	/**
	 * 根据文档所属应用程序中配置的流水号模式({@link Application#getProcessSequencePattern()})获取其最终结果。
	 * 
	 * <p>
	 * 从直接应用程序到根应用用程序逐级向上查找，并返回第一个找到的有效模式的结果。<br/>
	 * 如果文档存在名为“fld_processpatternresult”的字段，那么文档保存前会自动将此字段值设置为此方法返回的结果。
	 * </p>
	 * 
	 * @return String
	 */
	public String getProcessPatternResult() {
		int appCnt = this.getApplicationCount();
		for (int i = appCnt; i > 0; i--) {
			Application app = this.getApplication(i);
			if (app != null) {
				String templet = app.getProcessSequencePattern();
				if (templet != null && templet.length() > 0) return this.getFormatItemValue(templet, "");
			}
		}
		return null;
	}

	/**
	 * 获取文档点击数。
	 * 
	 * @return int
	 */
	public int getClickCount() {
		return this.m_clickCount;
	}

	/**
	 * 设置文档点击数。
	 * 
	 * @param clickCount int
	 */
	public void setClickCount(int clickCount) {
		this.m_clickCount = clickCount;
	}

	/**
	 * 增加一个文档点击量计数。
	 */
	public void increaseClickCount() {
		this.m_clickCount++;
	}

	/**
	 * 获取给文档评分的人数。
	 * 
	 * @return int
	 */
	public int getRankCount() {
		return this.m_rankCount;
	}

	/**
	 * 设置给文档评分的人数。
	 * 
	 * @param rankCount int
	 */
	public void setRankCount(int rankCount) {
		this.m_rankCount = rankCount;
	}

	/**
	 * 获取文档评级总分数。
	 * 
	 * @return int
	 */
	public int getRankValue() {
		return this.m_rankValue;
	}

	/**
	 * 设置文档评级总分数。
	 * 
	 * @param clickCount int
	 */
	public void setRankValue(int rankValue) {
		this.m_rankValue = rankValue;
	}

	/**
	 * 获取文档评分最终平均结果。
	 * 
	 * <p>
	 * {@link #getRankValue()}/{@link #getRankCount()}的结果。
	 * </p>
	 * 
	 * @return double
	 */
	public double getRankResult() {
		return (double) this.m_rankValue / this.m_rankCount;
	}

	/**
	 * 获取文档所属表单别名。
	 * 
	 * @return String
	 */
	public String getFormAlias() {
		if (this.m_formAlias == null || this.m_formAlias.length() == 0) {
			Form f = this.getForm();
			if (f != null) this.m_formAlias = f.getAlias();
		}
		return this.m_formAlias;
	}

	/**
	 * 设置文档所属表单别名。
	 * 
	 * @param formAlias String
	 */
	public void setFormAlias(String formAlias) {
		this.m_formAlias = formAlias;
		m_formUNID = null;
	}

	private String m_formUNID = null;

	/**
	 * 获取文档使用的表单对象实例。
	 * 
	 * <p>
	 * 如果文档已经通过{@link #setFormAlias(String)}设置了表单别名，则返回此别名对应的表单对象。<br/>
	 * 否则通过文档绑定的应用程序获取表单，规则如下：<br/>
	 * 系统先从文档所属直接应用程序获取绑定的表单，如果找不到对应表单则向上一级应用程序获取表单并一直到找到有效表单为止，如果查找到根应用程序还没有找到有效表单，则会抛出运行时异常。
	 * </p>
	 * <p>
	 * 文档所属应用程序可能有多级但只能绑定一个表单。
	 * </p>
	 * <p>
	 * 如果文档类型为配置文档({@link DocumentType#Profile})，那么直接从所属根应用程序的{@link Application#getProfileForm()}获取绑定的表单。
	 * </p>
	 * 
	 * @return Form
	 */
	public Form getForm() {
		Form form = null;
		ResourceContext rc = ResourceContext.getInstance();
		int appCnt = this.getApplicationCount();
		Application app = null;
		String unid = null;
		if (this.m_type == DocumentType.Profile) { // 配置文档
			app = this.getApplication(1);
			if (app != null) {
				unid = app.getProfileForm();
				form = rc.getResource(unid, Form.class);
			}
		} else { // 非配置文档
			if (m_formUNID != null && m_formUNID.length() > 0) {
				form = rc.getResource(m_formUNID, Form.class);
				if (form != null) return form;
			}
			if (m_formAlias != null && m_formAlias.length() > 0) {
				String resourceLocation = CommonConfig.getInstance().getResourcePath();
				ResourceDescriptor rp = ResourceDescriptorConfig.getInstance().getResourceDescriptor(Form.class);
				if (rp != null && resourceLocation != null && resourceLocation.length() > 0) {
					resourceLocation = resourceLocation + rp.getDirectory();
					File file = new File(resourceLocation);
					if (file.exists() && file.isDirectory()) {
						String[] files = file.list();
						Form f = null;
						for (String x : files) {
							if (x.length() != 36 || !x.toLowerCase().endsWith(".xml")) continue;
							f = rc.getResource(x.substring(0, x.length() - 4), Form.class);
							if (f == null || !m_formAlias.equalsIgnoreCase(f.getAlias())) continue;
							form = f;
							break;
						}
					}
				}
			}
			if (form == null) {
				for (int i = appCnt; i >= 1; i--) {
					app = this.getApplication(i);
					if (app == null) continue;
					unid = app.getForm();
					if (unid == null) continue;
					form = rc.getResource(unid, Form.class);
					if (form != null) break;
				}
			}
			if (form == null) throw new RuntimeException("无法获取文档所属表单！");
			m_formUNID = form.getUNID();
		}
		return form;
	}

	/**
	 * 获取文档标记信息。
	 * 
	 * <p>
	 * 标记一般用于记录文档的额外的分类信息。<br/>
	 * 多个标记用半角分号分隔。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTag() {
		return this.m_tag;
	}

	/**
	 * 设置文档标记信息。
	 * 
	 * @param tag String
	 */
	public void setTag(String tag) {
		this.m_tag = tag;
	}

	private List<Application> apps = null;

	/**
	 * 获取文档所属应用模块层次的个数。
	 * 
	 * <p>
	 * 一个文档只能属于一个根应用模块，但是根应用模块下可以包含下级应用模块（也可以不包含下级应用模块，也就是只有一个根应用模块，这时候返回1）。
	 * </p>
	 * <p>
	 * 比如发文（根应用模块）包含单位发文（二级应用模块）、部门发文（二级应用模块），而单位/部门发文下又包含不同的文件字（第三级应用模块）。
	 * </p>
	 * 
	 * @return int 如果只包含根应用模块，则返回1，否则返回最高级别数，比如包含三级，则返回3。
	 */
	public int getApplicationCount() {
		if (m_configResources == null || m_configResources.isEmpty()) throw new RuntimeException("文档配置信息不存在！");
		ResourceDescriptorConfig resourceDescriptorConfig = ResourceDescriptorConfig.getInstance();
		String directory = resourceDescriptorConfig.getResourceDirectory(Application.class);
		if (directory == null) throw new RuntimeException("无法从资源描述配置中获取应用程序资源目录名！");
		String unid = null;
		for (StringPair s : this.m_configResources) {
			if (s != null && directory.equalsIgnoreCase(s.getKey())) {
				unid = s.getValue();
				break;
			}
		}
		if (unid == null || unid.isEmpty()) throw new RuntimeException("无法获取文档所属直接应用程序信息！");
		Application app = ResourceContext.getInstance().getResource(unid, Application.class);
		if (app == null) throw new RuntimeException("无法获取文档所属直接应用程序资源，可能是目标应用程序资源不存在或者被删除！");
		apps = new ArrayList<Application>();
		while (app != null) {
			apps.add(app);
			String punid = app.getPUNID();
			if (punid == null || punid.isEmpty()) break;
			app = ResourceContext.getInstance().getResource(punid, Application.class);
		}
		Collections.reverse(apps);
		int cnt = apps.size();
		return cnt;
	}

	/**
	 * 获取级别数指定的文档所属的应用文档对象实例。
	 * 
	 * <p>
	 * 通过{@link #getApplication(int)}并传入{@link #getApplicationCount()}的返回值来获取文档所属直接应用。
	 * </p>
	 * 
	 * @param level int 表示级别，比如1则返回根应用模块，2则返回第二级应用模块，依此类推，如果level大于最高级别数或小于1则返回null。
	 * @return Application 如果只包含根应用模块，则返回1，否则返回最高级别数，比如包含三级，则返回3。
	 */
	public Application getApplication(int level) {
		int cnt = getApplicationCount();
		int idx = level - 1;
		if (cnt <= 0 || idx < 0 || idx >= cnt) return null;
		Application app = apps.get(idx);
		apps.clear();
		apps = null;
		return app;
	}

	/**
	 * 返回文档所有安全信息，包括所属直接模块中定义的安全信息。
	 * 
	 * @return Security
	 */
	public Security getSecurityAll() {
		Security security = new Security(this.getUNID());
		security.merge(this.getSecurity());

		int appCount = this.getApplicationCount();
		Application app = this.getApplication(appCount);
		Security documentDefaultSecurity = (app == null ? null : app.getDocumentDefaultSecurity());
		if (documentDefaultSecurity != null) security.merge(documentDefaultSecurity);

		return security;
	}

	/**
	 * 根据字段值格式化模板获取格式化的字段值。
	 * 
	 * <p>
	 * 字段名格式化模板示例：<br/>
	 * <i>{fld_subject}({category})表示名称格式为：“{文档fld_subject字段值}({文档所属模块名})”。</i>
	 * </p>
	 * 
	 * @param template 字段值格式化模板，如果忽略，则返回null。
	 * @param defaultReturen 如果没有提供模板或者获取到空字符串，则返回defaultReturen。
	 * @return
	 */
	public String getFormatItemValue(String template, String defaultReturen) {
		if (template == null || template.length() == 0) return defaultReturen;
		String result = template;

		Map<String, String> map = new HashMap<String, String>();
		String find = null;
		boolean hasFormatSymbol = false;
		String regExpSymbol = "/[-]?\\d+,[^/]{1}/";
		String regExpVariable = "\\{[^\\}]+\\}";
		Pattern ps = Pattern.compile(regExpSymbol);
		Pattern pv = Pattern.compile(regExpVariable);
		Matcher ms = ps.matcher(result);
		Matcher mv = pv.matcher(result);
		String itemValue = null;
		String itemName = null;
		String formatSymbol = null;
		int formatLength = 0;
		String formatPadding = "";
		while (mv != null && mv.find()) {
			itemName = result.substring(mv.start() + 1, mv.end() - 1);
			itemValue = StringUtil.getValueString(this.getItemValue(itemName), "");
			hasFormatSymbol = false;
			find = result.substring(mv.start(), mv.end());
			while (ms != null && ms.find()) {
				if (ms.start() == mv.end()) {
					hasFormatSymbol = true;
					formatSymbol = result.substring(ms.start() + 1, ms.end() - 1);
					formatLength = StringUtil.getValueInt(StringUtil.stringLeft(formatSymbol, ","), 0);
					formatPadding = StringUtil.stringRight(formatSymbol, ",");
					if (formatLength == 0) hasFormatSymbol = false;
					if (hasFormatSymbol) {
						find += result.substring(ms.start(), ms.end());
						if (formatLength > 0) {
							StringBuilder sb = new StringBuilder(itemValue);
							for (int i = sb.length(); i < formatLength; i++) {
								sb.insert(0, formatPadding);
							}
							itemValue = sb.toString();
						} else {
							StringBuilder sb = new StringBuilder();
							int itemValueLength = itemValue.length();
							for (int i = 0; i < (-formatLength); i++) {
								if ((itemValueLength - i - 1) >= 0) sb.insert(0, itemValue.charAt(itemValueLength - i - 1));
								else sb.insert(0, formatPadding);
							}
							itemValue = sb.toString();
						}
						break;
					}
				}// if end
			}// while end
			map.put(find, itemValue);
			ms.reset();
		}// while end
		if (map.isEmpty()) return result;
		for (String x : map.keySet()) {
			result = result.replace(x, map.get(x));
		}
		if (result == null || result.length() == 0) return defaultReturen;

		return result;
	}

	public static final String SUBJECT_ITEM_NAME = "fld_subject";
	public static final String NO_SUBJECT = "[无标题]";

	/**
	 * 重载getName:获取文档名称。
	 * 
	 * <p>
	 * 默认格式为：<br/>
	 * 如果文档所属直接模块中有配置自定义文档名称提供信息，则以文档名称提供类{@link DocumentPropertyValueProvider#providePropertyValue(Document, Object...)}返回的结果作为文档名称；<br/>
	 * 否则默认为：“{文档的“fld_subject”字段的值}”；<br/>
	 * 如果不存在名为“fld_subject”的字段则为:“{作者姓名}于{创建日期}发起的{所属直接应用程序名称}”，如：用户1于2014-01-15发起的发文管理。<br/>
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.model.Application#getDocumentNameProviderClassName()
	 * @see com.tansuosoft.discoverx.model.DocumentPropertyValueProvider
	 * @see com.tansuosoft.discoverx.model.Resource#getName()
	 */
	@Override
	public String getName() {
		String result = null;
		String providerResult = null;
		int appCount = this.getApplicationCount();
		Application app = this.getApplication(appCount);

		String providerClassName = (app == null ? null : app.getDocumentNameProviderClassName());
		if (providerClassName == null || providerClassName.isEmpty()) providerClassName = CommonConfig.getInstance().getValue("global.documentNameProviderClassName");
		if (providerClassName != null && providerClassName.length() > 0) {
			if (providerClassName.indexOf('{') >= 0 && this.m_items != null) { // 通过字段值模板获取。
				providerResult = getFormatItemValue(providerClassName.replace("{name}", ""), "");
			} else { // 提供类
				try {
					DocumentPropertyValueProvider provider = Instance.newInstance(providerClassName, DocumentPropertyValueProvider.class);
					if (provider != null) providerResult = provider.providePropertyValue(this);
				} catch (Exception ex) {
					FileLogger.error(ex);
				}
			}
		}
		if (providerResult != null) {
			result = providerResult;
		} else {
			if (this.hasItem(SUBJECT_ITEM_NAME)) {
				result = StringUtil.getValueString(this.getItemValue(SUBJECT_ITEM_NAME), NO_SUBJECT);
			} else {
				result = String.format("%1$s于%2$s发起的%3$s", ParticipantHelper.getFormatValue(this.getCreator(), "cn"), StringUtil.stringLeft(this.getCreated(), " "), this.getBusinessTitle());
			}
		}
		return result;
	}
}

