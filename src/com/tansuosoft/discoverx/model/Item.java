/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于描述文档包含的某个字段相关属性的资源类。
 * 
 * <p>
 * 字段资源一般包含于表单资源中，孤立的字段资源没有意义。
 * </p>
 * <p>
 * 配置说明：<br/>
 * 字段名称（英文名）通过资源别名（Alias属性）获取，如“fld_subject”、“fld_keyword”等。字段名称必须以fld_作为前缀。<br/>
 * 字段标题通过资源名称（Name属性）获取，如“主题”、“关键词”等。
 * </p>
 * <p>
 * 它的{@link Resource#getPUNID()}结果为所属门户表单的UNID。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Item extends Resource implements Cloneable {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 4439968502000702999L;

	/**
	 * 多值字段默认使用的多值分隔符：“,”(半角逗号)。
	 */
	public static final String DEFAULT_DELIMITER = ",";

	/**
	 * 缺省构造器
	 */
	public Item() {
	}

	private ItemType m_type = ItemType.Text; // 字段值的系统数据类型，缺省为文本。
	private ItemTypeOption m_valueDataTypeOption = null; // 字段值数据类型的额外选项，可选。
	private ItemDisplay m_display = ItemDisplay.Default; // 字段在用户界面中如何显示。
	private String m_displayImplement = null; // 显示字段的自定义实现类的全限定类名，可选。
	private String m_readonlyDisplayImplement = null;// 显示字段的只读方式的实现类的全限定类名，可选。
	private String m_delimiter = null; // 多值分隔符，默认为null（表示内容为单值处理）。
	private String m_defaultValue = null; // 字段缺省值变量或常数。
	private ItemValueComputationMethod m_computationMethod = ItemValueComputationMethod.Open; // 计算公式的计算方式。
	private String m_computationExpression = null; // 字段值计算公式。
	private String visibleExpression = null; // 隐藏表达式
	private boolean m_outputHiddenField = true; // 隐藏公式为结果false时，字段是否输出到浏览器。
	private String m_readonlyExpression = null; // 字段是否只读的公式
	private DataSourceType m_dataSourceType = DataSourceType.None; // 绑定的数据源类型。
	private DataSource m_dataSource = null; // 绑定的数据源信息。
	private String m_linkageItem = null; // 层次数据源联动选择字段
	private String m_valueStoreItem = null; // 数据源值保存字段
	private ItemDataSourceInput m_dataSourceInput = ItemDataSourceInput.Default; // 数据源输入方式。
	private String m_customDataSourceImplement = null; // 自定义数据源实现类的全限定类名。
	private String m_jsValidator = null; // 字段值的JS校验公式。
	private boolean m_required = false; // 字段值是否必填。
	private String m_jsTranslator = null; // 字段值的JS转换公式。
	private String m_group = null; // 绑定的字段分组UNID。
	private int m_row = 0; // 字段显示时所在行位置。
	private int m_column = 0; // 字段显示时所在列位置。
	private int m_width = 0; // 用于设置字段显示宽度。
	private int m_captionWidth = 0; // 用于设置字段显示宽度中字段标题部分的宽度。
	private int m_height = 0; // 字段显示高度（单位：像素）。
	private int m_captionHeight = 0; // 字段标题显示高度，可选。
	private boolean m_summarizable = false; // 字段值是否可被搜索摘要，默认为false。
	private boolean m_control = false; // 是否控制字段。
	private int m_size = 0; // 字段值最大长度。
	private List<HtmlAttribute> m_htmlAttributes = null;

	/**
	 * 返回字段标题。
	 * 
	 * @see Item#setCaption(String)
	 * @return String 标题文本
	 */
	public String getCaption() {
		return this.getName();
	}

	/**
	 * 返回字段名（字段资源的别名）。
	 */
	public String getItemName() {
		return this.getAlias();
	}

	/**
	 * 返回字段值的系统数据类型，缺省为文本。
	 * 
	 * @return ItemType 字段值的系统数据类型，缺省为文本。
	 */
	public ItemType getType() {
		if (m_type == null) m_type = ItemType.Text;
		return m_type;
	}

	/**
	 * 设置字段值的系统数据类型，缺省为文本。
	 * 
	 * @param valueDataType 字段值的系统数据类型，缺省为文本。
	 * @see type ItemType
	 */
	public void setType(ItemType type) {
		this.m_type = type;
	}

	/**
	 * 返回字段值数据类型的额外选项，可选。
	 * 
	 * @return ItemTypeOption
	 */
	public ItemTypeOption getDataTypeOption() {
		return this.m_valueDataTypeOption;
	}

	/**
	 * 设置字段值数据类型的额外选项，可选。
	 * 
	 * <p>
	 * 字段值为不同数据类型的字段，可能有一些不同的额外选项。
	 * </p>
	 * <p>
	 * 比如数字类型的字段，可以指示是整数还是小数和小数点位置等。 <br/>
	 * 比如日期时间型字段，可以提供格式选贤，还可以定义是只有日期呢还是完整的日期时间或者只有时间。<br/>
	 * 比如人员类型的字段，可以指示是全称（层次名）还是简称（只有姓名）等。
	 * </p>
	 * 
	 * @param valueDataTypeOption ItemTypeOption
	 */
	public void setDataTypeOption(ItemTypeOption valueDataTypeOption) {
		this.m_valueDataTypeOption = valueDataTypeOption;
	}

	/**
	 * 返回字段在用户界面中如何显示。
	 * 
	 * <p>
	 * 默认为“{@link ItemDisplay#Default}”，表示由系统自动根据类型选择合适的显示方式。
	 * </p>
	 * <p>
	 * 如果字段属于表格型（{@link ItemGroupType#Table}）的分组（{@link ItemGroup}），则只能以{@link ItemDisplay#SingleLineText}、{@link ItemDisplay#SingleComb}、 {@link ItemDisplay#Radio} 这三种方式显示，其余显示方式将被自动显示为单行文本框。<br/>
	 * 使用下拉选择框或单选按钮显示时，必须配置非参与者类型的数据源以提供选项列表。
	 * </p>
	 * 
	 * @return ItemDisplay
	 */
	public ItemDisplay getDisplay() {
		return this.m_display;
	}

	/**
	 * 设置字段在用户界面中如何显示。
	 * 
	 * @param display ItemDisplay
	 */
	public void setDisplay(ItemDisplay display) {
		this.m_display = display;
	}

	/**
	 * 返回字段自定义显示的实现类的全限定类名，可选。
	 * 
	 * @return String
	 */
	public String getDisplayImplement() {
		return this.m_displayImplement;
	}

	/**
	 * 设置字段自定义显示的实现类的全限定类名，可选。
	 * 
	 * <p>
	 * 如果Display属性值的值不是设置为自定义，那么此属性被忽略。
	 * </p>
	 * <p>
	 * 系统会根据字段值类型等信息自动选择最合适的方式显示域内容，如果要自定义显示方式，则配置此属性。
	 * </p>
	 * 
	 * @see Item#setDisplay(ItemDisplay)
	 * @param displayImplement String
	 */
	public void setDisplayImplement(String displayImplement) {
		this.m_displayImplement = displayImplement;
	}

	/**
	 * 返回显示字段的只读方式时的实现类的全限定类名，可选。
	 * 
	 * @return String
	 */
	public String getReadonlyDisplayImplement() {
		return this.m_readonlyDisplayImplement;
	}

	/**
	 * 设置显示字段的只读方式的实现类的全限定类名，可选。
	 * 
	 * @param readonlyDisplayImplement String
	 */
	public void setReadonlyDisplayImplement(String readonlyDisplayImplement) {
		this.m_readonlyDisplayImplement = readonlyDisplayImplement;
	}

	/**
	 * 返回多值分隔符。
	 * 
	 * <p>
	 * 默认为null表示字段值内容作为单值处理，多值分隔符如果不为null或空串，表示允许多值，请不要使用单个值本身会出现的字符作为多值分隔符以免导致混乱。
	 * </p>
	 * <p>
	 * 对于一些自动作为多值的字段（比如属于表格型分组的字段），其默认分隔符如果不配置，则使用半角逗号作为多值分隔符。<br/>
	 * </p>
	 * 
	 * @return String
	 */
	public String getDelimiter() {
		return m_delimiter;
	}

	/**
	 * 设置多值分隔符。
	 * 
	 * @param delimiter
	 */
	public void setDelimiter(String delimiter) {
		this.m_delimiter = delimiter;
	}

	/**
	 * 获取字段缺省值表达式。
	 * 
	 * @return String
	 */
	public String getDefaultValue() {
		return m_defaultValue;
	}

	/**
	 * 设置字段缺省值表达式。
	 * 
	 * <p>
	 * 如果有多值请用字段指定的分隔符分隔多个缺省值。文档新增时会自动设置文档所属字段的缺省值。
	 * </p>
	 * 
	 * @param defaultValue String
	 */
	public void setDefaultValue(String defaultValue) {
		this.m_defaultValue = defaultValue;
	}

	/**
	 * 返回字段值计算公式。
	 * 
	 * @return String 字段值计算公式。
	 */
	public String getComputationExpression() {
		return m_computationExpression;
	}

	/**
	 * 设置字段值计算表达式（公式）。
	 * 
	 * <p>
	 * <b>注意：如果指定了字段值计算表达式那么每次包含这种字段的文档打开、 保存时都会重新计算字段计算值公式的计算结果并设置到字段值中，因此一般用 于只读字段或者用于冗余保存某些用于显示等特殊用途的字段值。</b>
	 * </p>
	 * 
	 * @param computationExpression 字段值计算表达式。
	 */
	public void setComputationExpression(String computationExpression) {
		this.m_computationExpression = computationExpression;
	}

	/**
	 * 返回计算公式的计算方式（公式）。
	 * 
	 * <p>
	 * 默认为{@link ItemValueComputationMethod#Open}，只有配置了有效的计算公式时此属性才有意义，否则被忽略。
	 * </p>
	 * 
	 * @see Item#setComputationExpression(String)
	 * @see ItemValueComputationMethod
	 * @return ItemValueComputationMethod
	 */
	public ItemValueComputationMethod getComputationMethod() {
		return this.m_computationMethod;
	}

	/**
	 * 设置计算公式的计算方式。
	 * 
	 * @see Item#getComputationMethod()
	 * @param computationMethod ItemValueComputationMethod
	 */
	public void setComputationMethod(ItemValueComputationMethod computationMethod) {
		this.m_computationMethod = computationMethod;
	}

	/**
	 * 返回是否把隐藏公式结果为true的字段输出到浏览器的隐藏字段中（以input type="hidden"的方式），缺省为true。
	 * 
	 * @see Item#setHideFormula(String)
	 * @return boolean
	 */
	public boolean getOutputHiddenField() {
		return m_outputHiddenField;
	}

	/**
	 * 设置是否把可见公式（或隐藏公式）结果为false的字段输出到浏览器的隐藏字段中（以input type="hidden"的方式），缺省为true。
	 * 
	 * <p>
	 * 默认无可见公式情况下，字段是自动可见的，如果字段显示条件公式有配置则必须返回true字段才可见。<br/>
	 * 但是如果{@link Item#getOutputHiddenField()}属性值为true则仍以浏览器隐藏字段（input type="hidden"的方式）的方式提供浏览器页面访问此字段信息的方式。
	 * </p>
	 * <p>
	 * 隐藏公式结果为true时，如果此属性值设置为false，则浏览器里没有这个字段的信息。
	 * </p>
	 * <p>
	 * 隐藏公式结果为true时，如果此属性值设置为true，则浏览器里以input type="hidden"的方式输出这个字段的信息。
	 * </p>
	 * <p>
	 * 隐藏公式结果为false或没有配置隐藏公式，则忽略此属性。
	 * </p>
	 * <p>
	 * 此属性值对控制字段({@link Item#getControl()})的影响也有类似的效果。
	 * </p>
	 * 
	 * @param outputHiddenField boolean
	 */
	public void setOutputHiddenField(boolean outputHiddenField) {
		this.m_outputHiddenField = outputHiddenField;
	}

	/**
	 * 返回字段是否只读的公式。
	 * 
	 * @see Item#setReadonlyExpression(String)
	 * @return Reference 字段是否只读的公式
	 */
	public String getReadonlyExpression() {
		return m_readonlyExpression;
	}

	/**
	 * 设置字段是否只读的表达式。
	 * 
	 * <p>
	 * 默认情况下，字段在文档中显示出来的状态是可编辑的，如果此公式返回true则指示字段应以只读方式显示，不配置或者公式结果为false则字段处于编辑状态。<br/>
	 * 如果文档的状态为只读，那么所有字段都只读而不理会此公式的有无和结果。
	 * </p>
	 * 
	 * @param readonlyExpression 字段是否只读的公式
	 */
	public void setReadonlyExpression(String readonlyExpression) {
		this.m_readonlyExpression = readonlyExpression;
	}

	/**
	 * 返回绑定的数据源类型。
	 * 
	 * @return DataSourceType
	 */
	public DataSourceType getDataSourceType() {
		return this.m_dataSourceType;
	}

	/**
	 * 设置绑定的数据源类型。
	 * 
	 * <p>
	 * 默认为DataSourceType.None（表示没有数据源绑定）。
	 * </p>
	 * 
	 * @see DataSourceType
	 * @param dataSourceType DataSourceType
	 */
	public void setDataSourceType(DataSourceType dataSourceType) {
		this.m_dataSourceType = dataSourceType;
	}

	/**
	 * 返回绑定的数据源信息。
	 * 
	 * @see Item#setDataSource(DataSource)
	 * @return DataSource
	 */
	public DataSource getDataSource() {
		return this.m_dataSource;
	}

	/**
	 * 设置绑定的数据源信息。
	 * 
	 * <p>
	 * 只有数据源类型不为{@link DataSourceType#None}时才有意义，否则被忽略。
	 * </p>
	 * <p>
	 * 默认情况下(除了组合框、单选、复选按钮等类型的字段)，绑定了数据源的字段在浏览器中以编辑方式显示时会有一个选择标记用于选择数据源。
	 * </p>
	 * <p>
	 * 对于下拉组合框等类型的字段，必须配置数据源，否则无法获取下拉关键字列表。
	 * </p>
	 * <p>
	 * 属于表格型（{@link ItemGroupType#Table}）分组（{@link ItemGroup}）的字段的数据源输入方式是通过单击表格标题（第一行）中对应的列来弹出选择数据源的选择框。
	 * </p>
	 * 
	 * @see Item#setDataSourceType(DataSourceType)
	 * @param dataSource DataSource
	 */
	public void setDataSource(DataSource dataSource) {
		this.m_dataSource = dataSource;
	}

	/**
	 * 返回层次数据源联动选择的下级字段名。
	 * 
	 * <p>
	 * 返回结果为半角分号分隔的多个字段名(也可能只有一个下级字段名)，比如“fld_1;fld_2...”，此时表示当前字段对应于层次数据源的第一层，“fld_1”对应于层次数据源的第二层，“fld_2”对应于层次数据源的第三层，后续字段名依此类推。
	 * </p>
	 * 
	 * @return String
	 */
	public String getLinkageItem() {
		return this.m_linkageItem;
	}

	/**
	 * 设置层次数据源联动选择的下级字段名。
	 * 
	 * <p>
	 * 使用半角分号分隔多个字段名，包含在联动选择中的字段只支持下拉组合框和通过选择框输入的呈现方式，联动选择的下级字段不能再包含任何数据源和自定义的选择框配置。
	 * </p>
	 * <p>
	 * 如果设置了关联选择的字段名信息，则目标数据源类型只能为资源数据源并将其{@link DataSourceResource#setFullTree(boolean)}设置为false。
	 * </p>
	 * 
	 * @param linkageItem String
	 */
	public void setLinkageItem(String linkageItem) {
		this.m_linkageItem = linkageItem;
	}

	/**
	 * 返回数据源值保存字段名。
	 * 
	 * @return String
	 */
	public String getValueStoreItem() {
		return this.m_valueStoreItem;
	}

	/**
	 * 设置数据源值保存字段名。
	 * 
	 * <p>
	 * 如果不设置此属性值（默认），则默认把数据源的结果值保存于当前字段本身的字段值中。<br/>
	 * 在以选择框方式选择数据源时，如果设置了有效的数据源值保存字段名，则将数据源的显示值保存于当前字段本身的字段值中，而把数据源的结果值保存于此处设置的字段名对应的字段值中。<br/>
	 * 在以组合框方式选择数据源时，如果设置了有效的数据源值保存字段名，则将数据源的显示值保存于此处设置的字段名对应的字段值中，而把数据源的结果值保存于当前字段本身的字段值中。<br/>
	 * 此处设置的字段名对应的字段不能再绑定数据源且多值分隔符与当前字段相同<br/>
	 * 通常这里设置的字段名对应的字段一般是控制字段或隐藏字段，但是必须确保其在浏览器端能够访问（即{@link Item#getOutputHiddenField()}必须为true）。<br/>
	 * 如果当前字段的值格式（{@link DataSource#getValueFormat()}）和显示格式（{@link DataSource#getTitleFormat()}）相同，则此配置没有意义。
	 * </p>
	 * 
	 * @param valueStoreItem String
	 */
	public void setValueStoreItem(String valueStoreItem) {
		this.m_valueStoreItem = valueStoreItem;
	}

	/**
	 * 返回绑定数据源的字段值的输入/编辑方式。
	 * 
	 * <p>
	 * 默认为“{@link ItemDataSourceInput#Default}”，表示由系统综合字段配置选项自动提供一种方式。<br/>
	 * 系统处理规则为：<br/>
	 * <ul>
	 * <li>字段显示类型（{@link Item#getDisplay()}）为组合框({@link ItemDisplay#SingleCombo}/{@link ItemDisplay#MultipleCombo})、<br/>
	 * 单选/多选({@link ItemDisplay#Radio}/{@link ItemDisplay#CheckBox}))按钮，则忽略此配置。</li>
	 * <li>字段值类型({@link Item#getType()})为日期时间({@link ItemType#DateTime})时，系统采用{@link ItemDataSourceInput#SelectorHotspot}的方式在字段标题之后显示一个选择标记。。</li>
	 * <li>数据源类型{@link Item#getDataSourceType()}为常数({@link DataSourceType#Constant})或自定义({@link DataSourceType#Custom})且字段不显示为组合框、单选、多选按钮，<br/>
	 * 则使用{@link ItemDataSourceInput#AutoComplete}的方式。</li>
	 * <li>在不满足前述规则时，系统采用此属性的配置结果。</li>
	 * </ul>
	 * 如果{@link Item#getDataSource()}没有提供有效配置结果则此属性可能被忽略或导致运行时错误。
	 * </p>
	 * 
	 * <p>
	 * 对于没有绑定数据源但是字段类型自动隐含了数据源的字段，也可以使用此属性来自定义字段值的输入/编辑方式。<br/>
	 * 自动隐含数据源的字段类型包括：
	 * <ul>
	 * <li>日期时间：{@link ItemType#DateTime}</li>
	 * <li>图片：{@link ItemType#Image}</li>
	 * <li>链接：{@link ItemType#Link}</li>
	 * </ul>
	 * </p>
	 * 
	 * @return ItemDataSourceInput
	 */
	public ItemDataSourceInput getDataSourceInput() {
		return this.m_dataSourceInput;
	}

	/**
	 * 设置绑定数据源的字段值的输入/编辑方式。
	 * 
	 * @param dataSourceInput ItemDataSourceInput
	 */
	public void setDataSourceInput(ItemDataSourceInput dataSourceInput) {
		this.m_dataSourceInput = dataSourceInput;
	}

	/**
	 * 返回自定义数据源实现类的全限定类名。
	 * 
	 * @return String
	 */
	public String getCustomDataSourceImplement() {
		return this.m_customDataSourceImplement;
	}

	/**
	 * 设置自定义数据源实现类的全限定类名。
	 * 
	 * <p>
	 * 只有数据源类型为DataSourceType.Custom时才有意义，否则被忽略。
	 * </p>
	 * <p>
	 * 此类必须继承自{@link com.tansuosoft.discoverx.web.ui.item.DSValuesProvider}。
	 * </p>
	 * 
	 * @param customDataSourceImplement String
	 */
	public void setCustomDataSourceImplement(String customDataSourceImplement) {
		this.m_customDataSourceImplement = customDataSourceImplement;
	}

	/**
	 * 返回字段值的JS校验公式。
	 * 
	 * @see Item#setJsValidator(String)
	 * @return String 字段值的JS校验公式。
	 */
	public String getJsValidator() {
		return m_jsValidator;
	}

	/**
	 * 设置字段值的JS校验公式。
	 * 
	 * <p>
	 * 文档提交（文档保存）前在浏览器客户端校验本字段域的值是否合法。配置为js函数名。
	 * </p>
	 * <p>
	 * 多个校验公式请用半角分号分隔，如：validator1;validator2
	 * </p>
	 * 
	 * @param jsValidator 字段值的JS校验公式。
	 */
	public void setJsValidator(String jsValidator) {
		this.m_jsValidator = jsValidator;
	}

	/**
	 * 返回字段值是否必填。
	 * 
	 * <p>
	 * 默认为false。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getRequired() {
		return this.m_required;
	}

	/**
	 * 设置字段值是否必填。
	 * 
	 * @param required boolean
	 */
	public void setRequired(boolean required) {
		this.m_required = required;
	}

	/**
	 * 返回字段值的JS转换公式。
	 * 
	 * @see Item#setJsTranslator(String)
	 * @return String 字段值的JS转换公式。
	 */
	public String getJsTranslator() {
		return m_jsTranslator;
	}

	/**
	 * 设置字段值的JS转换公式。
	 * 
	 * <p>
	 * 用于文档提交时（文档保存）时在浏览器客户端转换本字段的值为新的值， 比如用户输入阿拉伯数字，保存时自动转换为中文大写数字。配置为js函数名，在字段值校验后执行。
	 * </p>
	 * 
	 * @param jsTranslator 字段值的JS转换公式。
	 */
	public void setJsTranslator(String jsTranslator) {
		this.m_jsTranslator = jsTranslator;
	}

	/**
	 * 返回绑定的字段分组UNID，可选。
	 * 
	 * @see Item#setGroup(String)
	 * @return String 字段组合名称
	 */
	public String getGroup() {
		return m_group;
	}

	/**
	 * 设置绑定的字段分组UNID，可选。
	 * 
	 * <p>
	 * 属于同一字段组的字段以整组为一行或一个段落（子表单）的方式呈现（具体呈现方式由{@link ItemGroupType}指定）。
	 * </p>
	 * <p>
	 * 也可以用控制类型的字段组来定义字段的通用行为，如统一隐藏、统一只读等。
	 * </p>
	 * 
	 * @see ItemGroup
	 * @see ItemGroupType
	 * @param group 字段组合名称。
	 */
	public void setGroup(String group) {
		this.m_group = group;
	}

	/**
	 * 返回字段显示时所在行位置。
	 * 
	 * <p>
	 * 在包含于段落型字段组的字段中，此属性会被作为分组内部的位置（可以重新从1开始设置）。
	 * </p>
	 * <p>
	 * 在包含于表格型字段组的字段中，此属性建议配置为与字段组本身行位置相同的值。
	 * </p>
	 * 
	 * @return int 字段显示时所在行位置。
	 */
	public int getRow() {
		return m_row;
	}

	/**
	 * 设置字段显示时所在行位置。
	 * 
	 * @param row 字段显示时所在行位置。
	 */
	public void setRow(int row) {
		this.m_row = row;
	}

	/**
	 * 返回字段显示时所在列位置。
	 * 
	 * @return int 字段显示时所在列位置。
	 */
	public int getColumn() {
		return m_column;
	}

	/**
	 * 设置字段显示时所在列位置。
	 * 
	 * <p>
	 * 配置时建议每行不超过4列。
	 * </p>
	 * <p>
	 * 在包含于表格型或者段落型的字段组的字段中，此属性会被作为分组内部的位置。
	 * </p>
	 * 
	 * @param column 字段显示时所在列位置。
	 */
	public void setColumn(int column) {
		this.m_column = column;
	}

	/**
	 * 返回字段显示宽度（单位：像素/百分比）。
	 * 
	 * <p>
	 * 默认为0，表示由系统自动计算宽度，单位由{@link Form#getMeasurement()}的结果决定。
	 * </p>
	 * <p>
	 * 此宽度为包括字段标题和字段值控件的总宽度。
	 * </p>
	 * <p>
	 * 宽度值不能超过系统配置的文档显示最大可用内容宽度数（特别是一行有多个字段时，所有宽度总合也不能超过文档显示最大可用内容宽度数）！
	 * </p>
	 * 
	 * @see Item#setCaptionWidth(int)
	 * @see DocumentDisplay#getMaxWidth()
	 * @see com.tansuosoft.discoverx.common.CommonConfig#DEFAULT_DOCUMENT_CONTENT_WIDTH
	 * @see com.tansuosoft.discoverx.common.CommonConfig#getMaxDocumentContentWidth()
	 * @return int
	 */
	public int getWidth() {
		return m_width;
	}

	/**
	 * 设置字段显示宽度（单位：像素/百分比）。
	 * 
	 * @param width
	 */
	public void setWidth(int width) {
		this.m_width = width;
	}

	/**
	 * 返回用于设置字段显示宽度中字段标题部分的宽度。
	 * 
	 * <p>
	 * 单位：像素。<br/>
	 * 默认为0，表示使用系统默认的字段标题宽度。<br/>
	 * 字段标题显示宽度不能超过字段总宽度，如果此属性为-1，表示不显示字段标题。
	 * </p>
	 * <p>
	 * 属于表格型字段组的字段忽略此属性。
	 * </p>
	 * 
	 * @return int
	 */
	public int getCaptionWidth() {
		return this.m_captionWidth;
	}

	/**
	 * 设置用于设置字段显示宽度中字段标题部分的宽度。
	 * 
	 * @param captionWidth int
	 */
	public void setCaptionWidth(int captionWidth) {
		this.m_captionWidth = captionWidth;
	}

	/**
	 * 返回字段显示高度（单位：像素）。
	 * 
	 * <p>
	 * 默认为0，表示由系统自动计算高度。
	 * </p>
	 * <p>
	 * 属于表格型字段组的字段忽略此属性。
	 * </p>
	 * 
	 * @see Item#setHeight(int)
	 * @return int
	 */
	public int getHeight() {
		return m_height;
	}

	/**
	 * 设置字段显示高度（单位：像素）。
	 * 
	 * @param height
	 */
	public void setHeight(int height) {
		this.m_height = height;
	}

	/**
	 * 返回字段标题显示高度。
	 * 
	 * <p>
	 * 默认为0表示字段标题和字段实际控件内容在同一行，设置为大于0则表示字段标题和字段实际控件内容分两行显示（即先显示字段标题，再显示内容）。
	 * </p>
	 * <p>
	 * 属于表格型字段组的字段忽略此属性。
	 * </p>
	 * 
	 * @return int
	 */
	public int getCaptionHeight() {
		return this.m_captionHeight;
	}

	/**
	 * 设置字段标题显示高度。
	 * 
	 * @param captionHeight int
	 */
	public void setCaptionHeight(int captionHeight) {
		this.m_captionHeight = captionHeight;
	}

	/**
	 * 返回字段值是否可作为文档的搜索摘要。
	 * 
	 * <p>
	 * 默认为false，参考{@link Document#getAbstract()}中的说明。
	 * </p>
	 * <p>
	 * 标记为可摘要的字段值可以被模糊检索，一般文档主题等常见字段设置为可摘要。 注意：所有摘要字段值的字节长度合计不能超过4K，否则将被截断到4K处！
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getSummarizable() {
		return m_summarizable;
	}

	/**
	 * 设置字段值是否可作为文档的搜索摘要。
	 * 
	 * @param summarizable
	 */
	public void setSummarizable(boolean summarizable) {
		this.m_summarizable = summarizable;
	}

	/**
	 * 返回字段内容呈现时字段控件节点额外属性。
	 * 
	 * <p>
	 * 可以用此属性来配置文档在浏览器中显示时html中字段内容输出对应元素的额外属性，如样式控制、javascript事件处理等。
	 * </p>
	 * <p>
	 * 请注意属性值的配置方式，因为输出按照类似“&lt;tagName {属性名}="{属性值}"...”的方式，所以引号不正常使用，可能导致html脚本错误。
	 * </p>
	 * <p>
	 * 如果输出内容原有已经包含了相同属性名的属性信息，则把同名的属性值追加到原有属性值后面。如原有属性为onclick="doX();" 要追加的同名属性值为“doY();”，则结果为“onclick="doX(); doY();"”
	 * </p>
	 * <p>
	 * 某些类型或状态的字段可能会忽略此属性，比如从属于某个表格型字段组的字段、隐藏字段、只读字段等。
	 * </p>
	 * 
	 * @return List<HtmlAttribute>
	 */
	public List<HtmlAttribute> getHtmlAttributes() {
		return this.m_htmlAttributes;
	}

	/**
	 * 设置字段内容呈现时字段控件节点额外属性，可选。
	 * 
	 * @see Form#getHtmlAttributes()
	 * @param htmlAttributes List<HtmlAttribute>
	 */
	public void setHtmlAttributes(List<HtmlAttribute> htmlAttributes) {
		this.m_htmlAttributes = htmlAttributes;
	}

	/**
	 * 返回字段值最大长度。
	 * 
	 * @return int
	 */
	public int getSize() {
		return this.m_size;
	}

	/**
	 * 设置字段值最大长度。
	 * 
	 * <p>
	 * 默认为0表示不限制长度。
	 * </p>
	 * 
	 * @param size int
	 */
	public void setSize(int size) {
		this.m_size = size;
	}

	/**
	 * 检查字段类型是否可以作为视图查询表的列字段。
	 * 
	 * 判断依据请参考“{@link com.tansuosoft.discoverx.model.ItemType}”的说明。
	 * 
	 * @see ItemType
	 * @return boolean 可以则返回true，否则返回false。
	 */
	public boolean isFormViewTableColumn() {
		if (m_type == null) m_type = ItemType.Text;
		return (this.m_type.isFormViewTableColumn());
	}

	/**
	 * 检查字段是否可以持久保存。
	 * 
	 * 判断依据请参考“{@link com.tansuosoft.discoverx.model.ItemType}”的说明。
	 * 
	 * @see ItemType
	 * @return boolean 可以则返回true，否则返回false。
	 */
	public boolean canSave() {
		if (m_type == null) m_type = ItemType.Text;
		return (this.m_type.canSave());
	}

	/**
	 * 判断字段是否允许多值。
	 * 
	 * @see Item#getDelimiter()
	 * @return
	 */
	public boolean allowMultipleValue() {
		return (this.m_delimiter != null && this.m_delimiter.length() > 0);
	}

	/**
	 * 获取显示/隐藏表达式（公式）。
	 * 
	 * <p>
	 * 默认（不配置）则总是显示，如果配置了显示表达式，那么表达式执行结果必须为true才会显示。
	 * </p>
	 * 
	 * <p>
	 * 属于表格型字段组的字段忽略此属性。
	 * </p>
	 * 
	 * <p>
	 * 在配置中，除非特别说明，否则需要提供配置好的公式或表达式的地方，一般都是理解为表达式（即可以有一个或多个公式组合成的合法表达式），请参考“{@link com.tansuosoft.discoverx.bll.function.ExpressionParser}”中的表达式说明。
	 * </p>
	 * 
	 * @return
	 */
	public String getVisibleExpression() {
		return this.visibleExpression;
	}

	/**
	 * 设置显示/隐藏表达式（公式），可选。
	 * 
	 * @param visibleExpression String
	 */
	public void setVisibleExpression(String visibleExpression) {
		this.visibleExpression = visibleExpression;
	}

	/**
	 * 返回是否控制字段。
	 * 
	 * <p>
	 * 默认为false，表示普通字段（即可以参与表单呈现布局的字段），如果为true，则此类字段不参与表单布局呈现（即控制字段总是不显示在浏览器界面上）。<br/>
	 * 控制字段值在前端浏览器上是否输出浏览器隐藏字段信息（形如：“&lt;input type="hidden" ...&gt;”）由{@link Item#getOutputHiddenField()}的配置决定。
	 * </p>
	 * <p>
	 * 控制字段相比隐藏公式属性具有更高优先级！
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getControl() {
		return this.m_control;
	}

	/**
	 * 设置是否控制字段。
	 * 
	 * @param control boolean
	 */
	public void setControl(boolean control) {
		this.m_control = control;
	}

	/**
	 * 重载getSort：返回行*列相乘的结果。
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#getSort()
	 */
	@Override
	public int getSort() {
		return this.m_row * this.m_column;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Item x = (Item) super.clone();

		x.setControl(this.getControl());
		x.setOutputHiddenField(this.getOutputHiddenField());
		x.setRequired(this.getRequired());
		x.setSummarizable(this.getSummarizable());
		x.setCaptionHeight(this.getCaptionHeight());
		x.setCaptionWidth(this.getCaptionWidth());
		x.setColumn(this.getColumn());
		x.setHeight(this.getHeight());
		x.setRow(this.getRow());
		x.setSize(this.getSize());
		x.setWidth(this.getWidth());
		x.setComputationExpression(this.getComputationExpression());
		x.setCustomDataSourceImplement(this.getCustomDataSourceImplement());
		x.setDefaultValue(this.getDefaultValue());
		x.setDelimiter(this.getDelimiter());
		x.setDisplayImplement(this.getDisplayImplement());
		x.setGroup(this.getGroup());
		x.setJsTranslator(this.getJsTranslator());
		x.setJsValidator(this.getJsValidator());
		x.setLinkageItem(this.getLinkageItem());
		x.setReadonlyDisplayImplement(this.getReadonlyDisplayImplement());
		x.setReadonlyExpression(this.getReadonlyExpression());
		x.setValueStoreItem(this.getValueStoreItem());
		x.setVisibleExpression(this.getVisibleExpression());
		x.setComputationMethod(this.getComputationMethod());
		x.setDataSourceInput(this.getDataSourceInput());
		x.setDataSourceType(this.getDataSourceType());
		x.setDisplay(this.getDisplay());
		x.setType(this.getType());
		x.setDataSource(this.getDataSource());
		x.setDataTypeOption(this.getDataTypeOption());
		x.setHtmlAttributes(this.getHtmlAttributes());

		List<HtmlAttribute> attrs = this.getHtmlAttributes();
		if (attrs != null) {
			List<HtmlAttribute> newattrs = new ArrayList<HtmlAttribute>(attrs.size());
			for (HtmlAttribute y : attrs) {
				if (x == null) continue;
				newattrs.add((HtmlAttribute) y.clone());
			}
			x.setHtmlAttributes(newattrs);
		}

		return x;
	}
}

