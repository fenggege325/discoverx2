/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 系统字段值数据类型枚举值。
 * 
 * <p>
 * <strong>注意：枚举代表的数字如果小于0则不保存（表示占位符）的字段，枚举代表的数字小于0或大于100的数据类型对应的数据不会同步到视图索引表（即不可以被检索或作为视图列）。</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public enum ItemType implements EnumBase {
	/**
	 * 文本（1）
	 */
	Text(1),
	/**
	 * 数字（2）
	 */
	Number(2),
	/**
	 * 日期时间（3）
	 */
	DateTime(3),
	/**
	 * 图片（4）
	 */
	Image(4),
	/**
	 * HTML链接,点击时弹出新窗口或刷新当前窗口（5）
	 */
	Link(5),
	/**
	 * 人员（6）
	 */
	// Person(6),
	/**
	 * 单位或部门（7）
	 */
	// Unit(7),
	/**
	 * 地址类型（8）
	 * <p>
	 * 地址类型即可以从内部人员、部门、群组、角色、个人通讯录、公共通讯录等数据源中选择收件人地址的半角分号分隔的文本字段。
	 * </p>
	 */
	// MailAddress(8),
	/**
	 * 富文本即包含格式的文本（101）
	 * 
	 * <p>
	 * 一个表单只能包含一个富文本类型的字段。
	 * </p>
	 */
	RTF(101),
	/**
	 * 附加文件占位符类型（-1）。
	 * 
	 * <p>
	 * 一个表单只能包含一个附加文件类型的字段。
	 * </p>
	 */
	Accessory(-1),
	/**
	 * 字段分组类型（-2）。
	 */
	Group(-2);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	ItemType(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举值对应的数字返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return ItemType
	 */
	public static ItemType parse(int v) {
		for (ItemType s : ItemType.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

	/**
	 * 检查此类型字段是否可以作为视图查询表的列字段。
	 * 
	 * <p>
	 * 判断依据请参考{@link ItemType}中的说明。
	 * </p>
	 * 
	 * @return boolean 可以则返回true，否则返回false。
	 */
	public boolean isFormViewTableColumn() {
		return (this.getIntValue() <= 100 && this.getIntValue() >= 0);
	}

	/**
	 * 检查此类型字段是否可以保存。
	 * 
	 * <p>
	 * 判断依据请参考{@link ItemType}中的说明。
	 * </p>
	 * 
	 * @return boolean 可以则返回true，否则返回false。
	 */
	public boolean canSave() {
		return (this.getIntValue() >= 0);
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字格式字符串返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return ItemType
	 */
	public ItemType parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (ItemType s : ItemType.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return ItemType.valueOf(v);
		}
		return null;
	}

}

