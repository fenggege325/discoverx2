/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 描述一条具体职权（授权）信息的类。
 * 
 * <p>
 * 授权信息一般用于定义用户{@link Participant}、群组{@link Group}等资源所属的群组、角色等。<br/>
 * 比如群组可以赋予某些角色，用户可以赋予某些群组、角色等。<br/>
 * 因此他们所拥有或归属的某一条具体角色、群组等信息就表示一条职权（授权）信息，某个资源被赋予的所有职权信息一般包含于{@link Authority}。
 * </p>
 * 
 * <p>
 * 注意：群组不能再被赋予群组！
 * </p>
 * 
 * @see Authority
 * @author coca@tansuosoft.cn
 */
public class AuthorityEntry {
	private int m_securityCode = 0; // 所属群组或角色的安全编码。
	private String m_UNID = null; // 所属群组或角色的UNID。
	private String m_name = null; // 所属群组或角色的名称。
	private String m_directory = null; // 所属群组或角色的资源目录名。

	/**
	 * 缺省构造器。
	 */
	public AuthorityEntry() {
	}

	/**
	 * 返回所属群组或角色的安全编码。
	 * 
	 * @return int
	 */
	public int getSecurityCode() {
		return this.m_securityCode;
	}

	/**
	 * 设置所属群组或角色的安全编码。
	 * 
	 * @param securityCode int
	 */
	public void setSecurityCode(int securityCode) {
		this.m_securityCode = securityCode;
	}

	/**
	 * 返回所属群组或角色的UNID。
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置所属群组或角色的UNID。
	 * 
	 * @param UNID String
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回所属群组或角色的名称。
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置所属群组或角色的名称。
	 * 
	 * @param name String
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回所属群组或角色的资源目录名。
	 * 
	 * @return String
	 */
	public String getDirectory() {
		return this.m_directory;
	}

	/**
	 * 设置所属群组或角色的资源目录名。
	 * 
	 * @param directory String
	 */
	public void setDirectory(String directory) {
		this.m_directory = directory;
	}

}

