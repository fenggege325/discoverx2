/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示常数数据源信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DataSourceConstant extends DataSource {
	/**
	 * 标题和值分隔符：“｜”（半角竖线）。
	 */
	public static final char TITLE_VALUE_DELIMITER = '|';
	/**
	 * 多个配置项分隔符：“;”（半角分号）。
	 */
	public static final char VALUES_DELIMITER = ';';

	/**
	 * 缺省构造器。
	 */
	public DataSourceConstant() {
		super();
	}

	private String m_configValue = null; // 配置的数据源常数值信息，必须。

	/**
	 * 返回配置的数据源常数值信息，必须。
	 * 
	 * @return String
	 */
	public String getConfigValue() {
		return this.m_configValue;
	}

	/**
	 * 设置配置的数据源常数值信息，必须。
	 * 
	 * <p>
	 * 格式为：标题1[|值1];标题2[|值2]...;标题n[|值n]
	 * </p>
	 * <p>
	 * 说明：<strong>标题</strong>表示显示给用户的标题，<strong>值</strong>表示后台实际保存的值，值是可选的，如果不提供，则和标题一样。
	 * </p>
	 * <p>
	 * 配置注意事项：标题和值之间用“|”分隔，多个用“;”（半角分号）分隔。配置时，要么全部只有标题（没有“|值n”部分），要么必须全部指定值（可以用形如“标题|”的方式配置，这和“标题”效果一致，但是可以让某一个项包含另外指定的值）。
	 * </p>
	 * <p>
	 * 合法的：
	 * <ul>
	 * <li>标题1|值1;标题2|值2</li>
	 * <li>标题1|;标题2|值2</li>
	 * <li>标题1;标题2</li>
	 * </ul>
	 * 不合法的：
	 * <ul>
	 * <li>标题1;标题2|值2</li>
	 * </ul>
	 * </p>
	 * 
	 * @param configValue String
	 */
	public void setConfigValue(String configValue) {
		this.m_configValue = configValue;
	}
}

