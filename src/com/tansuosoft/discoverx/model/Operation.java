/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示指向处理http请求并返回处理结果的系统功能函数表达式的操作类。
 * 
 * <p>
 * 此Operation对象的{@link Operation#getExpression()}返回一个指向某个系统功能函数的操作公式表达式，如：“@Save(param:=1)”（表示调用保存操作，并提供一个名为“param”，值为“1”的参数）。<br/>
 * 此类功能扩展函数都是处理http请求，并返回统一结果“{@link OperationResult}”的操作类型的系统功能函数（即继承“{@link com.tansuosoft.discoverx.bll.function.Operation}”的实现类），它包含处理结果导向、处理结果消息等。<br/>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Operation implements Cloneable {
	/**
	 * 缺省构造器。
	 */
	public Operation() {
	}

	private String m_expression = null; // 对应功能表达式，必须。
	private String m_visibleExpression = null; // 显示条件表达式，可选。
	private String m_title = null; // 标题，可选。
	private String m_description = null; // 说明，可选。
	private int m_sort = 0; // 排序号，默认为0。
	private String m_icon = null; // 图标文件名。

	/**
	 * 返回对应系统功能函数实现表达式。
	 * 
	 * @return String
	 */
	public String getExpression() {
		return this.m_expression;
	}

	/**
	 * 设置对应系统功能函数实现表达式，必须。
	 * 
	 * <p>
	 * 参见以下类的说明：<br/>
	 * {@link com.tansuosoft.discoverx.bll.Function}<br/>
	 * {@link com.tansuosoft.discoverx.bll.Operation}<br/>
	 * {@link com.tansuosoft.discoverx.bll.ExpressionParser}<br/>
	 * </p>
	 * 
	 * @param expression String
	 */
	public void setExpression(String expression) {
		this.m_expression = expression;
	}

	/**
	 * 返回操作的显示条件表达式。
	 * 
	 * <p>
	 * 如果配置了显示条件表达式，那么必须确保表达式执行结果为true操作才会显示在操作界面上，如果没有配置表达式，那么默认总是显示操作（在流程环节中配置的则对当前审批人默认显示）。
	 * </p>
	 * 
	 * @return String
	 */
	public String getVisibleExpression() {
		return this.m_visibleExpression;
	}

	/**
	 * 设置操作的显示条件表达式，可选。
	 * 
	 * @param visibleExpression String
	 */
	public void setVisibleExpression(String visibleExpression) {
		this.m_visibleExpression = visibleExpression;
	}

	/**
	 * 返回标题，可选。
	 * 
	 * @return String
	 */
	public String getTitle() {
		return this.m_title;
	}

	/**
	 * 设置标题，可选。
	 * 
	 * @param title String
	 */
	public void setTitle(String title) {
		this.m_title = title;
	}

	/**
	 * 返回说明，可选。
	 * 
	 * <p>
	 * 不提供则默认使用此操作对应的功能函数资源的描述（{@link com.tansuosoft.discoverx.model.Function#getDescription()}）。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置说明，可选。
	 * 
	 * @param description String
	 */
	public void setDescription(String description) {
		this.m_description = description;
	}

	/**
	 * 返回排序号，默认为0。
	 * 
	 * @return int
	 */
	public int getSort() {
		return this.m_sort;
	}

	/**
	 * 设置排序号，默认为0。
	 * 
	 * @param sort int
	 */
	public void setSort(int sort) {
		this.m_sort = sort;
	}

	/**
	 * 返回图标文件名。
	 * 
	 * @return String
	 */
	public String getIcon() {
		return this.m_icon;
	}

	/**
	 * 设置图标文件名。
	 * 
	 * <p>
	 * 可选，不提供则使用系统默认图标。<br/>
	 * 配置时，可以提供直接图标文件名（表示在“~common/icons/”下），如“<strong>icon_save.gif</strong>”，也可以设置为具体路径和文件名，如：<br/>
	 * “<strong>{frame}apptest/images/test.gif</strong>”。<br/>
	 * 图标文件一般为16*16大小的gif或png文件。
	 * </p>
	 * 
	 * @param icon String
	 */
	public void setIcon(String icon) {
		this.m_icon = icon;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Operation x = new Operation();

		x.setSort(this.getSort());
		x.setDescription(this.getDescription());
		x.setExpression(this.getExpression());
		x.setIcon(this.getIcon());
		x.setTitle(this.getTitle());
		x.setVisibleExpression(this.getVisibleExpression());

		return x;
	}
}

