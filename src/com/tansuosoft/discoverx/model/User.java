/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 用户资源类。
 * 
 * <p>
 * 它包含了系统内部的一个抽象用户账号相关的所有信息。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class User extends Resource implements Securer {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 6394799189010581892L;

	/**
	 * 系统内置的超级管理员UNID。
	 */
	public static final String SUPERADMIN_USER_UNID = "038EDC126D904ECC935050BE801D8820";

	/**
	 * 系统内置超级管理员的安全编码。
	 */
	public static final int SUPERADMIN_USER_SC = 1;

	/**
	 * 系统内置的匿名用户UNID。
	 */
	public static final String ANONYMOUS_USER_UNID = "098073195B714380AA6D6A0C6CD91420";

	/**
	 * 系统内置的匿名用户安全编码。
	 */
	public static final int ANONYMOUS_USER_SC = 0;

	/**
	 * 系统内置匿名用户别名：Anonymous。
	 */
	public static final String ANONYMOUS_USER_ALIAS = "Anonymous";

	/**
	 * 新增用户或重置密码后的用户缺省密码：“12345678”。
	 */
	public static final String DEFAULT_PASSWORD = StringUtil.getMD5HashString("12345678");

	/**
	 * 系统使用的用户层次名分隔符：“\”。
	 */
	public static final String SEPARATOR = "\\";

	/**
	 * 系统使用的用户层次名分隔符：“\”。
	 */
	public static final char SEPARATOR_CHAR = '\\';

	/**
	 * 系统定义的合法有效的用于返回参与者某一部分信息的格式标记。
	 * 
	 * <p>
	 * 系统内部合法有效的格式标记说明如下（不区分大小写）：
	 * <ul>
	 * <li>cn：通用名（对用来说即用户姓名）</li>
	 * <li>fn：全名（即包含单位，部门，用户通用名的用反斜杆分隔的用户层次名）</li>
	 * <li>o：所属组织或单位名称</li>
	 * <li>ou0：所在直接部门（即和通用名最靠近的部门，如果用户没有直接部门则为组织名）</li>
	 * <li>ou1：所在一级部门</li>
	 * <li>ou2：所在二级部门</li>
	 * <li>ou3：所在三级部门</li>
	 * <li>ou4：所在四级部门</li>
	 * <li>id：UNID</li>
	 * <li>sc：安全编码</li>
	 * <li>an：别名</li>
	 * <li>cn、o、ou0、ou1、ou2、ou3、ou4等可以组合，比如“ou0\cn”</li>
	 * </ul>
	 * </p>
	 */
	public static final String[] LEGALLY_NAME_PART_FORMATS = { "ou0", "ou1", "ou2", "ou3", "ou4", "fn", "cn", "id", "sc", "an", "o" };

	/**
	 * 缺省构造器。
	 */
	public User() {
		super();
		this.setPassword(DEFAULT_PASSWORD);
	}

	private int m_securityCode = 0; // 用户的安全编码
	private String m_password = null; // 用户密码摘要，默认为12345678对应的密码摘要，必须。
	private int m_accountType = AccountType.ValidAccount.getIntValue() | AccountType.User.getIntValue(); // 登录帐号类别。
	private String m_profile = null; // 用户配置文档的UNID，可选。
	private String m_duty = null; // 用户职责，可选。
	private Authority m_authority = null; // 用户被赋予的授权信息。

	/**
	 * 获取用户安全编码。
	 * 
	 * @see com.tansuosoft.discoverx.model.Securer#getSecurityCode()
	 */
	public int getSecurityCode() {
		return this.m_securityCode;
	}

	/**
	 * 设置用户安全编码。
	 * 
	 * @see com.tansuosoft.discoverx.model.Securer#setSecurityCode(int)
	 */
	public void setSecurityCode(int code) {
		this.m_securityCode = code;
	}

	/**
	 * 返回用户密码的加密格式文本。
	 * 
	 * <p>
	 * 默认用户密码为12345678。
	 * </p>
	 * 
	 * @return String 用户密码摘要。
	 */
	public String getPassword() {
		return this.m_password;
	}

	/**
	 * 设置用户密码的加密格式文本。
	 * 
	 * @see Participant#getPassword()
	 * @param password String
	 */
	public void setPassword(String password) {
		this.m_password = password;
	}

	/**
	 * 获取帐号类别。
	 * 
	 * <p>
	 * 账号类别是AccountType枚举中值对应数字的某一个或多个组合，用户默认的登录账号类别为“{@link AccountType#ValidAccount}对应数字值 + {@link AccountType#User}对应数字值”（3）。
	 * </p>
	 * 
	 * @return int
	 */
	public int getAccountType() {
		return this.m_accountType;
	}

	/**
	 * 设置帐号类别。
	 * 
	 * @see Participant#getAccountType()
	 * @param accountType int
	 */
	public void setAccountType(int accountType) {
		this.m_accountType = accountType;
	}

	/**
	 * 返回用户绑定的配置文档的UNID，可选。
	 * 
	 * <p>
	 * 每个用户可以帮定一个配置文档。<br/>
	 * 可以通过绑定的配置文档记录用户的个性化配置信息。<br/>
	 * </p>
	 * 
	 * @return String
	 */
	public String getProfile() {
		return this.m_profile;
	}

	/**
	 * 设置用户绑定的配置文档的UNID，可选。
	 * 
	 * @param profile String
	 */
	public void setProfile(String profile) {
		this.m_profile = profile;
	}

	/**
	 * 返回用户职责，可选。
	 * 
	 * @return String
	 */
	public String getDuty() {
		return this.m_duty;
	}

	/**
	 * 设置用户职责，可选。
	 * 
	 * @param duty String
	 */
	public void setDuty(String duty) {
		this.m_duty = duty;
	}

	/**
	 * 返回用户被赋予的授权信息。
	 * 
	 * <p>
	 * 可能包含群组、角色等，也可能都不包含任何授权信息。
	 * </p>
	 * 
	 * @return Authority
	 */
	public Authority getAuthority() {
		return this.m_authority;
	}

	/**
	 * 设置用户被赋予的授权信息。
	 * 
	 * @param authority Authority
	 */
	public void setAuthority(Authority authority) {
		this.m_authority = authority;
	}

	/**
	 * 返回用户简称（即用户姓名），必须。
	 * 
	 * @return String 用户简称（即用户姓名），必须。
	 */
	public String getCN() {
		return (this.parts == null ? null : this.parts.length > 0 ? this.parts[this.parts.length - 1] : null);
	}

	/**
	 * 获取用户全名中，去掉用户名部分的组织结构层次名。
	 * 
	 * @return
	 */
	public String getOUs() {
		String o = this.getO();
		String ou1 = this.getOU1();
		String ou2 = this.getOU2();
		String ou3 = this.getOU3();
		String ou4 = this.getOU4();
		StringBuilder sb = new StringBuilder();
		if (o != null && o.length() > 0) sb.append(o);
		if (ou1 != null && ou1.length() > 0) sb.append(sb.length() > 0 ? User.SEPARATOR : "").append(ou1);
		if (ou2 != null && ou2.length() > 0) sb.append(sb.length() > 0 ? User.SEPARATOR : "").append(ou2);
		if (ou3 != null && ou3.length() > 0) sb.append(sb.length() > 0 ? User.SEPARATOR : "").append(ou3);
		if (ou4 != null && ou4.length() > 0) sb.append(sb.length() > 0 ? User.SEPARATOR : "").append(ou4);
		return sb.toString();
	}

	/**
	 * 获取用户所属组织（单位）名称。
	 * 
	 * @return String
	 */
	public String getO() {
		String part = (this.parts == null ? null : this.parts.length >= 2 ? this.parts[0] : null);
		if (part == null || part.length() == 0) { return CommonConfig.getInstance().getBelong(); }
		return part;
	}

	/**
	 * 获取用户所属一级组织单元（一级部门）。
	 * 
	 * @return String 对应级别的部门名称，如果不存在此级别部门则返回null。
	 */
	public String getOU1() {
		return (this.parts == null ? null : this.parts.length >= 3 ? this.parts[1] : null);
	}

	/**
	 * 获取用户所属二级组织单元（二级部门）。
	 * 
	 * @return String 对应级别的部门名称，如果不存在此级别部门则返回null。
	 */
	public String getOU2() {
		return (this.parts == null ? null : this.parts.length >= 4 ? this.parts[2] : null);
	}

	/**
	 * 获取用户所属三级组织单元（三级部门）。
	 * 
	 * @return String 对应级别的部门名称，如果不存在此级别部门则返回null。
	 */
	public String getOU3() {
		return (this.parts == null ? null : this.parts.length >= 5 ? this.parts[3] : null);
	}

	/**
	 * 获取用户所属四级组织单元（四级部门）。
	 * 
	 * @return String 对应级别的部门名称，如果不存在此级别部门则返回null。
	 */
	public String getOU4() {
		return (this.parts == null ? null : this.parts.length >= 6 ? this.parts[4] : null);
	}

	/**
	 * 获取用户所属直接组织单元（直接部门，即离用户姓名最近的部门）。
	 * 
	 * @return String 对应级别的部门名称，如果不存在此级别部门则返回单位名。
	 */
	public String getOU0() {
		if (this.parts == null || this.parts.length <= 2) return this.getO();
		String ret = this.parts[this.parts.length - 2];
		return (ret == null || ret.length() == 0 ? this.getO() : ret);
	}

	/**
	 * 获取用户指定级别的部门名称。
	 * 
	 * @param x int 范围为-1—6，-1表示返回所属单位名称，0表示返回所属直接部门，1—4表示返回各级部门，5表示用户姓名，6表示层次名。
	 * @return
	 */
	public String getPartX(int x) {
		switch (x) {
		case 0:
			return this.getOU0();
		case 1:
			return this.getOU1();
		case 2:
			return this.getOU2();
		case 3:
			return this.getOU3();
		case 4:
			return this.getOU4();
		case 5:
			return this.getCN();
		case 6:
			return this.getName();
		case -1:
			return this.getO();
		default:
			return null;
		}
	}

	/**
	 * 获取用户全名的层次数。
	 * 
	 * @return int
	 */
	public int getHierarchicalCount() {
		return (this.parts == null ? 0 : this.parts.length);
	}

	private String[] parts = null; // 用户的全名分解后的各部分。

	/**
	 * 解析用户全名为各个独立的部分。
	 */
	private void parseParts() {
		String name = this.getName();
		if (name == null || name.length() == 0) return;
		int currentIndex = 0;
		int nextSeparator = -1;
		List<String> result = new ArrayList<String>();
		while ((nextSeparator = name.indexOf(SEPARATOR, currentIndex)) != -1) {
			result.add(name.substring(currentIndex, nextSeparator));
			currentIndex = nextSeparator + 1;
		}
		result.add(name.substring(currentIndex));
		this.parts = new String[result.size()];
		result.toArray(this.parts);
		result.clear();
		result = null;
	}

	/**
	 * 重载setName
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		super.setName(name);
		this.parseParts();
	}

	/**
	 * 是否匿名用户。
	 * 
	 * @return boolean
	 */
	public boolean isAnonymous() {
		return (User.ANONYMOUS_USER_UNID.equalsIgnoreCase(this.getUNID()));
	}

	/**
	 * 是否系统默认的超级管理员用户。
	 * 
	 * @return boolean
	 */
	public boolean isSystemBuiltinAdmin() {
		return (User.SUPERADMIN_USER_UNID.equalsIgnoreCase(this.getUNID()));
	}

	/**
	 * 获取匿名用户对应的User对象实例。
	 * 
	 * @return Participant
	 */
	public static User getAnonymous() {
		Resource r = null;
		try {
			r = ResourceContext.getInstance().getResource(User.ANONYMOUS_USER_UNID, User.class);
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		if (r == null || !(r instanceof User)) {
			User anonymous = new User();
			anonymous.setUNID(ANONYMOUS_USER_UNID);
			anonymous.setAlias(ANONYMOUS_USER_ALIAS);
			anonymous.setName("匿名用户");
			return anonymous;
		}
		return (User) r;
	}

	private static User m_system = null;
	private static Object m_lockObj = new Object();

	/**
	 * 获取代表系统的唯一用户资源对象实例。
	 * 
	 * @return
	 */
	protected static User getSystemUser() {
		synchronized (m_lockObj) {
			if (m_system == null) {
				m_system = new User();
				m_system.setUNID("08671F29937149048B0B1BDD0F129B69");
				m_system.setAlias("system");
				m_system.setName("系统");
				m_system.setDescription("表示系统本身的用户。");
				m_system.setSecurityCode(8);
				Authority a = new Authority(m_system.getUNID());
				a.setAuthorityEntries(new ArrayList<AuthorityEntry>());
				AuthorityEntry ae = new AuthorityEntry();
				ae.setUNID(Role.ROLE_ADMIN);
				ae.setSecurityCode(Role.ROLE_ADMIN_SC);
				ae.setDirectory(new Role().getDirectory());
				ae.setName("超级管理员");
				a.getAuthorityEntries().add(ae);
				m_system.setAuthority(a);
			}
		}
		return m_system;
	}
}

