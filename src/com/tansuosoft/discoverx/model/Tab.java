/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示定义文档内容在浏览器中分块显示标签类。
 * 
 * <p>
 * 文档内容在浏览器中显示时，一般按不同内容块放在不同标签下。
 * </p>
 * <p>
 * 比如文档内容在浏览器中显示时按照“基本信息”、“审批意见”、“审批日志”等分区显示（“基本信息”、“审批意见”等叫标签标题），单击不同标签标题切换显示不同分区的内容（比如单击“审批意见”，显示文档审批意见内容区，这些内容区也叫标签内容）。
 * </p>
 * 
 * <p>
 * 系统全局通用文档流转时刻的内容分区显示信息通过{@link DocumentDefaultTabConfig}配置类获取。
 * </p>
 * <p>
 * 系统全局通用文档发布后的内容分区显示信息通过{@link DocumentDefaultIssuedTabConfig}配置类获取。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Tab implements Cloneable {
	/**
	 * 缺省构造器。
	 */
	public Tab() {
	}

	private String m_title = null; // 标题，必须。
	private int m_sort = 0; // 排序号
	private String m_description = null; // 说明，可选。
	private TabShow m_tabShow = TabShow.ShowTabTitle; // 标签显示方式，默认为显示标签标题（TabShow.ShowTabTitle）。
	private String m_onselect = null; // 选中事件js函数。
	private String m_onleave = null; // 离开事件js函数。
	private String m_control = null; // 绑定的控件信息。
	private String m_expression = null; // 启用表达式

	/**
	 * 返回标题。
	 * 
	 * <p>
	 * 同一个页面中使用的{@link Tab}列表中的标题不能重复。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTitle() {
		return this.m_title;
	}

	/**
	 * 设置标题，必须。
	 * 
	 * @param title String
	 */
	public void setTitle(String title) {
		this.m_title = title;
	}

	/**
	 * 返回排序号。
	 * 
	 * @return int
	 */
	public int getSort() {
		return this.m_sort;
	}

	/**
	 * 设置排序号，可选。
	 * 
	 * @param sort int
	 */
	public void setSort(int sort) {
		this.m_sort = sort;
	}

	/**
	 * 返回说明，可选。
	 * 
	 * @return String
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置说明，可选。
	 * 
	 * @param description String
	 */
	public void setDescription(String description) {
		this.m_description = description;
	}

	/**
	 * 返回标签显示方式，默认为“{@link TabShow#ShowTabTitle}”。
	 * 
	 * @return {@link TabShow}
	 */
	public TabShow getTabShow() {
		return this.m_tabShow;
	}

	/**
	 * 设置标签显示方式。
	 * 
	 * @param tabShow {@link TabShow}
	 */
	public void setTabShow(TabShow tabShow) {
		this.m_tabShow = tabShow;
	}

	/**
	 * 返回选中事件js函数。
	 * 
	 * <p>
	 * 即标签选中时执行的js函数，第一个参数为选中的标签对应的js对象。
	 * </p>
	 * 
	 * @return String
	 */
	public String getOnselect() {
		return this.m_onselect;
	}

	/**
	 * 设置选中事件js函数。
	 * 
	 * @param onclick String
	 */
	public void setOnselect(String onclick) {
		this.m_onselect = onclick;
	}

	/**
	 * 返回离开事件js函数。
	 * 
	 * <p>
	 * 即标签从选中到未选中切换时执行的js函数，第一个参数为选中的标签对应的js对象。
	 * </p>
	 * 
	 * @return String
	 */
	public String getOnleave() {
		return this.m_onleave;
	}

	/**
	 * 设置离开事件js函数。
	 * 
	 * @param onleave String
	 */
	public void setOnleave(String onleave) {
		this.m_onleave = onleave;
	}

	/**
	 * 返回绑定的控件信息，必须。
	 * 
	 * <p>
	 * 此信息为呈现标签页内容的jsp文件的url路径。
	 * </p>
	 * 
	 * @return String
	 */
	public String getControl() {
		return this.m_control;
	}

	/**
	 * 设置绑定的控件信息。
	 * 
	 * <p>
	 * 同一个页面中使用的{@link Tab}列表中的控件文件名不能重复。
	 * </p>
	 * <p>
	 * 在配置时，可以使用如下变量：<br/>
	 * <ul>
	 * <li>“~”：表示根路径</li>
	 * <li>“{frame}”：表示当前使用的框架路径</li>
	 * <li>直接输入每个应用程序特有的文件名</li>
	 * </ul>
	 * 以下为符合要求的配置示例：
	 * <ul>
	 * <li>“control.jsp”或“{frame}control.jsp”：表示当前使用的框架路径下的“basic.jsp”文件</li>
	 * <li>“{frame}/controls/control.jsp”：表示当前使用的框架路径下“controls”子目录中的“control.jsp”文件</li>
	 * <li>“~controls/control.jsp”：表示当前web应用根路径下“controls”子目录中的“control.jsp”文件</li>
	 * <li>“special.jsp”：表示在应用程序特定功能目录(参考{@link Application}中的说明)下的“special.jsp”文件</li>
	 * </ul>
	 * 
	 * @param control String
	 */
	public void setControl(String control) {
		this.m_control = control;
	}

	/**
	 * 返回启用表达式。
	 * 
	 * <p>
	 * 如果没有配置表达式，则直接启用；如果配置了表达式，则需启用表达式计算结果为true才会启用
	 * </p>
	 * 
	 * @return String
	 */
	public String getExpression() {
		return this.m_expression;
	}

	/**
	 * 设置启用表达式。
	 * 
	 * @param expression String
	 */
	public void setExpression(String expression) {
		this.m_expression = expression;
	}

	/**
	 * 获取客户端json对象TabItem的唯一id。
	 * 
	 * @return
	 */
	public String getId() {
		String result = null;
		if (this.m_control == null || this.m_control.trim().length() == 0) { return String.format("id_%1$s", m_sort); }
		result = m_control.replace("~", "").replace("{frame}", "");
		int pos = result.lastIndexOf('/');
		if (pos < 0) result.lastIndexOf('\\');
		result = result.substring(pos >= 0 ? pos + 1 : 0).replace('.', '_');
		return result;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Tab x = (Tab) super.clone();

		x.setSort(this.getSort());
		x.setControl(this.getControl());
		x.setDescription(this.getDescription());
		x.setOnleave(this.getOnleave());
		x.setOnselect(this.getOnselect());
		x.setTitle(this.getTitle());
		x.setTabShow(this.getTabShow());

		return x;
	}
}

