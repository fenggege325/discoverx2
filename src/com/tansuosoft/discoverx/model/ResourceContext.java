/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.io.File;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.DefaultResourceConfig;
import com.tansuosoft.discoverx.common.OrganizationsContext;
import com.tansuosoft.discoverx.common.ResourceDescriptor;
import com.tansuosoft.discoverx.common.ResourceDescriptorConfig;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.Path;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.Deserializer;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 用于从已缓存的资源或XML、数据库等数据源中获取资源的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ResourceContext {
	/**
	 * 不同单位(租户)的差异化资源保存的基础路径名。
	 */
	public static final String ORG_VAR_BASE = "var" + File.separator;

	private ResourceDescriptorConfig m_resourceDescriptorConfig = null; // 资源信息描述配置
	private CacheManager m_cacheManager = null; // 缓存管理器
	private Cache m_cache = null; // 缓存。
	/**
	 * 资源缓存的缓存名称常数。
	 */
	protected static final String RESOURCE_CACHE_NAME = "resourceCache";

	/**
	 * 私有缺省构造器。
	 */
	private ResourceContext() {
		int capacity = CommonConfig.getInstance().getResourceCacheCapacity();
		if (capacity == 0) capacity = 10000;
		this.m_resourceDescriptorConfig = ResourceDescriptorConfig.getInstance();
		this.m_cacheManager = CacheManager.getInstance();
		this.m_cache = new Cache(RESOURCE_CACHE_NAME, // java.lang.String name,
				capacity, // int maxElementsInMemory,
				MemoryStoreEvictionPolicy.LRU, // MemoryStoreEvictionPolicy memoryStoreEvictionPolicy,
				false, // boolean overflowToDisk,
				null, // java.lang.String diskStorePath,
				true, // boolean eternal,
				120, // long timeToLiveSeconds,
				120, // long timeToIdleSeconds,
				false, // boolean diskPersistent,
				36000, // long diskExpiryThreadIntervalSeconds,
				null, // RegisteredEventListeners registeredEventListeners,
				null, // BootstrapCacheLoader bootstrapCacheLoader,
				0, // int maxElementsOnDisk,
				30 // int diskSpoolBufferSizeMB
		);
		this.m_cacheManager.addCache(this.m_cache);
	}

	private static ResourceContext m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return ResourceContext
	 */
	public static ResourceContext getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new ResourceContext();
			}
		}
		return m_instance;
	}

	protected static final String EMPTY = "";

	/**
	 * 获取多单位使用时特定单位(除默认管理单位外)的xml资源保存路径前缀。
	 * 
	 * @return 如果返回空串则表示默认管理单位或没有多各单位使用。
	 */
	protected static String getOrgPrefix() {
		if (!OrganizationsContext.getInstance().isInTenantContext()) return EMPTY;
		int contextOrgSc = OrganizationsContext.getInstance().getContextUserOrgSc();
		return (contextOrgSc + File.separator);
	}

	/**
	 * 根据资源UNID和对应资源描述获取相应资源实例。
	 * 
	 * @param unid
	 * @param descriptor ResourceDescriptor
	 * @return Resource
	 */
	public Resource getResource(String unid, ResourceDescriptor descriptor) {
		if (unid == null || unid.length() == 0 || descriptor == null) return null;
		Resource resource = null;
		String orgPrefix = (descriptor.getXmlStore()  ? getOrgPrefix() : "");
		boolean foundInOrgVarDir = false;
		Element el = null;
		// 先从单位特定缓存中取
		if (orgPrefix.length() > 0) {
			el = this.m_cache.get(orgPrefix + unid);
			if (el != null) {
				Object elval = el.getValue();
				if (elval != null && elval instanceof Resource) return (Resource) elval;
			}
		}
		// 取不到则从通用缓存中取
		if (el == null && orgPrefix.length() == 0) {
			el = this.m_cache.get(unid);
			if (el != null) {
				Object elval = el.getValue();
				if (elval != null && elval instanceof Resource) return (Resource) elval;
			}
		}
		// 如果都取不到则从反序列化工厂构造
		Deserializer deserializer = Instance.newInstance(descriptor.getDeserializer(), Deserializer.class);
		if (deserializer == null) deserializer = new XmlDeserializer();
		String src = null;
		if (descriptor.getXmlStore()) {
			String base = CommonConfig.getInstance().getResourcePath();
			String dir = descriptor.getDirectory();
			// 先找单位特有的，找不到则使用通用的
			if (orgPrefix.length() > 0) {
				src = base + dir + File.separator + ORG_VAR_BASE + orgPrefix + unid + ".xml";
				if (!new File(src).exists()) src = null;
				else foundInOrgVarDir = true;
			}
			if (src == null) src = base + dir + File.separator + unid + ".xml";
		}
		Class<?> clazz = null;
		try {
			clazz = Class.forName(descriptor.getEntity());
		} catch (ClassNotFoundException e) {
			FileLogger.debug("无法获取“" + descriptor.getName() + "”对应的类信息：" + descriptor.getEntity() + "");
		}
		if (clazz == null) return null;
		Object obj = deserializer.deserialize(src == null ? unid : src, clazz);
		if (obj == null || !(obj instanceof Resource)) {
			if (!Role.ROLE_ANONYMOUS.equals(unid) && CommonConfig.getInstance().getDebugable()) FileLogger.debug("%1$s资源:“%2$s://%3$s”不存在！", descriptor.getName(), descriptor.getDirectory(), unid);
			return null;
		}
		resource = (Resource) obj;
		if (foundInOrgVarDir) resource.setCommon(false);
		// 如果此类资源需要缓存则将新反序列化的资源添加到缓存
		if (resource != null && descriptor.getCache()) {
			if (foundInOrgVarDir) this.addObjectToCache(orgPrefix + unid, resource);
			else this.addObjectToCache(unid, resource);
		}

		return resource;
	}

	/**
	 * 根据资源UNID和资源实体对应的类信息获取对应资源实例。
	 * 
	 * @param unid
	 * @param clazz
	 * @return Resource
	 */
	@SuppressWarnings("unchecked")
	public <T> T getResource(String unid, Class<?> clazz) {
		if (unid == null || unid.length() == 0 || clazz == null) return null;
		ResourceDescriptor descriptor = this.m_resourceDescriptorConfig.getResourceDescriptor(clazz);
		Resource r = this.getResource(unid, descriptor);
		if (r == null) return null;
		if (clazz.isAssignableFrom(r.getClass())) return (T) r;
		return null;
	}

	/**
	 * 根据资源UNID和资源目录名称获取对应资源实例。
	 * 
	 * @param unid
	 * @param directory
	 * @return Resource
	 */
	public Resource getResource(String unid, String directory) {
		if (unid == null || unid.length() == 0 || directory == null || directory.length() == 0) return null;
		ResourceDescriptor descriptor = this.m_resourceDescriptorConfig.getResourceDescriptor(directory);
		Resource r = this.getResource(unid, descriptor);
		return r;
	}

	/**
	 * 从缓存中获取指定UNID对应的已缓存资源。
	 * 
	 * <p>
	 * <strong>资源必须事先已经存在于缓存中，否则返回null。</strong>
	 * </p>
	 * 
	 * @param unid String，资源UNID，必须。
	 * @return Resource
	 */
	public Resource getResource(String unid) {
		if (unid == null || unid.length() == 0) return null;
		String orgPrefix = getOrgPrefix();
		Element el = null;
		// 先从单位特定缓存中取
		if (orgPrefix.length() > 0) {
			el = this.m_cache.get(orgPrefix + unid);
			if (el != null) {
				Object elval = el.getValue();
				if (elval != null && elval instanceof Resource) return (Resource) elval;
			}
		}
		// 取不到则从通用缓存中取
		if (el == null) {
			el = this.m_cache.get(unid);
			if (el != null) {
				Object elval = el.getValue();
				if (elval != null && elval instanceof Resource) return (Resource) elval;
			}
		}
		return null;
	}

	/**
	 * 根据资源实体类对应的类信息获取（只有一个实例的）资源对象。
	 * 
	 * @param clazz
	 * @return Resource
	 */
	@SuppressWarnings("unchecked")
	public <T> T getSingleResource(Class<?> clazz) {
		if (clazz == null) return null;
		ResourceDescriptor descriptor = this.m_resourceDescriptorConfig.getResourceDescriptor(clazz);
		if (descriptor == null) return null;
		String directory = descriptor.getDirectory();
		Resource r = this.getSingleResource(directory);
		if (r == null) return null;
		if (clazz.isAssignableFrom(r.getClass())) return (T) r;
		return null;
	}

	/**
	 * 根据资源目录名获取此类资源的唯一资源对象（只有一个实例的资源）。
	 * 
	 * @param directory
	 * @return Resource
	 */
	public Resource getSingleResource(String directory) {
		if (directory == null || directory.length() == 0) return null;

		CommonConfig commonConfig = CommonConfig.getInstance();
		String resourceLocation = commonConfig.getResourcePath();
		Path path = new Path(resourceLocation);
		path = path.combine(directory);

		File file = path.getFileInstance();
		if (file == null || !file.exists() || !file.isDirectory()) return null;
		File[] files = file.listFiles();
		if (files == null || files.length == 0) return null;
		for (File x : files) {
			if (x.isDirectory() || x.getName().length() != 36) continue;
			return this.getResource(x.getName().substring(0, 32), directory);
		}

		return null;
	}

	/**
	 * 根据资源目录获取此类资源的默认资源。
	 * 
	 * @param directory String
	 * @return 资源实例或null（如果没有配置默认资源或找不到配置内容对应的资源）。
	 */
	public Resource getDefaultResource(String directory) {
		if (directory == null || directory.length() == 0) return null;
		ResourceDescriptor rd = ResourceDescriptorConfig.getInstance().getResourceDescriptor(directory);
		if (rd == null) throw new RuntimeException("无法获取“" + directory + "”" + "类型的资源的默认资源。");
		String cls = rd.getEntity();
		try {
			Class<?> clazz = Class.forName(cls);
			return this.getDefaultResource(clazz);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 根据资源类信息获取此类资源的默认资源。
	 * 
	 * @param clazz Class&lt;?&gt;
	 * @return 资源实例或null（如果没有配置默认资源或找不到配置内容对应的资源）。
	 */
	@SuppressWarnings("unchecked")
	public <T> T getDefaultResource(Class<?> clazz) {
		DefaultResourceConfig drc = DefaultResourceConfig.getInstance();
		String unid = drc.getDefaultResourceUNID(clazz);
		Resource r = this.getResource(unid, clazz);
		if (r == null) return null;
		if (clazz.isAssignableFrom(r.getClass())) return (T) r;
		return null;
	}

	/**
	 * 增加key指定的结果类实例到对象缓存。
	 * 
	 * @param key String 唯一键值
	 * @param value 要缓存的对象。
	 * @param liveSeconds 缓存的对象需要缓存多少时间。
	 * @return Object 如果增加成功，则返回value，否则返回null。
	 */
	public Object addObjectToCache(String key, Object value, int liveSeconds) {
		if (key == null || key.length() == 0 || value == null) return null;
		try {
			Element e = new Element(key, value);
			if (liveSeconds > 0) e.setTimeToLive(liveSeconds);
			this.m_cache.put(e);
			return value;
		} catch (Exception ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
			return null;
		}
	}

	/**
	 * 增加key指定的结果类实例到对象缓存。
	 * 
	 * @param key String 唯一键值
	 * @param value void
	 * @return Object 如果增加成功，则返回value，否则返回null。
	 */
	public Object addObjectToCache(String key, Object value) {
		return addObjectToCache(key, value, 0);
	}

	/**
	 * 重新加载key指定的value到缓存（先删除，再添加）。
	 * 
	 * @param key
	 * @param value
	 * @return Object
	 */
	public Object reloadObject(String key, Object value) {
		if (this.removeObjectFromCache(key) != null) { return this.addObjectToCache(key, value); }
		return null;
	}

	/**
	 * 从缓存库中获取指定键值的资源。
	 * 
	 * @param key
	 * @return Object 如果找不到则返回null。
	 */
	public Object getObjectFromCache(String key) {
		if (key == null || key.length() == 0) return null;
		Element el = this.m_cache.get(key);
		if (el == null) return null;
		return el.getObjectValue();
	}

	/**
	 * 从对象缓存中删除一个key指定的缓存。
	 * 
	 * @param key String 唯一键值
	 * @return Object 如果删除成功，则返回删除的结果对象。
	 */
	public Object removeObjectFromCache(String key) {
		Element el = null;
		Object ret = null;
		try {
			String orgPrefix = getOrgPrefix();
			if (orgPrefix.length() > 0) {
				String orgVarKey = orgPrefix + key;
				el = this.m_cache.get(orgVarKey);
				if (el != null) {
					ret = el.getValue();
					if (!this.m_cache.remove(orgVarKey) && CommonConfig.getInstance().getDebugable()) FileLogger.debug("无法删除“" + ret + "”对应缓存信息！");
					return ret;
				}
			}
			el = this.m_cache.get(key);
			if (el != null) {
				ret = el.getValue();
				if (!this.m_cache.remove(key) && CommonConfig.getInstance().getDebugable()) FileLogger.debug("无法删除“" + ret + "”对应缓存信息！");
			}
		} catch (Exception ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
			return null;
		}
		return ret;
	}

	/**
	 * 清除所有缓存。
	 */
	public void clearCache() {
		this.m_cache.removeAll();
	}

	/**
	 * 关闭缓存。
	 */
	public void shutdown() {
		clearCache();
		this.m_cacheManager.shutdown();
		this.m_cacheManager = null;
	}

	/**
	 * 获取缓存的资源数量。
	 * 
	 * @return long
	 */
	public long getCachedCount() {
		return m_cache.getStatistics().getObjectCount();
	}
}

