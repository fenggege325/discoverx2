/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.DateTime;

/**
 * 表示一条系统日志信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Log {
	/**
	 * 缺省构造器。
	 */
	public Log() {
		this.m_UNID = com.tansuosoft.discoverx.util.UNIDProvider.getUNID();
		this.m_created = DateTime.getNowDTString();
	}

	/**
	 * 接收所属资源的UNID的构造器。
	 * 
	 * @param punid缺省构造器。
	 */
	public Log(String punid) {
		this();
		this.m_owner = punid;
	}

	private String m_UNID = null; // unid
	private String m_subject = null; // 动作发起者（执行人）。
	private String m_created = null; // 日志记录的日期时间。
	private String m_verb = null; // 操作或者事件（即执行什么动作）。
	private String m_object = null; // 动作目标（即动作针对的对象是什么）。
	private String m_result = "成功"; // 结果，默认为“成功”。
	private String m_message = null; // 日志消息内容。
	private String m_owner = null; // 所属资源的UNID。
	private LogType m_logType = LogType.Normal; // 日志类型，默认为LogType.Normal（普通日志）。
	private LogLevel m_logLevel = LogLevel.Information; // 日志级别，默认为LogLevel.Information（信息日志）。
	private String m_start = null; // 开始日期时间，一般针对流程日志。
	private String m_expect = null; // 期望完成日期时间，一般针对流程日志。
	private String m_state = null; // 添加时的流程状态（格式：流程UNID\环节UNID），可选。

	/**
	 * 返回日志UNID。
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置日志UNID。
	 * 
	 * @param UNID String
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回动作发起者（执行人）。
	 * 
	 * @return String 动作发起者（执行人）。
	 */
	public String getSubject() {
		return this.m_subject;
	}

	/**
	 * 设置动作发起者（执行人）。
	 * 
	 * @param subject String 动作发起者（执行人）。
	 */
	public void setSubject(String subject) {
		this.m_subject = subject;
	}

	/**
	 * 返回日志记录的日期时间，默认为当前日期时间。
	 * 
	 * @return String 日志记录的日期时间。
	 */
	public String getCreated() {
		return this.m_created;
	}

	/**
	 * 设置日志记录的日期时间。
	 * 
	 * @param dt String 日志记录的日期时间。
	 */
	public void setCreated(String dt) {
		this.m_created = dt;
	}

	/**
	 * 返回操作或者事件（即执行什么动作）。
	 * 
	 * @return String 操作或者事件（即执行什么动作）。
	 */
	public String getVerb() {
		return this.m_verb;
	}

	/**
	 * 设置操作或者事件（即执行什么动作）。
	 * 
	 * @param verb String 操作或者事件（即执行什么动作）。
	 */
	public void setVerb(String verb) {
		this.m_verb = verb;
	}

	/**
	 * 返回动作目标（即动作操作的对象是什么）。
	 * 
	 * @return String 动作目标（即动作针对的对象是什么）。
	 */
	public String getObject() {
		return this.m_object;
	}

	/**
	 * 设置动作目标（即动作操作的对象是什么）。
	 * 
	 * @param object String 动作目标（即动作针对的对象是什么）。
	 */
	public void setObject(String object) {
		this.m_object = object;
	}

	/**
	 * 返回结果，默认为“成功”。
	 * 
	 * @return String 结果，默认为“成功”。
	 */
	public String getResult() {
		return this.m_result;
	}

	/**
	 * 设置结果，默认为“成功”。
	 * 
	 * @param result String 结果，默认为“成功”。
	 */
	public void setResult(String result) {
		this.m_result = result;
	}

	/**
	 * 返回日志消息内容，可选。
	 * 
	 * @return String 额外消息内容。
	 */
	public String getMessage() {
		return this.m_message;
	}

	/**
	 * 设置日志消息内容，可选。
	 * 
	 * @param message String 额外消息内容。
	 */
	public void setMessage(String message) {
		this.m_message = message;
	}

	/**
	 * 返回所属资源的UNID，必须。
	 * 
	 * @return String
	 */
	public String getOwner() {
		return this.m_owner;
	}

	/**
	 * 设置所属资源的UNID，必须。
	 * 
	 * @param owner String
	 */
	public void setOwner(String owner) {
		this.m_owner = owner;
	}

	/**
	 * 返回日志类型，默认为LogType.Normal（普通日志）。
	 * 
	 * @return LogType 日志类型，默认为LogType.Normal（普通日志）。
	 */
	public LogType getLogType() {
		return this.m_logType;
	}

	/**
	 * 设置日志类型，默认为LogType.Normal（普通日志）。
	 * 
	 * @param logType LogType 日志类型，默认为LogType.Normal（普通日志）。
	 */
	public void setLogType(LogType logType) {
		this.m_logType = logType;
	}

	/**
	 * 返回日志级别，默认为LogLevel.Information（信息日志）。
	 * 
	 * @return LogLevel 日志级别，默认为LogLevel.Information（信息日志）。
	 */
	public LogLevel getLogLevel() {
		return this.m_logLevel;
	}

	/**
	 * 设置日志级别，默认为LogLevel.Information（信息日志）。
	 * 
	 * @param logLevel LogLevel 日志级别，默认为LogLevel.Information（信息日志）。
	 */
	public void setLogLevel(LogLevel logLevel) {
		this.m_logLevel = logLevel;
	}

	/**
	 * 返回开始日期时间，一般针对流程日志。
	 * 
	 * @return String
	 */
	public String getStart() {
		return this.m_start;
	}

	/**
	 * 设置开始日期时间，一般针对流程日志。
	 * 
	 * @param start String
	 */
	public void setStart(String start) {
		this.m_start = start;
	}

	/**
	 * 返回期望完成日期时间，一般针对流程日志。
	 * 
	 * @return String
	 */
	public String getExpect() {
		return this.m_expect;
	}

	/**
	 * 设置期望完成日期时间，一般针对流程日志。
	 * 
	 * @param expect String
	 */
	public void setExpect(String expect) {
		this.m_expect = expect;
	}

	/**
	 * 返回添加时的流程状态（格式：流程\环节），可选。
	 * 
	 * @return String
	 */
	public String getState() {
		return this.m_state;
	}

	/**
	 * 设置添加时的流程状态（格式：流程\环节），可选。
	 * 
	 * @param state String
	 */
	public void setState(String state) {
		this.m_state = state;
	}
}

