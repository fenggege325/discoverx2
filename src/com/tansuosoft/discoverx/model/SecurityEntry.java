/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 用于控制用户使用、访问、处理资源时进行授权的安全条目类。
 * 
 * <p>
 * 安全条目包含于{@link Security}中。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class SecurityEntry {
	private int m_securityCode = -1; // 安全编码，必须。
	private int m_securityLevel = 0; // 普通权限级别，默认为0，可选。
	private int m_workflowLevel = 0; // 流程权限级别，默认为0，可选。
	private int m_codeSource = -1; // 来源安全编码，可选。
	private String m_PUNID = null; // 所控制资源UNID，必须。

	/**
	 * 缺省构造器。
	 */
	public SecurityEntry() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param punid
	 * @param securityCode
	 * @param securityLevel
	 * @param workflowLevel
	 */
	public SecurityEntry(String punid, int securityCode, int securityLevel, int workflowLevel) {
		this.m_PUNID = punid;
		this.m_securityCode = securityCode;
		this.m_securityLevel = securityLevel;
		this.m_workflowLevel = workflowLevel;
	}

	/**
	 * 返回安全编码，必须。
	 * 
	 * @return int 安全编码，必须。
	 */
	public int getSecurityCode() {
		return this.m_securityCode;
	}

	/**
	 * 返回普通权限级别，默认为0，可选。
	 * 
	 * <p>
	 * 0表示没有级别。
	 * </p>
	 * 
	 * @return int
	 */
	public int getSecurityLevel() {
		return this.m_securityLevel;
	}

	/**
	 * 设置普通权限级别，默认为0，可选。
	 * 
	 * @param securityLevel int
	 */
	public void setSecurityLevel(int securityLevel) {
		this.m_securityLevel = securityLevel;
	}

	/**
	 * 返回流程权限级别，默认为0，可选。
	 * 
	 * <p>
	 * 0表示没有级别。
	 * </p>
	 * 
	 * @return int
	 */
	public int getWorkflowLevel() {
		return this.m_workflowLevel;
	}

	/**
	 * 设置流程权限级别，默认为0，可选。
	 * 
	 * @param workflowLevel int
	 */
	public void setWorkflowLevel(int workflowLevel) {
		this.m_workflowLevel = workflowLevel;
	}

	/**
	 * 设置安全编码，必须。
	 * 
	 * @param securityCode int 安全编码，必须。
	 */
	public void setSecurityCode(int securityCode) {
		this.m_securityCode = securityCode;
	}

	/**
	 * 返回来源安全编码，可选。
	 * 
	 * @return int 来源安全编码，可选。
	 */
	public int getCodeSource() {
		return this.m_codeSource;
	}

	/**
	 * 设置来源安全编码，可选。
	 * 
	 * @param codeSource int 来源安全编码，可选。
	 */
	public void setCodeSource(int codeSource) {
		this.m_codeSource = codeSource;
	}

	/**
	 * 返回所控制资源UNID，必须。
	 * 
	 * @return String 所控制资源UNID，必须。
	 */
	public String getPUNID() {
		return this.m_PUNID;
	}

	/**
	 * 设置所控制资源UNID，必须。
	 * 
	 * @param PUNID String 所控制资源UNID，必须。
	 */
	public void setPUNID(String PUNID) {
		this.m_PUNID = PUNID;
	}

	/**
	 * 重载hashCode
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + m_securityCode;
		result = prime * result + m_securityLevel;
		result = prime * result + m_workflowLevel;
		return result;
	}

	/**
	 * 重载equals：安全编码、操作级别、流程级别相等则相等。
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!super.equals(obj)) return false;
		if (getClass() != obj.getClass()) return false;
		SecurityEntry other = (SecurityEntry) obj;
		if (m_securityCode != other.m_securityCode) return false;
		if (m_securityLevel != other.m_securityLevel) return false;
		if (m_workflowLevel != other.m_workflowLevel) return false;
		return true;
	}

	/**
	 * 重载toString：返回格式为“安全编码(操作级别,流程级别)”。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("%1$d(%2$d,%3$d)", this.m_securityCode, this.m_securityLevel, this.m_workflowLevel);
	}

	// private int m_securityRange = 1; // 此安全条目的安全控制范围。
	/**
	 * 获取或设置安全控制范围，默认为SecurityRange.Config对应的数字值（1）。
	 * 
	 * <p>
	 * 多个SecurityRange数字值可以以int的“|”（或）操作进行组合，一般继承自资源的描述对象中。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.common.ResourceDescriptor#getSecurityRange
	 * @return int
	 */
	// public int getSecurityRange() {
	// return this.m_securityRange;
	// }
	/**
	 * @see SecurityEntry#getSecurityRange()
	 * @param securityRange void
	 */
	// public void setSecurityRange(int securityRange) {
	// this.m_securityRange = securityRange;
	// }
}

