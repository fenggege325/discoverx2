/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

/**
 * 用于控制用户使用、访问、处理资源时进行授权的安全信息类。
 * 
 * <p>
 * 安全信息控制在使用、访问、处理资源时进行授权，如判断用户是否具有增、删、改、查、流程处理等相关权限。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Security implements Cloneable {
	/**
	 * 对所有人公开的文档的安全编码结果标记。
	 */
	public static final String PUBLISH_TO_ALL_FLAG = "*";

	/**
	 * 对所有登录注册用户公开的文档的安全编码结果标记。
	 */
	public static final String PUBLISH_TO_USER_FLAG = "**";

	/**
	 * 缺省构造器
	 */
	public Security() {
		this.m_securityEntries = new ArrayList<SecurityEntry>();
		this.m_securityLevels = new HashMap<Integer, Integer>();
		this.m_workflowLevels = new HashMap<Integer, Integer>();
	}

	/**
	 * 接收所属资源的UNID的构造器。
	 * 
	 * @param punid
	 */
	public Security(String punid) {
		this();
		this.m_PUNID = punid;
	}

	private String m_PUNID = null; // 接收安全控制所属（描述）的资源的UNID，必须。
	private boolean m_published = false; // 是否公开。
	private PublishOption m_publishOption = PublishOption.Custom; // 公开选项。
	private java.util.List<SecurityEntry> m_securityEntries = null; // 包含的安全条目列表集合。
	private java.util.Map<Integer, Integer> m_securityLevels = null; // 包含的安全编码与操作级别一一对应的字典集合。
	private java.util.Map<Integer, Integer> m_workflowLevels = null; // 包含的安全编码与流程级别一一对应的字典集合。

	/**
	 * 返回包含的安全条目列表集合。
	 * 
	 * @return java.util.List&lt;SecurityEntry&gt; SecurityEntries
	 */
	public java.util.List<SecurityEntry> getSecurityEntries() {
		return this.m_securityEntries;
	}

	/**
	 * 设置包含的安全条目列表集合。
	 * 
	 * @param securityEntries java.util.List&lt;SecurityEntry&gt; SecurityEntries
	 */
	public void setSecurityEntries(java.util.List<SecurityEntry> securityEntries) {
		this.m_securityEntries = securityEntries;
		this.sync2QueryDictionary();
	}

	/**
	 * 设置指定安全编码对应的操作级别，如果已包含指定编码，则合并级别。
	 * 
	 * @param securityCode
	 * @param level int 必须是{@link com.tansuosoft.discoverx.model.SecurityLevel}枚举对应的数字或其组合，否则可能出错。
	 * @return int 返回设置之前的级别或者-1（表示是新设置的级别）。
	 */
	public int setSecurityLevel(int securityCode, int level) {
		if (level <= 0) return level;
		boolean exists = false;
		int oldLevel = 0;
		int newLevel = 0;
		if (this.m_securityLevels != null) {
			Integer x = this.m_securityLevels.get(securityCode);
			if (x != null) {
				oldLevel = x;
				exists = true;
			}
		}
		if (exists) {
			newLevel = (oldLevel | level);
			for (SecurityEntry e : this.m_securityEntries) {
				if (e.getSecurityCode() == securityCode) {
					e.setSecurityLevel(newLevel);
					break;
				}
			}
			this.m_securityLevels.put(securityCode, newLevel);
			return oldLevel;
		} else {
			newLevel = level;
			this.m_securityEntries.add(new SecurityEntry(this.m_PUNID, securityCode, newLevel, 0));
			this.m_securityLevels.put(securityCode, newLevel);
			this.m_workflowLevels.put(securityCode, 0);
			return -1;
		}
	}

	/**
	 * 设置指定安全编码对应的流程级别，如果已包含指定编码，则合并级别。
	 * 
	 * @param securityCode
	 * @param level int 必须是{@link com.tansuosoft.discoverx.workflow.WFDataLevel}枚举对应的数字或其组合，否则可能出错。
	 * 
	 * @return int 返回设置之前的级别或者-1（表示是新设置的级别）。
	 */
	public int setWorkflowLevel(int securityCode, int level) {
		if (level < 0) return level;
		boolean exists = false;
		int oldLevel = 0;
		int newLevel = 0;
		if (this.m_workflowLevels != null) {
			Integer x = this.m_workflowLevels.get(securityCode);
			if (x != null) {
				oldLevel = x;
				exists = true;
			}
		}
		if (exists) {
			newLevel = level; // (oldLevel | level);
			for (SecurityEntry e : this.m_securityEntries) {
				if (e.getSecurityCode() == securityCode) {
					e.setWorkflowLevel(newLevel);
					break;
				}
			}
			this.m_workflowLevels.put(securityCode, newLevel);
			return oldLevel;
		} else {
			newLevel = level;
			this.m_securityEntries.add(new SecurityEntry(this.m_PUNID, securityCode, SecurityLevel.View.getIntValue(), newLevel));
			this.m_workflowLevels.put(securityCode, newLevel);
			this.m_securityLevels.put(securityCode, SecurityLevel.View.getIntValue());
			return -1;
		}
	}

	/**
	 * 删除指定安全编码对应的操作级别。
	 * 
	 * @param securityCode
	 * @param level int 必须是{@link com.tansuosoft.discoverx.model.SecurityLevel}枚举对应的数字或0（0表示没有任何级别），否则可能出错。
	 * 
	 * @return int 返回更改之前的级别或者-1（表示原来不存在这个级别）。
	 */
	public int removeSecurityLevel(int securityCode, int level) {
		if (level < 0) return level;
		boolean exists = false;
		int oldLevel = 0;
		int newLevel = 0;
		if (this.m_securityLevels != null) {
			Integer x = this.m_securityLevels.get(securityCode);
			if (x != null) {
				oldLevel = x;
				exists = true;
			}
		}
		if (exists) {
			newLevel = (level == 0 ? 0 : (oldLevel ^ level & oldLevel));
			for (SecurityEntry e : this.m_securityEntries) {
				if (e.getSecurityCode() == securityCode) {
					e.setSecurityLevel(newLevel);
					break;
				}
			}
			this.m_securityLevels.put(securityCode, newLevel);
			return oldLevel;
		} else {
			// newLevel = 0;
			// this.m_securityEntries.add(new SecurityEntry(this.m_PUNID, securityCode, newLevel, 0));
			// this.m_securityLevels.put(securityCode, newLevel);
			return -1;
		}
	}

	/**
	 * 删除指定安全编码对应的流程级别。
	 * 
	 * @param securityCode
	 * @param level int 必须是{@link com.tansuosoft.discoverx.workflow.WFDataLevel}枚举对应的数字或0（0表示没有任何级别），否则可能出错。
	 * 
	 * @return int 返回更改之前的级别或者-1（表示原来不存在这个级别）。
	 */
	public int removeWorkflowLevel(int securityCode, int level) {
		if (level < 0) return level;
		boolean exists = false;
		int oldLevel = 0;
		int newLevel = 0;
		if (this.m_workflowLevels != null) {
			Integer x = this.m_workflowLevels.get(securityCode);
			if (x != null) {
				oldLevel = x;
				exists = true;
			}
		}
		if (exists) {
			newLevel = (level == 0 ? 0 : (oldLevel ^ level & oldLevel));
			for (SecurityEntry e : this.m_securityEntries) {
				if (e.getSecurityCode() == securityCode) {
					e.setWorkflowLevel(newLevel);
					break;
				}
			}
			this.m_workflowLevels.put(securityCode, newLevel);
			return oldLevel;
		} else {
			newLevel = 0;
			this.m_securityEntries.add(new SecurityEntry(this.m_PUNID, securityCode, SecurityLevel.View.getIntValue(), newLevel));
			this.m_workflowLevels.put(securityCode, newLevel);
			return -1;
		}
	}

	/**
	 * 检查指定安全编码是否包含指定操作级别。
	 * 
	 * @param securityCode
	 * @param level
	 * @return boolean
	 */
	public boolean checkSecurityLevel(int securityCode, SecurityLevel level) {
		if (this.m_securityLevels.isEmpty()) this.sync2QueryDictionary();
		Integer x = (this.m_securityLevels == null ? null : this.m_securityLevels.get(securityCode));

		// 如果是检查查看权限
		if (level == SecurityLevel.View) {
			// 作者可以查看
			if (checkSecurityLevel(securityCode, SecurityLevel.Author)) return true;
			// 流程参与者固定可以查看
			Integer y = (this.m_workflowLevels == null ? null : this.m_workflowLevels.get(securityCode));
			if (y != null && y > 0) return true;
		}

		if (x == null) x = 0;
		return ((x & level.getIntValue()) == level.getIntValue());
	}

	/**
	 * 检查指定安全编码是否包含指定流程级别。
	 * 
	 * @param securityCode
	 * @param level int 必须是{@link com.tansuosoft.discoverx.workflow.WFDataLevel}枚举对应的数字，否则可能出错。
	 * @return boolean
	 */
	public boolean checkWorkflowLevel(int securityCode, int level) {
		if (this.m_securityLevels.isEmpty()) this.sync2QueryDictionary();
		Integer x = (this.m_workflowLevels == null ? null : this.m_workflowLevels.get(securityCode));
		if (x == null) return false;

		return ((x & level) == level);
	}

	/**
	 * 获取指定安全编码对应的{@link SecurityEntry}对象。
	 * 
	 * @param securityCode
	 * @return 如果找不到则返回null。
	 */
	public SecurityEntry getSecurityEntry(int securityCode) {
		if (securityCode < 0 || this.m_securityEntries == null || this.m_securityEntries.isEmpty()) return null;
		for (SecurityEntry x : this.m_securityEntries) {
			if (x != null && x.getSecurityCode() == securityCode) { return x; }
		}
		return null;
	}

	/**
	 * 重载toString：获取所有可查看安全编码字符串。
	 * 
	 * @see java.lang.Object#toString()
	 * @return String 返回安全编码字符串，格式为“0,半角逗号分隔的从小到大排序的编码值,0”，如果没有有效编码，则返回空字符串；<br/>
	 *         如果Published为true，则返回{@link com.tansuosoft.discoverx.model.Security.PUBLISHED_CODE_STRING}定义的编码“*”。
	 */
	public String toString() {
		getPublished();
		if (this.m_published) {
			if (this.m_publishOption == PublishOption.All) {
				return PUBLISH_TO_ALL_FLAG;
			} else {
				return PUBLISH_TO_USER_FLAG;
			}
		}
		if (this.m_securityEntries == null) return "";
		java.lang.StringBuilder sb = new java.lang.StringBuilder();
		for (SecurityEntry x : this.m_securityEntries) {
			if (x == null || x.getSecurityCode() <= 0 || (x.getSecurityLevel() <= 0 && x.getWorkflowLevel() <= 0)) continue;
			if (!this.checkSecurityLevel(x.getSecurityCode(), SecurityLevel.View)) continue;
			sb.append(sb.length() == 0 ? "" : ",").append(x.getSecurityCode());
		}
		if (sb.length() > 0) {
			sb.insert(0, "0,");
			sb.append(",0");
		}
		return sb.toString();
	}

	/**
	 * 同步安全信息以方便查询。
	 */
	protected void sync2QueryDictionary() {
		if (this.m_securityEntries == null || this.m_securityEntries.isEmpty()) return;
		this.m_securityLevels = new Hashtable<Integer, Integer>(this.m_securityEntries.size());
		this.m_workflowLevels = new Hashtable<Integer, Integer>(this.m_securityEntries.size());
		int securityCode = 0;
		int securityLevel = 0;
		int workflowLevel = 0;
		for (SecurityEntry x : this.m_securityEntries) {
			if (x == null) continue;
			securityCode = x.getSecurityCode();
			securityLevel = x.getSecurityLevel();
			workflowLevel = x.getWorkflowLevel();
			this.m_securityLevels.put(securityCode, securityLevel);
			this.m_workflowLevels.put(securityCode, workflowLevel);
		}
	}

	/**
	 * 返回接收所属资源的UNID，必须。
	 * 
	 * @return String
	 */
	public String getPUNID() {
		return this.m_PUNID;
	}

	/**
	 * 设置所属资源的UNID，必须。
	 * 
	 * @param PUNID String
	 */
	public void setPUNID(String PUNID) {
		this.m_PUNID = PUNID;
	}

	/**
	 * 返回资源是否公开。
	 * 
	 * <p>
	 * 如果为true，即表示资源对用户查阅权限公开。<br/>
	 * 具体对哪些用户公开查阅权限可以通过{@link #getPublishOption()}的返回值获取。<br/>
	 * 只有针对文档资源时此属性才有意义。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getPublished() {
		if (m_securityEntries == null) {
			m_published = false;
			return m_published;
		}
		for (SecurityEntry x : this.m_securityEntries) {
			if (x == null) continue;
			if (x.getSecurityCode() == Role.ROLE_DEFAULT_SC && (x.getSecurityLevel() & SecurityLevel.View.getIntValue()) == SecurityLevel.View.getIntValue()) {
				this.m_published = true;
				if (this.m_publishOption == PublishOption.Custom) this.m_publishOption = PublishOption.User;
			} else if (x.getSecurityCode() == Role.ROLE_ANONYMOUS_SC && (x.getSecurityLevel() & SecurityLevel.View.getIntValue()) == SecurityLevel.View.getIntValue()) {
				this.m_published = true;
				if (this.m_publishOption != PublishOption.All) this.m_publishOption = PublishOption.All;
				break;
			}
		}

		return this.m_published;
	}

	/**
	 * 返回公开选项。
	 * 
	 * <p>
	 * 默认为{@link PublishOption#Custom}，需{@link #getPublished()}返回true才有意义。<br/>
	 * 只有针对文档资源时此属性才有意义。
	 * </p>
	 * 
	 * @return PublishOption
	 */
	public PublishOption getPublishOption() {
		this.getPublished();
		return this.m_publishOption;
	}

	/**
	 * 检测并返回是否具有有效安全控制条目。
	 * 
	 * @return
	 */
	public boolean hasValidEntries() {
		return (this.m_securityEntries != null && m_securityEntries.size() > 0);
	}

	/**
	 * 合并another指定的安全信息到本对象。
	 * 
	 * <p>
	 * 本方法将another中包含的所有{@link SecurityEntry}对象集合合并到当前对象。
	 * </p>
	 * 
	 * @param another
	 */
	public void merge(Security another) {
		if (another == null) return;
		List<SecurityEntry> list = another.getSecurityEntries();
		if (list == null || list.isEmpty()) return;
		for (SecurityEntry se : list) {
			if (se == null || se.getSecurityCode() <= 0) continue;
			this.setSecurityLevel(se.getSecurityCode(), se.getSecurityLevel());
			this.setWorkflowLevel(se.getSecurityCode(), se.getWorkflowLevel());
		}
	}

	/**
	 * 重载clone：返回一个克隆的新对象。
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Security security = new Security();
		security.setPUNID(this.getPUNID());
		List<SecurityEntry> list = this.getSecurityEntries();
		if (list == null || list.isEmpty()) return security;
		List<SecurityEntry> newList = new ArrayList<SecurityEntry>();
		SecurityEntry se = null;
		for (SecurityEntry x : list) {
			if (x == null) continue;
			se = new SecurityEntry();
			se.setCodeSource(x.getCodeSource());
			se.setPUNID(x.getPUNID());
			se.setSecurityCode(x.getSecurityCode());
			se.setSecurityLevel(x.getSecurityLevel());
			se.setWorkflowLevel(x.getWorkflowLevel());
			newList.add(se);
		}
		security.setSecurityEntries(newList);
		return security;
	}
}

