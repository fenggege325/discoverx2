/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 参与者类型枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum ParticipantType implements EnumBase {
	/**
	 * 具体人员类型(1)。
	 */
	Person(1),
	/**
	 * 组织机构类型(2)。
	 */
	Organization(2),
	/**
	 * 群组类型(4)。
	 */
	Group(4),
	/**
	 * 角色类型(8)。
	 */
	Role(8),
	/**
	 * 根节点(16)。
	 * <p>
	 * 只能存在一个根节点。
	 * </p>
	 */
	Root(16),
	/**
	 * 其它未知类型(0)。
	 */
	Other(0);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	ParticipantType(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return SerializationType
	 */
	public ParticipantType parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (ParticipantType s : ParticipantType.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return ParticipantType.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return SerializationType
	 */
	public static ParticipantType parse(int v) {
		for (ParticipantType s : ParticipantType.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

	/**
	 * 根据参与者类型获取其对应数据源简称。
	 * 
	 * @param pt
	 * @return
	 */
	public static String getAbbreviation(ParticipantType pt) {
		if (pt == null) return "";
		switch (pt.getIntValue()) {
		case 2:
			return "unit";
		case 4:
			return "group";
		case 8:
			return "role";
		case 1:
			return "user";
		}
		return "";
	}
}

