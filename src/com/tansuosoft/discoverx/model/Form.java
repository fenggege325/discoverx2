/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.event.EventHandlerInfo;

/**
 * 用于描述文档包含的字段信息的资源类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Form extends Resource implements Cloneable {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -1776674756927866188L;

	/**
	 * 默认字段标题显示宽度：120px。
	 */
	public static final int DEFAULT_ITEMCAPTION_WIDTH = 120;

	/**
	 * 缺省构造器
	 */
	public Form() {
	}

	private List<ItemGroup> m_itemGroups = null; // 表单包含的字段分组信息，可选。
	private List<Item> m_items = new ArrayList<Item>();// 表单包含的字段
	private SizeMeasurement m_measurement = SizeMeasurement.Percentage; // 表单配置使用的度量单位。
	private String m_style = null; // 表单样式
	private int m_itemCaptionWidth = DEFAULT_ITEMCAPTION_WIDTH; // 字段标题默认宽度。
	private List<EventHandlerInfo> m_eventHandlers = null; // 包含使用此表单的文档的事件处理程序信息，可选。

	/**
	 * 返回表单包含的字段分组信息，可选。
	 * 
	 * @see Form#setItemGroups(List)
	 * @return List&lt;ItemGroup&gt;
	 */
	public List<ItemGroup> getItemGroups() {
		return this.m_itemGroups;
	}

	/**
	 * 设置表单包含的字段分组信息，可选。
	 * 
	 * <p>
	 * 可以在表单中先定义好一系列分组，然后在字段配置中和其进行绑定。
	 * </p>
	 * 
	 * @see ItemGroup
	 * @param itemGroups List&lt;ItemGroup&gt;
	 */
	public void setItemGroups(List<ItemGroup> itemGroups) {
		this.m_itemGroups = itemGroups;
	}

	/**
	 * 返回表单包含的字段资源列表。
	 * 
	 * @see Form#setItems(List)
	 * @return java.util.List&lt;Item&gt; Items
	 */
	public java.util.List<Item> getItems() {
		return m_items;
	}

	/**
	 * 设置表单包含的字段资源列表。
	 * 
	 * <p>
	 * <strong>表单字段必须至少包含一个有效配置的字段资源！</strong>
	 * </p>
	 * <p>
	 * <strong>注意：表单只能包含一个“附加文件”或“富文本”类型的字段。</strong>
	 * </p>
	 * 
	 * @param items java.util.List&lt;Item&gt; items
	 */
	public void setItems(java.util.List<Item> items) {
		this.m_items = items;
	}

	/**
	 * 返回相关尺寸配置值的度量单位。
	 * 
	 * <p>
	 * 默认为“{@link SizeMeasurement#Percentage}”
	 * </p>
	 * 
	 * @return SizeMeasurement
	 */
	public SizeMeasurement getMeasurement() {
		return this.m_measurement;
	}

	/**
	 * 设置相关尺寸配置值的度量单位。
	 * 
	 * @param measurement SizeMeasurement
	 */
	public void setMeasurement(SizeMeasurement measurement) {
		this.m_measurement = measurement;
	}

	/**
	 * 返回包含使用此表单的文档的事件处理程序信息，可选。
	 * 
	 * @return List<EventHandlerInfo> 包含使用此表单的文档的事件处理程序信息，可选。
	 */
	public List<EventHandlerInfo> getEventHandlers() {
		return this.m_eventHandlers;
	}

	/**
	 * 设置包含使用此表单的文档的事件处理程序信息，可选。
	 * 
	 * @param eventHandlers List<EventHandlerInfo> 包含使用此表单的文档的事件处理程序信息，可选。
	 */
	public void setEventHandlers(List<EventHandlerInfo> eventHandlers) {
		this.m_eventHandlers = eventHandlers;
	}

	/**
	 * 返回表单样式。
	 * 
	 * <p>
	 * 默认使用系统内置的样式。
	 * </p>
	 * 
	 * @return String
	 */
	public String getStyle() {
		return this.m_style;
	}

	/**
	 * 设置表单样式。
	 * 
	 * <p>
	 * 可选配置。<br/>
	 * 如果配置，其值必须配置为有效的css文件引用url路径。<br/>
	 * 配置的值可以带上级路径如：“{frame}app_app1/special.css”或者（如果文件在应用程序特定功能目录下则可以）不带路径，如“special.css”。
	 * </p>
	 * 
	 * @param style String
	 */
	public void setStyle(String style) {
		this.m_style = style;
	}

	/**
	 * 返回字段标题默认宽度。
	 * 
	 * <p>
	 * 单位为像素，默认为120。
	 * </p>
	 * 
	 * @return int
	 */
	public int getItemCaptionWidth() {
		return this.m_itemCaptionWidth;
	}

	/**
	 * 设置字段标题默认宽度。
	 * 
	 * @param itemCaptionWidth int
	 */
	public void setItemCaptionWidth(int itemCaptionWidth) {
		this.m_itemCaptionWidth = itemCaptionWidth;
	}

	/**
	 * 根据字段UNID获取字段对象。
	 * 
	 * @param itemUNID
	 * @return Item
	 */
	public Item getItemByUNID(String itemUNID) {
		if (this.m_items == null || this.m_items.isEmpty() || itemUNID == null || itemUNID.length() == 0) return null;
		for (Item x : this.m_items) {
			if (itemUNID.equalsIgnoreCase(x.getUNID())) return x;
		}
		return null;
	}

	/**
	 * 根据字段分组UNID获取字段分组对象。
	 * 
	 * @param itemGroupUNID
	 * @return Item
	 */
	public ItemGroup getItemGroupByUNID(String itemGroupUNID) {
		if (this.m_itemGroups == null || this.m_itemGroups.isEmpty() || itemGroupUNID == null || itemGroupUNID.length() == 0) return null;
		for (ItemGroup x : this.m_itemGroups) {
			if (itemGroupUNID.equalsIgnoreCase(x.getUNID())) return x;
		}
		return null;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Form f = (Form) super.clone();

		f.setItemCaptionWidth(this.getItemCaptionWidth());
		f.setStyle(this.getStyle());
		f.setMeasurement(this.getMeasurement());

		List<EventHandlerInfo> evts = this.getEventHandlers();
		if (evts != null) {
			List<EventHandlerInfo> evtsNew = new ArrayList<EventHandlerInfo>(evts.size());
			for (EventHandlerInfo x : evts) {
				if (x == null) continue;
				evtsNew.add((EventHandlerInfo) x.clone());
			}
			f.setEventHandlers(evtsNew);
		}

		List<ItemGroup> itemGroups = this.getItemGroups();
		if (itemGroups != null) {
			List<ItemGroup> itemGroupsNew = new ArrayList<ItemGroup>(itemGroups.size());
			for (ItemGroup x : itemGroups) {
				if (x == null) continue;
				itemGroupsNew.add((ItemGroup) x.clone());
			}
			f.setItemGroups(itemGroupsNew);
		}

		List<Item> items = this.getItems();
		if (items != null) {
			List<Item> itemsNew = new ArrayList<Item>(items.size());
			for (Item x : items) {
				if (x == null) continue;
				itemsNew.add((Item) x.clone());
			}
			f.setItems(itemsNew);
		}

		return f;
	}
}

