/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 用于描述意见({@link Opinion})类别的资源类。
 * 
 * @author coca@tansuosoft.cn
 */
public class OpinionType extends Resource {
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -4752723335641811952L;

	/**
	 * 系统默认意见类别UNID：BD9866DDBFD84A4EBE46E8C4E58EC754。
	 */
	public static final String DEFAULT_OPINION_TYPE_UNID = "BD9866DDBFD84A4EBE46E8C4E58EC754";

	/**
	 * 缺省构造器
	 */
	public OpinionType() {
	}

}

