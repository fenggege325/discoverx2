/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 菜单指向的目标枚举类型。
 * 
 * @author coca@tansuosoft.cn
 */
public enum MenuTarget implements EnumBase {

	/**
	 * 链接地址（0）。
	 */
	Href(0),
	/**
	 * 指向其它资源（1）。
	 */
	Resource(1),
	/**
	 * 指向客户端TreeView对应的json文本提供类的全限定类名（9）。
	 */
	TreeViewProvider(9),
	/**
	 * 基础分类项，默认值（10）。
	 */
	Category(10);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	MenuTarget(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return MenuTarget
	 */
	public MenuTarget parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (MenuTarget s : MenuTarget.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return MenuTarget.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return MenuTarget
	 */
	public static MenuTarget parse(int v) {
		for (MenuTarget s : MenuTarget.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

