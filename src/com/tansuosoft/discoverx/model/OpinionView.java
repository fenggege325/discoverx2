/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 控制环节审批意见查看权限的枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum OpinionView implements EnumBase {
	/**
	 * 所有可查看文档的人都可以查看意见（1）。
	 * 
	 * <p>
	 * 系统默认使用这个值。
	 * </p>
	 */
	ViewByDocumentViewer(1),
	/**
	 * 所有可查看文档的人都可以查看意见，但是前提是填写意见用户已经结束办理（2）。
	 */
	ViewByDocumentViewerOnDone(2),
	/**
	 * 只有后续审批环节的相关参与者可以查看意见（3）。
	 */
	ViewBySubsequentParticipants(3),
	/**
	 * 自定义（0）。
	 * 
	 * <p>
	 * 用户在填写意见时可以选择某一种方式或者选择哪些用户、部门、群组、角色可以查看意见。
	 * </p>
	 */
	Custom(0);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	OpinionView(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return OpinionView
	 */
	public OpinionView parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (OpinionView s : OpinionView.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return OpinionView.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return OpinionView
	 */
	public static OpinionView parse(int v) {
		for (OpinionView s : OpinionView.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

	/**
	 * 根据枚举对象返回相关文字信息
	 * 
	 * @param logType
	 * @return String
	 */
	public static String getStringValue(OpinionView logType) {
		String value = "[其它方式]";
		switch (logType.getIntValue()) {
		case 0:
			value = "意见公开";
			break;
		case 1:
			value = "只对后续审批人公开";
			break;
		case 2:
			value = "填写意见时再选择";
			break;
		case 4:
			value = "自定义";
			break;
		default:
			break;
		}
		return value;
	}
}

