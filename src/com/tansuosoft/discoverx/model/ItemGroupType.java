/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 字段分组组合方式枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum ItemGroupType implements EnumBase {
	/**
	 * 表格行方式（1）。
	 */
	Table(1),
	/**
	 * 段落方式（2）。
	 */
	Paragraph(2),
	/**
	 * 控制类型（用于定义一组字段的通用行为）。
	 */
	Control(0);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	ItemGroupType(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举值对应的数字返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return ItemGroupType
	 */
	public static ItemGroupType parse(int v) {
		for (ItemGroupType s : ItemGroupType.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字格式字符串返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return ItemGroupType
	 */
	public ItemGroupType parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (ItemGroupType s : ItemGroupType.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return ItemGroupType.valueOf(v);
		}
		return null;
	}

}

