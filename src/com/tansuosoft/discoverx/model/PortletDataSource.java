/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 用于描述显示在门户内容中的某个小窗口中的内容对应的数据来源源的小窗口数据源资源类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class PortletDataSource extends Resource {

	/**
	 * 序列化标识。
	 */
	private static final long serialVersionUID = -13243752369222419L;

	/**
	 * 缺省构造器。
	 */
	public PortletDataSource() {
	}

	private PortletDataSourceType m_dataSourceType = PortletDataSourceType.Ajax; // 数据源类型，默认为PortletDataSourceType.View（视图）。
	private PortletContentType m_contentType = PortletContentType.Url; // 结果类型，默认为PortletContentType.Entry。
	private String m_target = null; // 数据源。
	private String m_serverContentRender = null;
	private String m_clientContentRender = null;
	private int m_refreshInterval = 0; // 自动刷新内容的时间间隔。

	/**
	 * 返回数据源类型。
	 * 
	 * <p>
	 * 默认为:{@link PortletDataSourceType#Ajax}
	 * </p>
	 * 
	 * @return PortletDataSourceType
	 */
	public PortletDataSourceType getDataSourceType() {
		return this.m_dataSourceType;
	}

	/**
	 * 设置数据源类型。
	 * 
	 * @param dataSourceType PortletDataSourceType
	 */
	public void setDataSourceType(PortletDataSourceType dataSourceType) {
		this.m_dataSourceType = dataSourceType;
	}

	/**
	 * 返回结果类型。
	 * 
	 * <p>
	 * 默认为{@link PortletContentType#Url}
	 * </p>
	 * 
	 * @return PortletContentType
	 */
	public PortletContentType getContentType() {
		return this.m_contentType;
	}

	/**
	 * 设置结果类型
	 * 
	 * @param contentType PortletContentType
	 */
	public void setContentType(PortletContentType contentType) {
		this.m_contentType = contentType;
	}

	/**
	 * 返回数据源结果。
	 * 
	 * <p>
	 * 如果数据源类型为“{@link PortletDataSourceType#View}”则结果为视图UNID/别名。
	 * </p>
	 * <p>
	 * 如果数据源类型为“{@link PortletDataSourceType#Ajax}”则结果为Ajax地址。
	 * </p>
	 * <p>
	 * 如果数据源类型为“{@link PortletDataSourceType#Iframe}”则结果为iframe中显示的url地址。
	 * </p>
	 * <p>
	 * 如果数据源类型为“{@link PortletDataSourceType#Custom}”则结果为自定义小窗口内容提供类全限定类名称。即{@link com.tansuosoft.discoverx.web.ui.portal.PortletContentProvider}的实现类的名称。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTarget() {
		return this.m_target;
	}

	/**
	 * 设置数据源结果。
	 * 
	 * <p>
	 * 设置数据源结果时需按照{@link PortletDataSource#getDataSourceType()}返回结果的类型（{@link PortletDataSourceType}）设置符合格式且有意义的结果，请参考{@link PortletDataSource#getTarget()}中的说明。
	 * </p>
	 * 
	 * @param target String
	 */
	public void setTarget(String target) {
		this.m_target = target;
	}

	/**
	 * 返回数据源内容在服务器端输出给浏览器客户端时的全限定呈现类名称。
	 * 
	 * @return String
	 */
	public String getServerContentRender() {
		return this.m_serverContentRender;
	}

	/**
	 * 设置数据源内容在服务器端输出给浏览器客户端时的全限定呈现类名称。
	 * 
	 * <p>
	 * 结果必须是“{@link com.tansuosoft.discoverx.web.ui.portal.PortletContentRender}”的具体实现类。<br/>
	 * 若不设置，系统将根据{@link PortletDataSource#getDataSourceType()}和{@link PortletDataSource#getContentType()}的结果自动获取一个系统内置的实现类。
	 * </p>
	 * 
	 * @param serverContentRender String
	 */
	public void setServerContentRender(String serverContentRender) {
		this.m_serverContentRender = serverContentRender;
	}

	/**
	 * 返回数据源内容在客户端输出最终Html结果时的相关呈现信息。
	 * 
	 * @return String
	 */
	public String getClientContentRender() {
		return this.m_clientContentRender;
	}

	/**
	 * 设置数据源内容在客户端输出最终Html结果时的相关呈现信息。
	 * 
	 * <p>
	 * 不同的数据源类型和内容类型下有不同的意义，通常是指用于将服务器端返回的小窗口数据对应的xml、json、ajax请求结果等内容显示为具体html内容的js函数。<br/>
	 * 请参考具体{@link com.tansuosoft.discoverx.web.ui.portal.PortletContentRender}实现类中的说明。<br/>
	 * 如果服务器端直接输出了最终html，则此配置被忽略。
	 * </p>
	 * 
	 * @param clientContentRender String
	 */
	public void setClientContentRender(String clientContentRender) {
		this.m_clientContentRender = clientContentRender;
	}

	/**
	 * 返回自动刷新内容的时间间隔（单位:秒）。
	 * 
	 * <p>
	 * 如果不设置，则返回0，表示系统自动计算间隔。<br/>
	 * 系统自动计算间隔的周期为基数600(10分钟)加每个Portlet从上到下，从左到右位置的偏移数，后面的Portlet通常比前面的Portlet的自动刷新间隔长。<br/>
	 * </p>
	 * <p>
	 * 此配置仅针对{@link PortletDataSourceType#Ajax}类型的数据源。
	 * </p>
	 * 
	 * @return int
	 */
	public int getRefreshInterval() {
		return this.m_refreshInterval;
	}

	/**
	 * 设置自动刷新内容的时间间隔（单位:秒）。
	 * 
	 * @param refreshInterval int
	 */
	public void setRefreshInterval(int refreshInterval) {
		this.m_refreshInterval = refreshInterval;
	}
}

