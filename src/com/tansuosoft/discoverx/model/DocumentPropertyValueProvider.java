/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 提供文档自定义属性值的类需要实现的接口。
 * 
 * <p>
 * 文档的名称（Name）和摘要（Abstract）等属性值可能根据不同应用程序需要不同的结果，因此可以通过特定的属性值提供类来为这些属性提供定制结果。<br/>
 * 参考：{@link Application#setDocumentNameProviderClassName(String)};<br/>
 * {@link Document#getName()};<br/>
 * {@link Application#setDocumentAbstractProviderClassName(String)};<br/>
 * {@link Document#getAbstract()}.<br/>
 * </p>
 * 
 * @author coca
 */
public interface DocumentPropertyValueProvider {
	/**
	 * 根据传入的文档和可选的额外参数信息提供文档自定义属性值。
	 * 
	 * @return String 返回的结果必须和文档对应属性返回的结果一致。
	 */
	public String providePropertyValue(Document doc, Object... objects);
}

