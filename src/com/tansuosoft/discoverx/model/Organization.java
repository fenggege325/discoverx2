/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;


/**
 * 用于描述用户所属单位和部门等组织机构信息的资源类。
 * 
 * <p>
 * 一个此类的实例对应一个单位名称或部门名称，其包含的下级资源可能是一个单位所属的一级部门列表或部门所属的直接下级部门列表。
 * </p>
 * 
 * <p>
 * 单位、部门等可以支持多个层次，系统建议不超过5层（即一个单位加4级部门）。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Organization extends Resource implements Securer {
	/**
	 * 系统默认根组织的UNID。
	 */
	public static final String ROOT_ORGANIZATION_UNID = "071DF806A8C54199BBBEB1A264D0C9B8";
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -2552037732832370662L;

	/**
	 * 缺省构造器
	 */
	public Organization() {
		super();
	}

	private int m_securityCode = 0; // 组织机构安全编码
	private int m_accountType = AccountType.InvalidAccount.getIntValue() | AccountType.Unit.getIntValue(); // 登录帐号类别。
	private String m_fullName = null;

	/**
	 * 获取组织机构安全编码。
	 * 
	 * @see com.tansuosoft.discoverx.model.Securer#getSecurityCode()
	 */
	public int getSecurityCode() {
		return this.m_securityCode;
	}

	/**
	 * 设置组织机构安全编码。
	 * 
	 * @see com.tansuosoft.discoverx.model.Securer#setSecurityCode(int)
	 */
	public void setSecurityCode(int code) {
		this.m_securityCode = code;
	}

	/**
	 * 获取帐号类型。
	 * 
	 * <p>
	 * 账号类别是{@link AccountType}枚举中值对应数字的某一个或多个组合，组织机构的登录账号类别固定为“{@link AccountType#InvalidAccount}对应数字值 + {@link AccountType#Unit}对应数字值”（12）。
	 * </p>
	 * 
	 * @return int
	 */
	public int getAccountType() {
		return this.m_accountType;
	}

	/**
	 * 设置登录帐号类型。
	 * 
	 * @see Participant#getAccountType()
	 * @param accountType int
	 */
	public void setAccountType(int accountType) {
		this.m_accountType = accountType;
	}

	/**
	 * 获取组织全名（层次名）。
	 * 
	 * @return
	 */
	public String getFullName() {
		return m_fullName;
	}

	/**
	 * 重载getName：返回部门名称（即无层级的直接名称）。
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#getName()
	 */
	@Override
	public String getName() {
		String name = super.getName();
		if (name != null && name.length() > 0) {
			int pos = name.lastIndexOf(User.SEPARATOR_CHAR);
			if (pos > 0) return name.substring(pos + 1);
		}
		return name;
	}

	/**
	 * 设置组织全名（层次名）。
	 * 
	 * @param fn
	 */
	public void setFullName(String fn) {
		m_fullName = fn;
	}
}

