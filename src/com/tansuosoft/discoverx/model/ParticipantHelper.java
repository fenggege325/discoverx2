/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 参与者实用工具类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ParticipantHelper {
	/**
	 * 返回参与者相关的属性值根据提供的格式模板格式化后的结果。
	 * 
	 * <p>
	 * 系统内置的合法有效的格式标记请参考“{@link User#LEGALLY_NAME_PART_FORMATS}”的说明。
	 * <p>
	 * <p>
	 * 各格式标记可以组合，比如“ou0\cn”，表示“用户直接部门\用户姓名”。
	 * </p>
	 * 
	 * @param securer 表示要获取其某一部分属性值的参与者对象，可以是用户、部门、群组、角色资源对象或具体参与者{@link Participant}对象。
	 * @param format String 指示输出格式的字符串。
	 * 
	 * @return String 返回按照提供的模板格式化后的结果，如果format没有包含有效的格式标记，则返回format本身。
	 */
	public static String getFormatValue(Securer securer, String format) {
		if (securer == null) return null;

		String[] replacements = new String[User.LEGALLY_NAME_PART_FORMATS.length];
		replacements[8] = Integer.toString(securer.getSecurityCode()); // sc
		if (securer instanceof User) {
			User u = (User) securer;
			fillOus(replacements, u.getName());
			replacements[7] = u.getUNID(); // id
			replacements[9] = u.getAlias(); // an(alias)
		} else if (securer instanceof Participant) {
			Participant p = ((Participant) securer);
			fillOus(replacements, p.getName());
			replacements[7] = p.getUNID();
			replacements[9] = p.getAlias();
		} else if (securer instanceof Resource) {
			Resource r = ((Resource) securer);
			fillOus(replacements, r.getName());
			replacements[7] = r.getUNID();
			replacements[9] = r.getAlias();
		}
		return format(format, replacements);
	}

	/**
	 * 返回层次名对应的虚拟用户对象根据提供的格式模板格式化后的结果。
	 * 
	 * @param fullName 层次名
	 * @param format 格式模板，参见{@link ParticipantHelper#getFormatValue(Securer, String)}。
	 * @return String 返回按照提供的模板格式化后的结果，如果format没有包含有效的格式标记，则返回format本身。
	 */
	public static String getFormatValue(String fullName, String format) {
		String[] replacements = new String[User.LEGALLY_NAME_PART_FORMATS.length];
		fillOus(replacements, fullName);
		replacements[7] = "";
		replacements[8] = "0";
		replacements[9] = "";
		return format(format, replacements);
	}

	/**
	 * 按照format指定格式输出结果。
	 * 
	 * @param format
	 * @param replacements
	 * @return
	 */
	protected static String format(final String format, final String[] replacements) {
		if (format == null || format.trim().length() == 0) return format;
		if ("fn".equals(format)) return replacements[5];

		int rl = format.length();
		int last = (rl - 1);
		char c0 = 0;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < rl; i++) {
			int offset = -1;
			c0 = format.charAt(i);
			switch (c0) {
			case 'o':
				char c1 = (i < last ? format.charAt(i + 1) : 0);
				char c2 = (i < last - 1 ? format.charAt(i + 2) : 0);
				if (c1 == 'u' && c2 >= 48 && c2 <= 52) {
					offset = c2 - 48; // 0-4
					i += 2;
				} else {
					offset = 10; // 10
				}
				break;
			case 'f':
				if (i < last && format.charAt(i + 1) == 'n') offset = 5;
				break;
			case 'c':
				if (i < last && format.charAt(i + 1) == 'n') offset = 6;
				break;
			case 'i':
				if (i < last && format.charAt(i + 1) == 'd') offset = 7;
				break;
			case 's':
				if (i < last && format.charAt(i + 1) == 'c') offset = 8;
				break;
			case 'a':
				if (i < last && format.charAt(i + 1) == 'n') offset = 9;
				break;
			}
			if (offset >= 0) {
				sb.append(replacements[offset]);
				if (offset >= 5 && offset < 10) i++;
			} else {
				sb.append(c0);
			}
		}// for end
		return sb.toString();
	}

	protected static final String ORG_NAME = CommonConfig.getInstance().getBelong();

	/**
	 * 填充层次名相关信息。
	 * 
	 * @param replacements
	 * @param fn
	 */
	protected static void fillOus(String[] replacements, String fn) {
		String str = (fn == null ? "" : fn);
		String ns[] = StringUtil.splitString(str, User.SEPARATOR_CHAR);
		replacements[5] = fn; // fn
		replacements[6] = (ns.length == 0 ? "" : ns[ns.length - 1]); // cn
		replacements[10] = (ns.length <= 1 ? ORG_NAME : ns[0]); // o
		replacements[0] = (ns.length < 2 ? ORG_NAME : ns[ns.length - 2]); // ou0
		replacements[1] = (ns.length < 3 ? "" : ns[1]); // ou1
		replacements[2] = (ns.length < 4 ? "" : ns[2]); // ou2
		replacements[3] = (ns.length < 5 ? "" : ns[3]); // ou3
		replacements[4] = (ns.length < 6 ? "" : ns[4]); // ou4
	}

}

