/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 表示包含于文档中的字段信息的类。
 * 
 * <p>
 * 一个文档字段通常对应一个表单字段资源（如果这个字段是内置字段{@link Document#isBuiltinField(String)}，则没有对应表单字段资源）。<br/>
 * 同时还包含字段值（如果这个字段是内置字段{@link Document#isBuiltinField(String)} ，则没有字段值）、变更状态等信息。
 * </p>
 * 
 * @see Document
 * @see Item
 * @author coca@tansuosoft.cn
 */
public class Field implements Comparable<Field> {

	/**
	 * 缺省构造器。
	 */
	protected Field() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param item Item 对应表单字段。
	 * @param fieldType FieldType 字段类型。
	 * @param val String 字段值文本。
	 */
	protected Field(Item item, FieldType fieldType, String val) {
		this.m_item = item;
		this.m_value = val;
		this.m_type = fieldType;
		if (item != null) {
			this.m_name = item.getItemName();
			this.m_caption = item.getCaption();
		}
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param name String 字段名。
	 * @param fieldType FieldType 字段类型。
	 * @param val String 字段值文本。
	 * @param caption 字段标题
	 */
	protected Field(String name, FieldType fieldType, String val, String caption) {
		this.m_item = null;
		this.m_type = fieldType;
		this.m_name = name;
		this.m_value = val;
		m_caption = caption;
	}

	private String m_name = null;
	private Item m_item = null;
	private FieldType m_type = FieldType.Unknown;
	private String m_value = "";
	private PersistenceState m_persistenceState = PersistenceState.Raw; // 持久化保存状态。
	private String m_caption = null;

	/**
	 * 获取文档字段对应的表单字段资源。
	 * 
	 * @return
	 */
	public Item getItem() {
		return this.m_item;
	}

	/**
	 * 获取文档字段名。
	 * 
	 * @return
	 */
	public String getName() {
		if (this.m_name == null || this.m_name.length() == 0) {
			if (this.m_item != null) this.m_name = this.m_item.getItemName();
		}
		return this.m_name;
	}

	/**
	 * 获取文档字段类型。
	 * 
	 * <p>
	 * 默认为未知类型：FieldType.Unknown。
	 * </p>
	 * 
	 * @see FieldType
	 * @return
	 */
	public FieldType getFieldType() {
		return this.m_type;
	}

	/**
	 * 设置文档字段类型。
	 * 
	 * @param fieldType
	 */
	protected void setFieldType(FieldType fieldType) {
		this.m_type = fieldType;
	}

	/**
	 * 获取文档字段值对应的文本。
	 * 
	 * <p>
	 * 如果是内置字段，则返回null。请通过{@link Document#getItemValue(String)}或相应的getter方法获取内置字段的值。
	 * </p>
	 * 
	 * @return String
	 */
	public String getValue() {
		return this.m_value;
	}

	/**
	 * 设置文档字段值对应的文本。
	 * 
	 * @param val
	 */
	protected void setValue(String val) {
		this.m_value = val;
	}

	/**
	 * 获取持久化保存状态，默认为{@link PersistenceState.Raw}
	 * 
	 * @return PersistenceState
	 */
	public PersistenceState getPersistenceState() {
		return this.m_persistenceState;
	}

	/**
	 * 设置持久化保存状态。
	 * 
	 * @param persistenceState PersistenceState
	 */
	public void setPersistenceState(PersistenceState persistenceState) {
		this.m_persistenceState = persistenceState;
	}

	/**
	 * 获取字段标题。
	 * 
	 * @return
	 */
	public String getCaption() {
		if (this.m_caption == null || this.m_caption.length() == 0) {
			if (this.m_item != null) this.m_caption = this.m_item.getCaption();
		}
		return this.m_caption;
	}

	/**
	 * 对象比较。
	 * 
	 * <p>
	 * 比较方式：先比较其{@link #getFieldType()}结果，如果相等则比较{@link #getName()}结果。
	 * </p>
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Field o) {
		if (o == null) return 1;
		int result = this.getFieldType().getIntValue() - o.getFieldType().getIntValue();
		if (result == 0) {
			String n = this.getName();
			if (n == null) return -1;
			result = n.compareTo(o.getName());
		}
		return result;
	}
}

