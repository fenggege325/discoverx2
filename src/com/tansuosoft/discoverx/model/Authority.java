/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.List;

/**
 * 定义赋予用户{@link Participant}或群组{@link Group}资源地群组、角色等职权（授权）信息的类。
 * 
 * <p>
 * 它可能包含一个或多个{@link AuthorityEntry}。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Authority {
	private String m_PUNID = null; // 此授权信息被赋予的用户或群组资源的UNID。
	private List<AuthorityEntry> m_authorityEntries = null; // 包含所属的群组、角色等职权信息条目的列表。

	/**
	 * 缺省构造器。
	 */
	public Authority() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param punid String 此授权信息被赋予的用户或群组资源的UNID。
	 */
	public Authority(String punid) {
		this.m_PUNID = punid;
	}

	/**
	 * 返回此授权信息被赋予的用户或群组资源的UNID。
	 * 
	 * @return String
	 */
	public String getPUNID() {
		return this.m_PUNID;
	}

	/**
	 * 设置此授权信息被赋予的用户或群组资源的UNID。
	 * 
	 * @param PUNID String
	 */
	public void setPUNID(String PUNID) {
		this.m_PUNID = PUNID;
	}

	/**
	 * 返回包含所属群组、角色等职权信息条目的列表。
	 * 
	 * @return List<AuthorityEntry>
	 */
	public List<AuthorityEntry> getAuthorityEntries() {
		return this.m_authorityEntries;
	}

	/**
	 * 设置包含所属群组、角色等职权信息条目的列表。
	 * 
	 * @param authorityEntries List<AuthorityEntry>
	 */
	public void setAuthorityEntries(List<AuthorityEntry> authorityEntries) {
		this.m_authorityEntries = authorityEntries;
	}
}

