/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 描述系统参与者信息的类。
 * 
 * <p>
 * 表示系统内具有安全编码属性的资源相关通用信息的类。
 * </p>
 * <p>
 * 它描述用户（{@link Participant}）、组织（{@linkOrganization}）、群组（{@link Group}）、角色（{@link Role}）等资源中某一个资源的公共属性。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */

public class Participant implements Securer, Cloneable {
	private int m_securityCode = 0;// 获取或设置系统全局唯一安全编码值，必须。
	private String m_name = null;// 唯一参与者名称。
	private String m_UNID = null;// 唯一32位UNID。
	private String m_alias = null; // 唯一别名。
	private ParticipantType m_participantType = ParticipantType.Person;// 参与者类型.
	private boolean m_selected = false;// 是否选中。

	/**
	 * 缺省构造器
	 */
	public Participant() {
	}

	/**
	 * 接收必要属性的造器。
	 * 
	 * @param code
	 * @param name
	 * @param unid
	 * @param alias
	 * @param type
	 */
	public Participant(int securityCode, String name, String unid, String alias, ParticipantType participantType) {
		this(securityCode, name, unid, alias, participantType, false);
	}

	/**
	 * 接收必要属性的造器。
	 * 
	 * @param code
	 * @param name
	 * @param unid
	 * @param alias
	 * @param type
	 */
	public Participant(int securityCode, String name, String unid, String alias) {
		this(securityCode, name, unid, alias, ParticipantType.Person, false);
	}

	/**
	 * 接收必要属性的造器。
	 * 
	 * @param securityCode
	 * @param name
	 * @param unid
	 * @param alias
	 * @param type
	 * @param selected
	 */
	public Participant(int securityCode, String name, String unid, String alias, ParticipantType participantType, boolean selected) {
		this.m_securityCode = securityCode;
		this.m_name = name;
		this.m_UNID = unid;
		this.m_alias = alias;
		this.m_participantType = participantType;
		this.m_selected = selected;
	}

	/**
	 * 接收{@link Securer}对象的构造器。
	 * 
	 * @param securer
	 */
	public Participant(Securer securer) {
		if (securer == null) return;

		this.m_securityCode = securer.getSecurityCode();
		if (securer instanceof Resource) {
			Resource r = (Resource) securer;
			this.m_name = r.getName();
			this.m_UNID = r.getUNID();
			this.m_alias = r.getAlias();
		} else if (securer instanceof Participant) {
			Participant r = (Participant) securer;
			this.m_name = r.getName();
			this.m_UNID = r.getUNID();
			this.m_alias = r.getAlias();
		}
		if (securer instanceof User) {
			this.m_participantType = ParticipantType.Person;
		} else if (securer instanceof Group) {
			this.m_participantType = ParticipantType.Group;
		} else if (securer instanceof Role) {
			this.m_participantType = ParticipantType.Role;
		} else if (securer instanceof Organization) {
			this.m_participantType = ParticipantType.Organization;
		} else if (securer instanceof Participant) {
			this.m_participantType = ((Participant) securer).getParticipantType();
		} else {
			this.m_participantType = ParticipantType.Other;
		}

		this.m_selected = false;
	}

	/**
	 * 获取系统全局唯一安全编码值。
	 * 
	 * @return int
	 */

	public int getSecurityCode() {
		return this.m_securityCode;
	}

	/**
	 * 设置系统全局唯一安全编码值。
	 * 
	 * @param securityCode int
	 */

	public void setSecurityCode(int securityCode) {
		this.m_securityCode = securityCode;
	}

	/**
	 * 返回唯一参与者名称。
	 * 
	 * <p>
	 * 对于不同的参与者类型，其值具有不同意义，说明如下：
	 * <ul>
	 * <li>如果是用户则为用户全名（层次名），如：组织名\组织单元1...\通用名；</li>
	 * <li>如果是部门则保存部门全名（层次名）；</li>
	 * <li>如果是群组保存群组名称；</li>
	 * <li>如果是角色保存角色名称；</li>
	 * <li>其它类型则保存相应名称。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置唯一参与者名称。
	 * 
	 * @param name String
	 * 
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回唯一32位UNID值。
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置唯一32位UNID值。
	 * 
	 * @param unid String
	 * 
	 */
	public void setUNID(String unid) {
		this.m_UNID = unid;
	}

	/**
	 * 返回唯一别名。
	 * 
	 * @return String
	 */
	public String getAlias() {
		return this.m_alias;
	}

	/**
	 * 设置唯一别名。
	 * 
	 * @param alias String
	 */
	public void setAlias(String alias) {
		this.m_alias = alias;
	}

	/**
	 * 返回参与者类型。
	 * 
	 * <p>
	 * 默认为{@link ParticipantType.Person}。
	 * </p>
	 * 
	 * @return ParticipantType
	 */

	public ParticipantType getParticipantType() {
		return this.m_participantType;
	}

	/**
	 * 设置参与者名称对应的UNID值
	 * 
	 * @param type ParticipantType
	 */
	public void setParticipantType(ParticipantType type) {
		this.m_participantType = type;
	}

	/**
	 * 返回是否选中。
	 * 
	 * <p>
	 * 默认为false。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getSelected() {
		return this.m_selected;
	}

	/**
	 * 设置是否选中。
	 * 
	 * @param seleted boolean
	 */
	public void setSelected(boolean selected) {
		this.m_selected = selected;
	}

	/**
	 * 重载toString：返回安全编码的字符串形式。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.m_securityCode);
		return sb.toString();
	}

	/**
	 * 重载hashCode
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_UNID == null) ? 0 : m_UNID.hashCode());
		result = prime * result + ((m_alias == null) ? 0 : m_alias.hashCode());
		result = prime * result + ((m_name == null) ? 0 : m_name.hashCode());
		result = prime * result + ((m_participantType == null) ? 0 : m_participantType.hashCode());
		result = prime * result + m_securityCode;
		result = prime * result + (m_selected ? 1231 : 1237);
		return result;
	}

	/**
	 * 重载equals：所有属性值相等则相等。
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Participant other = (Participant) obj;
		if (m_UNID == null) {
			if (other.m_UNID != null) return false;
		} else if (!m_UNID.equals(other.m_UNID)) return false;
		if (m_alias == null) {
			if (other.m_alias != null) return false;
		} else if (!m_alias.equals(other.m_alias)) return false;
		if (m_name == null) {
			if (other.m_name != null) return false;
		} else if (!m_name.equals(other.m_name)) return false;
		if (m_participantType == null) {
			if (other.m_participantType != null) return false;
		} else if (!m_participantType.equals(other.m_participantType)) return false;
		if (m_securityCode != other.m_securityCode) return false;
		if (m_selected != other.m_selected) return false;
		return true;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Participant x = new Participant();

		x.setSelected(this.getSelected());
		x.setSecurityCode(this.getSecurityCode());
		x.setAlias(this.getAlias());
		x.setName(this.getName());
		x.setUNID(this.getUNID());
		x.setParticipantType(this.getParticipantType());

		return x;
	}
}
