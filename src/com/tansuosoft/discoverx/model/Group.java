/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;


/**
 * 用于描述系统群组信息的资源类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Group extends Resource implements Securer {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -3177475518736957728L;

	/**
	 * 缺省构造器
	 */
	public Group() {
		super();
	}

	private int m_securityCode = 0; // 群组的安全编码
	private GroupType m_groupType = GroupType.Static; // 群组类型，默认为静态群组（GroupType.Static）。
	private String m_groupImplement = null; // 动态群组包含的具体人员列表的实现类的全限定类名。
	private Authority m_authority = null; // 群组被赋予的授权信息。

	/**
	 * 设置角色安全编码。
	 * 
	 * @see com.tansuosoft.discoverx.model.Securer#getSecurityCode()
	 */
	public int getSecurityCode() {
		return this.m_securityCode;
	}

	/**
	 * 设置角色安全编码。
	 * 
	 * @see com.tansuosoft.discoverx.model.Securer#setSecurityCode(int)
	 */
	public void setSecurityCode(int code) {
		this.m_securityCode = code;
	}

	/**
	 * 返回群组类型。
	 * 
	 * @return GroupType
	 */
	public GroupType getGroupType() {
		return this.m_groupType;
	}

	/**
	 * 设置群组类型。
	 * 
	 * <p>
	 * 默认为静态群组（{@link GroupType#Static}）。<br/>
	 * 静态群组一般用于区分跨部门具有类似职权的用户，如：部门领导、单位领导、文书、秘书等。<br/>
	 * 动态群组表示运行时刻才知道本身包含哪些具体用户的群组，它一般用来限定流程参与者范围等，如：同部门用户、本部门领导等。
	 * </p>
	 * 
	 * @param groupType GroupType
	 */
	public void setGroupType(GroupType groupType) {
		this.m_groupType = groupType;
	}

	/**
	 * 返回动态群组包含的具体人员列表的实现类的全限定类名。
	 * 
	 * @return String
	 */
	public String getGroupImplement() {
		return this.m_groupImplement;
	}

	/**
	 * 设置动态群组包含的具体人员列表的实现类的全限定类名。
	 * 
	 * <p>
	 * 必须是“{@link com.tansuosoft.discoverx.bll.ParticipantsProvider}”的实现类的全限定类名。<br/>
	 * 只有类型为动态群组({@link GroupType#Dynamic})时有意义。
	 * </p>
	 * 
	 * @param groupImplement String
	 */
	public void setGroupImplement(String groupImplement) {
		this.m_groupImplement = groupImplement;
	}

	/**
	 * 返回群组被赋予的授权信息。
	 * 
	 * <p>
	 * 可能包含角色等，也可能都不包含任何授权信息。
	 * </p>
	 * 
	 * @return Authority
	 */
	public Authority getAuthority() {
		return this.m_authority;
	}

	/**
	 * 设置群组被赋予的授权信息。
	 * 
	 * <p>
	 * 注：只有静态群组才有意义，由于动态群组需要在运行时才知道具体包含了哪些用户，因此不能给动态群组授权。
	 * </p>
	 * 
	 * @param authority Authority
	 */
	public void setAuthority(Authority authority) {
		this.m_authority = authority;
	}

}

