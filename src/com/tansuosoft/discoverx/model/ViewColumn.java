/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 描述视图数据列（视图列）的类（查询配置项）。
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewColumn extends ViewQueryConfig implements java.io.Serializable {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 8610253922084190873L;

	private String m_title = null; // 列标题，可选。
	private String m_columnAlias = null; // 新字段名，可选（如table.col1 as colnew中的colnew）。
	private boolean m_categorized = false; // 是否分类列（分类列可以以下拉的方式过滤视图数据），默认为false。
	private boolean m_searchable = false; // 是否可供视图检索之用。
	private boolean m_linkable = true; // 列值显示时是否可作为链接单击，默认为true。
	private int m_width = 0; // 列宽。
	private String m_dictionary = null; // 字段值绑定的字典。

	/**
	 * 缺省构造器。
	 */
	public ViewColumn() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param tableName
	 * @param columnName
	 * @param title
	 */
	public ViewColumn(String tableName, String columnName, String title) {
		super(tableName, columnName);
		this.m_title = title;
	}

	/**
	 * 返回列标题。
	 * 
	 * @return String
	 */
	public String getTitle() {
		return this.m_title;
	}

	/**
	 * 设置列标题，必须。
	 * 
	 * @param title String
	 */
	public void setTitle(String title) {
		this.m_title = title;
	}

	/**
	 * 返回是否分类列，默认为false。
	 * 
	 * <p>
	 * 返回true表示此列的值可用于给视图做额外分类之用，一般作为分类列的字段值不能太多，否则可能导致分类没有意义并且影响系统性能。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getCategorized() {
		return this.m_categorized;
	}

	/**
	 * 设置是否分类列。
	 * 
	 * @param categorized boolean
	 */
	public void setCategorized(boolean categorized) {
		this.m_categorized = categorized;
	}

	/**
	 * 返回是否可供视图高级检索使用，默认为false。
	 * 
	 * <p>
	 * 如果为true则表示此列的字段可以列在视图高级搜索的组合搜索条件中以供做额外搜索之用。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getSearchable() {
		return this.m_searchable;
	}

	/**
	 * 设置是否可供视图高级检索使用。
	 * 
	 * @param searchable boolean
	 */
	public void setSearchable(boolean searchable) {
		this.m_searchable = searchable;
	}

	/**
	 * 返回列宽。
	 * 
	 * <p>
	 * 根据{@link View#getMeasurement()}结果可以理解为像素或百分比数字。
	 * </p>
	 * <p>
	 * 显示视图内容时，列框小于0的列不显示，配置时可以将那些只用来做检索用的搜索列（{@link #getSearchable()}）宽度设为小于0的数字并放置到所有显示列的后面。<br/>
	 * 最后一个显示列的列宽可以为0（表示自动计算）。
	 * </p>
	 * 
	 * @return int
	 */
	public int getWidth() {
		return this.m_width;
	}

	/**
	 * 设置列宽。
	 * 
	 * <p>
	 * 设置时，所有列的总和不能超过100（百分比为单位时）或者浏览器支持的最大显示像素。
	 * </p>
	 * 
	 * @param width int。
	 */
	public void setWidth(int width) {
		this.m_width = width;
	}

	/**
	 * 返回字段值绑定的字典。
	 * 
	 * <p>
	 * 可以是某个字典资源UNID或别名（用于替换实际显示的列值和高级搜索时选择值）；也可以是JSON对象格式的选择框信息或数据源信息（用于高级搜索时选择值或视图呈现）。<br/>
	 * 以下都是有效的配置项：<br/>
	 * <ul>
	 * <li><strong>dictTest</strong>：表示绑定别名为“dictTest”的字典资源，显示值（标题）为字典包含的字典项的名称，保存值（实际值）为字典包含的字典项的别名（如果没有配置别名则保存值与显示值相同）；</li>
	 * <li><strong>191E2FF947CB4EA9B58C1D063B268746</strong>：表示绑定UNID为“191E2FF947CB4EA9B58C1D063B268746”的字典资源，显示值（标题）为字典包含的字典项的名称，保存值（实际值）为字典包含的字典项的别名（
	 * 如果没有配置别名则保存值与显示值相同）；</li>
	 * <li><strong>{datasource:'p://user',value:'fn',label:'cn'}</strong>：表示绑定用户参与者，显示值（标题）为用户姓名，保存值（实际值）为用户层次名（全名）；</li>
	 * <li><strong>{datasource:'dt://',value:'yyyy-mm-dd'}</strong>：表示绑定日期时间，返回格式为“yyyy-mm-dd”；</li>
	 * <li><strong>[{v:'value1',t:'title1'},{v:'value2',t:'title2'}]</strong>：表示视图呈现时所使用的值与标题一一映射的JSON对象数组，数组中每一个JSON的“v”表示保存值（实际值），“t”表示显示值（标题） ；</li>
	 * <li>......</li>
	 * </ul>
	 * </p>
	 * 
	 * @return String
	 */
	public String getDictionary() {
		return this.m_dictionary;
	}

	/**
	 * 设置字段值绑定的字典，可选。
	 * 
	 * @param dictionary String
	 */
	public void setDictionary(String dictionary) {
		this.m_dictionary = dictionary;
	}

	/**
	 * 返回字段列别名。
	 * 
	 * @see ViewColumn#setColumnAlias(String)
	 * @return String
	 */
	public String getColumnAlias() {
		return this.m_columnAlias;
	}

	/**
	 * 设置字段列别名。
	 * 
	 * <p>
	 * 如“TableName.ColumnName [as] colnew”中的“colnew”即为ColumnAlias结果。<br/>
	 * 如果有配置了表达式则一定要提供别名。如果不提供或没有表达式，则ColumnName会作为默认别名。
	 * </p>
	 * <p>
	 * 只对来自数据来数据库表记录的视图有效。
	 * </p>
	 * 
	 * @see ViewQueryConfig#setExpression(String)
	 * @param columnAlias String
	 */
	public void setColumnAlias(String columnAlias) {
		this.m_columnAlias = columnAlias;
	}

	/**
	 * 返回列值显示时是否可作为链接单击，默认为true。
	 * 
	 * @return boolean
	 */
	public boolean getLinkable() {
		return this.m_linkable;
	}

	/**
	 * 设置列值显示时是否可作为链接单击，默认为true。
	 * 
	 * @param linkable boolean
	 */
	public void setLinkable(boolean linkable) {
		this.m_linkable = linkable;
	}

	/**
	 * 是否为有效列（即包含有效表名和字段名的列）。
	 * 
	 * @return
	 */
	public boolean isValid() {
		return (!StringUtil.isBlank(this.getTableName()) && !StringUtil.isBlank(this.getColumnName()));
	}

	/**
	 * 返回列是否可以显示。
	 * 
	 * <p>
	 * 不能显示的列不会从数据库中选择并填充值。
	 * </p>
	 * <p>
	 * 列宽({@link #getWidth()})小于0或没有提供合法表名（{@link #getTableName()}）和字段名（{@link #getColumnName()}）的列不能显示。
	 * </p>
	 * 
	 * @return
	 */
	public boolean getVisible() {
		return (this.getWidth() >= 0 && isValid());
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ViewColumn x = (ViewColumn) super.clone();

		x.setCategorized(this.getCategorized());
		x.setLinkable(this.getLinkable());
		x.setSearchable(this.getSearchable());
		x.setWidth(this.getWidth());
		x.setColumnAlias(this.getColumnAlias());
		x.setTitle(this.getTitle());
		x.setDictionary(this.getDictionary());

		return x;
	}
}

