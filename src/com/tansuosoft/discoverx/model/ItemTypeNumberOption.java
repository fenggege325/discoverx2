/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 字段值数据类型为数字类型的额外选项类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ItemTypeNumberOption extends ItemTypeOption {
	/**
	 * 缺省构造器。
	 */
	public ItemTypeNumberOption() {
	}

	private int m_precision = 8; // 数字的最长位数，默认为8。
	private int m_scale = 0; // 小数点位数，默认为0（表示整数）。

	/**
	 * 返回数字的最长位数，默认为8。
	 * 
	 * @return int
	 */
	public int getPrecision() {
		return this.m_precision;
	}

	/**
	 * 设置数字的最长位数，默认为8。
	 * 
	 * @param precision int
	 */
	public void setPrecision(int precision) {
		this.m_precision = precision;
	}

	/**
	 * 返回小数点位数，默认为0（表示整数）。
	 * 
	 * @return int
	 */
	public int getScale() {
		return this.m_scale;
	}

	/**
	 * 设置小数点位数，默认为0（表示整数）。
	 * 
	 * @param scale int
	 */
	public void setScale(int scale) {
		this.m_scale = scale;
	}
}

