/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.bll.function.ExpressionParser;

/**
 * 描述视图选择条件的类（查询配置项）。
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewCondition extends ViewQueryConfig implements java.io.Serializable {
	/**
	 * 表示条件组合的AND组合的字符串：“ AND ”。
	 */
	public static final String CONDITION_COMBINATION_AND = " AND ";
	/**
	 * 表示条件组合的OR组合的字符串：“ OR ”。
	 */
	public static final String CONDITION_COMBINATION_OR = " OR ";

	/**
	 * 左括号：“(”。
	 */
	public static final String LP = "(";

	/**
	 * 左括号：“)”。
	 */
	public static final String RP = ")";

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 9065986948960842049L;

	private String m_leftBracket = ""; // 左括号。
	private String m_compare = "="; // 比较方式。
	private String m_rightValue = null; // 比较右值
	private String m_rightBracket = ""; // 右括号。
	private String m_combineNextWith = CONDITION_COMBINATION_AND; // 和下一个条件的逻辑组合方式，缺省为“ AND ”。
	private boolean m_numericCompare = false; // 是否按数字方式比较。

	/**
	 * 缺省构造器。
	 */
	public ViewCondition() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param tableName
	 * @param columnName
	 */
	public ViewCondition(String tableName, String columnName) {
		super(tableName, columnName);
	}

	/**
	 * 返回左括号。
	 * 
	 * @return String
	 */
	public String getLeftBracket() {
		return this.m_leftBracket;
	}

	/**
	 * 设置左括号，可选。
	 * 
	 * @param leftBracket String
	 */
	public void setLeftBracket(String leftBracket) {
		this.m_leftBracket = leftBracket;
	}

	/**
	 * 返回比较方式。
	 * 
	 * <p>
	 * 默认为等于。
	 * </p>
	 * 
	 * @return String
	 */
	public String getCompare() {
		return this.m_compare;
	}

	/**
	 * 设置比较方式，可选。
	 * 
	 * @param compare String
	 */
	public void setCompare(String compare) {
		this.m_compare = compare;
	}

	/**
	 * 返回比较右值（位于查询比较表达式右边的比较值）。
	 * 
	 * @return String
	 */
	public String getRightValue() {
		return this.m_rightValue;
	}

	/**
	 * 设置比较右值，必须，可以是常数或公式表达式。
	 * 
	 * <p>
	 * 注：在SQL片断中的出现表达式的地方只能配置单个公式，不能有其它操作符（但是表达式的参数可以有其它表达式），因为会与sql运算符冲突造成歧义（所以如果出现运算符系统将其作为sql运算符，而不是表达式运算符）。<br/>
	 * 关于表达式配置方式请参考：{@link ExpressionParser}。
	 * </p>
	 * 
	 * @param rightValue String
	 */
	public void setRightValue(String rightValue) {
		this.m_rightValue = rightValue;
	}

	/**
	 * 返回右括号。
	 * 
	 * @return String
	 */
	public String getRightBracket() {
		return this.m_rightBracket;
	}

	/**
	 * 设置右括号，可选。
	 * 
	 * @param rightBracket String
	 */
	public void setRightBracket(String rightBracket) {
		this.m_rightBracket = rightBracket;
	}

	/**
	 * 返回和下一个条件的逻辑组合方式，缺省为“ AND ”。
	 * 
	 * @return String
	 */
	public String getCombineNextWith() {
		return this.m_combineNextWith;
	}

	/**
	 * 设置和下一个条件的逻辑组合方式。
	 * 
	 * @param combineNextWith String
	 */
	public void setCombineNextWith(String combineNextWith) {
		this.m_combineNextWith = combineNextWith;
	}

	/**
	 * 返回是否按数字方式比较。
	 * 
	 * <p>
	 * 默认为false，此时将自动为右值加头尾单引号并进行必要的特殊字符编码，如果为true，则比较时将直接使用右值结果。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getNumericCompare() {
		return this.m_numericCompare;
	}

	/**
	 * 设置是否按数字方式比较。
	 * 
	 * <p>
	 * 默认为false，此时将自动为右值加头尾单引号并进行必要的特殊字符编码，如果为true，则比较时将直接使用右值结果。
	 * </p>
	 * 
	 * @param numericCompare boolean
	 */
	public void setNumericCompare(boolean numericCompare) {
		this.m_numericCompare = numericCompare;
	}

	/**
	 * 重载toString
	 * 
	 * @see com.tansuosoft.discoverx.model.ViewQueryConfig#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString());
		sb.insert(0, this.m_leftBracket);
		sb.append(" ").append(this.m_compare);
		sb.append(" ").append(this.m_rightValue);
		sb.append(this.m_rightBracket);
		sb.append(" ").append(this.m_combineNextWith);
		sb.append(this.m_numericCompare ? "[n]" : "[s]");
		return sb.toString();
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ViewCondition x = (ViewCondition) super.clone();

		x.setNumericCompare(this.getNumericCompare());
		x.setCombineNextWith(this.getCombineNextWith());
		x.setCompare(this.getCompare());
		x.setLeftBracket(this.getLeftBracket());
		x.setRightBracket(this.getRightBracket());
		x.setRightValue(this.getRightValue());

		return x;
	}

}

