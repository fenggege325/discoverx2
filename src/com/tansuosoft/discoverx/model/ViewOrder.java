/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

/**
 * 描述视图排序方法的类（查询配置项）。
 * 
 * @author coca@tansuosoft.cn
 */
public class ViewOrder extends ViewQueryConfig implements java.io.Serializable {
	/**
	 * SQL Order By子句的降序排序方式常数：“ DESC ”
	 */
	public static final String ORDERBY_DESC = " DESC ";
	/**
	 * SQL Order By子句的升序排序方式常数：“ ASC ”
	 */
	public static final String ORDERBY_ASC = " ASC ";
	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = -6648195372631526331L;

	private String m_orderBy = ORDERBY_DESC; // 排序方式，默认为“ DESC ”。

	/**
	 * 缺省构造器。
	 */
	public ViewOrder() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param tableName
	 * @param columnName
	 */
	public ViewOrder(String tableName, String columnName) {
		super(tableName, columnName);
	}

	/**
	 * 返回排序方式，默认为“ DESC ”。
	 * 
	 * @return String
	 */
	public String getOrderBy() {
		return this.m_orderBy;
	}

	/**
	 * 设置排序方式。
	 * 
	 * @param orderBy String
	 */
	public void setOrderBy(String orderBy) {
		this.m_orderBy = orderBy;
	}

	/**
	 * 重载toString
	 * 
	 * @see com.tansuosoft.discoverx.model.ViewQueryConfig#toString()
	 */
	@Override
	public String toString() {
		return super.toString().concat(" " + this.m_orderBy);
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ViewOrder x = (ViewOrder) super.clone();

		x.setOrderBy(this.getOrderBy());

		return x;
	}
}

