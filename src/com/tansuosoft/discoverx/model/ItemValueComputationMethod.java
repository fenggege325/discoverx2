/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 字段值计算公式计算类型枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum ItemValueComputationMethod implements EnumBase {
	/**
	 * 创建时计算（1）。
	 * 
	 * <p>
	 * 即计算公式在文档创建时计算一次，此类型的计算公式等同于缺省值公式（{@link Item#setDefaultValue(String)}）。
	 * </p>
	 */
	Create(1),
	/**
	 * 打开时计算（2）。
	 * 
	 * <p>
	 * 即计算公式在每次文档打开后计算一次。
	 * </p>
	 */
	Open(2),
	/**
	 * 保存时计算（4）。
	 * 
	 * <p>
	 * 即计算公式在每次文档保存前计算一次。
	 * </p>
	 */
	Save(4),
	/**
	 * 第一次保存时计算（8）。
	 * 
	 * <p>
	 * 即计算公式在文档第一次保存时计算一次。
	 * </p>
	 */
	FirstSave(8);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	ItemValueComputationMethod(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return ItemValueComputationMethod
	 */
	public ItemValueComputationMethod parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (ItemValueComputationMethod s : ItemValueComputationMethod.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return ItemValueComputationMethod.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return ItemValueComputationMethod
	 */
	public static ItemValueComputationMethod parse(int v) {
		for (ItemValueComputationMethod s : ItemValueComputationMethod.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

