/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.Config;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 系统全局默认文档流转时刻的{@link Tab}信息配置类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DocumentDefaultTabConfig extends Config {
	/**
	 * 系统默认的文档内容标签控件文件目录名：“defaultdocctrls”（在默认页面框架目录“frame”下）。
	 */
	public static final String DEFAULT_DOCUMENT_CONTROLS_DIR = "defaultdocctrls";

	/**
	 * 缺省构造器。
	 */
	private DocumentDefaultTabConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static DocumentDefaultTabConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return DocumentDefaultTabConfig
	 */
	public static DocumentDefaultTabConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new DocumentDefaultTabConfig();
			}
		}
		return m_instance;
	}

	private List<Tab> m_tabs = null; // 配置的Tab对象列表。
	private boolean systemDefaultPathAppended = false;

	/**
	 * 返回配置的Tab对象列表。
	 * 
	 * @return List<Tab>
	 */
	public List<Tab> getTabs() {
		if (!systemDefaultPathAppended) {
			if (this.m_tabs != null && this.m_tabs.size() > 0) {
				for (Tab t : this.m_tabs) {
					if (t == null || t.getControl() == null || t.getControl().length() == 0 || t.getControl().indexOf('/') > 0) continue;
					t.setControl(DEFAULT_DOCUMENT_CONTROLS_DIR + "/" + t.getControl());
				}
				systemDefaultPathAppended = true;
			}
		}
		return this.m_tabs;
	}

	/**
	 * 设置配置的Tab对象列表。
	 * 
	 * @param tabs List<Tab>
	 */
	public void setTabs(List<Tab> tabs) {
		systemDefaultPathAppended = false;
		this.m_tabs = tabs;
	}
}

