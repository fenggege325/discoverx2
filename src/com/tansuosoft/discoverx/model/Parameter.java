/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.model;

import java.math.BigDecimal;

import com.tansuosoft.discoverx.util.Instance;

/**
 * 用于描述资源额外属性参数的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Parameter implements Cloneable {
	/**
	 * 缺省构造器。
	 */
	public Parameter() {
	}

	/**
	 * 接收所属资源UNID的构造器。
	 * 
	 * @param punid
	 */
	public Parameter(String punid) {
		this.m_PUNID = punid;
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param punid
	 * @param name
	 * @param val
	 */
	public Parameter(String punid, String name, String val) {
		this(punid);
		this.m_name = name;
		this.m_valueString = val;
	}

	private String m_PUNID = null; // 所属资源UNID，必须。
	private ParameterValueDataType m_valueType = ParameterValueDataType.Text; // 参数值类型，默认为字符串。
	private boolean m_multiple = false; // 是否多值，默认为false。
	private String m_delimiter = ","; // 多值分隔符，默认为半角逗号（multiple必须为true才有意义，否则被忽略）。
	private String m_name = null;// 参数名，必须。
	private String m_valueString = null; // 参数值的字符串表现形式，可选。
	private String m_description = null; // 参数说明，可选。
	private boolean m_required = false; // 参数是否必须。
	private PersistenceState m_persistenceState = PersistenceState.Raw; // 持久化保存状态。

	/**
	 * 返回所属资源UNID，必须。
	 * 
	 * @return String
	 */
	public String getPUNID() {
		return this.m_PUNID;
	}

	/**
	 * 设置所属资源UNID，必须。
	 * 
	 * @param PUNID String
	 */
	public void setPUNID(String PUNID) {
		this.m_PUNID = PUNID;
	}

	/**
	 * 返回参数名。
	 * 
	 * @return String 返回参数名。
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置参数名。
	 * 
	 * @param String name 参数名。
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回参数值的字符串表现形式，可选。
	 * 
	 * @return String 参数值的字符串表现形式，可选。
	 */
	public String getValue() {
		return this.m_valueString;
	}

	/**
	 * 设置参数值的字符串表现形式，可选。
	 * 
	 * @param valueString String 参数值的字符串表现形式，可选。
	 */
	public void setValue(String valueString) {
		this.m_valueString = valueString;
	}

	/**
	 * 返回参数值的String类型结果。
	 * 
	 * @param defaultReturn 如果参数值为null时返回的默认结果。
	 * @return String
	 */
	public String getValueString(String defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueString(m_valueString, defaultReturn);
	}

	/**
	 * 返回参数值的int类型结果。
	 * 
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return int
	 */
	public int getValueInt(int defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueInt(this.m_valueString, defaultReturn);
	}

	/**
	 * 返回参数值的long类型结果。
	 * 
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return long
	 */
	public long getValueLong(long defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueLong(this.m_valueString, defaultReturn);
	}

	/**
	 * 返回参数值的float类型结果。
	 * 
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return float
	 */
	public float getValueFloat(float defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueFloat(this.m_valueString, defaultReturn);
	}

	/**
	 * 返回参数值的double类型结果。
	 * 
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return double
	 */
	public double getValueDouble(double defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueDouble(this.m_valueString, defaultReturn);
	}

	/**
	 * 返回参数值的boolean类型结果。
	 * 
	 * <p>
	 * 参数值（不区分大小写）配置为“true”、“yes”、“y”、“1”等情况时返回true，否则返回false。
	 * </p>
	 * 
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return boolean
	 */
	public boolean getValueBool(boolean defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueBool(this.m_valueString, defaultReturn);
	}

	/**
	 * 返回参数值转换为enumCls定义的枚举中某一个具体枚举值类型之后的结果。
	 * 
	 * @param &lt;T&gt; 返回的枚举类型。
	 * @param enumCls Class<?> Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return T 具体枚举结果。
	 */
	public <T> T getValueEnum(Class<T> enumCls, T defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueEnum(this.m_valueString, enumCls, defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值，此返回值已被转换为此参数的指定数据类型。
	 * 
	 * <p>
	 * 根据参数值数据类型不同返回如下结果：
	 * </p>
	 * <p>
	 * <ul>
	 * <li>文本类型：原始参数值。</li>
	 * <li>数字类型：返回原始参数值的BigDecimal表示方式。</li>
	 * <li>来自XML文本的对象类型：原始参数值作为xml文本内容并反序列化为相应对象后结果（xml内容必须符合系统xml反序列化标准）。</li>
	 * <li>对象类型：原始参数值作为全限定类名调用缺省构造器后返回的对象（必须有缺省构造器）。</li>
	 * <li>整数类型：返回原始参数值转换为int类型的结果。</li>
	 * <li>浮点类型：返回原始参数值转换为float类型的结果。</li>
	 * <li>双精度类型：返回原始参数值转换为double类型的结果。</li>
	 * <li>布尔类型：原始值作为布尔结果返回（参数值为“true/yes/y/1”中的某一个时返回true，其余情况返回false）。</li>
	 * <li>不特别说明的，返回字符串。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return 返回对应数据类型的参数值，如果参数原始值字符串为null或空串，则返回null。
	 */
	public Object getTypeValue() {
		if (this.m_valueString == null || this.m_valueString.length() == 0) return null;
		switch (this.m_valueType.getIntValue()) {
		case 1: // Text
			return this.m_valueString;
		case 2: // Number
			try {
				BigDecimal bd = new BigDecimal(this.m_valueString);
				return bd;
			} catch (NumberFormatException ex) {
				return 0;
			}
		case 3: // ObjectFromXML
			Object ret = null;
			com.tansuosoft.discoverx.util.serialization.Deserializer des = new com.tansuosoft.discoverx.util.serialization.XmlDeserializer();
			ret = des.deserialize(this.m_valueString, null);
			return ret;
		case 4: // Object
			return Instance.newInstance(this.m_valueString);
		case 5: // Int
			return this.getValueInt(0);
		case 6: // Float
			return this.getValueFloat(0.0f);
		case 7: // Double
			return this.getValueDouble(0.0d);
		case 8: // Boolean
			return this.getValueBool(false);
		default:
			return this.m_valueString;
		}
	}

	/**
	 * 返回参数值类型，默认为字符串。
	 * 
	 * <p>
	 * 参数类型一般是字符串、枚举（其配置值用EnumBase.getIntValue对应数字表示）和java基础类型对应的值。
	 * </p>
	 * 
	 * @return String 参数值类型，默认为字符串。
	 */
	public ParameterValueDataType getValueType() {
		return this.m_valueType;
	}

	/**
	 * 设置参数值类型，默认为字符串。
	 * 
	 * @param m_valueType ParameterValueDataType 参数值类型，默认为字符串。
	 */
	public void setValueType(ParameterValueDataType m_valueType) {
		this.m_valueType = m_valueType;
	}

	/**
	 * 返回是否多值，默认为false。
	 * 
	 * @return boolean 是否多值，默认为false。
	 */
	public boolean getMultiple() {
		return this.m_multiple;
	}

	/**
	 * 设置是否多值，默认为false。
	 * 
	 * @param isMultipleValue boolean 是否多值，默认为false。
	 */
	public void setMultiple(boolean isMultipleValue) {
		this.m_multiple = isMultipleValue;
	}

	/**
	 * 返回多值分隔符。
	 * 
	 * <p>
	 * {@link Parameter#getMultiple()}必须返回true才有意义，否则被忽略。
	 * </p>
	 * 
	 * @return String。
	 */
	public String getDelimiter() {
		return this.m_delimiter;
	}

	/**
	 * 设置多值分隔符。
	 * 
	 * @param delimiter String
	 */
	public void setDelimiter(String delimiter) {
		this.m_delimiter = delimiter;
	}

	/**
	 * 返回参数说明，可选。
	 * 
	 * @return String 参数说明，可选。
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置参数说明，可选。
	 * 
	 * @param description String 参数说明，可选。
	 */
	public void setDescription(String description) {
		this.m_description = description;
	}

	/**
	 * 返回参数是否必须。
	 * 
	 * <p>
	 * 默认为false（不是必须）。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getRequired() {
		return this.m_required;
	}

	/**
	 * 设置参数是否必须。
	 * 
	 * <p>
	 * 只在配置预设参数时有意义。
	 * </p>
	 * 
	 * @param required boolean
	 */
	public void setRequired(boolean required) {
		this.m_required = required;
	}

	/**
	 * 获取持久化保存状态，默认为{@link PersistenceState#Raw}
	 * 
	 * <p>
	 * 只在运行时有意义，主要用于控制参数序列化。
	 * </p>
	 * 
	 * @return PersistenceState
	 */
	public PersistenceState getPersistenceState() {
		return this.m_persistenceState;
	}

	/**
	 * 设置持久化保存状态。
	 * 
	 * @param persistenceState PersistenceState
	 */
	public void setPersistenceState(PersistenceState persistenceState) {
		this.m_persistenceState = persistenceState;
	}

	/**
	 * 重载hashCode
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_name == null) ? 0 : m_name.hashCode());
		return result;
	}

	/**
	 * 重载equals：参数名相同则相同。
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Parameter other = (Parameter) obj;
		if (m_name == null) {
			if (other.m_name != null) return false;
		} else if (!m_name.equalsIgnoreCase(other.m_name)) return false;
		return true;
	}

	/**
	 * 重载clone：返回一个克隆的对象。
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Parameter p = new Parameter();
		p.setDelimiter(this.getDelimiter());
		p.setDescription(this.getDescription());
		p.setMultiple(this.getMultiple());
		p.setName(this.getName());
		p.setPersistenceState(this.getPersistenceState());
		p.setPUNID(this.getDelimiter());
		p.setRequired(this.getRequired());
		p.setValue(this.getValue());
		p.setValueType(this.getValueType());
		return p;
	}
}

