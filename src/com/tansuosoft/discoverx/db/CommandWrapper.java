/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.db;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * 封装{@link PreparedStatement}的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class CommandWrapper {
	Connection connection = null; // 绑定的数据库连接。
	PreparedStatement preparedStatement = null; // 带参数的预编译SQL语句对象。
	String sqlTemplet = null; // 带参数的SQL语句的模板

	/**
	 * 缺省构造器。
	 */
	protected CommandWrapper() {
	}

	/**
	 * 接收必要参数的构造器
	 */
	protected CommandWrapper(PreparedStatement pstmt, String sqlTemplate) {
		this.preparedStatement = pstmt;
		try {
			this.connection = this.preparedStatement.getConnection();
		} catch (SQLException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
		this.sqlTemplet = sqlTemplate;
	}

	/**
	 * 获取执行结果ResultSet的DataReader封装。
	 * 
	 * <p>
	 * 如果此对象封装的PreparedStatement对象的getResultSet方法返回null，则返回null。
	 * </p>
	 * 
	 * @return DataReader
	 */
	public DataReader getDataReader() {
		DataReader dr = null;
		try {
			ResultSet rs = this.preparedStatement.getResultSet();
			dr = new DataReader(this.connection, this.preparedStatement, rs);
		} catch (SQLException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
		}
		return dr;
	}

	/**
	 * 返回绑定的数据库连接。
	 * 
	 * @return Connection
	 */
	public Connection getConnection() {
		return this.connection;
	}

	/**
	 * 返回带参数的预编译SQL语句对象。
	 * 
	 * @return PreparedStatement
	 */
	public PreparedStatement getPreparedStatement() {
		return this.preparedStatement;
	}

	/**
	 * 返回带参数的SQL语句的模板。
	 * 
	 * @return String
	 */
	public String getSqlTemplet() {
		return this.sqlTemplet;
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#addBatch()
	 */
	public void addBatch() throws SQLException {
		preparedStatement.addBatch();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @throws SQLException
	 * @see java.sql.Statement#cancel()
	 */
	public void cancel() throws SQLException {
		preparedStatement.cancel();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @throws SQLException
	 * @see java.sql.Statement#clearBatch()
	 */
	public void clearBatch() throws SQLException {
		preparedStatement.clearBatch();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#clearParameters()
	 */
	public void clearParameters() throws SQLException {
		preparedStatement.clearParameters();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @throws SQLException
	 * @see java.sql.Statement#clearWarnings()
	 */
	public void clearWarnings() throws SQLException {
		preparedStatement.clearWarnings();
	}

	/**
	 * 关闭绑定的PreparedStatement。
	 * 
	 * @throws SQLException
	 * @see java.sql.Statement#close()
	 */
	public void close() {
		try {
			preparedStatement.close();
			preparedStatement = null;
			connection = null;
		} catch (SQLException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
		}

	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return boolean
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#execute()
	 */
	public boolean execute() throws SQLException {
		return preparedStatement.execute();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.Statement#executeBatch()
	 */
	public int[] executeBatch() throws SQLException {
		return preparedStatement.executeBatch();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return ResultSet
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#executeQuery()
	 */
	public ResultSet executeQuery() throws SQLException {
		return preparedStatement.executeQuery();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#executeUpdate()
	 */
	public int executeUpdate() throws SQLException {
		return preparedStatement.executeUpdate();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.Statement#getFetchDirection()
	 */
	public int getFetchDirection() throws SQLException {
		return preparedStatement.getFetchDirection();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return SQLException
	 * @throws SQLException
	 * @see java.sql.Statement#getFetchSize()
	 */
	public int getFetchSize() throws SQLException {
		return preparedStatement.getFetchSize();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return ResultSet
	 * @throws SQLException
	 * @see java.sql.Statement#getGeneratedKeys()
	 */
	public ResultSet getGeneratedKeys() throws SQLException {
		return preparedStatement.getGeneratedKeys();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return int
	 * @throws SQLException
	 * @see java.sql.Statement#getMaxFieldSize()
	 */
	public int getMaxFieldSize() throws SQLException {
		return preparedStatement.getMaxFieldSize();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return int
	 * @throws SQLException
	 * @see java.sql.Statement#getMaxRows()
	 */
	public int getMaxRows() throws SQLException {
		return preparedStatement.getMaxRows();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#getMetaData()
	 */
	public ResultSetMetaData getMetaData() throws SQLException {
		return preparedStatement.getMetaData();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return boolean
	 * @throws SQLException
	 * @see java.sql.Statement#getMoreResults()
	 */
	public boolean getMoreResults() throws SQLException {
		return preparedStatement.getMoreResults();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param current
	 * @return boolean
	 * @throws SQLException
	 * @see java.sql.Statement#getMoreResults(int)
	 */
	public boolean getMoreResults(int current) throws SQLException {
		return preparedStatement.getMoreResults(current);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return ParameterMetaData
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#getParameterMetaData()
	 */
	public ParameterMetaData getParameterMetaData() throws SQLException {
		return preparedStatement.getParameterMetaData();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return int
	 * @throws SQLException
	 * @see java.sql.Statement#getQueryTimeout()
	 */
	public int getQueryTimeout() throws SQLException {
		return preparedStatement.getQueryTimeout();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return ResultSet
	 * @throws SQLException
	 * @see java.sql.Statement#getResultSet()
	 */
	public ResultSet getResultSet() throws SQLException {
		return preparedStatement.getResultSet();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return int
	 * @throws SQLException
	 * @see java.sql.Statement#getResultSetConcurrency()
	 */
	public int getResultSetConcurrency() throws SQLException {
		return preparedStatement.getResultSetConcurrency();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return int
	 * @throws SQLException
	 * @see java.sql.Statement#getResultSetHoldability()
	 */
	public int getResultSetHoldability() throws SQLException {
		return preparedStatement.getResultSetHoldability();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return int
	 * @throws SQLException
	 * @see java.sql.Statement#getResultSetType()
	 */
	public int getResultSetType() throws SQLException {
		return preparedStatement.getResultSetType();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return int
	 * @throws SQLException
	 * @see java.sql.Statement#getUpdateCount()
	 */
	public int getUpdateCount() throws SQLException {
		return preparedStatement.getUpdateCount();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @return SQLWarning
	 * @throws SQLException
	 * @see java.sql.Statement#getWarnings()
	 */
	public SQLWarning getWarnings() throws SQLException {
		return preparedStatement.getWarnings();
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setArray(int, java.sql.Array)
	 */
	public void setArray(int parameterIndex, Array x) throws SQLException {
		preparedStatement.setArray(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @param length
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setAsciiStream(int, java.io.InputStream, int)
	 */
	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
		preparedStatement.setAsciiStream(parameterIndex, x, length);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBigDecimal(int, java.math.BigDecimal)
	 */
	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		preparedStatement.setBigDecimal(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @param length
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream, int)
	 */
	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
		preparedStatement.setBinaryStream(parameterIndex, x, length);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBlob(int, java.sql.Blob)
	 */
	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		preparedStatement.setBlob(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBoolean(int, boolean)
	 */
	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		preparedStatement.setBoolean(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setByte(int, byte)
	 */
	public void setByte(int parameterIndex, byte x) throws SQLException {
		preparedStatement.setByte(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setBytes(int, byte[])
	 */
	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		preparedStatement.setBytes(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param reader
	 * @param length
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setCharacterStream(int, java.io.Reader, int)
	 */
	public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
		preparedStatement.setCharacterStream(parameterIndex, reader, length);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setClob(int, java.sql.Clob)
	 */
	public void setClob(int parameterIndex, Clob x) throws SQLException {
		preparedStatement.setClob(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param name
	 * @throws SQLException
	 * @see java.sql.Statement#setCursorName(java.lang.String)
	 */
	public void setCursorName(String name) throws SQLException {
		preparedStatement.setCursorName(name);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @param cal
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setDate(int, java.sql.Date, java.util.Calendar)
	 */
	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
		preparedStatement.setDate(parameterIndex, x, cal);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setDate(int, java.sql.Date)
	 */
	public void setDate(int parameterIndex, Date x) throws SQLException {
		preparedStatement.setDate(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setDouble(int, double)
	 */
	public void setDouble(int parameterIndex, double x) throws SQLException {
		preparedStatement.setDouble(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param enable
	 * @throws SQLException
	 * @see java.sql.Statement#setEscapeProcessing(boolean)
	 */
	public void setEscapeProcessing(boolean enable) throws SQLException {
		preparedStatement.setEscapeProcessing(enable);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param direction
	 * @throws SQLException
	 * @see java.sql.Statement#setFetchDirection(int)
	 */
	public void setFetchDirection(int direction) throws SQLException {
		preparedStatement.setFetchDirection(direction);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param rows
	 * @throws SQLException
	 * @see java.sql.Statement#setFetchSize(int)
	 */
	public void setFetchSize(int rows) throws SQLException {
		preparedStatement.setFetchSize(rows);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setFloat(int, float)
	 */
	public void setFloat(int parameterIndex, float x) throws SQLException {
		preparedStatement.setFloat(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setInt(int, int)
	 */
	public void setInt(int parameterIndex, int x) throws SQLException {
		preparedStatement.setInt(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setLong(int, long)
	 */
	public void setLong(int parameterIndex, long x) throws SQLException {
		preparedStatement.setLong(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param max
	 * @throws SQLException
	 * @see java.sql.Statement#setMaxFieldSize(int)
	 */
	public void setMaxFieldSize(int max) throws SQLException {
		preparedStatement.setMaxFieldSize(max);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param max
	 * @throws SQLException
	 * @see java.sql.Statement#setMaxRows(int)
	 */
	public void setMaxRows(int max) throws SQLException {
		preparedStatement.setMaxRows(max);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param sqlType
	 * @param typeName
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNull(int, int, java.lang.String)
	 */
	public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
		preparedStatement.setNull(parameterIndex, sqlType, typeName);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param sqlType
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setNull(int, int)
	 */
	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		preparedStatement.setNull(parameterIndex, sqlType);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @param targetSqlType
	 * @param scaleOrLength
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int, int)
	 */
	public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
		preparedStatement.setObject(parameterIndex, x, targetSqlType, scaleOrLength);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @param targetSqlType
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int)
	 */
	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
		preparedStatement.setObject(parameterIndex, x, targetSqlType);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object)
	 */
	public void setObject(int parameterIndex, Object x) throws SQLException {
		preparedStatement.setObject(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param seconds
	 * @throws SQLException
	 * @see java.sql.Statement#setQueryTimeout(int)
	 */
	public void setQueryTimeout(int seconds) throws SQLException {
		preparedStatement.setQueryTimeout(seconds);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setRef(int, java.sql.Ref)
	 */
	public void setRef(int parameterIndex, Ref x) throws SQLException {
		preparedStatement.setRef(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setShort(int, short)
	 */
	public void setShort(int parameterIndex, short x) throws SQLException {
		preparedStatement.setShort(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setString(int, java.lang.String)
	 */
	public void setString(int parameterIndex, String x) throws SQLException {
		preparedStatement.setString(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @param cal
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTime(int, java.sql.Time, java.util.Calendar)
	 */
	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
		preparedStatement.setTime(parameterIndex, x, cal);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTime(int, java.sql.Time)
	 */
	public void setTime(int parameterIndex, Time x) throws SQLException {
		preparedStatement.setTime(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @param cal
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp, java.util.Calendar)
	 */
	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
		preparedStatement.setTimestamp(parameterIndex, x, cal);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp)
	 */
	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		preparedStatement.setTimestamp(parameterIndex, x);
	}

	/**
	 * 对应java.sql.PreparedStatement的同名委托方法。
	 * 
	 * @param parameterIndex
	 * @param x
	 * @throws SQLException
	 * @see java.sql.PreparedStatement#setURL(int, java.net.URL)
	 */
	public void setURL(int parameterIndex, URL x) throws SQLException {
		preparedStatement.setURL(parameterIndex, x);
	}

}

