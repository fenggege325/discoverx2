/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 提供数据库访问对象的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Database {
	protected DataSource ds = null;
	protected boolean autoCommit = true; // 是否自动提交
	protected boolean autoClose = true; // 是否自动关闭数据库连接。
	protected int resultSetType = ResultSet.TYPE_FORWARD_ONLY; // 结果集游标方向，默认为“ResultSet.TYPE_FORWARD_ONLY”。
	protected int resultSetConcurrency = ResultSet.CONCUR_READ_ONLY; // 结果集并发选项，默认为“ResultSet.CONCUR_READ_ONLY”。
	protected int resultSetHoldability = -1; // 结果集保留选项，默认为“-1”（表示使用jdbc驱动程序默认）。

	/**
	 * 默认构造器
	 */
	protected Database() {
	}

	/**
	 * 接收JDBC数据源对象的构造器。
	 * 
	 * @param ds
	 */
	protected Database(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * 检查ResultSet的resultSetType、resultSetConcurrency、resultSetHoldability是否使用默认值。
	 * 
	 * <p>
	 * <strong>如果设置，必须同时设置resultSetType、resultSetConcurrency、
	 * resultSetHoldability的有效值！</strong>
	 * </p>
	 * 
	 * @return boolean
	 *         如果resultSetType、resultSetConcurrency、resultSetHoldability没有变动
	 *         ，则使用jdbc默认值。
	 */
	protected boolean checkRSOptionChange() {
		return (resultSetType == ResultSet.TYPE_FORWARD_ONLY && resultSetConcurrency == ResultSet.CONCUR_READ_ONLY && resultSetHoldability == -1);
	}

	/**
	 * 返回是否自动提交，默认为true。
	 * 
	 * @return boolean
	 */
	public boolean getAutoCommit() {
		return this.autoCommit;
	}

	/**
	 * 设置是否自动提交。
	 * 
	 * @param autoCommit
	 *          boolean
	 */
	public void setAutoCommit(boolean autoCommit) {
		this.autoCommit = autoCommit;
	}

	/**
	 * 返回是否自动关闭数据库连接，默认为true。
	 * 
	 * <p>
	 * 如果设置为true，则{@link #executeScalar(String)}、{@link #executeNonQuery(String)}
	 * 执行后会自动关闭数据库连接。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getAutoClose() {
		return this.autoClose;
	}

	/**
	 * 设置是否自动关闭数据库连接。
	 * 
	 * @param autoClose
	 *          boolean
	 */
	public void setAutoClose(boolean autoClose) {
		this.autoClose = autoClose;
	}

	private Connection m_connection = null;

	/**
	 * 返回一个可用的数据库连接对象。
	 * 
	 * <p>
	 * 注意：使用完毕连接后要明确地调用close()关闭连接，否则数据库连接资源可能被消耗光从而导致系统异常！
	 * </p>
	 * 
	 * @return Connection
	 */
	public Connection getConnection() throws SQLException {
		try {
			if (this.m_connection == null || this.m_connection.isClosed()) {
				this.m_connection = (ds == null ? null : ds.getConnection());
			}
			if (this.m_connection != null) this.m_connection.setAutoCommit(this.autoCommit);
		} catch (SQLException ex) {
			throw ex;
		}
		return this.m_connection;
	}

	/**
	 * 关闭此数据库包含的活动数据库连接。
	 */
	public void close() {
		if (this.m_connection == null) return;
		try {
			if (this.m_connection.isClosed()) {
				this.m_connection = null;
				return;
			}
			this.m_connection.close();
			this.m_connection = null;
		} catch (SQLException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
		}
	}

	/**
	 * 执行sql查询语句并返回结果集({@link DataReader})。
	 * 
	 * <p>
	 * 请确保返回的{@link DataReader}使用完毕后调用{@link DataReader#close()}以释放
	 * {@link java.sql.ResultSet}资源!<br/>
	 * 同时请确保调用{@link #close()}关闭数据库连接!
	 * </p>
	 * 
	 * @param sql
	 *          String 要查询的sql语句。
	 * @return DataReader
	 * @throws SQLException
	 */
	public DataReader executeQuery(String sql) throws SQLException {
		DataReader dr = null;
		try {
			Connection con = this.getConnection();
			Statement stmt = (this.checkRSOptionChange() ? con.createStatement() : con.createStatement(this.resultSetType, this.resultSetConcurrency, this.resultSetHoldability));
			ResultSet rs = stmt.executeQuery(sql);
			dr = new DataReader(con, stmt, rs);
		} catch (SQLException ex) {
			FileLogger.debug("执行SQL语句时出现异常：%1$s，原因：%2$s", sql, ex.getMessage());
			throw ex;
		}
		return dr;
	}

	/**
	 * 执行sql查询语句并返回结果集中的第一行第一列的标量值。
	 * 
	 * <p>
	 * <strong>如果{@link #getAutoClose()}返回false，需手工关闭数据库连接！</strong>
	 * </p>
	 * 
	 * @param sql
	 *          String 要执行的sql语句。
	 * @return Object 返回JDBC规范中SQL数据类型对应的java对象，如果查询返回空列值，则返回null。
	 * @throws SQLException
	 */
	public Object executeScalar(String sql) throws SQLException {
		Object obj = null;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = this.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				obj = rs.getObject(1);
				break;
			}
		} catch (SQLException ex) {
			FileLogger.debug("执行SQL语句时出现异常：%1$s，原因：%2$s", sql, ex.getMessage());
			throw ex;
		} finally {
			if (stmt != null) stmt.close();
			if (this.getAutoClose()) {
				if (con != null) con.close();
			}
		}
		return obj;
	}

	/**
	 * 执行非查询语句（更新、插入、删除等）。
	 * 
	 * <p>
	 * <strong>如果{@link #getAutoClose()}返回false，需手工关闭数据库连接！</strong>
	 * </p>
	 * 
	 * @param sql
	 * @return int 返回受语句影响的行数，0表示没有更改任何行。
	 * @throws SQLException
	 */
	public int executeNonQuery(String sql) throws SQLException {
		int ret = 0;
		Connection con = null;
		Statement stmt = null;
		try {
			con = this.getConnection();
			stmt = (this.checkRSOptionChange() ? con.createStatement() : con.createStatement(this.resultSetType, this.resultSetConcurrency, this.resultSetHoldability));
			ret = stmt.executeUpdate(sql);
		} catch (SQLException ex) {
			FileLogger.debug("执行SQL语句时出现异常：%1$s，原因：%2$s", sql, ex.getMessage());
			throw ex;
		} finally {
			if (stmt != null) stmt.close();
			if (this.getAutoClose()) {
				if (con != null) con.close();
			}
		}
		return ret;
	}

	/**
	 * 批量执行多条SQL语句并返回每一条SQL语句对应的处理结果影响的数据行数据数组。
	 * 
	 * <p>
	 * <strong>如果{@link #getAutoClose()}返回false，需手工关闭数据库连接！</strong>
	 * </p>
	 * 
	 * @param sqls
	 *          String数组，表示要执行的多条SQL语句
	 * @return
	 * @throws SQLException
	 */
	public int[] executeBatch(String[] sqls) throws SQLException {
		if (sqls == null || sqls.length == 0) return null;
		int ret[] = { 0 };
		Connection con = null;
		Statement stmt = null;
		try {
			con = this.getConnection();
			stmt = con.createStatement();
			for (String s : sqls) {
				stmt.addBatch(s);
			}
			ret = stmt.executeBatch();
		} catch (SQLException ex) {
			StringBuilder sb = new StringBuilder();
			for (String s : sqls) {
				sb.append(s).append("\r\n");
			}
			FileLogger.debug("执行SQL语句时出现异常：%1$s，原因：%2$s", sb.toString(), ex.getMessage());
			throw ex;
		} finally {
			if (stmt != null) stmt.close();
			if (this.getAutoClose()) {
				if (con != null) con.close();
			}
		}
		return ret;
	}

	/**
	 * 创建一个新的{@link CommandWrapper}对象并返回之。
	 * 
	 * <p>
	 * 返回的{@link CommandWrapper}使用完毕后，要确保调用{@link CommandWrapper#close()}以关闭连接。
	 * </p>
	 * 
	 * @param sqlTemplet
	 *          String-表示带参数的SQL语句的模板。
	 * @return
	 */
	public CommandWrapper createCommandWrapper(String sqlTemplet) {
		if (sqlTemplet == null || sqlTemplet.length() == 0) return null;
		CommandWrapper cw = null;
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = this.getConnection();
			pstmt = (this.checkRSOptionChange() ? con.prepareStatement(sqlTemplet) : con.prepareStatement(sqlTemplet, this.resultSetType, this.resultSetConcurrency, this.resultSetHoldability));
			cw = new CommandWrapper(pstmt, sqlTemplet);
		} catch (SQLException ex) {
			FileLogger.debug("创建CommandWrapper对象时出现异常：%1$s，原因：%2$s", sqlTemplet, ex.getMessage());
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
			try {
				if (this.getAutoClose()) {
					if (con != null) con.close();
				}
			} catch (SQLException e) {
			}
		}

		return cw;
	}

}// class end

