/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.db;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

import com.tansuosoft.discoverx.util.EnumBase;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 封装{@link ResultSet}的类（对查询结果集类的封装）。
 * 
 * @author coca@tansuosoft.cn
 */
public class DataReader {

	private Connection connection = null;
	private Statement statement = null;
	private ResultSet resultSet = null;

	/**
	 * 缺省构造器
	 */
	protected DataReader() {
	}

	/**
	 * 接收数据库连接、语句、结果集等对象的构造器
	 * 
	 * @param connection
	 * @param statement
	 * @param resultSet
	 */
	protected DataReader(Connection connection, Statement statement, ResultSet resultSet) {
		super();
		this.connection = connection;
		this.statement = statement;
		this.resultSet = resultSet;
	}

	/**
	 * 获取绑定的数据库连接。
	 * 
	 * @return java.sql.Connection。
	 */
	public Connection getConnection() {
		return this.connection;
	}

	/**
	 * 获取绑定的Statement对象。
	 * 
	 * @return java.sql.Statement。
	 */
	public Statement getStatement() {
		return this.statement;
	}

	/**
	 * 获取绑定的数据库结果集。
	 * 
	 * @return java.sql.ResultSet。
	 */
	public ResultSet getResultSet() {
		return this.resultSet;
	}

	/**
	 * 移到resultset的下一条有效数据。
	 * 
	 * @return boolean
	 * @throws SQLException
	 */
	public boolean next() throws SQLException {
		if (this.resultSet == null) return false;
		try {
			return this.resultSet.next();
		} catch (SQLException ex) {
			throw ex;
		}
	}

	/**
	 * 关闭结果集，必须明确调用此方法以关闭ResultSet。
	 * 
	 * <p>
	 * <strong>关闭后即不能再次访问ResultSet有关任何成员，否则可能导致异常。</strong>
	 * </p>
	 */
	public void close() {
		try {
			if (this.resultSet != null) this.resultSet.close();
			if (this.statement != null) this.statement.close();
			this.resultSet = null;
			this.statement = null;
			this.connection = null;
		} catch (SQLException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
		}
	}

	/**
	 * 返回当前数据行第columnIndex列的整数值对应的枚举值。
	 * 
	 * @param columnIndex
	 * @param base 指定要返回的枚举值对象。
	 * @return
	 * @throws SQLException EnumBase
	 */
	public EnumBase getEnum(int columnIndex, EnumBase base) throws SQLException {
		int i = this.getInt(columnIndex);
		return (EnumBase) base.parse(i + "");
	}

	/**
	 * 返回当前数据行columnLabel名称指定的列的整数值对应的枚举值。
	 * 
	 * @param columnLabel
	 * @param base 指定要返回的枚举值对象。
	 * @return
	 * @throws SQLException EnumBase
	 */
	public EnumBase getEnum(String columnLabel, EnumBase base) throws SQLException {
		int i = this.getInt(columnLabel);
		return (EnumBase) base.parse(i + "");
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getArray(int)
	 */
	public Array getArray(int columnIndex) throws SQLException {
		return resultSet.getArray(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getArray(java.lang.String)
	 */
	public Array getArray(String columnLabel) throws SQLException {
		return resultSet.getArray(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getAsciiStream(int)
	 */
	public InputStream getAsciiStream(int columnIndex) throws SQLException {
		return resultSet.getAsciiStream(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getAsciiStream(java.lang.String)
	 */
	public InputStream getAsciiStream(String columnLabel) throws SQLException {
		return resultSet.getAsciiStream(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBigDecimal(int)
	 */
	public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
		return resultSet.getBigDecimal(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBigDecimal(java.lang.String)
	 */
	public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
		return resultSet.getBigDecimal(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBinaryStream(int)
	 */
	public InputStream getBinaryStream(int columnIndex) throws SQLException {
		return resultSet.getBinaryStream(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBinaryStream(java.lang.String)
	 */
	public InputStream getBinaryStream(String columnLabel) throws SQLException {
		return resultSet.getBinaryStream(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBlob(int)
	 */
	public Blob getBlob(int columnIndex) throws SQLException {
		return resultSet.getBlob(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBlob(java.lang.String)
	 */
	public Blob getBlob(String columnLabel) throws SQLException {
		return resultSet.getBlob(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBoolean(int)
	 */
	public boolean getBoolean(int columnIndex) throws SQLException {
		return resultSet.getBoolean(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBoolean(java.lang.String)
	 */
	public boolean getBoolean(String columnLabel) throws SQLException {
		return resultSet.getBoolean(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getByte(int)
	 */
	public byte getByte(int columnIndex) throws SQLException {
		return resultSet.getByte(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getByte(java.lang.String)
	 */
	public byte getByte(String columnLabel) throws SQLException {
		return resultSet.getByte(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBytes(int)
	 */
	public byte[] getBytes(int columnIndex) throws SQLException {
		return resultSet.getBytes(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getBytes(java.lang.String)
	 */
	public byte[] getBytes(String columnLabel) throws SQLException {
		return resultSet.getBytes(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getCharacterStream(int)
	 */
	public Reader getCharacterStream(int columnIndex) throws SQLException {
		return resultSet.getCharacterStream(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getCharacterStream(java.lang.String)
	 */
	public Reader getCharacterStream(String columnLabel) throws SQLException {
		return resultSet.getCharacterStream(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getClob(int)
	 */
	public Clob getClob(int columnIndex) throws SQLException {
		return resultSet.getClob(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getClob(java.lang.String)
	 */
	public Clob getClob(String columnLabel) throws SQLException {
		return resultSet.getClob(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getConcurrency()
	 */
	public int getConcurrency() throws SQLException {
		return resultSet.getConcurrency();
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getCursorName()
	 */
	public String getCursorName() throws SQLException {
		return resultSet.getCursorName();
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @param cal
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getDate(int, java.util.Calendar)
	 */
	public Date getDate(int columnIndex, Calendar cal) throws SQLException {
		return resultSet.getDate(columnIndex, cal);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getDate(int)
	 */
	public Date getDate(int columnIndex) throws SQLException {
		return resultSet.getDate(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @param cal
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getDate(java.lang.String, java.util.Calendar)
	 */
	public Date getDate(String columnLabel, Calendar cal) throws SQLException {
		return resultSet.getDate(columnLabel, cal);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getDate(java.lang.String)
	 */
	public Date getDate(String columnLabel) throws SQLException {
		return resultSet.getDate(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getDouble(int)
	 */
	public double getDouble(int columnIndex) throws SQLException {
		return resultSet.getDouble(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getDouble(java.lang.String)
	 */
	public double getDouble(String columnLabel) throws SQLException {
		return resultSet.getDouble(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getFetchDirection()
	 */
	public int getFetchDirection() throws SQLException {
		return resultSet.getFetchDirection();
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getFetchSize()
	 */
	public int getFetchSize() throws SQLException {
		return resultSet.getFetchSize();
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getFloat(int)
	 */
	public float getFloat(int columnIndex) throws SQLException {
		return resultSet.getFloat(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getFloat(java.lang.String)
	 */
	public float getFloat(String columnLabel) throws SQLException {
		return resultSet.getFloat(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getInt(int)
	 */
	public int getInt(int columnIndex) throws SQLException {
		return resultSet.getInt(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getInt(java.lang.String)
	 */
	public int getInt(String columnLabel) throws SQLException {
		return resultSet.getInt(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getLong(int)
	 */
	public long getLong(int columnIndex) throws SQLException {
		return resultSet.getLong(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getLong(java.lang.String)
	 */
	public long getLong(String columnLabel) throws SQLException {
		return resultSet.getLong(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getMetaData()
	 */
	public ResultSetMetaData getMetaData() throws SQLException {
		return resultSet.getMetaData();
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @param map
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getObject(int, java.util.Map)
	 */
	public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException {
		return resultSet.getObject(columnIndex, map);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getObject(int)
	 */
	public Object getObject(int columnIndex) throws SQLException {
		return resultSet.getObject(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @param map
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getObject(java.lang.String, java.util.Map)
	 */
	public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException {
		return resultSet.getObject(columnLabel, map);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getObject(java.lang.String)
	 */
	public Object getObject(String columnLabel) throws SQLException {
		return resultSet.getObject(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getRef(int)
	 */
	public Ref getRef(int columnIndex) throws SQLException {
		return resultSet.getRef(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getRef(java.lang.String)
	 */
	public Ref getRef(String columnLabel) throws SQLException {
		return resultSet.getRef(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getRow()
	 */
	public int getRow() throws SQLException {
		return resultSet.getRow();
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getShort(int)
	 */
	public short getShort(int columnIndex) throws SQLException {
		return resultSet.getShort(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getShort(java.lang.String)
	 */
	public short getShort(String columnLabel) throws SQLException {
		return resultSet.getShort(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getString(int)
	 */
	public String getString(int columnIndex) throws SQLException {
		return resultSet.getString(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getString(java.lang.String)
	 */
	public String getString(String columnLabel) throws SQLException {
		return resultSet.getString(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @param cal
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getTime(int, java.util.Calendar)
	 */
	public Time getTime(int columnIndex, Calendar cal) throws SQLException {
		return resultSet.getTime(columnIndex, cal);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getTime(int)
	 */
	public Time getTime(int columnIndex) throws SQLException {
		return resultSet.getTime(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @param cal
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getTime(java.lang.String, java.util.Calendar)
	 */
	public Time getTime(String columnLabel, Calendar cal) throws SQLException {
		return resultSet.getTime(columnLabel, cal);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getTime(java.lang.String)
	 */
	public Time getTime(String columnLabel) throws SQLException {
		return resultSet.getTime(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @param cal
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getTimestamp(int, java.util.Calendar)
	 */
	public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException {
		return resultSet.getTimestamp(columnIndex, cal);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getTimestamp(int)
	 */
	public Timestamp getTimestamp(int columnIndex) throws SQLException {
		return resultSet.getTimestamp(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @param cal
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getTimestamp(java.lang.String, java.util.Calendar)
	 */
	public Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException {
		return resultSet.getTimestamp(columnLabel, cal);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getTimestamp(java.lang.String)
	 */
	public Timestamp getTimestamp(String columnLabel) throws SQLException {
		return resultSet.getTimestamp(columnLabel);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getType()
	 */
	public int getType() throws SQLException {
		return resultSet.getType();
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getURL(int)
	 */
	public URL getURL(int columnIndex) throws SQLException {
		return resultSet.getURL(columnIndex);
	}

	/**
	 * 对应java.sql.ResultSet的同名委托方法。
	 * 
	 * @param columnLabel
	 * @return
	 * @throws SQLException
	 * @see java.sql.ResultSet#getURL(java.lang.String)
	 */
	public URL getURL(String columnLabel) throws SQLException {
		return resultSet.getURL(columnLabel);
	}

	/**
	 * 返回指定列序号的结果，如果出现异常则返回defaultReturn。
	 * 
	 * @param columnIndex 列序号。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return String
	 */
	public String getString(int columnIndex, String defaultReturn) {
		Object o = null;
		try {
			o = resultSet.getObject(columnIndex);
		} catch (SQLException e) {
			FileLogger.error(e);
		}
		if (o == null) return defaultReturn;
		return o.toString();
	}

	/**
	 * 返回指定列序号的结果，如果出现异常则返回defaultReturn。
	 * 
	 * @param columnIndex 列序号。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return int
	 */
	public int getInt(int columnIndex, int defaultReturn) {
		String str = this.getString(columnIndex, null);
		return StringUtil.getValueInt(str, defaultReturn);
	}

	/**
	 * 返回指定列序号的结果，如果出现异常则返回defaultReturn。
	 * 
	 * @param columnIndex 列序号。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return long
	 */
	public long getLong(int columnIndex, long defaultReturn) {
		String str = this.getString(columnIndex, null);
		return StringUtil.getValueLong(str, defaultReturn);
	}

	/**
	 * 返回指定列序号的结果，如果出现异常则返回defaultReturn。
	 * 
	 * @param columnIndex 列序号。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return float
	 */
	public float getFloat(int columnIndex, float defaultReturn) {
		String str = this.getString(columnIndex, null);
		return StringUtil.getValueFloat(str, defaultReturn);
	}

	/**
	 * 返回指定列序号的结果，如果出现异常则返回defaultReturn。
	 * 
	 * @param columnIndex 列序号。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return double
	 */
	public double getDouble(int columnIndex, double defaultReturn) {
		String str = this.getString(columnIndex, null);
		return StringUtil.getValueDouble(str, defaultReturn);
	}

	/**
	 * 返回指定列序号的结果，如果出现异常则返回defaultReturn。
	 * <p>
	 * 参数值（不区分大小写）配置为“true”、“yes”、“y”、“1”等情况时返回true，否则返回false。
	 * </p>
	 * 
	 * @param columnIndex 列序号。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return boolean
	 */
	public boolean getBool(int columnIndex, boolean defaultReturn) {
		String str = this.getString(columnIndex, null);
		return StringUtil.getValueBool(str, defaultReturn);
	}

	/**
	 * 返回指定列序号对应的结果转换为enumCls定义的枚举中某一个具体枚举值类型之后的结果。
	 * 
	 * @param &lt;T&gt; 返回的枚举类型。
	 * @param columnIndex
	 * @param enumCls Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return T 具体枚举结果。
	 */
	public <T> T getEnumGeneric(int columnIndex, Class<T> enumCls, T defaultReturn) {
		String str = this.getString(columnIndex, null);
		return StringUtil.getValueEnum(str, enumCls, defaultReturn);
	}
}// class end

