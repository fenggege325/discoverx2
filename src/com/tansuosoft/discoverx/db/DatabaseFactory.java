/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * 提供数据库访问对象的工厂类
 * 
 * @author coca@tansuosoft.cn
 */
public class DatabaseFactory {
	/**
	 * Web应用程序环境变量名称（“defaultDatabase”），通过这个环境变量获取应用程序默认使用的数据库连接池资源的资源引用名称，然后通过这个资源引用名称来获取数据源对象。
	 */
	protected static String DEFAULT_DATABASE_ENV_ENTRY_NAME = "defaultDatabase";

	/**
	 * 私有构造器
	 */
	private DatabaseFactory() {
	}

	/**
	 * 获取系统默认使用的数据库访问对象。 <para>必须在系统中配置相应的数据库连接池资源及其对应的资源引用。</para> <para>同时必须在应用程序的web .xml中配置名为“defaultDatabase”的Web应用程序环境变量并指定其值为默认的数据源资源名称。</para>
	 * 
	 * @return Database 返回获取到的数据库访问对象
	 */
	public static Database getDatabase() {
		Database db = null;
		try {
			Context env = (Context) new InitialContext().lookup("java:comp/env");
			String name = (String) env.lookup(DatabaseFactory.DEFAULT_DATABASE_ENV_ENTRY_NAME);
			db = getDatabase(name);
		} catch (NamingException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
		}

		return db;
	}

	/**
	 * 获取指定的资源名称对应的数据库访问对象。 <para>必须在系统中配置相应的数据库连接池资源及其对应的资源引用。</para> 在应用程序的web.xml中通过名为“defaultDatabase”的Web应用环境变量节点指定。
	 * 
	 * @param name 数据库资源名称
	 * @return Database 返回获取到的数据库访问对象
	 * @throws NamingException
	 */
	public static Database getDatabase(String name) throws NamingException {
		Database db = null;
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup(name);
			db = new Database(ds);
		} catch (NamingException ex) {
			throw ex;
		}
		return db;
	}

	/**
	 * 当前连接的数据库用户。
	 */
	public static String DATABASE_USERNAME = null;
	static {
		Database db = getDatabase();
		Connection con = null;
		try {
			con = db.getConnection();
			if (con != null) {
				DatabaseMetaData dmd = con.getMetaData();
				if (dmd != null) {
					DATABASE_USERNAME = dmd.getUserName();
				}
			}
		} catch (SQLException ex) {

		} finally {
			if (con != null) try {
				con.close();
			} catch (SQLException e) {
			}
		}
	};

}// class end

