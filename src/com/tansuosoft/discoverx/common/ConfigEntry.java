/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import com.tansuosoft.discoverx.util.ObjectUtil;

/**
 * 用于描述某一个配置项的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ConfigEntry {
	private String m_name = null; // 参数名称，必须。
	private String m_value = null; // 参数值，可选。
	private String m_type = null; // 参数值类型，可选，默认为字符串。
	private String m_description = null; // 参数说明，可选。

	/**
	 * 缺省构造器。
	 */
	public ConfigEntry() {
	}

	/**
	 * 返回参数名称，必须。
	 * 
	 * @return String 。
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置参数名称，必须。
	 * 
	 * @param m_name String。
	 */
	public void setName(String m_name) {
		this.m_name = m_name;
	}

	/**
	 * 返回参数值，可选。
	 * 
	 * @return String 。
	 */
	public String getValue() {
		return this.m_value;
	}

	/**
	 * 返回转换为getType()所指定的数据类型的参数值。
	 * <p>
	 * 如果getType()为null或空字符串（长度为零的字符串），则与getValue返回值一致。
	 * </p>
	 * 
	 * @return Object 转换返回的结果。
	 */
	public Object getTypeValue() {
		return ObjectUtil.getResult(this.getType(), this.getValue());
	}

	/**
	 * 设置参数值，可选。
	 * 
	 * @param m_value String。
	 */
	public void setValue(String m_value) {
		this.m_value = m_value;
	}

	/**
	 * 返回参数值类型，可选，默认为字符串。
	 * 
	 * @return String 。
	 */
	public String getType() {
		if (this.m_type != null)
			return this.m_type;
		else
			return "string";
	}

	/**
	 * 设置参数值类型，可选，默认为字符串。
	 * 
	 * @param m_type String 。
	 */
	public void setType(String m_type) {
		this.m_type = m_type;
	}

	/**
	 * 返回参数说明，可选。
	 * 
	 * @return String 。
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置参数说明，可选。
	 * 
	 * @param m_description String 。
	 */
	public void setDescription(String m_description) {
		this.m_description = m_description;
	}

}

