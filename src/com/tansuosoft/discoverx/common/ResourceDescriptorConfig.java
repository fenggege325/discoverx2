/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.util.List;

import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 用于获取系统所有资源相关的配置和属性描述信息的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ResourceDescriptorConfig extends Config {
	/**
	 * 文档资源目录名。
	 */
	public static final String DOCUMENT_DIRECTORY = "document";

	/**
	 * 缺省构造器。
	 */
	private ResourceDescriptorConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static ResourceDescriptorConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return ResourceDescriptorConfig
	 */
	public static ResourceDescriptorConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new ResourceDescriptorConfig();
			}
		}
		return m_instance;
	}

	private List<ResourceDescriptor> m_resourceDescriptors = null;

	/**
	 * 获取包含的资源描述对象的集合。
	 * 
	 * @return List&lt;ResourceDescriptor&gt;
	 */
	public List<ResourceDescriptor> getResourceDescriptors() {
		return this.m_resourceDescriptors;
	}

	/**
	 * 设置包含的资源描述对象的集合。
	 * 
	 * @param resourceDescriptors
	 */
	public void setResourceDescriptors(List<ResourceDescriptor> resourceDescriptors) {
		this.m_resourceDescriptors = resourceDescriptors;
	}

	/**
	 * 根据资源目录名获取此类资源的描述对象。
	 * 
	 * @param directory
	 * @return ResourceDescriptor
	 */
	public ResourceDescriptor getResourceDescriptor(String directory) {
		if (this.m_resourceDescriptors == null || directory == null || directory.length() == 0) return null;
		for (ResourceDescriptor x : this.m_resourceDescriptors) {
			if (directory.equalsIgnoreCase(x.getDirectory())) return x;
		}
		return null;
	}

	/**
	 * 根据资源对应实体类信息返回对应此类资源的描述对象。
	 * 
	 * @param cls Class&lt;?&gt; 资源对应实体类信息，比如：Form.class、Application.class等。
	 * @return ResourceDescriptor 如果找不到对应描述类型，则返回null.
	 */
	public ResourceDescriptor getResourceDescriptor(Class<?> cls) {
		if (this.m_resourceDescriptors == null || cls == null) return null;
		String clsName = cls.getName();
		for (ResourceDescriptor x : this.m_resourceDescriptors) {
			if (clsName.equalsIgnoreCase(x.getEntity())) { return x; }
		}
		return null;
	}

	/**
	 * 根据资源对应实体类信息返回对应此类资源的目录名称。
	 * 
	 * @param cls Class&lt;?&gt; 资源对应实体类信息，比如：Form.class、Application.class等。
	 * @return String 如果找不到对应描述类型，则返回cls对应的类名（首字母小写）。
	 */
	public String getResourceDirectory(Class<?> cls) {
		ResourceDescriptor rd = this.getResourceDescriptor(cls);
		if (rd == null) return cls == null ? null : StringUtil.toCamelCase(cls.getSimpleName());
		return rd.getDirectory();
	}

	/**
	 * 根据资源对应实体类信息返回对应此类资源的描述对象。
	 * 
	 * @param cls Class&lt;?&gt; 资源对应实体类信息，比如：Form.class、Application.class等。
	 * @return ResourceDescriptor 如果找不到对应描述类型，则返回null.
	 */
	public Class<?> getResourceClass(String directory) {
		if (this.m_resourceDescriptors == null || directory == null || directory.length() == 0) return null;
		for (ResourceDescriptor x : this.m_resourceDescriptors) {
			if (directory.equalsIgnoreCase(x.getDirectory())) {
				try {
					return Class.forName(x.getEntity());
				} catch (ClassNotFoundException e) {
					FileLogger.debug("找不到“%1$s”目录下“%2$s”对应的类信息！", directory, x.getEntity());
				}
			}
		}
		return null;
	}

}

