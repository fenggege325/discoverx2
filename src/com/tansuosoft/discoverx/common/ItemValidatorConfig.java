/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.util.List;

import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 保存字段值校验相关校验js函数信息的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ItemValidatorConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private ItemValidatorConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static ItemValidatorConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return ItemValidatorConfig
	 */
	public static ItemValidatorConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new ItemValidatorConfig();
			}
		}
		return m_instance;
	}

	private List<ItemValidator> m_itemValidators = null; // 配置中包含的ItemValidator集合。

	/**
	 * 返回配置中包含的ItemValidator集合。
	 * 
	 * @see ItemValidator
	 * 
	 * @return List<ItemValidator>
	 */
	public List<ItemValidator> getItemValidators() {
		return this.m_itemValidators;
	}

	/**
	 * 设置配置中包含的ItemValidator集合。
	 * 
	 * @see ItemValidator
	 * 
	 * @param itemValidators List<ItemValidator>
	 */
	public void setItemValidators(List<ItemValidator> itemValidators) {
		this.m_itemValidators = itemValidators;
	}

	/**
	 * 获取jsFunction指定函数名包含的ItemValidator对象，如果找不到则返回null。
	 * 
	 * @param jsFunction
	 * @return ItemValidator
	 */
	public ItemValidator getItemValidator(String jsFunction) {
		if (this.m_itemValidators == null || jsFunction == null || jsFunction.length() == 0) return null;
		for (ItemValidator x : this.m_itemValidators) {
			if (jsFunction.equalsIgnoreCase(x.getJsFunctionName())) return x;
		}
		return null;
	}
}

