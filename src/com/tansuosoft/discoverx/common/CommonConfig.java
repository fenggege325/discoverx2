/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.io.File;

import javax.servlet.ServletContext;

import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.Path;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 表示系统通用选项的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class CommonConfig extends Config {
	/**
	 * 系统全局内缺省的文档内容显示区的宽度（单位：像素）的默认值：970。
	 */
	public static final int DEFAULT_DOCUMENT_CONTENT_WIDTH = 970;
	private static final String DEFAULT_SUPPORT_URL = "http://www.tensosoft.com/";
	private static final String DEFAULT_ACCESSORY_PATH_NAME = "files";
	private static final String DEFAULT_RESOURCE_PATH_NAME = "resources";

	/**
	 * 缺省构造器。
	 */
	private CommonConfig() {
		String location = String.format("%s%s.xml", this.getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static CommonConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return CommonConfig
	 */
	public static CommonConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new CommonConfig();
			}
		}
		return m_instance;
	}

	private String m_systemName = null; // 系统名称。
	private String m_belong = null; // 系统所有者名称。
	private String m_provider = "腾硕科技"; // 系统提供商名称。
	private String m_providerEn = null; // 系统提供商英文名称
	private String m_supportUrl = ""; // 支持网站url
	private String m_belongAbbr = ""; // 使用单位简称。
	private String m_providerAbbr = ""; // 系统提供商简称。
	private String m_systemNameAbbr = ""; // 系统名称简称。
	private boolean m_debugable = false; // 是否调式状态，默认为false。
	private String m_eulaUrl = null; // 最终用户许可协议URL地址。
	private String m_installationPath = null; // 应用程序安装路径。
	private String m_resourcePath = null; // 资源文件路径。
	private String m_accessoryPath = null; // 附加文件保存路径。
	private static String defaultResourcePath = null;
	private static String defaultAccessoryPath = null;
	private int m_documentCacheCapacity = 3000; // 文档缓存最大容量。
	private int m_resourceCacheCapacity = 2000; // 资源缓存最大容量。
	private String m_authentication = null; // 系统默认使用的登录验证类的全限定类名。
	private String m_authorization = null; // 系统默认使用的授权验证类的全限定类名。
	private String m_sessionImplement = null; // 系统默认使用的用户自定义会话实现对象全限定类名称。
	private int m_maxTryLoginCount = 3; // 同一个用户登录尝试的最多次数，默认为3。
	private int m_maxTryLoginTimeout = 30; // 同一个用户登录在尝试最多次数后允许再次登录的时间间隔，单位为分钟，默认为30。
	private int m_maxDocumentContentWidth = 0; // 系统配置的全局默认的文档内容显示区的宽度（单位：像素），默认为900。
	private int m_maxUploadSize = 1020 * 1024 * 100; // 最大可上传内容字节数。
	private int m_itemValueStroageBoundaryLength = 4000; // 字段值存储时判断内容是以普通值还是CLOB值存储的字节数（以内容编码为utf-8字节数字计算字节数）。
	private boolean m_allowMultipleLogin = false; // 是否允许同一个用户在不同计算机上同时登录。
	private int m_DBType = DBType.Others.getIntValue(); // 系统可能使用到的数据库类型。

	/**
	 * 返回系统名称。
	 * 
	 * @return String
	 */
	public String getSystemName() {
		return this.m_systemName;
	}

	/**
	 * 设置系统名称。
	 * 
	 * @param systemName String
	 */
	public void setSystemName(String systemName) {
		this.m_systemName = systemName;
	}

	/**
	 * 返回使用单位（系统所有者）名称。
	 * 
	 * <p>
	 * 此名称即用户的单位名称。
	 * </p>
	 * 
	 * @return String
	 */
	public String getBelong() {
		return this.m_belong;
	}

	/**
	 * 设置使用单位（系统所有者）名称。
	 * 
	 * <p>
	 * 此名称即用户的单位名称。
	 * </p>
	 * 
	 * @param belong String
	 */
	public void setBelong(String belong) {
		this.m_belong = belong;
	}

	/**
	 * 返回系统提供商名称。
	 * 
	 * @return String
	 */
	public String getProvider() {
		return this.m_provider;
	}

	/**
	 * 设置系统提供商名称。
	 * 
	 * @param provider String
	 */
	public void setProvider(String provider) {
		this.m_provider = provider;
	}

	/**
	 * 返回支持网站url地址。
	 * 
	 * @return
	 */
	public String getSupportUrl() {
		if (m_supportUrl == null || m_supportUrl.length() == 0) return DEFAULT_SUPPORT_URL;
		return this.m_supportUrl;
	}

	/**
	 * 设置支持网站url地址。
	 * 
	 * @param supportUrl
	 */
	public void setSupportUrl(String supportUrl) {
		this.m_supportUrl = supportUrl;
	}

	/**
	 * 返回提供商英文名称。
	 * 
	 * <p>
	 * 通过支持网站中的域名获取。
	 * </p>
	 * 
	 * @return
	 */
	public String getProviderEn() {
		if (m_providerEn == null || m_providerEn.length() == 0) {
			if (this.m_supportUrl != null && this.m_supportUrl.length() > 0) {
				int pos = this.m_supportUrl.indexOf('.');
				String tmp = (pos > 0 ? this.m_supportUrl.substring(pos + 1) : "");
				if (tmp.length() > 0) {
					pos = tmp.indexOf('.');
					if (pos > 0) {
						m_providerEn = StringUtil.toPascalCase(tmp.substring(0, pos));
					}
				}
			} else {
				m_providerEn = "Tensosoft";
			}
		}
		return m_providerEn;
	}

	/**
	 * 返回使用单位简称。
	 * 
	 * <p>
	 * 如果没有配置则和{@link #getBelong()}返回相同结果。
	 * </p>
	 * 
	 * @return String
	 */
	public String getBelongAbbr() {
		if (m_belongAbbr == null || m_belongAbbr.length() == 0) return m_belong;
		return this.m_belongAbbr;
	}

	/**
	 * 设置使用单位简称。
	 * 
	 * @param belongAbbr String
	 */
	public void setBelongAbbr(String belongAbbr) {
		this.m_belongAbbr = belongAbbr;
	}

	/**
	 * 返回系统提供商简称。
	 * 
	 * <p>
	 * 如果没有配置则和{@link #getProvider()}返回相同结果。
	 * </p>
	 * 
	 * @return String
	 */
	public String getProviderAbbr() {
		if (m_providerAbbr == null || m_providerAbbr.length() == 0) return m_provider;
		return this.m_providerAbbr;
	}

	/**
	 * 设置系统提供商简称。
	 * 
	 * @param providerAbbr String
	 */
	public void setProviderAbbr(String providerAbbr) {
		this.m_providerAbbr = providerAbbr;
	}

	/**
	 * 返回系统名称简称。
	 * 
	 * <p>
	 * 如果没有配置则和{@link #getSystemName()}返回相同结果。
	 * </p>
	 * 
	 * @return String
	 */
	public String getSystemNameAbbr() {
		if (m_systemNameAbbr == null || m_systemNameAbbr.length() == 0) return m_systemName;
		return this.m_systemNameAbbr;
	}

	/**
	 * 设置系统名称简称。
	 * 
	 * @param systemNameAbbr String
	 */
	public void setSystemNameAbbr(String systemNameAbbr) {
		this.m_systemNameAbbr = systemNameAbbr;
	}

	/**
	 * 返回是否调式状态，默认为false。
	 * 
	 * @return boolean 是否调式状态，默认为false。
	 */
	public boolean getDebugable() {
		return this.m_debugable;
	}

	/**
	 * 设置是否调式状态，默认为false。
	 * 
	 * @param debugable boolean 是否调式状态，默认为false。
	 */
	public void setDebugable(boolean debugable) {
		this.m_debugable = debugable;
	}

	/**
	 * 返回最终用户许可协议URL地址。
	 * 
	 * @return String
	 */
	public String getEulaUrl() {
		return this.m_eulaUrl;
	}

	/**
	 * 设置最终用户许可协议URL地址。
	 * 
	 * @param eluaUrl String
	 */
	public void setEulaUrl(String eluaUrl) {
		this.m_eulaUrl = eluaUrl;
	}

	/**
	 * 获取应用程序在应用服务器上的物理安装路径。
	 * 
	 * <p>
	 * 返回结果类似：“c:\discoverx2\”、“/opt/discoverx2/”等。
	 * </p>
	 * <p>
	 * 如果没有配置则通过{@link com.tansuosoft.discoverx.util.Path#WEBAPP_ROOT}获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getInstallationPath() {
		this.m_installationPath = Path.normalizeDirectory(this.m_installationPath);
		if (this.m_installationPath == null || this.m_installationPath.length() == 0) this.m_installationPath = Path.WEBAPP_ROOT;
		if (this.m_installationPath == null || this.m_installationPath.length() == 0) throw new RuntimeException("无法获取服务器应用程序安装路径。");
		return this.m_installationPath;
	}

	/**
	 * 返回资源文件存储路径。
	 * 
	 * <p>
	 * 可以配置相对路径（相对{@link CommonConfig#getInstallationPath()}）或绝对路径，不配置则默认为：“{@link CommonConfig#getInstallationPath()}WEB-INF/resources/”。
	 * </p>
	 * 
	 * <p>
	 * 服务器端运行此应用程序的相关操作系统对应用户要对此路径具有增、删、改、读等权限。
	 * </p>
	 * 
	 * @return String
	 */
	public String getResourcePath() {
		if (this.m_resourcePath != null && (this.m_resourcePath.startsWith("/") || this.m_resourcePath.indexOf(":") == 1)) {
			this.m_resourcePath = Path.normalizeDirectory(this.m_resourcePath);
		}
		if (this.m_resourcePath == null || this.m_resourcePath.length() == 0) {
			if (defaultResourcePath == null) defaultResourcePath = this.getInstallationPath() + "WEB-INF" + File.separator + DEFAULT_RESOURCE_PATH_NAME + File.separator;
			return defaultResourcePath;
		}

		return this.m_resourcePath;
	}

	/**
	 * 返回附加文件保存路径。
	 * 
	 * <p>
	 * 可以配置相对路径（相对{@link CommonConfig#getInstallationPath()}）或绝对路径，不配置则默认为：“{@link CommonConfig#getInstallationPath()}WEB-INF/files/”。。
	 * </p>
	 * 
	 * <p>
	 * 服务器端运行此应用程序的相关操作系统对应用户要对此路径具有增、删、改、读等权限。
	 * </p>
	 * 
	 * @return String
	 */
	public String getAccessoryPath() {
		if (this.m_accessoryPath != null && (this.m_accessoryPath.startsWith("/") || this.m_accessoryPath.indexOf(":") == 1)) {
			this.m_accessoryPath = Path.normalizeDirectory(this.m_accessoryPath);
		}
		if (this.m_accessoryPath == null || this.m_accessoryPath.length() == 0) {
			if (defaultAccessoryPath == null) defaultAccessoryPath = this.getInstallationPath() + "WEB-INF" + File.separator + DEFAULT_ACCESSORY_PATH_NAME + File.separator;
			return defaultAccessoryPath;
		}
		return this.m_accessoryPath;
	}

	/**
	 * 返回系统默认使用的登录验证类的全限定类名。
	 * 
	 * @return String
	 */
	public String getAuthentication() {
		return this.m_authentication;
	}

	/**
	 * 设置系统默认使用的登录验证类的全限定类名。
	 * 
	 * @param authentication String
	 */
	public void setAuthentication(String authentication) {
		this.m_authentication = authentication;
	}

	/**
	 * 返回系统默认使用的授权验证类的全限定类名。
	 * 
	 * @return String
	 */
	public String getAuthorization() {
		return this.m_authorization;
	}

	/**
	 * 设置系统默认使用的授权验证类的全限定类名。
	 * 
	 * @param authorization String
	 */
	public void setAuthorization(String authorization) {
		this.m_authorization = authorization;
	}

	/**
	 * 返回文档缓存最大容量，默认为3000。
	 * 
	 * @return int
	 */
	public int getDocumentCacheCapacity() {
		return this.m_documentCacheCapacity;
	}

	/**
	 * 设置文档缓存最大容量。
	 * 
	 * @param documentCacheCapacity int
	 */
	public void setDocumentCacheCapacity(int documentCacheCapacity) {
		this.m_documentCacheCapacity = documentCacheCapacity;
	}

	/**
	 * 返回资源缓存最大容量，默认为2000。
	 * 
	 * @return int
	 */
	public int getResourceCacheCapacity() {
		return this.m_resourceCacheCapacity;
	}

	/**
	 * 设置资源缓存最大容量。
	 * 
	 * @param resourceCacheCapacity int
	 */
	public void setResourceCacheCapacity(int resourceCacheCapacity) {
		this.m_resourceCacheCapacity = resourceCacheCapacity;
	}

	/**
	 * 返回用户登录尝试的最多次数，默认为3。
	 * 
	 * @return int
	 */
	public int getMaxTryLoginCount() {
		return this.m_maxTryLoginCount;
	}

	/**
	 * 设置用户登录尝试的最多次数，默认为3。
	 * 
	 * @param maxTryLoginCount int
	 */
	public void setMaxTryLoginCount(int maxTryLoginCount) {
		this.m_maxTryLoginCount = maxTryLoginCount;
	}

	/**
	 * 返回用户在尝试登录超过允许的次数后允许再次登录的时间间隔，单位为分钟，默认为30。
	 * 
	 * <p>
	 * 最多允许尝试登录的次数通过{@link CommonConfig#getMaxTryLoginCount()}获取。
	 * </p>
	 * 
	 * @return int
	 */
	public int getMaxTryLoginTimeout() {
		return this.m_maxTryLoginTimeout;
	}

	/**
	 * 设置用户在尝试登录超过允许的次数后允许再次登录的时间间隔，单位为分钟，默认为30。
	 * 
	 * @param maxTryLoginTimeout int
	 */
	public void setMaxTryLoginTimeout(int maxTryLoginTimeout) {
		this.m_maxTryLoginTimeout = maxTryLoginTimeout;
	}

	/**
	 * 返回用户自定义会话实现对象全限定类名称。
	 * 
	 * <p>
	 * 不配置则使用系统默认的自定义会话实现！默认为：“com.tansuosoft.discoverx.model.impl.Session”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getSessionImplement() {
		return this.m_sessionImplement;
	}

	/**
	 * 设置用户自定义会话实现对象全限定类名称。
	 * 
	 * <p>
	 * 不配置则使用系统默认的自定义会话实现！默认为：“{@link com.tansuosoft.discoverx.model.Session}”。
	 * </p>
	 * 
	 * @param sessionImplement String
	 */
	public void setSessionImplement(String sessionImplement) {
		this.m_sessionImplement = sessionImplement;
	}

	/**
	 * 返回系统配置的全局默认的文档内容显示区的宽度（单位：像素），默认为900。
	 * 
	 * @return int
	 */
	public int getMaxDocumentContentWidth() {
		if (this.m_maxDocumentContentWidth <= 0) {
			this.m_maxDocumentContentWidth = CommonConfig.DEFAULT_DOCUMENT_CONTENT_WIDTH;
		}
		return this.m_maxDocumentContentWidth;
	}

	/**
	 * 设置系统配置的全局默认的文档内容显示区的宽度（单位：像素），默认为900。
	 * 
	 * @param maxDocumentContentWidth int
	 */
	public void setMaxDocumentContentWidth(int maxDocumentContentWidth) {
		this.m_maxDocumentContentWidth = maxDocumentContentWidth;
	}

	/**
	 * 返回最大可上传文件内容的字节数。
	 * 
	 * <p>
	 * 默认为100M，此配置可能还需事先配置好应用服务器的类似配置才会启用。
	 * </p>
	 * 
	 * @return int
	 */
	public int getMaxUploadSize() {
		if (this.m_maxUploadSize <= 0) {
			this.m_maxUploadSize = 1024 * 1024 * 100;
		}
		return this.m_maxUploadSize;
	}

	/**
	 * 设置最大可上传文件内容的字节数。
	 * 
	 * @param maxUploadSize int
	 */
	public void setMaxUploadSize(int maxUploadSize) {
		this.m_maxUploadSize = maxUploadSize;
	}

	/**
	 * 返回字段值存储时判断内容是以普通值还是CLOB值存储的字节数。
	 * 
	 * <p>
	 * 以待存储的内容编码为utf-8字节数组后计算总字节数，默认为4000。<br/>
	 * 如果待存储的内容计算出来的字节数大于等于这个数字，那么存储为CLOB值，否则存储为普通值（存储于可变长字符串字段中）。
	 * </p>
	 * 
	 * @return int
	 */
	public int getItemValueStroageBoundaryLength() {
		return this.m_itemValueStroageBoundaryLength;
	}

	/**
	 * 设置字段值存储时判断内容是以普通值还是CLOB值存储的字节数。
	 * 
	 * @param itemValueStroageBoundaryLength int
	 */
	public void setItemValueStroageBoundaryLength(int itemValueStroageBoundaryLength) {
		this.m_itemValueStroageBoundaryLength = itemValueStroageBoundaryLength;
	}

	/**
	 * 返回是否允许同一个用户在不同计算机上同时登录。
	 * 
	 * <p>
	 * 默认为false（即不允许同时在不动计算机上多次登录）。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getAllowMultipleLogin() {
		return this.m_allowMultipleLogin;
	}

	/**
	 * 设置是否允许同一个用户在不同计算机上同时登录。
	 * 
	 * @param allowMultipleLogin boolean
	 */
	public void setAllowMultipleLogin(boolean allowMultipleLogin) {
		this.m_allowMultipleLogin = allowMultipleLogin;
	}

	/**
	 * 返回系统可能使用到的数据库类型。
	 * 
	 * <p>
	 * 默认为0（{@link DBType#Others}），表示其它或未知类型的数据库。<br/>
	 * 数据库类型的有效值必须是{@link DBType}中定义的有效值之一或者{@link DBType}中定义的有效值的相加组合。<br/>
	 * 此属性值需手工配置于配置文件中才会返回非0值，它通常用于系统二次开发时获取或区分数据库信息；<br/>
	 * 通常读写系统主数据库信息应通过{@link com.tansuosoft.discoverx.dao.DBRequest}的各类派生类和<strong>com.tansuosoft.discoverx.dao</strong>包下的其它类来实现。
	 * </p>
	 * 
	 * @return int
	 */
	public int getDBType() {
		return this.m_DBType;
	}

	/**
	 * 设置系统可能使用到的数据库类型。
	 * 
	 * @param DBType int
	 */
	public void setDBType(int DBType) {
		this.m_DBType = DBType;
	}

	/**
	 * 保存配置。
	 * 
	 * @see com.tansuosoft.discoverx.common.Config#save()
	 */
	public boolean save() {
		return super.save();
	}

	private boolean m_inited = false;

	/**
	 * 通过{@link javax.servlet.ServletContext}对象进行初始化。
	 * 
	 * <p>
	 * 用于系统内部使用，请勿直接调用！
	 * </p>
	 * 
	 * @param servletContext
	 */
	public void init(ServletContext servletContext) {
		if (m_inited) {
			FileLogger.debug("基于ServletContext的通用配置初始化已经完成！");
			return;
		}
		if (servletContext == null) return;
		m_inited = true;

		final String resourceLocationParamName = "resourceLocation";
		final String accessoryLocationParamName = "accessoryLocation";

		String paramValue = servletContext.getInitParameter(resourceLocationParamName);
		if (paramValue != null && paramValue.length() > 0) {
			if (!paramValue.startsWith("/") && paramValue.indexOf(":") != 1) {
				this.m_resourcePath = this.getInstallationPath() + "WEB-INF" + File.separator + CommonConfig.DEFAULT_RESOURCE_PATH_NAME + File.separator;
			} else {
				this.m_resourcePath = paramValue;
			}
		}

		paramValue = servletContext.getInitParameter(accessoryLocationParamName);
		if (paramValue != null && paramValue.length() > 0) {
			if (!paramValue.startsWith("/") && paramValue.indexOf(":") != 1) {
				this.m_accessoryPath = this.getInstallationPath() + "WEB-INF" + File.separator + CommonConfig.DEFAULT_ACCESSORY_PATH_NAME + File.separator;
			} else {
				this.m_accessoryPath = paramValue;
			}
		}
		try {
			for (GenericPair<Long, Config> gp : instances) {
				if (gp == null) continue;
				if (gp.getKey() == 0L && gp.getValue() instanceof CommonConfig) {
					File f = new File(String.format("%s%s.xml", this.getResourcePath(), this.getClass().getSimpleName()));
					if (f.exists() && f.isFile()) gp.setKey(f.lastModified());
				}
			}
		} catch (Exception ex) {
			FileLogger.debug(ex.getMessage());
		}
	}

	/**
	 * 重载：
	 * 
	 * @see com.tansuosoft.discoverx.common.Config#reload()
	 */
	@Override
	public Config reload() {
		m_providerEn = null;
		return super.reload();
	}

}

