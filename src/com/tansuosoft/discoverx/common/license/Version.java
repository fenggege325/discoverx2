/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common.license;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;

/**
 * 表示系统版本信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Version implements JsonSerializable {
	private static Version m_instance = null;
	private static final String PATCHS_FILE = CommonConfig.getInstance().getInstallationPath() + "WEB-INF/classes/patchs.dat";
	protected static final DateTime BUILDDT = new DateTime("2014-05-20 15:30:00"); // 构建日期
	protected static final int MAJOR_VERSION = 2; // 主版本号
	protected static final int MINOR_VERSION = 2; // 主版本号

	/**
	 * 缺省构造器。
	 */
	private Version() {
		try {
			long bdt = BUILDDT.getTimeMillis();
			String v = String.format("%1$d.%2$d.%3$s", MAJOR_VERSION, MINOR_VERSION, BUILDDT.toString("yyyyMMdd"));
			if (v != null && v.length() > 0) {
				String[] strs = StringUtil.splitString(v, '.');
				m_majorNumber = (strs != null && strs.length > 0 ? StringUtil.getValueInt(strs[0], 1) : 0);
				m_minorNumber = (strs != null && strs.length > 1 ? StringUtil.getValueInt(strs[1], 1) : 0);
				m_buildNumber = (strs != null && strs.length > 2 ? StringUtil.getValueInt(strs[2], 2) : 0);
			}
			if (bdt > 0) m_buildDate = new DateTime(bdt).toString();
			int defaultPatchIndex = 0;
			// 补丁包信息
			File f = new File(PATCHS_FILE);
			if (!f.exists() || !f.isFile()) return;
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line = br.readLine();
			while (line != null && line.length() > 0) {
				if (this.m_patchs == null) this.m_patchs = new ArrayList<StringPair>();
				String pid = StringUtil.stringLeft(line, ":");
				if (pid == null || pid.length() == 0) pid = (defaultPatchIndex++) + "";
				String dt = StringUtil.getValueString(StringUtil.stringRight(line, ":"), "");
				this.m_patchs.add(new StringPair(pid, dt));
				line = br.readLine();
			}
			br.close();
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return
	 */
	public synchronized static Version getInstance() {
		if (m_instance == null) m_instance = new Version();
		return m_instance;
	}

	/**
	 * 重新加载版本信息。
	 */
	public synchronized static void reload() {
		m_instance = null;
		getInstance();
	}

	private int m_majorNumber = 2; // 主板本号。
	private int m_minorNumber = 0; // 辅版本号。
	private int m_buildNumber = 0; // 构建号。
	private String m_buildDate = null; // 构建日期时间。
	private List<StringPair> m_patchs = null; // 补丁包信息。

	/**
	 * 返回主板本号。
	 * 
	 * @return int
	 */
	public int getMajorNumber() {
		return this.m_majorNumber;
	}

	/**
	 * 返回辅版本号。
	 * 
	 * @return int
	 */
	public int getMinorNumber() {
		return this.m_minorNumber;
	}

	/**
	 * 返回构建号。
	 * 
	 * @return int
	 */
	public int getBuildNumber() {
		return this.m_buildNumber;
	}

	/**
	 * 返回构建日期时间。
	 * 
	 * @return String
	 */
	public String getBuildDate() {
		return this.m_buildDate;
	}

	/**
	 * 返回补丁包信息。
	 * 
	 * <p>
	 * 补丁包映射集合的key为补丁包id，value为补丁包安装的日期时间。
	 * </p>
	 * 
	 * @return List&lt;StringPair&gt;
	 */
	public List<StringPair> getPatchs() {
		return this.m_patchs;
	}

	/**
	 * 追加一个补丁包。
	 * 
	 * @param pid 补丁包id，必须。
	 * @param pdt 补丁包安装日期，必须。
	 * @param log 补丁包更新日志信息，可选。
	 */
	public void appendPatch(String pid, String pdt, String log) {
		if (StringUtil.isBlank(pid)) return;
		if (this.m_patchs == null) this.m_patchs = new ArrayList<StringPair>();
		String dt = (pdt == null || pdt.length() == 0 ? DateTime.getNowDTString() : pdt);
		PrintWriter pw = null;
		try {
			File f = new File(PATCHS_FILE);
			if (f.isDirectory() && f.exists()) throw new Exception("无法记录补丁包信息！");
			String key = String.format("%1$s%2$s%3$s", pid, (log == null || log.length() == 0 ? "" : "|"), (log == null || log.length() == 0 ? "" : log));
			for (StringPair sp : m_patchs) {
				if (sp != null && sp.getKey() != null && sp.getKey().equalsIgnoreCase(key)) {
					key = String.format("%1$s|%2$s", key, System.currentTimeMillis());
				}
			}
			pw = new PrintWriter(new FileWriter(f, f.exists()));
			pw.println(String.format("%1$s:%2$s", key, dt));
			this.m_patchs.add(new StringPair(key, dt));
		} catch (Exception ex) {
			FileLogger.error(ex);
		} finally {
			if (pw != null) {
				pw.flush();
				pw.close();
			}
		}
	}

	/**
	 * 重载toString：主板本号.辅版本号.构建号.构建日期{;补丁包个数(如果有的话)}
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.m_majorNumber).append(".").append(this.m_minorNumber).append(".").append(this.m_buildNumber).append(".").append(this.m_buildDate);
		if (this.m_patchs != null && this.m_patchs.size() > 0) {
			sb.append(";patchs:").append(m_patchs.size());
		}
		return sb.toString();
	}

	/**
	 * 重载toJson
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	@Override
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("majorNumber:").append(this.m_majorNumber);
		sb.append(",").append("minorNumber:").append(this.m_minorNumber);
		sb.append(",").append("buildNumber:").append(this.m_buildNumber);
		sb.append(",").append("buildDate:'").append(StringUtil.stringLeft(this.m_buildDate, " ")).append("'");
		if (this.m_patchs != null && this.m_patchs.size() > 0) {
			sb.append(",").append("patchCount:").append(m_patchs.size());
		}
		sb.append("}");
		return sb.toString();
	}
}

