/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import com.tansuosoft.discoverx.util.AJAXResponse;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 通过名称获取URL地址相关的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class UrlConfig extends Config {
	/**
	 * 直接关闭窗口的URL地址的配置参数名。
	 */
	public static final String URLCFGNAME_DIRECT_CLOSE = "direct_close";
	/**
	 * 显示对话框消息然后关闭窗口的URL消息地址的配置参数名。
	 */
	public static final String URLCFGNAME_SHOW_MESSAGE_AND_CLOSE = "show_message_and_close";
	/**
	 * 显示普通结果消息对话框的URL消息地址的配置参数名。
	 */
	public static final String URLCFGNAME_INFO_MESSAGE = "info_message";
	/**
	 * 显示错误消息对话框的URL消息地址的配置参数名。
	 */
	public static final String URLCFGNAME_ERROR_MESSAGE = "error_message";
	/**
	 * 显示{@link AJAXResponse}格式xml消息的URL消息地址的配置参数名。
	 */
	public static final String URLCFGNAME_XML_MESSAGE = "xml_message";
	/**
	 * 显示资源xml内容的URL消息地址的配置参数名。
	 */
	public static final String URLCFGNAME_XML_CONTENT = "xml";
	/**
	 * 显示{@link AJAXResponse}格式json内容的URL消息地址的配置参数名。
	 */
	public static final String URLCFGNAME_JSON_MESSAGE = "json_message";
	/**
	 * 显示通过{@link com.tansuosoft.discoverx.model.Session#getLastMessage()}传递的文本对应的json内容的URL消息地址的配置参数名。
	 */
	public static final String URLCFGNAME_JSON_CONTENT = "json";
	/**
	 * 显示登录界面的url地址的配置参数名。
	 */
	public static final String URLCFGNAME_LOGIN = "login";

	/**
	 * 缺省构造器。
	 */
	private UrlConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static UrlConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return UrlConfig
	 */
	public static UrlConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new UrlConfig();
			}
		}
		return m_instance;
	}

	/**
	 * 获取配置项名称对应的配置项值（url）。
	 * 
	 * @param name
	 * @return String
	 */
	public String getUrl(String name) {
		ConfigEntry entry = this.getConfigEntry(name);
		if (entry == null) return null;
		String url = entry.getValue();
		return url;
	}
}

