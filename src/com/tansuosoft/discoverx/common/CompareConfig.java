/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.util.List;

import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 视图查询比较信息配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class CompareConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private CompareConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static CompareConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return CompareConfig
	 */
	public static CompareConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new CompareConfig();
			}
		}
		return m_instance;
	}

	private List<StringPair> m_compares = null;

	/**
	 * 获取比较方式集合。
	 * 
	 * <p>
	 * StringPair的key中保存比较操作符，value中保存说明。
	 * </p>
	 * 
	 * @return List<StringPair>
	 */
	public List<StringPair> getCompares() {
		return this.m_compares;
	}

	/**
	 * 设置比较方式集合。
	 * 
	 * @see CompareConfig#getCompares()
	 * @param compares List<StringPair>
	 */
	public void setCompares(List<StringPair> compares) {
		this.m_compares = compares;
	}
}

