/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.util.serialization.Serializer;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;
import com.tansuosoft.discoverx.util.serialization.XmlSerializer;

/**
 * 用于描述系统相关配置信息的基类。
 * 
 * <p>
 * 其它具体配置对象可继承此类，并建议同一个配置类只有一个实例且缓存此实例。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class Config {
	protected static List<GenericPair<Long, Config>> instances = new ArrayList<GenericPair<Long, Config>>();

	/**
	 * 缺省构造器。
	 */
	protected Config() {
		if (!(this instanceof CommonConfig)) {
			File f = new File(CommonConfig.getInstance().getResourcePath() + this.getClass().getSimpleName() + ".xml");
			long lm = (f.exists() && f.isFile() ? f.lastModified() : 0L);
			instances.add(new GenericPair<Long, Config>(lm, this));
		} else {
			instances.add(new GenericPair<Long, Config>(0L, this));
		}
	}

	protected java.util.List<ConfigEntry> m_entries = null; // 包含的配置项列表
	protected java.util.HashMap<String, ConfigEntry> m_dict = null; // 配置项名称与配置项的对应关系
	protected String m_name = null; // 配置名称。

	/**
	 * 获取或设置配置名称。
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * @see Config#getName()
	 * @param name void
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回包含的配置项列表
	 * 
	 * @return java.util.List<ConfigEntry> 包含的配置项列表
	 */
	public java.util.List<ConfigEntry> getEntries() {
		return this.m_entries;
	}

	/**
	 * 设置包含的配置项列表
	 * 
	 * @param m_entries java.util.List<ConfigEntry> 包含的配置项列表
	 */
	public void setEntries(java.util.List<ConfigEntry> entries) {
		this.m_entries = entries;
	}

	/**
	 * 同步列表内容到哈希集合。
	 */
	protected void syncEntries2Hashtable() {
		if (this.m_entries == null || this.m_entries.isEmpty()) return;
		if (this.m_dict != null) return;
		this.m_dict = new java.util.HashMap<String, ConfigEntry>(this.m_entries.size());
		for (ConfigEntry x : this.m_entries) {
			this.m_dict.put(x.getName(), x);
		}
	}

	/**
	 * 根据参数名称获取参数值。
	 * 
	 * @param paramName
	 * @return
	 */
	public String getValue(String paramName) {
		ConfigEntry ce = this.getConfigEntry(paramName);
		return (ce == null ? null : ce.getValue());
	}

	/**
	 * 根据参数名称获取参数值类型信息。
	 * 
	 * <p>
	 * 获取参数名对应的{@link ConfigEntry#getType()}的结果。
	 * </p>
	 * 
	 * @param paramName
	 * @return String
	 */
	public String getType(String paramName) {
		ConfigEntry ce = this.getConfigEntry(paramName);
		return (ce == null ? null : ce.getType());
	}

	/**
	 * 根据参数名称返回转换为对应的参数值类型所指定的类型的参数值。
	 * <p>
	 * 如果参数名对应的ConfigEntry的getType()为null或空字符串，则与getValue返回值一致。
	 * </p>
	 * 
	 * @return
	 */
	public Object getTypeValue(String paramName) {
		ConfigEntry ce = this.getConfigEntry(paramName);
		return (ce == null ? null : ce.getTypeValue());
	}

	/**
	 * 根据参数名称获取参数对象。
	 * 
	 * @param paramName
	 * @return
	 */
	public ConfigEntry getConfigEntry(String paramName) {
		if (paramName == null || paramName.length() == 0) return null;
		this.syncEntries2Hashtable();
		if (this.m_dict == null) return null;
		ConfigEntry ce = this.m_dict.get(paramName);
		return ce;
	}

	/**
	 * 根据参数名获取对应参数值对应的对象实例。
	 * <p>
	 * 参数值中必须包含有效全限定类名，如参数值为“java.lang.String”则返回一个String对象实例。
	 * </p>
	 * 
	 * @param paramName
	 * @return
	 */
	public Object getInstanceFromEntryValue(String paramName) {
		return Instance.newInstance(this.getValue(paramName));
	}

	/**
	 * 返回参数名对应的参数值的int类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return int
	 */
	public int getValueInt(String paramName, int defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueInt(this.getValue(paramName), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的long类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return long
	 */
	public long getValueLong(String paramName, long defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueLong(this.getValue(paramName), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的float类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return float
	 */
	public float getValueFloat(String paramName, float defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueFloat(this.getValue(paramName), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的double类型结果。
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return double
	 */
	public double getValueDouble(String paramName, double defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueDouble(this.getValue(paramName), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值的boolean类型结果。
	 * 
	 * <p>
	 * 参数值（不区分大小写）配置为“true”、“yes”、“y”、“1”等情况时返回true，否则返回false。
	 * </p>
	 * 
	 * @param paramName String 参数名，必须。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return boolean
	 */
	public boolean getValueBool(String paramName, boolean defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueBool(this.getValue(paramName), defaultReturn);
	}

	/**
	 * 返回参数名对应的参数值转换为enumCls定义的枚举中某一个具体枚举值类型之后的结果。
	 * 
	 * @param &lt;T&gt; 返回的枚举类型。
	 * @param paramName String 参数名，必须。
	 * @param enumCls Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return T 具体枚举结果。
	 */
	public <T> T getValueEnum(String paramName, Class<T> enumCls, T defaultReturn) {
		return com.tansuosoft.discoverx.util.StringUtil.getValueEnum(this.getValue(paramName), enumCls, defaultReturn);
	}

	/**
	 * 保存配置。
	 * 
	 * <p>
	 * 更新某些配置项后，必须保存配置，此方法为保护级别，不能直接调用，需通过具体实现类的新方法。<br/>
	 * <strong>注意不能在与getter配对的setter中调用。</strong>
	 * </p>
	 */
	protected boolean save() {
		boolean ret = false;
		Class<?> clazz = this.getClass();
		String location = String.format("%s%s.xml", ((this instanceof CommonConfig) ? ((CommonConfig) this).getResourcePath() : CommonConfig.getInstance().getResourcePath()), clazz.getSimpleName());
		Serializer ser = new XmlSerializer();
		StringWriter writer = new StringWriter();
		ser.serialize(this, writer);
		File file = new File(location);
		if (file.exists()) file.delete();

		FileWriter fw = null;
		try {
			fw = new FileWriter(location);
			fw.write("<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n");
			fw.write("<config type=\"");
			fw.write(clazz.getName());
			fw.write("\">\r\n");
			fw.write(writer.toString());
			fw.write("</config>");
			fw.flush();
			fw.close();
			ret = true;
		} catch (IOException e) {
			FileLogger.error(e);
		}
		return ret;
	}

	/**
	 * 重新装载配置对象({@link Config})对应的配置。
	 * 
	 * <p>
	 * 注：重新装载后不要使用原来的配置（即config不能使用）!<br/>
	 * 要使用重新的装载后的新配置，请调用相应配置类的getInstance()方法。
	 * </p>
	 * 
	 * @param config
	 * @return 成功则返回config.
	 */
	public static Config reload(Config config) {
		if (config == null) return null;
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), config.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(config);
		deserializer.deserialize(location, config.getClass());
		return config;
	}

	/**
	 * 重载类名指定的配置对象实例。
	 * 
	 * @param className 要重载的配置对象的类名，如果为“*”，则表示重载所有已缓存的配置对象的实例。
	 * @return 成功则返回true，否则返回false。
	 */
	public static boolean reload(String className) {
		boolean ret = true;
		if (className == null || className.length() == 0) return false;
		boolean allFlag = ("*".equals(className));
		for (GenericPair<Long, Config> gp : instances) {
			if (gp == null) continue;
			Config c = gp.getValue();
			if (c == null) continue;
			if (allFlag || (c.getClass().getName().endsWith(className))) {
				ret = ret && (c.reload() != null);
				if (!allFlag) break;
			}
		}
		return ret;
	}

	/**
	 * 重载当前配置对象实例。
	 */
	public Config reload() {
		return reload(this);
	}
}

