/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common.exception;

/**
 * 表示表达式计算相关异常类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ExpressionEvaluatorException extends BaseException {

	/**
	 * 序列化版本号。
	 */
	private static final long serialVersionUID = 6818042263949802581L;

	/**
	 * 缺省构造器。
	 */
	public ExpressionEvaluatorException() {
	}

	/**
	 * 接收异常消息的构造器。
	 * 
	 * @param message
	 */
	public ExpressionEvaluatorException(String message) {
		super(message);
	}

	/**
	 * 接收导致此异常的源异常或错误对象的构造器。
	 * 
	 * @param cause
	 */
	public ExpressionEvaluatorException(Throwable cause) {
		super(cause);
	}

	/**
	 * 接收异常消息和导致此异常的源异常或错误对象的构造器。
	 * 
	 * @param message
	 * @param cause
	 */
	public ExpressionEvaluatorException(String message, Throwable cause) {
		super(message, cause);
	}

}

