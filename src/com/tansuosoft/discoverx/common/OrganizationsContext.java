/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ResultBuilder;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.DataReader;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.HttpContext;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 用于获取多租户状态下用户所属使用单位(根组织)信息的类。
 * 
 * @author coca@tensosoft.com
 */
public class OrganizationsContext {
	private static OrganizationsContext m_instance = null;
	private static Object m_lock = new Object();

	private Map<String, Integer> unidScMap = null;
	private Map<Integer, String> scUnidMap = null;

	/**
	 * 私有构造器。
	 */
	private OrganizationsContext() {
		unidScMap = Collections.synchronizedSortedMap(new TreeMap<String, Integer>());
		scUnidMap = Collections.synchronizedSortedMap(new TreeMap<Integer, String>());
		DBRequest dbr = new DBRequest() {
			@Override
			protected SQLWrapper buildSQL() {
				SQLWrapper r = new SQLWrapper();
				r.setSql("select c_unid,c_securitycode from t_user where c_punid is null or c_punid='' order by c_unid");
				return r;
			}
		};
		dbr.setResultBuilder(new ResultBuilder() {
			@Override
			public Object build(DBRequest request, Object rawResult) {
				if (rawResult == null || !(rawResult instanceof DataReader)) return null;
				DataReader dr = (DataReader) rawResult;
				try {
					int sc = 0;
					String unid = null;
					while (dr.next()) {
						unid = dr.getString(1);
						sc = dr.getInt(2);
						unidScMap.put(unid, sc);
						scUnidMap.put(sc, unid);
					}
				} catch (SQLException e) {
					FileLogger.error(e);
				}
				return null;
			}
		});
		dbr.sendRequest();
	}

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return OrganizationsContext
	 */
	public static OrganizationsContext getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new OrganizationsContext();
			}
		}
		return m_instance;
	}

	/**
	 * 返回所有使用单位(根组织)的安全编码数组。
	 * 
	 * @return
	 */
	public int[] getAllOrgScs() {
		Set<Integer> set = scUnidMap.keySet();
		int ret[] = new int[set.size()];
		int idx = 0;
		for (int sc : set) {
			ret[idx++] = sc;
		}
		return ret;
	}

	/**
	 * 返回使用单位(根组织或租户)的个数。
	 * 
	 * @return
	 */
	public int getOrgsCount() {
		return scUnidMap.size();
	}

	/**
	 * 返回是否为多租户(多个单位或组织共同)使用的系统。
	 * 
	 * @return
	 */
	public boolean isMultipleOrgs() {
		return (getOrgsCount() > 1);
	}

	/**
	 * 返回当前使用者对应的单位是否为一个租户单位。
	 * 
	 * @return
	 */
	public boolean isInTenantContext() {
		int sc = getContextUserOrgSc();
		// 有多个注册单位(组织)且单位安全编码不是默认管理单位的安全编码
		return (getOrgsCount() > 1) && sc > 0 && sc != ParticipantTree.ROOT_CODE;
	}

	/**
	 * 获取指定用户所属根组织的安全编码。
	 * 
	 * @param user
	 * @return
	 */
	public int getOrgSc(User user) {
		if (user == null || user.getPUNID() == null) return 0;
		Integer i = unidScMap.get(user.getPUNID());
		if (i == null) return 0;
		return i;
	}

	/**
	 * 获取用户自定义会话对应用户所属根组织的安全编码。
	 * 
	 * @param s
	 * @return
	 */
	public int getOrgSc(Session s) {
		return getOrgSc(Session.getUser(s));
	}

	/**
	 * 获取指定用户所属根组织的UNID。
	 * 
	 * @param user
	 * @return
	 */
	public String getOrgUnid(User user) {
		if (user == null) return null;
		int sc = getOrgSc(user);
		return scUnidMap.get(sc);
	}

	/**
	 * 获取用户自定义会话对应用户所属根组织的UNID。
	 * 
	 * @param s
	 * @return
	 */
	public String getOrgUnid(Session s) {
		return getOrgUnid(Session.getUser(s));
	}

	/**
	 * 返回当前用户所属根组织的安全编码。
	 * 
	 * @return
	 */
	public int getContextUserOrgSc() {
		return getOrgSc(SessionContext.getSession(HttpContext.getRequest()));
	}

	/**
	 * 返回当前用户所属根组织的UNID。
	 * 
	 * @return
	 */
	public String getContextUserOrgUnid() {
		return getOrgUnid(SessionContext.getSession(HttpContext.getRequest()));
	}

	/**
	 * 追加一个注册单位。
	 * 
	 * @param unid
	 * @param sc
	 */
	public void add(String unid, int sc) {
		if (unid == null || unid.isEmpty() || sc <= 0) return;
		unidScMap.put(unid, sc);
		scUnidMap.put(sc, unid);
	}
}

