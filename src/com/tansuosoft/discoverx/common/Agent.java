/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

/**
 * 表示一个可以定时执行的代理相关信息的类。
 * 
 * <p>
 * 实际需要系统定时执行一个任务时，需继承此类并实现{@link java.lang.Runnable#run()}方法，然后将此类配置在{@link AgentConfig}配置文件中。
 * </p>
 * <p>
 * 系统定时任务通过{@link java.util.concurrent.ScheduledExecutorService}调度执行，如果某个任务执行过程中抛出了异常，那么此任务及其后续就不会继续执行了。所以如果要确保任务一直被周期执行，那么应确保捕获{@link Agent#run()}中的一切可能的异常。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public abstract class Agent implements Runnable {
	private String m_name = null; // 返回配置的任务名称。
	private String m_description = null; // 返回配置的任务说明。

	/**
	 * 返回{@link AgentConfig}中对应的配置项({@link ConfigEntry#getName()})配置的任务名称。
	 * 
	 * @return String
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置任务名称。
	 * 
	 * <p>
	 * 由系统内部使用。
	 * </p>
	 * 
	 * @param name String
	 */
	protected void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回{@link AgentConfig}中对应的配置项({@link ConfigEntry#getDescription()})配置的任务说明。
	 * 
	 * @return String
	 */
	public String getDescription() {
		return this.m_description;
	}

	/**
	 * 设置任务说明。
	 * 
	 * <p>
	 * 由系统内部使用。
	 * </p>
	 * 
	 * @param description String
	 */
	protected void setDescription(String description) {
		this.m_description = description;
	}

	/**
	 * 返回第一次执行的延迟秒数
	 * 
	 * <p>
	 * 表示此任务第一次执行相对于任务初始化时的时间间隔(单位为秒)，如果为0(默认)或小于0则表示马上执行。
	 * </p>
	 * <p>
	 * 实际实现类可重载此方法以提供个性化的结果。
	 * </p>
	 * 
	 * @return long
	 */
	public long getInitialDelay() {
		return 0;
	}

	/**
	 * 返回重复执行的频率。
	 * 
	 * <p>
	 * 即此定时任务定时执行的间隔(单位为秒)，如果为0则表示不定时执行(即只在{@link #getInitialDelay}后执行一次)。
	 * </p>
	 * <p>
	 * 实际实现类需重载此方法以提供定时频率结果。
	 * </p>
	 * 
	 * @return long
	 */
	public abstract long getPeriod();

	/**
	 * 返回执行频率是否绝对频率。
	 * 
	 * <p>
	 * 如果为true，则为绝对频率(即此定时任务的上一个任务启动和下一个任务启动之间的延迟为{@link #getPeriod()}的结果)。否则默认为相对频率(即此定时任务的上一个执行完毕后{@link #getPeriod()}时间后开始执行下一个任务)。
	 * </p>
	 * <p>
	 * 实际实现类可重载此方法以提供个性化的结果。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getAbsolute() {
		return false;
	}

	/**
	 * 返回是否输出执行日志信息。
	 * 
	 * <p>
	 * 默认为false，如果为true，则会在开始和完成执行时输出控制台日志信息。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getVerbose() {
		return false;
	}
}

