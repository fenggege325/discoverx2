/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.util.List;

import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 记录系统自动定时执行的任务信息的配置实现类。
 * 
 * <p>
 * 其中{@link ConfigEntry#getValue()}为{@link Agent}实现类的全限定类名。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class AgentConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private AgentConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static AgentConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return AgentConfig
	 */
	public static AgentConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new AgentConfig();
			}
		}
		return m_instance;
	}

	/**
	 * 返回所有配置的定时任务对象数组。
	 * 
	 * @return Agent[]
	 */
	public Agent[] getAllAgents() {
		List<ConfigEntry> entries = this.getEntries();
		Agent cr = new ConfigReloader();
		if (entries == null || entries.isEmpty()) {
			Agent[] result = new Agent[1];
			result[0] = cr;
			return result;
		}
		Agent[] result = new Agent[entries.size() + 1];
		int idx = 0;
		result[idx++] = cr;
		for (ConfigEntry x : entries) {
			if (x == null) continue;
			Agent a = Instance.newInstance(x.getValue(), Agent.class);
			if (a == null) continue;
			a.setName(x.getName());
			a.setDescription(x.getDescription());
			result[idx++] = a;
		}
		return result;
	}
}

