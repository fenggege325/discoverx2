/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.util.List;

import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 表示描述某一类（属于同一个目录）资源相关配置和属性的资源描述类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ResourceDescriptor {

	/**
	 * 缺省构造器。
	 */
	public ResourceDescriptor() {
	}

	private String m_entity = null; // 此类资源对应的实体类（model中的资源类）的全限定类名称。
	private String m_directory = null; // 此类资源的目录名（必须唯一）。
	private String m_name = null; // 此类资源的名称（必须唯一）。
	private boolean m_single = false; // 此类资源是否为单实例资源，默认为false。
	private boolean m_cache = false; // 此类资源是否被缓存，默认为true。
	private int m_sort = 0; // 排序号，默认为0。
	private String m_serializer = null;
	private String m_deserializer = null;
	private String m_receiver = null;
	private String m_opener = null;
	private String m_constructor = null;
	private String m_inserter = null;
	private String m_updater = null;
	private String m_remover = null;
	private boolean m_accessoryContainer = false; // 是否能够包含附加文件，默认为false。
	private int m_securityRange = 1; // 此类资源可定义的安全控制范围。
	private boolean m_configurable = true; // 此类资源是否可供用户配置。
	private boolean m_xmlStore = true; // 此类资源是否以xml文件方式持久保存，默认为true。
	private String m_configView = null; // 配置时列出此类资源列表的视图UNID，必须。
	private boolean m_embeded = false; // 是否嵌入资源，默认为false。
	private String m_jsonSerializerFilter = null; // json序列化时的自定义过滤器实现类的全限定类名。
	private String m_xmlSerializerFilter = null; // xml序列化时的自定义过滤器实现类的全限定类名。

	/**
	 * 返回此类资源对应的实体类（model中的资源类）的全限定类名称。
	 * 
	 * @return String 此类资源对应的实体类（model中的资源类）的全限定类名称。
	 */
	public String getEntity() {
		return this.m_entity;
	}

	/**
	 * 设置此类资源对应的实体类（model中的资源类）的全限定类名称。
	 * 
	 * @param entity String 此类资源对应的实体类（model中的资源类）的全限定类名称。
	 */
	public void setEntity(String entity) {
		this.m_entity = entity;
	}

	/**
	 * 返回此类资源的目录名（必须唯一）。
	 * 
	 * @return String 此类资源的目录名（必须唯一）。
	 */
	public String getDirectory() {
		return this.m_directory;
	}

	/**
	 * 设置此类资源的目录名（必须唯一）。
	 * 
	 * @param directory String 此类资源的目录名（必须唯一）。
	 */
	public void setDirectory(String directory) {
		this.m_directory = directory;
	}

	/**
	 * 返回此类资源的名称（必须唯一）。
	 * 
	 * @return String 此类资源的名称（必须唯一）。
	 */
	public String getName() {
		return this.m_name;
	}

	/**
	 * 设置此类资源的名称（必须唯一）。
	 * 
	 * @param name String 此类资源的名称（必须唯一）。
	 */
	public void setName(String name) {
		this.m_name = name;
	}

	/**
	 * 返回此类资源是否为单实例资源，默认为false。
	 * 
	 * @return boolean 此类资源是否为单实例资源，默认为false。
	 */
	public boolean getSingle() {
		return this.m_single;
	}

	/**
	 * 设置此类资源是否为单实例资源，默认为false。
	 * 
	 * @param single boolean 此类资源是否为单实例资源，默认为false。
	 */
	public void setSingle(boolean single) {
		this.m_single = single;
	}

	/**
	 * 返回此类资源是否被缓存，默认为true。
	 * 
	 * @return boolean 此类资源是否被缓存，默认为true。
	 */
	public boolean getCache() {
		return this.m_cache;
	}

	/**
	 * 设置此类资源是否被缓存，默认为true。
	 * 
	 * @param cache boolean 此类资源是否被缓存，默认为true。
	 */
	public void setCache(boolean cache) {
		this.m_cache = cache;
	}

	/**
	 * 返回排序号，默认为0。
	 * 
	 * @return int 排序号，默认为0。
	 */
	public int getSort() {
		return this.m_sort;
	}

	/**
	 * 设置排序号，默认为0。
	 * 
	 * @param sort int 排序号，默认为0。
	 */
	public void setSort(int sort) {
		this.m_sort = sort;
	}

	/**
	 * 返回此类资源对应的实体对象的序列化为xml文本的全限定类名称。
	 * 
	 * <p>
	 * 默认为“{@link com.tansuosoft.discoverx.util.serialization.XmlSerializer}”
	 * </p>
	 * 
	 * @return String
	 */
	public String getSerializer() {
		if (this.m_serializer == null || this.m_serializer.length() == 0) this.m_serializer = "com.tansuosoft.discoverx.util.serialization.XmlSerializer";
		return this.m_serializer;
	}

	/**
	 * 设置此类资源对应的实体对象的序列化为xml文本的全限定类名称。
	 * 
	 * @param serializer String
	 */
	public void setSerializer(String serializer) {
		this.m_serializer = serializer;
	}

	/**
	 * 返回从持久化保存的信息（XML或数据库表记录）中反序列化此类资源对应的实体对象的全限定类名称。
	 * 
	 * <p>
	 * 如果配置则返回配置的结果，如果没有配置，则按以下条件返回：<br/>
	 * 如果资源为持久化保存到Xml文件的资源类型，则返回：{@link com.tansuosoft.discoverx.util.serialization.XmlDeserializer};<br/>
	 * 否则返回：“com.tansuosoft.discoverx.dao.impl.[资源实体对象名称]Deserializer”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDeserializer() {
		if (this.m_deserializer != null && this.m_deserializer.length() > 0) { return this.m_deserializer; }
		if (this.m_xmlStore) {
			return "com.tansuosoft.discoverx.util.serialization.XmlDeserializer";
		} else {
			String name = StringUtil.stringRightBack(this.m_entity, ".", this.m_entity.length() - 1);
			return String.format("com.tansuosoft.discoverx.dao.impl.%1$sDeserializer", name);
		}
	}

	/**
	 * 设置从持久化保存的信息（XML或数据库表记录）中反序列化此类资源对应的实体对象的全限定类名称。
	 * 
	 * @param deserializer String
	 */
	public void setDeserializer(String deserializer) {
		this.m_deserializer = deserializer;
	}

	/**
	 * 返回新增此类资源对应的实体对象的{@link com.tansuosoft.discoverx.dao.DBRequest}实现类的全限定类名。
	 * 
	 * <p>
	 * 对于保存于数据库表记录中的资源才有意义，否则被忽略。
	 * </p>
	 * <p>
	 * 如果没有配置，则默认返回：“com.tansuosoft.discoverx.dao.impl.[资源实体对象名称]Inserter”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getInserter() {
		if (this.m_inserter == null || this.m_inserter.length() == 0) {
			String name = StringUtil.stringRightBack(this.m_entity, ".", this.m_entity.length() - 1);
			this.m_inserter = String.format("com.tansuosoft.discoverx.dao.impl.%1$sInserter", name);
		}
		return this.m_inserter;
	}

	/**
	 * 设置新增此类资源对应的实体对象的{@link com.tansuosoft.discoverx.dao.DBRequest}实现类的全限定类名。
	 * 
	 * @param inserter String
	 */
	public void setInserter(String inserter) {
		this.m_inserter = inserter;
	}

	/**
	 * 返回更新此类资源对应的实体对象的{@link com.tansuosoft.discoverx.dao.DBRequest}实现类的全限定类名。
	 * 
	 * <p>
	 * 对于保存于数据库表记录中的资源才有意义，否则被忽略。
	 * </p>
	 * <p>
	 * 如果没有配置，则默认返回：“com.tansuosoft.discoverx.dao.impl.[资源实体对象名称]Updater”。
	 * </p>
	 * 
	 * @return String 更新此类资源对应的实体对象的已有持久化记录的类的全限定类名称。
	 */
	public String getUpdater() {
		if (this.m_updater == null || this.m_updater.length() == 0) {
			String name = StringUtil.stringRightBack(this.m_entity, ".", this.m_entity.length() - 1);
			this.m_updater = String.format("com.tansuosoft.discoverx.dao.impl.%1$sUpdater", name);
		}
		return this.m_updater;
	}

	/**
	 * 设置更新此类资源对应的实体对象的{@link com.tansuosoft.discoverx.dao.DBRequest}实现类的全限定类名。
	 * 
	 * @param updater String 更新此类资源对应的实体对象的已有持久化记录的类的全限定类名称。
	 */
	public void setUpdater(String updater) {
		this.m_updater = updater;
	}

	/**
	 * 返回删除此类资源对应的{@link com.tansuosoft.discoverx.dao.DBRequest}实现类的全限定类名。
	 * 
	 * <p>
	 * 对于保存于数据库表记录中的资源才有意义，否则被忽略。
	 * </p>
	 * <p>
	 * 如果没有配置，则默认返回：“com.tansuosoft.discoverx.dao.impl.[资源实体对象名称]Remover”。
	 * </p>
	 * 
	 * @return String 删除此类资源对应的实体对象的已有持久化记录的类的全限定类名称。
	 */
	public String getRemover() {
		if (this.m_remover == null || this.m_remover.length() == 0) {
			String name = StringUtil.stringRightBack(this.m_entity, ".", this.m_entity.length() - 1);
			this.m_remover = String.format("com.tansuosoft.discoverx.dao.impl.%1$sRemover", name);
		}
		return this.m_remover;
	}

	/**
	 * 设置删除此类资源对应的实体对象的{@link com.tansuosoft.discoverx.dao.DBRequest}实现类的全限定类名。
	 * 
	 * @param remover String 删除此类资源对应的实体对象的已有持久化记录的类的全限定类名称。
	 */
	public void setRemover(String remover) {
		this.m_remover = remover;
	}

	/**
	 * 返回通过web提交的信息更新此类资源对应的实体对象的类（{@link com.tansuosoft.discoverx.bll.ResourceReceiver}）的权限定名称。
	 * 
	 * <p>
	 * 如果没有配置，则自动按规则组合一个。
	 * </p>
	 * 
	 * @return String
	 */
	public String getReceiver() {
		if (this.m_receiver == null || this.m_receiver.length() == 0) {
			String name = StringUtil.stringRightBack(this.m_entity, ".", this.m_entity.length() - 1);
			this.m_receiver = String.format("com.tansuosoft.discoverx.web.receiver.%1$sReceiver", name);
		}
		return this.m_receiver;
	}

	/**
	 * 设置通过web提交的信息更新此类资源对应的实体对象的类（{@link com.tansuosoft.discoverx.bll.ResourceReceiver}）的权限定名称。
	 * 
	 * @param receiver String
	 */
	public void setReceiver(String receiver) {
		this.m_receiver = receiver;
	}

	/**
	 * 返回打开此类资源对应的实体对象的类（{@link com.tansuosoft.discoverx.bll.ResourceOpener}）的全限定类名称。
	 * 
	 * <p>
	 * 如果没有配置，则默认返回：“{@link com.tansuosoft.discoverx.bll.resource.DefaultResourceOpener}”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getOpener() {
		return this.m_opener;
	}

	/**
	 * 设置打开此类资源对应的实体对象的类（{@link com.tansuosoft.discoverx.bll.ResourceOpener}）的全限定类名称。
	 * 
	 * @param opener String
	 */
	public void setOpener(String opener) {
		this.m_opener = opener;
	}

	/**
	 * 返回构造（新增）此类资源对应的实体对象的类（{@link com.tansuosoft.discoverx.bll.ResourceConstructor}）的全限定类名称。
	 * 
	 * <p>
	 * 如果没有配置，则默认返回：“{@link com.tansuosoft.discoverx.bll.resource.DefaultResourceConstructor}”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getConstructor() {
		return this.m_constructor;
	}

	/**
	 * 设置构造（新增）此类资源对应的实体对象的类（{@link com.tansuosoft.discoverx.bll.ResourceConstructor}）的全限定类名称。
	 * 
	 * @param constructor String
	 */
	public void setConstructor(String constructor) {
		this.m_constructor = constructor;
	}

	/**
	 * 返回是否能够包含附加文件，默认为false。
	 * 
	 * <p>
	 * 对于文档等类别的资源，可能会包含一些额外的文件，比如正文、附件或其它类型的二进制文件，那么这些资源需要实现此接口以提供附加文件操作功能。
	 * </p>
	 * 
	 * @return boolean 是否能够包含附加文件，默认为false。
	 */
	public boolean getAccessoryContainer() {
		return this.m_accessoryContainer;
	}

	/**
	 * 设置是否能够包含附加文件，默认为false。
	 * 
	 * @param accessoryContainer boolean 是否能够包含附加文件，默认为false。
	 */
	public void setAccessoryContainer(boolean accessoryContainer) {
		this.m_accessoryContainer = accessoryContainer;
	}

	/**
	 * 获取或设置此类资源可定义的安全控制范围，默认为{@link SecurityRange#Config}（在<b>com.tansuosoft.discoverx.model</b>中）对应的数字值（1）。
	 * 
	 * <p>
	 * 多个SecurityRange数字值可以以int的“|”（或）操作进行组合。<br/>
	 * 有些资源可以定义多个控制范围，比如应用模块资源，可以定义本身安全控制，还可以定义其所包含文档的安全控制。
	 * </p>
	 * 
	 * @return int
	 */
	public int getSecurityRange() {
		return this.m_securityRange;
	}

	/**
	 * @see ResourceDescriptor#getSecurityRange()
	 * @param securityRange void
	 */
	public void setSecurityRange(int securityRange) {
		this.m_securityRange = securityRange;
	}

	/**
	 * 返回此类资源是否可供用户配置，默认为true。
	 * 
	 * <p>
	 * 如果此属性为true，那么用户可以在管理通道中配置资源信息，否则为运行时动态生成的资源。
	 * </p>
	 * 
	 * @return boolean 此类资源是否可供用户配置。
	 */
	public boolean getConfigurable() {
		return this.m_configurable;
	}

	/**
	 * 设置此类资源是否可供用户配置。
	 * 
	 * @param configurable boolean 此类资源是否可供用户配置。
	 * @see ResourceDescriptor#getConfigurable()
	 */
	public void setConfigurable(boolean configurable) {
		this.m_configurable = configurable;
	}

	/**
	 * 返回此类资源是否以xml文件方式持久保存，默认为true。
	 * 
	 * @return boolean
	 */
	public boolean getXmlStore() {
		return this.m_xmlStore;
	}

	/**
	 * 设置此类资源是否以xml文件方式持久保存，默认为true。
	 * 
	 * @param xmlStore boolean
	 */
	public void setXmlStore(boolean xmlStore) {
		this.m_xmlStore = xmlStore;
	}

	private List<EventHandlerInfo> m_eventHandlers = null; // 包含所定义的资源的事件处理程序信息，可选。

	/**
	 * 返回包含所定义的资源的事件处理程序信息，可选。
	 * 
	 * @return List<EventHandlerInfo> 包含所定义的资源的事件处理程序信息，可选。
	 */
	public List<EventHandlerInfo> getEventHandlers() {
		return this.m_eventHandlers;
	}

	/**
	 * 设置包含所定义的资源的事件处理程序信息，可选。
	 * 
	 * @param eventHandlers List<EventHandlerInfo> 包含所定义的资源的事件处理程序信息，可选。
	 */
	public void setEventHandlers(List<EventHandlerInfo> eventHandlers) {
		this.m_eventHandlers = eventHandlers;
	}

	/**
	 * 返回配置时列出此类资源列表的视图UNID。
	 * 
	 * <p>
	 * 在管理通道中需要进行配置的资源必须提供有效的视图UNID。
	 * </p>
	 * 
	 * @return String
	 */
	public String getConfigView() {
		return this.m_configView;
	}

	/**
	 * 设置配置时列出此类资源列表的视图UNID。
	 * 
	 * @param configView String
	 */
	public void setConfigView(String configView) {
		this.m_configView = configView;
	}

	/**
	 * 返回是否嵌入类型的资源，默认为false。
	 * 
	 * <p>
	 * 嵌入类型的资源一般是指包含于其它资源中的资源。如表单中的字段、流程中的环节、文档中的附加文件、意见等。<br/>
	 * 潜入资源的权限等信息会自动继承自所属资源。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getEmbeded() {
		return this.m_embeded;
	}

	/**
	 * 设置是否嵌入资源，默认为false。
	 * 
	 * @see ResourceDescriptor#getEmbeded()
	 * @param embeded boolean
	 */
	public void setEmbeded(boolean embeded) {
		this.m_embeded = embeded;
	}

	/**
	 * 返回json序列化时的自定义过滤器实现类的全限定类名。
	 * 
	 * <p>
	 * 必须是实现{@link com.tansuosoft.discoverx.util.serialization.SerializationFilter}的类的全限定类名。
	 * </p>
	 * 
	 * @return String
	 */
	public String getJsonSerializerFilter() {
		return this.m_jsonSerializerFilter;
	}

	/**
	 * 设置json序列化时的自定义过滤器实现类的全限定类名。
	 * 
	 * <p>
	 * 可选，默认系统序列化全部可读写属性。
	 * </p>
	 * 
	 * @param jsonSerializerFilter String
	 */
	public void setJsonSerializerFilter(String jsonSerializerFilter) {
		this.m_jsonSerializerFilter = jsonSerializerFilter;
	}

	/**
	 * 返回xml序列化时的自定义过滤器实现类的全限定类名。
	 * 
	 * <p>
	 * 必须是实现{@link com.tansuosoft.discoverx.util.serialization.SerializationFilter}的类的全限定类名。
	 * </p>
	 * 
	 * @return String
	 */
	public String getXmlSerializerFilter() {
		return this.m_xmlSerializerFilter;
	}

	/**
	 * 设置xml序列化时的自定义过滤器实现类的全限定类名。
	 * 
	 * <p>
	 * 可选，默认系统序列化全部可读写属性。
	 * </p>
	 * 
	 * @param xmlSerializerFilter String
	 */
	public void setXmlSerializerFilter(String xmlSerializerFilter) {
		this.m_xmlSerializerFilter = xmlSerializerFilter;
	}

	/**
	 * 获取保存在数据库中的资源对应的主表名。
	 * 
	 * <p>
	 * 如果{@link ResourceDescriptor#getXmlStore()}为true，则返回null。
	 * </p>
	 * 
	 * @return
	 */
	public String getDbTableName() {
		if (this.m_xmlStore) return null;
		String t = null;
		if ("organization".equalsIgnoreCase(this.m_directory)) {
			t = "user";
		} else {
			t = this.m_directory;
		}
		if (t == null || t.length() == 0) throw new RuntimeException("资源目录没有提供！");
		return String.format("t_%1$s", t.toLowerCase());
	}
}

