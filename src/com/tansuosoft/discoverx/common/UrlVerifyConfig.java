/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 记录需要登录才能访问的URL地址对应的正则表达式配置类。
 * 
 * @author coca@tansuosoft.cn
 */
public class UrlVerifyConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private UrlVerifyConfig() {
		String location = String.format("%1$s%2$s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static UrlVerifyConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return UrlConfig
	 */
	public static UrlVerifyConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new UrlVerifyConfig();
			}
		}
		return m_instance;
	}

}

