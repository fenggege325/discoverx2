/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.util.List;

import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 提供系统全局事件配置信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class EventHandlerConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private EventHandlerConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static EventHandlerConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return EventHandlerConfig
	 */
	public static EventHandlerConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new EventHandlerConfig();
			}
		}
		return m_instance;
	}

	private List<EventHandlerInfo> m_eventHandlers = null; // 配置的事件处理程序信息列表集合。

	/**
	 * 返回配置的事件处理程序信息列表集合。
	 * 
	 * @return List<EventHandlerInfo>
	 */
	public List<EventHandlerInfo> getEventHandlers() {
		return this.m_eventHandlers;
	}

	/**
	 * 设置配置的事件处理程序信息列表集合。
	 * 
	 * @param eventHandlers List<EventHandlerInfo>
	 */
	public void setEventHandlers(List<EventHandlerInfo> eventHandlers) {
		this.m_eventHandlers = eventHandlers;
	}
}

