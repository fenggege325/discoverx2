/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 表示数据库类型的枚举。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public enum DBType implements EnumBase {
	/**
	 * 表示Mysql数据库（1）。
	 */
	Mysql(1),
	/**
	 * 表示Oracle数据库（2）。
	 */
	Oracle(2),
	/**
	 * 表示Sqlserver数据库（4）。
	 */
	Sqlserver(4),
	/**
	 * 表示DB2数据库（8）。
	 */
	DB2(8),
	/**
	 * 表示PostgreSQL数据库（16）。
	 */
	PostgreSQL(16),
	/**
	 * 表示Informix数据库（32）。
	 */
	Informix(32),
	/**
	 * 表示Derby数据库（64）。
	 */
	Derby(64),
	/**
	 * 表示Sybase数据库（128）。
	 */
	Sybase(128),
	/**
	 * 表示SQLite数据库（256）。
	 */
	SQLite(256),
	/**
	 * 表示其它数据库类型(0)。
	 */
	Others(0);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	DBType(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return DBType
	 */
	public DBType parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (DBType s : DBType.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return DBType.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return DBType
	 */
	public static DBType parse(int v) {
		for (DBType s : DBType.values())
			if (s.getIntValue() == v) return s;
		return null;
	}
}

