/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

/**
 * 定义字段值校验相关校验js函数信息的类
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ItemValidator {
	/**
	 * 缺省构造器。
	 */
	public ItemValidator() {
	}

	private String m_validatorName = null; // 校验名称。
	private String m_jsFunctionName = null; // js校验实现函数名。
	private String m_errorMessage = null; // 字段值校验时的错误消息模板文本。
	private String m_complement = null; // 补充参数（以JSON字符串方式传递给js校验函数），可选。

	/**
	 * 返回校验名称。
	 * 
	 * <p>
	 * 比如：“不允许为空”、“必须输入整数数字”等都是合法的名称，<strong>但是名称不能重复。</strong>
	 * </p>
	 * 
	 * @return String
	 */
	public String getValidatorName() {
		return this.m_validatorName;
	}

	/**
	 * 设置校验名称。
	 * 
	 * @see ItemValidator#getValidatorName()
	 * 
	 * @param validatorName String
	 */
	public void setValidatorName(String validatorName) {
		this.m_validatorName = validatorName;
	}

	/**
	 * 返回js校验实现函数名。
	 * 
	 * <p>
	 * 比如：“validatingEmpty”、“validatingInt”等都是合法的名称，<strong>但是名称不能重复。</strong>
	 * </p>
	 * 
	 * @return String
	 */
	public String getJsFunctionName() {
		return this.m_jsFunctionName;
	}

	/**
	 * 设置js校验实现函数名。
	 * 
	 * @see ItemValidator#getJsFunctionName()
	 * 
	 * @param jsFunctionName String
	 */
	public void setJsFunctionName(String jsFunctionName) {
		this.m_jsFunctionName = jsFunctionName;
	}

	/**
	 * 返回字段值校验时的错误消息模板文本。
	 * 
	 * <p>
	 * 在配置中：
	 * </p>
	 * <p>
	 * 可以包含{name}表示当前被校验的字段名称。
	 * </p>
	 * <p>
	 * 可以包含{caption}表示当前被校验的字段标题文本。
	 * </p>
	 * <p>
	 * 比如配置为：<strong>对不起，您必须输入具体“{caption}”值！</strong>，则在校验类似标题文本为“文号”的字段值失败时实际校验失败时提示：<strong>对不起，您必须输入具体“文号”值！</strong>
	 * </p>
	 * 
	 * @return String
	 */
	public String getErrorMessage() {
		return this.m_errorMessage;
	}

	/**
	 * 设置字段值校验时的错误消息模板文本。
	 * 
	 * @see ItemValidator#getErrorMessage()
	 * @param errorMessage String
	 */
	public void setErrorMessage(String errorMessage) {
		this.m_errorMessage = errorMessage;
	}

	/**
	 * 返回补充参数（以JSON字符串方式传递给js校验函数），可选。
	 * 
	 * @return String
	 */
	public String getComplement() {
		return this.m_complement;
	}

	/**
	 * 设置补充参数（以JSON字符串方式传递给js校验函数），可选。
	 * 
	 * <p>
	 * 如：{param1:'value1',param2:2...}
	 * </p>
	 * 
	 * @param complement String
	 */
	public void setComplement(String complement) {
		this.m_complement = complement;
	}
}

