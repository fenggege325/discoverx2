/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.lang.reflect.Method;

import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 保存系统默认使用的资源的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class DefaultResourceConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private DefaultResourceConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static DefaultResourceConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return DefaultResourceConfig
	 */
	public static DefaultResourceConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new DefaultResourceConfig();
			}
		}
		return m_instance;
	}

	/**
	 * 根据资源对象的类信息获取此类资源在资源个数有多个时默认使用的资源的UNID。
	 * 
	 * @param clazz Class&lt;?&gt; 资源对象的类信息，必须
	 * @return String 如果没有定义默认资源信息，则返回null或空字符串。
	 */
	public String getDefaultResourceUNID(Class<?> clazz) {
		if (clazz == null) return null;
		String result = this.getValue(clazz.getName());
		if (result == null || result.length() == 0) {
			String dir = ResourceDescriptorConfig.getInstance().getResourceDirectory(clazz);
			if (dir != null && dir.length() > 0) result = this.getValue(dir);
		}
		return result;
	}

	/**
	 * 根据资源对象的类信息设置此类资源在资源个数有多个时默认使用的资源的UNID。
	 * 
	 * @param clazz Class&lt;?&gt; 资源对象的类信息，必须
	 * @param defaultResourceUNID String 要设置为默认资源的资源UNID。
	 * @return String 如果有设置原来的资源则返回之，否则返回null。
	 */
	public String setDefaultResource(Class<?> clazz, String defaultResourceUNID) {
		if (clazz == null) return null;
		ConfigEntry ce = this.getConfigEntry(clazz.getName());
		if (ce == null) {
			String dir = ResourceDescriptorConfig.getInstance().getResourceDirectory(clazz);
			if (dir != null && dir.length() > 0) ce = this.getConfigEntry(dir);
		}
		String result = ce.getValue();
		ce.setValue(defaultResourceUNID);
		this.save();
		return result;
	}

	/**
	 * 设置与指定资源同一类的多个资源中默认使用的资源为指定资源。
	 * 
	 * @param resource 要设置其为默认资源的资源对象，此对象必须是{@link com.tansuosoft.discoverx.model.Resource}的实现类。
	 * @return String 如果有设置原来的资源则返回之，否则返回null。
	 */
	public String setDefaultResource(Object resource) {
		if (resource == null || !ObjectUtil.checkInstance(resource, "com.tansuosoft.discoverx.model.Resource")) return null;
		Method m = ObjectUtil.getMethod(resource, "getUNID");
		if (m == null) return null;
		Object unid = ObjectUtil.getMethodResult(resource, m);
		if (unid == null) return null;
		return setDefaultResource(resource.getClass(), unid.toString());
	}
}

