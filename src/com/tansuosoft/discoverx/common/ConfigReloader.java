/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.io.File;
import java.util.List;

import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 配置文件对应的内容更改时重载对应配置文件对象的类。
 * 
 * @author coca@tensosoft.com
 */
public class ConfigReloader extends Agent {
	protected static final String RES_DIR = CommonConfig.getInstance().getResourcePath();

	/**
	 * 重载：实现功能。
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		List<GenericPair<Long, Config>> list = Config.instances;
		if (list == null || list.isEmpty()) return;
		String fn = null;
		File f = null;
		long lm = 0L;
		long llm = 0L;
		Config c = null;
		try {
			for (GenericPair<Long, Config> gp : list) {
				if (gp == null) continue;
				c = gp.getValue();
				llm = gp.getKey();
				if (c == null) continue;
				fn = RES_DIR + c.getClass().getSimpleName() + ".xml";
				f = new File(fn);
				if (!(f.isFile() && f.exists())) continue;
				lm = f.lastModified();
				if (lm <= llm) continue;
				System.out.println(String.format("%1$s 重载%2$s:%3$s(%4$s->%5$s)...", DateTime.getNowDTString(), c.getName(), fn, new DateTime(llm).toString(), new DateTime(lm).toString()));
				c.reload();
				gp.setKey(lm);
			}// for end
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
	}

	/**
	 * 重载：每10分钟执行一次。
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getPeriod()
	 */
	@Override
	public long getPeriod() {
		return 600L;
	}

	/**
	 * 重载：代理名称。
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getName()
	 */
	@Override
	public String getName() {
		return "配置文件更改自动重新加载定时代理";
	}

	/**
	 * 重载：10分钟后开始执行
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getInitialDelay()
	 */
	@Override
	public long getInitialDelay() {
		return 600L;
	}

	/**
	 * 重载：返回true。
	 * 
	 * @see com.tansuosoft.discoverx.common.Agent#getAbsolute()
	 */
	@Override
	public boolean getAbsolute() {
		return true;
	}

}

