/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 文件扩展名与MIME ContentType类型对照的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class MIMEContentTypeConfig extends Config {
	/**
	 * 找不到匹配扩展名的ContentType时用于再次查找的扩展名。
	 */
	protected static final String UNKNOWN_CT_EXT = "[unknown]";

	/**
	 * 找不到匹配扩展名的ContentType时默认返回的结果。
	 */
	public static final String DEFAULT_MIME_CONTENTTYPE = "application/octet-stream";

	/**
	 * 缺省构造器。
	 */
	private MIMEContentTypeConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static MIMEContentTypeConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return MIMEContentTypeConfig
	 */
	public static MIMEContentTypeConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new MIMEContentTypeConfig();
			}
		}
		return m_instance;
	}

	/**
	 * 获取文件名中扩展名对应的MIME ContentType类型。
	 * 
	 * @param fn String 文件名，必须包含半角点号和扩展名，如file.bat、.bat、c:\dir\file.bat、c:\file等均合法，如果此参数值为null或空串，则返回null。
	 * @param returnIfNoExt boolean 如果找不到扩展名，则返回此参数指定的结果。
	 * @return String 文件扩展名对应的MIME ContentType字符串，如果找不到文件扩展名对应的MIME ContentType，则返回“application/octet-stream”。
	 */
	public String check(String fn, String returnIfNoExt) {
		if (fn == null || fn.trim().length() == 0) return null;
		String ext = (com.tansuosoft.discoverx.util.FileHelper.getExtName(fn, null));
		if (ext == null) return returnIfNoExt;
		String ct = this.getValue(ext);
		if (ct == null || ct.length() == 0) {
			return DEFAULT_MIME_CONTENTTYPE;
		} else {
			return ct;
		}
	}

	/**
	 * 获取MIME类型名称对应的文件扩展名。
	 * 
	 * @param contentType
	 * @param returnIfNoFound
	 * @return 找到的扩展名，如果没找到则返回returnIfNoFound。
	 */
	public String checkExt(String contentType, String returnIfNoFound) {
		if (contentType == null || contentType.length() == 0) return returnIfNoFound;
		for (ConfigEntry ce : this.m_entries) {
			if (contentType.equalsIgnoreCase(ce.getValue())) return ce.getName();
		}
		return returnIfNoFound;
	}

	/**
	 * 获取fn指定的文件扩展名对应的mime类型的文件的流输出实现类全限定类名。
	 * 
	 * 具体流输出实现类的实现请参考{@link com.tansuosoft.discoverx.web.accessory.FileOutput}的说明。
	 * 
	 * @param fn
	 * @return String
	 */
	public String checkOutput(String fn) {
		if (fn == null || fn.trim().length() == 0) return null;
		String ext = (com.tansuosoft.discoverx.util.FileHelper.getExtName(fn, null));
		if (ext == null) return null;
		String outputType = this.getType(ext);
		return outputType;
	}

	protected static final String IMG_EXTS[] = { ".jpg", ".gif", ".png", ".bmp", ".tif", ".tiff", ".jpeg" };

	/**
	 * 通过fn指定的文件名判断并返回fn是否为常见的图片文件类型。
	 * 
	 * <p>
	 * 常见图片文件包括：“jpg、gif、png、bmp、tif”等。
	 * </p>
	 * 
	 * @param fn
	 * @return
	 */
	public static boolean isImage(String fn) {
		if (fn == null || fn.isEmpty()) return false;
		String l = fn.toLowerCase();
		for (String s : IMG_EXTS) {
			if (l.toLowerCase().endsWith(s)) return true;
		}
		return false;
	}
}

