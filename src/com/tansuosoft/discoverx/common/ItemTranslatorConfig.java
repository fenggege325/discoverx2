/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import java.util.List;

import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 保存字段值转换相关转换js函数信息的配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ItemTranslatorConfig extends Config {
	/**
	 * 缺省构造器。
	 */
	private ItemTranslatorConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static ItemTranslatorConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return ItemTranslatorConfig
	 */
	public static ItemTranslatorConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new ItemTranslatorConfig();
			}
		}
		return m_instance;
	}

	private List<ItemTranslator> m_itemTranslators = null; // 配置中包含的ItemTranslator集合。

	/**
	 * 返回配置中包含的ItemTranslator集合。
	 * 
	 * @see ItemTranslator
	 * 
	 * @return List<ItemTranslator>
	 */
	public List<ItemTranslator> getItemTranslators() {
		return this.m_itemTranslators;
	}

	/**
	 * 设置配置中包含的ItemTranslator集合。
	 * 
	 * @see ItemTranslator
	 * 
	 * @param itemTranslators List<ItemTranslator>
	 */
	public void setItemTranslators(List<ItemTranslator> itemTranslators) {
		this.m_itemTranslators = itemTranslators;
	}

	/**
	 * 获取jsFunction指定函数名包含的ItemTranslator对象，如果找不到则返回null。
	 * 
	 * @param jsFunction
	 * @return ItemTranslator
	 */
	public ItemTranslator getItemTranslator(String jsFunction) {
		if (this.m_itemTranslators == null || jsFunction == null || jsFunction.length() == 0) return null;
		for (ItemTranslator x : this.m_itemTranslators) {
			if (jsFunction.equalsIgnoreCase(x.getJsFunctionName())) return x;
		}
		return null;
	}
}

