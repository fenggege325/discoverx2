/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

/**
 * 定义字段值转换相关转换js函数信息的类
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ItemTranslator {
	/**
	 * 缺省构造器。
	 */
	public ItemTranslator() {
	}

	private String m_translatorName = null; // 转换名称。
	private String m_jsFunctionName = null; // js转换实现函数名。
	private String m_complement = null; // 补充参数（以JSON字符串方式传递给js转换函数），可选。

	/**
	 * 返回转换名称。
	 * 
	 * <p>
	 * 比如：“转换为大写”、“转换为小写”等都是合法的名称，<strong>但是名称不能重复。</strong>
	 * </p>
	 * 
	 * @return String
	 */
	public String getTranslatorName() {
		return this.m_translatorName;
	}

	/**
	 * 设置转换名称。
	 * 
	 * @see ItemTranslator#getTranslatorName()
	 * @param translatorName String
	 */
	public void setTranslatorName(String translatorName) {
		this.m_translatorName = translatorName;
	}

	/**
	 * 返回js校验实现函数名。
	 * 
	 * <p>
	 * 比如：“translatingUppercase”、“translatingLowercase”等都是合法的名称，<strong>但是名称不能重复。</strong>
	 * </p>
	 * 
	 * @return String
	 */
	public String getJsFunctionName() {
		return this.m_jsFunctionName;
	}

	/**
	 * 设置js校验实现函数名。
	 * 
	 * @see ItemTranslator#getJsFunctionName()
	 * 
	 * @param jsFunctionName String
	 */
	public void setJsFunctionName(String jsFunctionName) {
		this.m_jsFunctionName = jsFunctionName;
	}

	/**
	 * 返回补充参数（以JSON字符串方式传递给js转换函数），可选。
	 * 
	 * @return String
	 */
	public String getComplement() {
		return this.m_complement;
	}

	/**
	 * 设置补充参数（以JSON字符串方式传递给js转换函数），可选。
	 * 
	 * <p>
	 * 如：{param1:'value1',param2:2...}
	 * </p>
	 * 
	 * @param complement String
	 */
	public void setComplement(String complement) {
		this.m_complement = complement;
	}
}

