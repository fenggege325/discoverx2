/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common.event;

/**
 * 表示所有事件包含的事件参数对应的基类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class EventArgs {
	/**
	 * 缺省构造器。
	 */
	public EventArgs() {
	}

	private String m_eventHandlerType = null; // 事件类型。
	private Object m_tag = null; // 事件参数包含的额外数据。

	/**
	 * 返回事件类型。
	 * 
	 * <p>
	 * 返回值结果说明请参考{@link EventHandler}类中的事件处理程序类型常数定义。
	 * </p>
	 * 
	 * @return String
	 */
	public String getEventHandlerType() {
		return this.m_eventHandlerType;
	}

	/**
	 * 设置事件类型。
	 * 
	 * @param eventHandlerType String
	 */
	protected void setEventHandlerType(String eventHandlerType) {
		this.m_eventHandlerType = eventHandlerType;
	}

	/**
	 * 检查目标事件处理程序类型是否eht指定的类型。
	 * 
	 * @param eht
	 * @return boolean
	 */
	public boolean checkEventHandlerType(String eht) {
		if (eht == null && m_eventHandlerType == null) return true;
		if ((m_eventHandlerType == null && eht != null) || (m_eventHandlerType != null && eht == null)) return false;
		return (m_eventHandlerType.indexOf(eht) >= 0);
	}

	/**
	 * 返回事件参数包含的额外数据。
	 * 
	 * @return Object
	 */
	public Object getTag() {
		return this.m_tag;
	}

	/**
	 * 设置事件参数包含的额外数据，可选。
	 * 
	 * @param tag Object
	 */
	public void setTag(Object tag) {
		this.m_tag = tag;
	}

}

