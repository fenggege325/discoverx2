/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common.event;

/**
 * 描述一个事件处理程序相关信息的类
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class EventHandlerInfo implements Cloneable, Comparable<EventHandlerInfo> {

	/**
	 * 缺省构造器。
	 */
	public EventHandlerInfo() {
	}

	/**
	 * 接收相关属性值的构造器。
	 * 
	 * @param eventHandler
	 * @param eventHandlerType
	 * @param eventHandlerName
	 * @param sort
	 */
	public EventHandlerInfo(String eventHandler, String eventHandlerType, String eventHandlerName, int sort) {
		this.m_eventHandler = eventHandler;
		this.m_eventHandlerType = eventHandlerType;
		this.m_eventHandlerName = eventHandlerName;
		this.m_sort = sort;
	}

	private String m_eventHandler = null; // 事件处理程序对应的类名，必须。
	private String m_eventHandlerType = null; // 事件处理程序对应的类型，必须。
	private String m_eventHandlerName = null; // 事件处理程序对应的名称，可选。
	private int m_sort = 0; // 事件处理程序对应的排序号，可选。

	/**
	 * 返回事件处理程序实现类的全限定类名。
	 * 
	 * @return String
	 */
	public String getEventHandler() {
		return this.m_eventHandler;
	}

	/**
	 * 设置事件处理程序实现类的全限定类名，必须。
	 * 
	 * @param eventHandler String。
	 */
	public void setEventHandler(String eventHandler) {
		this.m_eventHandler = eventHandler;
	}

	/**
	 * 返回事件处理程序对应的名称，可选。
	 * 
	 * @return String
	 */
	public String getEventHandlerName() {
		return this.m_eventHandlerName;
	}

	/**
	 * 设置事件处理程序对应的名称，可选。
	 * 
	 * @param eventHanderName String
	 */
	public void setEventHandlerName(String eventHandlerName) {
		this.m_eventHandlerName = eventHandlerName;
	}

	/**
	 * 返回事件处理程序对应的类型，必须。
	 * 
	 * <p>
	 * 只有事件处理程序类型匹配的事件才会被执行，比如保存完成事件的类型为“saved”，那么只有getEventHandlerType返回也为“saved”时其事件处理程序才会触发执行。
	 * <p>
	 * <p>
	 * 系统支持的事件类型请参考以下说明：
	 * <ul>
	 * <li>construct：资源开始架构（新建）时；</li>
	 * <li>constructed：资源架构（新建）完成时；</li>
	 * <li>open：资源打开前；</li>
	 * <li>opened：资源打开完成；</li>
	 * <li>save：资源保存前；</li>
	 * <li>saved：资源保存完成；</li>
	 * <li>delete：资源删除前；</li>
	 * <li>deleted：资源删除完成；</li>
	 * <li>close：资源关闭前；</li>
	 * <li>closed：资源关闭完成；</li>
	 * <li>documentsetstatebegin：文档（资源）设置状态前；</li>
	 * <li>documentsetstateend：文档（资源）设置状态完成；</li>
	 * <li>documentitemvaluechange：文档（资源）设置字段值前；</li>
	 * <li>documentitemvaluechanged：文档（资源）设置字段值完成；</li>
	 * <li>userlogin：用户登录完成；</li>
	 * <li>userlogout：用户注销完成；</li>
	 * <li>functionexecutionbegin：功能扩展函数执行前；</li>
	 * <li>functionexecutionend：功能扩展函数执行后；</li>
	 * <li>workflowtransactionbegin：流程事务开始；</li>
	 * <li>workflowtransactionend：流程事务结束；</li>
	 * <li>workflowdataadded：追加了流程控制数据；</li>
	 * <li>workflowdatadeleted：删除了流程控制数据；</li>
	 * <li>workflowdatamarkeddone：流程控制数据被标记为完成；</li>
	 * <li>workflowdatalevelchanged：流程控制数据级别变更；</li>
	 * <li>onlogin：用户登录完成。</li>
	 * </ul>
	 * </p>
	 * 
	 * @see EventHandler
	 * @return String
	 */
	public String getEventHandlerType() {
		return this.m_eventHandlerType;
	}

	/**
	 * 设置事件处理程序对应的类型，必须。
	 * 
	 * @param eventHandlerType String
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public void setEventHandlerType(String eventHandlerType) {
		this.m_eventHandlerType = eventHandlerType;
	}

	/**
	 * 返回事件处理程序对应的排序号。
	 * 
	 * <p>
	 * 通常情况下，系统将按照排序号顺序触发执行对应的事件处理程序，但是通常不建议事件处理程序严格依赖排序号来确定触发的先后顺序。
	 * </p>
	 * 
	 * @return int
	 */
	public int getSort() {
		return this.m_sort;
	}

	/**
	 * 设置事件处理程序对应的排序号。
	 * 
	 * @param sort int
	 */
	public void setSort(int sort) {
		this.m_sort = sort;
	}

	/**
	 * 获取字符串表示方式，返回格式为：“sort[不足3位补零]/eventHandlerType/eventHandler/eventHanderName”。
	 * 
	 * @return String
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.m_sort >= 100 ? "" : (this.m_sort >= 10 ? "0" : "00")).append(this.m_sort).append("/");
		sb.append(this.m_eventHandlerType).append("/");
		sb.append(this.m_eventHandler).append("/");
		sb.append(this.m_eventHandlerName);
		return sb.toString();
	}

	/**
	 * 判断两个EventHandlerInfo实例是否相等。
	 * 
	 * <p>
	 * 只有两个实例的eventHandlerType与eventHandlerClassName返回值相等时才相等。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!(obj instanceof EventHandlerInfo)) return false;
		EventHandlerInfo ehi = (EventHandlerInfo) obj;
		String thisValue = String.format("%1$s%2$s", this.getEventHandler(), this.getEventHandlerType());
		String thatValue = String.format("%1$s%2$s", ehi.getEventHandler(), ehi.getEventHandlerType());
		return thisValue.equalsIgnoreCase(thatValue);
	}

	/**
	 * 
	 * 重载：实现功能。
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		EventHandlerInfo x = new EventHandlerInfo();

		x.setEventHandler(this.getEventHandler());
		x.setEventHandlerName(this.getEventHandlerName());
		x.setEventHandlerType(this.getEventHandlerType());
		x.setSort(this.getSort());

		return x;
	}

	/**
	 * 重载：先按排序号比较，然后按类全名比较。
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(EventHandlerInfo o) {
		int result = 0;
		if (o == null) return 1;
		result = this.getSort() - o.getSort();
		if (result == 0) {
			if (this.getEventHandler() == null) { return (o.getEventHandler() == null ? 0 : -1); }
			result = this.m_eventHandler.compareToIgnoreCase(o.getEventHandler());
			if (result == 0) {
				result = this.hashCode() - o.hashCode();
				result = (result == 0 ? 0 : result > 0 ? 1 : -1);
			}
		}
		return result;
	}
}
