/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common.event;

/**
 * 表示所有可以触发事件的对象的接口。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public interface EventSource {

	/**
	 * 增加一个指定类型的事件处理程序。
	 * 
	 * @param String eventHandlerType 事件处理程序类型名称。
	 * @param handler EventHandler 事件处理程序对象实例。
	 */
	public void addHandler(String eventHandlerType, EventHandler handler);

	/**
	 * 触发指定类型事件处理程序。
	 * 
	 * @param sender 触发事件的源对象。
	 * @param String eventHandlerType 要触发的事件处理程序类型。
	 * @param e EventArgs 事件参数。
	 */
	public void callEventHandler(Object sender, String eventHandlerType, EventArgs e);

	/**
	 * 触发指定类型事件处理程序（使用当前对象即“this”引用作为触发事件的源对象）。
	 * 
	 * @param String eventHandlerType 要触发的事件处理程序类型。
	 * @param e EventArgs 事件参数。
	 */
	public void callEventHandler(String eventHandlerType, EventArgs e);
}

