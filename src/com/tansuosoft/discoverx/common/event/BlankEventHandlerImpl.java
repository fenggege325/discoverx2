/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common.event;

import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 不执行任何操作的空白事件处理程序实现类。
 * 
 * @author coca@tensosoft.com
 */
public class BlankEventHandlerImpl implements EventHandler {

	/**
	 * 重载：实现功能。
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		FileLogger.debug("触发了不执行任何操作的事件！");
	}
}

