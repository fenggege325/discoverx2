/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common.event;

/**
 * 表示所有可以接收事件通知并执行相应处理的事件处理类都必须实现的接口。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public interface EventHandler {
	/**
	 * 系统内置事件处理程序类型常数：construct（新建资源初始化完成后）。
	 * 
	 * <p>
	 * 此事件只针对文档资源触发。
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_INITIALIZE = "construct";
	/**
	 * 系统内置事件处理程序类型常数：constructed（新建资源初始化完成后）。
	 * 
	 * <p>
	 * 对于非文档资源表示新建初始化完成后触发，对于文档资源则表示新建文档资源初始化完成且加入文档缓存后触发。
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_INITIALIZED = "constructed";
	/**
	 * 系统内置事件处理程序类型常数：open（资源打开前）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_OPEN = "open";
	/**
	 * 系统内置事件处理程序类型常数：opened（资源成功打开后）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_OPENED = "opened";
	/**
	 * 系统内置事件处理程序类型常数：save（资源保存前）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_SAVE = "save";
	/**
	 * 系统内置事件处理程序类型常数：saved（资源成功保存后）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_SAVED = "saved";
	/**
	 * 系统内置事件处理程序类型常数：delete（资源彻底删除前）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_DELETE = "delete";
	/**
	 * 系统内置事件处理程序类型常数：deleted（资源彻底删除成功后）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_DELETED = "deleted";
	/**
	 * 系统内置事件处理程序类型常数：close（资源关闭前）。
	 * 
	 * <p>
	 * 只有在单击系统内置关闭按钮时才会触发且仅针对文档资源。
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_CLOSE = "close";
	/**
	 * 系统内置事件处理程序类型常数：closed（资源关闭后）。
	 * 
	 * <p>
	 * 只有在单击系统内置关闭按钮时才会触发且仅针对缓存的配置资源和文档资源。
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_CLOSED = "closed";
	/**
	 * 系统内置事件处理程序类型常数：documentsetstatebegin（设置文档状态开始前）。
	 * 
	 * <p>
	 * 只有通过调用{@link com.tansuosoft.discoverx.web.operation.SetDocumentState}操作设置文档状态时才会触发！
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_DOCUMENTSETSTATEBEGIN = "documentsetstatebegin";
	/**
	 * 系统内置事件处理程序类型常数：documentsetstateend（设置文档状态成功后）。
	 * 
	 * <p>
	 * 只有通过调用{@link com.tansuosoft.discoverx.web.operation.SetDocumentState}操作设置文档状态时才会触发！
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_DOCUMENTSETSTATEAFTER = "documentsetstateend";
	/**
	 * 系统内置事件处理程序类型常数：documentitemvaluechange（执行更改字段值前）。
	 * 
	 * <p>
	 * 只有通过系统默认的“保存”操作保存文档时才会触发！
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_DOCUMENTITEMVALUECHANGE = "documentitemvaluechange";
	/**
	 * 系统内置事件处理程序类型常数：documentitemvaluechanged（字段值更改且发生变化后）。
	 * 
	 * <p>
	 * 只有通过系统默认的“保存”操作保存文档时才会触发！
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_DOCUMENTITEMVALUECHANGED = "documentitemvaluechanged";
	/**
	 * 系统内置事件处理程序类型常数：userlogin（用户登录完成）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_USERLOGIN = "userlogin";
	/**
	 * 系统内置事件处理程序类型常数：userlogout（用户注销完成）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_USERLOGOUT = "userlogout";
	/**
	 * 系统内置事件处理程序类型常数：functionexecutionbegin（功能扩展函数执行前）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_FUNCTIONEXECUTIONBEGIN = "functionexecutionbegin";
	/**
	 * 系统内置事件处理程序类型常数：functionexecutionend（功能扩展函数执行后）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_FUNCTIONEXECUTIONAFTER = "functionexecutionend";
	/**
	 * 系统内置事件处理程序类型常数：workflowtransactionbegin（流程事务开始）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_WORKFLOWTRANSACTIONBEGIN = "workflowtransactionbegin";
	/**
	 * 系统内置事件处理程序类型常数：workflowtransactionend（流程事务结束）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_WORKFLOWTRANSACTIONEND = "workflowtransactionend";

	/**
	 * 系统内置事件处理程序类型常数：workflowdataadded（追加了流程控制数据）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_WORKFLOWDATAADDED = "workflowdataadded";

	/**
	 * 系统内置事件处理程序类型常数：workflowdatadeleted（删除了流程控制数据）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_WORKFLOWDATADELETED = "workflowdatadeleted";

	/**
	 * 系统内置事件处理程序类型常数：workflowdatamarkeddone（流程控制数据被标记为完成）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_WORKFLOWDATAMARKEDDONE = "workflowdatamarkeddone";

	/**
	 * 系统内置事件处理程序类型常数：workflowdatalevelchanged（流程控制数据级别变更）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_WORKFLOWDATALEVELCHANGED = "workflowdatalevelchanged";

	/**
	 * 系统内置事件处理程序类型常数：onlogin（用户登录成功）。
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_ONLOGIN = "onlogin";
	/**
	 * 系统内置事件处理程序类型常数：onsysteminit（第一次启动系统）。
	 * 
	 * <p>
	 * 必须将第一次启动系统事件的处理程序信息配置在“EventHandlerInfo.xml”配置文件中（须在在第一次启动前先配置好）。
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_ONSYSTEMINIT = "onsysteminit";

	/**
	 * 系统内置事件处理程序类型常数：onsystemstarted（系统启动完成）。
	 * 
	 * <p>
	 * 必须将第一次启动系统事件的处理程序信息配置在“EventHandlerInfo.xml”配置文件中。
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_ONSYSTEMSTARTED = "onsystemstarted";

	/**
	 * 系统内置事件处理程序类型常数：onsystemshutdown（系统关闭）。
	 * 
	 * <p>
	 * 必须将第一次启动系统事件的处理程序信息配置在“EventHandlerInfo.xml”配置文件中。
	 * </p>
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static final String EHT_ONSYSTEMSHUTDOWN = "onsystemshutdown";

	/**
	 * 执行事件处理程序。
	 * 
	 * @param sender Object 触发事件的源对象（不同的事件处理其源对象可能不同，在某些时候，如果需要具体类型的源对象可以对sender使用类型转换）。
	 * @param e EventArgs 事件参数（不同的事件可能为不同类型参数，但是他们都继承自EventArgs，需要使用具体类型参数时可以转换到具体类型）。
	 */
	public void handle(Object sender, EventArgs e);
}

