/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.tansuosoft.discoverx.util.GenericPair;

/**
 * 用于触发事件处理程序的类。
 * 
 * <p>
 * 此类是{@link EventSource}接口的一个通用默认实现。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class EventSourceImpl implements EventSource {

	/**
	 * 缺省构造器。
	 */
	public EventSourceImpl() {
	}

	private HashMap<String, Integer> m_eventHandlers = new HashMap<String, Integer>();
	private List<GenericPair<String, EventHandler>> m_realEventHandlers = new ArrayList<GenericPair<String, EventHandler>>();

	/**
	 * 重载addHandler
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventSource#addHandler(java.lang.String, com.tansuosoft.discoverx.common.event.EventHandler)
	 */
	public void addHandler(String eventHandlerType, EventHandler handler) {
		if (handler == null) return;
		String clsName = handler.getClass().getName();
		String key = eventHandlerType + clsName;
		if (m_eventHandlers.containsKey(key)) return;
		this.m_eventHandlers.put(key, 1);
		m_realEventHandlers.add(new GenericPair<String, EventHandler>(eventHandlerType, handler));
	}

	/**
	 * 重载callEventHandler
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventSource#callEventHandler(java.lang.Object, java.lang.String,
	 *      com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	public void callEventHandler(Object sender, String eventHandlerType, EventArgs e) {
		if (eventHandlerType == null || eventHandlerType.length() == 0 || this.m_realEventHandlers.isEmpty()) return;
		String evt = null;
		EventHandler handler = null;
		if (e != null) e.setEventHandlerType(eventHandlerType);
		for (GenericPair<String, EventHandler> x : m_realEventHandlers) {
			if (x == null) continue;
			evt = x.getKey();
			handler = x.getValue();
			if (evt == null || handler == null) continue;
			if (eventHandlerType == null || !eventHandlerType.equalsIgnoreCase(evt)) continue;
			handler.handle(sender, e);
		}
	}

	/**
	 * 重载callEventHandler
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventSource#callEventHandler(java.lang.String, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	public void callEventHandler(String eventHandlerType, EventArgs e) {
		callEventHandler(this, eventHandlerType, e);
	}
}

