/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common.event;

import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.util.Instance;

/**
 * 用于注册配置的事件处理程序的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class EventRegister {

	/**
	 * 注册事件处理程序信息列表对应的指定类型的事件处理程序。
	 * 
	 * @param source EventSource 注册的事件触发源对象。
	 * @param eventHandlerType 事件处理程序类型（多个用半角逗号分隔），“*”表示注册所有事件处理程序，null或空字符串则不注册任何事件处理程序。
	 * @param eventHandlerInfoList List&lt;EventHandlerInfo&gt;
	 * 
	 * @see EventHandlerInfo#getEventHandlerType()
	 */
	public static void registerEventHandler(EventSource source, String eventHandlerType, List<EventHandlerInfo> eventHandlerInfoList) {
		if (source == null || eventHandlerType == null || eventHandlerType.length() == 0 || eventHandlerInfoList == null || eventHandlerInfoList.isEmpty()) return;
		Collections.sort(eventHandlerInfoList);
		String evt = eventHandlerType.toLowerCase();
		boolean registerAllFlag = (evt.equals("*") ? true : false);
		EventHandler handler = null;
		String getEvt = null;
		for (EventHandlerInfo x : eventHandlerInfoList) {
			if (x == null) continue;
			getEvt = x.getEventHandlerType();
			if (getEvt == null || getEvt.length() == 0) continue;
			if (!registerAllFlag) {
				if (evt.indexOf(getEvt.toLowerCase()) < 0) continue;
			}
			Object obj = Instance.newInstance(x.getEventHandler());
			if (obj == null || !(obj instanceof EventHandler)) continue;
			handler = (EventHandler) obj;
			source.addHandler(getEvt, handler);
		}
	}
}

