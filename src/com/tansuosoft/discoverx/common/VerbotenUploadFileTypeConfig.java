/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.common;

import com.tansuosoft.discoverx.util.serialization.XmlDeserializer;

/**
 * 禁止上传的文件的扩展名配置类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class VerbotenUploadFileTypeConfig extends Config {

	/**
	 * 缺省构造器。
	 */
	private VerbotenUploadFileTypeConfig() {
		String location = String.format("%s%s.xml", CommonConfig.getInstance().getResourcePath(), this.getClass().getSimpleName());
		XmlDeserializer deserializer = new XmlDeserializer();
		deserializer.setTarget(this);
		deserializer.deserialize(location, this.getClass());
	}

	private static VerbotenUploadFileTypeConfig m_instance = null;
	private static Object m_lock = new Object();

	/**
	 * 获取此对象的唯一实例。
	 * 
	 * @return VerbotenUploadFileTypeConfig
	 */
	public static VerbotenUploadFileTypeConfig getInstance() {
		synchronized (m_lock) {
			if (m_instance == null) {
				m_instance = new VerbotenUploadFileTypeConfig();
			}
		}
		return m_instance;
	}

	/**
	 * 检查文件名对应的类型是否允许上传。
	 * 
	 * @param fn String 文件名，必须包含半角点号和扩展名，如file.bat、.bat、c:\dir\file.bat、c:\file等均合法，如果此参数值为null或空串，则返回false。
	 * @param returnIfNoExt boolean 如果找不到扩展名，则返回此参数指定的结果。
	 * @return 返回true表示可以上传，返回false表示不允许上传。
	 */
	public boolean check(String fn, boolean returnIfNoExt) {
		if (fn == null || fn.trim().length() == 0) return false;
		String ext = (com.tansuosoft.discoverx.util.FileHelper.getExtName(fn, null));
		if (ext == null) return returnIfNoExt;
		String ct = this.getValue(ext);
		if (ct != null && ct.length() > 0) return false;
		else return true;
	}
}

