/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 审批方式（参与者在流程某个流程环节的处理过程中的审批方式）枚举值。
 * 
 * @author coca@tansuosoft.cn
 */
public enum Participation implements EnumBase {
	/**
	 * 单人审批（1）。
	 */
	Single(1),
	/**
	 * （多人）并行审批（2）。
	 */
	Parallel(2),
	/**
	 * （多人）串行（顺序）审批（4）。
	 */
	Series(4),
	/**
	 * （多人）优先审批（优先抢占式审批）（8）。
	 * 
	 * <p>
	 * 优先审批指在选择的审批人中，如果某个人审批了，那么整个审批就完成了。
	 * </p>
	 */
	Priority(8),
	/**
	 * 用户自定义（0）。
	 */
	Custom(0);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	Participation(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return Participation
	 */
	public Participation parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (Participation s : Participation.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return Participation.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return Participation
	 */
	public static Participation parse(int v) {
		for (Participation s : Participation.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

	/**
	 * 返回指定审批方式对应的具体名称。
	 * 
	 * @param p {@link Participation}
	 * @param defaultUnknownName 如果没有提供有效审批方式则返回此结果。
	 * @return String
	 */
	public static String getName(Participation p, String defaultUnknownName) {
		if (p == null) return defaultUnknownName;
		switch (p) {
		case Single:
			return "单人审批";
		case Parallel:
			return "并行审批";
		case Series:
			return "顺序审批";
		case Priority:
			return "优先审批";
		case Custom:
			return "自定义";
		default:
			return defaultUnknownName;
		}
	}
}

