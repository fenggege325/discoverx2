/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.tansuosoft.discoverx.bll.SessionContext;
import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.function.ExpressionEvaluator;
import com.tansuosoft.discoverx.common.CommonConfig;
import com.tansuosoft.discoverx.common.event.EventCategory;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.common.event.EventRegister;
import com.tansuosoft.discoverx.common.event.EventSourceImpl;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentOthersInfoQuery;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.Parameter;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.PersistenceState;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.SecurityLevel;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.GenericPair;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringPair;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.event.WorkflowDataEventArgs;
import com.tansuosoft.discoverx.workflow.event.WorkflowDataLevelChangedEventArgs;
import com.tansuosoft.discoverx.workflow.event.WorkflowEventArgs;
import com.tansuosoft.discoverx.workflow.event.WorkflowStateRecorder;
import com.tansuosoft.discoverx.workflow.transaction.IdleTransaction;
import com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction;

/**
 * 表示获取流程运行时（流程实例）信息和控制其运行状态的流程运行时类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowRuntime extends EventSourceImpl {
	/**
	 * 供流程运行时对象内部使用的流程控制数据包装类。
	 * 
	 * @author coca@tansuosoft.cn
	 */
	class WFDataWrapper {
		private WFData wfdata = null;
		private PersistenceState persistenceState = null;
		private int securityCode = 0;
		protected int oldLevel = -1;
		protected int oldSubLevel = -1;

		/**
		 * 接收必要属性的构造器。
		 * 
		 * @param wfdata
		 * @param persistenceState
		 * @param securityCode
		 */
		protected WFDataWrapper(WFData wfdata, PersistenceState persistenceState, int securityCode) {
			this.wfdata = wfdata;
			this.persistenceState = persistenceState;
			this.securityCode = securityCode;
		}

		/**
		 * 获取{@link WFData}。
		 * 
		 * @return
		 */
		public WFData getWFData() {
			return wfdata;
		}

		/**
		 * 获取{@link WFData}对应的持久化状态{@link PersistenceState}。
		 * 
		 * @return
		 */
		public PersistenceState getPersistenceState() {
			return persistenceState;
		}

		/**
		 * 获取导致{@link WFData}变更的参与者的安全编码。
		 * 
		 * @return
		 */
		public int getSecurityCode() {
			return securityCode;
		}

	}

	private Object m_lock = new Object();
	private Document m_document = null; // 当前正在处理的文档。
	private int m_instance = 0; // 当前流程实例号。
	private int m_parentInstance = 0; // 当前流程父实例号。
	private Activity m_currentActivity = null; // 当前实例对应的流程的当前环节。
	private Workflow m_currentWorkflow = null; // 当前实例对应的流程。
	private Transaction m_transaction = null; // 当前事务。
	private WorkflowForm m_workflowForm = null; // 用户交互接口提供的信息。
	private int m_activityCount = 0; // 环节个数。
	// 用于保存经历过的所有环节信息的Map，其中String表示经历过的环节UNID，Integer表示环节经历的次数（从0开始），StringPair的key和value分别表示环节开始时间和结束时间。
	// 如果一个环节经过多次，则Integer越小的时间越靠近当前。
	private Map<String, Map<Integer, StringPair>> m_activityTraversalMap = new HashMap<String, Map<Integer, StringPair>>();
	// 所有访问流程运行时的用户对应的用户自定义会话信息，key中的Long值为线程id，GenericPair的key为会话引用次数。
	private Map<Long, GenericPair<Integer, Session>> m_sessions = new HashMap<Long, GenericPair<Integer, Session>>();
	private List<WFData> m_wfdata = null; // 当前正在处理的文档对应的所有流程控制数据。
	private List<WFDataWrapper> m_changedWFData = null; // 按先后顺序记录的有变动的控制数据队列。
	private static final String LOGS_PARAM_NAME = "_$$tobesavedlogs";
	private static final String LOG_INSERTER_IMPL = "com.tansuosoft.discoverx.dao.impl.LogInserter";

	/**
	 * 返回当前文档绑定的所有未删除的流程控制数据列表。
	 * 
	 * <p>
	 * 通常情况下，控制数据列表均按照发生的时间顺序倒叙排序方式返回。
	 * </p>
	 * <p>
	 * 在事务处理过程中调用本方法时由于控制数据可能会被事务所改动，因此每次调用返回的结果可能会不同。
	 * </p>
	 * <p>
	 * 向返回的列表中添加或删除控制数据不会影响影响本对象内部的控制数据内容，其余返回控制数据列表的方法也遵循同样的规则，应使用{@link #addWFData(WFData)}、{@link #removeWFData(WFData)}等方法增删控制数据。<br/>
	 * 直接通过添加数据库记录的方式添加的控制数据由于缓存的原因，可能要下次从数据库读取文档时才会起作用。
	 * </p>
	 * 
	 * @return List&lt;WFData&gt;
	 */
	public List<WFData> getWFData() {
		List<WFData> result = new ArrayList<WFData>();
		for (WFData x : this.m_wfdata) {
			if (x == null || this.getPersistenceState(x) == PersistenceState.Delete) continue;
			result.add(x);
		}
		return result;
	}

	/**
	 * 返回当前文档绑定的指定环节下的所有未删除的流程控制数据列表。
	 * 
	 * <p>
	 * 在事务处理过程中调用本方法时由于控制数据可能会被事务所改动，因此每次调用返回的结果可能会不同。
	 * </p>
	 * 
	 * @param activityUNID 必须，表示环节UNID。
	 * @param traversalIndex 必须，表示环节的流转次数索引（有效值从0到{@link WorkflowRuntime#getActivityTraversalCount(String)}-1）
	 * @return
	 */
	public List<WFData> getWFData(String activityUNID, int traversalIndex) {
		if (activityUNID == null || activityUNID.length() == 0) return null;
		List<WFData> result = new ArrayList<WFData>();
		String startDT = this.getActivityStartDateTime(activityUNID, traversalIndex);
		if (startDT == null) startDT = "";
		String endDT = this.getActivityEndDateTime(activityUNID, traversalIndex);
		if (endDT == null) endDT = DateTime.getNowDTString();
		String accomplishedDT = null;
		for (WFData x : this.m_wfdata) {
			if (x == null || this.getPersistenceState(x) == PersistenceState.Delete || !activityUNID.equalsIgnoreCase(x.getActivity()) || !this.getCurrentWorkflow().getUNID().equalsIgnoreCase(x.getWorkflow())) continue;
			accomplishedDT = (x.getDone() ? x.getAccomplished() : x.getCreated());
			if (startDT.compareToIgnoreCase(x.getCreated()) <= 0 && endDT.compareToIgnoreCase(accomplishedDT) >= 0) {
				result.add(x);
			}
		}
		return result;
	}

	/**
	 * 返回当前环节对应的所有未完成、未删除的流程控制数据列表。
	 * 
	 * <p>
	 * 在事务处理过程中调用本方法时由于控制数据可能会被事务所改动，因此每次调用返回的结果可能会不同。
	 * </p>
	 * 
	 * @return List&lt;WFData&gt;
	 */
	public List<WFData> getContextWFData() {
		List<WFData> result = new ArrayList<WFData>();
		for (WFData x : this.m_wfdata) {
			if (x == null || !isContext(x)) continue;
			if (this.getPersistenceState(x) == PersistenceState.Delete || x.getDone()) continue;
			result.add(x);
		}
		return result;
	}

	/**
	 * 返回当前环节对应的所有未完成、未删除且级别为指定级别的流程控制数据列表。
	 * 
	 * <p>
	 * 在事务处理过程中调用本方法时由于控制数据可能会被事务所改动，因此每次调用返回的结果可能会不同。
	 * </p>
	 * 
	 * @return List&lt;WFData&gt;
	 */
	public List<WFData> getContextWFData(WFDataLevel level) {
		List<WFData> result = new ArrayList<WFData>();
		for (WFData x : this.m_wfdata) {
			if (x == null || !x.checkLevel(level) || !isContext(x)) continue;
			if (x.getDone() || this.getPersistenceState(x) == PersistenceState.Delete) continue;
			result.add(x);
		}
		return result;
	}

	/**
	 * 在当前环节的控制数据中查找并返回current指定的控制数据对应的第一个上级控制数据。
	 * 
	 * <p>
	 * 比如current为当前审批人对应的控制数据，那么：
	 * <ul>
	 * <li>如果current为代理审批用户，则会返回其委托人对应的审批用户控制数据。</li>
	 * <li>如果current为转发目标人对应的审批用户，则会返回其委托人对应的审批用户控制数据。</li>
	 * </ul>
	 * 在使用返回结果时，需注意系统可能不一定返回预定的结果，应在使用返回值时判断null和必要的主辅控制数据类型。<br/>
	 * 比如期望获取代理审批人对应的委托审批人时，可能返回的结果不是其委托人，此时应判断返回结果的控制数据类型是否为普通审批人。
	 * </p>
	 * <p>
	 * 在事务处理过程中调用本方法时由于控制数据可能会被事务所改动，因此每次调用返回的结果可能会不同。
	 * </p>
	 * 
	 * @param current 表示要查找其上级控制数据的源控制数据，必须。
	 * @param prevFound 表示前一个已经找到的但是不符合要求的{@link WFData}对象，可以为null（通常在第一次调用时传入null）。
	 * @return WFData 返回第一个找到的上级控制数据，也可能不存在而返回null。
	 */
	public WFData getParentWFData(WFData current, WFData prevFound) {
		if (current == null) return null;
		for (WFData x : this.m_wfdata) {
			if (x == null || !isContext(x) || x.getUNID().equalsIgnoreCase(current.getUNID()) || x.getData() != current.getParentData()) continue;
			if (prevFound == null) return x;
			if (x.getUNID().equalsIgnoreCase(prevFound.getUNID())) continue;
		}
		return null;
	}

	/**
	 * 返回前环节对应的所有未完成、未删除的流程控制数据列表中匹配当前用户的控制数据列表。
	 * 
	 * <p>
	 * 在事务处理过程中调用本方法时由于控制数据可能会被事务所改动，因此每次调用返回的结果可能会不同。
	 * </p>
	 * 
	 * @return List&lt;WFData&lt;，如果找不到匹配结果则返回null或空列表。
	 */
	public List<WFData> getCurrentWFData() {
		List<WFData> list = getContextWFData();
		List<WFData> result = new ArrayList<WFData>();
		if (list == null || list.isEmpty()) return result;
		int csc = this.getCurrentUserSecurityCode();
		for (WFData x : list) {
			if (x != null && x.getData() == csc) {
				result.add(x);
			}
		}
		return result;
	}

	/**
	 * 返回前环节对应的所有未完成、未删除的流程控制数据列表中匹配当前用户和指定级别的控制数据。
	 * 
	 * <p>
	 * 在事务处理过程中调用本方法时由于控制数据可能会被事务所改动，因此每次调用返回的结果可能会不同。
	 * </p>
	 * 
	 * @param level {@link WFDataLevel}
	 * 
	 * @return WFData，如果找不到匹配结果则返回null，如果有多条匹配，则只会返回第一条匹配的结果。
	 */
	public WFData getCurrentWFData(WFDataLevel level) {
		List<WFData> list = getContextWFData();
		if (list == null || list.isEmpty()) return null;
		int csc = this.getCurrentUserSecurityCode();
		for (WFData x : list) {
			if (x != null && x.getData() == csc && x.checkLevel(level)) return x;
		}
		return null;
	}

	/**
	 * 获取当前请求对应的用户自定义会话信息。
	 * 
	 * @return {@link Session}
	 */
	public Session getSession() {
		long tid = Thread.currentThread().getId();
		GenericPair<Integer, Session> p = m_sessions.get(tid);
		Session session = (p == null ? null : p.getValue());
		if (session == null) session = SessionContext.getSession(null);
		// System.out.println("获取：" + tid + "=" + Session.getUser(session).getName() + ",count=" + m_sessions.size());
		return session;
	}

	/**
	 * 返回当前请求绑定的用户的安全编码。
	 * 
	 * @return int
	 */
	public int getCurrentUserSecurityCode() {
		Session session = this.getSession();
		return Session.getUser(session).getSecurityCode();
	}

	/**
	 * 获取当前用户流程处理权限（主要）级别。
	 * 
	 * <p>
	 * 返回结果为本次事务处理开始之前的值，因此每次在事务提交前调用总是返回相同值。
	 * </p>
	 * 
	 * @return int
	 */
	public int getCurrentLevel() {
		int result = 0;
		WFDataWrapper w = null;
		int csc = this.getCurrentUserSecurityCode();
		for (WFData x : this.m_wfdata) {
			if (x == null || x.getData() != csc || !isContext(x)) continue;
			if (x.getDone() && this.getPersistenceState(x) == PersistenceState.Raw) continue;
			w = getWrapper(x);
			if (w != null && w.oldLevel != -1) {
				result |= w.oldLevel;
			} else {
				result |= x.getLevel();
			}
		}
		return result;
	}

	/**
	 * 获取当前用户流程处理权限辅助级别。
	 * 
	 * <p>
	 * 返回结果为本次事务处理开始之前的值，因此每次在事务提交前调用总是返回相同值。
	 * </p>
	 * 
	 * @return int
	 */
	public int getCurrentSubLevel() {
		int result = 0;
		WFDataWrapper w = null;
		int csc = this.getCurrentUserSecurityCode();
		for (WFData x : this.m_wfdata) {
			if (x == null || x.getData() != csc || !isContext(x)) continue;
			if (x.getDone() && this.getPersistenceState(x) == PersistenceState.Raw) continue;
			w = getWrapper(x);
			if (w != null && w.oldSubLevel != -1) {
				result |= w.oldSubLevel;
			} else {
				result |= x.getSubLevel();
			}
		}
		return result;
	}

	/**
	 * 返回{@link #getCurrentLevel()}的结果是否包含指定{@link WFDataLevel}值且{@link #getCurrentSubLevel()}的结果是否包含指定{@link WFDataSubLevel}值。
	 * 
	 * @param l 如果为null，则不判断此值。
	 * @param sl 如果为null，则不判断此值。
	 * @return boolean
	 */
	public boolean checkCurrentLevel(WFDataLevel l, WFDataSubLevel sl) {
		int lv = getCurrentLevel();
		int slv = getCurrentSubLevel();
		boolean b1 = (l == null ? true : (lv & l.getIntValue()) == l.getIntValue());
		boolean b2 = (sl == null ? true : (sl.getIntValue() == 0 ? slv == 0 : (slv & sl.getIntValue()) == sl.getIntValue()));
		return b1 && b2;
	}

	/**
	 * 返回当前流程实例对应的当前环节。
	 * 
	 * @return
	 */
	public Activity getCurrentActivity() {
		return this.m_currentActivity;
	}

	/**
	 * 设置当前环节为activityUNID指定的环节。
	 * 
	 * @param activityUNID，String，表示新环节的UNID，必须。
	 */
	public void setCurrentActivity(String activityUNID) {
		if (activityUNID == null || activityUNID.length() == 0) new RuntimeException("没有提供有效的新环节UNID！");
		WorkflowHelper wfh = new WorkflowHelper(this.m_currentWorkflow);
		Activity activity = wfh.getActivity(activityUNID);
		if (activity == null) throw new RuntimeException("流程“" + this.m_currentWorkflow.getName() + "”中找不到“" + activityUNID + "”指定的环节！");

		if (this.m_currentActivity != null) { // 把原来环节的结束时间设置为当前时间。
			String oldActivityUNID = this.m_currentActivity.getUNID();
			Map<Integer, StringPair> map = this.m_activityTraversalMap.get(oldActivityUNID);
			if (map != null) {
				StringPair sp = map.get(0);
				sp.setValue(DateTime.getNowDTString());
			}
		}

		this.m_currentActivity = activity;
	}

	/**
	 * 获取activityUNID指定的环节的流转（经历）次数。
	 * 
	 * <p>
	 * 即文档到目前为止经历过指定环节几次。
	 * </p>
	 * 
	 * @param activityUNID
	 * @return 如果没有被流转过，则返回0。
	 */
	public int getActivityTraversalCount(String activityUNID) {
		Map<Integer, StringPair> map = this.m_activityTraversalMap.get(activityUNID);
		return (map == null ? 0 : map.size());
	}

	/**
	 * 获取当前流程实例中指定环节的开始日期时间信息。
	 * 
	 * @param activityUNID 必须，表示环节UNID。
	 * @param traversalIndex 必须，表示环节经历次数的索引（有效值从0到{@link WorkflowRuntime#getActivityTraversalCount(String)}-1），索引数越大表示越早经历的环节。
	 * @return 如果获取不到则返回null。
	 */
	public String getActivityStartDateTime(String activityUNID, int traversalIndex) {
		if (activityUNID == null || activityUNID.length() == 0) return null;
		Map<Integer, StringPair> map = this.m_activityTraversalMap.get(activityUNID);
		if (map == null) return null;
		StringPair sp = map.get(traversalIndex);
		String r = (sp == null ? null : sp.getKey());
		return r;
	}

	/**
	 * 获取当前流程实例中指定环节的结束日期时间信息。
	 * 
	 * <p>
	 * 如果指定的是当前未完成环节的unid，则返回最近一次流程事务的处理时间。
	 * </p>
	 * 
	 * @param activityUNID 必须，表示环节UNID。
	 * @param traversalIndex 必须，表示环节经历次数的索引（有效值从0到{@link WorkflowRuntime#getActivityTraversalCount(String)}-1），索引数越大表示越早经历的环节。
	 * @return 如果获取不到则返回null。
	 */
	public String getActivityEndDateTime(String activityUNID, int traversalIndex) {
		if (activityUNID == null || activityUNID.length() == 0) return null;
		Map<Integer, StringPair> map = this.m_activityTraversalMap.get(activityUNID);
		if (map == null) return null;
		StringPair sp = map.get(traversalIndex);
		String r = (sp == null ? null : sp.getValue());
		if (!activityUNID.equalsIgnoreCase(this.getCurrentActivity().getUNID()) && (r == null || r.length() == 0)) r = DateTime.getNowDTString();
		return r;
	}

	/**
	 * 返回当前环节有效的自动流转配置。
	 * 
	 * <p>
	 * 此处返回的结果已经过滤了表达式校验不通过的自动流转配置。
	 * </p>
	 * 
	 * @return List&lt;Automation&gt;
	 */
	public List<Automation> getContextAutomations() {
		Activity activity = this.getCurrentActivity();
		List<Automation> autos = activity.getAutomations();
		// 自动流转
		if (autos == null || autos.isEmpty()) return null;
		List<Automation> result = new ArrayList<Automation>(autos.size());
		String expression = null;
		Session s = this.getSession();
		for (Automation x : autos) {
			if (x == null || x.getActivity() == null || x.getActivity().length() == 0 || x.getParticipants() == null || x.getParticipants().isEmpty()) continue;
			expression = x.getCondition();
			boolean r = true;
			if (expression != null && expression.length() > 0) {
				ExpressionEvaluator evaluator = new ExpressionEvaluator(expression, s, m_document);
				r = evaluator.evalBoolean();
			}
			if (r) result.add(x);
			if (this.debugable()) {
				StringBuilder sb = new StringBuilder();
				if (x.getParticipants() != null) {
					for (Participant pt : x.getParticipants()) {
						if (pt == null) continue;
						if (sb.length() > 0) sb.append(" ");
						sb.append(ParticipantHelper.getFormatValue(pt, "cn"));
					}
				}
				if (sb.length() == 0) sb.append("[没有配置参与者范围]");
				WorkflowHelper wh = new WorkflowHelper(this.getCurrentWorkflow());
				Activity ta = wh.getActivity(x.getActivity());
				appendVerboseLog("计算自动流转启用条件表达式:目标环节=%1$s(%2$s),表达式=%3$s,结果=%4$s,参与者范围=%5$s", (ta == null ? "[未知]" : ta.getName()), x.getActivity(), expression, (r ? "true" : "false"), sb.toString());
			}
		}
		return result;
	}

	/**
	 * 返回当前环节包含的有效的环节出口列表集合。
	 * 
	 * <p>
	 * 此处返回的结果过滤了表达式校验不通过的环节出口配置并按环节排序号进行了排序。
	 * </p>
	 * 
	 * @return List&lt;Transition&gt;
	 */
	public List<Transition> getContextTransitions() {
		List<Transition> result = new ArrayList<Transition>();
		Workflow wf = this.getCurrentWorkflow();
		Map<String, Activity> map = new HashMap<String, Activity>();
		if (wf != null && wf.getActivities() != null) {
			for (Activity a : wf.getActivities()) {
				if (a == null) continue;
				map.put(a.getUNID(), a);
			}
		}
		Session s = this.getSession();
		Document doc = this.getDocument();
		ExpressionEvaluator evaluator = null;
		Activity activity = this.getCurrentActivity();
		List<Transition> trans = (activity == null ? null : activity.getTransitions());
		Transition t = null;
		Activity ax = null;
		try {
			boolean arbitrary = (doc.getParamValueInt(Workflow.WORKFLOW_OPTION_PARAMNAME, 0) == 1);
			// 处理自由流转：可以转到任意环节（除了当前环节本身）。
			if (arbitrary && map.size() > 0) {
				for (Activity a : wf.getActivities()) {
					if (a == null || activity == null) continue;
					if (activity.getUNID().equalsIgnoreCase(a.getUNID()) && a.getActivityType() != ActivityType.Normal) continue;
					t = new Transition(a);
					result.add(t);
				}
			} else if (trans != null && !trans.isEmpty()) {
				String expression = null;
				boolean addFlag = false;
				for (Transition x : trans) {
					if (x == null) continue;
					expression = x.getCondition();
					addFlag = true;
					if (expression != null && expression.length() > 0) {
						evaluator = new ExpressionEvaluator(expression, s, doc);
						addFlag = evaluator.evalBoolean();
						appendVerboseLog("计算目标环节的准入条件表达式:环节=%1$s(%2$s),表达式=%3$s,结果=%4$s", x.getTitle(), x.getUNID(), expression, (addFlag ? "true" : "false"));
					}
					if (addFlag) {
						t = (Transition) x.clone();
						ax = map.get(t.getUNID());
						if (ax != null) t.setSort(ax.getSort());
						result.add(t);
					}
				} // for end
			} // else end
			map.clear();
			// 排序
			if (result != null && result.size() > 0) {
				Comparator<Transition> comparator = new Comparator<Transition>() {
					@Override
					public int compare(Transition o1, Transition o2) {
						if (o1 == null && o2 == null) return 0;
						if (o1 == null && o2 != null) return -1;
						if (o1 != null && o2 == null) return 1;
						int result = (o1.getSort() - o2.getSort());
						if (result == 0) {
							result = o1.getTitle().compareToIgnoreCase(o2.getTitle());
							if (result == 0) { return o1.getUNID().compareToIgnoreCase(o2.getUNID()); }
						}
						return (result < 0 ? -1 : 1);
					}
				};
				Collections.sort(result, comparator);
			}
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		return result;
	}

	/**
	 * 获取当前流程所有已经经历过的环节列表集合。
	 * 
	 * <p>
	 * 此处返回的结果过滤了当前环节，对于经历过多次的环节只按最近一次完成的环节来处理，最终结果按环节开始时间倒序进行排序。<br/>
	 * 如果开始时间相同则按照目标流程环节的排序结果{@link Activity#compareTo(Activity)}的顺序输出。
	 * </p>
	 * 
	 * @return
	 */
	public List<Activity> getTraversalActivities() {
		List<Activity> result = null;
		if (this.m_activityTraversalMap == null || this.m_activityTraversalMap.isEmpty()) return null;
		List<GenericPair<String, Activity>> list = new ArrayList<GenericPair<String, Activity>>(m_activityTraversalMap.size());
		Activity current = this.getCurrentActivity();
		Activity activity = null;
		WorkflowHelper wh = new WorkflowHelper(this.getCurrentWorkflow());
		StringPair sp = null;
		Map<Integer, StringPair> tmap = null;
		for (String key : this.m_activityTraversalMap.keySet()) {
			if (key == null || (current != null && key.equalsIgnoreCase(current.getUNID()))) continue;
			tmap = this.m_activityTraversalMap.get(key);
			if (tmap == null) continue;
			sp = tmap.get(0);
			if (sp == null || sp.getKey() == null) continue;
			activity = wh.getActivity(key);
			if (activity == null) continue;
			list.add(new GenericPair<String, Activity>(sp.getKey(), activity));
		}
		if (list != null && list.size() > 0) {
			Collections.sort(list, C);
			result = new ArrayList<Activity>(list.size());
			for (GenericPair<String, Activity> gp : list) {
				result.add(gp.getValue());
			}
			list.clear();
		}
		return result;
	}

	static final Comparator<GenericPair<String, Activity>> C = new Comparator<GenericPair<String, Activity>>() {
		@Override
		public int compare(GenericPair<String, Activity> o1, GenericPair<String, Activity> o2) {
			String dt1 = o1.getKey();
			String dt2 = o2.getKey();
			int r = dt1.compareTo(dt2);
			if (r != 0) return (r > 0 ? 1 : -1);
			Activity a1 = o1.getValue();
			Activity a2 = o2.getValue();
			return a1.compareTo(a2);
		}
	};

	/**
	 * 返回当前实例对应的流程。
	 * 
	 * @return
	 */
	public Workflow getCurrentWorkflow() {
		if (this.m_document != null && this.m_currentWorkflow != null) {
			Workflow wf = ResourceContext.getInstance().getResource(this.m_currentWorkflow.getUNID(), Workflow.class);
			if (wf != null && !wf.getModified().equalsIgnoreCase(this.m_currentWorkflow.getModified())) {
				this.m_currentWorkflow = wf;
				FileLogger.debug("文档“%1$s”所绑定的流程“%2$s”已被修改，正在重新加载...", m_document.getName(), this.m_currentWorkflow.getName());
				if (this.m_currentActivity != null) {
					WorkflowHelper wfh = new WorkflowHelper(wf);
					this.m_currentActivity = wfh.getActivity(this.m_currentActivity.getUNID());
				}
			}
		}
		return this.m_currentWorkflow;
	}

	/**
	 * 返回当前流程实例号。
	 * 
	 * <p>
	 * 保留使用
	 * </p>
	 * 
	 * @return int
	 */
	public int getInstance() {
		return this.m_instance;
	}

	/**
	 * 返回当前流程父实例号。
	 * 
	 * <p>
	 * 保留使用
	 * </p>
	 * 
	 * @return int
	 */
	public int getParentInstance() {
		return this.m_parentInstance;
	}

	/**
	 * 返回当前正在处理的文档。
	 * 
	 * @return
	 */
	public Document getDocument() {
		return this.m_document;
	}

	/**
	 * 获取指定{@link WFData}对象的对应的{@link WFDataWrapper}对象。
	 * 
	 * @param x
	 * @return {@link WFDataWrapper}
	 */
	protected WFDataWrapper getWrapper(WFData x) {
		if (x == null || this.m_changedWFData == null || this.m_changedWFData.isEmpty()) return null;
		for (WFDataWrapper y : this.m_changedWFData) {
			if (y != null && y.getWFData() != null && x.getUNID() != null && x.getUNID().equalsIgnoreCase(y.getWFData().getUNID())) return y;
		}
		return null;
	}

	/**
	 * 获取指定{@link WFData}对象的持久化状态信息。
	 * 
	 * <p>
	 * 持久化状态信息是指新增、删除或更改的控制数据等待持久化保存到数据库之前的状态，如果返回{@link PersistenceState#Raw}则表示控制数据为数据库中读取出来的原始状态。
	 * </p>
	 * 
	 * @param x
	 * @return {@link PersistenceState}
	 */
	public PersistenceState getPersistenceState(WFData x) {
		WFDataWrapper w = getWrapper(x);
		return w == null ? null : w.getPersistenceState();
	}

	/**
	 * 返回指定{@link WFData}对象是否属于当前流程实例和当前环节。
	 * 
	 * @param x {@link WFData}
	 * @return
	 */
	public boolean isContext(WFData x) {
		if (x == null) return false;
		return (x.getInstance() == this.m_instance && this.m_currentActivity.getUNID().equalsIgnoreCase(x.getActivity()));
	}

	/**
	 * 返回当前环节是否还有其他有效审批人。
	 * 
	 * <p>
	 * 如果当前用户不是有效审批人则返回false，如果当前用户是有效审批人，那么只有以下情况返回true：
	 * <ul>
	 * <li>本环节还有其它审批人；</li>
	 * <li>本环节还有其它等待审批人。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean hasOtherApprover() {
		boolean result = false;
		WFData current = getCurrentWFData(WFDataLevel.Approver);
		if (current != null) {
			boolean isAgent = current.checkSubLevel(WFDataSubLevel.Agent);
			int parentData = current.getParentData();
			int data = current.getData();
			List<WFData> list = getContextWFData();
			for (WFData x : list) {
				if (x == null || current.getUNID().equalsIgnoreCase(x.getUNID())) continue;
				// 等效办理人不算其他有效办理人
				if (x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Equivalent)) continue;
				// 代理人不算其他有效办理人
				if (!isAgent && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Agent) && x.getParentData() == data) continue;
				// 委托人不算其他有效办理人
				if (isAgent && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Normal) && x.getData() == parentData) continue;
				// 如果有其它办理人、等待办理人则说明还有其它办理人。
				if (x.checkLevel(WFDataLevel.Approver) || x.checkLevel(WFDataLevel.Standby)) { return true; }
			}
		}
		return result;
	}

	/**
	 * 检查当前环节下是否有指定级别的控制数据。
	 * 
	 * @param level
	 * @return
	 */
	public boolean hasContextWFData(WFDataLevel level) {
		List<WFData> list = getContextWFData();
		for (WFData x : list) {
			if (x != null && x.checkLevel(level)) return true;
		}
		return false;
	}

	/**
	 * 返回用户交互接口提供的信息。
	 * 
	 * <p>
	 * 可以通过此方法返回的{@link WorkflowForm}对象获取客户端传递的参数信息。
	 * </p>
	 * 
	 * @return WorkflowForm
	 */
	public WorkflowForm getWorkflowForm() {
		return this.m_workflowForm;
	}

	/**
	 * 设置用户交互接口提供的信息。
	 * 
	 * @param workflowForm WorkflowForm
	 */
	public void setWorkflowForm(WorkflowForm workflowForm) {
		this.m_workflowForm = workflowForm;
		if (m_workflowForm != null) {
			if (m_workflowForm.getOption() != 0 && m_document.getParamValueInt(Workflow.WORKFLOW_OPTION_PARAMNAME, -1) != m_workflowForm.getOption()) {
				Parameter p = new Parameter(m_document.getUNID());
				p.setName(Workflow.WORKFLOW_OPTION_PARAMNAME);
				p.setValue(m_workflowForm.getOption() + "");
				p.setDescription("流程流转选项。");
				DBRequest dbr = Instance.newInstance("com.tansuosoft.discoverx.dao.impl.ParameterInserter", DBRequest.class);
				dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, p);
				dbr.sendRequest();
				if (dbr.getResultLong() > 0) m_document.setParameter(p);
			}
		}
	}

	/**
	 * 设置当前事务处理对象。
	 * 
	 * @param transaction
	 */
	protected void setTransaction(Transaction transaction) {
		this.m_transaction = transaction;
		if (this.m_transaction != null) this.m_transaction.setWorkflowRuntime(this);
	}

	/**
	 * 执行指定的流程事务直到最终事务处理结果返回null、{@link IdleTransaction}、{@link InteractionTransaction}之一。
	 * 
	 * <p>
	 * 从保证流程事务完整性、一致性及提高性能角度考虑，如果执行链中有多个流程事务那么建议在最后一个事务的{@link Transaction#getAutoPersistence()}设置为true,之前事务的{@link Transaction#getAutoPersistence()}都设置为false。
	 * </p>
	 * <p>
	 * 执行过程中如果当前执行的{@link Transaction}对象的{@link Transaction#getAutoRaiseEvent()}返回true，则会触发流程事务开始和结束事件。<br/>
	 * 其中流程流程事务结束事件在执行流程状态数据变更持久化之后执行（即先把对流程状态数据的更改（如增、删、改）保存到数据库中然后触发流程流程事务结束事件）。<br/>
	 * 如果执行链中有多个流程事务，那么可能多次触发事件（如果有多个{@link Transaction}对象的{@link Transaction#getAutoRaiseEvent()}都返回true的话）。
	 * </p>
	 * <p>
	 * 如果执行链中的事务的{@link Transaction#getAutoLog()}返回true，则会记录事务日志。
	 * </p>
	 * 
	 * @param transaction {@link Transaction}，要执行的流程事务。
	 * @param session {@link Session}，执行流程事务的用户自定义会话。
	 * @return {@link Transaction}，可以返回null（表示事务处理完毕）或{@link InteractionTransaction}（表示需要用户交互）或{@link IdleTransaction}（表示一个空闲等待事务）。
	 */
	@SuppressWarnings("unchecked")
	public Transaction transact(Transaction transaction, Session session) {
		boolean storedSession = false;
		if (session != null) {
			Session stored = this.getSession();
			if (stored == null) {
				this.storeSession(session);
				storedSession = true;
			} else {
				if (!session.getSessionUNID().equalsIgnoreCase(stored.getSessionUNID())) throw new RuntimeException("用户会话不匹配！");
			}
		}
		this.setTransaction(transaction);

		if (this.m_transaction == null || this.m_transaction instanceof IdleTransaction) { return this.m_transaction; }
		if (this.m_transaction instanceof InteractionTransaction) {
			appendVerboseLog("返回等待用户交互流程事务:%1$s", m_transaction.getClass().getName());
			return this.m_transaction;
		}

		WorkflowEventArgs e = null; // 事件参数
		try {
			if (this.m_transaction.getAutoRaiseEvent()) {
				e = new WorkflowEventArgs();
				e.setTransaction(this.m_transaction);
				e.setWorkflowRuntime(this);
				// 触发流程事务开始事件
				this.callEventHandler(EventHandler.EHT_WORKFLOWTRANSACTIONBEGIN, e);
			}

			WFData approvder = getCurrentWFData(WFDataLevel.Approver);
			if (approvder != null) m_transaction.buildLog(approvder);

			// 执行当前流程事务。
			appendVerboseLog("执行流程事务:%1$s...", m_transaction.getClass().getName());
			Transaction nextTransaction = this.m_transaction.transact(session);
			if (nextTransaction != null) {
				nextTransaction.setPrev(m_transaction);
			} else {
				nextTransaction = this.m_transaction.getNext();
			}

			// 记录日志
			Log log = m_transaction.getLog();
			if (log != null) {
				appendVerboseLog("生成流程日志:动作=%1$s;目标=%2$s,消息=%3$s", log.getVerb(), log.getObject(), log.getMessage());
				List<Log> logs = session.getParamValueObject(LOGS_PARAM_NAME, List.class, null);
				if (logs == null) {
					logs = new ArrayList<Log>();
					session.setParameter(LOGS_PARAM_NAME, logs);
				}
				logs.add(log);
			}

			// 触发流程事务结束事件(在流程事务提交之前)
			if (m_transaction.getSubmitAfterTransactionEnd() && m_transaction.getAutoRaiseEvent()) this.callEventHandler(EventHandler.EHT_WORKFLOWTRANSACTIONEND, e);

			// 如果没有返回有效的下一个流程事务则根据需要同步控制数据的变更到持久层（即在最后一个流程事务完成后根据需要同步控制数据的变更到持久层）。
			if (nextTransaction == null && this.m_transaction.getAutoPersistence()) this.synchronizeToPersistentLevel();

			// 触发流程事务结束事件(在流程事务提交之后)
			if (!m_transaction.getSubmitAfterTransactionEnd() && m_transaction.getAutoRaiseEvent()) this.callEventHandler(EventHandler.EHT_WORKFLOWTRANSACTIONEND, e);

			// 如果返回了有效的下一个流程事务则继续执行之。
			if (nextTransaction != null) { return this.transact(nextTransaction, session); }
		} catch (Exception ex) {
			if (ex instanceof RuntimeException) throw (RuntimeException) ex;
			throw new RuntimeException(ex);
		} finally {
			if (storedSession) this.removeSession();
		}
		return null;
	}

	/**
	 * 追加参数指定的新流程控制数据（{@link WFData}）到现有流程控制数据列表中并设置为待新增状态。
	 * 
	 * <p>
	 * 此方法执行时，将在现有流程控制数据列表的第一个位置插入数据（即此方法执行完毕后，调用{@link #getWFData()}返回的结果已经包含新加的控制数据）。
	 * </p>
	 * <p>
	 * 如果控制数据已经存在则不做处理直接返回，只有持久化保存后（通常是当前事务链中最后一个事务处理完成时）才能实际写入数据库。
	 * </p>
	 * <p>
	 * 将触发{@link EventHandler#EHT_WORKFLOWDATAADDED}类型的事件。
	 * </p>
	 * 
	 * @param wfdata 如果参数为null，则什么也不做直接返回，如果控制数据已经存在，则会抛出运行时异常。
	 */
	public void addWFData(WFData wfdata) {
		if (wfdata == null) return;
		int oldWFDataSize = m_wfdata.size();
		synchronized (m_lock) {
			for (WFData x : m_wfdata) {
				if (x != null && x.getUNID() != null && x.getUNID().equalsIgnoreCase(wfdata.getUNID())) {
					if (CommonConfig.getInstance().getDebugable()) FileLogger.debug("警告：“%1$s”指定的控制数据已经存在%2$s。", wfdata.getUNID());
					return;
				}
			}
			this.m_wfdata.add(0, wfdata);
			Map<Integer, StringPair> map = this.m_activityTraversalMap.get(wfdata.getActivity());
			if (map == null) {
				map = new TreeMap<Integer, StringPair>();
				map.put(0, new StringPair(wfdata.getCreated(), wfdata.getAccomplished()));
				m_activityTraversalMap.put(wfdata.getActivity(), map);
			}
			this.m_changedWFData.add(new WFDataWrapper(wfdata, PersistenceState.New, this.getCurrentUserSecurityCode()));
		}
		// 如果是流程开始阶段新加的控制数据则忽略
		if (oldWFDataSize > 0) appendVerboseLog("新增控制数据:%1$s", getWFDataDebugInfo(wfdata));
		WorkflowDataEventArgs e = new WorkflowDataEventArgs();
		e.setData(wfdata);
		e.setTransaction(m_transaction);
		e.setWorkflowRuntime(this);
		this.callEventHandler(EventHandler.EHT_WORKFLOWDATAADDED, e);
	}

	/**
	 * 从现有流程控制数据列表中设置指定的流程控制数据（{@link WFData}）为待删除状态。
	 * 
	 * <p>
	 * 此方法执行时，只标记控制数据为待删除，只有在持久化保存完成（通常是最后一步事务完成时）才会在现有流程控制数据列表中删除对应控制数据（即只有在持久化保存完成后调用{@link #getWFData()}返回的结果才是实际删除了指定控制的结果）。
	 * </p>
	 * <p>
	 * 现有流程控制数据列表中必须存在传入的流程控制数据且不能有其它等待增、删、改的状态，否则不做处理直接返回。
	 * </p>
	 * <p>
	 * 将触发{@link EventHandler#EHT_WORKFLOWDATADELETED}类型的事件。
	 * </p>
	 * 
	 * @param wfdata
	 */
	public void removeWFData(WFData wfdata) {
		if (wfdata == null) return;
		synchronized (m_lock) {
			boolean found = false;
			for (WFData x : m_wfdata) {
				if (x != null && x.getUNID() != null && x.getUNID().equalsIgnoreCase(wfdata.getUNID())) {
					found = true;
					break;
				}
			}
			if (!found) {
				if (CommonConfig.getInstance().getDebugable()) FileLogger.debug("警告：“%1$s”控制数据不存在。", wfdata.getUNID());
				return;
			}
			this.m_changedWFData.add(new WFDataWrapper(wfdata, PersistenceState.Delete, this.getCurrentUserSecurityCode()));
		}
		appendVerboseLog("删除控制数据:%1$s", getWFDataDebugInfo(wfdata));
		WorkflowDataEventArgs e = new WorkflowDataEventArgs();
		e.setData(wfdata);
		e.setTransaction(m_transaction);
		e.setWorkflowRuntime(this);
		this.callEventHandler(EventHandler.EHT_WORKFLOWDATADELETED, e);
	}

	/**
	 * 标记指定流程控制数据（{@link WFData}）为完成状态（同时将其完成时间{@link WFData#setAccomplished(String)}设置为当前时间）并设置为待更新状态。
	 * 
	 * <p>
	 * 此方法执行时，将立即设置已完成标记和完成时间结果（即此方法执行完毕后，调用{@link #getWFData()}返回的结果中对应的控制数据已经修改了完成标记和完成时间）。
	 * </p>
	 * <p>
	 * 现有流程控制数据列表中必须存在传入的流程控制数据，否则不做任何处理直接返回。只有持久化保存后（通常是当前事务链中最后一个事务处理完成时）才能实际写入数据库。
	 * </p>
	 * <p>
	 * 将触发{@link EventHandler#EHT_WORKFLOWDATAMARKEDDONE}类型的事件。
	 * </p>
	 * 
	 * @param wfdata
	 */
	public void markWFDataDone(WFData wfdata) {
		if (wfdata == null) return;
		synchronized (m_lock) {
			boolean found = false;
			for (WFData x : m_wfdata) {
				if (x != null && x.getUNID() != null && x.getUNID().equalsIgnoreCase(wfdata.getUNID())) {
					found = true;
					break;
				}
			}
			if (!found) {
				if (CommonConfig.getInstance().getDebugable()) FileLogger.debug("警告：“%1$s”指定的控制数据不存在。", wfdata.getUNID());
				return;
			}
			wfdata.setDone(true);
			String accDt = DateTime.getNowDTString();
			wfdata.setAccomplished(accDt);
			Map<Integer, StringPair> map = this.m_activityTraversalMap.get(wfdata.getActivity());
			if (map != null) {
				StringPair sp = map.get(0);
				if (sp != null) {
					String endDt = sp.getValue();
					if (endDt == null) {
						sp.setValue(wfdata.getAccomplished());
					} else if (endDt.compareToIgnoreCase(accDt) < 0) {
						sp.setValue(accDt);
					}
				}
			}
			this.m_changedWFData.add(new WFDataWrapper(wfdata, PersistenceState.Update, this.getCurrentUserSecurityCode()));
		}
		appendVerboseLog("标记控制数据完成:%1$s", getWFDataDebugInfo(wfdata));
		WorkflowDataEventArgs e = new WorkflowDataEventArgs();
		e.setData(wfdata);
		e.setTransaction(m_transaction);
		e.setWorkflowRuntime(this);
		this.callEventHandler(EventHandler.EHT_WORKFLOWDATAMARKEDDONE, e);
	}

	/**
	 * 标记指定流程控制数据（{@link WFData}）的主辅级别为新的级别并设置为待更新状态。
	 * 
	 * <p>
	 * 此方法执行时，将立即设置新级别值（即此方法执行完毕后，调用{@link #getWFData()}返回的结果中对应的控制数据已经修改为新的级别结果）。
	 * </p>
	 * <p>
	 * 现有流程控制数据列表中必须存在传入的流程控制数据，否则不做任何处理直接返回。只有持久化保存后（通常是当前事务链中最后一个事务处理完成时）才能实际写入数据库。
	 * </p>
	 * <p>
	 * 将触发{@link EventHandler#EHT_WORKFLOWDATALEVELCHANGED}类型的事件。
	 * </p>
	 * 
	 * @param wfdata
	 * @param newLevel
	 * @param newSubLevel
	 */
	public void markWFDataLevel(WFData wfdata, int newLevel, int newSubLevel) {
		if (wfdata == null) return;
		int oldLevel = 0;
		int oldSubLevel = 0;
		synchronized (m_lock) {
			boolean found = false;
			for (WFData x : m_wfdata) {
				if (x != null && x.getUNID() != null && x.getUNID().equalsIgnoreCase(wfdata.getUNID())) {
					found = true;
					break;
				}
			}
			if (!found) {
				if (CommonConfig.getInstance().getDebugable()) FileLogger.debug("警告：“%1$s”指定的控制数据不存在。", wfdata.getUNID());
				return;
			}
			oldLevel = wfdata.getLevel();
			oldSubLevel = wfdata.getSubLevel();
			wfdata.setLevel(newLevel);
			wfdata.setSubLevel(newSubLevel);
			WFDataWrapper wfdw = new WFDataWrapper(wfdata, PersistenceState.Update, this.getCurrentUserSecurityCode());
			wfdw.oldLevel = oldLevel;
			wfdw.oldSubLevel = oldSubLevel;
			this.m_changedWFData.add(wfdw);
		}
		appendVerboseLog("修改控制数据级别:主级别从%1$d到%2$d,辅级别从%3$d到%4$d(%5$s)。", oldLevel, newLevel, oldSubLevel, newSubLevel, getWFDataDebugInfo(wfdata));
		WorkflowDataLevelChangedEventArgs e = new WorkflowDataLevelChangedEventArgs();
		e.setData(wfdata);
		e.setTransaction(m_transaction);
		e.setWorkflowRuntime(this);
		e.setOldLevel(oldLevel);
		e.setOldSubLevel(oldSubLevel);
		e.setNewLevel(newLevel);
		e.setNewSubLevel(newSubLevel);
		this.callEventHandler(EventHandler.EHT_WORKFLOWDATALEVELCHANGED, e);
	}

	/**
	 * 新建一个新的流程控制数据对象，然后绑定当前流程运行时的文档、流程、环节、实例、父实例等信息到相应属性中的，最后返回之。
	 * 
	 * @return {@link WFData}
	 */
	public WFData newContextWFData() {
		WFData result = new WFData();
		setWFDataContextProperty(result);
		return result;
	}

	/**
	 * 设置当前流程运行时的文档、流程、环节、实例、父实例等信息到参数提供的流程控制数据对象的相应属性中。
	 * 
	 * @return
	 */
	protected void setWFDataContextProperty(WFData wfdata) {
		wfdata.setPUNID(this.m_document.getUNID());
		wfdata.setWorkflow(this.getCurrentWorkflow().getUNID());
		wfdata.setActivity(this.getCurrentActivity().getUNID());
		wfdata.setInstance(this.getInstance());
		wfdata.setParentInstance(this.getParentInstance());
	}

	private static final String insertClassName = "com.tansuosoft.discoverx.dao.impl.WFDataInserter";
	private static final String updateClassName = "com.tansuosoft.discoverx.dao.impl.WFDataUpdater";
	private static final String deleteClassName = "com.tansuosoft.discoverx.dao.impl.WFDataRemover";
	private static final String securityEntryInsertClassName = "com.tansuosoft.discoverx.dao.impl.SecurityEntryInserter";
	private static final String securityEntryUpdateClassName = "com.tansuosoft.discoverx.dao.impl.SecurityEntryUpdater";
	private static final String securityUpdateClassName = "com.tansuosoft.discoverx.dao.impl.SecurityUpdater";

	/**
	 * 最终提交流程事务。
	 * 
	 * <p>
	 * 即将增、删、改（改过属性的）流程控制数据持久化保存到数据库。
	 * </p>
	 * <p>
	 * 本方法由系统内部自动调用（通常在流程事务链的最后一个事务执行完毕后调用此方法）请勿直接调用（可以通过{@link Transaction#getAutoPersistence()}控制流程事务执行完毕时是否自动调用此方法)。
	 * </p>
	 * <p>
	 * 调用此方法前通过{@link #addWFData(WFData)}、{@link #removeWFData(WFData)}、{@link #markWFDataDone(WFData)}、{@link #markWFDataLevel(WFData, int, int)}
	 * 等方法增、删、改的控制数据只在内存缓存中体现，执行此方法成功后才会持久保存。同时此方法执行后（或理解为流程事务最终提交后）才会把流程用户相关的权限信息同步提交到文档安全控制信息({@link Document#getSecurity()})。
	 * </p>
	 * 
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	protected boolean synchronizeToPersistentLevel() {
		PersistenceState state = null;
		List<WFData> deleteds = new ArrayList<WFData>();
		DBRequest first = null;
		DBRequest dbr = null;
		DBRequest dbrs = null;
		boolean securityDbrFlag = false;
		boolean insertFlag = false;
		int currentSecurityCode = this.getCurrentUserSecurityCode();
		int commitCnt = 0;
		List<WFDataWrapper> newList = new ArrayList<WFDataWrapper>();
		Session session = this.getSession();
		if (session == null) throw new RuntimeException("登录会话异常(TID:" + Thread.currentThread().getId() + ")，可能原因有：没有登录、登录超时、浏览器因Cookie设置或插件病毒等原因无法保留会话等！");
		try {
			synchronized (m_lock) {
				Security security = this.m_document.getSecurity();
				if (security != null) {
					try {
						security = (Security) security.clone();
					} catch (CloneNotSupportedException e) {
					}
				}
				if (security == null) security = new Security(this.m_document.getUNID());

				WFData x = null;
				// 循环持久化保存变更的控制数据
				for (WFDataWrapper z : this.m_changedWFData) {
					if (z == null || currentSecurityCode != z.getSecurityCode()) {
						if (z != null) newList.add(z);
						continue;
					}
					x = z.getWFData();
					if (x == null) continue;
					state = z.getPersistenceState();
					if (state == null || state == PersistenceState.Raw) continue;
					commitCnt++;
					dbrs = null;
					securityDbrFlag = false;
					insertFlag = false;
					switch (state.getIntValue()) {
					case 1: // new
						dbr = DBRequest.getInstance(insertClassName);
						if (dbr == null) throw new RuntimeException("无法初始化“" + insertClassName + "”对应的数据库请求对象！");
						dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, x);
						securityDbrFlag = true;
						if (security.setWorkflowLevel(x.getData(), x.getLevel()) < 0) insertFlag = true;
						break;
					case 2: // delete
						dbr = DBRequest.getInstance(deleteClassName);
						if (dbr == null) throw new RuntimeException("无法初始化“" + deleteClassName + "”对应的数据库请求对象！");
						dbr.setParameter(DBRequest.UNID_PARAM_NAME, x.getUNID());
						deleteds.add(x);
						securityDbrFlag = true;
						if (security.removeWorkflowLevel(x.getData(), x.getLevel()) < 0) insertFlag = true;
						break;
					case 3: // update
						dbr = DBRequest.getInstance(updateClassName);
						if (dbr == null) throw new RuntimeException("无法初始化“" + updateClassName + "”对应的数据库请求对象！");
						dbr.setParameter(DBRequest.ENTITY_PARAM_NAME, x);
						securityDbrFlag = true;
						if (x.getDone()) {
							if (security.removeWorkflowLevel(x.getData(), x.getLevel()) < 0) insertFlag = true;
						} else {
							if (security.setWorkflowLevel(x.getData(), x.getLevel()) < 0) insertFlag = true;
						}
						break;
					default:
						break;
					}// switch end
						// 安全信息数据库请求
					if (securityDbrFlag) {
						SecurityEntry securityEntry = security.getSecurityEntry(x.getData());
						if (securityEntry != null) {
							if (insertFlag) {
								dbrs = DBRequest.getInstance(securityEntryInsertClassName);
								if (dbrs == null) throw new RuntimeException("无法初始化“" + securityEntryInsertClassName + "”对应的数据库请求对象！");
							} else {
								dbrs = DBRequest.getInstance(securityEntryUpdateClassName);
								if (dbrs == null) throw new RuntimeException("无法初始化“" + securityEntryUpdateClassName + "”对应的数据库请求对象！");
							}
							dbrs.setParameter(DBRequest.ENTITY_PARAM_NAME, securityEntry);
							dbr.setNextRequest(dbrs);
						}
					}

					if (first == null) {
						first = dbr;
					} else {
						first.setNextRequest(dbr);
					}
				}// for end

				// 日志
				List<Log> logs = session.getParamValueObject(LOGS_PARAM_NAME, List.class, null);
				if (logs != null && logs.size() > 0) {
					DBRequest dbrl = null;
					for (Log log : logs) {
						if (log == null) continue;
						dbrl = Instance.newInstance(LOG_INSERTER_IMPL, DBRequest.class);
						if (dbrl == null) throw new Exception("无法初始化日志写入数据库请求类。");
						dbrl.setParameter(DBRequest.ENTITY_PARAM_NAME, log);
						if (first == null) {
							first = dbrl;
						} else {
							first.setNextRequest(dbrl);
						}
					}
				}

				// 随流程事务一起提交的数据库访问请求
				if (m_transaction != null && m_transaction.getSubmitAfterTransactionEnd()) {
					List<DBRequest> codbrs = m_transaction.getCoDBRequests();
					if (codbrs != null) {
						for (DBRequest codbr : codbrs) {
							if (codbr == null) continue;
							if (first == null) {
								first = codbr;
							} else {
								first.setNextRequest(codbr);
							}
						}
					}
				}// if end

				long resultLong = -1;
				if (first != null) {
					DBRequest dbrlast = DBRequest.getInstance(securityUpdateClassName);
					dbrlast.setParameter(DBRequest.ENTITY_PARAM_NAME, security);
					dbrlast.setParameter("state", this.m_document.getState());
					// 包含流程状态信息的文档额外信息
					DocumentOthersInfoQuery documentOthersInfoQuery = WorkflowStateRecorder.getDocumentOthersInfoQueryDBRequest(this, this.m_transaction);
					if (documentOthersInfoQuery != null) dbrlast.setParameter("c_others", documentOthersInfoQuery.toString());
					first.setNextRequest(dbrlast);
					first.setUseTransaction(true);
					first.sendRequest();
					resultLong = first.getResultLong();
				}

				if (resultLong >= 0) { // 如果是单环节的情况且为更新或删除流程控制数据，因为单环节不保存控制数据，所以可能返回0。
					if (logs != null && logs.size() > 0) {
						for (Log log : logs) {
							if (log != null) m_document.getLogs().add(0, log);
						}
						logs.clear();
					}
					// 持久化成功后同步设置文档的安全信息
					if (security != null) this.m_document.setSecurity(security);
					for (WFDataWrapper z : this.m_changedWFData) {
						if (z == null || currentSecurityCode != z.getSecurityCode()) continue;
						x = z.getWFData();
						if (x == null) continue;
						state = z.getPersistenceState();
						if (state == null || state == PersistenceState.Raw) continue;
						switch (state.getIntValue()) {
						case 1: // new
							security.setWorkflowLevel(x.getData(), x.getLevel());
							break;
						case 2: // delete
							security.removeWorkflowLevel(x.getData(), x.getLevel());
							break;
						case 3: // update
							if (x.getDone()) {
								security.removeWorkflowLevel(x.getData(), x.getLevel());
							} else {
								security.setWorkflowLevel(x.getData(), x.getLevel());
							}
							break;
						default:
							break;
						}// switch end
					}// for end

					// 持久化成功后去除删除掉的控制数据。
					for (WFData deleted : deleteds) {
						this.m_wfdata.remove(deleted);
					}

					// 持久化成功后将变更内容列表中当前用户变更的条目去掉
					this.m_changedWFData.clear();
					this.m_changedWFData = newList;

					// 删除掉文档缓存中与当前文档不指向同一个地址的被缓存的与当前文档unid相同的文档
					Document docBuffered = DocumentBuffer.getInstance().getDocument(this.m_document.getUNID());
					if (docBuffered != null && docBuffered != this.m_document) {
						DocumentBuffer.getInstance().removeDocument(this.m_document.getUNID());
					}
				}// if end
			}// synchronized end
			appendVerboseLog("提交控制数据和日志:已提交数=%1$d,待提交数=%2$d,总控制数据数=%3$d。", commitCnt, newList.size(), m_wfdata.size());
			return true;
		} catch (Exception ex) {
			// 持久化失败时将控制数据设置回原始状态
			FileLogger.error(ex);
			FileLogger.debug("提交流程控制数据时出现错误：%1$s", ex.getMessage());
			this.m_wfdata = WFDataProvider.reloadWFData(this.m_document.getUNID());
		}

		return false;
	}

	/**
	 * 关闭之前获取到的流程运行时。
	 * 
	 * <p>
	 * 只有通过{@link #getInstance(Document, Session)}或{@link #getInstance(Document, Session, int)}获取的实例每次使用完毕后才能且必须调用此方法。<br/>
	 * 其余情况不能调用此方法，否则可能导致错误，比如以下两种情况下不能调用此方法： <br/>
	 * 1.流程事件处理程序中通过流程事件参数获取的实例在事件处理程序执行完毕时不能调用此方法；<br/>
	 * 2.对象的某个方法运行时通过参数传入的实例在方法执行完毕时不能调用此方法。
	 * </p>
	 */
	public void shutdown() {
		removeSession();
	}

	/**
	 * 流程运行时对象对应的绑定名。
	 */
	protected static final String WORKFLOWRUNTIME_BIND_NAME = "8EDFE7F0FB1449708061FB8B5DEB8984";
	/**
	 * 文档流程控制数据列表集合对象对应的绑定名。
	 */
	protected static final String WFDATA_BIND_NAME = "D8BF715D8A9A4281B43E020D98568601";

	/**
	 * 接收文档、自定义会话信息、流程实例号的构造器。
	 * 
	 * @param document
	 * @param session
	 * @param instance。
	 */
	private WorkflowRuntime(Document document, Session session, int instance) {
		this.m_document = document;
		this.m_instance = instance;
		this.m_parentInstance = 0;
		this.m_changedWFData = new ArrayList<WFDataWrapper>();
		this.storeSession(session);
		init();
		// 添加事件处理程序。
		List<EventHandlerInfo> eventHandlerInfoList = EventHandlerInfoProvider.getProvider(EventCategory.Workflow).provide(this, null);
		String eventHandlerType = String.format("%1$s,%2$s,%3$s,%4$s,%5$s,%6$s", EventHandler.EHT_WORKFLOWTRANSACTIONBEGIN, EventHandler.EHT_WORKFLOWTRANSACTIONEND, EventHandler.EHT_WORKFLOWDATAADDED, EventHandler.EHT_WORKFLOWDATADELETED, EventHandler.EHT_WORKFLOWDATAMARKEDDONE, EventHandler.EHT_WORKFLOWDATALEVELCHANGED);
		EventRegister.registerEventHandler(this, eventHandlerType, eventHandlerInfoList);
	}

	/**
	 * 根据文档、自定义会话、流程实例号获取流程运行时对象实例。
	 * 
	 * @param document 文档资源，必须。
	 * @param session 用户自定会话。
	 * @param instance 流程实例号。
	 * @return
	 */
	public synchronized static WorkflowRuntime getInstance(Document document, Session session, int instance) {
		if (document == null) throw new IllegalArgumentException("必须提供有效文档资源！");
		String docUNID = document.getUNID();
		DocumentBuffer documentBuffer = DocumentBuffer.getInstance();
		Document doc = documentBuffer.getDocument(docUNID);
		if (doc == null) documentBuffer.setDocument(document, Session.getUser(session).getName());
		WorkflowRuntime workflowRuntime = (WorkflowRuntime) documentBuffer.getBindObject(docUNID, WORKFLOWRUNTIME_BIND_NAME);
		if (workflowRuntime == null) {
			workflowRuntime = new WorkflowRuntime(document, session, instance);
			documentBuffer.setBindObject(docUNID, WORKFLOWRUNTIME_BIND_NAME, workflowRuntime);
		} else {
			workflowRuntime.storeSession(session);
		}

		return workflowRuntime;
	}

	/**
	 * 根据文档、自定义会话获取默认流程实例对应的流程运行时对象实例。
	 * 
	 * <p>
	 * 默认流程实例即流程实例号为0的主流程实例。
	 * </p>
	 * 
	 * @param document 文档资源，必须。
	 * @param session 用户自定会话。
	 * @return
	 */
	public static WorkflowRuntime getInstance(Document document, Session session) {
		return getInstance(document, session, 0);
	}

	/**
	 * 存储线程对应的用户自定义会话。
	 * 
	 * @param session 如
	 */
	private void storeSession(Session session) {
		if (session == null) { return; }
		long tid = Thread.currentThread().getId();
		GenericPair<Integer, Session> p = m_sessions.get(tid);
		if (p == null) {
			p = new GenericPair<Integer, Session>(1, session);
			m_sessions.put(tid, p);
		} else {
			p.setValue(session);
			p.setKey(p.getKey() + 1);
			m_sessions.put(tid, p);
		}
		if (debugable()) {
			Workflow wf = this.getCurrentWorkflow();
			Activity a = this.getCurrentActivity();
			Document doc = this.getDocument();
			String wfn = (wf == null ? NO_INIT : wf.getName());
			String an = (a == null ? NO_INIT : a.getName());
			String docn = (doc == null ? NO_INIT : doc.getName() + "(" + doc.getUNID() + ")");
			appendVerboseLog("开始流程运行时会话:线程ID=%1$s,会话计数=%2$s,引用计数=%3$s,文档=%4$s,流程=%5$s,环节=%6$s", tid, m_sessions.size(), p.getKey(), docn, wfn, an);
		}
	}

	protected static final String NO_INIT = "[未初始化]";
	protected static final String NAU = "[未知用户]";
	protected static final String NA = "[未知]";

	/**
	 * 删除线程对应的用户自定义会话。
	 * 
	 */
	private Session removeSession() {
		long tid = Thread.currentThread().getId();
		GenericPair<Integer, Session> p = m_sessions.get(tid);
		if (p != null) {
			p.setKey(p.getKey() - 1);
			if (p.getKey() <= 0) {
				m_sessions.remove(tid);
			}
		}

		Session result = (p == null ? null : p.getValue());
		if (debugable()) {
			Workflow wf = this.getCurrentWorkflow();
			Activity a = this.getCurrentActivity();
			Document doc = this.getDocument();
			String wfn = (wf == null ? NO_INIT : wf.getName());
			String an = (a == null ? NO_INIT : a.getName());
			String docn = (doc == null ? NO_INIT : doc.getName() + "(" + doc.getUNID() + ")");
			appendVerboseLog("结束流程运行时会话:线程ID=%1$s,会话计数=%2$s,引用计数=%3$s,文档=%4$s,流程=%5$s,环节=%6$s", tid, m_sessions.size(), (p == null ? NA : p.getKey()), docn, wfn, an);
		}
		return result;
	}

	/**
	 * 根据初始提供的信息初始化工作流运行时。
	 */
	private void init() {
		try {
			this.setTransaction(new IdleTransaction());
			String docUNID = this.m_document.getUNID();
			this.m_wfdata = WFDataProvider.getWFData(docUNID);

			Security s = m_document.getSecurity();
			int authorSecurityCode = -1;
			if (s == null || s.getSecurityEntries() == null || s.getSecurityEntries().isEmpty()) throw new RuntimeException("无法获取文档安全信息！");
			for (SecurityEntry se : s.getSecurityEntries()) {
				if ((se.getSecurityLevel() & SecurityLevel.Author.getIntValue()) == SecurityLevel.Author.getIntValue()) {
					authorSecurityCode = se.getSecurityCode();
					break;
				}
			}

			WorkflowHelper wfh = null;
			if (this.m_wfdata.isEmpty()) {// 如果找不到流程控制数据列表集合，则表示可能是刚新增的文档或为在开始环节还没发送出去的文档。
				int appCnt = m_document.getApplicationCount();
				Workflow wf = null;
				Resource reswf = m_document.getConfigResource("workflow");
				if (reswf != null && (reswf instanceof Workflow)) {
					wf = (Workflow) reswf;
				} else {
					Application app = null;
					for (int i = appCnt; i > 0; i--) {
						app = this.m_document.getApplication(i);
						String wfUNID = app.getWorkflow();
						wf = (Workflow) ResourceContext.getInstance().getResource(wfUNID, Workflow.class);
						if (wf != null) break;
					}
				}
				if (wf == null) throw new RuntimeException("无法获取文档绑定的流程，可能是目标流程不存在或者被删除!");
				wfh = new WorkflowHelper(wf);
				Activity begin = wfh.getBeginActivity();
				if (begin == null) throw new RuntimeException("无法获取文档绑定流程“" + wf.getName() + "”中的初始环节，请联系管理员重新配置流程!");
				this.m_currentWorkflow = wf;
				this.m_currentActivity = begin;
				Map<Integer, StringPair> map = new TreeMap<Integer, StringPair>();
				map.put(0, new StringPair(m_document.getCreated(), null));
				this.m_activityTraversalMap.put(begin.getUNID(), map);
				this.m_instance = 0;
				this.m_parentInstance = 0;
			} else {// 否则从当前办理人处获取当前流程、环节等。
				String lastActivity = null;
				String activity = null;
				int activityIndex = 0;
				String created = null;
				String accomplished = null;
				String dtMin = null;
				String dtMax = null;
				StringPair dtMinAndMaxStringPair = null;
				Map<Integer, StringPair> map = null;
				for (WFData x : this.m_wfdata) {
					if (x == null || x.getInstance() != this.m_instance) continue;
					activity = x.getActivity();
					if (activity == null || activity.length() == 0) continue;
					// 从未完成的第一个审批人控制数据中获取当前流程、环节、实例号等信息。
					if (m_currentActivity == null && x.checkLevel(WFDataLevel.Approver) && !x.getDone()) {
						this.m_currentWorkflow = ResourceContext.getInstance().getResource(x.getWorkflow(), Workflow.class);
						wfh = new WorkflowHelper(this.m_currentWorkflow);
						this.m_currentActivity = wfh.getActivity(activity);
						this.m_parentInstance = x.getParentInstance();
					}// if end
					map = this.m_activityTraversalMap.get(activity);
					if (map == null) { // 原先没有碰到的环节
						map = new TreeMap<Integer, StringPair>();
						activityIndex = 0;
						map.put(activityIndex, new StringPair());
						this.m_activityTraversalMap.put(activity, map);
					} else { // 原先有碰到的环节
						if (activity.equalsIgnoreCase(lastActivity)) { // 如果是同环节的
							activityIndex = map.size() - 1;
						} else { // 同环节不同时期的
							activityIndex = map.size();
							map.put(activityIndex, new StringPair());
						}
					}
					dtMinAndMaxStringPair = map.get(activityIndex);
					if (dtMinAndMaxStringPair == null) {
						dtMinAndMaxStringPair = new StringPair();
						map.put(activityIndex, dtMinAndMaxStringPair);
					}
					dtMin = dtMinAndMaxStringPair.getKey();
					dtMax = dtMinAndMaxStringPair.getValue();
					created = x.getCreated();
					accomplished = x.getAccomplished();
					if (accomplished == null || accomplished.length() == 0) accomplished = x.getCreated();
					dtMin = (dtMin == null ? created : (dtMin.compareToIgnoreCase(created) > 0 ? created : dtMin));
					dtMax = (dtMax == null ? accomplished : (dtMax.compareToIgnoreCase(accomplished) < 0 ? accomplished : dtMax));
					dtMinAndMaxStringPair.setKey(dtMin);
					dtMinAndMaxStringPair.setValue(dtMax);
					lastActivity = x.getActivity();
				}// for end
					// 如果获取不到当前环节，则尝试使用第一个控制数据中的流程和环节信息（在流程结束的时候会调用到）
				if (m_currentActivity == null && this.m_wfdata.get(0) != null) {
					WFData wfdata = this.m_wfdata.get(0);
					this.m_currentWorkflow = ResourceContext.getInstance().getResource(wfdata.getWorkflow(), Workflow.class);
					wfh = new WorkflowHelper(this.m_currentWorkflow);
					this.m_currentActivity = wfh.getActivity(wfdata.getActivity());
					this.m_parentInstance = wfdata.getParentInstance();
				}
			}// else end
			if (this.m_currentWorkflow == null) throw new RuntimeException("无法获取文档当前绑定的流程，可能是目标流程不存在或者被删除！");
			if (this.m_currentActivity == null) throw new RuntimeException("无法获取文档绑定的流程的当前环节，可能是目标环节已被删除！");
			// 合并只有传阅人的环节历经次数到第一个
			for (String aid : m_activityTraversalMap.keySet()) {
				if (aid == null || aid.length() == 0) continue;
				int atc = this.getActivityTraversalCount(aid);
				if (atc <= 1) continue;
				List<Integer> toBeDeled = new ArrayList<Integer>();
				for (int i = 0; i < atc; i++) {
					List<WFData> l = this.getWFData(aid, i);
					if (l == null || l.isEmpty()) continue;
					boolean delFlag = true;
					for (WFData wfdx : l) {
						if (!wfdx.checkLevel(WFDataLevel.Reader)) {
							delFlag = false;
							break;
						}
					}
					if (delFlag) {
						toBeDeled.add(i);
					}
				}
				if (toBeDeled.size() > 0) {
					Map<Integer, StringPair> mx = m_activityTraversalMap.get(aid);
					if (mx != null) {
						for (Integer ix : toBeDeled) {
							mx.remove(ix);
						}
						TreeMap<Integer, StringPair> newtm = new TreeMap<Integer, StringPair>();
						int tidx = 0;
						for (Integer ix : mx.keySet()) {
							newtm.put(tidx, mx.get(ix));
							tidx++;
						}
						mx.clear();
						m_activityTraversalMap.put(aid, newtm);
					}
				}
			}
			// 如果处于开始环节且没有当前审批人，那么虚拟一个作者作为当前审批人。
			List<WFData> approvers = this.getContextWFData(WFDataLevel.Approver);
			m_activityCount = wfh.getActivityCount();
			if (m_currentActivity.getActivityType() == ActivityType.Begin && (approvers == null || approvers.isEmpty())) {
				if (authorSecurityCode < 0) throw new RuntimeException("无法获取文档作者信息！");
				WFData wfdata = this.newContextWFData();
				wfdata.setLevel(WFDataLevel.Approver.getIntValue());
				wfdata.setData(authorSecurityCode);
				wfdata.setCreated(m_document.getCreated());
				this.addWFData(wfdata);
				// 如果流程只有一个环节那么不让虚拟的审批人被保存。
				if (m_activityCount <= 1) {
					s.removeWorkflowLevel(authorSecurityCode, WFDataLevel.Approver.getIntValue());
					for (WFDataWrapper wfdw : this.m_changedWFData) {
						if (wfdw == null) continue;
						WFData wfd = wfdw.getWFData();
						if (wfd == null || !wfd.getUNID().equalsIgnoreCase(wfdata.getUNID())) continue;
						wfdw.persistenceState = PersistenceState.Raw;
						break;
					}
				} else {
					s.setWorkflowLevel(authorSecurityCode, WFDataLevel.Approver.getIntValue());
				}
			}
		} catch (Exception ex) {
			FileLogger.debug(ex.getMessage());
			String docunid = (this.m_document == null ? "" : this.m_document.getUNID());
			String docname = (this.m_document == null ? "" : this.m_document.getName());
			throw new RuntimeException("无法初始化文档“" + docname + "(" + docunid + ")" + "”对应的工作流运行时环境，原因是：" + ex.getMessage());
		}
	}// func end

	private List<String> verboseLogs = null;

	/**
	 * 流程运行时是否输出详细日志。
	 * 
	 * <p>
	 * 为了调试方便，可以在流程运行时输出详细日志信息（包括控制数据的增、删、改日志等）。<br/>
	 * 默认返回false（不输出日志），只有流程配置了名为“verbose”的额外参数且参数值为“true”时才会输出日志。
	 * </p>
	 * 
	 * @return boolean
	 */
	protected boolean debugable() {
		return (m_currentWorkflow == null ? false : m_currentWorkflow.getParamValueBool("verbose", false));
	}

	/**
	 * 追加并输出流程运行时调试日志。
	 * 
	 * @param format
	 * @param args
	 */
	public void appendVerboseLog(String format, Object... args) {
		if (!debugable()) return;
		synchronized (m_lock) {
			if (verboseLogs == null) verboseLogs = new ArrayList<String>();
		}
		Session s = this.getSession();
		User u = (s == null ? null : Session.getUser(this.getSession()));
		String cn = "";
		if (u != null) cn = u.getCN();
		String sc = "";
		if (u != null) sc = "(" + u.getSecurityCode() + ")";
		String log = cn + sc + "@" + DateTime.getNowDTString() + ":" + String.format(format, args);
		FileLogger.debug(log);
		verboseLogs.add(log);
	}

	/**
	 * 获取流程运行时输出的详细调试日志信息。
	 * 
	 * @return List&lt;String&gt; 如果{@link #debugable()}返回false或没有日志则可能返回null或空列表。
	 */
	public List<String> getVerboseLogs() {
		return verboseLogs;
	}

	/**
	 * 返回{@link WFData}对象对应的调试日志信息。
	 * 
	 * @param wfdata
	 * @return String
	 */
	protected static String getWFDataDebugInfo(WFData wfdata) {
		if (wfdata == null) return "[控制数据为null]";
		String params = wfdata.getParams();
		return String.format("data=%1$d,level=%2$d,subLevel=%3$d,done=%4$s,id=%5$s,sort=%6$s,parentData=%7$d,accomplished=%8$s,derivative=%9$s,params=%10$s", wfdata.getData(), wfdata.getLevel(), wfdata.getSubLevel(), wfdata.getDone() ? "true" : "false", wfdata.getUNID(), wfdata.getSort(), wfdata.getParentData(), wfdata.getAccomplished(), wfdata.getDerivative() ? "true" : "false", params);
	}
}
