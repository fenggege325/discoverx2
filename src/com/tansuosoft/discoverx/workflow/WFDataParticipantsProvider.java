/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.GroupType;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.workflow.participants.WorkflowGroupParticipantsProvider;

/**
 * 从当前用户自定义会话的当前文档对应的流程控制数据中获取相关参与者的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WFDataParticipantsProvider {

	/**
	 * 缺省构造器。
	 */
	private WFDataParticipantsProvider() {
	}

	/**
	 * 根据流程运行时获取指定的流程群组包含的参与者信息列表集合。
	 * 
	 * @param workflowRuntime 工作流运行时，必须。
	 * @param group 内置工作流群组资源，必须。
	 * @param parameters Map&lt;String, String&gt;表示参数名和参数值一一对应的额外参数。
	 * @return
	 */
	public static List<Participant> getParticipants(WorkflowRuntime workflowRuntime, Group group, Map<String, String> parameters) {
		if (workflowRuntime == null || group == null || group.getGroupType() != GroupType.Workflow) return null;
		WorkflowGroupParticipantsProvider participantsProvider = null;
		String impl = "com.tansuosoft.discoverx.workflow.participants." + group.getAlias().substring(3);
		participantsProvider = Instance.newInstance(impl, WorkflowGroupParticipantsProvider.class);
		if (participantsProvider != null) {
			participantsProvider.setWorkflowRuntime(workflowRuntime);
			if (parameters != null) {
				for (String n : parameters.keySet()) {
					group.setParameter(n, parameters.get(n));
				}
			}
			List<Participant> result = participantsProvider.provide(null, group);
			if (parameters != null) {
				for (String n : parameters.keySet()) {
					group.setParameter(n, null);
				}
			}
			return result;
		}
		return null;
	}
}

