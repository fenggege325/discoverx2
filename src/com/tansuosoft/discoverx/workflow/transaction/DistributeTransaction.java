/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 实现传阅功能的流程事务实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class DistributeTransaction extends Transaction {

	private int m_applicationFlag = 0;

	/**
	 * 返回在控制数据辅助级别中追加的额外应用程序标记。
	 * 
	 * <p>
	 * 默认为false。
	 * </p>
	 * 
	 * @return
	 */
	protected int getApplicationFlag() {
		return this.m_applicationFlag;
	}

	/**
	 * 设置在控制数据辅助级别中追加的额外应用程序标记。
	 * 
	 * @param flag
	 */
	public void setApplicationFlag(int flag) {
		this.m_applicationFlag = flag;
	}

	/**
	 * 缺省构造器。
	 */
	public DistributeTransaction() {
		super();
		this.setLogVerb("分发");
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WorkflowForm wff = wfr.getWorkflowForm();

		List<Integer> approvers = wff.getParticipants();
		if (approvers == null || approvers.isEmpty()) return null;

		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree p = null;
		int parentSecurityCode = wfr.getCurrentUserSecurityCode();
		WFData newdata = null;
		int idx = 0;
		boolean isAppFlag = (this.getApplicationFlag() != 0);
		for (int x : approvers) {
			p = ptp.getParticipantTree(x);
			if (p == null) continue;
			this.appendParticipantsForLogObject(p);
			newdata = wfr.newContextWFData();
			newdata.setData(x);
			newdata.setParentData(parentSecurityCode);
			newdata.setLevel(WFDataLevel.Reader.getIntValue());
			if (isAppFlag) newdata.setSubLevel(newdata.getSubLevel() | this.getApplicationFlag());
			newdata.setExpect(wff.getDeadline());
			newdata.setNotify(wff.getNotify());
			newdata.setSort(idx);

			wfr.addWFData(newdata);
			idx++;
		}
		this.setLogObject(this.getFormattedParticipantsForLogObject());
		return null;
	}
}

