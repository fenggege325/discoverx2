/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.WFDataParticipantsProvider;
import com.tansuosoft.discoverx.workflow.WorkflowGroupsProvider;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.participants.ActivityApprovers;

/**
 * 流程退办/退回时的用户交互实现类。
 * 
 * @author simon@tansuosoft.cn
 */
public class RejectInteraction extends InteractionTransaction {
	/**
	 * 缺省构造器。
	 */
	public RejectInteraction() {
		super();
	}

	private List<Activity> m_activities = null;
	private Map<String, List<Participant>> m_participants = null;

	/**
	 * 重载agentSupport：返回true。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#agentSupport()
	 */
	@Override
	public boolean agentSupport() {
		return true;
	}

	/**
	 * 重载getActivities：返回当前流程所有已经经历过的环节。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#getActivities()
	 */
	@Override
	public List<Activity> getActivities() {
		if (m_activities != null) return m_activities;
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		if (wfr == null) throw new RuntimeException("无法获取绑定的流程运行时。");
		List<Activity> transitions = wfr.getTraversalActivities();
		if (transitions == null || transitions.isEmpty()) return m_activities;
		for (Activity x : transitions) {
			if (x == null) continue;
			if (m_activities == null) m_activities = new ArrayList<Activity>();
			m_activities.add(x);
		}
		return m_activities;
	}

	/**
	 * 重载getSecurityCodes：返回当前流程所有已经经历过的环节的办理人范围。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#getSecurityCodes()
	 */
	@Override
	public Map<String, List<Participant>> getSecurityCodes() {
		if (m_participants != null) return m_participants;
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		if (wfr == null) throw new RuntimeException("无法获取绑定的流程运行时。");
		if (m_activities == null || m_activities.isEmpty()) getActivities();
		if (m_activities == null || m_activities.isEmpty()) return null;

		int traversalCount = 0;
		Group group = null;
		List<Participant> historyParticipants = null;
		List<Participant> list = null;
		for (Activity x : m_activities) {
			if (x == null) continue;
			traversalCount = wfr.getActivityTraversalCount(x.getUNID());
			Map<Integer, Object> query4DupMap = new HashMap<Integer, Object>();
			for (int i = 0; i < traversalCount; i++) {
				group = WorkflowGroupsProvider.getGroup("grpActivityApprovers");
				group.setParameter(ActivityApprovers.ACT_UNID_PARAM_NAME, x.getUNID());
				group.setParameter(ActivityApprovers.ACT_TIDX_PARAM_NAME, traversalCount + "");
				historyParticipants = WFDataParticipantsProvider.getParticipants(wfr, group, null);
				if (historyParticipants == null || historyParticipants.isEmpty()) continue;
				if (m_participants == null) m_participants = new HashMap<String, List<Participant>>();
				list = m_participants.get(x.getUNID());
				if (list == null) {
					list = new ArrayList<Participant>();
					m_participants.put(x.getUNID(), list);
				}
				for (Participant p : historyParticipants) {
					if (query4DupMap.containsKey(p.getSecurityCode())) continue;
					list.add(p);
					query4DupMap.put(p.getSecurityCode(), null);
				}// for end
			}// for end
		}// for end

		return m_participants;
	}
}

