/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 协办者协办完毕的流程事务实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AssistorTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public AssistorTransaction() {
		super();
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		List<WFData> wfdata = wfr.getContextWFData();
		int securityCode = wfr.getCurrentUserSecurityCode();

		for (WFData x : wfdata) {
			// 把当前协办人设置为已协办人
			if (x != null && x.getData() == securityCode && x.checkLevel(WFDataLevel.Assistor)) {
				wfr.markWFDataDone(x);
				break;
			}
		}
		return null;
	}

}

