/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 转办类型的用户办理完毕的流程事务实现类。
 * 
 * <p>
 * 参考{@link ForwardTransaction}中的说明。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ForwardApproverTransaction extends Transaction {
	/**
	 * 缺省构造器。
	 */
	public ForwardApproverTransaction() {
		super();
		this.setAutoLog(false);
		this.setAutoPersistence(false);
		this.setAutoRaiseEvent(false);
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WFData approver = wfr.getCurrentWFData(WFDataLevel.Approver);
		if (approver == null) throw new RuntimeException("必须是审批人才能执行转办审批。");
		if (!approver.checkSubLevel(WFDataSubLevel.Forward)) throw new RuntimeException("必须是转办审批人才能执行执行转办审批。");

		boolean isAgent = wfr.checkCurrentLevel(WFDataLevel.Approver, WFDataSubLevel.Agent);
		WFData clientApprover = null; // 代理人对应的委托人控制数据
		if (isAgent) {
			do {
				clientApprover = wfr.getParentWFData(approver, null);
				if (clientApprover.checkLevel(WFDataLevel.Approver) && clientApprover.checkSubLevel(WFDataSubLevel.Forward)) {
					break;
				}
				clientApprover = wfr.getParentWFData(approver, clientApprover);
			} while (clientApprover != null);
		}
		WFData realApprover = (clientApprover != null ? clientApprover : approver);
		if (!realApprover.checkSubLevel(WFDataSubLevel.Forward)) throw new RuntimeException("必须是转办审批人才能执行执行转办审批。");
		WFData forwarder = null;// 转办人控制数据
		do {
			forwarder = wfr.getParentWFData(realApprover, null);
			if (forwarder.checkLevel(WFDataLevel.Approver)) {
				break;
			}
			forwarder = wfr.getParentWFData(realApprover, forwarder);
		} while (forwarder != null);
		if (forwarder == null) throw new RuntimeException("无法获取转办人信息。");
		int forwarderSecurityCode = forwarder.getParentData(); // 转办人安全编码

		Transaction t = new NormalApproverTransaction();
		setLogMemo(this.buildForwardLogMemo(forwarderSecurityCode));
		return t;
	}

	/**
	 * 构造并返回转办目标用户对应的转发人日志备注信息。
	 * 
	 * @param clientSC
	 * @return String
	 */
	protected String buildForwardLogMemo(int forwarderSC) {
		ParticipantTree pt = ParticipantTreeProvider.getInstance().getParticipantTree(forwarderSC);
		if (pt == null) return null;
		return "由\"" + ParticipantHelper.getFormatValue(pt, "ou0\\cn") + "\"转发";
	}
}

