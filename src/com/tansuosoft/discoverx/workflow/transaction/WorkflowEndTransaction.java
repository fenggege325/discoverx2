/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 结束流程流转的流程事务实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowEndTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public WorkflowEndTransaction() {
		super();
		this.setLogVerb("流程结束");
	}

	/**
	 * 重载transact：挂起所有未办理人员。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		List<WFData> data = wfr.getContextWFData(WFDataLevel.Approver);
		if (data != null && data.size() > 0) {
			for (WFData x : data) {
				if (x == null || x.getDone()) continue;
				wfr.markWFDataLevel(x, WFDataLevel.Suspension.getIntValue(), x.getSubLevel());
			}
		}
		return null;
	}
}

