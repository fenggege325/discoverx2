/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 环节补发时的用户交互实现类。
 * 
 * @author simon@tansuosoft.cn
 */
public class ResendInteraction extends InteractionTransaction {
	/**
	 * 缺省构造器。
	 */
	public ResendInteraction() {
		super();
	}

	private List<Activity> m_activities = null;
	private Map<String, List<Participant>> m_participants = null;

	/**
	 * 重载agentSupport：返回true。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#agentSupport()
	 */
	@Override
	public boolean agentSupport() {
		return true;
	}

	/**
	 * 重载getActivities：返回当前环节。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#getActivities()
	 */
	@Override
	public List<Activity> getActivities() {
		if (m_activities != null) return m_activities;
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		if (wfr == null) throw new RuntimeException("无法获取绑定的流程运行时。");
		Activity currentActivity = wfr.getCurrentActivity();
		if (m_activities == null) m_activities = new ArrayList<Activity>();
		m_activities.add(currentActivity);
		return m_activities;
	}

	/**
	 * 重载getSecurityCodes：返回当前环节的所定义的参与者范围。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#getSecurityCodes()
	 */
	@Override
	public Map<String, List<Participant>> getSecurityCodes() {
		if (m_participants != null) return m_participants;
		getActivities();
		if (m_activities == null || m_activities.isEmpty()) return null;
		List<Participant> list = null;
		for (Activity x : m_activities) {
			list = x.getParticipants();
			if (m_participants == null) m_participants = new HashMap<String, List<Participant>>(m_activities.size());
			m_participants.put(x.getUNID(), list);
		}
		return m_participants;
	}
}

