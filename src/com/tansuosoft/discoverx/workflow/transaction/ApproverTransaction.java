/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 用户办理完毕的流程事务实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ApproverTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public ApproverTransaction() {
		super();
		this.setAutoLog(false);
		this.setAutoPersistence(false);
		this.setAutoRaiseEvent(false);
	}

	/**
	 * 重载transact：根据办理完毕的用户的不同类型分派不同处理事务。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WFData current = wfr.getCurrentWFData(WFDataLevel.Approver);
		Transaction next = null;
		if (current != null) {
			// 交办办理完毕
			if (current.checkSubLevel(WFDataSubLevel.Dispatch)) next = new DispatchApproverTransaction();

			// 转办办理完毕
			if (next == null && current.checkSubLevel(WFDataSubLevel.Forward)) next = new ForwardApproverTransaction();

			// 正常办理完毕
			if (next == null && current.checkSubLevel(WFDataSubLevel.Normal) || current.checkSubLevel(WFDataSubLevel.Equivalent) || current.checkSubLevel(WFDataSubLevel.Commutator)) next = new NormalApproverTransaction();
			if (next == null) next = new NormalApproverTransaction();
		} else {
			// 未知办理完毕
			next = new UnknownApproverTransaction();
		}

		return next;
	}
}

