/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.workflow.Activity;

/**
 * 环节转办时的用户交互实现类。
 * 
 * @author simon@tansuosoft.cn
 */
public class ForwardInteraction extends InteractionTransaction {
	/**
	 * 缺省构造器。
	 */
	public ForwardInteraction() {
		super();
	}

	private List<Activity> m_activities = null;
	private Map<String, List<Participant>> m_participants = null;

	/**
	 * 重载agentSupport：返回true。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#agentSupport()
	 */
	@Override
	public boolean agentSupport() {
		return false;
	}

	/**
	 * 重载getActivities：返回null表示不能选择环节。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#getActivities()
	 */
	@Override
	public List<Activity> getActivities() {
		return m_activities;
	}

	/**
	 * 重载getSecurityCodes：返回null表示可以从系统所有有效参与者中选择。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#getSecurityCodes()
	 */
	@Override
	public Map<String, List<Participant>> getSecurityCodes() {
		return m_participants;
	}
}

