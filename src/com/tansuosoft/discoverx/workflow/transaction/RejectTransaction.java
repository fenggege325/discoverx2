/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowHelper;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 实现退回/退办功能的流程事务实现类。
 * 
 * @author simon@tansuosoft.cn
 */
public class RejectTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public RejectTransaction() {
		super();
		this.setAutoLog(false);
		this.setAutoPersistence(false);
		this.setAutoRaiseEvent(false);
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WFData current = wfr.getCurrentWFData(WFDataLevel.Approver);
		if (current == null) throw new RuntimeException("必须是审批人才能执行退回操作。");

		WorkflowForm wff = wfr.getWorkflowForm();
		// 如果没有提供退回环节信息那么返回等待用户交互事物。
		if (wff == null || wff.getActivity() == null || wff.getActivity().length() == 0 || wff.getParticipants() == null || wff.getParticipants().isEmpty()) {
			InteractionTransaction interaction = this.getInteractionTransaction(new RejectInteraction());
			this.setAutoPersistence(false);
			this.setAutoLog(false);
			this.setAutoRaiseEvent(false);
			return interaction;
		}

		WFData approver = wfr.getCurrentWFData(WFDataLevel.Approver);
		// 删除其它审批人员
		List<WFData> wfdata = wfr.getContextWFData(WFDataLevel.Approver);
		boolean isAgent = (approver == null ? false : approver.checkSubLevel(WFDataSubLevel.Agent));
		int parentData = (approver == null ? 0 : approver.getParentData());
		if (wfdata != null && wfdata.size() > 0) {
			int currentSecurityCode = wfr.getCurrentUserSecurityCode();
			boolean marked = false;
			for (WFData x : wfdata) {
				if (x == null) continue;
				marked = false;
				x.setSubLevel(x.getSubLevel() | WFDataSubLevel.Reverse.getIntValue());
				if (isAgent) { // 如果是代理人审批完毕
					if (currentSecurityCode == x.getData() && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Agent)) {
						wfr.markWFDataDone(x); // 设置代理人完成标记
						marked = true;
					}
					if (parentData == x.getData() && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Normal)) {
						wfr.markWFDataDone(x); // 设置委托人完成标记
						marked = true;
					}
				} else { // 如果是正常办理人办理完毕
					if (currentSecurityCode == x.getData() && x.checkLevel(WFDataLevel.Approver) && !x.checkSubLevel(WFDataSubLevel.Agent)) {
						wfr.markWFDataDone(x); // 设置审批人完成标记
						marked = true;
					}
				}
				// 删除其他未完成的非传阅人
				if (!marked && !x.getDone() && !x.checkLevel(WFDataLevel.Reader)) {
					wfr.removeWFData(x);
				}
			}
		}

		Workflow wf = wfr.getCurrentWorkflow();
		WorkflowHelper wfh = new WorkflowHelper(wf);
		Activity act = wfh.getActivity(wff.getActivity());
		if (act == null) throw new RuntimeException("无法获取流程“" + wf.getName() + "”中id为+“" + wff.getActivity() + "”的目标退回环节。");
		Transaction next = new TransitionTransaction();
		next.setLogVerb("退回");
		return next;
	}
}

