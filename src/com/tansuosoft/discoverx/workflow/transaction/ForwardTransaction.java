/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataDerivativeLevel;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 实现转办功能的流程事务实现类。
 * 
 * <p>
 * 转办和交办({@link DispatchTransaction})既有相同又有不同的地方。<br/>
 * 转办和交办({@link DispatchTransaction})的相同点如下：<br/>
 * 它们都只能在同一个环节内进行；每一个转/交办审批人都有一个来源审批人（即转办人或交办人），转/交办审批人可以继续转/交办（即支持多级转/交办）。<br/>
 * 转办和交办({@link DispatchTransaction})的区别如下：<br/>
 * 交办目标用户审批完毕后会将审批权返回给其直接交办用户；而转办目标用户审批完毕后，会将审批权返回给原始转办人对应的发送人。<br/>
 * </p>
 * <p>
 * <strong>转办和交办混合使用可能导致流程走向混乱，因此不建议在某个环节同时提供转办和交办操作。</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ForwardTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public ForwardTransaction() {
		super();
		this.setLogVerb("转办");
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WFData current = wfr.getCurrentWFData(WFDataLevel.Approver);
		if (current == null) throw new RuntimeException("必须是审批人才能进行转发。");

		boolean isAgent = wfr.checkCurrentLevel(WFDataLevel.Approver, WFDataSubLevel.Agent);
		WFData clientApprover = null; // 代理人对应的委托人控制数据
		if (isAgent) {
			do {
				clientApprover = wfr.getParentWFData(current, null);
				if (clientApprover != null && clientApprover.checkLevel(WFDataLevel.Approver)) {
					break;
				}
				clientApprover = wfr.getParentWFData(current, clientApprover);
			} while (clientApprover != null);
		}
		WFData realApprover = (clientApprover != null ? clientApprover : current);
		StringBuilder sb = new StringBuilder();
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree pt = null;
		int level = 0;
		WFData parent = realApprover;
		while (parent != null) {
			level++;
			pt = ptp.getParticipantTree(parent.getData());
			if (pt != null) {
				if (sb.length() > 0) {
					sb.insert(0, "\"\",").insert(1, ParticipantHelper.getFormatValue(pt, "ou0\\cn"));
				} else {
					sb.append("\"").append(ParticipantHelper.getFormatValue(pt, "ou0\\cn")).append("\"");
				}
			}
			if (parent.checkDerivativeLevel(WFDataDerivativeLevel.Sender)) {
				break;
			}
			parent = wfr.getParentWFData(parent, null);
		}

		WorkflowForm wff = wfr.getWorkflowForm();
		// 如果没有提供转办信息那么返回等待用户交互事务。
		if (wff == null || wff.getParticipants() == null || wff.getParticipants().isEmpty()) {
			InteractionTransaction interaction = this.getInteractionTransaction(new ForwardInteraction());
			this.setAutoPersistence(false);
			this.setAutoLog(false);
			this.setAutoRaiseEvent(false);
			return interaction;
		}

		this.doneCurrentApproverContext();

		List<Integer> forwarders = wff.getParticipants();
		if (forwarders == null || forwarders.isEmpty()) throw new RuntimeException("没有提供转办目标人！");

		int parentSecurityCode = (current.checkSubLevel(WFDataSubLevel.Agent) ? current.getParentData() : current.getData());
		WFData newdata = null;
		int idx = 0;
		String created = DateTime.getNowDTString();
		for (int x : forwarders) {
			pt = ptp.getParticipantTree(x);
			if (pt == null) continue;
			this.appendParticipantsForLogObject(pt);
			newdata = wfr.newContextWFData();
			newdata.setCreated(created);
			newdata.setData(x);
			newdata.setParentData(parentSecurityCode);
			newdata.setLevel(WFDataLevel.Approver.getIntValue());
			newdata.setSubLevel(WFDataSubLevel.Forward.getIntValue());
			newdata.setExpect(wff.getDeadline());
			newdata.setNotify(wff.getNotify());
			newdata.setSort(idx);
			wfr.addWFData(newdata);
			idx++;
		}
		this.setLogObject(this.getFormattedParticipantsForLogObject());
		if (sb.length() > 0) {
			if (level > 1) {
				this.setLogMemo(sb.insert(0, "经").append("逐级").append(this.getLogVerb()).append("(").append(level).append(")").toString());
			} else {
				this.setLogMemo(sb.insert(0, "由").append(this.getLogVerb()).toString());
			}
		}
		return null;
	}
}
