/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Participation;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 实现重发/补发功能的流程事务实现类。
 * 
 * @author Simon@tansuosoft.cn
 */
public class ResendTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public ResendTransaction() {
		super();
		this.setLogVerb("补发");
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WorkflowForm wff = wfr.getWorkflowForm();

		// 如果没有提供补发信息那么返回等待用户交互事务。
		if (wff == null || wff.getActivity() == null || wff.getActivity().length() == 0 || wff.getParticipants() == null || wff.getParticipants().isEmpty()) {
			InteractionTransaction interaction = this.getInteractionTransaction(new ResendInteraction());
			this.setAutoPersistence(false);
			this.setAutoLog(false);
			this.setAutoRaiseEvent(false);
			return interaction;
		}

		List<Integer> resenders = wff.getParticipants();
		if (resenders == null || resenders.isEmpty()) throw new RuntimeException("没有提供补发的目标审批人！");
		Activity currentActivity = wfr.getCurrentActivity();
		if (currentActivity == null) throw new RuntimeException("无法获取流程当前环节！");

		Participation participation = wff.getParticipation();
		if (participation != null) this.setLogMemo(this.buildParticipationLogMemo(participation));
		int count = wfr.getActivityTraversalCount(currentActivity.getUNID());
		List<WFData> wfdata = wfr.getWFData(currentActivity.getUNID(), count - 1);
		int idx = 0;
		if (wfdata != null && wfdata.size() > 0) idx = wfdata.size();

		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree p = null;
		int parentSecurityCode = wfr.getCurrentUserSecurityCode();
		WFData newdata = null;
		String created = DateTime.getNowDTString();
		for (int x : resenders) {
			p = ptp.getParticipantTree(x);
			if (p == null) continue;
			this.appendParticipantsForLogObject(p);
			newdata = wfr.newContextWFData();
			newdata.setCreated(created);
			newdata.setData(x);
			newdata.setParentData(parentSecurityCode);
			switch (participation.getIntValue()) {
			case 4: // Participation.Series
				newdata.setLevel(WFDataLevel.Standby.getIntValue());
				break;
			case 8: // Participation.Priority
				newdata.setLevel(WFDataLevel.Approver.getIntValue());
				newdata.setSubLevel(WFDataSubLevel.Equivalent.getIntValue());
				break;
			default:
				newdata.setLevel(WFDataLevel.Approver.getIntValue());
				break;
			}
			newdata.setExpect(wff.getDeadline());
			newdata.setNotify(wff.getNotify());
			newdata.setSort(idx);
			wfr.addWFData(newdata);
			idx++;
		}
		this.setLogObject(this.getFormattedParticipantsForLogObject());

		return null;
	}
}
