/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 自动发给环节配置的转接发送者的流程事务实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class CommutatorTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public CommutatorTransaction() {
		super();
	}

	/**
	 * 重载transact：自动把转接者设置为转接审批人。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		Activity activity = wfr.getCurrentActivity();
		boolean support = activity.getCommutatorSupport();
		if (!support) return null;
		Participant p = activity.getCommutator();
		if (p == null) throw new RuntimeException("环节中没有指定转接人/中转人！");
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		List<Participant> list = ptp.getParticipants(p, session);
		if (list == null || list.isEmpty()) throw new RuntimeException("无法获取“" + p.getName() + "”对应的转接者！");
		WFData newdata = wfr.newContextWFData();
		newdata.setData(list.get(0).getSecurityCode());
		newdata.setParentData(wfr.getCurrentUserSecurityCode());
		newdata.setLevel(WFDataLevel.Approver.getIntValue());
		newdata.setSubLevel(WFDataSubLevel.Commutator.getIntValue());
		wfr.addWFData(newdata);
		this.setLogVerb("送转接人");
		this.appendParticipantsForLogObject(p);
		return null;
	}
}

