/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 标记当前用户对应的待阅为已阅的流程事务实现类。
 * 
 * <p>
 * 如果当前用户不是待阅用户，则什么也不做。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ReaderTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public ReaderTransaction() {
		super();
		this.setLogVerb("传阅");
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		List<WFData> wfdata = wfr.getWFData();
		int securityCode = wfr.getCurrentUserSecurityCode();
		if (securityCode == 0) securityCode = Session.getUser(session).getSecurityCode();
		this.setAutoPersistence(false);
		this.setAutoLog(false);
		this.setAutoRaiseEvent(false);
		boolean found = false;
		for (WFData x : wfdata) {
			// 把当前传阅人设置为已传阅人
			if (x != null && !x.getDone() && x.getData() == securityCode && x.checkLevel(WFDataLevel.Reader)) {
				wfr.markWFDataDone(x);
				found = true;
			}
		}
		if (found) {
			this.setAutoPersistence(true);
			this.setAutoLog(true);
			this.setAutoRaiseEvent(true);
		}
		return null;
	}

}

