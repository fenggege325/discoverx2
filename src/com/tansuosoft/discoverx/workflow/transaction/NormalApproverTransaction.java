/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.ActivityType;
import com.tansuosoft.discoverx.workflow.Automation;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 正常发送的用户办理完毕的流程事务实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class NormalApproverTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public NormalApproverTransaction() {
		super();
		this.setLogVerb("审批完毕");
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		List<WFData> wfdata = null;
		int currentSecurityCode = wfr.getCurrentUserSecurityCode();

		if (wfr.getWFData().size() < 2) this.setAutoLog(false);

		WorkflowForm wff = wfr.getWorkflowForm();
		Activity current = wfr.getCurrentActivity();
		if (current.getActivityType() == ActivityType.Begin || current.getActivityType() == ActivityType.End) this.setAutoLog(false);
		List<Automation> autos = wfr.getContextAutomations();
		boolean hasOtherApprover = wfr.hasOtherApprover(); // 是否还有其它审批人
		boolean isEndActivity = (current.getActivityType() == ActivityType.End); // 结束环节标记
		boolean hasAuto = (autos != null && autos.size() > 0); // 自动流转标记
		boolean commutatorSupport = current.getCommutatorSupport(); // 转接人标记
		// 如果启用了流程转接且当前用户即为转接人，则关闭流程转接标记
		if (commutatorSupport) {
			Participant cp = current.getCommutator();
			if (cp != null && cp.getSecurityCode() == currentSecurityCode) commutatorSupport = false;
		}
		// 如果没有其它有效未审批人、没有自动流转、没有启用转接人、非结束环节且没有提供新环节信息那么返回等待用户交互事务。
		if (!hasOtherApprover && !hasAuto && !commutatorSupport && !isEndActivity && (wff == null || !wff.hasValidActivityAndParticipant())) {
			InteractionTransaction interaction = this.getInteractionTransaction(new TransitionInteraction());
			this.setAutoLog(false);
			this.setAutoPersistence(false);
			this.setAutoRaiseEvent(false);
			return interaction;
		}

		this.doneCurrentApproverContext();

		// 如果没有其它有效未审批人
		if (!hasOtherApprover) {
			if (hasAuto) { // 自动流转
				this.setAutoPersistence(false);
				return new AutomationTransaction();
			} else if (commutatorSupport) {// 转接人
				this.setAutoPersistence(false);
				return new CommutatorTransaction();
			} else if (isEndActivity) {
				this.setAutoPersistence(false);
				return new WorkflowEndTransaction();
			} else { // 普通环节变更
				this.setAutoPersistence(false);
				return new TransitionTransaction();
			}
		}

		// 如果还有其它需要审批的人则返回
		wfdata = wfr.getContextWFData(WFDataLevel.Approver);
		if (wfdata != null && !wfdata.isEmpty()) return null;

		// 弹出等待审批人为待审批人
		popStandby();

		return null;
	}
}

