/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Participation;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 处理流程环节转换的流程事务实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class TransitionTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public TransitionTransaction() {
		super();
		this.setLogVerb("发送");
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WorkflowForm wff = wfr.getWorkflowForm();

		List<Integer> approvers = wff.getParticipants();
		if (approvers == null || approvers.isEmpty()) throw new RuntimeException("没有提供新审批人！");

		// 更换环节
		String newActivityId = wff.getActivity();
		if (newActivityId == null || newActivityId.length() == 0) throw new RuntimeException("没有提供目标环节！");
		Activity currentActivity = wfr.getCurrentActivity();
		if (currentActivity == null) throw new RuntimeException("无法获取原有环节！");
		if (!newActivityId.equalsIgnoreCase(currentActivity.getUNID())) {
			wfr.setCurrentActivity(newActivityId);
		}
		Activity newActivity = wfr.getCurrentActivity();
		if (newActivity == null) throw new RuntimeException("无法获取目标环节！");
		this.setLogMemo(String.format("从[%1$s]的[%2$s]到[%3$s]", wfr.getCurrentWorkflow().getName(), currentActivity.getName(), newActivity.getName()));

		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		ParticipantTree p = null;
		int parentSecurityCode = wfr.getCurrentUserSecurityCode();
		WFData newdata = null;
		int idx = 0;
		Participation participation = wff.getParticipation();
		if (participation != null) this.setLogMemo(this.buildParticipationLogMemo(participation));
		for (int x : approvers) {
			p = ptp.getParticipantTree(x);
			if (p == null) continue;
			this.appendParticipantsForLogObject(p);
			newdata = wfr.newContextWFData();
			newdata.setData(x);
			newdata.setParentData(parentSecurityCode);
			if (idx == 0) {
				newdata.setLevel(WFDataLevel.Approver.getIntValue());
				if (participation == Participation.Priority) newdata.setSubLevel(WFDataSubLevel.Equivalent.getIntValue());
			} else {
				switch (participation.getIntValue()) {
				case 4: // Participation.Series
					newdata.setLevel(WFDataLevel.Standby.getIntValue());
					break;
				case 8: // Participation.Priority
					newdata.setLevel(WFDataLevel.Approver.getIntValue());
					newdata.setSubLevel(WFDataSubLevel.Equivalent.getIntValue());
					break;
				default:
					newdata.setLevel(WFDataLevel.Approver.getIntValue());
					break;
				}
			}
			newdata.setExpect(wff.getDeadline());
			newdata.setNotify(wff.getNotify());
			newdata.setSort(idx);
			wfr.addWFData(newdata);
			idx++;
		}
		this.setLogObject(this.getFormattedParticipantsForLogObject());
		return null;
	}
}
