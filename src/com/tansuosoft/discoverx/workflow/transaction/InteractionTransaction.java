/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.serialization.JsonSerializable;
import com.tansuosoft.discoverx.util.serialization.JsonSerializer;
import com.tansuosoft.discoverx.util.serialization.SerializationFilter;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 等待用户交互的流程事务抽象类。
 * 
 * <p>
 * 用户交互是一种特殊的流程事务，此事务实际没有对流程运行时的内部控制数据做改动。<br/>
 * 此事务用于给前端用户交互界面提供必要的信息，如可用目标环节、参与者范围、审批方式等，在用户选择必要的结果后，通过{@link WorkflowForm}对象传递给实际的事务处理{@link Transaction}对象。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class InteractionTransaction extends Transaction implements JsonSerializable {
	private Exception m_exception = null; // 异常信息。

	/**
	 * 缺省构造器。
	 */
	public InteractionTransaction() {
		super();
		this.setAutoLog(false);
		this.setAutoPersistence(false);
		this.setAutoRaiseEvent(false);
	}

	/**
	 * 设置异常。
	 * 
	 * @param ex
	 */
	protected void setException(Exception ex) {
		m_exception = ex;
	}

	/**
	 * 重载transact：不允许任何操作。
	 * 
	 * <p>
	 * 具体交互实现类不允许再重载此方法。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		return null;
	}

	/**
	 * 获取交互时可用的流程环节范围。
	 * 
	 * <p>
	 * 默认返回null。<br/>
	 * 具体派生类可以重载以提供具体环节范围。
	 * </p>
	 * 
	 * @return
	 */
	public List<Activity> getActivities() {
		return null;
	}

	/**
	 * 获取交互时可用的参与者范围。
	 * 
	 * <p>
	 * 默认返回null。<br/>
	 * 具体派生类可以重载以提供具体参与者范围。
	 * </p>
	 * 
	 * @return Map&lt;String, List&lt;Participant&gt;&gt;，表示每一个可用流程环节unid对应的参与者范围，如果没有可用环节而只有参与者范围，那么可以返回包含只包含单个List&lt;Participant&gt;的结果。
	 */
	public Map<String, List<Participant>> getSecurityCodes() {
		return null;
	}

	/**
	 * 是否支持办理期限。
	 * 
	 * <p>
	 * 默认返回true。<br/>
	 * 具体派生类可以重载以关闭办理期限功能。
	 * </p>
	 * 
	 * @return
	 */
	public boolean deadlineSupport() {
		return true;
	}

	/**
	 * 是否支持催办期限。
	 * 
	 * <p>
	 * 默认返回true。<br/>
	 * 具体派生类可以重载以关闭催办期限功能。
	 * </p>
	 * 
	 * @return
	 */
	public boolean notificationSupport() {
		return true;
	}

	/**
	 * 是否启用代理人。
	 * 
	 * <p>
	 * 默认返回false。<br/>
	 * 具体派生类可以重载以启用代理人功能。
	 * </p>
	 * 
	 * @return
	 */
	public boolean agentSupport() {
		return false;
	}

	/**
	 * 是否支持一定条件下的流程操作向导完成按钮操作自动执行。
	 * 
	 * <p>
	 * 默认为true，具体派生类可以重载以启用代理人功能。<br/>
	 * 如果为true，则当以下条件满足时可以不用用户交互而自动执行流程操作向导完成按钮对应的代码。<br/>
	 * <ul>
	 * <li>没有可选择的环节或者只有一个环节；</li>
	 * <li>没有可选择的审批方式或者只有一个审批方式；</li>
	 * <li>有至少一个默认选中的参与者（即{@link Participant#getSelected()}为true）。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return
	 */
	public boolean autoDoneSupport() {
		return true;
	}

	/**
	 * 是否支持多参与者。
	 * 
	 * <p>
	 * 通常用于没有可用流程环节但是需要选择目标用户（参与者）的流程事务交互，在这种情况下，运行时能否选择多个用户由此返回结果决定。<br/>
	 * 有可用流程环节时，此属性被忽略。<br/>
	 * 默认为true，具体派生类可以重载以关闭多参与者支持。<br/>
	 * 如果为true，表示可以选择多个用户，否则只能选择一个用户。<br/>
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean multipleParticipant() {
		return true;
	}

	/**
	 * 重载toJson
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	@Override
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		if (this.m_exception != null) { return String.format("error:true,message:'%1$s'", StringUtil.encode4Json(m_exception.getMessage())); }
		List<Activity> activities = this.getActivities();
		Map<String, List<Participant>> map = this.getSecurityCodes();
		JsonSerializer json = new JsonSerializer();
		SerializationFilter filter = new SerializationFilter() {
			@Override
			public boolean filter(String propName) {
				if ("parent".equalsIgnoreCase(propName) || "security".equalsIgnoreCase(propName) || "participants".equalsIgnoreCase(propName) || "operations".equalsIgnoreCase(propName) || "automations".equalsIgnoreCase(propName) || "transitions".equalsIgnoreCase(propName)) return false;
				return true;
			}
		};
		json.setSerializationFilter(filter);
		StringWriter sw = null;
		int activityIndex = 0;
		int participantIndex = 0;
		List<Participant> participants = null;
		if (activities != null && activities.size() > 0) {
			sb.append("activities:[");
			for (Activity x : activities) {
				sw = new StringWriter();
				json.serialize(x, sw);
				sb.append(activityIndex == 0 ? "" : ",");
				sb.append("{");
				sb.append(sw.toString());
				sb.append(",").append("participants:[");
				participants = (map == null ? null : map.get(x.getUNID()));
				if (participants != null) {
					participantIndex = 0;
					for (Participant y : participants) {
						sw = new StringWriter();
						json.serialize(y, sw);
						sb.append(participantIndex == 0 ? "" : ",");
						sb.append("{");
						sb.append(sw.toString());
						sb.append("}");
						participantIndex++;
					}
				}
				sb.append("]");
				sb.append("}");
				activityIndex++;
			}
			sb.append("]");
		} else if (map != null && map.size() > 0) {
			for (String k : map.keySet()) {
				participants = map.get(k);
				if (participants != null) break;
			}
			if (participants != null) {
				sb.append("participants:[");
				participantIndex = 0;
				for (Participant y : participants) {
					sw = new StringWriter();
					json.serialize(y, sw);
					sb.append(participantIndex == 0 ? "" : ",");
					sb.append("{");
					sb.append(sw.toString());
					sb.append("}");
					participantIndex++;
				}
				sb.append("]");
			}
		}
		sb.append(sb.length() > 0 ? "," : "");
		sb.append("agentSupport:").append(this.agentSupport() ? "true" : "false");
		sb.append(",").append("deadlineSupport:").append(this.deadlineSupport() ? "true" : "false");
		sb.append(",").append("notificationSupport:").append(this.notificationSupport() ? "true" : "false");
		sb.append(",").append("autoDoneSupport:").append(this.autoDoneSupport() ? "true" : "false");
		sb.append(",").append("multipleParticipant:").append(this.multipleParticipant() ? "true" : "false");
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		if (wfr != null) {
			if (wfr.getCurrentWorkflow() != null) sb.append(",").append("workflow:'").append(wfr.getCurrentWorkflow().getUNID()).append("'");
			if (wfr.getCurrentActivity() != null) sb.append(",").append("activity:'").append(wfr.getCurrentActivity().getUNID()).append("'");
			sb.append(",").append("workflowInstance:").append(wfr.getInstance());
		}

		return sb.toString();
	}
}

