/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 实现撤回/撤办功能的流程事务实现类。
 * 
 * <p>
 * 如果当前环节所有人被撤回则流程环节会自动转到发送环节并设置当前用户为审批人。
 * </p>
 * 
 * @author simon@tansuosoft.cn
 */
public class RevokeTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public RevokeTransaction() {
		super();
		this.setLogVerb("撤回");
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		List<WFData> wfdata = wfr.getContextWFData();
		int currentSecurityCode = wfr.getCurrentUserSecurityCode();
		Activity current = wfr.getCurrentActivity();

		List<Activity> transitions = wfr.getTraversalActivities();
		// 获取当前环节的发送环节（上一个环节）
		Activity senderActivity = (transitions == null || transitions.isEmpty() ? null : transitions.get(0));

		WorkflowForm wff = wfr.getWorkflowForm();
		// 如果没有提供撤办环节信息那么返回等待用户交互事务。
		if (wff == null || wff.getParticipants() == null || wff.getParticipants().isEmpty()) {
			InteractionTransaction interaction = this.getInteractionTransaction(new RevokeInteraction());
			this.setAutoPersistence(false);
			this.setAutoLog(false);
			this.setAutoRaiseEvent(false);
			return interaction;
		}

		List<Integer> removedParticipants = wff.getParticipants();

		for (WFData x : wfdata) {
			Participant pt = ParticipantTreeProvider.getInstance().getParticipantTree(x.getData());
			if (pt == null) continue;
			// 删除撤办目标人数据
			if (x != null && removedParticipants.contains(x.getData())) {
				wfr.removeWFData(x);
				this.appendParticipantsForLogObject(pt);
			}
			// 删除撤办目标人的代理人数据
			if (x != null && x.checkSubLevel(WFDataSubLevel.Agent) && removedParticipants.contains(x.getParentData())) {
				wfr.removeWFData(x);
			}
		}

		boolean hasApprover = false;
		boolean hasStandby = false;
		wfdata = wfr.getContextWFData(WFDataLevel.Approver);
		hasApprover = (wfdata != null && !wfdata.isEmpty());
		if (!hasApprover) {
			wfdata = wfr.getContextWFData(WFDataLevel.Standby);
			hasStandby = (wfdata != null && !wfdata.isEmpty());
		}

		// 如果没有有效审批人，则将流程状态设置为当前环节的发送环节并设置当前用户为审批人
		if (!hasApprover && !hasStandby) {
			if (senderActivity == null) throw new RuntimeException("无法获取当前环节（“" + current.getName() + "”）的发送环节！");
			this.setAutoPersistence(false);

			WorkflowForm wffnew = new WorkflowForm();
			wffnew.setActivity(senderActivity.getUNID());
			List<Integer> ps = new ArrayList<Integer>(1);
			ps.add(currentSecurityCode);
			wffnew.setParticipants(ps);
			wffnew.setAgentSupport(false);
			wfr.setWorkflowForm(wffnew);

			this.setLogMemo(String.format("从[%1$s]撤回到[%2$s]", wfr.getCurrentActivity().getName(), senderActivity.getName()));

			Transaction next = new TransitionTransaction();
			next.setAutoRaiseEvent(false);
			next.setAutoLog(false);
			return next;
		}

		// 如果有等待人员则弹出等待审批人为待审批人
		if (hasStandby) popStandby();

		this.setLogObject(this.getFormattedParticipantsForLogObject());

		return null;
	}
}

