/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantType;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Automation;
import com.tansuosoft.discoverx.workflow.Participation;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataParticipantsProvider;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowForm;
import com.tansuosoft.discoverx.workflow.WorkflowGroupsProvider;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 完成环节自动流转（{@link Activity#getAutomations()}）的流程事务实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class AutomationTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public AutomationTransaction() {
		super();
		this.setAutoLog(false);
	}

	/**
	 * 重载transact：实现自动流转功能。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		Workflow workflow = wfr.getCurrentWorkflow();
		String wfUnid = workflow.getUNID();
		List<Automation> autos = wfr.getContextAutomations();
		WorkflowForm wff = wfr.getWorkflowForm();

		if (wff == null) {
			wff = new WorkflowForm();
			wff.setAgentSupport(false);
			wfr.setWorkflowForm(wff);
		}
		if (autos == null || autos.isEmpty()) return null;
		Transaction t = null;
		List<Participant> raw = null;
		List<Participant> tmp = null;
		List<Integer> result = null;
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		Group wfGroup = null;
		for (Automation x : autos) {
			if (x == null) continue;
			raw = x.getParticipants();
			if (raw == null || raw.isEmpty()) throw new RuntimeException("没有配置自动流转的参与者。");
			StringBuilder sberr = new StringBuilder();
			for (Participant p : raw) {
				if (p == null) continue;
				sberr.append(sberr.length() == 0 ? "" : ",").append(p.getName()).append("(");
				wfGroup = WorkflowGroupsProvider.getGroup(p.getSecurityCode());
				if (p.getParticipantType() == ParticipantType.Person) {
					if (result == null) result = new ArrayList<Integer>();
					if (!result.contains(p.getSecurityCode())) result.add(p.getSecurityCode());
					sberr.append("人员");
				} else if (wfGroup != null && p.getParticipantType() == ParticipantType.Group) { // 先尝试流程群组
					tmp = WFDataParticipantsProvider.getParticipants(wfr, wfGroup, null);
					sberr.append("内置流程群组");
				} else { // 否则可能是静态或动态群组或角色。
					tmp = ptp.getParticipants(p, session);
					sberr.append(p.getParticipantType() == ParticipantType.Group ? "群组" : (p.getParticipantType() == ParticipantType.Role ? "角色" : (p.getParticipantType() == ParticipantType.Organization ? "部门" : "其它")));
				}
				sberr.append(")");
				if (tmp != null && tmp.size() > 0) {
					for (Participant y : tmp) {
						if (y == null || y.getParticipantType() != ParticipantType.Person) continue;
						if (result == null) result = new ArrayList<Integer>();
						if (!result.contains(y.getSecurityCode())) result.add(y.getSecurityCode());
					}
				}// if end
			}// for end
			if (result == null || result.isEmpty()) throw new RuntimeException("无法获取自动流转的参与者，可能原因：" + (sberr.length() == 0 ? "没有配置有效自动流转参与者！" : "“" + sberr.toString() + "”不存在或因系统更新导致安全编码不匹配或无法获取到其包含的有效人员！请尝试联系系统管理员检查配置或重新选择参与者。"));
			wff.setParticipants(result);
			wff.setActivity(x.getActivity());
			wff.setParticipation(x.getParticipation());
			if (x.getWorkflow() == null || x.getWorkflow().length() == 0 || wfUnid.equalsIgnoreCase(x.getWorkflow())) { // 流程不变
				Activity current = wfr.getCurrentActivity();
				if (current != null && current.getUNID().equalsIgnoreCase(wff.getActivity())) { // 环节不变
					Participation participation = x.getParticipation();
					int parentSecurityCode = wfr.getCurrentUserSecurityCode();
					for (int i = 0; i < result.size(); i++) {
						int sc = result.get(i);
						WFData d = wfr.newContextWFData();
						d.setData(sc);
						d.setParentData(parentSecurityCode);
						if (i == 0) {
							d.setLevel(WFDataLevel.Approver.getIntValue());
							if (participation == Participation.Priority) d.setSubLevel(WFDataSubLevel.Equivalent.getIntValue());
						} else {
							switch (participation) {
							case Series:
								d.setLevel(WFDataLevel.Standby.getIntValue());
								break;
							case Priority:
								d.setLevel(WFDataLevel.Approver.getIntValue());
								d.setSubLevel(WFDataSubLevel.Equivalent.getIntValue());
								break;
							default:
								d.setLevel(WFDataLevel.Approver.getIntValue());
								break;
							}
						}
						d.setExpect(wff.getDeadline());
						d.setNotify(wff.getNotify());
						d.setSort(i);
						wfr.addWFData(d);
					}// for end
					this.setAutoLog(true);
					this.setLogVerb("自动发送");
					return null;
				}// if end
				t = new TransitionTransaction();
			} else { // 流程变更
				wff.setWorkflow(x.getWorkflow());
				t = new WorkflowInstanceAppenderTransaction();
				t.setLogVerb("转入子流程");
			}
			if (t != null) {
				this.setAutoPersistence(false);
				t.setLogVerb("自动发送");
				t.setAutoLog(true);
				t.setAutoRaiseEvent(true);
				t.setAutoPersistence(true);
				return t;
			}
		}// for end
		return null;
	}
}

