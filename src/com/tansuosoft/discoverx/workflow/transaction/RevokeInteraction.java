/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 环节撤办/撤回时的用户交互实现类。
 * 
 * @author simon@tansuosoft.cn
 */
public class RevokeInteraction extends InteractionTransaction {
	/**
	 * 缺省构造器。
	 */
	public RevokeInteraction() {
		super();
	}

	private List<Activity> m_activities = null;
	private Map<String, List<Participant>> m_participants = null;

	/**
	 * 重载agentSupport：返回false。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#agentSupport()
	 */
	@Override
	public boolean agentSupport() {
		return false;
	}

	/**
	 * 重载getActivities：返回空集合。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#getActivities()
	 */
	@Override
	public List<Activity> getActivities() {
		if (m_activities != null) return m_activities;
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		if (wfr == null) throw new RuntimeException("无法获取绑定的流程运行时。");

		// 当前环节有效审批人(包括：当前审批人、等待审批人和等效审批人)
		List<Participant> list = new ArrayList<Participant>();
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		List<WFData> wfdata = wfr.getContextWFData();
		for (WFData x : wfdata) {
			if (x == null) continue;
			// 代理人不算有效审批人
			if (x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Agent)) continue;
			// 如果有当前审批人、等待审批人、等效审批人则说明是有效审批人。
			if (x.checkLevel(WFDataLevel.Approver) || x.checkLevel(WFDataLevel.Standby)) {
				Participant pt = ptp.getParticipantTree(x.getData());
				list.add(pt);
			}
		}
		if (list == null || list.isEmpty()) throw new RuntimeException("没有可以撤回的用户！");
		m_participants = new HashMap<String, List<Participant>>(m_activities == null ? 1 : m_activities.size());
		m_participants.put("", list);
		m_activities = new ArrayList<Activity>();
		return m_activities;
	}

	/**
	 * 重载getSecurityCodes：返回当前环节允许撤回的审批人范围。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction#getSecurityCodes()
	 */
	@Override
	public Map<String, List<Participant>> getSecurityCodes() {
		if (m_participants != null) return m_participants;
		if (m_activities == null || m_activities.isEmpty()) getActivities();
		if (m_activities == null || m_activities.isEmpty()) return null;

		return m_participants;
	}
}

