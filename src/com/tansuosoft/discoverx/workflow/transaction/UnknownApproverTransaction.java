/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Transaction;

/**
 * 非审批用户执行审批完毕时的流程事务实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class UnknownApproverTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public UnknownApproverTransaction() {
		super();
	}

	/**
	 * 重载transact：不做任何操作，也不出发日志、流程事件，不执行控制数据提交。
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		// WorkflowRuntime wfr = this.getWorkflowRuntime();
		//
		// int securityCode = wfr.getCurrentUserSecurityCode();
		// if (securityCode == 0) securityCode = Session.getUser(session).getSecurityCode();
		//
		// WFData wfd = wfr.newContextWFData();
		// wfd.setDone(true);
		// wfd.setAccomplished(DateTime.getNowDTString());
		// wfd.setData(securityCode);
		// wfr.addWFData(wfd);
		this.setAutoLog(false);
		this.setAutoPersistence(false);
		this.setAutoRaiseEvent(false);
		return null;
	}

}

