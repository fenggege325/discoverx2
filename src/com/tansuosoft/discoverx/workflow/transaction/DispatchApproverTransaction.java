/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.transaction;

import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 交办类型的用户办理完毕的流程事务实现类。
 * 
 * <p>
 * 参考{@link ForwardTransaction}中的说明。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class DispatchApproverTransaction extends Transaction {

	/**
	 * 缺省构造器。
	 */
	public DispatchApproverTransaction() {
		super();
	}

	/**
	 * 重载transact
	 * 
	 * @see com.tansuosoft.discoverx.workflow.Transaction#transact(com.tansuosoft.discoverx.model.Session)
	 */
	@Override
	public Transaction transact(Session session) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WFData approver = wfr.getCurrentWFData(WFDataLevel.Approver);
		if (approver == null) throw new RuntimeException("必须是审批人才能执行交办审批。");
		if (!approver.checkSubLevel(WFDataSubLevel.Dispatch)) throw new RuntimeException("必须是交办审批人才能执行执行交办审批。");

		boolean isAgent = wfr.checkCurrentLevel(WFDataLevel.Approver, WFDataSubLevel.Agent);
		WFData clientApprover = null; // 代理人对应的委托人控制数据
		if (isAgent) {
			do {
				clientApprover = wfr.getParentWFData(approver, null);
				if (clientApprover.checkLevel(WFDataLevel.Approver) && clientApprover.checkSubLevel(WFDataSubLevel.Dispatch)) {
					break;
				}
				clientApprover = wfr.getParentWFData(approver, clientApprover);
			} while (clientApprover != null);
		}
		WFData realApprover = (clientApprover != null ? clientApprover : approver);
		if (!realApprover.checkSubLevel(WFDataSubLevel.Dispatch)) throw new RuntimeException("必须是交办审批人才能执行执行交办审批。");
		WFData dispatcher = null;// 交办人控制数据
		do {
			dispatcher = wfr.getParentWFData(realApprover, null);
			if (dispatcher.checkLevel(WFDataLevel.Approver)) {
				break;
			}
			dispatcher = wfr.getParentWFData(realApprover, dispatcher);
		} while (dispatcher != null);
		if (dispatcher == null) throw new RuntimeException("无法获取交办人信息。");
		int dispatcherSecurityCode = dispatcher.getData(); // 交办人安全编码

		doneCurrentApproverContext();

		List<WFData> wfdata = wfr.getContextWFData();
		boolean foundOther = false; // 是否发现其它同一个用户交办的办理人。
		if (wfdata != null && wfdata.size() > 0) {
			for (WFData x : wfdata) {
				if (x == null) continue;
				if (x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Dispatch) && x.getParentData() == dispatcherSecurityCode) {
					foundOther = true;
					break;
				}
			}
		}

		if (!foundOther) { // 如果没有其它同一个用户交办的办理人，那么把办理权限交给原来的交办人以及交办人的代理人。
			DateTime dtnow = new DateTime();
			String created = dtnow.toString();
			WFData forwarderAsApprover = wfr.newContextWFData();
			forwarderAsApprover.setCreated(created);
			forwarderAsApprover.setData(dispatcherSecurityCode);
			forwarderAsApprover.setParentData(dispatcher.getParentData());
			forwarderAsApprover.setLevel(WFDataLevel.Approver.getIntValue());
			forwarderAsApprover.setSubLevel(dispatcher.getSubLevel());
			forwarderAsApprover.setSort(dispatcher.getSort());
			int seconds = dispatcher.getExpectSeconds();
			DateTime dt = null;
			if (seconds > 0) {
				dt = new DateTime();
				dt.adjustSecond(seconds);
				forwarderAsApprover.setExpect(dt.toString());
			}
			seconds = dispatcher.getNotifySeconds();
			if (seconds > 0) {
				dt = new DateTime();
				dt.adjustSecond(seconds);
				forwarderAsApprover.setNotify(dt.toString());
			}
			wfr.addWFData(forwarderAsApprover);
			// 目标日志
			ParticipantTree pt = ParticipantTreeProvider.getInstance().getParticipantTree(dispatcherSecurityCode);
			if (pt != null) {
				appendParticipantsForLogObject(pt);
				setLogObject(this.getFormattedParticipantsForLogObject());
			}
		}
		setLogVerb("审批完毕");
		setLogMemo(this.buildDispatchLogMemo(dispatcherSecurityCode));
		return null;
	}

	/**
	 * 构造并返回交办目标用户对应的交办人日志备注信息。
	 * 
	 * @param clientSC
	 * @return String
	 */
	protected String buildDispatchLogMemo(int dispatcherSC) {
		ParticipantTree pt = ParticipantTreeProvider.getInstance().getParticipantTree(dispatcherSC);
		if (pt == null) return null;
		return "由\"" + ParticipantHelper.getFormatValue(pt, "ou0\\cn") + "\"交办";
	}
}
