/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.document.DocumentBuffer;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.util.Instance;

/**
 * 提供指定文档对应流程控制数据列表集合的类。
 * 
 * <p>
 * <strong>系统中所有需要流程控制数据列表集合的地方应通过此对象获取！</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
class WFDataProvider {
	private static final String impl = "com.tansuosoft.discoverx.dao.impl.WFDataDeserializer";

	/**
	 * 缺省构造器。
	 */
	private WFDataProvider() {
	}

	/**
	 * 根据文档UNID获取其对应所有流程控制数据列表集合。
	 * 
	 * @param docUNID String，文档UNID，必须。
	 * @return List&lt;WFData&gt;<br/>
	 *         <strong>返回的结果列表中的控制数据需按照创建日期时间倒序排序，如果创建日期时间相同，则按照其排序号升序排序。</strong>
	 */
	@SuppressWarnings("unchecked")
	public static List<WFData> getWFData(String docUNID) {
		if (docUNID == null || docUNID.length() == 0) return null;
		List<WFData> result = (List<WFData>) DocumentBuffer.getInstance().getBindObject(docUNID, WorkflowRuntime.WFDATA_BIND_NAME);
		Object obj = null;
		if (result == null) {
			DBRequest dbr = Instance.newInstance(impl, DBRequest.class);
			if (dbr == null) throw new RuntimeException("无法获取“" + impl + "”对应的数据库请求！");
			dbr.setParameter(DBRequest.PUNID_PARAM_NAME, docUNID);
			dbr.sendRequest();
			obj = dbr.getResult();
			if (obj != null && obj instanceof WFData) {
				result = new ArrayList<WFData>();
				result.add((WFData) obj);
			} else if (obj != null && obj instanceof List) {
				result = (List<WFData>) dbr.getResult();
			}
			if (result == null) result = new ArrayList<WFData>();
			DocumentBuffer.getInstance().setBindObject(docUNID, WorkflowRuntime.WFDATA_BIND_NAME, result);
		}
		return result;
	}

	/**
	 * 重新加载文档UNID对应的所有流程控制数据列表集合。
	 * 
	 * @param docUNID
	 * @return
	 */
	public static List<WFData> reloadWFData(String docUNID) {
		DocumentBuffer.getInstance().setBindObject(docUNID, WorkflowRuntime.WFDATA_BIND_NAME, null);
		return getWFData(docUNID);
	}
}

