/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 流程控制数据级别（权限）主类型枚举值。
 * 
 * <p>
 * 这些值也统称用户在指定文档中的流程状态值（简称流程状态）或流程权限，请参考{@link WFDataSubLevel}、{@link WFDataDerivativeLevel}。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public enum WFDataLevel implements EnumBase {
	/**
	 * 审批人（1）。
	 */
	Approver(1),
	/**
	 * 传阅人（2）。
	 */
	Reader(2),
	/**
	 * 辅助审批人，保留使用（4）。
	 */
	Assistor(4),
	/**
	 * 等待审批人（8）。
	 * 
	 * <p>
	 * 通常表示多人顺序审批时后续等待审批人的标记。
	 * </p>
	 */
	Standby(8),
	/**
	 * 挂起审批人（16）。
	 * 
	 * <p>
	 * 通常表示审批权限被挂起的标记。
	 * </p>
	 */
	Suspension(16),
	/**
	 * 表示无具体意义的标记（0）。
	 * 
	 * <p>
	 * 通常用于标记一些不需要参与流程处理的自定义控制数据。
	 * </p>
	 */
	Unknown(0);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	WFDataLevel(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return WFDataLevel
	 */
	public WFDataLevel parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (WFDataLevel s : WFDataLevel.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return WFDataLevel.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return WFDataLevel
	 */
	public static WFDataLevel parse(int v) {
		for (WFDataLevel s : WFDataLevel.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

