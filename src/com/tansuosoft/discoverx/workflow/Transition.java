/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

/**
 * 流程环节转换(出口)相关属性对应的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Transition implements Cloneable, Comparable<Transition> {
	/**
	 * 缺省构造器。
	 */
	public Transition() {
	}

	/**
	 * 接收环节对象的构造器。
	 * 
	 * @param a
	 */
	protected Transition(Activity a) {
		if (a != null) {
			this.m_UNID = a.getUNID();
			this.m_title = a.getName();
			this.m_sort = a.getSort();
		}
	}

	private String m_UNID = null; // 转换环节UNID。
	private String m_condition = null; // 转换条件公式。
	private String m_title = null; // 转换环节名称，可选。
	private int m_sort = 0; // 排序号，默认为0。

	/**
	 * 返回转换环节UNID。
	 * 
	 * @return String
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置转换环节UNID。
	 * 
	 * @param UNID String
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回转换条件公式。
	 * 
	 * <p>
	 * 如果配置了公式，必须公式结果返回true才能启用此出口。
	 * </p>
	 * 
	 * @return String
	 */
	public String getCondition() {
		return this.m_condition;
	}

	/**
	 * 设置转换条件公式。
	 * 
	 * @param condition String
	 */
	public void setCondition(String condition) {
		this.m_condition = condition;
	}

	/**
	 * 返回转换环节名称。
	 * 
	 * <p>
	 * 通常由系统运行时从对应环节中获取。
	 * </p>
	 * 
	 * @return String
	 */
	public String getTitle() {
		return this.m_title;
	}

	/**
	 * 设置转换环节名称。
	 * 
	 * @param title String
	 */
	public void setTitle(String title) {
		this.m_title = title;
	}

	/**
	 * 返回排序号，默认为0。
	 * 
	 * <p>
	 * 通常由系统运行时从对应环节中获取。
	 * </p>
	 * 
	 * @return int
	 */
	public int getSort() {
		return this.m_sort;
	}

	/**
	 * 设置排序号。
	 * 
	 * @param sort int
	 */
	public void setSort(int sort) {
		this.m_sort = sort;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Transition x = new Transition();

		x.setSort(this.getSort());
		x.setCondition(this.getCondition());
		x.setTitle(this.getTitle());
		x.setUNID(this.getUNID());

		return x;
	}

	/**
	 * 比较：先比较排序号，再比较标题，最后比较UNID。
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Transition o) {
		if (o == null) return 1;
		int result = this.getSort() - o.getSort();
		result = (result == 0 ? 0 : result > 0 ? 1 : -1);
		if (result == 0) {
			result = (m_title == null ? (o.getTitle() == null ? 0 : -1) : this.getTitle().compareToIgnoreCase(o.getTitle()));
			if (result == 0) return (m_UNID == null ? (o.getUNID() == null ? 0 : -1) : m_UNID.compareToIgnoreCase(o.getUNID()));
		}

		return result;
	}
}

