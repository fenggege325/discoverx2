/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.bll.view.XmlResourceOrderByComparator;
import com.tansuosoft.discoverx.model.Operation;
import com.tansuosoft.discoverx.model.OperationComparator;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.Resource;

/**
 * 描述工作流包含的某一个具体活动（也叫流程节点或流程环节）相关属性的资源类。
 * 
 * <p>
 * 流程环节资源一般包含于流程资源中，孤立的流程环节资源没有意义。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class Activity extends Resource implements Cloneable, Comparable<Activity> {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 6991143705428299903L;

	/**
	 * 缺省构造器。
	 */
	public Activity() {
		super();
	}

	private ActivityType m_activityType = ActivityType.Normal; // 当前环节类型，默认为中间环节。
	private int m_participation = Participation.Single.getIntValue(); // 审批方式，默认为单人审批。
	private List<Automation> m_automations = null; // 自动流转选项。
	private List<Transition> m_transitions = new ArrayList<Transition>(); // 环节出口。
	private List<Participant> m_participants = new ArrayList<Participant>(); // 环节参与者（处理人员）范围。
	private int m_left = 0; // 可视化流程配置时对应图形化节点的左边（x轴）位置。
	private int m_top = 0; // 可视化流程配置时对应图形化节点的顶部（y轴）位置。
	private List<Operation> m_operations = null; // 操作列表
	private boolean m_commutatorSupport = false; // 是否启用环节办理完毕后由指定参与者进行转接发送。
	private Participant m_commutator = null; // 环节办理完毕后的转接发送参与者。

	/**
	 * 检查当前节点环节类型是否为指定环节类型。
	 * 
	 * <p>
	 * 系统将没有任何出口的环节视为结束环节。
	 * </p>
	 * 
	 * @param activityType {@link ActivityType}
	 * @return
	 */
	public boolean checkActivityType(ActivityType activityType) {
		if ((this.m_transitions == null || this.m_transitions.isEmpty()) && activityType.getIntValue() == ActivityType.End.getIntValue()) return true;
		if (this.m_activityType == null && activityType != null) return false;
		if (this.m_activityType != null && activityType == null) return false;
		return (this.m_activityType.getIntValue() == activityType.getIntValue());
	}

	/**
	 * 返回当前环节类型，默认为普通环节{@link ActivityType}。
	 * 
	 * <p>
	 * 每个流程必须有且只有一个开始环节，同时可以存在多个中间普通环节或结束环节。
	 * </p>
	 * 
	 * @return ActivityType
	 */
	public ActivityType getActivityType() {
		return this.m_activityType;
	}

	/**
	 * 设置当前环节类型，默认为中间环节。
	 * 
	 * @param activityType ActivityType
	 */
	public void setActivityType(ActivityType activityType) {
		this.m_activityType = activityType;
	}

	/**
	 * 返回环节支持的审批方式。
	 * 
	 * <p>
	 * 环节支持的审批方式表示在某个环节内，可以有几个参与者参与处理（审批），并且这些参与者按怎样的顺序（如顺序、并行）进行审批。<br/>
	 * 可以是{@link Participation}中某一个值或其中几个值的组合。<br/>
	 * 默认为单人审批。
	 * </p>
	 * 
	 * @see Participation
	 * @return int
	 */
	public int getParticipation() {
		return this.m_participation;
	}

	/**
	 * 设置环节支持的审批方式。
	 * 
	 * @see Activity#getParticipation()
	 * @param participation int
	 */
	public void setParticipation(int participation) {
		this.m_participation = participation;
	}

	/**
	 * 返回自动流转选项。
	 * 
	 * <p>
	 * 自动流转选项具有比配置的环节出口选项更高的优先级。
	 * </p>
	 * 
	 * @return List&lt;Automation&gt;
	 */
	public List<Automation> getAutomations() {
		return this.m_automations;
	}

	/**
	 * 设置自动流转选项。
	 * 
	 * <p>
	 * 可选。
	 * </p>
	 * 
	 * @param automations List&lt;Automation&gt;
	 */
	public void setAutomations(List<Automation> automations) {
		this.m_automations = automations;
	}

	/**
	 * 返回环节出口。
	 * 
	 * <p>
	 * 环节出口即此环节完成后，流程进入的下一个可能环节的相关信息，系统将没有任何出口的环节视为结束环节。
	 * </p>
	 * 
	 * @see Transition
	 * @return List<Transition>
	 */
	public List<Transition> getTransitions() {
		if (m_transitions != null && m_transitions.size() > 1) {
			Collections.sort(m_transitions);
		}
		return this.m_transitions;
	}

	/**
	 * 设置环节出口。
	 * 
	 * <p>
	 * 可选。
	 * </p>
	 * 
	 * @param transitions List<Transition>
	 */
	public void setTransitions(List<Transition> transitions) {
		this.m_transitions = transitions;
	}

	/**
	 * 返回环节参与者（环节处理人员）范围。
	 * 
	 * <p>
	 * 环节参与者范围即在流程流转到即将进入本环节前，用户可以预先规定哪些具体人员（或具有特定部门、群组、角色的人员）才能参与本环节的处理（如填写审批意见、进行审批等）。
	 * </p>
	 * <p>
	 * 如果本环节没有选择参与者范围，则进入本环节前，用户可以从本单位的任意用户中选择。
	 * </p>
	 * <p>
	 * 从用户界面上体现，在进入本环节前，如果本环节指定了参与者范围，则其对应的用户会列出来作为待选用户列表。 <br/>
	 * 同时如果参与者范围中某一个或几个的“selected”属性（参考com.tansuosoft.discoverx.model.Participant.getSelect()）为true，那么这些参与者范围对应的具体用户会自动被设置为选择结果。<br/>
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.model.Participant
	 * @see com.tansuosoft.discoverx.model.Participant#getSelected()
	 * @see Activity#getAutoFlow()
	 * @return List<Participant>
	 */
	public List<Participant> getParticipants() {
		if (this.m_participants != null && this.m_participants.size() == 1) {
			Participant p = this.m_participants.get(0);
			if (p.getSecurityCode() <= 0) this.m_participants.clear();
		}
		return this.m_participants;
	}

	/**
	 * 设置环节参与者（环节处理人员）范围。
	 * 
	 * @param participants List<Participant>
	 */
	public void setParticipants(List<Participant> participants) {
		this.m_participants = participants;
	}

	/**
	 * 返回可视化流程配置时对应图形化节点的左边（x轴）位置，必须。
	 * 
	 * @return int
	 */
	public int getLeft() {
		return this.m_left;
	}

	/**
	 * 设置可视化流程配置时对应图形化节点的左边（x轴）位置，必须。
	 * 
	 * @param left int
	 */
	public void setLeft(int left) {
		this.m_left = left;
	}

	/**
	 * 返回可视化流程配置时对应图形化节点的顶部（y轴）位置，必须。
	 * 
	 * @return int
	 */
	public int getTop() {
		return this.m_top;
	}

	/**
	 * 设置可视化流程配置时对应图形化节点的顶部（y轴）位置，必须。
	 * 
	 * @param top int
	 */
	public void setTop(int top) {
		this.m_top = top;
	}

	/**
	 * 返回文档处于当前环节时可执行的操作信息的集合，可选。
	 * 
	 * <p>
	 * 区别于文档所属模块资源中定义的文档通用操作集合。 请参考“{@link com.tansuosoft.discoverx.model.Application#getOperations()}”
	 * </p>
	 * 
	 * @return List&lt;Operation&gt;
	 */
	public List<Operation> getOperations() {
		if (this.m_operations != null) Collections.sort(this.m_operations, new OperationComparator());
		return this.m_operations;
	}

	/**
	 * 设置文档处于当前环节时可执行的操作信息的集合，可选。
	 * 
	 * @param operations List&lt;Operation&gt;
	 */
	public void setOperations(List<Operation> operations) {
		this.m_operations = operations;
	}

	/**
	 * 返回是否启用环节办理完毕后由指定参与者进行转接发送。
	 * 
	 * @return boolean
	 */
	public boolean getCommutatorSupport() {
		return this.m_commutatorSupport;
	}

	/**
	 * 设置是否启用环节审批完毕后由指定参与者进行转接发送。
	 * 
	 * <p>
	 * 默认为false，表示由环节最后一个审批人进行发送。
	 * </p>
	 * 
	 * @param commutatorSupport boolean
	 */
	public void setCommutatorSupport(boolean commutatorSupport) {
		this.m_commutatorSupport = commutatorSupport;
	}

	/**
	 * 返回环节审批完毕后的转接发送参与者。
	 * 
	 * @return Participant
	 */
	public Participant getCommutator() {
		return this.m_commutator;
	}

	/**
	 * 设置环节审批完毕后的转接发送参与者。
	 * 
	 * <p>
	 * 如果{@link Activity#getCommutatorSupport()}为true，那么必须提供一个默认参与者用来转接发送下一个环节。<br/>
	 * 如果提供的参与者类别是部门、群组、角色等可能返回多个参与者的，则只有第一个参与者可以转接发送。
	 * </p>
	 * 
	 * @param commutator Participant
	 */
	public void setCommutator(Participant commutator) {
		this.m_commutator = commutator;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Activity x = (Activity) super.clone();

		x.setLeft(this.getLeft());
		x.setParticipation(this.getParticipation());
		x.setTop(this.getTop());
		x.setActivityType(this.getActivityType());

		x.setCommutatorSupport(this.getCommutatorSupport());
		Participant p = this.getCommutator();
		x.setCommutator((Participant) (p == null ? null : p.clone()));

		x.setParticipants(Resource.cloneList(this.getParticipants()));
		x.setOperations(Resource.cloneList(this.getOperations()));
		x.setAutomations(Resource.cloneList(this.getAutomations()));
		x.setTransitions(Resource.cloneList(this.getTransitions()));

		return x;
	}

	public static final XmlResourceOrderByComparator C = new XmlResourceOrderByComparator();

	/**
	 * 重载：比较环节以供排序。
	 * 
	 * <p>
	 * 开始环节在最前，结束环节在最后，其它环节按照{@link XmlResourceOrderByComparator}的比较结果输出。
	 * </p>
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Activity o) {
		if (o == null) return 1;
		ActivityType pt1 = this.getActivityType();
		ActivityType pt2 = o.getActivityType();
		int r = pt1.getIntValue() - pt2.getIntValue();
		if (r != 0) return (r > 0 ? 1 : -1);

		return C.compare(this, o);
	}
}

