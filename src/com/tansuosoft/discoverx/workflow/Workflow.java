/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.model.Resource;

/**
 * 描述一个工作流相关属性的资源类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Workflow extends Resource implements Cloneable {

	/**
	 * 序列化版本标识。
	 */
	private static final long serialVersionUID = 6991143705428299903L;

	/**
	 * 表示一个虚拟流程的UNID。
	 */
	public static final String VIRTUAL_WORKFLOW_UNID = "0C4B6FC149A242E48E059FAE88D3EADD";
	/**
	 * 表示存取流转选项值的参数名。
	 */
	public static final String WORKFLOW_OPTION_PARAMNAME = "wf_option";

	/**
	 * 缺省构造器。
	 */
	public Workflow() {
		super();
		m_activities = new ArrayList<Activity>();
	}

	private List<Activity> m_activities = null; // 包含的流程环节列表。
	private List<EventHandlerInfo> m_eventHandlers = null; // 包含使用此流程的文档的流程事件处理程序信息，可选。

	/**
	 * 获取流程包含的流程环节列表集合。
	 * 
	 * @return
	 */
	public List<Activity> getActivities() {
		if (this.m_activities != null && this.m_activities.size() > 1) Collections.sort(this.m_activities);
		return this.m_activities;
	}

	/**
	 * 设置流程包含的流程环节列表集合。
	 * 
	 * @param activities
	 */
	public void setActivities(List<Activity> activities) {
		this.m_activities = activities;
	}

	/**
	 * 添加一个流程环节（{@link Activity}）对象。
	 * 
	 * @param activity
	 * @return
	 */
	public boolean addActivity(Activity activity) {
		if (activity == null) return false;
		if (this.m_activities == null) this.m_activities = new ArrayList<Activity>();
		return this.m_activities.add(activity);
	}

	/**
	 * 返回绑定此流程的文档流转时的流程事件处理程序信息。
	 * 
	 * @return List&lt;EventHandlerInfo&gt;
	 */
	public List<EventHandlerInfo> getEventHandlers() {
		return this.m_eventHandlers;
	}

	/**
	 * 设置绑定此流程的文档流转时的流程事件处理程序信息。
	 * 
	 * @param eventHandlers List&lt;EventHandlerInfo&gt;
	 */
	public void setEventHandlers(List<EventHandlerInfo> eventHandlers) {
		this.m_eventHandlers = eventHandlers;
	}

	/**
	 * 重载clone：返回一个克隆的流程。
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Workflow x = (Workflow) super.clone();

		x.setActivities(Resource.cloneList(this.getActivities()));
		x.setEventHandlers(Resource.cloneList(this.getEventHandlers()));

		return x;
	}

}

