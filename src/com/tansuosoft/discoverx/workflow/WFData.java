/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import com.tansuosoft.discoverx.util.DateTime;

/**
 * 描述一条流程状态控制数据相关属性的类。
 * 
 * <p>
 * 与某份文档绑定（即punid指向同一份文档unid的）的所有控制数据集合也叫文档的流程状态。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class WFData {
	/**
	 * 缺省构造器。
	 */
	public WFData() {
		this.m_UNID = com.tansuosoft.discoverx.util.UNIDProvider.getUNID();
		this.m_created = com.tansuosoft.discoverx.util.DateTime.getNowDTString();
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param punid 所属文档UNID
	 * @param data 安全编码
	 * @param level 主级别
	 * @param subLevel 辅级别
	 * @param wf 流程UNID
	 * @param activity 流程环节UNID
	 */
	public WFData(String punid, int data, WFDataLevel level, WFDataSubLevel subLevel, String wf, String activity) {
		this();
		this.m_PUNID = punid;
		this.m_data = data;
		this.m_level = level.getIntValue();
		this.m_subLevel = subLevel.getIntValue();
		this.m_workflow = wf;
		this.m_activity = activity;
	}

	private String m_UNID = null; // 流程状态控制数据UNID。
	private String m_PUNID = null; // 所属文档UNID。
	private int m_data = 0; // 具体流程状态数据。
	private int m_level = WFDataLevel.Unknown.getIntValue(); // 主流程控制数据的权限级别。
	private int m_subLevel = WFDataSubLevel.Normal.getIntValue(); // 辅助流程控制数据的权限级别。
	private String m_workflow = null; // 流程UNID。
	private String m_activity = null; // 流程环节UNID。
	private int m_parentData = 0; // 具体流程状态数据的上级数据。
	private int m_instance = 0; // 实例号。
	private int m_parentInstance = 0; // 上级实例号。
	private String m_created = null; // 创建时的日期时间，默认为当前日期时间。
	private String m_expect = null; // 期望结束日期时间
	private String m_notify = null; // 催办日期时间
	private String m_accomplished = null; // 实际完成流程事务的日期时间。
	private String m_params = null; // 额外参数属性。
	private int m_sort = 0; // 排序号。
	private boolean m_done = false; // 是否历史参与者标记。
	private boolean m_derivative = false; // 是否计算级别标记。

	/**
	 * 返回流程状态控制数据唯一UNID。
	 * 
	 * @return
	 */
	public String getUNID() {
		return this.m_UNID;
	}

	/**
	 * 设置流程状态控制数据唯一UNID。
	 * 
	 * <p>
	 * 对象构造时会自动生成一个UNID。
	 * </p>
	 * 
	 * @param UNID
	 */
	public void setUNID(String UNID) {
		this.m_UNID = UNID;
	}

	/**
	 * 返回流程状态数据所属文档资源UNID。
	 * 
	 * @return
	 */
	public String getPUNID() {
		return this.m_PUNID;
	}

	/**
	 * 设置流程状态数据所属文档资源UNID。
	 * 
	 * @param PUNID
	 */
	public void setPUNID(String PUNID) {
		this.m_PUNID = PUNID;
	}

	/**
	 * 返回流程状态控制数据的结果值。
	 * 
	 * @return int
	 */
	public int getData() {
		return this.m_data;
	}

	/**
	 * 设置流程状态控制数据的结果值。
	 * 
	 * <p>
	 * 此数据通常是某一个（用户的）安全编码值。
	 * </p>
	 * 
	 * @param data
	 */
	public void setData(int data) {
		this.m_data = data;
	}

	/**
	 * 返回具体流程状态数据的上级数据。
	 * 
	 * <p>
	 * 常见{@link WFData#getLevel()}与{@link WFData#getSubLevel()}组合结果含意信息如下表：<br/>
	 * <table border="1" cellspacing="0" cellpadding="2" style="table-layout:fixed;border-collapse:collapse">
	 * <tr bgcolor="#ece9d8">
	 * <td width="35%">getLevel结果</td>
	 * <td width="30%">getSubLevel结果</td>
	 * <td width="35%">ParentData值的含义</td>
	 * </tr>
	 * <tr>
	 * <td>{@link WFDataLevel#Approver}</td>
	 * <td>{@link WFDataSubLevel#Normal}</td>
	 * <td>{@link WFDataDerivativeLevel#Sender}</td>
	 * </tr>
	 * <tr>
	 * <td>{@link WFDataLevel#Approver}</td>
	 * <td>{@link WFDataSubLevel#Agent}</td>
	 * <td>{@link WFDataDerivativeLevel#Client}</td>
	 * </tr>
	 * <tr>
	 * <td>{@link WFDataLevel#Approver}</td>
	 * <td>{@link WFDataSubLevel#Forward}</td>
	 * <td>{@link WFDataDerivativeLevel#Forwarder}</td>
	 * </tr>
	 * <tr>
	 * <td>{@link WFDataLevel#Approver}</td>
	 * <td>{@link WFDataSubLevel#Dispatch}</td>
	 * <td>{@link WFDataDerivativeLevel#Dispatcher}</td>
	 * </tr>
	 * <tr>
	 * <td colspan="3">其余组合可以做类似计算。<br/>
	 * 其中{@link WFData#getSubLevel()}的{@link WFDataSubLevel#Agent}具有第一优先级。<br/>
	 * 比如{@link WFData#getSubLevel()}包含{@link WFDataSubLevel#Agent}和{@link WFDataSubLevel#Forward}的组合值，则其{@link #getParentData()}返回的是委托人的安全编码（而不是转发人的安全编码）。</td>
	 * </tr>
	 * </table>
	 * </p>
	 * 
	 * @return int
	 */
	public int getParentData() {
		return this.m_parentData;
	}

	/**
	 * 设置具体流程状态数据的上级数据。
	 * 
	 * <p>
	 * 可选，表示{@link WFData#getData()}对应的安全编码是来自哪个安全编码，比如当前办理人对应的发送人的安全编码。<br/>
	 * 根据{@link WFData#getLevel()}与{@link WFData#getSubLevel()}的不同组合可以计算出此结果的不同意义。<br/>
	 * </p>
	 * 
	 * @param parentData int
	 */
	public void setParentData(int parentData) {
		this.m_parentData = parentData;
	}

	/**
	 * 返回流程控制数据的主权限级别。
	 * 
	 * @return
	 */
	public int getLevel() {
		return this.m_level;
	}

	/**
	 * 设置流程控制数据的主权限级别。
	 * 
	 * <p>
	 * 必须是{@link WFDataLevel}中某一个或几个枚举值对应数字或其组合。
	 * </p>
	 * 
	 * @param level
	 */
	public void setLevel(int level) {
		this.m_level = level;
	}

	/**
	 * 返回流程控制数据的辅权限级别。
	 * 
	 * @return
	 */
	public int getSubLevel() {
		return this.m_subLevel;
	}

	/**
	 * 返回流程控制数据的辅权限级别。
	 * 
	 * <p>
	 * 必须是{@link WFDataSubLevel}中某一个或几个枚举值对应数字或其组合。
	 * </p>
	 * 
	 * @param subLevel
	 */
	public void setSubLevel(int subLevel) {
		this.m_subLevel = subLevel;
	}

	/**
	 * 返回绑定的流程UNID。
	 * 
	 * @return
	 */
	public String getWorkflow() {
		return this.m_workflow;
	}

	/**
	 * 设置绑定的流程UNID。
	 * 
	 * @param workflow
	 */
	public void setWorkflow(String workflow) {
		this.m_workflow = workflow;
	}

	/**
	 * 返回绑定的环节UNID。
	 * 
	 * @return
	 */
	public String getActivity() {
		return this.m_activity;
	}

	/**
	 * 设置绑定的环节UNID。
	 * 
	 * @param activity
	 */
	public void setActivity(String activity) {
		this.m_activity = activity;
	}

	/**
	 * 返回控制数据创建时的日期时间。
	 * 
	 * <p>
	 * 即此控制数据对应流程事务的开始时间。
	 * </p>
	 * 
	 * @param created
	 */
	public String getCreated() {
		return this.m_created;
	}

	/**
	 * 设置控制数据创建时的日期时间。
	 * 
	 * <p>
	 * 对象构造时会自动生成当前日期时间。
	 * </p>
	 */
	public void setCreated(String created) {
		this.m_created = created;
	}

	/**
	 * 返回期望完成日期时间。
	 * 
	 * <p>
	 * 即期望此控制数据对应流程事务在此日期时间之内完成。
	 * </p>
	 * 
	 * @return
	 */
	public String getExpect() {
		return this.m_expect;
	}

	/**
	 * 设置期望完成日期时间。
	 * 
	 * <p>
	 * 可选，有支持时限的流程处理事务才有意义。
	 * </p>
	 * 
	 * @param expect
	 */
	public void setExpect(String expect) {
		this.m_expect = expect;
	}

	/**
	 * 返回开始发送催办通知的日期时间。
	 * 
	 * <p>
	 * 即在此日期时间后可以发送催办通知。
	 * </p>
	 * 
	 * @return
	 */
	public String getNotify() {
		return this.m_notify;
	}

	/**
	 * 设置开始发送催办通知的日期时间。
	 * 
	 * <p>
	 * 可选，有支持时限的流程处理事务才有意义。
	 * </p>
	 * 
	 * @param notify
	 */
	public void setNotify(String notify) {
		this.m_notify = notify;
	}

	/**
	 * 返回实际完成流程事务的日期时间。
	 * 
	 * @return String
	 */
	public String getAccomplished() {
		return this.m_accomplished;
	}

	/**
	 * 设置实际完成流程事务的日期时间。
	 * 
	 * @param accomplished String
	 */
	public void setAccomplished(String accomplished) {
		this.m_accomplished = accomplished;
	}

	/**
	 * 返回辅助参数属性。
	 * 
	 * @return
	 */
	public String getParams() {
		return this.m_params;
	}

	/**
	 * 设置辅助参数属性。
	 * 
	 * <p>
	 * 此参数内容由设置和使用者自行定义。<br/>
	 * </p>
	 * 
	 * @param params
	 */
	public void setParams(String params) {
		this.m_params = params;
	}

	/**
	 * 返回排序号。
	 * 
	 * @return
	 */
	public int getSort() {
		return this.m_sort;
	}

	/**
	 * 设置排序号。
	 * 
	 * <p>
	 * 可选，在需要排序号的事务对应的控制数据中才有意义，否则被忽略。
	 * </p>
	 * 
	 * @param sort
	 */
	public void setSort(int sort) {
		this.m_sort = sort;
	}

	/**
	 * 返回实例号。
	 * 
	 * <p>
	 * 默认为0，表示根实例（即文档初始流程实例）。
	 * </p>
	 * 
	 * @return int
	 */
	public int getInstance() {
		return this.m_instance;
	}

	/**
	 * 设置实例号。
	 * 
	 * <p>
	 * 默认为0，表示根实例，只有嵌套子流程才会有大于零的实例号。
	 * </p>
	 * 
	 * @param instance int
	 */
	public void setInstance(int instance) {
		this.m_instance = instance;
	}

	/**
	 * 返回上级实例号。
	 * 
	 * @return int
	 */
	public int getParentInstance() {
		return this.m_parentInstance;
	}

	/**
	 * 设置上级实例号。
	 * 
	 * <p>
	 * 只有嵌套的子流程的上级（父）实例号才有意义。
	 * </p>
	 * 
	 * @param parentInstance int
	 */
	public void setParentInstance(int parentInstance) {
		this.m_parentInstance = parentInstance;
	}

	/**
	 * 返回是否完成事务办理标记。
	 * 
	 * <p>
	 * 默认为false，表示还在等待办理或办理中。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getDone() {
		return this.m_done;
	}

	/**
	 * 设置是否完成事务办理标记。
	 * 
	 * @param done boolean
	 */
	public void setDone(boolean done) {
		this.m_done = done;
	}

	/**
	 * 返回是否派生（计算）出来的流程控制数据级别标记。
	 * 
	 * <p>
	 * 默认为false，表示原生权限级别，即{@link WFData#getLevel()}返回的是{@link WFDataLevel}的值。<br/>
	 * 如果为true，则表示{@link WFData#getLevel()}返回的是{@link WFDataDerivativeLevel}的值。
	 * </p>
	 * <p style="font-weight:bold">
	 * 除了特殊流程应用情况，通常派生类型的控制数据不会独立保存于数据库中（因为他们可以通过非派生类型控制数据计算出来）。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getDerivative() {
		return this.m_derivative;
	}

	/**
	 * 设置是否派生（计算）出来的流程控制数据级别标记。
	 * 
	 * @param derivative boolean
	 */
	public void setDerivative(boolean derivative) {
		this.m_derivative = derivative;
	}

	/**
	 * 检查是否包含指定流程主权限级别。
	 * 
	 * @param level {@link WFDataLevel}
	 * @return
	 */
	public boolean checkLevel(WFDataLevel level) {
		if (level.getIntValue() == 0) return (this.getLevel() == 0);
		return ((this.getLevel() & level.getIntValue()) == level.getIntValue());
	}

	/**
	 * 检查是否包含指定流程辅权限级别。
	 * 
	 * @param level {@link WFDataSubLevel}
	 * @return
	 */
	public boolean checkSubLevel(WFDataSubLevel level) {
		if (level.getIntValue() == 0) return (this.getSubLevel() == 0);
		return ((this.getSubLevel() & level.getIntValue()) == level.getIntValue());
	}

	/**
	 * 检查流程控制数据是否指定派生级别数据。
	 * 
	 * <p>
	 * 此方法返回值等同于第二个参数之为0时调用{@link #checkDerivativeLevel(WFDataDerivativeLevel, int)}的结果。
	 * </p>
	 * 
	 * @param l 要检查的派生级别，必须。
	 * @return boolean
	 */
	public boolean checkDerivativeLevel(WFDataDerivativeLevel l) {
		return checkDerivativeLevel(l, 0);
	}

	/**
	 * 检查流程控制数据是否指定派生级别数据。
	 * 
	 * @param l 要检查的派生级别，必须。
	 * @param parentData 检查派生级别的同时，检查{@link #getParentData()}的值是否与参数parentData指定的值相同，如果parentData为0，则不检查{@link #getParentData()}的值。
	 * @return boolean
	 */
	public boolean checkDerivativeLevel(WFDataDerivativeLevel l, int parentData) {
		if (l == null) return false;
		boolean parentMatch = (parentData == 0 ? true : parentData == m_parentData);
		switch (l) {
		case Sender:
			return checkLevel(WFDataLevel.Approver) && checkSubLevel(WFDataSubLevel.Normal) && parentMatch;
		case Client:
			return checkLevel(WFDataLevel.Approver) && checkSubLevel(WFDataSubLevel.Agent) && parentMatch;
		case Forwarder:
			return checkLevel(WFDataLevel.Approver) && checkSubLevel(WFDataSubLevel.Forward) && parentMatch;
		case Dispatcher:
			return checkLevel(WFDataLevel.Approver) && checkSubLevel(WFDataSubLevel.Dispatch) && parentMatch;
		default:
			return false;
		}
	}

	/**
	 * 获取从此控制数据启用的日期时间到期望结束的日期时间之间的间隔秒数。
	 * 
	 * @return int 表示间隔多少秒，为0或负数则无意义。
	 */
	public int getExpectSeconds() {
		String expect = this.getExpect();
		if (expect == null || expect.length() == 0) return 0;
		DateTime dt1 = new DateTime(this.getCreated());
		DateTime dt2 = new DateTime(expect);
		long result = dt2.getTimeMillis() - dt1.getTimeMillis();
		return (int) (result / 1000);
	}

	/**
	 * 获取从此控制数据启用的日期时间到开始发送催办的日期时间之间的间隔秒数。
	 * 
	 * @return int 表示间隔多少秒，为0或负数则无意义。
	 */
	public int getNotifySeconds() {
		String notify = this.getNotify();
		if (notify == null || notify.length() == 0) return 0;
		DateTime dt1 = new DateTime(this.getCreated());
		DateTime dt2 = new DateTime(notify);
		long result = dt2.getTimeMillis() - dt1.getTimeMillis();
		return (int) (result / 1000);
	}

	/**
	 * 获取从此控制数据启用的日期时间到实际结束的日期时间之间的间隔秒数。
	 * 
	 * @return int 表示间隔多少秒，为0或负数则无意义。
	 */
	public int getDoneSeconds() {
		String acc = this.getAccomplished();
		if (!this.getDone() || acc == null || acc.length() == 0) return 0;
		DateTime dt1 = new DateTime(this.getCreated());
		DateTime dt2 = new DateTime(acc);
		long result = dt2.getTimeMillis() - dt1.getTimeMillis();
		return (int) (result / 1000);
	}

	/**
	 * 根据当前WFData的基本信息克隆一个新的类似的WFData。
	 * 
	 * <p>
	 * 说明：UNID、日期时间、额外参数等属性值没有克隆。
	 * </p>
	 * 
	 * @return
	 */
	public WFData cloneWFData() {
		WFData wfdata = new WFData();
		wfdata.setActivity(this.getActivity());
		wfdata.setData(this.getData());
		wfdata.setParentData(this.getParentData());
		wfdata.setDerivative(this.getDerivative());
		wfdata.setDone(this.getDone());
		wfdata.setInstance(this.getInstance());
		wfdata.setParentInstance(this.getParentData());
		wfdata.setLevel(this.getLevel());
		wfdata.setSubLevel(this.getSubLevel());
		wfdata.setPUNID(this.getPUNID());
		wfdata.setWorkflow(this.getWorkflow());
		wfdata.setSort(this.getSort());
		wfdata.setExpect(this.getExpect());
		wfdata.setAccomplished(this.getAccomplished());
		wfdata.setDone(this.getDerivative());
		wfdata.setDerivative(this.getDerivative());
		wfdata.setNotify(this.getNotify());
		wfdata.setParams(this.getParams());
		return wfdata;
	}
}

