/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.event;

import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.dao.ParametersSetter;
import com.tansuosoft.discoverx.dao.RequestType;
import com.tansuosoft.discoverx.dao.SQLWrapper;
import com.tansuosoft.discoverx.db.CommandWrapper;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentOthersInfoQuery;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;
import com.tansuosoft.discoverx.workflow.transaction.WorkflowEndTransaction;

/**
 * 根据需要同步记录流程状态信息到额外查询信息表(t_extra)的流程事件实现类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowStateRecorder implements EventHandler {
	/**
	 * 缺省构造器。
	 */
	public WorkflowStateRecorder() {
	}

	/**
	 * 返回要写入流程状态信息。
	 * 
	 * @param wfr
	 * @param t
	 * @return DocumentOthersInfoQuery 如果返回null则表示没有要写入的流程状态信息。
	 */
	public static DocumentOthersInfoQuery getDocumentOthersInfoQueryDBRequest(WorkflowRuntime wfr, Transaction t) {
		if (wfr == null) return null;
		final Document doc = wfr.getDocument();
		if (doc == null) return null;
		try {
			final WorkflowState current = new WorkflowState(doc);
			if (!current.isAutoRecordStateInfo()) return null;
			// 初始化并填充当前状态信息
			Workflow wf = wfr.getCurrentWorkflow();
			Activity act = wfr.getCurrentActivity();
			List<Activity> acts = wf.getActivities();
			String actName = act.getName();

			current.setWorkflow(wf.getName());
			current.setInstance(wfr.getInstance());
			if (acts == null || acts.size() <= 1) {
				current.setApprovers(doc.getState() > DocumentState.Signed.getIntValue() ? DocumentOthersInfoQuery.SPACE : StringUtil.stringRightBack(doc.getCreator(), User.SEPARATOR));
				current.setActivity(DocumentOthersInfoQuery.NA);
			} else {
				if (t != null && t instanceof WorkflowEndTransaction) {
					actName = DocumentOthersInfoQuery.WFEND;
				} else {
					actName = act.getName();
				}
				current.setActivity(actName);
				current.setApprovers(getWorkflowRuntimeUserCN(wfr, WFDataLevel.Approver));
				current.setReaders(getWorkflowRuntimeUserCN(wfr, WFDataLevel.Reader));
				current.setStandbys(getWorkflowRuntimeUserCN(wfr, WFDataLevel.Standby));
				current.setAssistors(getWorkflowRuntimeUserCN(wfr, WFDataLevel.Assistor));
			}
			return current;
		} catch (Exception ex) {
			FileLogger.error(ex);
		}
		return null;
	}

	/**
	 * 重载：保留使用。
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof WorkflowEventArgs)) return;
		WorkflowEventArgs wfea = (WorkflowEventArgs) e;
		WorkflowRuntime wfr = wfea.getWorkflowRuntime();
		if (wfr == null) return;
		final Document doc = wfr.getDocument();
		if (doc == null) return;
		try {
			Transaction t = wfea.getTransaction();
			final WorkflowState current = (WorkflowState) getDocumentOthersInfoQueryDBRequest(wfr, t);
			if (current == null) return;
			DBRequest dbr = new DBRequest() {
				@Override
				protected SQLWrapper buildSQL() {
					SQLWrapper r = new SQLWrapper();
					r.setSql("update t_document set c_others=?");
					r.setParameterized(true);
					r.setRequestType(RequestType.NonQuery);
					return r;
				}
			};
			dbr.setParametersSetter(new ParametersSetter() {
				@Override
				public void setParameters(DBRequest request, CommandWrapper cw) {
					try {
						String v = request.getParamValueString(DBRequest.OBJECT_PARAM_NAME, "");
						cw.setString(1, v);
					} catch (Exception e) {
						FileLogger.error(e);
					}
				}
			});
			dbr.setParameter(DBRequest.OBJECT_PARAM_NAME, current.toString());
			dbr.sendRequest();
			if (dbr.getResultLong() <= 0) FileLogger.debug("为文档“%1$s”同步记录流程状态信息时没有写入结果！");
		} catch (Exception ex) {
			FileLogger.debug("为文档“%1$s”同步记录流程状态信息时发生错误：%2$s", doc.getName(), ex.getMessage());
		}
	}

	/**
	 * 通过wfr的{@link WorkflowRuntime#getContextWFData(WFDataLevel)}方法获取{@link WFDataLevel}为level指定的值对应的参与者的通用名（姓名），多个用半角逗号分隔。
	 * 
	 * @param wfr
	 * @param level
	 * @return
	 */
	private static String getWorkflowRuntimeUserCN(WorkflowRuntime wfr, WFDataLevel level) {
		List<WFData> list = wfr.getContextWFData(level);
		if (list == null || list.isEmpty()) return "";
		ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
		StringBuilder result = new StringBuilder();
		Participant p = null;
		String cn = null;
		for (WFData x : list) {
			if (x == null) continue;
			p = ptp.getParticipantTree(x.getData());
			if (p == null) continue;
			cn = ParticipantHelper.getFormatValue(p, "cn");
			result.append(result.length() > 0 ? "," : "").append(cn);
		}
		return result.toString();
	}
}

