/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.event;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider;
import com.tansuosoft.discoverx.bll.event.GlobalEventHandlerInfoProvider;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.common.event.EventHandlerInfo;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 提供流程事件处理程序信息对象的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class WorkflowEventHandlerInfoProvider extends EventHandlerInfoProvider {
	/**
	 * 缺省构造器。
	 */
	public WorkflowEventHandlerInfoProvider() {
	}

	private static final String URGEAPPROVER_CLSNAME = "com.tansuosoft.discoverx.web.app.message.UrgeApprover";

	/**
	 * 返回forObject指向的流程运行时对象（{@link WorkflowRuntime}）中当前流程和当前环节注册的所有可用事件处理程序信息对象列表集合。
	 * 
	 * @see com.tansuosoft.discoverx.bll.event.EventHandlerInfoProvider#provide(java.lang.Object, java.lang.Object)
	 */
	@Override
	public List<EventHandlerInfo> provide(Object forObject, Object tag) {
		String eventHandlerType = String.format("%1$s,%2$s,%3$s,%4$s,%5$s,%6$s", EventHandler.EHT_WORKFLOWTRANSACTIONBEGIN, EventHandler.EHT_WORKFLOWTRANSACTIONEND, EventHandler.EHT_WORKFLOWDATAADDED, EventHandler.EHT_WORKFLOWDATADELETED, EventHandler.EHT_WORKFLOWDATAMARKEDDONE, EventHandler.EHT_WORKFLOWDATALEVELCHANGED);
		if (forObject == null || !(forObject instanceof WorkflowRuntime)) return null;
		EventHandlerInfoProvider globalProvider = new GlobalEventHandlerInfoProvider();
		List<EventHandlerInfo> result = globalProvider.provide(eventHandlerType, null);
		if (result == null) result = new ArrayList<EventHandlerInfo>();

		EventHandlerInfo agentProcess = new EventHandlerInfo(AgentProcess.class.getName(), EventHandler.EHT_WORKFLOWDATAADDED, "添加对应代理人的事件处理程序", 0);
		result.add(agentProcess);

		final EventHandlerInfo notify1 = new EventHandlerInfo(URGEAPPROVER_CLSNAME, EventHandler.EHT_WORKFLOWDATAADDED, "催办通知事件处理程序", 0);
		result.add(notify1);
		final EventHandlerInfo notify2 = new EventHandlerInfo(URGEAPPROVER_CLSNAME, EventHandler.EHT_WORKFLOWDATADELETED, "催办通知事件处理程序", 0);
		result.add(notify2);
		final EventHandlerInfo notify3 = new EventHandlerInfo(URGEAPPROVER_CLSNAME, EventHandler.EHT_WORKFLOWDATALEVELCHANGED, "催办通知事件处理程序", 0);
		result.add(notify3);
		final EventHandlerInfo notify4 = new EventHandlerInfo(URGEAPPROVER_CLSNAME, EventHandler.EHT_WORKFLOWDATAMARKEDDONE, "催办通知事件处理程序", 0);
		result.add(notify4);

		EventHandlerInfo tsimnotify = new EventHandlerInfo("com.tansuosoft.discoverx.web.app.tsim.NotifyApprover", EventHandler.EHT_WORKFLOWTRANSACTIONEND, "发送通过客户端小助手通知流程待办、待阅的人员的消息", 1);
		result.add(tsimnotify);

		EventHandlerInfo mailnotify = new EventHandlerInfo("com.tansuosoft.discoverx.web.app.message.NotifyApproverByMail", EventHandler.EHT_WORKFLOWTRANSACTIONEND, "发送内部邮件通知消息", 100);
		result.add(mailnotify);

		EventHandlerInfo smsnotify = new EventHandlerInfo("com.tansuosoft.discoverx.web.app.message.NotifyApproverBySms", EventHandler.EHT_WORKFLOWTRANSACTIONEND, "发送短消息通知消息", 100);
		result.add(smsnotify);

		EventHandlerInfo wechatnotify = new EventHandlerInfo("com.tansuosoft.discoverx.app.wechat.oa.NotifyApprover", EventHandler.EHT_WORKFLOWTRANSACTIONEND, "发送微信通知消息", 100);
		result.add(wechatnotify);

		WorkflowRuntime wfr = (WorkflowRuntime) forObject;
		Document doc = wfr.getDocument();
		int appCnt = doc.getApplicationCount();
		Application app = null;
		for (int i = appCnt; i > 0; i--) {
			app = doc.getApplication(i);
			this.listAddAll(result, app.getEventHandlers());
		}

		Workflow wf = wfr.getCurrentWorkflow();

		this.listAddAll(result, wf.getEventHandlers());
		List<EventHandlerInfo> ret = new ArrayList<EventHandlerInfo>();
		for (EventHandlerInfo x : result) {
			if (x == null || eventHandlerType.indexOf(x.getEventHandlerType()) < 0) continue;
			if (!ret.contains(x)) ret.add(x);
		}
		return result;
	}
}
