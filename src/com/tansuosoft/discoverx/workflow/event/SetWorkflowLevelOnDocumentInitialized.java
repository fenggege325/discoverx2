/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.event;

import java.util.List;

import com.tansuosoft.discoverx.bll.ResourceHelper;
import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Application;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.ResourceContext;
import com.tansuosoft.discoverx.model.Security;
import com.tansuosoft.discoverx.model.SecurityEntry;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.Workflow;

/**
 * 文档构造完成后根据文档绑定流程信息决定文档权限中是否包含默认的流程待办权限的文档构造完成事件处理类。
 * 
 * <p>
 * 如果文档绑定流程只包含一个环节或者文档没有绑定的流程，则不记录用户待办。
 * </p>
 * <p>
 * 此类为系统内部使用，请勿直接配置或在代码中使用。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class SetWorkflowLevelOnDocumentInitialized implements EventHandler {

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof ResourceEventArgs)) return;
		ResourceEventArgs rea = ((ResourceEventArgs) e);
		Resource r = rea.getResource();
		if (r == null || !(r instanceof Document)) return;
		Document doc = (Document) r;

		int appCnt = doc.getApplicationCount();
		Application app = null;
		Workflow wf = null;
		String wfUNID = null;
		for (int i = appCnt; i > 0; i--) {
			app = doc.getApplication(i);
			wfUNID = app.getWorkflow();
			wf = (Workflow) ResourceContext.getInstance().getResource(wfUNID, Workflow.class);
			if (wf != null) break;
		}
		Security security = doc.getSecurity();
		Session session = rea.getSession();
		User u = Session.getUser(session);
		int removedLevel = 0;
		boolean needRemove = (wf == null || wf.getActivities() == null || wf.getActivities().size() <= 1);
		if (needRemove && security != null) {
			removedLevel = security.removeWorkflowLevel(u.getSecurityCode(), WFDataLevel.Approver.getIntValue());
			if (removedLevel < 0) {
				u = ResourceHelper.getAuthor(doc);
				if (u != null) removedLevel = security.removeWorkflowLevel(u.getSecurityCode(), WFDataLevel.Approver.getIntValue());
			}
			if (removedLevel < 0) {
				List<SecurityEntry> ses = security.getSecurityEntries();
				if (ses != null && ses.size() > 0) {
					for (SecurityEntry se : ses) {
						if (se == null) continue;
						se.setWorkflowLevel(0);
					} // for end
				}// if end
			}// if end
		}// if end
	}// func end
}// class end

