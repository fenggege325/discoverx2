/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.event;

import com.tansuosoft.discoverx.bll.participant.AgentProvider;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 根据需要添加代理人的程控制数据添加事件处理程序实现类。
 * 
 * <p>
 * 由系统内部使用。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class AgentProcess implements EventHandler {

	/**
	 * 重载：实现功能。
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof WorkflowDataEventArgs)) return;
		if (!e.getEventHandlerType().equalsIgnoreCase(EHT_WORKFLOWDATAADDED)) return;
		WorkflowDataEventArgs wdea = (WorkflowDataEventArgs) e;
		WFData wfdata = wdea.getData();
		if (wfdata == null || !wfdata.checkLevel(WFDataLevel.Approver)) return;
		if (wfdata.checkSubLevel(WFDataSubLevel.Agent)) return;
		Transaction t = wdea.getTransaction();
		if (!t.getAgentSupport()) return;
		WorkflowRuntime wfr = wdea.getWorkflowRuntime();
		AgentProvider agentProvider = AgentProvider.getInstance();
		int delegatorSc = wfdata.getData();
		Participant agent = agentProvider.getAgent(delegatorSc);
		if (agent == null) return;
		WFData agentWfdata = wfr.newContextWFData();
		agentWfdata.setData(agent.getSecurityCode());
		agentWfdata.setParentData(delegatorSc);
		agentWfdata.setLevel(wfdata.getLevel());
		agentWfdata.setSubLevel(wfdata.getSubLevel() | WFDataSubLevel.Agent.getIntValue());
		agentWfdata.setExpect(wfdata.getExpect());
		agentWfdata.setNotify(wfdata.getNotify());
		agentWfdata.setSort(wfdata.getSort());
		wfr.addWFData(agentWfdata);
	}
}
