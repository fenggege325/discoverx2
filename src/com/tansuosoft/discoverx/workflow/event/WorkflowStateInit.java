/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.event;

import com.tansuosoft.discoverx.bll.event.ResourceEventArgs;
import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.common.event.EventHandler;
import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentState;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 文档保存时初始化状态信息的事件处理程序。
 * 
 * <p>
 * 系统内部保留使用。
 * </p>
 * 
 * @deprecated 保留使用。
 * @author coca@tansuosoft.cn
 */
@Deprecated
public class WorkflowStateInit implements EventHandler {

	/**
	 * 重载handle
	 * 
	 * @see com.tansuosoft.discoverx.common.event.EventHandler#handle(java.lang.Object, com.tansuosoft.discoverx.common.event.EventArgs)
	 */
	@Override
	public void handle(Object sender, EventArgs e) {
		if (e == null || !(e instanceof ResourceEventArgs)) return;
		ResourceEventArgs rea = ((ResourceEventArgs) e);
		Resource r = rea.getResource();
		if (r == null || !(r instanceof Document)) return;
		Document doc = (Document) r;
		if (!doc.checkState(DocumentState.New)) return;
		Session s = rea.getSession();
		WorkflowRuntime wfr = null;
		try {
			wfr = WorkflowRuntime.getInstance(doc, s);
			WorkflowStateRecorder wfsr = new WorkflowStateRecorder();
			WorkflowEventArgs arg = new WorkflowEventArgs();
			arg.setWorkflowRuntime(wfr);
			wfsr.handle(wfr, arg);
		} catch (Exception ex) {
			FileLogger.error(ex);
		} finally {
			wfr.shutdown();
		}
	}// func end
}// class end

