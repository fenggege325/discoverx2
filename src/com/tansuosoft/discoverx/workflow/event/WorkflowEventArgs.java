/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.event;

import java.util.List;

import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 触发流程事务相关事件时提供给事件处理程序的包含相关信息的参数类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowEventArgs extends EventArgs {

	/**
	 * 缺省构造器。
	 */
	public WorkflowEventArgs() {
		super();
	}

	private Transaction m_transaction = null; // 触发事件的流程事务对象。
	private WorkflowRuntime m_workflowRuntime = null; // 触发事件的流程运行时。
	private List<Participant> m_extraParticipants = null; // 额外的参与者信息。

	/**
	 * 返回触发事件的流程事务对象。
	 * 
	 * @return Transaction
	 */
	public Transaction getTransaction() {
		return this.m_transaction;
	}

	/**
	 * 设置触发事件的流程事务对象。
	 * 
	 * @param transaction Transaction
	 */
	public void setTransaction(Transaction transaction) {
		this.m_transaction = transaction;
	}

	/**
	 * 返回触发事件的流程运行时。
	 * 
	 * @return WorkflowRuntime
	 */
	public WorkflowRuntime getWorkflowRuntime() {
		return this.m_workflowRuntime;
	}

	/**
	 * 设置触发事件的流程运行时。
	 * 
	 * @param workflowRuntime WorkflowRuntime
	 */
	public void setWorkflowRuntime(WorkflowRuntime workflowRuntime) {
		this.m_workflowRuntime = workflowRuntime;
	}

	/**
	 * 返回额外的参与者信息。
	 * 
	 * <p>
	 * 可选，默认为null。<br/>
	 * 一般在事务完成时触发的事件处理程序对应的参数中使用此额外参与者，比如代理人即是一种常见的额外参与者。
	 * </p>
	 * 
	 * @return List&lt;Participant&gt;
	 */
	public List<Participant> getExtraParticipants() {
		return this.m_extraParticipants;
	}

	/**
	 * 设置额外的参与者信息。
	 * 
	 * @param extraParticipants List&lt;Participant&gt;
	 */
	public void setExtraParticipants(List<Participant> extraParticipants) {
		this.m_extraParticipants = extraParticipants;
	}
}

