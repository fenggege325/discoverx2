/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.event;

/**
 * 触发流程控制数据级别变更事件时提供给事件处理程序的包含相关信息的参数类。
 * 
 * @author coca@tensosoft.com
 */
public class WorkflowDataLevelChangedEventArgs extends WorkflowDataEventArgs {

	/**
	 * 缺省构造器。
	 */
	public WorkflowDataLevelChangedEventArgs() {
		super();
	}

	private int m_oldLevel = 0; // 原来的主级别。
	private int m_newLevel = 0; // 更改后的主级别。
	private int m_oldSubLevel = 0; // 原来的辅助级别。
	private int m_newSubLevel = 0; // 更改后的辅助级别。

	/**
	 * 返回原来的主级别。
	 * 
	 * @return int
	 */
	public int getOldLevel() {
		return this.m_oldLevel;
	}

	/**
	 * 设置原来的主级别。
	 * 
	 * @param oldLevel int
	 */
	public void setOldLevel(int oldLevel) {
		this.m_oldLevel = oldLevel;
	}

	/**
	 * 返回更改后的主级别。
	 * 
	 * @return int
	 */
	public int getNewLevel() {
		return this.m_newLevel;
	}

	/**
	 * 设置更改后的主级别。
	 * 
	 * @param newLevel int
	 */
	public void setNewLevel(int newLevel) {
		this.m_newLevel = newLevel;
	}

	/**
	 * 返回原来的辅助级别。
	 * 
	 * @return int
	 */
	public int getOldSubLevel() {
		return this.m_oldSubLevel;
	}

	/**
	 * 设置原来的辅助级别。
	 * 
	 * @param oldSubLevel int
	 */
	public void setOldSubLevel(int oldSubLevel) {
		this.m_oldSubLevel = oldSubLevel;
	}

	/**
	 * 返回更改后的辅助级别。
	 * 
	 * @return int
	 */
	public int getNewSubLevel() {
		return this.m_newSubLevel;
	}

	/**
	 * 设置更改后的辅助级别。
	 * 
	 * @param newSubLevel int
	 */
	public void setNewSubLevel(int newSubLevel) {
		this.m_newSubLevel = newSubLevel;
	}

}

