/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.event;

import com.tansuosoft.discoverx.common.event.EventArgs;
import com.tansuosoft.discoverx.workflow.Transaction;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 触发流程控制数据相关事件时提供给事件处理程序的包含相关信息的参数类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowDataEventArgs extends EventArgs {

	/**
	 * 缺省构造器。
	 */
	public WorkflowDataEventArgs() {
		super();
	}

	private Transaction m_transaction = null; // 触发事件的流程事务对象。
	private WorkflowRuntime m_workflowRuntime = null; // 触发事件的流程运行时。
	private WFData m_data = null; // 关联的控制数据。

	/**
	 * 返回触发事件的流程事务对象。
	 * 
	 * @return Transaction
	 */
	public Transaction getTransaction() {
		return this.m_transaction;
	}

	/**
	 * 设置触发事件的流程事务对象。
	 * 
	 * @param transaction Transaction
	 */
	public void setTransaction(Transaction transaction) {
		this.m_transaction = transaction;
	}

	/**
	 * 返回触发事件的流程运行时。
	 * 
	 * @return WorkflowRuntime
	 */
	public WorkflowRuntime getWorkflowRuntime() {
		return this.m_workflowRuntime;
	}

	/**
	 * 设置触发事件的流程运行时。
	 * 
	 * @param workflowRuntime WorkflowRuntime
	 */
	public void setWorkflowRuntime(WorkflowRuntime workflowRuntime) {
		this.m_workflowRuntime = workflowRuntime;
	}

	/**
	 * 返回关联的控制数据。
	 * 
	 * <p>
	 * <ul>
	 * <li>对于控制数据追加事件，则为被追加的控制数据。</li>
	 * <li>对于控制数据删除事件，则为被删除的控制数据。</li>
	 * <li>对于控制数据状态变更相关事件（如被标记为完成、设置了新的权限级别等），则为被更改的控制数据。</li>
	 * </ul>
	 * </p>
	 * 
	 * @return WFData
	 */
	public WFData getData() {
		return this.m_data;
	}

	/**
	 * 设置关联的控制数据。
	 * 
	 * @param data WFData
	 */
	public void setData(WFData data) {
		this.m_data = data;
	}
}

