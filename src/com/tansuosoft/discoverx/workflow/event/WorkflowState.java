/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.event;

import com.tansuosoft.discoverx.model.Document;
import com.tansuosoft.discoverx.model.DocumentOthersInfoQuery;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 用于提供文档流程状态额外查询信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowState extends DocumentOthersInfoQuery implements Comparable<WorkflowState> {
	/**
	 * 接收目标文档的构造器。
	 */
	public WorkflowState(Document doc) {
		super(doc);
	}

	private String m_workflow = null; // 当前流程名称。
	private String m_activity = null; // 当前流程环节名称。
	private int m_instance = 0; // 当前流程实例号。
	private String m_approvers = null; // 当前所有有效审批人员。
	private String m_readers = null; // 当前所有待传阅人员。
	private String m_standbys = null; // 当前所有等待审批人员。
	private String m_assistors = null; // 当前所有有效协办人员，保留使用。

	/**
	 * 返回当前流程名称。
	 * 
	 * @return String
	 */
	public String getWorkflow() {
		return this.m_workflow;
	}

	/**
	 * 设置当前流程名称。
	 * 
	 * @param workflow String
	 */
	public void setWorkflow(String workflow) {
		this.m_workflow = workflow;
	}

	/**
	 * 返回当前流程环节名称。
	 * 
	 * @return String
	 */
	public String getActivity() {
		return this.m_activity;
	}

	/**
	 * 设置当前流程环节名称。
	 * 
	 * @param activity String
	 */
	public void setActivity(String activity) {
		this.m_activity = activity;
	}

	/**
	 * 返回当前流程实例号。
	 * 
	 * @return int
	 */
	public int getInstance() {
		return this.m_instance;
	}

	/**
	 * 设置当前流程实例号。
	 * 
	 * @param instance int
	 */
	public void setInstance(int instance) {
		this.m_instance = instance;
	}

	/**
	 * 返回当前所有有效审批人员。
	 * 
	 * <p>
	 * 返回审批人的姓名，多个用半角逗号分隔，如：“张三,李四”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getApprovers() {
		return this.m_approvers;
	}

	/**
	 * 设置当前所有有效审批人员。
	 * 
	 * @param approvers String
	 */
	public void setApprovers(String approvers) {
		this.m_approvers = approvers;
	}

	/**
	 * 返回当前所有待传阅人员。
	 * 
	 * <p>
	 * 返回审批人的姓名，多个用半角逗号分隔，如：“张三,李四”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getReaders() {
		return this.m_readers;
	}

	/**
	 * 设置当前所有待传阅人员。
	 * 
	 * @param readers String
	 */
	public void setReaders(String readers) {
		this.m_readers = readers;
	}

	/**
	 * 返回当前所有等待审批人员。
	 * 
	 * <p>
	 * 返回审批人的姓名，多个用半角逗号分隔，如：“张三,李四”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getStandbys() {
		return this.m_standbys;
	}

	/**
	 * 设置当前所有等待审批人员。
	 * 
	 * @param standbys String
	 */
	public void setStandbys(String standbys) {
		this.m_standbys = standbys;
	}

	/**
	 * 返回当前所有有效协办人员，保留使用。
	 * 
	 * <p>
	 * 返回审批人的姓名，多个用半角逗号分隔，如：“张三,李四”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getAssistors() {
		return this.m_assistors;
	}

	/**
	 * 设置当前所有有效协办人员，保留使用。
	 * 
	 * @param assistors String
	 */
	public void setAssistors(String assistors) {
		this.m_assistors = assistors;
	}

	/**
	 * 重载toString：输出保存流程状态信息到t_document.c_others字段值的结果。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("w:'").append(StringUtil.encode4Json(m_workflow)).append("'");
		sb.append(",s:'").append(StringUtil.encode4Json(m_activity)).append("'");
		sb.append(",p:'").append(StringUtil.encode4Json(m_approvers)).append("'");
		sb.append(",r:'").append(StringUtil.encode4Json(m_readers)).append("'");
		// sb.append(",t:'").append(StringUtil.encode4Json(m_standbys)).append("'");
		// sb.append(",a:'").append(StringUtil.encode4Json(m_assistors)).append("'");
		// sb.append(",i:").append(m_instance);
		return sb.toString();
	}

	/**
	 * 重载compareTo：返回{@link WorkflowState#toString()}的{@link String#compareToIgnoreCase(String)}结果。
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(WorkflowState o) {
		if (o == null) return 1;
		String strThis = this.toString();
		String strThat = o.toString();
		return strThis.compareToIgnoreCase(strThat);
	}
}

