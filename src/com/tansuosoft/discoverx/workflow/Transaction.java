/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.dao.DBRequest;
import com.tansuosoft.discoverx.model.Log;
import com.tansuosoft.discoverx.model.LogType;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.ParticipantHelper;
import com.tansuosoft.discoverx.model.ParticipantTree;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.model.User;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;
import com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction;

/**
 * 表示一个流程事务的基类。
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class Transaction {
	/**
	 * 缺省构造器。
	 */
	public Transaction() {
	}

	private WorkflowRuntime m_workflowRuntime = null; // 工作流运行时对象。
	private boolean m_autoLog = true;
	private boolean m_autoPersistence = true;
	private boolean m_autoRaiseEvent = true;
	private String m_logVerb = null;
	private String m_logObject = "";
	private StringBuilder m_logMemo = null;
	private Log m_log = null;
	private Transaction m_next = null;
	private Transaction m_prev = null;
	private Map<String, List<String>> m_formattedParticipantsForLogObjectMap = new HashMap<String, List<String>>();
	private boolean m_submitAfterTransactionEnd = false;
	private List<DBRequest> m_coRequests = null;

	/**
	 * 追加一个记录流程日志时写入日志目标的参与者信息以供格式化输出。
	 * 
	 * <p>
	 * 全部追加完成后，用{@link #getFormattedParticipantsForLogObject()}输出结果。
	 * </p>
	 * 
	 * @param p
	 */
	protected void appendParticipantsForLogObject(Participant p) {
		if (p == null) return;
		String ou0 = ParticipantHelper.getFormatValue(p, "ou0");
		if (ou0 == null || ou0.length() == 0) ou0 = "[无单位]";
		String cn = ParticipantHelper.getFormatValue(p, "cn");
		if (cn == null || cn.length() == 0) cn = "[无姓名]";
		List<String> list = m_formattedParticipantsForLogObjectMap.get(ou0);
		if (list == null) {
			list = new ArrayList<String>();
			m_formattedParticipantsForLogObjectMap.put(ou0, list);
		}
		list.add(cn);
	}

	/**
	 * 为当前流程事务构造一个事务处理日志。
	 * 
	 * @param wfdata 表示基于提供的{@link WFData}对象中包含的用户、日期时间等信息构造日志，也可以为NULL（表示基于当前用户构造日志）。
	 */
	protected void buildLog(WFData wfdata) {
		if (m_log == null) m_log = new Log();
		if (wfdata != null) {
			ParticipantTreeProvider ptp = ParticipantTreeProvider.getInstance();
			ParticipantTree pt = ptp.getParticipantTree(wfdata.getData());
			if (pt != null) m_log.setSubject(pt.getName());
			m_log.setStart(wfdata.getCreated());
			m_log.setExpect(wfdata.getExpect());
		}
	}

	/**
	 * 返回当前流程事务可能存在的事务处理日志。
	 * 
	 * @return {@link Log}，如果{@link #getAutoLog()}返回false，则返回null，如果没有调用过{@link #buildLog(WFData)}则构造一个默认（wfdata为null的)日志。
	 */
	protected Log getLog() {
		if (!this.getAutoLog()) return null;
		if (m_log == null) buildLog(null);
		m_log.setVerb(getLogVerb());
		m_log.setObject(getLogObject());
		m_log.setLogType(LogType.Workflow);
		m_log.setMessage(getLogMemo());
		if (m_workflowRuntime != null) {
			m_log.setOwner(m_workflowRuntime.getDocument().getUNID());
			if (StringUtil.isBlank(m_log.getSubject())) {
				User u = Session.getUser(m_workflowRuntime.getSession());
				if (u.isAnonymous()) {
					FileLogger.debug("匿名会话！");
				} else {
					m_log.setSubject(u.getName());
				}
			}
			Workflow wf = m_workflowRuntime.getCurrentWorkflow();
			Activity act = m_workflowRuntime.getCurrentActivity();
			m_log.setState(String.format("%1$s\\%2$s", wf == null ? "" : wf.getUNID(), act == null ? "" : act.getUNID()));
		}
		return m_log;
	}

	/**
	 * 按部门分类输出参与者格式化结果供记录流程日志时写入日志目标。
	 * 
	 * <p>
	 * 输出结果格式如下：<br/>
	 * 部门1:用户1,用户2...;部门2:用户1,用户2...;...
	 * </p>
	 * 
	 * @return
	 */
	protected String getFormattedParticipantsForLogObject() {
		StringBuilder result = new StringBuilder();
		for (String k : m_formattedParticipantsForLogObjectMap.keySet()) {
			List<String> list = m_formattedParticipantsForLogObjectMap.get(k);
			if (list == null || list.isEmpty()) continue;
			result.append(result.length() > 0 ? ";" : "").append(k).append(":");
			int idx = 0;
			for (String x : list) {
				if (x != null && x.length() > 0) {
					result.append(idx == 0 ? "" : ",").append(x);
					idx++;
				}
			}
		}
		return result.toString();
	}

	/**
	 * 获取绑定的当前流程运行时对象。
	 * 
	 * @return
	 */
	protected WorkflowRuntime getWorkflowRuntime() {
		return this.m_workflowRuntime;
	}

	/**
	 * 设置{@link WorkflowRuntime}。
	 * 
	 * @param workflowRuntime {@link WorkflowRuntime}
	 */
	protected void setWorkflowRuntime(WorkflowRuntime workflowRuntime) {
		if (workflowRuntime == null) throw new RuntimeException("必须提供工作流运行时对象！");
		this.m_workflowRuntime = workflowRuntime;
	}

	/**
	 * 返回此事务执行过程中改变的流程状态数据是否需要在事务执行完毕后马上自动持久化保存到持久层的标记。
	 * 
	 * <p>
	 * 此方法用来控制流程事务更改的流程状态是否需要在事务执行完毕后马上提交。
	 * </p>
	 * <p>
	 * 默认返回true，实现类通常通过{@link #setAutoPersistence(boolean)} 或重载此方法修改返回值以确保在需要多个事务实现类参与的事务链中由最后一个流程事务执行最终提交操作。如果返回false，那么此事务处理时对流程状态（即流程控制数据）的变动（包括增、删、改等）不会立即同步到持久层（一般是数据库记录）中。
	 * </p>
	 * <p>
	 * 如果完成某个流程事务只需一个流程事务处理实现类，那么通常应返回true以确保控制数据变更得到及时提交。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getAutoPersistence() {
		return m_autoPersistence;
	}

	/**
	 * 设置此事务执行过程中改变的流程状态数据是否需要在事务执行完毕后马上自动持久化保存到持久层的标记。
	 * 
	 * <p>
	 * 默认为true。
	 * </p>
	 * 
	 * @param autoPersistence
	 */
	public void setAutoPersistence(boolean autoPersistence) {
		m_autoPersistence = autoPersistence;
	}

	/**
	 * 返回是否自动触发流程事务完成前后的事件。
	 * 
	 * <p>
	 * 默认返回true，如果为false，则不触发事件。
	 * </p>
	 * 
	 * @return
	 */
	public boolean getAutoRaiseEvent() {
		return m_autoRaiseEvent;
	}

	/**
	 * 设置是否自动触发流程事务完成前后的事件。
	 * 
	 * <p>
	 * 默认为true。
	 * </p>
	 * 
	 * @param autoRaiseEvent
	 */
	public void setAutoRaiseEvent(boolean autoRaiseEvent) {
		m_autoRaiseEvent = autoRaiseEvent;
	}

	/**
	 * 是否启用代理支持。
	 * 
	 * <p>
	 * 如果工作流运行时绑定了必要的请求信息，那么返回{@link WorkflowForm#getAgentSupport()}的结果，否则返回false。
	 * </p>
	 * 
	 * @return
	 */
	public boolean getAgentSupport() {
		WorkflowForm wff = this.m_workflowRuntime.getWorkflowForm();
		if (wff != null) return wff.getAgentSupport();
		return false;
	}

	/**
	 * 返回是否自动记录事务日志。
	 * 
	 * <p>
	 * 默认返回true，如果为false，则不自动记录日志。
	 * </p>
	 * 
	 * @return
	 */
	public boolean getAutoLog() {
		return m_autoLog;
	}

	/**
	 * 设置是否自动记录事务日志。
	 * 
	 * <p>
	 * 默认为true。
	 * </p>
	 * 
	 * @param autoLog
	 */
	public void setAutoLog(boolean autoLog) {
		m_autoLog = autoLog;
	}

	/**
	 * 获取记录日志时的日志动作名称。
	 * 
	 * <p>
	 * 如果提供的{@link WorkflowForm#getVerb()}包含值，那么优先返回。 <br/>
	 * 否则返回实现类本身提供的结果，如果实现类没有提供有效结果那么默认返回“处理完毕”。<br/>
	 * 需{@link Transaction#getAutoLog()}返回true才有意义。<br/>
	 * </p>
	 * 
	 * @return
	 */
	public String getLogVerb() {
		WorkflowForm wfForm = this.m_workflowRuntime.getWorkflowForm();
		if (wfForm != null) {
			String verb = wfForm.getVerb();
			if (verb != null && verb.length() > 0) return verb;
		}
		if (m_logVerb == null || m_logVerb.length() == 0) { return "执行完毕"; }
		return this.m_logVerb;
	}

	/**
	 * 设置记录日志时的日志动作名称。
	 * 
	 * <p>
	 * 默认为“执行完毕”。
	 * </p>
	 * 
	 * @param logVerb
	 */
	public void setLogVerb(String logVerb) {
		m_logVerb = logVerb;
	}

	/**
	 * 获取记录日志时的日志目标。
	 * 
	 * <p>
	 * 如果未通过{@link #setLogObject(String)}指定有效内容则返回{@link #getFormattedParticipantsForLogObject()}的结果，需{@link Transaction#getAutoLog()}返回true才有意义。<br/>
	 * 具体实现类可以返回一些具体值，比如接收用户等。
	 * </p>
	 * 
	 * @return
	 */
	public String getLogObject() {
		if (m_logObject == null || m_logObject.length() == 0) return this.getFormattedParticipantsForLogObject();
		return m_logObject;
	}

	/**
	 * 设置记录日志时的日志目标。
	 * 
	 * @param logObject
	 */
	public void setLogObject(String logObject) {
		m_logObject = logObject;
	}

	/**
	 * 获取记录日志时的备注信息。
	 * 
	 * <p>
	 * 默认为空字符串。
	 * </p>
	 * 
	 * @return String
	 */
	public String getLogMemo() {
		return m_logMemo == null ? null : m_logMemo.toString();
	}

	/**
	 * 设置记录日志时的备注信息。
	 * 
	 * <p>
	 * 需{@link #getAutoLog()}返回true才有意义。<br/>
	 * </p>
	 * <p>
	 * 多次调用此方法将使用回车换行符分隔并追加日志备注信息。
	 * </p>
	 * 
	 * @param logMemo
	 */
	public void setLogMemo(String logMemo) {
		if (logMemo == null || logMemo.length() == 0) return;
		if (m_logMemo == null || m_logMemo.length() == 0) {
			m_logMemo = new StringBuilder();
			m_logMemo.append(logMemo);
			return;
		}
		if (m_logMemo != null && m_logMemo.indexOf(logMemo) >= 0) return;
		m_logMemo.append("\r\n").append(logMemo);
	}

	/**
	 * 获取此事务执行完毕后执行的下一个事务。
	 * 
	 * <p>
	 * 默认为null（表示没有下一个事务了）。<br/>
	 * 如果返回有效的{@link Transaction}对象则表示继续执行此对象。
	 * </p>
	 * 
	 * @return {@link Transaction}
	 */
	public Transaction getNext() {
		return m_next;
	}

	/**
	 * 设置此事务执行完毕后执行的下一个事务。
	 * 
	 * <p>
	 * 使用此方法或{@link #transact(Session)}执行完毕后返回下一个有效事务对象均可以指派事务链中的下一个事务。<br/>
	 * 设置事务处理链时，需注意事务处理链中的后一个事务处理的控制数据和前一个事务处理的控制数据会变化（因为可能前一个事务处理中变更了控制数据）。<br/>
	 * 在事务处理链处理过程中，需要密切注意日志({@link #setAutoLog(boolean)})、事务持久化提交（通常应该让最后一个事务做持久化提交）({@link #setAutoPersistence(boolean)})、事件处理程序( {@link #setAutoRaiseEvent(boolean)})等的设置。
	 * </p>
	 * 
	 * @param next {@link Transaction}
	 */
	public void setNext(Transaction next) {
		m_next = next;
		m_next.m_prev = this;
	}

	/**
	 * 获取此事务执行执行时的前一个事务。
	 * 
	 * <p>
	 * 只有后续事务才有非null值，事务处理链中的第一个事务总是返回null.
	 * </p>
	 * 
	 * @return {@link Transaction}
	 */
	public Transaction getPrev() {
		return m_prev;
	}

	/**
	 * 设置此事务执行执行时的前一个事务。
	 * 
	 * @param prev
	 */
	public void setPrev(Transaction prev) {
		m_prev = prev;
		m_prev.m_next = this;
	}

	/**
	 * 执行具体流程事务并返回下一个要执行的事务。
	 * 
	 * <p>
	 * 关于事务处理链的注意事项，请参考{@link #setNext(Transaction)}中的说明。
	 * </p>
	 * 
	 * @param session {@link Session}，执行流程事务的用户自定义会话。
	 * 
	 * @return {@link Transaction}，返回下一个要执行的事务，如果返回null，则说明没有需要继续执行的事务了。
	 */
	public abstract Transaction transact(Session session);

	/**
	 * 将与当前用户对应的审批人相关的控制数据设置为完成。
	 * 
	 * <p>
	 * 通常用于用户审批完毕时设置相关控制数据，包括设置代理人或委托人为完成标记、删除等效审批人等。
	 * </p>
	 * 
	 * @return 有处理则返回true，否则返回false。
	 */
	protected boolean doneCurrentApproverContext() {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WFData approver = wfr.getCurrentWFData(WFDataLevel.Approver);
		if (approver == null) return false;
		List<WFData> wfdata = wfr.getContextWFData();
		if (wfdata == null) return false;
		int currentSecurityCode = wfr.getCurrentUserSecurityCode();
		int parentData = approver.getParentData();
		boolean isAgent = wfr.checkCurrentLevel(WFDataLevel.Approver, WFDataSubLevel.Agent);
		if (isAgent) {
			setLogMemo(buildAgentLogMemo(parentData));
		}
		boolean marked = false;
		for (WFData x : wfdata) {
			if (x == null) continue;
			marked = false;
			if (isAgent) { // 如果是代理人审批完毕
				if (currentSecurityCode == x.getData() && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Agent)) {
					wfr.markWFDataDone(x); // 设置代理人完成标记
					marked = true;
				}
				if (parentData == x.getData() && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Normal)) {
					wfr.markWFDataDone(x); // 设置委托人完成标记
					marked = true;
				}
			} else { // 如果是正常审批人
				if (currentSecurityCode == x.getData() && x.checkLevel(WFDataLevel.Approver) && !x.checkSubLevel(WFDataSubLevel.Agent)) {
					wfr.markWFDataDone(x); // 设置审批人完成标记
					marked = true;
				}
				if (currentSecurityCode == x.getParentData() && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Agent)) {
					wfr.removeWFData(x); // 删除代理人数据
					marked = true;
				}
			}
			// 删除其他等同办理人
			if (!marked && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Equivalent)) {
				wfr.removeWFData(x);
			}
		}// for end
		return true;
	}

	/**
	 * 将与当前用户对应的审批人相关的控制数据设置为删除。
	 * 
	 * <p>
	 * 与{@link #doneCurrentApproverContext()}不同，此方法不会在相关控制数据中设置完成标记，而是将所有相关控制数据如：代理人、委托人、等效审批用户等删除。
	 * </p>
	 * 
	 * @return 有处理则返回true，否则返回false。
	 */
	protected boolean removeCurrentApproverContext() {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		WFData approver = wfr.getCurrentWFData(WFDataLevel.Approver);
		if (approver == null) return false;
		List<WFData> wfdata = wfr.getContextWFData();
		if (wfdata == null) return false;
		int currentSecurityCode = wfr.getCurrentUserSecurityCode();
		int parentData = approver.getParentData();
		boolean isAgent = wfr.checkCurrentLevel(WFDataLevel.Approver, WFDataSubLevel.Agent);
		boolean marked = false;
		for (WFData x : wfdata) {
			if (x == null) continue;
			marked = false;
			if (isAgent) { // 如果是代理人
				if (currentSecurityCode == x.getData() && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Agent)) {
					wfr.removeWFData(x);// 删除代理人
					marked = true;
				}
				if (parentData == x.getData() && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Normal)) {
					wfr.removeWFData(x); // 删除委托人
					marked = true;
				}
			} else { // 如果是正常审批人
				if (currentSecurityCode == x.getData() && x.checkLevel(WFDataLevel.Approver) && !x.checkSubLevel(WFDataSubLevel.Agent)) {
					wfr.removeWFData(x); // 删除审批人
					marked = true;
				}
				if (currentSecurityCode == x.getParentData() && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Agent)) {
					wfr.removeWFData(x);// 删除对应代理人数据
					marked = true;
				}
			}
			// 删除其他等同办理人
			if (!marked && x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Equivalent)) {
				wfr.removeWFData(x);
			}
		}// for end
		return true;
	}

	/**
	 * 如果存在等待审批人员则将最近一个等待审批人员（及其可能存在的代理人）设置为审批人员。
	 * 
	 * @return 有处理则返回true，否则返回false。
	 */
	public boolean popStandby() {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		List<WFData> wfdata = wfr.getContextWFData(WFDataLevel.Standby);
		if (wfdata == null || wfdata.isEmpty()) return false;
		WFData standby = null;
		// 把第一个等待人员设置为审批人。
		for (int i = 0; i < wfdata.size(); i++) {
			if (wfdata.get(i) == null) continue;
			if (standby == null) {
				standby = wfdata.get(i);
			} else {
				if (standby.getSort() > wfdata.get(i).getSort()) standby = wfdata.get(i);
			}
		} // for end
		if (standby == null) return false;
		DateTime dtnow = new DateTime();
		DateTime dt = null;
		int seconds = standby.getExpectSeconds();
		if (seconds > 0) {
			dt = new DateTime();
			dt.adjustSecond(seconds);
			standby.setExpect(dt.toString());
		}
		seconds = standby.getNotifySeconds();
		if (seconds > 0) {
			dt = new DateTime();
			dt.adjustSecond(seconds);
			standby.setNotify(dt.toString());
		}
		standby.setCreated(dtnow.toString());
		wfr.markWFDataLevel(standby, WFDataLevel.Approver.getIntValue(), standby.getSubLevel());
		// 代理人处理
		for (WFData x : wfdata) {
			if (x == null) continue;
			if (x.getParentData() == standby.getData() && x.checkSubLevel(WFDataSubLevel.Agent)) {
				x.setCreated(standby.getCreated());
				x.setExpect(standby.getExpect());
				x.setNotify(standby.getNotify());
				wfr.markWFDataLevel(x, WFDataLevel.Approver.getIntValue(), x.getSubLevel());
				break;
			}
		}
		return true;
	}

	/**
	 * 构造并返回代理用户对应的委托人日志备注信息。
	 * 
	 * @param clientSC
	 * @return String
	 */
	protected String buildAgentLogMemo(int clientSC) {
		ParticipantTree pt = ParticipantTreeProvider.getInstance().getParticipantTree(clientSC);
		if (pt == null) return null;
		return "代理\"" + ParticipantHelper.getFormatValue(pt, "ou0\\cn") + "\"执行的操作";
	}

	/**
	 * 构造并返回指定审批方式对应的日志备注信息。
	 * 
	 * @param p {@link Participation}对象
	 * @return String
	 */
	protected String buildParticipationLogMemo(Participation p) {
		return Participation.getName(p, "");
	}

	/**
	 * 获取当前流程事务对应的交互事务对象。
	 * 
	 * <p>
	 * 如果当前流程事务没有配置特定的{@link InteractionTransaction}实现类，则返回defaultReturn的结果。<br/>
	 * 如果流程事务需要用户交互才能继续则不能让此方法返回null，否则可能导致流程无法继续。
	 * </p>
	 * 
	 * @param defaultReturn
	 * @return InteractionTransaction
	 */
	protected InteractionTransaction getInteractionTransaction(InteractionTransaction defaultReturn) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		if (wfr == null) return defaultReturn;
		WorkflowForm wf = wfr.getWorkflowForm();
		if (wf == null) return defaultReturn;
		InteractionTransaction interaction = wf.getInteractionTransaction();
		return (interaction == null ? defaultReturn : interaction);
	}

	/**
	 * 获取流程事务提交是否应在流程事务处理完毕事件结束后执行。
	 * 
	 * <p>
	 * 流程事务提交是指将流程事务处理结果写入持久层(通常是数据库)的过程。<br/>
	 * 默认为false，表示流程事务提交在流程事务处理完毕事件结束前执行。如果为true则表示流程事务提交在流程事务处理完毕事件结束后执行。
	 * </p>
	 * 
	 * @return boolean
	 */
	protected boolean getSubmitAfterTransactionEnd() {
		return m_submitAfterTransactionEnd;
	}

	/**
	 * 设置流程事务提交是否应在流程事务处理完毕事件结束后执行。
	 * 
	 * <p>
	 * 请参考{@link #getSubmitAfterTransactionEnd()}中的说明。
	 * </p>
	 * 
	 * @param submitAfterTransactionEnd
	 */
	public void setSubmitAfterTransactionEnd(boolean submitAfterTransactionEnd) {
		m_submitAfterTransactionEnd = submitAfterTransactionEnd;
	}

	/**
	 * 追加一个随流程事务一起提交的数据访问请求。
	 * 
	 * <p>
	 * 通常此类请求为随流程事务提交时一起提交的数据库记录增(Insert)、删(Delete)、改(Update)请求。<br/>
	 * 通过此方法追加的请求会与流程事务提交使用同一个数据库事务。<br/>
	 * <strong>只有{@link #getSubmitAfterTransactionEnd()}返回true时才有意义，否则会抛出运行时异常且追加的结果会被忽略。</strong>
	 * </p>
	 * 
	 * @param dbr
	 */
	public void appendCoDBRequest(DBRequest dbr) {
		if (!this.getSubmitAfterTransactionEnd()) throw new RuntimeException("流程事务提交在流程事务处理完毕事件结束前执行时无法追加随流程事务一起提交的数据访问请求");
		if (dbr == null) return;
		if (m_coRequests == null) m_coRequests = new ArrayList<DBRequest>();
		m_coRequests.add(dbr);
	}

	/**
	 * 返回所有随流程事务一起提交的数据访问请求对象列表。
	 * 
	 * <p>
	 * 仅供系统内部使用。
	 * </p>
	 * 
	 * @return List&lt;DBRequest&gt;
	 */
	protected List<DBRequest> getCoDBRequests() {
		return m_coRequests;
	}
}

