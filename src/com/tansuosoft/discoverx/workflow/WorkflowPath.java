/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;

/**
 * 表示流程定义中某一条流转路径信息的类。
 * 
 * @author coca@tensosoft.com
 */
final public class WorkflowPath {
	private Activity m_activity = null;
	private Workflow m_workflow = null;
	private ArrayList<WorkflowPath> m_path = new ArrayList<WorkflowPath>();
	private boolean automation = false;

	/**
	 * 返回是否为通过自动流转进入绑定环节的标记。
	 * 
	 * @return boolean
	 */
	public boolean isAutomation() {
		return automation;
	}

	/**
	 * 检查整个路径中是否有通过自动流转进入的环节。
	 * 
	 * @return boolean
	 */
	public boolean hasAutomation() {
		for (WorkflowPath p : this.m_path) {
			if (p.isAutomation()) return true;
		}
		return false;
	}

	/**
	 * 获取完整路径列表。
	 * 
	 * <p>
	 * 注意返回的列表中不包含当前{@link WorkflowPath}本身。<br/>
	 * 通常应用于根路径(开始环节对应的{@link WorkflowPath})对象。
	 * </p>
	 * 
	 * @return WorkflowPath[] 成功则按照先后顺序返回结果路径数组。
	 */
	public WorkflowPath[] getFullPath() {
		WorkflowPath[] r = new WorkflowPath[m_path.size()];
		m_path.toArray(r);
		return r;
	}

	/**
	 * 获取路径绑定的流程环节信息。
	 * 
	 * @return Activity
	 */
	public Activity getActivity() {
		return m_activity;
	}

	/**
	 * 获取路径绑定的流程信息。
	 * 
	 * <p>
	 * 通常只有根节点会绑定流程。
	 * </p>
	 * 
	 * @return Workflow
	 */
	public Workflow getWorkflow() {
		return m_workflow;
	}

	/**
	 * 接收绑定的流程和环节的构造器。
	 * 
	 * @param wf
	 * @param a
	 */
	protected WorkflowPath(Workflow wf, Activity a) {
		m_workflow = wf;
		m_activity = a;
		if (wf != null) m_path.add(this);
	}

	/**
	 * 接收绑定的环节的构造器。
	 * 
	 * @param a缺省构造器。
	 */
	protected WorkflowPath(Activity a) {
		this(null, a);
	}

	/**
	 * 追加一个环节对应的流转路径。
	 * 
	 * @param a
	 * @return WorkflowPath
	 */
	protected WorkflowPath addPath(Activity a) {
		if (a == null) return null;
		WorkflowPath p = new WorkflowPath(a);
		m_path.add(p);
		return p;
	}

	/**
	 * 设置是否为通过自动流转进入绑定环节的标记。
	 * 
	 * @param automation
	 */
	protected void setAutomation(boolean automation) {
		this.automation = automation;
	}

	protected static final String NO_BIND_ACTIVITY = "[未绑定目标环节]";

	/**
	 * 重载：从头到尾顺序输出每个环节名称通过“-&gt;”连接起来的完整路径名。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (WorkflowPath p : m_path) {
			if (sb.length() > 0) sb.append("->");
			Activity a = p.getActivity();
			if (a == null) sb.append(NO_BIND_ACTIVITY);
			else sb.append(a.getName());
		}
		return sb.toString();
	}
}

