/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 流程操作相关的辅助类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowHelper {
	private Workflow m_workflow = null;

	/**
	 * 接收流程定义实例的构造器。
	 * 
	 * @param workflow
	 */
	public WorkflowHelper(Workflow workflow) {
		this.m_workflow = workflow;
	}

	/**
	 * 获取流程开始环节。
	 * 
	 * @return
	 */
	public Activity getBeginActivity() {
		if (this.m_workflow == null) return null;
		List<Activity> activities = this.m_workflow.getActivities();
		if (activities == null || activities.isEmpty()) { return null; }
		for (Activity x : activities) {
			if (x != null && x.checkActivityType(ActivityType.Begin)) return x;
		}
		return null;
	}

	/**
	 * 获取流程结束环节。
	 * 
	 * @return
	 */
	public Activity getEndActivity() {
		if (this.m_workflow == null) return null;
		List<Activity> activities = this.m_workflow.getActivities();
		if (activities == null || activities.isEmpty()) { return null; }
		for (Activity x : activities) {
			if (x != null && x.checkActivityType(ActivityType.End)) return x;
		}
		return null;
	}

	/**
	 * 根据环节的别名或者unid获取流程指定环节。
	 * 
	 * @param clue String,环节的别名或者unid。
	 * @return
	 */
	public Activity getActivity(String clue) {
		if (this.m_workflow == null || clue == null) return null;
		List<Activity> activities = this.m_workflow.getActivities();
		if (activities == null || activities.isEmpty()) { return null; }
		for (Activity x : activities) {
			if (x != null && clue.equalsIgnoreCase(x.getUNID())) {
				return x;
			} else if (x != null && clue.equalsIgnoreCase(x.getAlias())) { return x; }
		}
		return null;
	}

	/**
	 * 追加新流程环节。
	 * 
	 * @return boolean
	 */
	public boolean addActivity(Activity activity) {
		if (this.m_workflow == null) return false;
		List<Activity> activities = this.m_workflow.getActivities();
		if (activities == null) {
			activities = new ArrayList<Activity>();
			this.m_workflow.setActivities(activities);
		}
		return activities.add(activity);
	}

	/**
	 * 在指定位置插入新流程环节。
	 * 
	 * @param idx
	 * @param activity
	 * @return
	 */
	public boolean addActivity(int idx, Activity activity) {
		if (this.m_workflow == null) return false;
		List<Activity> activities = this.m_workflow.getActivities();
		if (activities == null) {
			activities = new ArrayList<Activity>();
			activities.add(activity);
			this.m_workflow.setActivities(activities);
		} else {
			activities.add(idx, activity);
		}
		return true;
	}

	/**
	 * 删除指定环节，如果删除成功则返回true，否则返回false。
	 * 
	 * @param activity
	 * @return
	 */
	public boolean removeActivity(Activity activity) {
		if (this.m_workflow == null) return false;
		List<Activity> activities = this.m_workflow.getActivities();
		if (activities == null) return false;
		return activities.remove(activity);
	}

	/**
	 * 删除指定位置的环节，如果删除成功则返回被删除的环节，否则返回null。
	 * 
	 * @param idx
	 * @return
	 */
	public Activity removeActivity(int idx) {
		if (this.m_workflow == null) return null;
		List<Activity> activities = this.m_workflow.getActivities();
		if (activities == null) return null;
		return activities.remove(idx);
	}

	/**
	 * 获取环节个数。
	 * 
	 * @return
	 */
	public int getActivityCount() {
		if (this.m_workflow == null) return 0;
		List<Activity> activities = this.m_workflow.getActivities();
		return (activities == null ? 0 : activities.size());
	}

	// 以下为枚举流转路径时需要的成员变量
	private Map<String, List<Activity>> mapOut = new HashMap<String, List<Activity>>();
	private Map<String, List<Activity>> mapIn = new HashMap<String, List<Activity>>();
	private List<List<Integer>> pathResult = new ArrayList<List<Integer>>();
	private int pathes[][] = null;
	private List<WorkflowPath> allPath = new ArrayList<WorkflowPath>();

	/**
	 * 枚举并返回当前流程中所有可能的流转路径。
	 * 
	 * @return List&lt;WorkflowPath&gt; 返回所有可能的流转路径的根路径的列表集合。
	 */
	public List<WorkflowPath> enumAllPath() {
		allPath.clear();

		List<Activity> list = m_workflow.getActivities();
		List<Transition> tlist = null;
		List<Automation> alist = null;

		pathes = new int[list.size()][list.size()];
		for (int i = 0; i < pathes.length; i++) {
			for (int j = 0; j < pathes[i].length; j++) {
				pathes[i][j] = 0;
			}
		}
		int idx = 0;
		for (Activity a : list) {
			tlist = a.getTransitions();
			if (tlist != null) {
				for (Transition t : tlist) {
					if (t == null) continue;
					int tidx = getActivityIdx(list, t.getUNID());
					if (tidx >= 0) {
						pathes[idx][tidx] += 1;
						if (!StringUtil.isBlank(t.getCondition())) pathes[idx][tidx] += 2;
					}
				}
			}

			alist = a.getAutomations();
			if (alist != null) {
				for (Automation au : alist) {
					if (au == null) continue;
					String aunid = au.getActivity();
					int auaidx = (aunid != null && aunid.length() == 32 ? getActivityIdx(list, aunid) : list.size() + 1);
					if (auaidx >= 0 && auaidx < list.size()) { // 有效自动流转
						pathes[idx][auaidx] += 4;
						if (!StringUtil.isBlank(au.getCondition())) pathes[idx][auaidx] += 8;
					}
				}
			}

			idx++;
		}

		// 流程走向路径分析并分解为具体几条走向，同时提示孤立节点、死循环、走不下去等情况。
		if (list.size() > 1) calcPath(list);
		buildPathes(list);
		return allPath;
	}

	/**
	 * 分析计算流程路径。
	 * 
	 * @param list 表示原始流程配置的环节列表。
	 */
	protected void calcPath(List<Activity> list) {
		Activity ta = null;
		String aid = null;
		List<Transition> tlist = null;
		List<Automation> alist = null;

		for (Activity a : list) {
			aid = a.getUNID();
			tlist = a.getTransitions();
			if (tlist != null && tlist.size() > 0) {
				for (Transition t : tlist) {
					if (t == null) continue;
					int tidx = getActivityIdx(list, t.getUNID());
					if (tidx >= 0) {
						ta = list.get(tidx);
						if (ta != null) {
							List<Activity> l = mapOut.get(aid);
							if (l == null) {
								l = new ArrayList<Activity>();
								mapOut.put(aid, l);
							}
							l.add(ta);
							//
							l = mapIn.get(ta.getUNID());
							if (l == null) {
								l = new ArrayList<Activity>();
								mapIn.put(ta.getUNID(), l);
							}
							l.add(a);
						}
					}
				}// for end
			}// if end
			alist = a.getAutomations();
			if (alist == null || alist.size() == 0) continue;
			for (Automation au : alist) {
				if (au == null) continue;
				String aunid = au.getActivity();
				int auaidx = (aunid != null && aunid.length() == 32 ? getActivityIdx(list, aunid) : list.size() + 1);
				if (auaidx >= 0 && auaidx < list.size()) {
					ta = list.get(auaidx);
					if (ta != null) {
						List<Activity> l = mapOut.get(aid);
						if (l == null) {
							l = new ArrayList<Activity>();
							mapOut.put(aid, l);
						}
						l.add(ta);
						//
						l = mapIn.get(ta.getUNID());
						if (l == null) {
							l = new ArrayList<Activity>();
							mapIn.put(ta.getUNID(), l);
						}
						l.add(a);
					}
				}
			}// for end
		}// for end
		List<Activity> firsts = new ArrayList<Activity>();
		for (Activity a : list) {
			if (a.getActivityType() == ActivityType.Begin) firsts.add(a);
		}

		for (Activity f : firsts) {
			List<Integer> r = new ArrayList<Integer>();
			r.add(getActivityIdx(list, f.getUNID()));
			getPath(f, list, r);
		}
	}// func end

	/**
	 * 获取路径
	 * 
	 * @param from 开始环节
	 * @param list 表示原始流程配置的环节列表。
	 * @param result 路径结果
	 */
	protected void getPath(Activity from, List<Activity> list, List<Integer> result) {
		if (from == null || list == null) return;
		List<Activity> acts = mapOut.get(from.getUNID());
		if (acts == null || acts.size() == 0) {
			pathResult.add(result);
			return;
		}
		int listIdx = -1;
		Activity x = null;

		for (int i = 0; i < acts.size(); i++) {
			x = acts.get(i);
			listIdx = getActivityIdx(list, x.getUNID());
			if (listIdx < 0) continue;
			List<Integer> l = new ArrayList<Integer>(result);
			boolean recursiveFlag = true;
			for (int j : l) {
				if (listIdx == j) {
					recursiveFlag = false;
					break;
				}
			}
			l.add(listIdx);
			if (recursiveFlag) {
				getPath(x, list, l);
			} else {
				pathResult.add(l);
			}
		}
	}

	/**
	 * 获取流程环节id指定的环节在环节列表中的序号。
	 * 
	 * @param list
	 * @param activityId
	 * @return int
	 */
	protected static int getActivityIdx(List<Activity> list, String activityId) {
		if (list == null || activityId == null) return -1;
		int idx = 0;
		for (Activity a : list) {
			if (a.getUNID().equalsIgnoreCase(activityId)) return idx;
			idx++;
		}
		return -1;
	}

	/**
	 * 输出所有可能的流转路径结果。
	 * 
	 * @param list
	 * @return String
	 */
	protected void buildPathes(List<Activity> list) {
		if (pathResult.size() == 0) throw new RuntimeException("无有效流转路径！");
		Activity a = null;
		WorkflowPath root = null;
		// 路径分三类：正常流转到结束环节的，最后一个节点非结束类型的节点的，循环流转的
		for (List<Integer> l : pathResult) {
			int idx = 0;
			for (int i : l) {
				a = list.get(i);
				if (idx == 0) {
					if (root != null) allPath.add(root);
					root = new WorkflowPath(m_workflow, a);
				} else if (idx > 0) {
					WorkflowPath p = root.addPath(a);
					int pv = pathes[l.get(idx - 1)][i];
					if ((pv & 4) == 4) {
						pathes[l.get(idx - 1)][i] -= 4;
						p.setAutomation(true); // 自动流转
					}
				}
				idx++;
			}
		}// for end
		if (root != null) allPath.add(root);
	}

}

