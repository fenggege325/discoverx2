/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.participants;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.PersistenceState;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.Workflow;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 获取流程运行时环境中指定节点已完成审批的参与者列表集合的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ActivityApprovers extends WorkflowGroupParticipantsProvider {
	/**
	 * 获取环节UNID结果的参数名。
	 */
	public static final String ACT_UNID_PARAM_NAME = "wf_activityUNID";
	/**
	 * 获取环节UNID对应的环节经历次数索引结果的参数名。
	 */
	public static final String ACT_TIDX_PARAM_NAME = "wf_activityTraversalIndex";

	/**
	 * 缺省构造器。
	 */
	public ActivityApprovers() {
		super();
	}

	/**
	 * 重载provide
	 * 
	 * <p>
	 * 通过bindResource（即流程动态群组对应的{@link Group}对象)中{@link #ACT_UNID_PARAM_NAME}和{@link #ACT_TIDX_PARAM_NAME}获取环节UNID结果的参数名。}的参数获取环节UNID和环节流转次数索引（索引默认为0）。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.bll.participant.ParticipantsProvider#provide(com.tansuosoft.discoverx.model.Session, com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	public List<Participant> provide(Session session, Resource bindResource) {
		if (bindResource == null) return null;
		String activityUNID = bindResource.getParamValueString(ACT_UNID_PARAM_NAME, null);
		if (activityUNID == null || activityUNID.length() == 0) return null;
		int traversalIndex = bindResource.getParamValueInt(ACT_TIDX_PARAM_NAME, 0);
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		if (wfr == null) return null;
		Workflow wf = wfr.getCurrentWorkflow();
		if (wf == null) return null;
		List<WFData> wfdata = wfr.getWFData(activityUNID, traversalIndex);
		List<Participant> result = new ArrayList<Participant>();
		for (WFData x : wfdata) {
			if (x == null || wfr.getPersistenceState(x) == PersistenceState.Delete || !x.getDone() || !x.checkLevel(WFDataLevel.Approver)) continue;
			Participant pt = ParticipantTreeProvider.getInstance().getParticipantTree(x.getData());
			result.add(pt);
		}
		return result;
	}
}

