/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.participants;

import com.tansuosoft.discoverx.bll.participant.ParticipantsProvider;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 定义用于获取流程运行时环境中动态参与者列表集合的抽象类。
 * 
 * @author coca@tansuosoft.cn
 */
public abstract class WorkflowGroupParticipantsProvider implements ParticipantsProvider {
	/**
	 * 缺省构造器。
	 */
	public WorkflowGroupParticipantsProvider() {
	}

	private WorkflowRuntime m_workflowRuntime = null;

	/**
	 * 获取有效流程运行时环境。
	 * 
	 * @return
	 */
	public WorkflowRuntime getWorkflowRuntime() {
		return this.m_workflowRuntime;
	}

	/**
	 * 设置有效流程运行时环境。
	 * 
	 * @param workflowRuntime
	 */
	public void setWorkflowRuntime(WorkflowRuntime workflowRuntime) {
		if (workflowRuntime == null) throw new IllegalArgumentException("必须提供有效流程运行时环境！");
		m_workflowRuntime = workflowRuntime;
	}
}

