/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow.participants;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.bll.participant.ParticipantTreeProvider;
import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.model.PersistenceState;
import com.tansuosoft.discoverx.model.Resource;
import com.tansuosoft.discoverx.model.Session;
import com.tansuosoft.discoverx.workflow.Activity;
import com.tansuosoft.discoverx.workflow.WFData;
import com.tansuosoft.discoverx.workflow.WFDataLevel;
import com.tansuosoft.discoverx.workflow.WFDataSubLevel;
import com.tansuosoft.discoverx.workflow.WorkflowRuntime;

/**
 * 获取流程运行时环境中当前环节发送人参与者列表集合的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ContextSender extends WorkflowGroupParticipantsProvider {
	/**
	 * 缺省构造器。
	 */
	public ContextSender() {
		super();
	}

	/**
	 * 重载provide：返回的列表中只包含一个参与者。
	 * 
	 * @see com.tansuosoft.discoverx.bll.participant.ParticipantsProvider#provide(com.tansuosoft.discoverx.model.Session, com.tansuosoft.discoverx.model.Resource)
	 */
	@Override
	public List<Participant> provide(Session session, Resource bindResource) {
		WorkflowRuntime wfr = this.getWorkflowRuntime();
		if (wfr == null) return null;
		Activity activity = wfr.getCurrentActivity();
		if (activity == null) return null;
		List<WFData> wfdata = wfr.getWFData(activity.getUNID(), wfr.getActivityTraversalCount(activity.getUNID()) - 1);
		List<Participant> result = new ArrayList<Participant>();
		for (WFData x : wfdata) {
			if (x == null || wfr.getPersistenceState(x) == PersistenceState.Delete) continue;
			if (x.checkLevel(WFDataLevel.Approver) && x.checkSubLevel(WFDataSubLevel.Normal)) {
				Participant pt = ParticipantTreeProvider.getInstance().getParticipantTree(x.getParentData());
				if (pt != null) {
					result.add(pt);
					break;
				}
			}
		}
		return result;
	}
}

