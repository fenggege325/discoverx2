/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Participant;
import com.tansuosoft.discoverx.util.ObjectUtil;

/**
 * 流程自动流转选项对应的配置信息类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Automation implements Cloneable {

	/**
	 * 缺省构造器。
	 */
	public Automation() {
	}

	private String m_condition = null; // 自动流转条件公式。
	private List<Participant> m_participants = new ArrayList<Participant>(); // 自动流转参与者（处理人员）范围。
	private String m_activity = null; // 自动流转的环节UNID。
	private String m_workflow = null; // 自动流转的流程UNID。
	private Participation m_participation = Participation.Parallel; // 审批方式，默认为多人并行。

	/**
	 * 返回自动流转参与者（处理人员）范围。
	 * 
	 * <p>
	 * 此范围必须符合相应节点的审批方式，比如单人审批的环节，不能设置为多个参与者。另外，如果是部门、群组、角色等参与者，必须能够在运行时计算为有效具体人员。
	 * </p>
	 * <p>
	 * 必须提供有效参与者，否则会导致异常！
	 * </p>
	 * 
	 * @return List<Participant>
	 */
	public List<Participant> getParticipants() {
		return this.m_participants;
	}

	/**
	 * 设置自动流转参与者（处理人员）范围。
	 * 
	 * @param participants List<Participant>
	 */
	public void setParticipants(List<Participant> participants) {
		this.m_participants = participants;
	}

	/**
	 * 返回自动流转的环节UNID。
	 * 
	 * <p>
	 * 如果此属性不提供，则使用当前环节。
	 * </p>
	 * 
	 * @return String
	 */
	public String getActivity() {
		return this.m_activity;
	}

	/**
	 * 设置自动流转的环节UNID。
	 * 
	 * @param activity String
	 */
	public void setActivity(String activity) {
		this.m_activity = activity;
	}

	/**
	 * 返回自动流转的流程UNID。
	 * 
	 * <p>
	 * 如果此属性不提供，则使用当前流程。
	 * </p>
	 * 
	 * @return String
	 */
	public String getWorkflow() {
		return this.m_workflow;
	}

	/**
	 * 设置自动流转的流程UNID。
	 * 
	 * @param workflow String
	 */
	public void setWorkflow(String workflow) {
		this.m_workflow = workflow;
	}

	/**
	 * 返回自动流转条件表达式。
	 * 
	 * <p>
	 * 如果配置了公式，必须公式结果返回true才能自动流转。
	 * </p>
	 * 
	 * @return String
	 */
	public String getCondition() {
		return this.m_condition;
	}

	/**
	 * 设置自动流转条件表达式。
	 * 
	 * <p>
	 * 可选。
	 * </p>
	 * 
	 * @param condition String
	 */
	public void setCondition(String condition) {
		this.m_condition = condition;
	}

	/**
	 * 返回审批方式。
	 * 
	 * @return Participation
	 */
	public Participation getParticipation() {
		return this.m_participation;
	}

	/**
	 * 设置审批方式。
	 * 
	 * <p>
	 * 默认为“{@link Participation#Parallel}”。
	 * </p>
	 * 
	 * @param participation Participation
	 */
	public void setParticipation(Participation participation) {
		this.m_participation = participation;
	}

	/**
	 * 重载clone
	 * 
	 * @see com.tansuosoft.discoverx.model.Resource#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Automation x = new Automation();

		x.setActivity(this.getActivity());
		x.setCondition(this.getCondition());
		x.setWorkflow(this.getWorkflow());
		x.setParticipation(this.getParticipation());
		x.setParticipants(ObjectUtil.cloneList(this.getParticipants()));

		return x;
	}
}

