/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.model.Group;
import com.tansuosoft.discoverx.model.GroupType;

/**
 * 工作流内置群组列表提供类。
 * 
 * <p>
 * 系统默认的工作流内置群组如下：
 * <table width="500" cellspacing="0" cellpadding="0" border="1" style="table-layout:fixed;border-collapse:collapse;">
 * <tr style="background:#ccc none;">
 * <td>群组名称</td>
 * <td>群组别名</td>
 * <td>群组安全编码/排序号</td>
 * </tr>
 * <tr>
 * <td>当前环节已审批人</td>
 * <td>grpContextDoneApprovers</td>
 * <td>10</td>
 * </tr>
 * <tr>
 * <td>当前环节未审批人</td>
 * <td>grpContextApprovers</td>
 * <td>11</td>
 * </tr>
 * <tr>
 * <td>当前环节等待审批人</td>
 * <td>grpContextStandbys</td>
 * <td>12</td>
 * </tr>
 * <tr>
 * <td>当前环节挂起审批人</td>
 * <td>grpContextSuspensions</td>
 * <td>13</td>
 * </tr>
 * <tr>
 * <td>当前环节发送人</td>
 * <td>grpContextSender</td>
 * <td>14</td>
 * </tr>
 * <tr>
 * <td>所有已审批人</td>
 * <td>grpAllDoneApprovers</td>
 * <td>15</td>
 * </tr>
 * <tr>
 * <td>特定环节审批人</td>
 * <td>grpActivityApprovers</td>
 * <td>16</td>
 * </tr>
 * <tr>
 * <td>所有待阅人</td>
 * <td>grpReaders</td>
 * <td>17</td>
 * </tr>
 * <tr>
 * <td>所有已阅人</td>
 * <td>grpDoneReaders</td>
 * <td>18</td>
 * </tr>
 * <tr>
 * <td>文档作者</td>
 * <td>grpAuthor</td>
 * <td>19</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowGroupsProvider {
	private static final String[] GroupNames = { "当前环节已审批人", "当前环节未审批人", "当前环节等待审批人", "当前环节挂起审批人", "当前环节发送人", "所有已审批人", "特定环节已审批人", "所有待阅人", "所有已阅人", "文档发起人" };
	private static final String[] GroupAlias = { "grpContextDoneApprovers", "grpContextApprovers", "grpContextStandbys", "grpContextSuspensions", "grpContextSender", "grpAllDoneApprovers", "grpActivityApprovers", "grpReaders", "grpDoneReaders", "grpAuthor" };
	private static final String[] GroupUNIDs = { "4A4749CADA144F4AACAE406B560BD77F", "895BB37E885F4792BFB5D0CE4A96F351", "86273CA86E744E9AB74D0D60E54DFDCE", "D80150B87154458194EDC441DFC33959", "4A960CDEED7B4496B7304E5521BA0B5E", "AB4EEE2D5F574D01B5065AB5242461AB", "5310F2A231844CE0A47218A60BD0DB19", "60E3610E4EDE4DDDB38E484801D20E48", "53072DD2E3A847A5838E893081C8FDC3", "B358DF3DC1944A6EB041C411511841DC" };
	private static final int[] GroupSecurityCodes = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
	private static final int[] GroupSorts = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };

	/**
	 * 流程群组中存取绑定的{@link WorkflowRuntime}对象的参数名。
	 */
	public static final String WORKFLOW_RUNTIME_PARAM_NAME = "C0CABE0601DB49858CAC25CF80281EF5";

	/**
	 * 获取工作流内置群组列表集合。
	 * 
	 * @return List&lt;Group&gt;
	 */
	public static List<Group> getGroups() {
		List<Group> groups = null;
		int index = 0;
		Group group = null;
		for (String n : GroupNames) {
			group = new Group();
			group.setName(n);
			group.setDescription(n);
			group.setAlias(GroupAlias[index]);
			group.setUNID(GroupUNIDs[index]);
			group.setSecurityCode(GroupSecurityCodes[index]);
			group.setSort(GroupSorts[index]);
			group.setGroupType(GroupType.Workflow);
			if (groups == null) groups = new ArrayList<Group>(GroupNames.length);
			groups.add(group);
			index++;
		}
		return groups;
	}

	/**
	 * 获取安全编码指定的工作流内置群组对象。
	 * 
	 * @param securityCode
	 * @return 如果找不到与任何内部工作流内置群组安全编码匹配的群组，则返回null。
	 */
	public static Group getGroup(int securityCode) {
		int index = 0;
		boolean found = false;
		for (int n : GroupSecurityCodes) {
			if (n == securityCode) {
				found = true;
				break;
			}
			index++;
		}
		if (found) {
			Group group = new Group();
			group.setName(GroupNames[index]);
			group.setAlias(GroupAlias[index]);
			group.setUNID(GroupUNIDs[index]);
			group.setSecurityCode(securityCode);
			group.setSort(GroupSorts[index]);
			group.setGroupType(GroupType.Workflow);
			return group;
		}
		return null;
	}

	/**
	 * 获取别名或unid指定的工作流内置群组对象。
	 * 
	 * @param clue
	 * @return 如果找不到与任何内部工作流内置群组别名或unid匹配的群组，则返回null。
	 */
	public static Group getGroup(String clue) {
		int index = 0;
		boolean found = false;
		for (String n : GroupAlias) {
			if (clue.equalsIgnoreCase(n)) {
				found = true;
				break;
			}
			index++;
		}
		if (!found) {
			index = 0;
			for (String n : GroupUNIDs) {
				if (clue.equalsIgnoreCase(n)) {
					found = true;
					break;
				}
				index++;
			}
		}
		if (found) {
			Group group = new Group();
			group.setName(GroupNames[index]);
			group.setAlias(GroupAlias[index]);
			group.setUNID(GroupUNIDs[index]);
			group.setSecurityCode(GroupSecurityCodes[index]);
			group.setSort(GroupSorts[index]);
			group.setGroupType(GroupType.Workflow);
			return group;
		}
		return null;
	}
}

