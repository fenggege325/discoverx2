/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.tansuosoft.discoverx.bll.function.CommonForm;
import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.Instance;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.workflow.transaction.InteractionTransaction;

/**
 * 表示从流程处理相关用户交互接口（一般是http请求）中提交的流程事务处理请求信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public class WorkflowForm extends CommonForm {

	/**
	 * 缺省构造器。
	 */
	public WorkflowForm() {
		super();
	}

	private String m_workflow = null; // 目标流程UNID。
	private String m_activity = null; // 目标环节UNID。
	private String m_deadline = null; // 处理时限。
	private String m_notify = null; // 催办日期时间。
	private Participation m_participation = null; // 审批方式。
	private List<Integer> wf_participants = null; // 办理人参与者对应安全编码。
	private Transaction m_transaction = null; // 触发的流程事务对象。
	private String m_verb = null; // 日志动作名称。
	private boolean m_agentSupport = true; // 启用办理人代理。
	private int m_option = 0; // 流转选项。
	private int m_notification = 0; // 通知方式。
	private InteractionTransaction m_interaction = null; // 自定义的流程事务交互对象。

	/**
	 * 返回目标流程UNID。
	 * 
	 * <p>
	 * 从名为“wf_workflow”的http请求参数中获取并自动填充。
	 * </p>
	 * 
	 * @return String
	 */
	public String getWorkflow() {
		return this.m_workflow;
	}

	/**
	 * 设置目标流程UNID。
	 * 
	 * @param workflow String
	 */
	public void setWorkflow(String workflow) {
		this.m_workflow = workflow;
	}

	/**
	 * 返回目标环节UNID。
	 * 
	 * <p>
	 * 从名为“wf_activity”的http请求参数中获取并自动填充。
	 * </p>
	 * 
	 * @return String
	 */
	public String getActivity() {
		return this.m_activity;
	}

	/**
	 * 设置目标环节UNID。
	 * 
	 * @param activity String
	 */
	public void setActivity(String activity) {
		this.m_activity = activity;
	}

	/**
	 * 返回处理时限。
	 * 
	 * <p>
	 * 从名为“wf_deadline”的http请求参数中获取并自动填充，格式为：“yyyy-mm-dd hh:nn:ss”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getDeadline() {
		return this.m_deadline;
	}

	/**
	 * 设置处理时限。
	 * 
	 * @param deadline String
	 */
	public void setDeadline(String deadline) {
		this.m_deadline = deadline;
	}

	/**
	 * 返回催办日期时间。
	 * 
	 * <p>
	 * 从名为“wf_notify”的http请求参数中获取并自动填充，格式为：“yyyy-mm-dd hh:nn:ss”。
	 * </p>
	 * 
	 * @return String
	 */
	public String getNotify() {
		return this.m_notify;
	}

	/**
	 * 设置催办日期时间。
	 * 
	 * @param notify String
	 */
	public void setNotify(String notify) {
		this.m_notify = notify;
	}

	/**
	 * 返回审批方式。
	 * 
	 * <p>
	 * 从名为“wf_participation”的http请求参数中获取并自动填充。<br/>
	 * 不提供则从配置的流程操作中获取预先配置的参数。
	 * </p>
	 * 
	 * @return {@link Participation}
	 */
	public Participation getParticipation() {
		return this.m_participation;
	}

	/**
	 * 设置审批方式。
	 * 
	 * @param participation {@link Participation}
	 */
	public void setParticipation(Participation participation) {
		this.m_participation = participation;
	}

	/**
	 * 返回当前流程事务处理所绑定的目标参与者对应安全编码列表集合。
	 * 
	 * <p>
	 * 从名为“wf_participants”的http请求参数中获取并自动填充；多个编码用半角逗号分隔、半角分号、空格等分隔，也可以直接在http请求中提交多个值。
	 * </p>
	 * 
	 * @return List&lt;Integer&gt;
	 */
	public List<Integer> getParticipants() {
		return this.wf_participants;
	}

	/**
	 * 设置当前流程事务处理所绑定的目标参与者对应安全编码列表集合。
	 * 
	 * @param approvers List&lt;Integer&gt;
	 */
	public void setParticipants(List<Integer> participants) {
		this.wf_participants = participants;
	}

	/**
	 * 返回触发的流程事务对象。
	 * 
	 * <p>
	 * 从名为“wf_transaction”的http请求参数中获取并自动填充。<br/>
	 * 不提供则从配置的流程操作中获取预先配置的参数。
	 * </p>
	 * 
	 * @return Transaction
	 */
	public Transaction getTransaction() {
		return this.m_transaction;
	}

	/**
	 * 设置触发的流程事务对象。
	 * 
	 * @param transaction Transaction
	 */
	public void setTransaction(Transaction transaction) {
		this.m_transaction = transaction;
	}

	/**
	 * 返回自定义的流程事务交互对象。
	 * 
	 * <p>
	 * 从名为“wf_interaction”的http请求参数中获取并自动填充。<br/>
	 * （默认情况下）不提供则使用系统内部的{@link InteractionTransaction}对象。
	 * </p>
	 * 
	 * @return InteractionTransaction
	 */
	public InteractionTransaction getInteractionTransaction() {
		return this.m_interaction;
	}

	/**
	 * 设置自定义的流程事务交互对象。
	 * 
	 * @param interaction InteractionTransaction
	 */
	public void setInteractionTransaction(InteractionTransaction interaction) {
		this.m_interaction = interaction;
	}

	/**
	 * 返回日志动作名称。
	 * 
	 * <p>
	 * 从名为“wf_verb”的http请求参数中获取并自动填充。<br/>
	 * 不提供则从配置的流程操作中获取预先配置的参数，如果都没有提供，则使用流程事务实现类中默认的动作名称。
	 * </p>
	 * 
	 * @return String
	 */
	public String getVerb() {
		return this.m_verb;
	}

	/**
	 * 设置日志动作名称。
	 * 
	 * @param verb String
	 */
	public void setVerb(String verb) {
		this.m_verb = verb;
	}

	/**
	 * 返回是否启用办理人代理。
	 * 
	 * <p>
	 * 默认为true。<br/>
	 * 从名为“wf_agentsupport”的http请求参数中获取并自动填充。<br/>
	 * 不提供则从配置的流程操作中获取预先配置的参数。
	 * </p>
	 * 
	 * @return boolean
	 */
	public boolean getAgentSupport() {
		return this.m_agentSupport;
	}

	/**
	 * 设置是否启用办理人代理。
	 * 
	 * @param agentSupport boolean
	 */
	public void setAgentSupport(boolean agentSupport) {
		this.m_agentSupport = agentSupport;
	}

	/**
	 * 返回流转选项。
	 * 
	 * <p>
	 * 从名为“wf_option”的http请求参数中获取并自动填充。<br/>
	 * 如果不提供或者为0（默认），则表示正常流转；1表示可以在各环节中自由流转；2表示作为正式流程的母版进行学习性流转。<br/>
	 * <strong>只有在流程处于第一个流转环节时才能通过流程操作向导设置值。</strong>
	 * </p>
	 * 
	 * @return int
	 */
	public int getOption() {
		return this.m_option;
	}

	/**
	 * 设置流转选项。
	 * 
	 * <p>
	 * <strong>只有在流程处于第一个流转环节时才能通过流程操作向导设置值。</strong>
	 * </p>
	 * 
	 * @param option int
	 */
	public void setOption(int option) {
		this.m_option = option;
	}

	/**
	 * 返回通知方式。
	 * 
	 * <p>
	 * 从名为“wf_notification”的http请求参数中获取并自动填充。<br/>
	 * 如果不提供或者为0（默认），则表示使用默认的通知方式（即通过即时消息方式通知，如果即时消息通知方式可用的话）；1表示额外通过内部邮件通知；4表示额外通过手机短信通知（如果可用的话）；值可以组合，如5表示即通过邮件又通过短信进行通知。<br/>
	 * </p>
	 * 
	 * @return int
	 */
	public int getNotification() {
		return this.m_notification;
	}

	/**
	 * 设置通知方式。
	 * 
	 * @param notification int
	 */
	public void setNotification(int notification) {
		this.m_notification = notification;
	}

	/**
	 * 返回请求中是否包含有效的环节信息和参与者信息。
	 * 
	 * @return
	 */
	public boolean hasValidActivityAndParticipant() {
		return (this.getActivity() != null && this.getActivity().length() > 0 && this.getParticipants() != null && this.getParticipants().size() > 0);
	}

	/**
	 * 重载fillWebRequestForm
	 * 
	 * @see com.tansuosoft.discoverx.bll.function.CommonForm#fillWebRequestForm(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public CommonForm fillWebRequestForm(HttpServletRequest request) {
		if (request == null) return null;
		this.setWorkflow(request.getParameter("wf_workflow"));
		this.setActivity(request.getParameter("wf_activity"));
		this.setParticipation(StringUtil.getValueEnum(request.getParameter("wf_participation"), Participation.class, Participation.Single));
		int minutes = StringUtil.getValueInt(request.getParameter("wf_deadline"), -1);

		// 办理时限
		DateTime dt = new DateTime();
		this.setDeadline(null);
		if (minutes > 0) {
			dt.adjustMinute(minutes);
			this.setDeadline(dt.toString());
		}

		// 催办时限
		dt = new DateTime();
		minutes = StringUtil.getValueInt(request.getParameter("wf_notify"), -1);
		this.setNotify(null);
		if (minutes > 0) {
			dt.adjustMinute(minutes);
			this.setNotify(dt.toString());
		}

		String transactionImpl = request.getParameter("wf_transaction");
		if (transactionImpl != null && transactionImpl.length() > 0) {
			Transaction transaction = Instance.newInstance(transactionImpl, Transaction.class);
			if (transaction != null) this.setTransaction(transaction);
		}

		String interactionImpl = request.getParameter("wf_interaction");
		if (interactionImpl != null && interactionImpl.length() > 0) {
			InteractionTransaction interaction = Instance.newInstance(interactionImpl, InteractionTransaction.class);
			if (interaction != null) this.setInteractionTransaction(interaction);
		}
		String agentSupport = request.getParameter("wf_agentsupport");
		if (agentSupport != null && agentSupport.length() > 0) {
			this.setAgentSupport(StringUtil.getValueBool(agentSupport, true));
		}
		this.setVerb(request.getParameter("wf_verb"));
		this.setParticipants(getSecurityCodes(request, "wf_participants"));
		this.setOption(StringUtil.getValueInt(request.getParameter("wf_option"), 0));
		this.setNotification(StringUtil.getValueInt(request.getParameter("wf_notification"), 0));
		return this;
	}

	/**
	 * 从{@link javax.servlet.http.HttpServletRequest}中获取并解析指定参数名对应的安全编码列表并返回。
	 * 
	 * @param request
	 * @param paramName
	 * @return
	 */
	public static List<Integer> getSecurityCodes(HttpServletRequest request, String paramName) {
		List<Integer> result = null;
		String[] scs = request.getParameterValues(paramName);
		if (scs != null && scs.length > 0) {
			int sc = 0;
			if (scs.length == 1) {
				String vs[] = scs[0].split("[,;\\s]");
				result = new ArrayList<Integer>(vs.length);
				for (String x : vs) {
					if (x.length() == 0) continue;
					sc = StringUtil.getValueInt(x, 0);
					if (sc == 0) continue;
					result.add(sc);
				}
			} else {
				result = new ArrayList<Integer>(scs.length);
				for (String x : scs) {
					if (x == null || x.length() == 0) continue;
					sc = StringUtil.getValueInt(x, 0);
					if (sc == 0) continue;
					result.add(sc);
				}
			}
		}
		return result;
	}

}

