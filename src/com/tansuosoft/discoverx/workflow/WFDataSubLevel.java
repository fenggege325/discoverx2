/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.workflow;

import com.tansuosoft.discoverx.util.EnumBase;

/**
 * 流程控制数据级别（权限）辅助类型枚举值。
 * 
 * <p>
 * 枚举中的值通常用于描述流程控制级别主类型({@link WFDataLevel})的额外意义和流程控制数据的上级数据({@link WFData#getParentData()})的意义。<br/>
 * 比如某一条控制数据的主类型为“{@link WFDataLevel#Approver}”(当前办理人)且辅类型为“{@link WFDataSubLevel#Agent}”，那么表示这个办理人是一个实际办理人（委托人）的代理，<br/>
 * 这时，可以通过{@link WFData#getParentData()}获取其委托人（实际办理人）。<br/>
 * 不同的组合{@link WFDataLevel}与{@link WFDataSubLevel}组合有不同意义，有些组合可能无效或者不合法，常见组合请参考{@link WFData#getParentData()}中的注释。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public enum WFDataSubLevel implements EnumBase {
	/**
	 * 正常类型（0）。
	 */
	Normal(0),
	/**
	 * 说明主级别的人员是委托人设置的代理人（1）。
	 */
	Agent(1),
	/**
	 * 说明主级别的人员是转办人设置的转办目标人（2）。
	 */
	Forward(2),
	/**
	 * 说明主级别的人员是交办人设置的交办目标人（4）。
	 */
	Dispatch(4),
	/**
	 * 说明主级别的人员是优先审批方式中的等效办理人（8）。
	 */
	Equivalent(8),
	/**
	 * 说明主级别的人员是系统内部的转接人（16）。
	 */
	Commutator(16),
	/**
	 * 说明由主级别的人员执行了反向流转（通常是退回）标记（32）。
	 */
	Reverse(32),
	/**
	 * 应用级别标记1（1024）。
	 * 
	 * <p>
	 * 表示由具体应用程序自定义其含义的第一个自定义标记。
	 * </p>
	 */
	Application1(1024),
	/**
	 * 应用级别标记2（2048）。
	 * 
	 * <p>
	 * 表示由具体应用程序自定义其含义的第二个自定义标记。
	 * </p>
	 */
	Application2(2048),
	/**
	 * 应用级别标记3（2048）。
	 * 
	 * <p>
	 * 表示由具体应用程序自定义其含义的第三个自定义标记。
	 * </p>
	 */
	Application3(4096),
	/**
	 * 应用级别标记4（8192）。
	 * 
	 * <p>
	 * 表示由具体应用程序自定义其含义的第四个自定义标记。
	 * </p>
	 */
	Application4(8192);

	private final int m_intValue; // 对应的int值。

	/**
	 * 接收对应int值的构造器
	 * 
	 * @param intValue
	 */
	WFDataSubLevel(int intValue) {
		this.m_intValue = intValue;
	}

	/**
	 * 重载默认的获取枚举字符串表现形式：返回其表示的int值的字符串。
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return String.format("%s", this.m_intValue);
	}

	/**
	 * 返回对应的int值。
	 * 
	 * @return int 对应的int值。
	 */
	public int getIntValue() {
		return this.m_intValue;
	}

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return WFDataSubLevel
	 */
	public WFDataSubLevel parse(String v) {
		if (v == null || v.length() == 0) return null;
		if (com.tansuosoft.discoverx.util.StringUtil.isNumber(v)) {
			for (WFDataSubLevel s : WFDataSubLevel.values())
				if (s.getIntValue() == Integer.parseInt(v)) return s;
		} else {
			return WFDataSubLevel.valueOf(v);
		}
		return null;
	}

	/**
	 * 根据枚举值对应的数字值返回枚举对象（对于数字格式，必须有getIntValue这个方法）。
	 * 
	 * @param v
	 * @return WFDataSubLevel
	 */
	public static WFDataSubLevel parse(int v) {
		for (WFDataSubLevel s : WFDataSubLevel.values())
			if (s.getIntValue() == v) return s;
		return null;
	}

}

