/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.util.UUID;

/**
 * 提供32位UNID的类。
 * 
 * @author coca@tansuosoft.cn
 * @version 1.0
 */
public class UNIDProvider {
	/**
	 * 缺省私有构造器。
	 */
	private UNIDProvider() {
	}

	/**
	 * 获取32位UNID字符串。
	 * 
	 * @return String
	 */
	public static String getUNID() {
		return UUID.randomUUID().toString().replace("-", "").toUpperCase();
	}

}

