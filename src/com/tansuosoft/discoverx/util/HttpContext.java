/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.lang.ref.Reference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 用于获取当前执行环境中对应的http请求、http会话等信息的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public final class HttpContext {

	/**
	 * 用于保存当前请求线程中{@link javax.servlet.http.HttpServletRequest}和{@link javax.servlet.http.HttpServletResponse}线程实例的ThreadLocal实例。
	 */
	private static ThreadLocal<HttpContext> m_instance = null;

	private HttpServletRequest m_request = null; // 当前http请求绑定的HttpServletRequest对象实例。
	private HttpServletResponse m_response = null; // 当前http请求绑定的HttpServletResponse对象实例。

	/**
	 * 返回当前请求线程绑定的HttpServletRequest对象实例。
	 * 
	 * @return HttpServletRequest
	 */
	public static HttpServletRequest getRequest() {
		HttpContext httpContext = (m_instance == null ? null : m_instance.get());
		if (httpContext == null) return null;
		return httpContext.m_request;
	}

	/**
	 * 返回当前请求线程绑定的HttpServletResponse对象实例。
	 * 
	 * @return HttpServletResponse
	 */
	public static HttpServletResponse getResponse() {
		HttpContext httpContext = (m_instance == null ? null : m_instance.get());
		if (httpContext == null) return null;
		return httpContext.m_response;
	}

	/**
	 * 返回当前请求线程绑定的http会话。
	 * 
	 * @return HttpSession
	 */
	public static HttpSession getHttpSession() {
		HttpServletRequest request = getRequest();
		if (request == null) return null;
		return request.getSession();
	}

	/**
	 * 保存当前请求线程中的HttpServletRequest和HttpServletResponse以供后续使用。
	 * 
	 * <p>
	 * <strong>仅供系统内部使用，请勿直接调用。</strong>
	 * </p>
	 * 
	 * @param request
	 * @param response
	 */
	public static void store(HttpServletRequest request, HttpServletResponse response) {
		if (request == null || response == null) {
			if (m_instance != null) reset();
			return;
		}
		if (m_instance == null) m_instance = new ThreadLocal<HttpContext>() {
			// @Override
			// public String toString() {
			// return StringUtil.stringRightBack(this.getClass().getName(), ".") + "@" + this.hashCode() + ",TID:" + Thread.currentThread().getId();
			// }
		};
		HttpContext httpContext = new HttpContext();
		httpContext.m_request = request;
		httpContext.m_response = response;
		m_instance.set(httpContext);
	}

	/**
	 * 重置（清理）已经存在于线程本地变量中的信息。
	 * 
	 * <p>
	 * <strong>仅供系统内部使用，请勿直接调用。</strong>
	 * </p>
	 */
	public static void reset() {
		HttpContext context = (m_instance == null ? null : m_instance.get());
		if (context != null) {
			context.m_request = null;
			context.m_response = null;
			context = null;
			m_instance.remove();
		}
	}

	/**
	 * 清理所有线程对应的线程本地变量保存的信息。
	 */
	public static void shutdown() {
		Thread[] ts = getThreads();
		if (ts != null) {
			for (Thread t : ts) {
				clearReferencesThreadLocals(m_instance, t);
			}
		}
		m_instance = null;
	}

	/**
	 * 获取当前所有线程并返回包含这些线程的数组。
	 * 
	 * @return Thread[]
	 */
	public static Thread[] getThreads() {
		ThreadGroup tg = Thread.currentThread().getThreadGroup();
		while (tg.getParent() != null) {
			tg = tg.getParent();
		}

		int threadCountGuess = tg.activeCount() + 50;
		Thread[] threads = new Thread[threadCountGuess];
		int threadCountActual = tg.enumerate(threads);

		while (threadCountActual == threadCountGuess) {
			threadCountGuess *= 2;
			threads = new Thread[threadCountGuess];
			threadCountActual = tg.enumerate(threads);
		}

		return threads;
	}

	/**
	 * 清理指定线程包含的tl对应的线程本地变量的值以防止内存泄漏。
	 * 
	 * @param tl
	 * @param t
	 */
	public static void clearReferencesThreadLocals(ThreadLocal<?> tl, Thread t) {
		if (tl == null || t == null) return;
		try {
			Field threadLocalsField = Thread.class.getDeclaredField("threadLocals");
			threadLocalsField.setAccessible(true);
			Field inheritableThreadLocalsField = Thread.class.getDeclaredField("inheritableThreadLocals");
			inheritableThreadLocalsField.setAccessible(true);
			Class<?> tlmClass = Class.forName("java.lang.ThreadLocal$ThreadLocalMap");
			Field tableField = tlmClass.getDeclaredField("table");
			tableField.setAccessible(true);

			Object threadLocalMap = null;

			// 清理threadLocalsField中的结果
			threadLocalMap = threadLocalsField.get(t);
			clearThreadLocalMap(tl, threadLocalMap, tableField);
			// 清理inheritableThreadLocalsField中的结果
			threadLocalMap = inheritableThreadLocalsField.get(t);
			clearThreadLocalMap(tl, threadLocalMap, tableField);
		} catch (Exception e) {
			FileLogger.debug(e.getMessage());
		}
	}

	/**
	 * 在指定线程对应的线程本地变量包含的所有值中清理tl对应的线程本地变量的值以防止内存泄漏。
	 * 
	 * @param tl
	 * @param map
	 * @param internalTableField
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws NoSuchFieldException
	 * @throws InvocationTargetException
	 */
	protected static void clearThreadLocalMap(ThreadLocal<?> tl, Object map, Field internalTableField) throws NoSuchMethodException, IllegalAccessException, NoSuchFieldException, InvocationTargetException {
		if (tl == null || map == null) return;
		Method mapRemove = map.getClass().getDeclaredMethod("remove", ThreadLocal.class);
		mapRemove.setAccessible(true);
		Object[] table = (Object[]) internalTableField.get(map);
		int staleEntriesCount = 0;
		if (table != null) {
			for (int j = 0; j < table.length; j++) {
				if (table[j] == null) continue;

				Object key = ((Reference<?>) table[j]).get();
				boolean remove = (key != null && (tl.equals(key) || tl.getClass().getName().equals(key.getClass().getName())));

				if (remove) {
					if (key == null) {
						staleEntriesCount++;
					} else {
						mapRemove.invoke(map, key);
					}
				}
			}
		}
		if (staleEntriesCount > 0) {
			Method mapRemoveStale = map.getClass().getDeclaredMethod("expungeStaleEntries");
			mapRemoveStale.setAccessible(true);
			mapRemoveStale.invoke(map);
		}
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HttpContext@" + this.hashCode() + ",TID:" + Thread.currentThread().getId() + " [request=" + m_request.hashCode() + ", response=" + m_response.hashCode() + "]";
	}
}

