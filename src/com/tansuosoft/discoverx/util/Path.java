/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件路径处理类。
 * 
 * @author coca@tansuosoft.cn
 */
public class Path {
	/**
	 * Web应用程序安装的根路径（在服务器上的物理路径）。
	 * 
	 * <p>
	 * 系统会尝试自动计算此路径，如果计算出来的路径非法则结果为null（此时需要通过配置获取）。
	 * </p>
	 */
	public static String WEBAPP_ROOT = "";
	static {
		WEBAPP_ROOT = Path.class.getResource("/").toString();
		if (WEBAPP_ROOT.startsWith("file:/")) WEBAPP_ROOT = WEBAPP_ROOT.substring(5);
		int pos = WEBAPP_ROOT.lastIndexOf("WEB-INF");
		if (pos > 0) {
			WEBAPP_ROOT = WEBAPP_ROOT.substring(JVM.isHostWindows() ? 1 : 0, pos);
			WEBAPP_ROOT = WEBAPP_ROOT.replace('/', java.io.File.separatorChar);
		} else {
			WEBAPP_ROOT = null;
		}
		if (WEBAPP_ROOT != null && WEBAPP_ROOT.length() > 0) {
			java.io.File f = new java.io.File(WEBAPP_ROOT);
			if (!f.exists() || !f.isDirectory()) WEBAPP_ROOT = null;
		}
	}

	private String[] chunks = null;
	private transient String pathAsString = null;

	/**
	 * 接收一个路径作为构造器。
	 * 
	 * @param pathAsString
	 */
	public Path(String pathAsString) {
		List<String> result = new ArrayList<String>();
		int currentIndex = 0;
		int nextSeparator;
		String tmp = pathAsString.replace('\\', '/');
		while ((nextSeparator = tmp.indexOf('/', currentIndex)) != -1) {
			result.add(tmp.substring(currentIndex, nextSeparator));
			currentIndex = nextSeparator + 1;
		}
		result.add(pathAsString.substring(currentIndex));
		String[] arr = new String[result.size()];
		result.toArray(arr);
		chunks = arr;
		this.pathAsString = tmp.replace('/', java.io.File.separatorChar);
		if (!this.pathAsString.endsWith(java.io.File.separator)) this.pathAsString = this.pathAsString.concat(java.io.File.separator);
	}

	/**
	 * 默认构造器：构造一个包含WEBAPP_ROOT的路径类。
	 */
	public Path() {
		this(Path.WEBAPP_ROOT);
	}

	/**
	 * 合并路径并返回合并后的新路径。
	 * 
	 * @return Path
	 */
	public Path combine(String subPathAsString) {
		if (subPathAsString == null || subPathAsString.length() == 0) return new Path(this.toString());
		String tmp = subPathAsString;
		if (tmp.startsWith("/") || tmp.startsWith("\\")) tmp = tmp.substring(1);
		return new Path(this.toString() + tmp);
	}

	/**
	 * 与subPathAsString指定的包含最终文件的子路径合并并返回合并的结果字符串。
	 * 
	 * @param subPathAsString
	 * @return String
	 */
	public String combineFile(String subPathAsString) {
		if (subPathAsString == null || subPathAsString.length() == 0) return null;
		String tmp = subPathAsString;
		if (tmp.startsWith("/") || tmp.startsWith("\\")) tmp = tmp.substring(1);
		return this.toString() + tmp;
	}

	/**
	 * 返回路径是否存在。
	 * 
	 * @return boolean
	 */
	public boolean checkPathExists() {
		java.io.File file = this.getFileInstance();
		if (file != null) return (file.isDirectory() && file.exists());
		return false;
	}

	/**
	 * 判断是否存在路径，如果不存在，则创建之。
	 */
	public void makeDirIfNotExists() {
		java.io.File file = this.getFileInstance();
		if (file != null && !file.exists()) file.mkdirs();
	}

	/**
	 * 返回一个对应的<code>java.io.File</code>对象实例。
	 * 
	 * @return java.io.File
	 */
	public java.io.File getFileInstance() {
		return new java.io.File(this.toString());
	}

	/**
	 * 返回路径的深度，比如C:\temp\logs\返回3；
	 * 
	 * @return int
	 */
	public int getDepth() {
		return this.chunks.length;
	}

	/**
	 * 返回完整路径字符串。
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		if (pathAsString == null) {
			StringBuilder buffer = new StringBuilder();
			for (int i = 0; i < chunks.length; i++) {
				if (i > 0) buffer.append(java.io.File.separatorChar);
				buffer.append(chunks[i]);
			}
			buffer.append(java.io.File.separatorChar);
			pathAsString = buffer.toString();
		}
		return pathAsString;
	}

	/**
	 * 规范化目录，并返回之。
	 * 
	 * <p>
	 * 包括替换路径分隔符，追加尾部分隔符、检查是否存在等。
	 * </p>
	 * 
	 * @param raw String，原始目录信息，必须。
	 * @return String,返回规范化后且存在的目录。
	 */
	public static String normalizeDirectory(String raw) {
		if (raw == null || raw.length() == 0) return null;
		String result = null;
		result = raw.replace('\\', '/');
		result = result.replace("/", File.separator);
		if (!result.endsWith(File.separator)) {
			result = StringUtil.concatString(result, File.separator);
		}
		File f = new File(result);
		if (!f.exists() || !f.isDirectory()) return null;
		return result;
	}

	/**
	 * 检查指定路径是否存在。
	 * 
	 * @param path
	 * @return
	 */
	public static boolean checkPathExists(String path) {
		if (path == null || path.length() == 0) return false;
		File file = new File(path);
		return (file.isDirectory() && file.exists());
	}

}// class end

