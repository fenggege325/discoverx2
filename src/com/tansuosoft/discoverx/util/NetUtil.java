/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 包含网络常用实用方法的类。
 * 
 * @author coca@tensosoft.com
 */
public class NetUtil {
	/**
	 * 缺省构造器。
	 */
	private NetUtil() {
	}

	/**
	 * 获取子网掩码地址。
	 * 
	 * @param netPrefix 用于计算子网掩码地址的网络位长度（有效值在0-32位之间）。
	 * @return InetAddress 返回获取的子网掩码地址。
	 */
	public static InetAddress getIPv4SubnetMask(int netPrefix) {
		try {
			int shiftby = (1 << 31);
			for (int i = netPrefix - 1; i > 0; i--) {
				shiftby = (shiftby >> 1);
			}
			String maskString = Integer.toString((shiftby >> 24) & 255) + "." + Integer.toString((shiftby >> 16) & 255) + "." + Integer.toString((shiftby >> 8) & 255) + "." + Integer.toString(shiftby & 255);
			return InetAddress.getByName(maskString);
		} catch (Exception e) {
			FileLogger.error(e);
		}
		return null;
	}

	private static GenericPair<Long, String> ipvs = new GenericPair<Long, String>(0L, null);

	/**
	 * 获取主机所有正在使用的非回调(loopback)ipv4格式的地址。
	 * 
	 * @return String，多个用半角分号分隔。
	 */
	public static String getHostIPv4Address() {
		long c = System.currentTimeMillis();
		if (ipvs.getValue() == null || ipvs.getKey() == 0 || (c - ipvs.getKey()) > 1000L * 3600L) {
			StringBuilder sb = new StringBuilder();
			try {
				Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
				Enumeration<InetAddress> inetAddresses = null;
				// List<InterfaceAddress> iads = null;
				String add = null;
				for (NetworkInterface netint : Collections.list(nets)) {
					if (!netint.isUp()) continue;
					if (netint.isLoopback()) continue;
					// iads = netint.getInterfaceAddresses();
					// for (InterfaceAddress iad : iads) {
					// System.out.printf("Netmask: %s\n", iad.getNetworkPrefixLength());
					// }
					inetAddresses = netint.getInetAddresses();
					for (InetAddress ia : Collections.list(inetAddresses)) {
						if (!(ia instanceof Inet4Address)) continue;
						add = ia.getHostAddress();
						sb.append(sb.length() > 0 ? ";" : "").append(add);
					}
				}
			} catch (SocketException e) {
				FileLogger.error(e);
			}
			ipvs.setKey(c);
			ipvs.setValue(sb.toString());
			return ipvs.getValue();
		} else {
			return ipvs.getValue();
		}
	}

	/**
	 * 获取主机所有ipv4格式的地址对象数组。
	 * 
	 * @return InetAddress[] 如果找不到有效地址可能返回null或空数组。
	 */
	public static InetAddress[] getHostAllIPv4Address() {
		try {
			Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
			Enumeration<InetAddress> inetAddresses = null;
			for (NetworkInterface netint : Collections.list(nets)) {
				if (!netint.isUp()) continue;
				if (netint.isLoopback()) continue;
				inetAddresses = netint.getInetAddresses();
				List<InetAddress> list = Collections.list(inetAddresses);
				InetAddress[] result = new InetAddress[list.size()];
				list.toArray(result);
				return result;
			}
		} catch (SocketException e) {
			FileLogger.error(e);
		}
		return null;
	}

	/**
	 * 检查两个ipv4地址是否在同一个子网内。
	 * 
	 * @param ip1 第一个ip地址
	 * @param ip2 第二个ip地址
	 * @param mask1 第一个ip地址对应的子网掩码地址
	 * @param mask2 第二个ip地址对应的子网掩码地址（通常需与第一个相同）
	 * @return boolean
	 */
	public static boolean checkSameSubnet(InetAddress ip1, InetAddress ip2, InetAddress mask1, InetAddress mask2) {
		if (ip1 == null) throw new RuntimeException("没有提供有效地址1参数！");
		if (ip2 == null) throw new RuntimeException("没有提供有效地址2参数！");
		if (mask1 == null) throw new RuntimeException("没有提供有效子网掩码1参数！");
		if (mask2 == null) throw new RuntimeException("没有提供有效子网掩码2参数！");
		int iipa1 = ByteBuffer.wrap(ip1.getAddress()).getInt();
		int iipa2 = ByteBuffer.wrap(ip2.getAddress()).getInt();
		int iipma1 = ByteBuffer.wrap(mask1.getAddress()).getInt();
		int iipma2 = ByteBuffer.wrap(mask2.getAddress()).getInt();
		return ((iipa1 & iipma1) == (iipa2 & iipma2));
	}

	/**
	 * 检查两个ipv4地址是否在同一个子网内。
	 * 
	 * @param ip1 第一个ip地址
	 * @param ip2 第二个ip地址
	 * @param mask ip地址对应的子网掩码地址
	 * @return boolean
	 */
	public static boolean checkSameSubnet(InetAddress ip1, InetAddress ip2, InetAddress mask) {
		if (ip1 == null) throw new RuntimeException("没有提供有效地址1参数！");
		if (ip2 == null) throw new RuntimeException("没有提供有效地址2参数！");
		if (mask == null) throw new RuntimeException("没有提供有效子网掩码参数！");
		int iipa1 = ByteBuffer.wrap(ip1.getAddress()).getInt();
		int iipa2 = ByteBuffer.wrap(ip2.getAddress()).getInt();
		int iipma = ByteBuffer.wrap(mask.getAddress()).getInt();
		return ((iipa1 & iipma) == (iipa2 & iipma));
	}

	/**
	 * 检查两个ipv4地址是否在同一个子网内。
	 * 
	 * @param ip1 第一个ip地址
	 * @param ip2 第二个ip地址
	 * @param mask ip地址对应的子网掩码地址
	 * @return boolean
	 */
	public static boolean checkSameSubnet(String ip1, String ip2, String mask) {
		if (ip1 == null) throw new RuntimeException("没有提供有效地址1参数！");
		if (ip2 == null) throw new RuntimeException("没有提供有效地址2参数！");
		if (mask == null) throw new RuntimeException("没有提供有效子网掩码参数！");
		try {
			int iipa1 = ByteBuffer.wrap(InetAddress.getByName(ip1).getAddress()).getInt();
			int iipa2 = ByteBuffer.wrap(InetAddress.getByName(ip2).getAddress()).getInt();
			int iipma = ByteBuffer.wrap(InetAddress.getByName(mask).getAddress()).getInt();
			return ((iipa1 & iipma) == (iipa2 & iipma));
		} catch (UnknownHostException e) {
			FileLogger.error(e);
		}
		return false;
	}

	/**
	 * 检查两个ipv4地址是否在同一个子网内。
	 * 
	 * @param ip1 第一个ip地址
	 * @param ip2 第二个ip地址
	 * @param mask1 第一个ip地址对应的网络位长度（用于计算子网掩码，有效值在0-32之间）
	 * @param mask2 第一个ip地址对应的网络位长度（用于计算子网掩码，有效值在0-32之间，通常与第一个相同）
	 * @return boolean
	 */
	public static boolean checkSameSubnet(InetAddress ip1, InetAddress ip2, int prefixLength1, int prefixLength2) {
		if (ip1 == null) throw new RuntimeException("没有提供有效地址1参数！");
		if (ip2 == null) throw new RuntimeException("没有提供有效地址2参数！");
		if (prefixLength1 <= 0 || prefixLength1 > 32) throw new RuntimeException("没有提供有效子网掩码1长度参数！");
		if (prefixLength2 <= 0 || prefixLength2 > 32) throw new RuntimeException("没有提供有效子网掩码2长度参数！");
		int iipa1 = ByteBuffer.wrap(ip1.getAddress()).getInt();
		int iipa2 = ByteBuffer.wrap(ip2.getAddress()).getInt();
		int iipma1 = ByteBuffer.wrap(getIPv4SubnetMask(prefixLength1).getAddress()).getInt();
		int iipma2 = ByteBuffer.wrap(getIPv4SubnetMask(prefixLength2).getAddress()).getInt();
		return ((iipa1 & iipma1) == (iipa2 & iipma2));
	}

	/**
	 * 检查两个ipv4地址是否在同一个子网内。
	 * 
	 * @param ip1 第一个ip地址
	 * @param ip2 第二个ip地址
	 * @param mask1 ip地址对应的网络位长度（用于计算子网掩码，有效值在0-32之间）
	 * @return boolean
	 */
	public static boolean checkSameSubnet(InetAddress ip1, InetAddress ip2, int prefixLength) {
		if (ip1 == null) throw new RuntimeException("没有提供有效地址1参数！");
		if (ip2 == null) throw new RuntimeException("没有提供有效地址2参数！");
		if (prefixLength <= 0 || prefixLength > 32) throw new RuntimeException("没有提供有效子网掩码1长度参数！");
		int iipa1 = ByteBuffer.wrap(ip1.getAddress()).getInt();
		int iipa2 = ByteBuffer.wrap(ip2.getAddress()).getInt();
		int iipma = ByteBuffer.wrap(getIPv4SubnetMask(prefixLength).getAddress()).getInt();
		return ((iipa1 & iipma) == (iipa2 & iipma));
	}

	/**
	 * 检查两个ipv4地址是否在同一个子网内。
	 * 
	 * @param ip1 第一个ip地址
	 * @param ip2 第二个ip地址
	 * @param mask1 ip地址对应的网络位长度（用于计算子网掩码，有效值在0-32之间）
	 * @return boolean
	 */
	public static boolean checkSameSubnet(String ip1, String ip2, int prefixLength) {
		if (ip1 == null) throw new RuntimeException("没有提供有效地址1参数！");
		if (ip2 == null) throw new RuntimeException("没有提供有效地址2参数！");
		if (prefixLength <= 0 || prefixLength > 32) throw new RuntimeException("没有提供有效子网掩码1长度参数！");
		try {
			int iipa1 = ByteBuffer.wrap(InetAddress.getByName(ip1).getAddress()).getInt();
			int iipa2 = ByteBuffer.wrap(InetAddress.getByName(ip2).getAddress()).getInt();
			int iipma = ByteBuffer.wrap(getIPv4SubnetMask(prefixLength).getAddress()).getInt();
			return ((iipa1 & iipma) == (iipa2 & iipma));
		} catch (UnknownHostException e) {
			FileLogger.error(e);
		}
		return false;
	}

	public static void main(String[] args) {
		String ip1 = "172.23.3.4";
		String mask1 = "255.255.255.0";
		String ip2 = "172.23.4.4";
		String mask2 = "255.255.255.0";
		try {
			System.out.println("result=" + checkSameSubnet(ip1, ip2, mask1));
			System.out.println("result=" + checkSameSubnet(InetAddress.getByName(ip1), InetAddress.getByName(ip2), InetAddress.getByName(mask1)));
			System.out.println("result=" + checkSameSubnet(InetAddress.getByName(ip1), InetAddress.getByName(ip2), InetAddress.getByName(mask1), InetAddress.getByName(mask2)));
			System.out.println("result=" + checkSameSubnet(InetAddress.getByName(ip1), InetAddress.getByName(ip2), 10));
			System.out.println("result=" + checkSameSubnet(ip1, ip2, 20));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

