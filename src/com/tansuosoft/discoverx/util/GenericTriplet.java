/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

/**
 * 提供用于存储三个相关泛型对象的基本实用工具类。
 * 
 * @author coca@tansuosoft.cn
 */
public class GenericTriplet<F, S, T> {
	private F m_first = null; // 第一个值。
	private S m_second = null; // 第二个值。
	private T m_third = null; // 第三个值。

	/**
	 * 缺省构造器。
	 */
	public GenericTriplet() {
	}

	/**
	 * 接收值参数的构造器。
	 * 
	 * @param first
	 * @param second
	 * @param third
	 */
	public GenericTriplet(F first, S second, T third) {
		this.m_first = first;
		this.m_second = second;
		this.m_third = third;
	}

	/**
	 * 返回第一个值。
	 * 
	 * @return Object
	 */
	public F getFirst() {
		return this.m_first;
	}

	/**
	 * 设置第一个值。
	 * 
	 * @param first F
	 */
	public void setFirst(F first) {
		this.m_first = first;
	}

	/**
	 * 返回第二个值。
	 * 
	 * @return S
	 */
	public S getSecond() {
		return this.m_second;
	}

	/**
	 * 设置第二个值。
	 * 
	 * @param second S
	 */
	public void setSecond(S second) {
		this.m_second = second;
	}

	/**
	 * 返回第三个值。
	 * 
	 * @return T
	 */
	public T getThird() {
		return this.m_third;
	}

	/**
	 * 设置第三个值。
	 * 
	 * @param third T
	 */
	public void setThird(T third) {
		this.m_third = third;
	}

	@Override
	public String toString() {
		return String.format("%1$s|%2$s|%3$s", StringUtil.getValueString(this.getFirst(), "null"), StringUtil.getValueString(this.getSecond(), "null"), StringUtil.getValueString(this.getThird(), "null"));
	}
}

