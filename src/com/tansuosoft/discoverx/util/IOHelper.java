/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 文件读写实用工具类。
 * 
 * @author coca@tansuosoft.cn
 */
public class IOHelper {
	/**
	 * 从文本文件对象中读取内容并转换为字符数组。
	 * 
	 * @param file File 对象
	 * @return char[] 返回读取到的字符数组
	 */
	public static char[] readChars(File file) {
		CharArrayWriter caw = new CharArrayWriter();
		try {
			Reader fr = new FileReader(file);
			Reader in = new BufferedReader(fr);
			int count = 0;
			char[] buf = new char[16384];
			while ((count = in.read(buf)) != -1) {
				if (count > 0) caw.write(buf, 0, count);
			}
			in.close();
		} catch (Exception e) {
			FileLogger.error(e);
		}
		return caw.toCharArray();
	}

	/**
	 * 从二进制文件对象中读取内容并返回读取的字节数组。
	 * 
	 * @param file 要读取的File对象
	 * @return byte[] 读取后的字节数据
	 */
	public static byte[] readBytes(File file) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			InputStream fis = new FileInputStream(file);
			InputStream is = new BufferedInputStream(fis);
			int count = 0;
			byte[] buf = new byte[16384];
			while ((count = is.read(buf)) != -1) {
				if (count > 0) baos.write(buf, 0, count);
			}
			is.close();
		} catch (Exception e) {
			FileLogger.error(e);
		}
		return baos.toByteArray();
	}

	/**
	 * 将data对应的字节数组内容写入到file指定的二进制文件中。
	 * 
	 * @param file File对象
	 * @param data byte[]
	 */
	public static void writeBytes(File file, byte[] data) {
		try {
			OutputStream fos = new FileOutputStream(file);
			OutputStream os = new BufferedOutputStream(fos);
			os.write(data);
			os.flush();
			os.close();
		} catch (Exception e) {
			FileLogger.error(e);
		}
	}

	/**
	 * 将data对应的字符数组内容写入到file指定的文本文件中。
	 * 
	 * @param file File对象
	 * @param data char[]
	 */
	public static void writeChars(File file, char[] data) {
		try {
			Writer fos = new FileWriter(file);
			Writer os = new BufferedWriter(fos);
			os.write(data);
			os.flush();
			os.close();
		} catch (Exception e) {
			FileLogger.error(e);
		}
	}
}

