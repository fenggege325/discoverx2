/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 对象、类、反射相关的实用工具。
 * 
 * <p>
 * 频繁调用时、对性能要求高的地方请谨慎使用，可能影响执行速度。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class ObjectUtil {
	/**
	 * 缺省构造器。
	 */
	private ObjectUtil() {
	}

	/**
	 * 返回对象指定的方法
	 * 
	 * @param o Object-要查找其方法的对象。
	 * @param methodName String-方法名
	 * @param parameterTypes Class<?>...-表示方法参数。
	 * @return java.lang.reflect.Method
	 */
	public static java.lang.reflect.Method getMethod(Object o, String methodName, Class<?>... parameterTypes) {
		if (o == null || methodName == null || methodName.length() == 0) return null;
		Class<? extends Object> cls = o.getClass();
		try {
			return cls.getMethod(methodName, parameterTypes);
		} catch (SecurityException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		} catch (NoSuchMethodException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.debug("找不到“%1$s.%2$s”对应方法！", o.getClass().getName(), methodName);
		}
		return null;
	}

	/**
	 * 针对目标对象调用指定方法，并返回结果。
	 * 
	 * @param o
	 * @param method
	 * @param args
	 * @return Object 如果出现异常或者没有返回值，则返回null.
	 */
	public static Object getMethodResult(Object o, java.lang.reflect.Method method, Object... args) {
		if (o == null || method == null) return null;
		try {
			return method.invoke(o, args);
		} catch (IllegalArgumentException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		} catch (IllegalAccessException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		} catch (InvocationTargetException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
		return null;
	}

	/**
	 * 获取对象实例的所有实例方法。
	 * 
	 * <p>
	 * 大规模使用会影响性能，请谨慎使用。
	 * </p>
	 * 
	 * @param o
	 * @return Method[]
	 */
	public static Method[] getInstanceMethods(Object o) {
		if (o == null) return null;
		Method[] methods = o.getClass().getMethods();
		if (methods == null || methods.length == 0) return null;
		return methods;
	}

	/**
	 * 获取对象实例的所有getter和setter的实例方法。
	 * 
	 * @param o
	 * @return Method[]
	 */
	public static Method[] getInstanceXetters(Object o) {
		if (o == null) return null;
		Method[] methods = o.getClass().getMethods();
		if (methods == null || methods.length == 0) return null;
		String name = null;
		ArrayList<Method> al = new ArrayList<Method>(methods.length);
		for (Method x : methods) {
			name = x.getName();
			if (!name.startsWith("get") && !name.startsWith("set")) continue;
			al.add(x);
		}
		Method[] rets = new Method[al.size()];
		int i = 0;
		for (Method x : al) {
			rets[i++] = x;
		}
		return rets;
	}

	/**
	 * 获取对象实例的所有实例方法名称。
	 * 
	 * <p>
	 * 大规模使用会影响性能，请谨慎使用。
	 * </p>
	 * 
	 * @param o
	 * @return String[]
	 */
	public static String[] getInstanceMethodNames(Object o) {
		Method[] methods = getInstanceMethods(o);
		if (methods == null || methods.length == 0) return null;
		String[] strs = new String[methods.length];
		int i = 0;
		for (Method x : methods) {
			strs[i++] = x.getName();
		}
		return strs;
	}

	/**
	 * 获取对象实例的所有getter和setter的实例方法名称。
	 * 
	 * <p>
	 * 大规模使用会影响性能，请谨慎使用。
	 * </p>
	 * 
	 * @param o
	 * @return String[]
	 */
	public static String[] getInstanceXetterNames(Object o) {
		Method[] methods = getInstanceXetters(o);
		if (methods == null || methods.length == 0) return null;
		String[] strs = new String[methods.length];
		int i = 0;
		for (Method x : methods) {
			strs[i++] = x.getName();
		}
		return strs;
	}

	/**
	 * 获取对象实例的所有getter和setter配对（即具有可读写特性）的属性方法哈希集合（getter对应的Method作为key，setter对应的Method作为value）。
	 * 
	 * <p>
	 * 大规模使用会影响性能，请谨慎使用。
	 * </p>
	 * 
	 * @param o
	 * @return Map<Method, Method>
	 */
	public static Map<Method, Method> getInstanceRWPropMethods(Object o) {
		Method[] methods = getInstanceXetters(o);
		if (methods == null || methods.length == 0) return null;

		String name = null;
		Map<String, Method> getters = new HashMap<String, Method>(methods.length);
		Map<String, Method> setters = new HashMap<String, Method>(methods.length);
		for (Method x : methods) {
			name = x.getName();
			if (name.startsWith("get")) getters.put(name.substring(3), x);
			if (name.startsWith("set")) setters.put(name.substring(3), x);
		}
		TreeMap<Method, Method> results = new TreeMap<Method, Method>(new PropertyMethodComparator());
		Method getter = null;
		Method setter = null;
		Class<?> returnType = null;
		Class<?>[] params = null;
		for (String x : getters.keySet()) {
			getter = getters.get(x);
			if (getter == null) continue;
			setter = setters.get(x);
			if (setter == null) continue;
			returnType = getter.getReturnType();
			if (returnType == null) continue;
			params = getter.getParameterTypes();
			if (params.length > 0) continue;
			params = setter.getParameterTypes();
			if (params == null || params.length != 1) continue;
			if (!returnType.getName().equalsIgnoreCase(params[0].getName())) continue;
			results.put(getter, setter);
		}
		getters.clear();
		setters.clear();

		return results;
	}

	/**
	 * 获取对象实例的所有getter和setter配对（即具有可读写特性）的属性名称（去掉get/set前缀的名称）。
	 * 
	 * <p>
	 * 大规模使用会影响性能，请谨慎使用。
	 * </p>
	 * 
	 * @param o
	 * @return String[]
	 */
	public static String[] getInstanceRWPropNames(Object o) {
		Map<Method, Method> ht = getInstanceRWPropMethods(o);
		if (ht == null || ht.size() == 0) return null;
		String[] strs = new String[ht.size()];
		int i = 0;
		for (Method x : ht.keySet()) {
			strs[i++] = x.getName().substring(3);
		}
		return strs;
	}

	/**
	 * 判断指定对象（o）是否有实现/继承某个类/接口。
	 * 
	 * @param o
	 * @param clazz
	 * @return boolean
	 */
	public static boolean checkInstance(Object o, Class<?> clazz) {
		if (o == null || clazz == null) return false;
		return clazz.isInstance(o);
	}

	/**
	 * 判断指定对象（o）是否有实现/继承某个类/接口。
	 * 
	 * @param o
	 * @param className 类/接口的全限定名称
	 * @return boolean
	 */
	public static boolean checkInstance(Object o, String className) {
		if (o == null || className == null || className.length() == 0) return false;
		try {
			Class<?> clazz = Class.forName(className);
			return checkInstance(o, clazz);
		} catch (ClassNotFoundException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
		return false;
	}

	/**
	 * 判断指定对象对应的类是否有实现某个接口。
	 * 
	 * @param clazz
	 * @param interfaceName 接口全限定名称(不包含范型部分)。
	 * @return boolean
	 */
	public static boolean checkInterface(Class<?> clazz, String interfaceName) {
		if (clazz == null || interfaceName == null || interfaceName.length() == 0) return false;
		for (Class<?> x : clazz.getInterfaces()) {
			if (x.getName().toLowerCase().indexOf(interfaceName.toLowerCase()) >= 0) return true;
		}
		if (clazz.getName().toLowerCase().indexOf(interfaceName.toLowerCase()) >= 0) return true;
		return false;
	}

	/**
	 * 判断指定对象对应的类是否有实现某个接口。
	 * 
	 * @param clazz
	 * @param interfaceClass 接口对应的类。
	 * @return boolean
	 */
	public static boolean checkInterface(Class<?> clazz, Class<?> interfaceClass) {
		if (clazz == null || interfaceClass == null) return false;
		for (Class<?> x : clazz.getInterfaces()) {
			if (x.getName().toLowerCase().indexOf(interfaceClass.getName().toLowerCase()) >= 0) return true;
		}
		if (clazz.getName().indexOf(interfaceClass.getName()) >= 0) return true;
		return false;
	}

	/**
	 * 判断指定对象对应的类名称是否有实现某个接口。
	 * 
	 * @param className String
	 * @param interfaceClass 接口对应的类。
	 * @return boolean
	 */
	public static boolean checkInterface(String className, Class<?> interfaceClass) {
		if (className == null || className.length() == 0 || interfaceClass == null) return false;
		Class<?> clazz = null;
		try {
			clazz = Class.forName(className);
		} catch (ClassNotFoundException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
		return checkInterface(clazz, interfaceClass);

	}

	/**
	 * 获取raw指定的文本值转换为type指定的java类型名称对应的原始类型值。
	 * 
	 * <p>
	 * 针对EnumBase（系统枚举）、java.lang.String、java.lang.Boolean、java.lang.Character、java.lang.Byte、java.lang.Short、java.lang.Integer、java.lang.Long、java.lang.Float、
	 * java.lang.Double等类型进行直接转换，对于其它类型则获取其对应默认实例（必须有公开的默认构造器，否则返回null）。
	 * </p>
	 * <p>
	 * 比如type为“java.lang.Integer”或“int”，raw为“1”时，则返回1（java.lang.Integer）。
	 * </p>
	 * 
	 * @param type 某个Class对应的{@link java.lang.Class#getName()}的返回值。
	 * @param raw
	 * @return Object
	 */
	public static Object getBoxResult(String type, String raw) {
		if (type == null || type.length() == 0) return null;
		String src = raw;
		if (src == null || src.length() == 0) src = "0";

		if (type.equalsIgnoreCase("boolean") || type.equalsIgnoreCase("java.lang.Boolean")) {
			if (src == null || src.length() == 0) return Boolean.FALSE;
			return (StringUtil.getValueBool(src, false) ? Boolean.TRUE : Boolean.FALSE);
		} else if (type.equalsIgnoreCase("java.lang.String")) {
			return (raw == null ? "" : raw);
		} else if (type.equalsIgnoreCase("char") || type.equalsIgnoreCase("java.lang.Character")) {
			return Character.valueOf(src.charAt(1));
		} else if (type.equalsIgnoreCase("byte") || type.equalsIgnoreCase("java.lang.Byte")) {
			return Byte.valueOf(src);
		} else if (type.equalsIgnoreCase("short") || type.equalsIgnoreCase("java.lang.Short")) {
			return Short.valueOf(src);
		} else if (type.equalsIgnoreCase("int") || type.equalsIgnoreCase("java.lang.Integer")) {
			return Integer.valueOf(src);
		} else if (type.equalsIgnoreCase("long") || type.equalsIgnoreCase("java.lang.Long")) {
			return Long.valueOf(src);
		} else if (type.equalsIgnoreCase("float") || type.equalsIgnoreCase("java.lang.Float")) {
			return Float.valueOf(src);
		} else if (type.equalsIgnoreCase("double") || type.equalsIgnoreCase("java.lang.Double")) {
			return Double.valueOf(src);
		} else if (ObjectUtil.checkInterface(type, EnumBase.class)) {
			Class<?> clazz = null;
			try {
				clazz = Class.forName(type);
			} catch (ClassNotFoundException e) {
				com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
			}
			EnumBase[] values = (EnumBase[]) clazz.getEnumConstants();
			for (EnumBase e : values) {
				if (src.equals(e.getIntValue() + "")) { return e; }
			}
			return null;
		} else if (type.equalsIgnoreCase("java.lang.Object")) {
			return raw;
		} else { // 其它类型尝试转换为对应对象
			Object obj = Instance.newInstance(type);
			return (obj == null ? raw : obj);
		}
	}

	/**
	 * 获取raw指定的文本值转换为type指定的java类型名称对应的原始类型值。
	 * 
	 * <p>
	 * 针对EnumBase（系统枚举）、java.lang.String、java.lang.Boolean、java.lang.Character、java.lang.Byte、java.lang.Short、java.lang.Integer、java.lang.Long、java.lang.Float、
	 * java.lang.Double等类型进行直接转换，对于其它类型则获取其对应默认实例（必须有公开的默认构造器，否则返回null）。
	 * </p>
	 * <p>
	 * 比如type为“java.lang.Integer”或“int”，raw为“1”时，则返回1（int）
	 * </p>
	 * 
	 * @param type 某个Class对应的{@link java.lang.Class#getName()}的返回值。
	 * @param raw
	 * @return Object
	 */
	public static Object getResult(String type, String raw) {
		if (type == null || type.length() == 0) return null;
		String src = raw;
		if (src == null || src.length() == 0) src = "0";
		if (type.equalsIgnoreCase("boolean") || type.equalsIgnoreCase("java.lang.Boolean")) {
			if (src == null || src.length() == 0) return false;
			return StringUtil.getValueBool(src, false);
		} else if (type.equalsIgnoreCase("string") || type.equalsIgnoreCase("java.lang.String")) {
			return (raw == null ? "" : raw);
		} else if (type.equalsIgnoreCase("char") || type.equalsIgnoreCase("java.lang.Character")) {
			return src.charAt(1);
		} else if (type.equalsIgnoreCase("byte") || type.equalsIgnoreCase("java.lang.Byte")) {
			return Byte.parseByte(src);
		} else if (type.equalsIgnoreCase("short") || type.equalsIgnoreCase("java.lang.Short")) {
			return Short.parseShort(src);
		} else if (type.equalsIgnoreCase("int") || type.equalsIgnoreCase("java.lang.Integer")) {
			return Integer.parseInt(src);
		} else if (type.equalsIgnoreCase("long") || type.equalsIgnoreCase("java.lang.Long")) {
			return Long.parseLong(src);
		} else if (type.equalsIgnoreCase("float") || type.equalsIgnoreCase("java.lang.Float")) {
			return Float.parseFloat(src);
		} else if (type.equalsIgnoreCase("double") || type.equalsIgnoreCase("java.lang.Double")) {
			return Double.parseDouble(src);
		} else if (ObjectUtil.checkInterface(type, EnumBase.class)) {
			Class<?> clazz = null;
			try {
				clazz = Class.forName(type);
			} catch (ClassNotFoundException e) {
				com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
			}
			EnumBase[] values = (EnumBase[]) clazz.getEnumConstants();
			for (EnumBase e : values) {
				if (src.equals(e.getIntValue() + "")) { return e; }
			}
			return null;
		} else if (type.equalsIgnoreCase("java.lang.Object")) {
			return raw;
		} else { // 其它类型尝试转换为对应对象
			Object obj = Instance.newInstance(type);
			return (obj == null ? raw : obj);
		}
	}

	/**
	 * 获取raw指定的文本值转换为cls指定的java类型信息对应的原始类型值。
	 * 
	 * @see ObjectUtil#getResult(String, String)
	 * @param cls
	 * @param raw
	 * @return Object
	 */
	public static Object getResult(Class<?> cls, String raw) {
		if (cls == null) return null; // || raw == null || raw.length() == 0
		return getResult(cls.getName(), raw);
	}

	/**
	 * 设置obj指定对象中propName指定可读写属性名对应的属性值为propValue。
	 * 
	 * <p>
	 * 在性能要求很严格的地方大量使用会严重影响执行性能，请谨慎使用。
	 * </p>
	 * 
	 * @param obj Object 目标对象。
	 * @param propName String 对象可读写属性名称（不包括get/set前缀）。
	 * @param propValue Object 属性值。
	 */
	public static void propSetValue(Object obj, String propName, Object propValue) {
		if (obj == null || propName == null || propName.length() == 0) return;
		String propNameFormatted = StringUtil.toPascalCase(propName);
		Method get = ObjectUtil.getMethod(obj, "get" + propNameFormatted);
		if (get == null) return;
		Class<?> retType = get.getReturnType();
		Method set = ObjectUtil.getMethod(obj, "set" + propNameFormatted, retType);
		if (set == null) return;
		String tip = String.format("%1$s.set%2$s(%3$s)", obj.getClass().getName(), propNameFormatted, (propValue == null ? "null" : propValue.toString()));
		try {
			set.invoke(obj, propValue);
		} catch (IllegalArgumentException e) {
			FileLogger.debug("调用“%1$s”错误：参数不符", tip);
		} catch (IllegalAccessException e) {
			FileLogger.debug("调用“%1$s”错误：非法访问", tip);
		} catch (InvocationTargetException e) {
			FileLogger.debug("调用“%1$s”错误：调用异常", tip);
		} catch (Exception e) {
			FileLogger.debug("调用“%1$s”错误：%2$s", tip, e.getMessage());
		}
	}

	/**
	 * 设置obj指定对象中propName指定可读写属性名对应的属性值为propValue转换为属性值类型后的结果。
	 * 
	 * <p>
	 * 在性能要求很严格的地方大量使用会严重影响执行性能，请谨慎使用。
	 * </p>
	 * 
	 * @param obj Object 目标对象。
	 * @param propName String 对象可读写属性名称（不包括get/set前缀）。
	 * @param propValue String 属性值的文本形式。
	 */
	public static void propSetValueFromText(Object obj, String propName, String propValue) {
		if (obj == null || propName == null || propName.length() == 0) return;
		Method get = ObjectUtil.getMethod(obj, "get" + propName);
		if (get == null) return;
		Class<?> retType = get.getReturnType();
		Method set = ObjectUtil.getMethod(obj, "set" + propName, retType);
		if (set == null) return;
		String tip = String.format("%1$s.set%2$s(%3$s)", obj.getClass().getName(), propName, propValue);
		try {
			Object x = ObjectUtil.getResult(get.getReturnType(), propValue);
			set.invoke(obj, x);
		} catch (IllegalArgumentException e) {
			FileLogger.debug("调用“%1$s”错误：参数不符", tip);
		} catch (IllegalAccessException e) {
			FileLogger.debug("调用“%1$s”错误：非法访问", tip);
		} catch (InvocationTargetException e) {
			FileLogger.debug("调用“%1$s”错误：调用异常", tip);
		} catch (Exception e) {
			FileLogger.debug("调用“%1$s”错误：%2$s", tip, e.getMessage());
		}

	}// func end

	/**
	 * 将raw对应的文本指转换为v对应的java类型(Class)值，然后把这个值直接设置给v。
	 * 
	 * <p>
	 * 只针对EnumBase（系统枚举）、java.lang.String、java.lang.Boolean、java.lang.Character、java.lang.Byte、java.lang.Short、java.lang.Integer、java.lang.Long、java.lang.Float、 java.lang.Double等类型进行转换。
	 * </p>
	 * 
	 * @param v
	 * @param raw
	 * @return boolean 如果赋值成功返回true，否则返回false。
	 */
	public static boolean setResultDirect(Object v, String raw) {
		boolean ret = true;
		Class<?> cls = v.getClass();
		String type = cls.getName();
		String src = raw;
		if (src == null || src.length() == 0) src = "0";

		if (v instanceof EnumBase) {
			v = ((EnumBase) v).parse(src);
		} else if (v instanceof String) {
			v = (raw == null ? "" : raw);
		} else {
			if (type.equals("java.lang.Boolean")) {
				if (src == null || src.length() == 0) v = Boolean.FALSE;
				else v = (src.equals("1") || src.equalsIgnoreCase("yes") || src.equalsIgnoreCase("true") || src.equalsIgnoreCase("y") ? Boolean.TRUE : Boolean.FALSE);
			} else if (type.equals("java.lang.Character")) {
				v = Character.valueOf(src.charAt(1));
			} else if (type.equals("java.lang.Byte")) {
				v = Byte.valueOf(src);
			} else if (type.equals("java.lang.Short")) {
				v = Short.valueOf(src);
			} else if (type.equals("java.lang.Integer")) {
				v = Integer.parseInt(src);
			} else if (type.equals("java.lang.Long")) {
				v = Long.parseLong(src);
			} else if (type.equals("java.lang.Float")) {
				v = Float.parseFloat(src);
			} else if (type.equals("java.lang.Double")) {
				v = Double.parseDouble(src);
			} else ret = false;
		}
		System.out.println("v=" + (v == null ? "[null]" : v.toString()));
		return ret;
	}

	/**
	 * java基础数据类型对应的java封装(Box)类名称数组。
	 */
	public static final String[] PRIMITIVE_TYPES = { "java.lang.Boolean", "java.lang.Character", "java.lang.Byte", "java.lang.Short", "java.lang.Integer", "java.lang.Long", "java.lang.Float", "java.lang.Double" };

	/**
	 * 检查指定的className是否{@link java.lang.Void}。
	 * 
	 * @param className
	 * @return
	 */
	public static boolean checkVoid(String className) {
		return (className.equalsIgnoreCase("java.lang.Void") || className.equalsIgnoreCase("void"));
	}

	/**
	 * 检查指定的className是否原始类型对象（即是否匹配{@link ObjectUtil#PRIMITIVE_TYPES}中的类型）。
	 * 
	 * @param className
	 * @return
	 */
	public static boolean checkPrimitive(String className) {
		for (String t : PRIMITIVE_TYPES) {
			if (t.equalsIgnoreCase(className)) return true;
		}
		return false;
	}

	/**
	 * 检查指定的className是否简单对象（即是否匹配{@link ObjectUtil#PRIMITIVE_TYPES}中的类型或字符串类型）。
	 * 
	 * @param className
	 * @return boolean
	 */
	public static boolean checkSimple(String className) {
		for (String t : PRIMITIVE_TYPES) {
			if (t.equalsIgnoreCase(className)) return true;
		}
		if (className.equals("java.lang.String")) return true;
		return false;
	}

	/**
	 * 根据className指定的原始数据类型名称获取其缺省结果实例。
	 * 
	 * @param className 某个Class对应的{@link java.lang.Class#getName()}的返回值。
	 * @return Object
	 */
	public static Object getDefaultPrimitiveObject(String className) {
		Object x = null;
		x = getResult(className, "0");
		return x;
	}

	/**
	 * 克隆列表中的对象并返回包含克隆对象的列表。
	 * 
	 * <p>
	 * 列表中包含的对象必须实现了{@link java.lang.Cloneable}接口并提供了重载后的{@link Object#clone()}公共方法。
	 * </p>
	 * 
	 * @param <T>
	 * @param list
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> cloneList(List<T> list) {
		if (list == null) return null;
		if (list.isEmpty()) return new ArrayList<T>();
		List<T> result = new ArrayList<T>(list.size());
		try {
			Method clone = null;
			for (T t : list) {
				if (t == null || !(t instanceof Cloneable)) continue;
				if (clone == null) clone = t.getClass().getMethod("clone");
				if (clone == null) continue;
				result.add((T) clone.invoke(t));
			}
		} catch (IllegalArgumentException e) {
			FileLogger.error(e);
		} catch (IllegalAccessException e) {
			FileLogger.error(e);
		} catch (InvocationTargetException e) {
			FileLogger.error(e);
		} catch (SecurityException e) {
			FileLogger.error(e);
		} catch (NoSuchMethodException e) {
			FileLogger.error(e);
		}
		return result;
	}
}// class end

