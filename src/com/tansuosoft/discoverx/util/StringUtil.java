/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类。
 */
public class StringUtil {

	/**
	 * 判断指定的字符串是否大写。
	 * 
	 * @param str
	 * @return boolean 如果是大写则返回true,否则返回false.
	 */
	public static boolean isUpperCase(String str) {
		if (str == null) return false;
		String strUpperCase = str.toUpperCase();
		return (strUpperCase.equals(str));
	}

	/**
	 * 判断指定的字符串是否小写。
	 * 
	 * @param str
	 * @return boolean 如果是小写则返回true,否则返回false.
	 */
	public static boolean isLowerCase(String str) {
		if (str == null) return false;
		String strLowerCase = str.toLowerCase();
		return (strLowerCase.equals(str));
	}

	protected static final Pattern UNID = Pattern.compile("^[0-9A-F]{32}$", Pattern.CASE_INSENSITIVE);

	/**
	 * 判断指定字符串是否32位的UNID值（包括0-9A-F的32位字符串）。
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isUNID(String str) {
		if (str == null || str.length() != 32) return false;
		return UNID.matcher(str).matches();
	}

	/**
	 * 把指定字符串转换为Pascal命名格式的字符串，如"backColor"转换后变为"BackColor"。
	 * 
	 * @param str String 初始字符串
	 * @return String 转换后的字符串，如果已经是Camel命名格式则返回值和初始值一样。
	 */
	public static String toPascalCase(String str) {
		if (str == null) return null;
		if (str.length() == 0) return "";
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	/**
	 * 把指定字符串转换为Camel命名格式的字符串，如"BackColor"转换后变为"backColor"。
	 * 
	 * @param str String 初始字符串
	 * @return String 转换后的字符串，如果已经是Camel命名格式则返回值和初始值一样。
	 */
	public static String toCamelCase(String str) {
		if (str == null) return null;
		if (str.length() == 0) return "";
		if (isUpperCase(str)) return str;
		if (str.length() > 1 && isUpperCase(str.substring(1, 2))) return str;
		return str.substring(0, 1).toLowerCase() + str.substring(1);
	}

	private static final Pattern pn = Pattern.compile("^[+-]?[\\d]+$");
	private static final Pattern pf = Pattern.compile("^[+-]?\\d{0,}\\.{0,1}\\d{0,}$");

	/**
	 * 判断字符串是否可转换为整数数字。
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isNumber(String str) {
		if (str == null || str.length() == 0) return false;
		// String regex = "^[+-]?[\\d]+$";
		// return Pattern.matches(regex, str);
		return pn.matcher(str).matches();
	}

	/**
	 * 判断字符串是否可转换为浮点数字。
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isFloat(String str) {
		if (str == null || str.length() == 0) return false;
		// String regex = "^[+-]?\\d{0,}\\.{0,1}\\d{0,}$";
		// return Pattern.matches(regex, str);
		return pf.matcher(str).matches();
	}

	/**
	 * 对原始字符串进行xml输出编码。
	 * 
	 * <p>
	 * 将对内置的五个xml特殊字符进行编码。
	 * </p>
	 * 
	 * @param raw
	 * @return String
	 */
	public static String encode4Xml(String raw) {
		if (raw == null) return null;
		if (raw.length() == 0) return raw;
		return raw.replace("&", "&amp;").replace("\"", "&quot;").replace("'", "&apos;").replace("<", "&lt;").replace(">", "&gt;");
	}

	/**
	 * 对原始字符串进行json输出编码。
	 * 
	 * <p>
	 * 编码结果：“\”(反斜杆)->“\\”(两个反斜杆)；“'”(单引号)->“\'”(反斜杆加单引号)。
	 * </p>
	 * 
	 * @param raw
	 * @return String
	 */
	public static String encode4Json(String raw) {
		if (raw == null) return "";
		if (raw.length() == 0) return raw;
		return raw.replace("\\", "\\\\").replace("'", "\\'").replace("\r\n", "\\r\\n");
	}

	/**
	 * 对原始字符串进行html输出编码。
	 * 
	 * <p>
	 * 编码结果：“"”(半角双引号)->“&amp;quot;”；“>”->“&amp;gt;”；“<”->“&amp;lt;”。
	 * </p>
	 * 
	 * @param raw
	 * @return String
	 */
	public static String encode4HtmlPropValue(String raw) {
		if (raw == null) return null;
		if (raw.length() == 0) return raw;
		return raw.replace("\"", "&quot;").replace("<", "&lt;").replace(">", "&gt;");
	}

	/**
	 * 提取raw指定的字符串对应的algorithm指定的摘要算法的摘要结果。
	 * <p>
	 * 字符串先被转换为“UTF-8”编码格式的字节后再提取摘要。
	 * </p>
	 * 
	 * @param raw
	 * @param algorithm
	 * @return String
	 */
	private static String getHashString(String raw, String algorithm) {
		if (raw == null) return null;
		if (raw.length() == 0) return "";
		byte[] data = null;
		MessageDigest md5 = null;
		try {
			data = raw.getBytes("UTF-8");// .getBytes("UTF-16LE");
			md5 = MessageDigest.getInstance(algorithm);
		} catch (NoSuchAlgorithmException ex) {
			return null;
		} catch (UnsupportedEncodingException ex) {
			return null;
		}
		md5.update(data);
		byte[] array = md5.digest();
		StringBuilder ret = new StringBuilder();
		int b = 0;
		for (int j = 0; j < array.length; ++j) {
			b = array[j] & 0xFF;
			if (b < 0x10) ret.append('0');
			ret.append(Integer.toHexString(b));
		}
		return ret.toString().toUpperCase();
	}

	/**
	 * 获取指定的输入字符串的MD5哈希结果字符串。
	 * 
	 * <p>
	 * 字符串先被转换为“UTF-8”编码格式的字节后再提取摘要。
	 * </p>
	 * 
	 * @param raw
	 * @return String
	 */
	public static String getMD5HashString(String raw) {
		return getHashString(raw, "MD5");
	}

	/**
	 * 获取指定的输入字符串的SHA1哈希结果字符串。
	 * 
	 * <p>
	 * 字符串先被转换为“UTF-8”编码格式的字节后再提取摘要。
	 * </p>
	 * 
	 * @param raw
	 * @return String
	 */
	public static String getSHA1HashString(String raw) {
		return getHashString(raw, "SHA1");
	}

	/**
	 * 把Object转换为String，如果是原始类型，则直接转换为String，如果是数组类型则把数组中每个元素转为字符串并用半角逗号分隔， 其它对象转换结果为其toString方法的返回值。
	 * 
	 * @param obj
	 * @param returnIfNull 如果obj为null时默认返回的值。
	 * @return String
	 */
	public static String convertToString(Object obj, String returnIfNull) {
		if (obj == null) return returnIfNull;
		Class<?> cls = obj.getClass();
		if (cls.isArray()) {
			StringBuffer sb = new StringBuffer();
			for (Object x : (Object[]) obj) {
				sb.append(sb.length() > 0 ? "," : "").append(convertToString(x, returnIfNull));
			}
			return sb.toString();
		} else {
			return obj.toString();
		}
	}

	/**
	 * 把Object转换为String，如果是原始类型，则直接转换为String，如果是数组类型则把数组中每个元素转为字符串并用半角逗号分隔， 其它对象转换结果为其toString方法的返回值。
	 * 
	 * @param obj
	 * @param returnIfNull 如果obj为null时默认返回的值。
	 * @return String
	 */
	public static String getValueString(Object obj, String returnIfNull) {
		return convertToString(obj, returnIfNull);
	}

	/**
	 * 检查obj是否为null或者其toString结果返回的字符串长度是否为0。
	 * 
	 * @param obj
	 * @return boolean
	 */
	public static boolean isNullOrEmpty(Object obj) {
		return (obj == null || obj.toString().length() == 0);
	}

	/**
	 * 把HttpServletRequest的getQueryString返回的值转换为支持中文并返回。
	 * 
	 * @param raw HttpServletRequest的getQueryString返回的值
	 * @return String 转换后的结果，如果HttpServletRequest的getQueryString返回的值为null则返回null。
	 */
	public static String getQueryString(String raw) {
		if (raw == null) return null;
		if (raw.length() == 0) return raw;
		try {
			return new String(raw.getBytes("ISO8859_1"), "gbk");
		} catch (UnsupportedEncodingException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
		return null;
	}

	/**
	 * 在str中从左向右查找sub，并返回sub位置左边的字符串。
	 * 
	 * @param str
	 * @param sub
	 * @param fromIndex fromIndex用于指定str中哪个位置开始查找sub
	 * @return String 如果找不到sub则返回str本身。
	 */
	public static String stringLeft(String str, String sub, int fromIndex) {
		if (str == null) return str;
		if (fromIndex > str.length()) return null;
		int end = str.indexOf(sub, fromIndex);
		if (end != -1) { return str.substring(0, end); }
		return str;
	}

	/**
	 * 在str中从第一个字符开始从左向右查找sub，并返回sub位置左边的字符串。
	 * 
	 * @param str
	 * @param sub
	 * @return String 如果找不到sub则返回str本身。
	 */
	public static String stringLeft(String str, String sub) {
		return stringLeft(str, sub, 0);
	}

	/**
	 * 在str中从左向右查找sub，并返回sub位置右边的字符串
	 * 
	 * @param str
	 * @param sub
	 * @param fromIndex 指定str中哪个位置开始查找sub
	 * @return String 如果找不到sub则返回null。
	 */
	public static String stringRight(String str, String sub, int fromIndex) {
		if (str == null) return str;
		if (fromIndex > str.length()) return null;
		int end = str.indexOf(sub, fromIndex);
		if (end != -1) { return str.substring(end + sub.length()); }
		return null;
	}

	/**
	 * 在str中从第一个字符开始从左向右查找sub，并返回sub位置右边的字符串
	 * 
	 * @param str
	 * @param sub
	 * @return String 如果找不到sub则返回null。
	 */
	public static String stringRight(String str, String sub) {
		return stringRight(str, sub, 0);
	}

	/**
	 * 在str中从右向左查找sub，并返回sub位置左边的字符串。
	 * 
	 * @param str
	 * @param sub
	 * @param fromIndex fromIndex用于指定str中哪个位置开始查找sub
	 * @return String 如果找不到sub则返回str本身。
	 */
	public static String stringLeftBack(String str, String sub, int fromIndex) {
		if (str == null) return str;
		if (fromIndex > str.length()) return null;
		int end = str.lastIndexOf(sub, fromIndex);
		String result = str;
		if (end != -1) {
			result = str.substring(0, end);
		}
		return result;
	}

	/**
	 * 在str中从最后一个字符开始从右向左查找sub，并返回sub位置左边的字符串。
	 * 
	 * @param str
	 * @param sub
	 * @return String 如果找不到sub则返回str本身。
	 */
	public static String stringLeftBack(String str, String sub) {
		if (str == null) return null;
		return stringLeftBack(str, sub, str.length());
	}

	/**
	 * 在str中从右向左查找sub，并返回sub位置右边的字符串
	 * 
	 * @param str
	 * @param sub
	 * @param fromIndex 指定str中哪个位置开始查找sub
	 * @return String 如果找不到sub则返回null。
	 */
	public static String stringRightBack(String str, String sub, int fromIndex) {
		if (str == null) return str;
		if (fromIndex > str.length()) return null;
		int end = str.lastIndexOf(sub, fromIndex);
		if (end != -1) { return str.substring(end + sub.length()); }
		return null;
	}

	/**
	 * 在str中从最后一个字符开始从右向左查找sub，并返回sub位置右边的字符串
	 * 
	 * @param str
	 * @param sub
	 * @return String 如果找不到sub则返回null。
	 */
	public static String stringRightBack(String str, String sub) {
		if (str == null) return null;
		return stringRightBack(str, sub, str.length());
	}

	/**
	 * 检查str指定的字符串是否为空白串（即不包含字符，只有空格、换行、tab等）。
	 * 
	 * <p>
	 * 如果str为null或长度为0，也返回true。
	 * </p>
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean isBlank(String str) {
		if (str == null || str.length() == 0) return true;
		boolean ret = false;
		ret = str.matches("[\\s]+");
		return ret;
	}

	/**
	 * 判断字符串是否NULL，如果是返回""，否则返回字符串本身。
	 * 
	 * @param str
	 * @return
	 */
	public static String nullToString(String str) {
		return str == null ? "" : str;
	}

	/**
	 * 按条件合并str指定的字符串到raw指定的字符串的结尾并返回合并后的字符串。
	 * 
	 * @param raw String
	 * @param str String
	 * @return String 返回合并后的字符串，如果raw已经以str结尾，则直接返回raw。
	 */
	public static String concatString(String raw, String str) {
		if (raw == null) return null;
		if (str == null || str.length() == 0) return raw;
		if (raw.endsWith(str)) return raw;
		else return raw.concat(str);
	}

	/**
	 * 将raw指定的字符串用delimiter分隔，并返回结果数组。
	 * 
	 * <p>
	 * 如果无法分割，则返回只包含一个元素raw的数组。
	 * </p>
	 * 
	 * @param raw
	 * @param delimiter
	 * @return
	 */
	public static String[] splitString(String raw, char delimiter) {
		if (raw == null) return null;
		int currentIndex = 0;
		int nextSeparator;
		List<String> result = new ArrayList<String>();
		while ((nextSeparator = raw.indexOf(delimiter, currentIndex)) != -1) {
			result.add(raw.substring(currentIndex, nextSeparator));
			currentIndex = nextSeparator + 1;
		}
		result.add(raw.substring(currentIndex));
		String[] arr = new String[result.size()];
		result.toArray(arr);
		result.clear();
		return arr;
	}

	/**
	 * 将raw指定的字符串用delimiter分隔，并返回结果数组。
	 * 
	 * <p>
	 * 如果无法分割，则返回只包含一个元素raw的数组。
	 * </p>
	 * 
	 * @param raw
	 * @param delimiter
	 * @return
	 */
	public static String[] splitString(String raw, String delimiter) {
		if (raw == null) return null;
		if (delimiter == null || delimiter.length() == 0) {
			String[] arr = new String[1];
			arr[0] = raw;
			return arr;
		}
		int currentIndex = 0;
		int nextSeparator;
		List<String> result = new ArrayList<String>();
		int delimiterLen = delimiter.length();
		while ((nextSeparator = raw.indexOf(delimiter, currentIndex)) != -1) {
			result.add(raw.substring(currentIndex, nextSeparator));
			currentIndex = nextSeparator + delimiterLen;
		}
		result.add(raw.substring(currentIndex));
		String[] arr = new String[result.size()];
		result.toArray(arr);
		result.clear();
		return arr;
	}

	/**
	 * 返回指定参数值的结果。
	 * 
	 * @param str 原始字符型参数值。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return String 如果传入参数为null或空字符串，则返回defaultReturn。
	 */
	public static String getValueString(String str, String defaultReturn) {
		if (str == null || str.length() == 0) return defaultReturn;
		return str;
	}

	/**
	 * 返回指定参数值转换为int类型后的结果。
	 * 
	 * @param str 原始字符型参数值。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return int
	 */
	public static int getValueInt(String str, int defaultReturn) {
		if (str == null || str.length() == 0) return defaultReturn;
		if (StringUtil.isNumber(str)) {
			return Integer.parseInt(str);
		} else if (StringUtil.isFloat(str)) {
			double d = getValueDouble(str, 0.0D);
			return (int) d;
		}
		return defaultReturn;
	}

	/**
	 * 返回指定参数值转换为long类型后的结果。
	 * 
	 * @param str 原始字符型参数值。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return long
	 */
	public static long getValueLong(String str, long defaultReturn) {
		if (str == null || str.length() == 0) return defaultReturn;
		if (StringUtil.isNumber(str)) {
			return Long.parseLong(str);
		} else if (StringUtil.isFloat(str)) {
			double d = getValueDouble(str, 0.0D);
			return (long) d;
		}
		return defaultReturn;
	}

	/**
	 * 返回指定参数值转换为float类型后的结果。
	 * 
	 * @param str 原始字符型参数值。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return float
	 */
	public static float getValueFloat(String str, float defaultReturn) {
		if (str == null || str.length() == 0) return defaultReturn;
		if (StringUtil.isFloat(str)) { return Float.parseFloat(str); }
		return defaultReturn;
	}

	/**
	 * 返回指定参数值转换为double类型后的结果。
	 * 
	 * @param str 原始字符型参数值。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return double
	 */
	public static double getValueDouble(String str, double defaultReturn) {
		if (str == null || str.length() == 0) return defaultReturn;
		if (StringUtil.isFloat(str)) { return Double.parseDouble(str); }
		return defaultReturn;
	}

	/**
	 * 返回指定参数值转换为boolean类型后的结果。
	 * 
	 * <p>
	 * 参数值（不区分大小写）配置为“true”、“t”、“yes”、“y”、“1”等情况时返回true，否则返回false。
	 * </p>
	 * 
	 * @param str 原始字符型参数值。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return boolean
	 */
	public static boolean getValueBool(String str, boolean defaultReturn) {
		if (str == null || str.length() == 0) return defaultReturn;
		String tmp = str.trim();
		if (tmp.equalsIgnoreCase("true") || tmp.equalsIgnoreCase("t") || tmp.equalsIgnoreCase("yes") || tmp.equalsIgnoreCase("y") || tmp.equals("1") || tmp.equals("是")) return true;
		else return false;
	}

	/**
	 * 返回指定参数值转换为enumCls定义的枚举中某一个具体枚举值类型之后的结果。
	 * 
	 * @param str 原始字符型参数值。
	 * @param enumCls Class&lt;?&gt; 必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return Enum&lt;?&gt;
	 */
	public static Enum<?> getValueEnumCommon(String str, Class<?> enumCls, Enum<?> defaultReturn) {
		if (str == null || str.length() == 0) return defaultReturn;
		String tmp = str.trim();
		if (enumCls == null || !enumCls.isEnum()) return defaultReturn;
		Enum<?>[] value = (Enum<?>[]) enumCls.getEnumConstants();
		for (Enum<?> e : value) {
			if (e.toString().equalsIgnoreCase(tmp) || e.name().equalsIgnoreCase(tmp)) { return e; }
		}
		return defaultReturn;
	}

	/**
	 * 返回指定参数值转换为enumCls定义的枚举中某一个具体枚举值类型之后的结果。
	 * 
	 * @param &lt;T&gt; 返回的枚举类型。
	 * @param str 原始字符型参数值。
	 * @param enumCls Class&lt;T&gt;必须是某个具体枚举类的类型。
	 * @param defaultReturn 如果没有值或无法转换时返回的默认结果。
	 * @return T 具体枚举结果。
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getValueEnum(String str, Class<T> enumCls, T defaultReturn) {
		if (str == null || str.trim().length() == 0) return defaultReturn;
		String tmp = str.trim();
		if (enumCls != null && enumCls.isEnum()) {
			Enum<?>[] value = (Enum<?>[]) enumCls.getEnumConstants();
			for (Enum<?> e : value) {
				if (e.toString().equalsIgnoreCase(tmp) || e.name().equalsIgnoreCase(tmp)) { return (T) e; }
			}
		}
		return defaultReturn;
	}

	/**
	 * 转换对象。
	 * 
	 * @param &lt;T&gt; 要返回的目标对象名
	 * @param obj Object，源对象
	 * @param clazz Class&lt;T&gt; 要返回的对象类型
	 * @param defaultReturn
	 * @return 如果可以转换，则返回转换结果，否则返回defaultReturn。
	 */
	public static <T> T getValueGeneric(Object source, Class<T> clazz, T defaultReturn) {
		if (source == null) return defaultReturn;
		if (clazz == null) return defaultReturn;
		T result = null;
		try {
			result = clazz.cast(source);
		} catch (ClassCastException ex) {
		}
		return (result == null ? defaultReturn : result);
	}

	static final String HEXES = "0123456789ABCDEF";

	/**
	 * 将byte数组中的值显示为16进制文本。
	 * 
	 * <p>
	 * 参考：{@link StringUtil#fromHexString(String)}
	 * </p>
	 * 
	 * @param raw
	 * @return
	 */
	public static String toHexString(byte[] byteArray) {
		if (byteArray == null) { return null; }
		final StringBuilder hex = new StringBuilder(2 * byteArray.length);
		for (final byte b : byteArray) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
		}
		return hex.toString();
	}

	/**
	 * 将{@link StringUtil#toHexString(byte[])}的转换结果转化为原始字节数组。
	 * 
	 * @param hexString
	 * @return
	 */
	public static byte[] fromHexString(final String hexString) {
		int len = hexString.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character.digit(hexString.charAt(i + 1), 16));
		}
		return data;
	}

	/**
	 * 删除raw指定的文本中所有名称为tagName的html标记。
	 * 
	 * @param raw
	 * @param tagName
	 * @return String，返回剥离后的结果，如果raw或tagName为null或空字符串，则返回raw，如果找不到tagName，也返回raw。
	 */
	public static String stripHtmlTag(String raw, String tagName) {
		if (raw == null || raw.length() == 0 || tagName == null || tagName.length() == 0) return raw;
		Pattern p = Pattern.compile("<" + tagName + "[^>]*>([\\u0001-\\uFFFF]*?)</" + tagName + ">", Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(raw);
		return (m == null ? raw : m.replaceAll(""));
	}

	private static final Pattern PATTERNOFHTMLTAG = Pattern.compile("</?\\w+((\\s+[\\w-]+(\\s*=\\s*(?:\".*?\"|'.*?'|[^'\">\\s]+))?)+\\s*|\\s*)/?>", Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);

	/**
	 * 删除raw指定的文本中的所有html标记。
	 * 
	 * @param raw
	 * @return String 如果raw为null或空串，则直接返回raw。
	 */
	public static String stripHtmlTag(String raw) {
		if (raw == null || raw.length() == 0) return raw;
		return raw.replaceAll(PATTERNOFHTMLTAG.pattern(), "");
	}

}// class end

