/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

/**
 * 表示由字符串类型的关键字和对应的值构成的键-值对象。
 * 
 * @author coca@tansuosoft.cn
 */
public class StringPair {

	/**
	 * 缺省构造器。
	 */
	public StringPair() {
	}

	/**
	 * 接收必要属性的构造器。
	 * 
	 * @param k
	 * @param v
	 */
	public StringPair(String k, String v) {
		this.m_key = k;
		this.m_value = v;
	}

	private String m_key;
	private String m_value;

	/**
	 * 返回关键字。
	 * 
	 * @return 返回关键字（名称）。
	 */
	public String getKey() {
		return this.m_key;
	}

	/**
	 * 设置关键字（名称）。
	 * 
	 * @param key 关键字。
	 */
	public void setKey(String key) {
		this.m_key = key;
	}

	/**
	 * 返回关键字对应值。
	 * 
	 * @return 返回对应值。
	 */
	public String getValue() {
		return this.m_value;
	}

	/**
	 * 设置对应值。
	 * 
	 * @param value 要设置的值。
	 */
	public void setValue(String value) {
		this.m_value = value;
	}

	@Override
	public String toString() {
		return String.format("%1$s|%2$s", this.getKey(), this.getValue());
	}

}

