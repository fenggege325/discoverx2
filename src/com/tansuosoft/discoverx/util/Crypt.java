/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

/**
 * 用于提供加解密功能的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class Crypt {
	private static final String CHARSET = "utf-8";
	/**
	 * 默认加密方式
	 */
	private static final String DEFAULT_TRANSFORMATION = "DES/CBC/PKCS5Padding";
	/**
	 * 默认加密算法
	 */
	private static final String DEFAULT_ALGORITHM = "DES";
	/**
	 * 默认密钥
	 */
	private static byte[] DEFAULT_DES_KEY = { 0x1, 0x1, 0x2, 0x3, 0x5, 0x8, 0x1, 0x3 };
	/**
	 * 默认密钥向量
	 */
	private static byte[] DEFAULT_DES_KEYIV = { (byte) 0xEF, 0x34, (byte) 0xCD, 0x78, (byte) 0x90, (byte) 0xAB, 0x56, 0x12 };

	private Cipher encryptCipher = null;
	private Cipher decryptCipher = null;

	/**
	 * 缺省构造器。
	 */
	public Crypt() {
		this(DEFAULT_DES_KEY, DEFAULT_DES_KEYIV, DEFAULT_ALGORITHM, DEFAULT_TRANSFORMATION);
	}

	/**
	 * 接收8位字符串长度作为密钥种子的构造器。
	 * 
	 * @param key
	 * @throws UnsupportedEncodingException
	 */
	public Crypt(String key) throws UnsupportedEncodingException {
		this(key.getBytes(CHARSET), DEFAULT_DES_KEYIV, DEFAULT_ALGORITHM, DEFAULT_TRANSFORMATION);
	}

	/**
	 * 接收密钥字节数组、密钥向量字节数组、加密算法名称、加密方式的构造器。
	 * 
	 * @param key
	 * @param keyiv
	 * @param algorithm
	 * @param transformation
	 */
	public Crypt(byte[] key, byte[] keyiv, String algorithm, String transformation) {
		try {
			javax.crypto.spec.DESKeySpec dks = new javax.crypto.spec.DESKeySpec(key);
			javax.crypto.SecretKeyFactory keyFactory = javax.crypto.SecretKeyFactory.getInstance(algorithm);
			javax.crypto.SecretKey securekey = keyFactory.generateSecret(dks);
			javax.crypto.spec.IvParameterSpec iv = new javax.crypto.spec.IvParameterSpec(keyiv);

			encryptCipher = Cipher.getInstance(transformation);
			encryptCipher.init(Cipher.ENCRYPT_MODE, securekey, iv);
			decryptCipher = Cipher.getInstance(transformation);
			decryptCipher.init(Cipher.DECRYPT_MODE, securekey, iv);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 加密字节数组。
	 * 
	 * @param arrB 需加密的字节数组
	 * @return 加密后的字节数组
	 */
	public byte[] encrypt(byte[] arrB) throws Exception {
		return encryptCipher.doFinal(arrB);
	}

	/**
	 * 加密字符串并返回加密后字节的base64表示形式。
	 * 
	 * @param strIn 需加密的字符串
	 * @return 加密后的字符串
	 * @throws Exception
	 */
	public String encrypt(String strIn) throws Exception {
		return new String(Base64.encode(encrypt(strIn.getBytes(CHARSET))));
	}

	/**
	 * 加密字符串并返回加密后字节的16进制文本表示形式。
	 * 
	 * @param strIn 需加密的字符串
	 * @return 加密后的字符串
	 * @throws Exception
	 */
	public String encryptToHexString(String strIn) throws Exception {
		return StringUtil.toHexString(encrypt(strIn.getBytes(CHARSET)));
	}

	/**
	 * 解密加密后的字节数组到原始字节数组。
	 * 
	 * @param arrB 需解密的字节数组
	 * @return 解密后的字节数组
	 * @throws Exception
	 */
	public byte[] decrypt(byte[] arrB) throws Exception {
		return decryptCipher.doFinal(arrB);
	}

	/**
	 * 解密base64字符串格式的加密串到原始字符串。
	 * 
	 * @param strIn 需解密的字符串
	 * @return 解密后的字符串
	 * @throws Exception
	 */
	public String decrypt(String strIn) throws Exception {
		return new String(this.decrypt(Base64.decode(strIn.toCharArray())), CHARSET);
	}

	/**
	 * 解密16进制文本表示形式的加密字符串到原始字符串。
	 * 
	 * @param strIn 需解密的字符串
	 * @return 解密后的字符串
	 * @throws Exception
	 */
	public String decryptFromHexString(String strIn) throws Exception {
		return new String(this.decrypt(StringUtil.fromHexString(strIn)), CHARSET);
	}

	public static void main(String[] args) {
		try {
			System.out.println(KeyStore.getDefaultType());
			System.out.println("-------------------------------------");

			// 1.java生成key命令：
			// keytool -genkey -dname "cn=test, ou=tensosoft, o=tensosoft, c=ZH" -alias myalias -keypass 12345678 -keystore my.keystore
			// -storepass 12345678 -validity 1800 -keyalg RSA

			// 2.java导出keystore为pkcs#12格式文件命令：
			// keytool -importkeystore -srckeystore my.keystore -destkeystore dest.p12 -srcstoretype JKS -deststoretype PKCS12

			// 3.java导出keystore中别名对应的公钥命令：
			// keytool -export -alias myalias -file cert.cer -keystore my.keystore
			KeyStore keyStore = KeyStore.getInstance("pkcs12");
			// pkcs12格式
			keyStore.load(new FileInputStream("C:\\Users\\coca\\dest.p12"), "12345678".toCharArray());
			Key key = keyStore.getKey("myalias", "12345678".toCharArray());
			PrivateKey privKey = null;
			if (key instanceof PrivateKey) {
				// Certificate cert = keyStore.getCertificate("myalias");
				// PublicKey pubKey = cert.getPublicKey();
				privKey = (PrivateKey) key;
			}
			Cipher rsac = Cipher.getInstance("RSA"); // java与.net默认为“RSA/ECB/PKCS1Padding”，因此“RSA/ECB/PKCS1Padding”与“RSA”相同。
			rsac.init(Cipher.DECRYPT_MODE, privKey);
			byte[] dec = rsac.doFinal(StringUtil.fromHexString("344F78D31F986A78AF0FE94300D314423701497B49DAE2E2A568B9EF492DA88109DC251B7C0DFFB689006F848D10F760B8BA7A30F27BD42BF3E6515C75F015216F5140B9C1845D21D143F16B1A82DB539F57F5D8D408AB4674573CECBF5070C947C3A4B9012C4F92148EA6FD6941DFD90D30769A2E1F368372150BB78651704A"));
			System.out.println("dec from c#=" + new String(dec, "utf-8"));

			// jks格式
			KeyStore keyStoreD = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStoreD.load(new FileInputStream("C:\\Users\\coca\\my.keystore"), "12345678".toCharArray());
			key = keyStoreD.getKey("myalias", "12345678".toCharArray());
			privKey = null;
			if (key instanceof PrivateKey) {
				// Certificate cert = keyStore.getCertificate("myalias");
				// PublicKey pubKey = cert.getPublicKey();
				privKey = (PrivateKey) key;
			}
			rsac = Cipher.getInstance("RSA"); // java与.net默认为“RSA/ECB/PKCS1Padding”，因此“RSA/ECB/PKCS1Padding”与“RSA”相同。
			rsac.init(Cipher.DECRYPT_MODE, privKey);
			dec = rsac.doFinal(StringUtil.fromHexString("344F78D31F986A78AF0FE94300D314423701497B49DAE2E2A568B9EF492DA88109DC251B7C0DFFB689006F848D10F760B8BA7A30F27BD42BF3E6515C75F015216F5140B9C1845D21D143F16B1A82DB539F57F5D8D408AB4674573CECBF5070C947C3A4B9012C4F92148EA6FD6941DFD90D30769A2E1F368372150BB78651704A"));
			System.out.println("dec from c#=" + new String(dec, "utf-8"));

			System.out.println("-------------------------------------");

			String test = "测试";
			byte bs[] = test.getBytes("utf-8");
			System.out.println(StringUtil.toHexString(bs));
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(512);
			KeyPair kp = kpg.genKeyPair();
			Key publicKey = kp.getPublic();
			Key privateKey = kp.getPrivate();
			KeyFactory fact = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec pub = fact.getKeySpec(publicKey, RSAPublicKeySpec.class);
			RSAPrivateKeySpec priv = fact.getKeySpec(privateKey, RSAPrivateKeySpec.class);
			byte privModulus[] = priv.getModulus().toByteArray();
			byte privExponent[] = priv.getPrivateExponent().toByteArray();

			KeyFactory keyFact = KeyFactory.getInstance("RSA");

			System.out.print(privateKey.getFormat() + "=");
			PKCS8EncodedKeySpec pkcs8 = new PKCS8EncodedKeySpec(privateKey.getEncoded());
			System.out.println(StringUtil.toHexString(pkcs8.getEncoded()));
			PrivateKey priKey = keyFact.generatePrivate(pkcs8);
			if (!priKey.equals(privateKey)) System.out.println("error!");

			System.out.print(publicKey.getFormat() + "=");
			X509EncodedKeySpec x509 = new X509EncodedKeySpec(publicKey.getEncoded());
			System.out.println(StringUtil.toHexString(x509.getEncoded()));
			PublicKey pubKey = keyFact.generatePublic(x509);
			if (!pubKey.equals(publicKey)) System.out.println("error!");

			System.out.println("privModulus=" + StringUtil.toHexString(privModulus));
			System.out.println("privExponent=" + StringUtil.toHexString(privExponent));

			byte pubModulus[] = pub.getModulus().toByteArray();
			byte pubExponent[] = pub.getPublicExponent().toByteArray();

			System.out.println("pubModulus=" + StringUtil.toHexString(pubModulus));
			System.out.println("pubExponent=" + StringUtil.toHexString(pubExponent));

			Cipher rsa = Cipher.getInstance("RSA");
			rsa.init(Cipher.ENCRYPT_MODE, publicKey);
			byte[] encrypted = rsa.doFinal(bs);
			System.out.println("rawtext =" + test);
			System.out.println("encypted=" + StringUtil.toHexString(encrypted));

			Cipher rsa1 = Cipher.getInstance("RSA");
			// BigInteger privModulus = null, privExponen = null;
			// RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(privModulus, privExponen);
			// KeyFactory fact = KeyFactory.getInstance("RSA");
			// PrivateKey privKey = fact.generatePrivate(keySpec);
			rsa1.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] decrypted = rsa1.doFinal(encrypted);

			System.out.println("decypted=" + new String(decrypted, "utf-8"));
		} catch (Exception ex) {
		}
	}

}

