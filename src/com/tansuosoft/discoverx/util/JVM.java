/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Java虚拟机相关信息。
 * 
 * @author coca@tansuosoft.cn
 */
public class JVM {
	private Map<String, Class<? extends Object>> loaderCache = new HashMap<String, Class<? extends Object>>();
	private final boolean supportsAWT = loadClass("java.awt.Color") != null;
	private final boolean supportsSQL = loadClass("java.sql.Date") != null;
	private static final float majorJavaVersion = getMajorJavaVersion();
	static final float DEFAULT_JAVA_VERSION = 1.5f;

	/**
	 * 获取java虚拟机版本号。
	 * 
	 * @return float
	 */
	public static final float getMajorJavaVersion() {
		try {
			return Float.parseFloat(System.getProperty("java.specification.version"));
		} catch (NumberFormatException e) {
			return DEFAULT_JAVA_VERSION;
		}
	}

	/**
	 * 返回java版本是否大于等于1.4
	 * 
	 * @return boolean
	 */
	public static boolean is14() {
		return majorJavaVersion >= 1.4f;
	}

	/**
	 * 返回java版本是否大于等于1.5
	 * 
	 * @return boolean
	 */
	public static boolean is15() {
		return majorJavaVersion >= 1.5f;
	}

	/**
	 * 返回java版本是否大于等于1.6
	 * 
	 * @return boolean
	 */
	public static boolean is16() {
		return majorJavaVersion >= 1.6f;
	}

	/**
	 * 获取操作系统名称。
	 * 
	 * @return String
	 */
	public static String getOSName() {
		return System.getProperty("os.name");
	}

	/**
	 * 获取操作系统版本。
	 * 
	 * @return String 返回格式：主版本.辅版本
	 */
	public static String getOSVersion() {
		return System.getProperty("os.version");
	}

	/**
	 * 是否运行于Windows类型操作系统。
	 * 
	 * @return
	 */
	public static boolean isHostWindows() {
		String osName = getOSName();
		if (osName != null && osName.toLowerCase().indexOf("windows") >= 0) return true;
		return (File.pathSeparatorChar == ';');
	}

	/**
	 * 是否运行于UNIX、Linux类型操作系统。
	 * 
	 * @return
	 */
	public static boolean isHostUNIX() {
		return (File.pathSeparatorChar != ';' && File.separatorChar == '/');
	}

	/**
	 * 返回java虚拟机提供者是否为Sun。
	 * 
	 * @return bollean
	 */
	public static boolean isVendeorSun() {
		return System.getProperty("java.vm.vendor").indexOf("Sun") != -1;
	}

	/**
	 * 返回java虚拟机提供者是否为Apple。
	 * 
	 * @return boolean
	 */
	public static boolean isVendeorApple() {
		return System.getProperty("java.vm.vendor").indexOf("Apple") != -1;
	}

	/**
	 * 返回java虚拟机提供者是否为HP。
	 * 
	 * @return boolean
	 */
	public static boolean isVendeorHPUX() {
		return System.getProperty("java.vm.vendor").indexOf("Hewlett-Packard Company") != -1;
	}

	/**
	 * 返回java虚拟机提供者是否为IBM。
	 * 
	 * @return boolean
	 */
	public static boolean isVendeorIBM() {
		return System.getProperty("java.vm.vendor").indexOf("IBM") != -1;
	}

	/**
	 * 从全限定类名称加载类。
	 * 
	 * @param name
	 * @return Class<? extends Object>
	 */
	public Class<? extends Object> loadClass(String name) {
		try {
			Class<? extends Object> clazz = loaderCache.get(name);
			if (clazz == null) {
				clazz = Class.forName(name, false, getClass().getClassLoader());
				loaderCache.put(name, clazz);
			}
			return clazz;
		} catch (ClassNotFoundException e) {
			return null;
		}
	}

	/**
	 * 返回是否支持AWT
	 * 
	 * @return boolean
	 */
	public boolean supportsAWT() {
		return this.supportsAWT;
	}

	/**
	 * 返回是否支持JDBC
	 * 
	 * @return boolean
	 */
	public boolean supportsSQL() {
		return this.supportsSQL;
	}

	/**
	 * 获取系统硬件支持的数据存储字节顺序类型。
	 * 
	 * <p>
	 * 此方法返回的是系统CPU硬件支持的数据存储字节顺序类型，Java内部各种数据总是以{@link com.tansuosoft.discoverx.util.JVM.ByteOrder#BIG_ENDIAN}的方式存储。
	 * </p>
	 * 
	 * @return ByteOrder
	 */
	public static ByteOrder getHardwareByteOrder() {
		if (java.nio.ByteOrder.nativeOrder().toString().equals(java.nio.ByteOrder.BIG_ENDIAN.toString())) {
			return ByteOrder.BIG_ENDIAN;
		} else {
			return ByteOrder.LITTLE_ENDIAN;
		}
	}

	/**
	 * 数据存储字节顺序类型枚举。
	 * 
	 * @author coca@tensosoft.com
	 */
	public enum ByteOrder {
		/**
		 * 表示高位字节在前的数据存储字节顺序模式。
		 */
		BIG_ENDIAN,
		/**
		 * 表示低位字节在前的数据存储字节顺序模式。
		 */
		LITTLE_ENDIAN;
	}
}

