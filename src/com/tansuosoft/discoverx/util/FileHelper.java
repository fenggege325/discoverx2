/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.CRC32;

import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 对文件及文件夹的操作帮助类
 * 
 * @author coca@tansuosoft.cn
 */
public class FileHelper {
	/**
	 * 获取文件扩展名。
	 * 
	 * <p>
	 * 通过文件最后的半角句号查找扩展名。
	 * </p>
	 * 
	 * @param fn String 传入的文件名。
	 * @param returnIfNoExt String
	 * @return String 返回扩展名（包括点号：“.”）,如果没有扩展名或者fn为null或空串，则返回returnIfNoExt
	 * 
	 */
	public static String getExtName(String fn, String returnIfNoExt) {
		if (fn == null || fn.trim().length() == 0) return returnIfNoExt;
		String ext = null;
		int pos = fn.lastIndexOf('.');
		if (pos >= 0) ext = fn.substring(pos);
		return ext == null ? returnIfNoExt : ext;
	}

	/**
	 * 删除文件夹及其包含的所有文件。
	 * 
	 * <p>
	 * 此方法会删除文件和文件夹，请谨慎使用！
	 * </p>
	 * 
	 * @param folder
	 */
	public static void deleteFolder(String folder) {
		if (folder == null || folder.length() == 0) return;
		try {
			deleteAllFiles(folder); // 删除完里面所有内容
			File d = new File(folder);
			if (!d.delete()) d.deleteOnExit(); // 删除空文件夹
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除文件夹下所有文件，包括子文件夹及其文件。
	 * 
	 * @param folder
	 */
	public static void deleteAllFiles(String folder) {
		if (folder == null || folder.length() == 0) return;
		File file = new File(folder);
		if (!file.exists()) return;
		if (!file.isDirectory()) return;
		String[] tempList = file.list();
		File temp = null;
		String dir = folder.replace('\\', File.separatorChar).replace('/', File.separatorChar);
		for (int i = 0; i < tempList.length; i++) {
			temp = new File(dir + (dir.endsWith(File.separator) ? "" : File.separator) + tempList[i]);
			if (temp.isFile()) {
				temp.delete();
			} else if (temp.isDirectory()) {
				deleteFolder(folder + File.separator + tempList[i]);
			}
		}
	}

	/**
	 * 拷贝源文件到目标文件。
	 * 
	 * @param src 源文件，必须。
	 * @param dst 目标文件，必须。
	 * @return boolean 拷贝成功则返回true，否则返回false。
	 */
	public static boolean copyFile(File src, File dst) {
		if (src == null || !src.exists() || !src.isFile()) return false;
		if (dst == null) return false;
		FileChannel source = null;
		FileChannel destination = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			File p = dst.getParentFile();
			p.mkdirs();
			if (!dst.exists()) dst.createNewFile();
			fis = new FileInputStream(src);
			source = fis.getChannel();
			fos = new FileOutputStream(dst);
			destination = fos.getChannel();
			destination.transferFrom(source, 0, source.size());
			source.close();
			return true;
		} catch (IOException ioe) {
			FileLogger.error(ioe);
		} finally {
			try {
				if (source != null) source.close();
				if (destination != null) destination.close();
				if (fis != null) fis.close();
				if (fos != null) fos.close();
			} catch (IOException ioe) {
				FileLogger.error(ioe);
			}
		}
		return false;
	}

	private static final int SIZE = 8192;

	/**
	 * 获取fn指定的文件内容的哈希结果。
	 * 
	 * 
	 * <p>
	 * 与{@link #getHashBufferedBytes(String, String)}效果一致，可择优使用。
	 * </p>
	 * 
	 * @param fn
	 * @param algorithm 此参数指定返回的哈希类型，如“md5”、“sha1”、“sha-256”等。
	 * @return String
	 */
	public static String getHashMappedBytes(String fn, String algorithm) {
		MessageDigest alg = null;
		FileInputStream f = null;
		try {
			alg = MessageDigest.getInstance(algorithm);
			f = new FileInputStream(fn);
			FileChannel ch = f.getChannel();
			MappedByteBuffer mb = ch.map(MapMode.READ_ONLY, 0L, ch.size());
			byte[] barray = new byte[SIZE];

			int nGet = 0;
			while (mb.hasRemaining()) {
				nGet = Math.min(mb.remaining(), SIZE);
				mb.get(barray, 0, nGet);
				alg.update(barray, 0, nGet);
			}
			BigInteger bi = new BigInteger(1, alg.digest());
			return bi.toString(16).toUpperCase();
		} catch (IOException ex) {
			FileLogger.error(ex);
			return null;
		} catch (NoSuchAlgorithmException ex) {
			FileLogger.error(ex);
			return null;
		} finally {
			try {
				if (f != null) f.close();
			} catch (IOException e) {
				FileLogger.error(e);
			}
		}
	}

	/**
	 * 获取fn指定的文件内容的哈希结果。
	 * 
	 * <p>
	 * 与{@link #getHashMappedBytes(String, String)}效果一致，可择优使用。
	 * </p>
	 * 
	 * @param fn
	 * @param algorithm 此参数指定返回的哈希类型，如“md5”、“sha1”、“sha-256”等。
	 * @return String
	 */
	public static String getHashBufferedBytes(String fn, String algorithm) {
		MessageDigest alg = null;
		InputStream f = null;
		try {
			alg = MessageDigest.getInstance(algorithm);
			f = new BufferedInputStream(new FileInputStream(fn));
			byte[] buffer = new byte[SIZE];
			int sizeRead = -1;
			while ((sizeRead = f.read(buffer)) != -1) {
				alg.update(buffer, 0, sizeRead);
			}

			BigInteger bi = new BigInteger(1, alg.digest());
			return bi.toString(16).toUpperCase();
		} catch (IOException ex) {
			FileLogger.error(ex);
			return null;
		} catch (NoSuchAlgorithmException ex) {
			FileLogger.error(ex);
			return null;
		} finally {
			try {
				if (f != null) f.close();
			} catch (IOException e) {
				FileLogger.error(e);
			}
		}
	}

	/**
	 * 获取fn指定的文件内容的CRC32值。
	 * 
	 * @param fn
	 * @return long
	 */
	public static long getCRC32(String fn) {
		CRC32 alg = null;
		FileInputStream f = null;
		try {
			alg = new CRC32();
			f = new FileInputStream(fn);
			FileChannel ch = f.getChannel();
			MappedByteBuffer mb = ch.map(MapMode.READ_ONLY, 0L, ch.size());
			byte[] barray = new byte[SIZE];

			int nGet = 0;
			while (mb.hasRemaining()) {
				nGet = Math.min(mb.remaining(), SIZE);
				mb.get(barray, 0, nGet);
				alg.update(barray, 0, nGet);
			}
			return alg.getValue();
		} catch (IOException ex) {
			FileLogger.error(ex);
			return 0L;
		} finally {
			try {
				if (f != null) f.close();
			} catch (IOException e) {
				FileLogger.error(e);
			}
		}
	}

	// public static void main(String args[]) {
	// String fn = "e:\\temp\\jquery-1.8.2.js";// "e:\\temp\\javadoc.log";// "E:\\temp\\vs\\fallback\\tsim-97dc4970\\tsim.sdf";
	// String algs[] = { "md5", "sha1", "sha-256" };
	// long s = 0L;
	// long e = 0L;
	// String r = null;
	// long crc = 0L;
	//
	// for (String alg : algs) {
	// s = System.nanoTime();
	// r = getHashBuffered(fn, alg);
	// e = System.nanoTime();
	// System.out.println("getHashBuffered:" + alg + " result=" + r + ", calc time=" + (e - s));
	// }
	// System.out.println("---------------------------------");
	// for (String alg : algs) {
	// s = System.nanoTime();
	// r = getHash(fn, alg);
	// e = System.nanoTime();
	// System.out.println("getHashMapBytes:" + alg + " result=" + r + ", calc time=" + (e - s));
	// }
	//
	// System.out.println("---------------------------------");
	// s = System.nanoTime();
	// crc = getCRC32(fn);
	// e = System.nanoTime();
	// System.out.println("CRC32 result=" + BigInteger.valueOf(crc).toString(16) + ", calc time=" + (e - s));
	// }
}

