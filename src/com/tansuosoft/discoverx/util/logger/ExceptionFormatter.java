/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.logger;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import com.tansuosoft.discoverx.util.DateTime;

/**
 * 输出异常信息的格式化类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ExceptionFormatter extends Formatter {

	/**
	 * 格式化错误日志。
	 * 
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 */
	@Override
	public String format(LogRecord record) {
		StringBuilder sb = new StringBuilder();
		DateTime dt = new DateTime();
		sb.append(dt.toString()).append(" ").append(record.getSourceClassName());
		String method = record.getSourceMethodName();
		boolean methodValid = (method != null && method.length() > 0);
		if (methodValid) sb.append(".").append(method);
		sb.append("(TID:").append(Thread.currentThread().getId()).append(")").append(":\r\n").append(record.getMessage());
		sb.append("--------------------------------------------------------------------------------------------------------------------------------");
		sb.append("\r\n");
		return sb.toString();
	}

}

