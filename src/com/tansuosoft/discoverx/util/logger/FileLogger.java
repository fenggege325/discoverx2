/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.logger;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import com.tansuosoft.discoverx.util.DateTime;
import com.tansuosoft.discoverx.util.InfoLogLevelException;
import com.tansuosoft.discoverx.util.ThrowableUtil;

/**
 * 用于记录各类日志信息的类。
 * 
 * @author coca@tansuosoft.cn
 */
public final class FileLogger {
	/**
	 * 日志文件最大字节数。
	 */
	protected static final int DEFAULT_LIMIT = 1048576;
	/**
	 * 允许的日志文件总个数。
	 */
	protected static final int DEFAULT_COUNT = 10;

	/**
	 * 日志文件所在完整路径。
	 */
	protected static String logFileRoot = "";
	/**
	 * 调试日志记录器。
	 */
	protected static Handler debugHandler = null;
	/**
	 * 错误日志记录器。
	 */
	protected static Handler errorHandler = null;
	/**
	 * 日志是否初始化标记。
	 */
	protected static boolean handlerInitialized = false;
	static {
		init();
	}
	/**
	 * 缓存写入文件的阀值计数器。
	 */
	protected static int appendCount = 0;

	/**
	 * 初始化记录日志的{@link java.util.logging.FileHandler}对象。
	 * 
	 * @return
	 */
	protected static boolean init() {
		if (!handlerInitialized) {
			try {
				logFileRoot = com.tansuosoft.discoverx.util.Path.WEBAPP_ROOT;
				logFileRoot = logFileRoot.concat("logs" + java.io.File.separator);
				debugHandler = new FileHandler(logFileRoot + "debug" + "%g.log", DEFAULT_LIMIT, DEFAULT_COUNT, true);
				errorHandler = new FileHandler(logFileRoot + "error" + "%g.log", DEFAULT_LIMIT, DEFAULT_COUNT, true);
				debugHandler.setFormatter(new DebugFormatter());
				errorHandler.setFormatter(new ExceptionFormatter());
				handlerInitialized = true;
			} catch (SecurityException e) {
				System.err.println(DateTime.getNowDTString() + " 严重：无法初始化文件日志记录器，可能是启动应用服务器的用户没有相关读写权限。系统日志将无法保存！");
			} catch (IOException e) {
				System.err.println(DateTime.getNowDTString() + " 严重：无法初始化文件日志记录器，可能是日志文件保存路径“" + logFileRoot + "error" + "”不存在或没有权限。系统日志将无法保存！");
			} catch (Exception ex) {
				System.err.println(DateTime.getNowDTString() + " 严重：无法初始化日志文件保存路径，请联系管理员检查“web.xml”中的安装路径信息是否配置正确！");
			}
		}
		return handlerInitialized;
	}

	/**
	 * 追加日志信息。
	 * 
	 * @param cls Class-表示引发记录日志/异常信息的对象对应的类，如果为null，则尝试从当前运行堆栈中获取。
	 * @param method String-表示引发记录日志/异常信息的对象的方法名，如果为null，则尝试从当前运行堆栈中获取。
	 * @param level int-日志级别，介于900-1000之间的值表示异常或错误日志，其它值表示普通日志。参考：{@link java.util.logging.Level}。
	 * @param msg String-表示消息字符串，格式请参考：{@link java.lang.String#format(String, Object...)}。
	 * @param arguments 可选参数，用于为msg对应的消息字符串中的替换符提供替换值。可参考：{@link java.lang.String#format(String, Object...)}。
	 */
	public static void log(Class<? extends Object> cls, String method, int level, String msg, Object... arguments) {
		if (!handlerInitialized) {
			Console.log(cls, method, msg, arguments);
			return;
		}
		boolean isException = (level >= Level.WARNING.intValue() && level <= Level.SEVERE.intValue());
		LogRecord lr = null;
		Handler handler = null;

		String loggerName = (cls == null ? null : cls.getName());
		String sourceClsName = (cls == null ? null : cls.getName());
		String methodName = method;

		if (cls == null) {
			StackTraceElement stack[] = new Throwable().getStackTrace();
			StackTraceElement el = null;
			for (int i = 0; i < stack.length; i++) {
				el = stack[i];
				if (!el.getClassName().equalsIgnoreCase(FileLogger.class.getName())) break;
			}
			if (el != null) {
				if (methodName == null) methodName = el.getMethodName();
				if (sourceClsName == null) sourceClsName = el.getClassName();
				if (loggerName == null) loggerName = el.getMethodName();
			}
		}

		if (loggerName == null) loggerName = (isException ? "错误" : "日志");
		Logger logger = Logger.getLogger(loggerName);
		Level l = Level.parse(String.valueOf(level));
		logger.setLevel(l);

		handler = (isException ? errorHandler : debugHandler);

		// try {
		// handler = new FileHandler(logFileRoot + (isException ? "error" : "debug") + "%g.log", DEFAULT_LIMIT, DEFAULT_COUNT, true);
		// } catch (SecurityException e) {
		// System.err.println(DateTime.getNowDTString() + " 严重：无法初始化文件日志记录器，可能是启动应用服务器的用户没有相关读写权限。系统日志将无法保存！");
		// } catch (IOException e) {
		// System.err.println(DateTime.getNowDTString() + " 严重：无法初始化文件日志记录器，可能是日志文件保存路径“" + logFileRoot + "error" + "”不存在或没有权限。系统日志将无法保存！");
		// }

		logger.addHandler(handler);

		lr = new LogRecord(l, String.format(msg, arguments));
		lr.setSourceClassName(sourceClsName);
		lr.setSourceMethodName(methodName);
		logger.log(lr);

		if (appendCount > 10) {
			appendCount = 0;
			handler.flush();
		} else {
			appendCount++;
		}

		// handler.close();
	}

	/**
	 * 记录消息日志。
	 * 
	 * <p>
	 * 关联参数说明请参考：{@link #log(Class, String, int, String, Object...)}。
	 * </p>
	 * 
	 * @param cls
	 * @param method
	 * @param msg
	 * @param arguments
	 */
	public static void debug(Class<? extends Object> cls, String method, String msg, Object... arguments) {
		FileLogger.log(cls, method, Level.INFO.intValue(), msg, arguments);
	}

	/**
	 * 记录调试日志。
	 * 
	 * <p>
	 * 等同于调用{@link #debug(Class, String, String, Object...)}，其中前两个参数为null。
	 * </p>
	 * 
	 * @param msg
	 * @param arguments
	 */
	public static void debug(String msg, Object... arguments) {
		FileLogger.log(null, null, Level.INFO.intValue(), msg, arguments);
	}

	/**
	 * 记录调试日志。
	 * 
	 * <p>
	 * 关联参数说明请参考：{@link #log(Class, String, int, String, Object...)}。
	 * </p>
	 * 
	 * @param cls
	 * @param method
	 * @param ex {@link java.lang.Throwable}，表示要记录的异常对象。
	 * @return Throwable 直接返回传入的ex。
	 */
	public static Throwable error(Class<? extends Object> cls, String method, Throwable ex) {
		StackTraceElement[] stels = ex.getStackTrace();
		String msg = "抛出了“%s”异常：%s\r\n堆栈：\r\n%s";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < stels.length; i++) {
			sb.append(stels[i].getClassName()).append(".").append(stels[i].getMethodName()).append("(").append(stels[i].getLineNumber()).append(").\r\n");
		}
		FileLogger.log(cls, method, Level.SEVERE.intValue(), msg, ex.getClass().getName(), ex.getLocalizedMessage(), sb.toString());
		return ex;
	}

	/**
	 * 记录异常日志。
	 * 
	 * <p>
	 * 等同于调用{@link #error(Class, String, Throwable)}其中前两个参数为null。
	 * </p>
	 * 
	 * @param ex Throwable，表示要记录的异常/错误对象。
	 * @return Throwable 直接返回ex。
	 */
	public static Throwable error(Exception ex) {
		if (ex != null && (ex instanceof InfoLogLevelException)) {
			Throwable t = ThrowableUtil.getRootThrowable(ex);
			FileLogger.debug("%1$s", (t == null ? ex.getMessage() : t.getMessage()));
			return ex;
		}
		return FileLogger.error(null, null, ex);
	}

	/**
	 * 关闭日志。
	 * 
	 * <p>
	 * 由系统调用。
	 * </p>
	 */
	public static void close() {
		try {
			Console.debug("%s.close", FileLogger.class.getName());
			if (handlerInitialized) {
				handlerInitialized = false;
				debugHandler.close();
				errorHandler.close();
			}
		} catch (Exception e) {
			System.err.println("日志关闭错误：" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * 强行将缓存里的日志写入文件。
	 */
	public static void flush() {
		close();
		init();
	}

	/**
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		close();
	}

	/**
	 * 私有构造器
	 */
	private FileLogger() {
	}
}

