/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.logger;

import com.tansuosoft.discoverx.util.DateTime;

/**
 * 输出日志信息到控制台的类。
 * 
 * @author coca@tensosoft.com
 */
public final class Console {

	/**
	 * 输出控制台日志信息。
	 * 
	 * @param cls Class-表示引发记录日志/异常信息的对象对应的类，如果为null，则尝试从当前运行堆栈中获取。
	 * @param method String-表示引发记录日志/异常信息的对象的方法名，如果为null，则尝试从当前运行堆栈中获取。
	 * @param msg String-表示消息字符串，格式请参考：{@link java.lang.String#format(String, Object...)}。
	 * @param arguments 可选参数，用于为msg对应的消息字符串中的替换符提供替换值。可参考：{@link java.lang.String#format(String, Object...)}。
	 */
	public static void log(Class<? extends Object> cls, String method, String msg, Object... arguments) {
		StringBuilder sb = new StringBuilder();
		String sourceClsName = (cls == null ? null : cls.getName());
		String methodName = method;

		if (cls == null) {
			StackTraceElement stack[] = new Throwable().getStackTrace();
			StackTraceElement el = null;
			for (int i = 0; i < stack.length; i++) {
				el = stack[i];
				if (!el.getClassName().equalsIgnoreCase(Console.class.getName()) && !el.getClassName().equalsIgnoreCase(FileLogger.class.getName())) break;
			}
			if (el != null) {
				if (methodName == null) methodName = el.getMethodName();
				if (sourceClsName == null) sourceClsName = el.getClassName();
			}
		}
		sb.append(DateTime.getNowDTString()).append(" ").append(sourceClsName);
		boolean methodValid = (methodName != null && methodName.length() > 0);
		if (methodValid) sb.append(".").append(methodName);
		String msgResult = String.format(msg, arguments);
		sb.append("(TID:").append(Thread.currentThread().getId()).append(")").append(": ").append(msgResult);
		System.out.println(sb.toString());
	}

	/**
	 * 输出控制台调试日志。
	 * 
	 * @see #log(Class, String, String, Object...)
	 * @param msg
	 * @param arguments
	 */
	public static void debug(String msg, Object... arguments) {
		log(null, null, msg, arguments);
	}

	/**
	 * 输出控制台错误日志
	 * 
	 * @see #log(Class, String, String, Object...)
	 * @param e {@link java.lang.Throwable}，表示要记录的异常对象。
	 */
	public static void error(Throwable e) {
		if (e == null) return;
		StackTraceElement[] stels = e.getStackTrace();
		String msg = "抛出了“%s”异常：%s\r\n堆栈：\r\n%s";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < stels.length; i++) {
			sb.append(stels[i].getClassName()).append(".").append(stels[i].getMethodName()).append("(").append(stels[i].getLineNumber()).append(").\r\n");
		}
		log(null, null, msg, e.getClass().getName(), e.getLocalizedMessage(), sb.toString());
	}
}

