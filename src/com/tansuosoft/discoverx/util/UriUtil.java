/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 处理uri/url等的实用工具类。
 * 
 * @author coca@tensosoft.com
 */
public final class UriUtil {
	/**
	 * 缺省构造器。
	 */
	private UriUtil() {
	}

	/**
	 * javascript的“encodeURIComponent”函数编码时默认使用的字符集：“UTF-8”
	 */
	protected static final String DEFAULT_ENCODING = "UTF-8";

	/**
	 * 使用UTF-8作为编码格式将指定字符串编码为符合http请求参数编码格式的结果。
	 * 
	 * 此方法与javascript的“encodeURIComponent”返回一致。
	 * 
	 * @param s
	 * @return String
	 */
	public static String encodeURIComponent(String s) {
		return encodeURIComponent(s, DEFAULT_ENCODING);
	}

	/**
	 * 使用“encoding”值作为编码格式将指定字符串编码为http请求参数格式的结果。
	 * 
	 * @param s
	 * @param encoding
	 * @return String
	 */
	public static String encodeURIComponent(String s, String encoding) {
		if (s == null || s.length() == 0) return s;
		try {
			s = URLEncoder.encode(s, encoding);
		} catch (Exception e) {
			FileLogger.error(e);
		}
		s = s.replaceAll("\\+", "%20");
		// s = s.replaceAll("%2B", "+");
		return s;
	}

	/**
	 * 将{@link UriUtil#encodeURIComponent(String)}返回的结果重新解码为原始字符串。
	 * 
	 * @param s
	 * @return String
	 */
	public static String decodeURIComponent(String s) {
		return decodeURIComponent(s, DEFAULT_ENCODING);
	}

	protected static final Pattern p = Pattern.compile("%u[0-9a-fA-F]{4}");

	/**
	 * 将{@link UriUtil#encodeURIComponent(String, String)}返回的结果重新解码为原始字符串。
	 * 
	 * @param s
	 * @param encoding
	 * @return String
	 */
	public static String decodeURIComponent(String s, String encoding) {
		if (s == null || s.length() == 0) return s;
		// s = s.replaceAll("%u[0-9a-fA-F]{4}", "");
		Matcher m = p.matcher(s);
		Map<String, String> replaceMap = new HashMap<String, String>();
		while (m.find()) {
			replaceMap.put(m.group(), Escape.unescape(m.group()));
		}
		for (String g : replaceMap.keySet()) {
			s = s.replace(g, replaceMap.get(g));
		}
		// s = s.replaceAll("\\+", "%2B");
		s = s.replaceAll("%20", "+");
		try {
			s = URLDecoder.decode(s, encoding);
		} catch (Exception e) {
			FileLogger.error(e);
		}
		return s;
	}

}

