/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.lang.reflect.Method;
import java.util.Comparator;

/**
 * 用于比较两个有返回值的java.lang.reflect.Method对象实例的比较类。
 * 
 * <p>
 * 一般用于排序输出java bean对象的getter方法时使用。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class PropertyMethodComparator implements Comparator<Method> {

	protected static final String[] PRIMITIVE_TYPES = { "boolean", "char", "byte", "short", "int", "long", "float", "double", "String", "EnumBase", "Object" };
	protected static final String[] PRIMITIVE_OBJECTTYPES = { "java.lang.Boolean", "java.lang.Character", "java.lang.Byte", "java.lang.Short", "java.lang.Integer", "java.lang.Long", "java.lang.Float", "java.lang.Double", "java.lang.String", "com.tansuosoft.discoverx.util.EnumBase", "java.lang.Object" };
	protected static final String[] PRIMITIVE_TYPE_VALUES = { "001", "002", "003", "004", "005", "006", "007", "008", "009", "010", "011" };

	/**
	 * 比较两个Method的大小。
	 * <p>
	 * 比较规则为：
	 * <ul>
	 * <li>先比较方法返回值类型，方法返回值为对象类型的大于基础类型（字符串、枚举、java内置类型）；</li>
	 * <li>如果返回类型都为基础类型（字符串、枚举、java内置类型）或对象类型，则比较方法名称。</li>
	 * </ul>
	 * </p>
	 */
	public int compare(Method o1, Method o2) {
		if (o1 == null && o2 == null) return 0;
		if (o1 == null && o2 != null) return -1;
		if (o1 != null && o2 == null) return 1;
		Class<?> cls1 = o1.getReturnType();
		Class<?> cls2 = o2.getReturnType();
		String clsName1 = cls1.getName();
		String clsName2 = cls2.getName();

		String methodName1 = o1.getName();
		String methodName2 = o2.getName();

		int cnt = PRIMITIVE_TYPES.length;
		String val1 = "";
		String val2 = "";
		for (int i = 0; i < cnt; i++) {
			if (clsName1.equalsIgnoreCase(PRIMITIVE_TYPES[i]) || clsName1.equalsIgnoreCase(PRIMITIVE_OBJECTTYPES[i]) || ObjectUtil.checkInterface(cls1, PRIMITIVE_OBJECTTYPES[i])) {
				val1 = PRIMITIVE_TYPE_VALUES[i];
				break;
			}
		}
		if (val1.length() == 0) val1 = "012";
		for (int i = 0; i < cnt; i++) {
			if (clsName2.equalsIgnoreCase(PRIMITIVE_TYPES[i]) || clsName2.equalsIgnoreCase(PRIMITIVE_OBJECTTYPES[i]) || ObjectUtil.checkInterface(cls2, PRIMITIVE_OBJECTTYPES[i])) {
				val2 = PRIMITIVE_TYPE_VALUES[i];
				break;
			}
		}
		if (val2.length() == 0) val2 = "012";
		return (val1 + methodName1).compareTo((val2 + methodName2));

	}
}

