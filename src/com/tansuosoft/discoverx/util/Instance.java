/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * 提供类实例的对象。
 * 
 * @author coca@tansuosoft.cn
 */
public class Instance {
	/**
	 * 返回java类全限定名称对应的类的默认构造器构造的实例对象。
	 * 
	 * <p>
	 * 此方法调用默认构造器，如果类没有公开默认构造器，则返回null！
	 * </p>
	 * 
	 * @param className
	 * @return Object
	 */
	public static Object newInstance(String className) {
		if (className == null || className.length() == 0) {
			FileLogger.debug("未指定有效类名信息。");
			return null;
		}
		Object obj = null;
		try {
			obj = newInstance(Class.forName(className));
		} catch (ClassNotFoundException e) {
			FileLogger.debug("找不到“%1$s”对应的类信息！", className);
		} catch (Exception e) {
			FileLogger.debug("实例化类“%1$s”时出现位置异常：%2$s。", className, e.getMessage());
		}
		return obj;
	}

	/**
	 * 返回java类信息指定的类的默认构造器构造的实例对象。
	 * 
	 * <p>
	 * 此方法调用默认构造器，如果类没有公开默认构造器，则返回null！
	 * </p>
	 * 
	 * @param cls Class&lt;?&gt;
	 * @return Object
	 */
	public static Object newInstance(Class<?> cls) {
		if (cls == null) {
			FileLogger.debug("未指定有效类型信息。");
			return null;
		}
		Object obj = null;
		try {
			obj = cls.newInstance();
		} catch (InstantiationException e) {
			FileLogger.debug("无法实例化“%1$s”类！", cls.getName());
		} catch (IllegalAccessException e) {
			FileLogger.debug("访问类“%1$s”时发生安全异常！", cls.getName());
		} catch (Exception e) {
			FileLogger.debug("实例化类“%1$s”时出现未知异常：%2$s！", cls.getName(), e.getMessage());
		}
		return obj;
	}

	/**
	 * 使用默认构造器实例化指定类型并返回实例化的对象。
	 * 
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	public static <T> T newInstanceT(Class<T> clazz) {
		if (clazz == null) {
			FileLogger.debug("未指定有效类型信息。");
			return null;
		}
		T ret = null;
		try {
			ret = clazz.newInstance();
		} catch (InstantiationException e) {
			FileLogger.debug("无法实例化类：“%1$s”。", clazz.getName());
		} catch (IllegalAccessException e) {
			FileLogger.debug("非法访问类：“%1$s”。", clazz.getName());
		}
		return ret;
	}

	/**
	 * 使用默认构造器实例化className指定的类并返回类型为T的实例化的对象。
	 * 
	 * @param <T>
	 * @param className
	 * @param clazz
	 * @return 如果类型不为T，则返回null。
	 */
	@SuppressWarnings("unchecked")
	public static <T> T newInstance(String className, Class<T> clazz) {
		if (className == null || className.length() == 0) {
			FileLogger.debug("未指定有效类名信息。");
			return null;
		}
		if (clazz == null) {
			FileLogger.debug("未指定有效类型信息。");
			return null;
		}
		T ret = null;

		Class<?> cls = null;
		try {
			cls = Class.forName(className);
		} catch (ClassNotFoundException e) {
			FileLogger.debug("找不到“%1$s”对应的类信息！", className);
		}
		if (cls == null) return null;

		Object obj = newInstance(cls);
		if (obj == null) return null;
		if (clazz.isInstance(obj)) return (T) obj;
		if (clazz.isAssignableFrom(cls)) {
			ret = (T) obj;
		}
		return ret;
	}

	/**
	 * 检查obj是否可以转换为clazz指定的类，如果可以则转换并返回转换后的类型对象，否则返回null。
	 * 
	 * @param <T>
	 * @param obj
	 * @param clazz
	 * @return 转换后的对象或者null（如果不能转换）。
	 */
	@SuppressWarnings("unchecked")
	public static <T> T safeCast(Object obj, Class<T> clazz) {
		if (obj == null) return null;
		if (clazz.isInstance(obj)) return (T) obj;
		if (clazz.isAssignableFrom(obj.getClass())) return (T) obj;
		return null;
	}
}

