/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.nio.ByteBuffer;

/**
 * 数字相关实用工具类。
 * 
 * @author coca@tensosoft.com
 */
public final class NumberUtil {
	/**
	 * 缺省构造器。
	 */
	private NumberUtil() {
	}

	/**
	 * 把整数值转换为byte数组。
	 * 
	 * @param value
	 * @return byte[]
	 */
	public static final byte[] intToByteArray(int value) {
		return new byte[] { (byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value };
	}

	/**
	 * 转换长度为4的byte数组为对应整数值。
	 * 
	 * @param b
	 * @return int
	 */
	public static final int byteArrayToInt(byte[] b) {
		return (b[0] << 24) | ((b[1] & 0xFF) << 16) | ((b[2] & 0xFF) << 8) | (b[3] & 0xFF);
	}

	/**
	 * 把短整数值转换为byte数组。
	 * 
	 * @param value
	 * @return byte[]
	 */
	public static final byte[] shortToByteArray(short value) {
		return new byte[] { (byte) (value >>> 8), (byte) value };
	}

	/**
	 * 转换长度为2的byte数组为对应短整数值。
	 * 
	 * @param b
	 * @return short
	 */
	public static final short byteArrayToShort(byte[] b) {
		return (short) (((b[0] & 0xFF) << 8) | (b[1] & 0xFF));
	}

	/**
	 * 把字符对应数值转换为byte数组。
	 * 
	 * @param value
	 * @return byte[]
	 */
	public static final byte[] charToByteArray(char value) {
		return new byte[] { (byte) (value >>> 8), (byte) value };
	}

	/**
	 * 转换长度为2的byte数组为字符对应的数值。
	 * 
	 * @param b
	 * @return char
	 */
	public static final char byteArrayToChar(byte[] b) {
		return (char) (((b[0] & 0xFF) << 8) | (b[1] & 0xFF));
	}

	/**
	 * 把长整数值转换为byte数组。
	 * 
	 * @param value
	 * @return byte[]
	 */
	public static final byte[] longToByteArray(long value) {
		return new byte[] { (byte) (value >>> 56), (byte) (value >>> 48), (byte) (value >>> 40), (byte) (value >>> 32), (byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value };
		// return ByteBuffer.allocate(8).putLong(value).array(); //另一种方法。
	}

	/**
	 * 转换长度为8的byte数组为对应长整数值。
	 * 
	 * @param b
	 * @return long
	 */
	public static final long byteArrayToLong(byte[] b) {
		return ByteBuffer.allocate(8).put(b).getLong(0);
	}

	/**
	 * 测试示例代码。
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// int
			int i = -253706112;
			byte[] bs = intToByteArray(i);
			int i1 = byteArrayToInt(bs);
			System.out.println(i1 == i);

			// short
			short s = 0;
			bs = shortToByteArray(s);
			short s1 = byteArrayToShort(bs);
			System.out.println(s1 == s);

			// char
			char c = 0x4e2d;
			bs = charToByteArray(c);
			char c1 = byteArrayToChar(bs);
			System.out.println(c1 == c);

			// long
			long l = 0x1122334455667788L;
			bs = longToByteArray(l);
			long l1 = byteArrayToLong(bs);
			System.out.println(l1 == l);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

