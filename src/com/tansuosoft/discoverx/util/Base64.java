/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.io.File;

/**
 * Base64编码对象
 * 
 * @author coca@tansuosoft.cn
 * @version 1.00
 */
public class Base64 {
	/**
	 * 编码文件对象所指的文件
	 * 
	 * @param file
	 * @return 编码后的字符数组
	 */
	static public char[] encode(File file) {
		if (!file.exists()) {
			System.err.println("错误:文件不存在！");
			return null;
		}
		return encode(IOHelper.readBytes(file));
	}

	/**
	 * 编码文件名所指的文件
	 * 
	 * @param filename
	 * @return 编码后的字符数组
	 */
	static public char[] encode(String filename) {
		File file = new File(filename);
		if (!file.exists()) {
			System.err.println("错误:文件“" + filename + "”不存在！");
			return null;
		}
		return encode(IOHelper.readBytes(file));
	}

	/**
	 * 编码传入的字节数组，输出编码后的字符数组
	 * 
	 * @param data
	 * @return 编码后的字符数组
	 */
	static public char[] encode(byte[] data) {
		char[] out = new char[((data.length + 2) / 3) * 4];
		for (int i = 0, index = 0; i < data.length; i += 3, index += 4) {
			boolean quad = false;
			boolean trip = false;

			int val = (0xFF & data[i]);
			val <<= 8;
			if ((i + 1) < data.length) {
				val |= (0xFF & data[i + 1]);
				trip = true;
			}
			val <<= 8;
			if ((i + 2) < data.length) {
				val |= (0xFF & data[i + 2]);
				quad = true;
			}
			out[index + 3] = alphabet[(quad ? (val & 0x3F) : 64)];
			val >>= 6;
			out[index + 2] = alphabet[(trip ? (val & 0x3F) : 64)];
			val >>= 6;
			out[index + 1] = alphabet[val & 0x3F];
			val >>= 6;
			out[index + 0] = alphabet[val & 0x3F];
		}
		return out;
	}

	/**
	 * 把Base64编码的字符数组解码为字节数组
	 * 
	 * @param data Base64编码的数据字符数组
	 * @return 字节数据
	 */
	static public byte[] decode(char[] data) {
		int tempLen = data.length;
		for (int ix = 0; ix < data.length; ix++) {
			if ((data[ix] > 255) || codes[data[ix]] < 0)
				--tempLen;
		}
		int len = (tempLen / 4) * 3;
		if ((tempLen % 4) == 3)
			len += 2;
		if ((tempLen % 4) == 2)
			len += 1;

		byte[] out = new byte[len];

		int shift = 0;
		int accum = 0;
		int index = 0;

		for (int ix = 0; ix < data.length; ix++) {
			int value = (data[ix] > 255) ? -1 : codes[data[ix]];
			if (value >= 0) {
				accum <<= 6;
				shift += 6;
				accum |= value;
				if (shift >= 8) {
					shift -= 8;
					out[index++] = (byte) ((accum >> shift) & 0xff);
				}
			}
		}
		if (index != out.length) {
			throw new Error("数据长度不一致(实际写入了 " + index + "字节，但是系统指示有" + out.length
			    + "字节)");
		}
		return out;
	}

	/*
	 * 用于Base64编码的字符
	 */
	static private char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
	    .toCharArray();
	/*
	 * 用于解码的字节（0-255）
	 */
	static private byte[] codes = new byte[256];
	static {
		for (int i = 0; i < 256; i++)
			codes[i] = -1;
		for (int i = 'A'; i <= 'Z'; i++)
			codes[i] = (byte) (i - 'A');
		for (int i = 'a'; i <= 'z'; i++)
			codes[i] = (byte) (26 + i - 'a');
		for (int i = '0'; i <= '9'; i++)
			codes[i] = (byte) (52 + i - '0');
		codes['+'] = 62;
		codes['/'] = 63;
	}
}

