/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 时间日期封装类。
 * 
 * <p>
 * 时间格式参数说明：注意这个时间格式有大小写的区别，特别是MM,和mm代表月份和分钟
 * </p>
 * <p>
 * <ui>
 * <li>yyyy:4位的年</li>
 * <li>MM:2位的月份，如果只要一位的年，只要一个M</li>
 * <li>dd:2位的天，如果只要一位，只要一个d</li>
 * <li>HH:24小时制小时，如果只要一位的小时，只要一个H;如果12小时制的，用h</li>
 * <li>mm:2位的分钟，如果只要一位的分钟，只要一个m</li>
 * <li>ss:2位的秒数，如果只要一位的秒数，只要一个s</li>
 * <li style="display:none">E:星期几，将直接返回类似<tt><b>星期一</b></tt>的字符串</li> </ui>
 * </p>
 * 
 * @see java.text.SimpleDateFormat
 * 
 * @author coca@tansuosoft.cn
 * @version 1.0
 * 
 */
public class DateTime {
	protected final static String DEFAULT_DT_PATTERN = "yyyy-MM-dd HH:mm:ss";
	protected final static String DEFAULT_DT_PATTERN_CN = "yyyy年M月d日H时m分s秒 E";
	protected final static SimpleDateFormat sf = new SimpleDateFormat(DEFAULT_DT_PATTERN);

	// 此对象绑定的日期时间毫秒值。
	long nowtime;

	/**
	 * 构造当前系统时间对应的DateTime对象。
	 */
	public DateTime() {
		nowtime = System.currentTimeMillis();
	}

	/**
	 * 构造指定的日期时间毫秒值对应的DateTime对象。
	 * 
	 * @param timeMillis
	 */
	public DateTime(long timeMillis) {
		nowtime = timeMillis;
	}

	/**
	 * 构造指定的java.util.Date对象对应的DateTime对象。
	 * 
	 * @param date
	 */
	public DateTime(Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date == null ? new Date() : date);
		nowtime = cal.getTimeInMillis();
	}

	/**
	 * 构造字符串指定的日期时间对应的DateTime对象。如果指定的时间不符合格式，将使用系统当前时间构造DateTime对象。
	 * 
	 * @param dt 必须以“yyyy-MM-dd HH:mm:ss”格式提供日期时间构造字符串
	 */
	public DateTime(String dt) {
		this(dt, DEFAULT_DT_PATTERN);
	}

	/**
	 * 构造指定模式的指定日期时间字符串对应的DateTime对象。如果指定的时间不符合格式，将使用系统当前时间构造DateTime对象。
	 * 
	 * @param dt 指定的时间字符串
	 * @param pattern 指定的时间字符串模式
	 */
	public DateTime(String dt, String pattern) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			Date d = sdf.parse(dt);
			nowtime = d.getTime();
		} catch (ParseException e) {
			nowtime = System.currentTimeMillis();
		}
	}

	/**
	 * 返回此对象绑定的日期时间字符串，返回格式为：yyyy-MM-dd HH:mm:ss
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return toString(DEFAULT_DT_PATTERN);
	}

	/**
	 * 按指定格式返回此对象绑定的日期时间字符串。
	 * 
	 * @param pattern String 指定返回的日期时间字符串的格式字符串。
	 * @return String
	 */
	public String toString(String pattern) {
		String retValue = null;
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		retValue = sdf.format(new Date(nowtime));
		return retValue;
	}

	/**
	 * 返回4位的年份,如“2008”
	 * 
	 * @return String
	 */
	public String getYear() {
		return toString("yyyy");
	}

	/**
	 * 返回月份。
	 * 
	 * @return Strning 如果8月返回08，12月返回12
	 */
	public String getMonth() {

		return toString("MM");
	}

	/**
	 * 返回月中的第几天
	 * 
	 * @return String 月份的第几天，如当前是4月1日将返回'01'
	 */
	public String getDay() {
		return toString("dd");
	}

	/**
	 * 返回24小时制的小时
	 * 
	 * @return String
	 */
	public String getHour() {
		return toString("HH");
	}

	/**
	 * 返回分钟
	 * 
	 * @return String 分钟数，从00-59。
	 */
	public String getMinute() {
		return toString("mm");
	}

	/**
	 * 返回秒数
	 * 
	 * @return 秒数，从00-59。
	 */
	public String getSecond() {
		return toString("ss");
	}

	/**
	 * 返回星期几
	 * 
	 * @return String 如星期二将返回"星期二"
	 */
	public String getDayInWeek() {
		return toString("E");
	}

	/**
	 * 只返回日期部分，如“2008-06-06”.月份和日期都是两位，不足的在前面补0
	 * 
	 * @return String 日期字符串
	 */
	public String getDate() {
		return toString("yyyy-MM-dd");
	}

	/**
	 * 只返回时间部分，如“12:20:30”.时间为24小时制,分钟和秒数都是两位，不足补0
	 * 
	 * @return String 时间字符串
	 */
	public String getTime() {
		return toString("HH:mm:ss");
	}

	/**
	 * 调整年份
	 * 
	 * @param i int 要调整的基数，正表示加，负表示减
	 */
	public void adjustYear(int i) {
		this.adjustTime(i, 0, 0, 0, 0, 0);
	}

	/**
	 * 调整月份
	 * 
	 * @param i int 要调整的基数，正表示加，负表示减
	 */
	public void adjustMonth(int i) {
		this.adjustTime(0, i, 0, 0, 0, 0);
	}

	/**
	 * 调整天数
	 * 
	 * @param i int 要调整的基数，正表示加，负表示减
	 */
	public void adjustDay(int i) {
		this.adjustTime(0, 0, i, 0, 0, 0);
	}

	/**
	 * 调整小时
	 * 
	 * @param i int 要调整的基数，正表示加，负表示减
	 */
	public void adjustHour(int i) {
		this.adjustTime(0, 0, 0, i, 0, 0);
	}

	/**
	 * 调整分钟数
	 * 
	 * @param i int 要调整的基数，正表示加，负表示减
	 */
	public void adjustMinute(int i) {
		this.adjustTime(0, 0, 0, 0, i, 0);
	}

	/**
	 * 调整秒数
	 * 
	 * @param i int 要调整的基数，正表示加，负表示减
	 */
	public void adjustSecond(int i) {
		this.adjustTime(0, 0, 0, 0, 0, i);
	}

	/**
	 * 用指定的年月日时分秒等值调整日期时间。
	 * 
	 * @param y 年
	 * @param m 月
	 * @param d 日
	 * @param h 小时
	 * @param mm 分钟
	 * @param s 秒
	 */
	protected void adjustTime(int y, int m, int d, int h, int mm, int s) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeInMillis(nowtime);
		cal.add(Calendar.YEAR, y);
		cal.add(Calendar.MONTH, m);
		cal.add(Calendar.DAY_OF_MONTH, d);
		cal.add(Calendar.HOUR_OF_DAY, h);
		cal.add(Calendar.MINUTE, mm);
		cal.add(Calendar.SECOND, s);
		nowtime = cal.getTimeInMillis();
	}

	/**
	 * 返回默认格式的中文大写日期时间字符串.如当前日期是“2008-06-26 18:05:12”，则返回“二零零八年六月二十六日十八时五分十二秒 星期五”
	 * 
	 * @return String
	 */
	public String toCNUpperCaseString() {
		return this.toCNUpperCaseString(DEFAULT_DT_PATTERN_CN);
	}

	/**
	 * 返回指定格式的中文大写日期时间字符串.如当前日期是“2008-06-26”，则pattern为“yyyy年MM月dd日”时返回“ 二零零八年零六月二十六日”
	 * 
	 * @param pattern
	 * @return String
	 */
	public String toCNUpperCaseString(String pattern) {
		String raw = null;
		if (pattern == null || pattern.length() == 0)
			raw = this.toString(DEFAULT_DT_PATTERN_CN);
		else
			raw = this.toString(pattern);

		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(raw);
		String ret = new String(raw);
		while (m.find()) {
			ret = ret.replaceFirst(m.group(), this.dtNumToCNUpperCase(m.group()));
		}

		return ret;
	}

	/**
	 * 把日期时间的数字部分转换为中文大写。
	 * 
	 * @param part
	 * @return
	 */
	private String dtNumToCNUpperCase(String part) {
		if (part == null || part.length() == 0) return part;
		boolean isYear = false;
		if (part.length() == 4) isYear = true;
		final String CN_NUMS[] = { "〇", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
		final String CN_NUMXS[] = { "", "十", "百", "千", "万" };
		char[] chars = null;
		int idx = 0;
		StringBuffer sb = null;

		// 处理年
		if (isYear) {
			chars = part.toCharArray();
			idx = 0;
			sb = new StringBuffer();
			for (int i = 0; i < chars.length; i++) {
				idx = Integer.parseInt("" + chars[i]);
				sb.append(CN_NUMS[idx]);
			}
			return sb.toString();
		}

		// 处理其它
		if (Integer.parseInt(part) == 0)
			return (part.length() == 1 ? "零" : "零零"); // return new
		// String(CN_NUMS[Integer
		// .parseInt(part)]);
		else if (Integer.parseInt(part) < 10) {
			if (part.length() == 1)
				return new String(CN_NUMS[Integer.parseInt(part)]);
			else
				return String.format("零%s", new String(CN_NUMS[Integer.parseInt(part)]));
		} else {
			chars = part.toCharArray();
			idx = 0;
			sb = new StringBuffer();
			for (int i = 0; i < chars.length; i++) {
				idx = Integer.parseInt("" + chars[i]);
				if (idx != 0) sb.append(CN_NUMS[idx]);
				if (i < (chars.length - 1)) sb.append(CN_NUMXS[chars.length - 1 - i]);
			}
			if (sb.length() == 3 && sb.charAt(0) == '一')
				return sb.substring(1);
			else
				return sb.toString();
		}
	}

	/**
	 * 返回此对象绑定的日期时间对应的Java Date对象。
	 * 
	 * @return java.util.Date
	 */
	public Date getJavaDate() {
		return new Date(nowtime);
	}

	/**
	 * 获取调整指定的年月日时分值后的新的DateTime对象。
	 * 
	 * @param y 年
	 * @param m 月
	 * @param d 日
	 * @param h 小时
	 * @param mm 分钟
	 * @param s 秒
	 * @return DateTime
	 */
	public DateTime getAdjustDateTime(int y, int m, int d, int h, int mm, int s) {
		DateTime dt = null;
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeInMillis(nowtime);
		cal.add(Calendar.YEAR, y);
		cal.add(Calendar.MONTH, m);
		cal.add(Calendar.DAY_OF_MONTH, d);
		cal.add(Calendar.HOUR_OF_DAY, h);
		cal.add(Calendar.MINUTE, mm);
		cal.add(Calendar.SECOND, s);
		dt = new DateTime(cal.getTimeInMillis());
		return dt;
	}

	/**
	 * 返回当前日期时间对象对应的毫秒值。
	 * 
	 * @return long
	 */
	public long getTimeMillis() {
		return nowtime;
	}

	/**
	 * 比较两个日期时间。
	 * 
	 * @param dt
	 * @return int
	 */
	public int compareTo(DateTime dt) {
		if (dt == null) return 1;
		return this.getJavaDate().compareTo(dt.getJavaDate());
	}

	/**
	 * 返回当前日期时间对应DateTime对象的toString结果。
	 * 
	 * @return String
	 */
	public static String getNowDTString() {
		DateTime dt = new DateTime();
		return dt.toString();
	}
}

