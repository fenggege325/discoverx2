/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

/**
 * 异常处理相关实用工具类。
 * 
 * @author coca@tensosoft.com
 */
public class ThrowableUtil {
	/**
	 * 缺省构造器。
	 */
	private ThrowableUtil() {
	}

	/**
	 * 获取t指定的异常对象的根{@link java.lang.Throwable}对象。
	 * 
	 * @param t
	 * @return {@link Throwable}
	 */
	public static Throwable getRootThrowable(Throwable t) {
		if (t == null) return t;
		Throwable cause = t.getCause();
		if (cause == null) return t;
		Throwable p = cause.getCause();
		while (p != null) {
			cause = p;
			p = p.getCause();
		}
		return cause;
	}

}

