/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

/**
 * 类序列化时可返回其绑定的{@link SerializationFilter}对象的类所需实现的通用接口。
 * 
 * @author coca@tansuosoft.cn
 */
public interface SerializationFilterable {
	/**
	 * 返回类绑定的{@link SerializationFilter}对象。
	 * 
	 * @param serializationType 序列化类型。
	 * @return 如果返回null，则说明使用系统默认的序列化选项。
	 */
	public SerializationFilter getSerializationFilter(SerializationType serializationType);
}

