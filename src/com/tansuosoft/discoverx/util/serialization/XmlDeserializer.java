/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.tansuosoft.discoverx.util.Instance;

/**
 * 读取XML文件或文本中信息并反序列化为指定对象的类。
 * 
 * <p>
 * 此类只能反序列化通过{@link XmlSerializer}序列化（即符合{@link XmlSerializer}的序列化规则）的xml内容。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class XmlDeserializer extends DefaultHandler implements Deserializer {
	private SAXParser m_sp = null; // SAX解析器
	private InputSource m_isrc = null; // 要解析的xml输入数据源

	@SuppressWarnings("unused")
	private int m_elementCount = 0; // 节点计数
	private int m_depth = 0; // 节点层次计数
	private Stack<ParsedInfo> m_stack = null; // 包含解析结果信息对象的堆栈
	private Object m_target = null; // 最终解析结果对象
	private ParsedInfo m_current = null; // 当前解析对象
	private String m_currentElementName = null; // 当前解析的节点元素名
	private StringBuilder m_currentElementValue = null; // 当前解析的节点元素值
	private String m_xmlSource = null; // 解析的xml源信息。
	private boolean m_verbose = true; // 是否输出详细解析日志。

	/**
	 * 缺省构造器
	 */
	public XmlDeserializer() {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setValidating(false);
		try {
			this.m_sp = spf.newSAXParser();
		} catch (ParserConfigurationException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		} catch (SAXException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
	}

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	public Object deserialize(String src, Class<?> cls) {
		Object x = null;
		m_xmlSource = src;
		if (src.trim().startsWith("<")) { // xml内容
			x = this.produce(src, cls == null ? null : cls.getName());
		} else { // 文件路径
			File file = new File(src);
			if (!file.exists()) return null;
			x = this.produce(file, cls == null ? null : cls.getName());
		}
		return x;
	}

	/**
	 * 根据xml文件构造（反序列化）targetClassName指定对象。
	 * 
	 * @param xmlFile
	 * @param targetClassName
	 * @return Object
	 */
	public Object produce(java.io.File xmlFile, String targetClassName) {
		m_xmlSource = (xmlFile.exists() ? xmlFile.getName() : "");
		if (this.m_target == null && targetClassName != null && targetClassName.length() > 0) {
			this.m_target = com.tansuosoft.discoverx.util.Instance.newInstance(targetClassName);
		}
		try {
			this.m_isrc = new InputSource(new FileInputStream(xmlFile));
			this.m_sp.parse(this.m_isrc, this);
		} catch (FileNotFoundException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		} catch (SAXException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		} catch (IOException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
		return this.m_target;
	}

	/**
	 * 根据xml文本内容构造（反序列化）targetClassName指定对象。
	 * 
	 * @param xmlContent
	 * @param targetClassName
	 * @return Object
	 */
	public Object produce(String xmlContent, String targetClassName) {
		m_xmlSource = xmlContent;
		if (this.m_target == null && targetClassName != null && targetClassName.length() > 0) {
			this.m_target = com.tansuosoft.discoverx.util.Instance.newInstance(targetClassName);
		}
		this.m_isrc = new InputSource(new StringReader(xmlContent));
		try {
			this.m_sp.parse(this.m_isrc, this);
		} catch (SAXException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		} catch (IOException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
		return this.m_target;
	}

	/**
	 * 根据xml文件构造（反序列化）对象。
	 * 
	 * @param xmlFile
	 * @return Object
	 */
	public Object produce(java.io.File xmlFile) {
		return this.produce(xmlFile, null);
	}

	/**
	 * 根据xml文本内容构造（反序列化）对象。
	 * 
	 * @param xmlContent
	 * @return Object
	 */
	public Object produce(String xmlContent) {
		return this.produce(xmlContent, null);
	}

	/**
	 * 设置构造（反序列化）的目标对象。
	 * 
	 * <p>
	 * 如果不设置且produce或deserialize时没有指定目标对象信息，则会尝试从xml根节点中获取目标对象信息。
	 * </p>
	 * 
	 * @param target 设置目标对象。
	 */
	public void setTarget(Object target) {
		this.m_target = target;
	}

	/**
	 * 文档开始事件处理程序。
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#startDocument()
	 */
	public void startDocument() throws SAXException {
		this.m_elementCount = 0;
		this.m_depth = 0;
		this.m_stack = new Stack<ParsedInfo>();
	}

	/**
	 * 节点解析开始事件处理程序。
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		// 设置状态
		this.increaseElementCount();
		this.increaseDepth();
		this.m_currentElementName = qName;

		Object bindObj = null; // 要设置到当前处理对象属性中的对象值。
		ParsedInfo parsedInfo = null;
		// 判断解析到的节点对应的对象类型
		if (this.isObject(attributes) || (m_depth == 1 && m_target != null)) { // 对象类型或第一层次的节点且提供了目标对象
			if (this.isList(attributes)) { // 列表集合
				bindObj = new ArrayList<Object>();
				String[] strs = getCollectionGenericTypes(attributes);
				parsedInfo = ParsedInfoFactory.getParsedInfo(ParsedInfoType.ListObject, this.m_depth, qName, bindObj);
				((ListObject) parsedInfo).setGenericType(strs[0]);
			} else if (this.isMap(attributes)) { // 哈希集合
				bindObj = new Hashtable<Object, Object>();
				String[] strs = getCollectionGenericTypes(attributes);
				parsedInfo = ParsedInfoFactory.getParsedInfo(ParsedInfoType.MapObject, this.m_depth, qName, bindObj);
				((MapObject) parsedInfo).setKeyGenericType(strs[0]);
				((MapObject) parsedInfo).setValueGenericType(strs[1]);
			} else { // 普通javabean对象
				if (this.m_depth == 1 && this.m_target != null) {
					bindObj = this.m_target;
				} else {
					bindObj = this.getObject(attributes);
					if (this.m_depth == 1 && this.m_target == null) this.m_target = bindObj; // 如果返回结果对象为null，那么用第一个绑定对象（根节点对应的对象）设置之。
				}
				parsedInfo = ParsedInfoFactory.getParsedInfo(ParsedInfoType.NormalObject, this.m_depth, qName, bindObj);
			}
		} else { // 其它类型（集合条目、字符串、数字、布尔、枚举等类型的属性）
			if (qName == "listItem") { // 列表集合项
				parsedInfo = ParsedInfoFactory.getParsedInfo(ParsedInfoType.ListItem, this.m_depth, qName, bindObj);
			} else if (qName == "mapItem") { // 哈希集合项开始
				// 忽略
			} else if (qName == "mapKey") { // 哈希集合key信息
				parsedInfo = ParsedInfoFactory.getParsedInfo(ParsedInfoType.MapKeyItem, this.m_depth, qName, bindObj);
			} else if (qName == "mapValue") {// 哈希集合value信息
				parsedInfo = ParsedInfoFactory.getParsedInfo(ParsedInfoType.MapValueItem, this.m_depth, qName, bindObj);
			} else { // 字符串、数字、布尔、枚举等基础类型的属性
				parsedInfo = ParsedInfoFactory.getParsedInfo(ParsedInfoType.Property, this.m_depth, qName, bindObj);
			}
		}

		// 设置上级
		if (this.m_current != null && parsedInfo != null) {
			int depthDiff = parsedInfo.getDepth() - this.m_current.getDepth();
			// 设置上级：如果当前的解析结果信息深度大于的上一次解析结果深度，那么上一次的解析结果信息是当前解析结果信息的上级。
			if (depthDiff > 0) {
				parsedInfo.setParent(this.m_current);
			} else if (depthDiff == 0) {// 如果深度相等，那么是平级。
				ParsedInfo parent = this.m_current.getParent();
				if (parent != null) parsedInfo.setParent(parent);
			} else {// 如果小于，那么上一次的解析结果信息是当前解析结果信息的上级。
				ParsedInfo parent = this.m_current.getParent();
				while (parent != null) {
					if (parent.getDepth() == parsedInfo.getDepth()) {
						parsedInfo.setParent(parent.getParent());
						break;
					}
					parent = parent.getParent();
				}
			}// else end
		}

		// 设置状态
		if (parsedInfo != null) {
			parsedInfo.setVerbose(m_verbose);
			parsedInfo.setXmlSource(m_xmlSource);
			this.m_stack.push(parsedInfo); // 把刚解析的解析结果对象压栈
			this.m_current = parsedInfo; // 替换当前解析结果信息对象为刚解析的解析结果对象。
		}
		this.m_currentElementValue = null; // 清空值
	}

	/**
	 * 常量字符串事件处理程序。
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	public void characters(char ac[], int start, int length) throws SAXException {
		if (this.m_currentElementName != null) { // 与当前值匹配的节点名称不为null时追加节点值（如果m_currentElementName为null说明是上级节点的结束碰到的文本事件，忽略之）。
			if (this.m_currentElementValue == null) {
				this.m_currentElementValue = new StringBuilder(new String(ac, start, length));
			} else {
				this.m_currentElementValue.append(new String(ac, start, length));
			}
		}
	}

	/**
	 * 节点解析结束事件处理程序。
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(String, String, String)
	 */
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// 如果包含文本值的节点结束，那么把文本值设置给当前对象。
		if (this.m_currentElementValue != null) {
			String elementValueText = this.m_currentElementValue.toString(); // 节点包含的文本值。
			this.m_current.setElementValue(elementValueText);
		}
		// 设置状态
		this.decreaseDepth();
		this.m_currentElementName = null;
		this.m_currentElementValue = null;
	}

	/**
	 * 文档结束事件处理程序。
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#endDocument()
	 */
	public void endDocument() throws SAXException {
		this.assemble(); // 组装对象
	}

	/**
	 * 错误事件。
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#error(org.xml.sax.SAXParseException)
	 */
	public void error(SAXParseException ex) {

	}

	/**
	 * 严重错误事件处理程序。
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#fatalError(org.xml.sax.SAXParseException)
	 */
	public void fatalError(SAXParseException ex) throws SAXException {

	}

	/**
	 * 警告事件处理程序。
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#warning(org.xml.sax.SAXParseException)
	 */
	public void warning(SAXParseException ex) {

	}

	/**
	 * 装配最终结果对象。
	 */
	protected void assemble() {
		for (ParsedInfo x : this.m_stack) {
			if (x == null) continue;
			x.assemble();
		}// for end
	}// func end

	/**
	 * 判断xml节点包含的xml属性中是否包含有对象类型（type）对应的xml属性。
	 * 
	 * @param attributes
	 * @return boolean 如果包含有效的type属性值，则返回true，否则返回false。
	 */
	protected boolean isObject(Attributes attributes) {
		String clsName = attributes.getValue("type");
		return (clsName != null && clsName.trim().length() > 0);
	}

	/**
	 * 根据xml节点包含的名为“type”的xml属性值中的对象全限定名称返回对象实例。
	 * 
	 * @param attributes
	 * @return Object
	 */
	protected Object getObject(Attributes attributes) {
		String clsName = attributes.getValue("type");
		if (clsName == null || clsName.length() == 0) return null;
		return Instance.newInstance(clsName);
	}

	/**
	 * 判断是否列表(java.util.List)。
	 * 
	 * <p>
	 * 名为“type”的xml属性值对应结果以“java.util.List”开始则为列表。
	 * </p>
	 * 
	 * @param attributes Attributes xml属性集合
	 * @return boolean
	 */
	protected boolean isList(Attributes attributes) {
		String clsName = attributes.getValue("type");
		return (clsName != null && clsName.startsWith("java.util.List"));
	}

	/**
	 * 判断是否哈希Map(java.util.Map)。
	 * 
	 * <p>
	 * 名为“type”的xml属性值对应结果以“java.util.Map”开始则为哈希Map。
	 * </p>
	 * 
	 * @param attributes Attributes xml属性集合
	 * @return boolean
	 */
	protected boolean isMap(Attributes attributes) {
		String clsName = attributes.getValue("type");
		return (clsName != null && clsName.startsWith("java.util.Map"));
	}

	/**
	 * 根据xml节点包含的名为“type”的xml属性值中的集合对象全限定名称返回集合包含的范型结果数组。
	 * 
	 * @param attributes
	 * @return String[] 返回包含二个元素的字符串数组，如果是哈希集合，则返回的结果的第二个元素不能是null，其余情况下为null。
	 */
	protected String[] getCollectionGenericTypes(Attributes attributes) {
		String clsName = attributes.getValue("type");
		if (clsName == null || clsName.length() == 0) return null;
		int startPos = clsName.indexOf('<');
		int endPos = clsName.lastIndexOf('>');
		if (startPos <= 0 || endPos <= 0) return null;
		String genericTypeString = clsName.substring(startPos + 1, endPos);
		if (genericTypeString == null || genericTypeString.length() == 0) return null;
		startPos = genericTypeString.indexOf(',');
		String first = null;
		String second = null;
		if (startPos > 0) {
			first = genericTypeString.substring(0, startPos);
			second = genericTypeString.substring(startPos + 1);
		} else {
			first = genericTypeString;
			second = null;
		}
		ArrayList<String> al = new ArrayList<String>();
		al.add(first);
		al.add(second);
		String[] strs = new String[al.size()];
		al.toArray(strs);
		return strs;
	}

	/**
	 * 节点计数加1。
	 */
	protected void increaseElementCount() {
		this.m_elementCount++;
	}

	/**
	 * 节点计数减1。
	 */
	protected void decreaseElementCount() {
		this.m_elementCount--;
	}

	/**
	 * 节点层次计数加1。
	 */
	protected void increaseDepth() {
		this.m_depth++;
	}

	/**
	 * 节点层次计数减1。
	 */
	protected void decreaseDepth() {
		this.m_depth--;
	}

	/**
	 * 设置是否输出详细解析日志。
	 * 
	 * <p>
	 * 默认为true，此时如果出现未找到的方法等异常情况会输出日志，否则不会输出日志。
	 * </p>
	 * 
	 * @param verbose boolean
	 */
	public void setVerbose(boolean verbose) {
		this.m_verbose = verbose;
	}

}// class end

