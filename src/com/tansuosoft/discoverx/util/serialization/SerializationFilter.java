/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

/**
 * 判断指定属性是否需要序列化的类必须实线的接口。
 * 
 * @author coca@tansuosoft.cn
 */
public interface SerializationFilter {
	/**
	 * 判断指定属性是否需要序列化
	 * 
	 * @param propName
	 * @return
	 */
	public boolean filter(String propName);
}

