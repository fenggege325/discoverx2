/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 哈希集合类型的JSON序列化类。
 * 
 * @author coca@tansuosoft.cn
 */
public class MapJsonSerializer extends JsonSerializer {

	/**
	 * 重载serialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Serializer#serialize(java.lang.Object, java.io.Writer)
	 */
	public void serialize(Object o, Writer writer) {
		StringWriter sw = null; // 输出文本的缓存对象
		JsonSerializer ser = null; // 序列化对象
		Class<?> actualTypeCls1 = null; // 泛型包含参数实际类型1对应java类
		Class<?> actualTypeCls2 = null; // 泛型包含参数实际类型2对应java类
		Map<?, ?> valMap = null; // 哈希集合
		Object key = null; // 关键字
		Object val = null; // 关键字对应的值
		valMap = (Map<?, ?>) o;
		if (valMap == null || valMap.isEmpty()) return;
		StringBuilder sb = new StringBuilder();
		for (Object x : valMap.keySet()) {
			key = x;
			if (key == null) continue;
			val = valMap.get(key);
			if (val != null) break;
		}
		actualTypeCls1 = key.getClass();
		actualTypeCls2 = val.getClass();

		String crlf = "\r\n";
		int idx = 0;
		for (Object x : valMap.keySet()) {
			if (x == null) continue;
			sb.append("{");
			if (ObjectUtil.checkPrimitive(actualTypeCls1.getName())) {
				sb.append("mapKey:").append(x);
			} else if (actualTypeCls1.isPrimitive() || actualTypeCls1.isEnum()) {
				sb.append("mapKey:").append(x);
			} else if (actualTypeCls1.getName() == String.class.getName()) {
				sb.append("mapKey:").append("'").append(StringUtil.encode4Json(x.toString())).append("'");
			} else {
				sw = new StringWriter();
				ser = new JsonSerializer();
				ser.serialize(x, sw);
				sb.append("mapKey:{");
				sb.append(sw.toString());
				sb.append("}");
			}
			sb.append(",");
			val = valMap.get(x);
			if (actualTypeCls2.getName() == Integer.class.getName() || actualTypeCls2.getName() == Character.class.getName() || actualTypeCls2.getName() == Byte.class.getName() || actualTypeCls2.getName() == Short.class.getName() || actualTypeCls2.getName() == Boolean.class.getName() || actualTypeCls2.getName() == Long.class.getName() || actualTypeCls2.getName() == Double.class.getName() || actualTypeCls2.getName() == Float.class.getName()) {
				sb.append("mapValue:").append(val);
			} else if (actualTypeCls2.isPrimitive() || actualTypeCls2.isEnum()) {
				sb.append("mapValue:").append(val);
			} else if (actualTypeCls2.getName() == String.class.getName()) {
				sb.append("mapValue:").append("'").append(val.toString().replace("'", "\\'")).append("'");
			} else {
				sw = new StringWriter();
				ser = new JsonSerializer();
				ser.serialize(val, sw);
				sb.append("mapValue:{");
				sb.append(sw.toString());
				sb.append("}");
			}
			sb.append("}");
			idx++;
			sb.append(idx < valMap.size() ? "," : "").append(crlf);
		}// for end
		try {
			writer.write(sb.toString());
		} catch (IOException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
	}// func end
}

