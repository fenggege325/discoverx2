/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 将对象序列化为JSON格式的文本的类。
 * 
 * <p>
 * 序列化规则/要求说明请参考{@link XmlSerializer}
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class JsonSerializer implements Serializer {
	private SerializationFilter m_fileter = null;

	/**
	 * 缺省构造器。
	 */
	public JsonSerializer() {
	}

	/**
	 * 设置自定义属性序列化类。
	 * 
	 * @param serializationFilter
	 */
	public void setSerializationFilter(SerializationFilter serializationFilter) {
		m_fileter = serializationFilter;
	}

	/**
	 * 重载serialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Serializer#serialize(java.lang.Object, java.io.Writer)
	 */
	public void serialize(Object o, Writer writer) {
		if (o == null) return;

		Class<?> cls = o.getClass();
		if (cls.isPrimitive()) return;
		try {
			if (o instanceof JsonSerializable) {
				writer.write(((JsonSerializable) o).toJson());
				return;
			}
		} catch (IOException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
		}

		if (m_fileter == null && o instanceof SerializationFilterable) m_fileter = ((SerializationFilterable) o).getSerializationFilter(SerializationType.Json);

		Map<Method, Method> methods = ObjectUtil.getInstanceRWPropMethods(o);// 获取可读写的属性。
		if (methods == null || methods.size() == 0) return;
		StringBuilder sb = new StringBuilder();
		String methodName = null;
		String propNameToCamelCase = null;

		Object val = null; // 方法返回值
		Class<?> retCls = null; // 返回值对应的java类

		List<?> valList = null;
		Map<?, ?> valMap = null;

		Class<?> itemTypeFirst = null; // 列表集合值或字典集合键值对应类型
		Class<?> itemTypeSecond = null; // 字典集合值对应类型
		Object itemValue = null;

		StringWriter sw = null; // 输出文本的缓存对象
		JsonSerializer ser = null; // 包含的下级序列化对象

		String crlf = "\r\n";
		for (Method x : methods.keySet()) {
			retCls = x.getReturnType();
			methodName = x.getName();
			if (!methodName.startsWith("get")) continue; // 忽略非getter方法。
			if (x.getParameterTypes().length > 0) continue; // 忽略多个参数的方法。
			String propName = methodName.substring(3);
			if (StringUtil.isUpperCase(propName)) {
				propNameToCamelCase = propName;
			} else {
				propNameToCamelCase = StringUtil.toCamelCase(propName);
			}
			if (m_fileter != null && !m_fileter.filter(propNameToCamelCase)) continue;

			val = ObjectUtil.getMethodResult(o, x);
			if (val == null) continue;

			if (ObjectUtil.checkVoid(retCls.getName())) continue; // void不处理。
			if (retCls.isPrimitive()) { // 原始类型属性。
				sb.append(propNameToCamelCase).append(":").append(val);
			} else if (val instanceof String) { // 字符串
				sb.append(propNameToCamelCase).append(":'").append(StringUtil.encode4Json((String) val)).append("'");
			} else if (retCls.isEnum()) { // 枚举
				sb.append(propNameToCamelCase).append(":").append(val);
			} else if (val instanceof java.util.List) { // 列表集合
				valList = (List<?>) val;
				if (valList == null || valList.isEmpty()) continue;
				itemValue = valList.get(0);
				itemTypeFirst = (itemValue != null ? itemValue.getClass() : null);
				if (itemTypeFirst == null) continue;
				sb.append(propNameToCamelCase).append(":[").append(crlf);
				ser = new ListJsonSerializer();
				sw = new StringWriter();
				ser.serialize(val, sw);
				sb.append(sw.toString());
				sb.append("]");
			} else if (val instanceof java.util.Map) { // 哈希集合
				valMap = (Map<?, ?>) val;
				if (valMap == null || valMap.isEmpty()) continue;
				for (Object k : valMap.keySet()) {
					itemTypeFirst = (k != null ? k.getClass() : null);
					itemValue = valMap.get(k);
					itemTypeSecond = (itemValue != null ? itemValue.getClass() : null);
				}
				if (itemTypeFirst == null || itemTypeSecond == null) continue;
				sb.append(propNameToCamelCase).append(":[").append(crlf);
				ser = new MapJsonSerializer();
				sw = new StringWriter();
				ser.serialize(val, sw);
				sb.append(sw.toString());
				sb.append("]");
			} else if (val instanceof JsonSerializable) { // 可json序列化的对象。
				sb.append(propNameToCamelCase).append(((JsonSerializable) val).toJson());
			} else { // 其它普通类
				sb.append(propNameToCamelCase).append(":{").append(crlf);
				sw = new StringWriter();
				ser = new JsonSerializer();
				ser.serialize(val, sw);
				sb.append(sw.toString());
				sb.append("}");
			}
			sb.append(",").append(crlf);
		}// for end
		try {
			if (sb.length() == 0) { return; }
			sb.delete(sb.length() - 3, sb.length()).append(crlf);
			writer.write(sb.toString());
		} catch (IOException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
	}// func end
}// class end

