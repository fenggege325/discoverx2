/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 通用对象的XML序列化类。
 * 
 * <p>
 * 序列化规则/要求说明：<br/>
 * <ul>
 * <li>不能序列化通用类，如Object等，否则可能导致无法反序列化；</li>
 * <li>只序列化可读写的属性；</li>
 * <li>序列化对象时，如果属性值类型为列表（{@link java.util.List}）或字典映射（{@link java.util.Map}），那么列表或字典映射对应的属性值中不能再嵌套（包含）其它集合；</li>
 * <li>包含于列表、字典映射集合中，不能包含同一基类/接口下的不同类型的继承/实现类，只能由一种统一类型的具体继承/实现类（提供缺省公开构造器的类）；<br/>
 * 如某个属性值返回的类型（type）为"java.util.List&lt;{@link Resource}&gt;"，但是具体结果中包含{@link Form}和{@link View} 对象（虽然他们都继承自{@link Resource})，这是不合法的。<br/>
 * 只有纯粹为"java.util.List&lt;{@link Form}&gt;"或"java.util.List&lt;{@link View}&gt;"时才是合法的；</li>
 * <li>属性名称不能为listItem、mapItem、mapKey、mapValue等值。</li>
 * </ul>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 */
public class XmlSerializer implements Serializer {

	/**
	 * 重载serialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Serializer#serialize(java.lang.Object, java.io.Writer)
	 */
	public void serialize(Object o, Writer writer) {
		if (o == null) return;
		Class<?> cls = o.getClass();
		if (cls.isPrimitive()) return;

		try {
			if (o instanceof XmlSerializable) {
				writer.write(((XmlSerializable) o).toXml());
				return;
			}
		} catch (IOException ex) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(ex);
		}

		SerializationFilter filter = null;
		if (o instanceof SerializationFilterable) filter = ((SerializationFilterable) o).getSerializationFilter(SerializationType.Xml);

		Map<Method, Method> methods = ObjectUtil.getInstanceRWPropMethods(o);// 获取可读写的属性。
		if (methods == null || methods.size() == 0) return;
		StringBuffer sb = new StringBuffer();

		String methodName = null;
		String propNameToCamelCase = null;

		Object val = null; // 方法返回值
		Class<?> retCls = null; // 返回值对应的java类

		StringWriter sw = null; // 输出文本的缓存对象
		Serializer ser = null; // 序列化对象
		List<?> valList = null;
		Map<?, ?> valMap = null;
		Class<?> itemTypeFirst = null; // 列表集合值或字典集合键值对应类型
		Class<?> itemTypeSecond = null; // 字典集合值对应类型
		Object itemValue = null;
		for (Method x : methods.keySet()) {
			retCls = x.getReturnType();
			methodName = x.getName();
			if (!methodName.startsWith("get")) continue; // 忽略非getter方法。
			if (x.getParameterTypes().length > 0) continue; // 忽略多个参数的方法。
			String propName = methodName.substring(3);
			propNameToCamelCase = StringUtil.toCamelCase(propName);
			if (filter != null && !filter.filter(propNameToCamelCase)) continue;

			val = ObjectUtil.getMethodResult(o, x);
			if (val == null) continue;

			if (ObjectUtil.checkVoid(retCls.getName())) continue; // void不处理。
			if (retCls.isPrimitive()) { // 原始类型属性。
				ser = new PrimitiveSerializer();
				sw = new StringWriter();
				ser.serialize(val, sw);
				sb.append("<").append(propNameToCamelCase).append(">").append(sw.toString()).append("</").append(propNameToCamelCase).append(">\r\n");
			} else if (val instanceof String) { // 字符串
				sb.append("<").append(propNameToCamelCase).append(">").append(StringUtil.encode4Xml(StringUtil.convertToString(val, ""))).append("</").append(propNameToCamelCase).append(">\r\n");
			} else if (retCls.isEnum()) { // 枚举
				ser = new EnumSerializer();
				sw = new StringWriter();
				ser.serialize(val, sw);
				sb.append("<").append(propNameToCamelCase).append(">").append(sw.toString()).append("</").append(propNameToCamelCase).append(">\r\n");
			} else if (val instanceof java.util.List) { // 列表集合
				valList = (List<?>) val;
				if (valList == null || valList.isEmpty()) continue;
				// retType = x.getGenericReturnType();
				itemValue = valList.get(0);
				itemTypeFirst = (itemValue != null ? itemValue.getClass() : null);
				if (itemTypeFirst == null) continue;
				sb.append("<").append(propNameToCamelCase).append(" type=\"java.util.List&lt;").append(StringUtil.encode4Xml(itemTypeFirst.getName())).append("&gt;\">\r\n");
				ser = new ListXmlSerializer();
				sw = new StringWriter();
				ser.serialize(val, sw);
				sb.append(sw.toString());
				sb.append("</").append(propNameToCamelCase).append(">\r\n");
			} else if (val instanceof java.util.Map) { // 哈希集合
				valMap = (Map<?, ?>) val;
				if (valMap == null || valMap.isEmpty()) continue;
				for (Object k : valMap.keySet()) {
					itemTypeFirst = (k != null ? k.getClass() : null);
					itemValue = valMap.get(k);
					itemTypeSecond = (itemValue != null ? itemValue.getClass() : null);
				}
				if (itemTypeFirst == null || itemTypeSecond == null) continue;
				sb.append("<").append(propNameToCamelCase).append(" type=\"java.util.Map&lt;").append(StringUtil.encode4Xml(itemTypeFirst.getName())).append(",").append(StringUtil.encode4Xml(itemTypeSecond.getName())).append("&gt;\">\r\n");
				ser = new MapXmlSerializer();
				sw = new StringWriter();
				ser.serialize(val, sw);
				sb.append(sw.toString());
				sb.append("</").append(propNameToCamelCase).append(">\r\n");
			} else if (XmlSerializable.class.isInstance(val)) { // 可xml序列化的对象。
				sb.append("<").append(propNameToCamelCase).append(" type=\"").append(StringUtil.encode4Xml(retCls.getName())).append("\">\r\n");
				sb.append(((XmlSerializable) val).toXml());
				sb.append("</").append(propNameToCamelCase).append(">\r\n");
			} else { // 其它普通类
				sb.append("<").append(propNameToCamelCase).append(" type=\"").append(StringUtil.encode4Xml(val.getClass().getName())).append("\">\r\n");
				sw = new StringWriter();
				this.serialize(val, sw);
				sb.append(sw.toString());
				sb.append("</").append(propNameToCamelCase).append(">\r\n");
			}
		}// for end
		// sb.append("</").append(elementName).append(">\r\n");

		try {
			writer.write(sb.toString());
		} catch (IOException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
	}// func end
}// class end

