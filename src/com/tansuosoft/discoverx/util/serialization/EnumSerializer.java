/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.io.IOException;
import java.io.Writer;

import com.tansuosoft.discoverx.util.EnumBase;
import com.tansuosoft.discoverx.util.ObjectUtil;

/**
 * 枚举类型的序列化和反序列化类。
 * 
 * @author coca@tansuosoft.cn
 */
public class EnumSerializer implements Serializer, Deserializer {
	/**
	 * 缺省构造器。
	 */
	public EnumSerializer() {
	}

	/**
	 * 重载serialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Serializer#serialize(java.lang.Object, java.io.Writer)
	 */
	public void serialize(Object o, Writer writer) {
		try {
			if (ObjectUtil.checkInstance(o, EnumBase.class))
				writer.write("" + ((EnumBase) o).getIntValue());
			else
				writer.write(o.toString());
		} catch (IOException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
	}

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	public Object deserialize(String src, Class<?> cls) {
		if (cls == null || !cls.isEnum()) return null;
		EnumBase[] enums = (EnumBase[]) cls.getEnumConstants();
		if (enums == null) return null;
		for (EnumBase x : enums) {
			if (Integer.parseInt(src) == x.getIntValue()) return x;
		}
		return null;
	}
}// class end

