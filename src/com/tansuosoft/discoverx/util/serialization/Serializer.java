/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.io.Writer;

/**
 * 将对象序列化到目标文本格式（一般为XML格式）的类对应的接口。
 * 
 * @author coca@tansuosoft.cn
 */
public interface Serializer {
	/**
	 * 将指定对象(o)序列化并输出序列化结果到指定Writer。
	 * 
	 * @param o 要序列化的对象。
	 * @param writer Writer 序列化输出结果（一般是文本）。
	 */
	public void serialize(Object o, Writer writer);
}

