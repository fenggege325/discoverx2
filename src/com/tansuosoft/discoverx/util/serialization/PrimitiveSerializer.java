/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.io.IOException;
import java.io.Writer;

import com.tansuosoft.discoverx.util.ObjectUtil;

/**
 * 原始类型对象的序列化和反序列化类。
 * 
 * @author coca@tansuosoft.cn
 */
public class PrimitiveSerializer implements Serializer, Deserializer {
	/**
	 * 缺省构造器。
	 */
	public PrimitiveSerializer() {
	}

	/**
	 * 重载serialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Serializer#serialize(java.lang.Object, java.io.Writer)
	 */
	public void serialize(Object o, Writer writer) {
		try {
			writer.write(com.tansuosoft.discoverx.util.StringUtil.convertToString(o, ""));
		} catch (IOException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
	}

	/**
	 * 重载deserialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Deserializer#deserialize(java.lang.String, java.lang.Class)
	 */
	public Object deserialize(String src, Class<?> cls) {
		if (cls == null || !cls.isPrimitive()) return null;
		return ObjectUtil.getResult(cls, src);
	}
}// class end

