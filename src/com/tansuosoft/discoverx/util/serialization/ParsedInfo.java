/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;
import com.tansuosoft.discoverx.util.logger.FileLogger;

/**
 * XML节点信息被初步解析后的解析结果信息对应的基础类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public abstract class ParsedInfo {
	private int m_depth = 0; // xml节点深度。
	private String m_elementName = null; // xml节点元素名。
	private String m_elementValue = null; // xml节点元素值。
	private Object m_bindObject = null; // xml节点绑定的对象。
	private ParsedInfo m_parent = null; // 所属上级解析信息。
	private String m_xmlSource = null; // 解析的xml源信息。

	/**
	 * 接收必要参数的构造器。
	 * 
	 * @param m_depth
	 * @param elementName
	 * @param bindObject
	 */
	public ParsedInfo(int depth, String elementName, Object bindObject) {
		this.m_depth = depth;
		this.m_elementName = elementName;
		this.m_bindObject = bindObject;
	}

	/**
	 * 返回xml节点深度。
	 * 
	 * @return int xml节点深度。
	 */
	public int getDepth() {
		return this.m_depth;
	}

	/**
	 * 设置xml节点深度。
	 * 
	 * @param depth int xml节点深度。
	 */
	public void setDepth(int depth) {
		this.m_depth = depth;
	}

	/**
	 * 返回xml节点元素名。
	 * 
	 * @return String xml节点元素名。
	 */
	public String getElementName() {
		return this.m_elementName;
	}

	/**
	 * 设置xml节点元素名。
	 * 
	 * @param elementName String xml节点元素名。
	 */
	public void setElementName(String elementName) {
		this.m_elementName = elementName;
	}

	/**
	 * 返回xml节点元素值。
	 * 
	 * @return String xml节点元素值。
	 */
	public String getElementValue() {
		return this.m_elementValue;
	}

	/**
	 * 设置xml节点元素值。
	 * 
	 * @param elementValue String xml节点元素值。
	 */
	public void setElementValue(String elementValue) {
		this.m_elementValue = elementValue;
	}

	/**
	 * 返回xml节点绑定的对象。
	 * 
	 * @return Object xml节点绑定的对象。
	 */
	public Object getBindObject() {
		return this.m_bindObject;
	}

	/**
	 * 设置xml节点绑定的对象。
	 * 
	 * @param bindObject Object xml节点绑定的对象。
	 */
	public void setBindObject(Object bindObject) {
		this.m_bindObject = bindObject;
	}

	/**
	 * 返回所属上级解析信息。
	 * 
	 * @return ParsedInfo 所属上级解析信息。
	 */
	public ParsedInfo getParent() {
		return this.m_parent;
	}

	/**
	 * 设置所属上级解析信息。
	 * 
	 * @param parent ParsedInfo 所属上级解析信息。
	 */
	public void setParent(ParsedInfo parent) {
		this.m_parent = parent;
	}

	/**
	 * 根据解析结果信息组装最终对象并设置组装结果到其上级对象。
	 */
	@SuppressWarnings("unchecked")
	public void assemble() {
		ParsedInfo parent = this.m_parent;
		if (parent == null) return;// 没有上级表示根对象。

		ParsedInfoType pit = this.getParsedInfoType();
		Object parentBindObject = parent.getBindObject();
		List<Object> list = null;
		ListObject listObject = null;
		MapObject mapObject = null;

		switch (pit.getIntValue()) {
		case 0: // 普通java bean对象
			this.setPropValue(parentBindObject, this.m_elementName, this.m_bindObject);
			break;
		case 1: // 基础属性
			this.setPropValueFromText(parentBindObject, this.m_elementName, this.m_elementValue);
			break;
		case 2: // 列表集合对象
			this.setPropValue(parentBindObject, this.m_elementName, this.m_bindObject);
			break;
		case 3: // 列表条目
			list = (List<Object>) parentBindObject;
			listObject = ((ListObject) parent);
			String listItemClassName = listObject.getGenericType();
			Object listItemObject = ObjectUtil.getBoxResult(listItemClassName, "");
			if (ObjectUtil.checkSimple(listItemClassName)) {
				listItemObject = ObjectUtil.getBoxResult(listItemClassName, this.m_elementValue);
			} else {
				this.setBindObject(listItemObject);
			}
			list.add(listItemObject);
			break;
		case 4: // 哈希字典集合对象
			this.setPropValue(parentBindObject, this.m_elementName, this.m_bindObject);
			break;
		case 5: // 哈希字典关键字
			mapObject = ((MapObject) parent);
			String keyClassName = mapObject.getKeyGenericType();
			Object keyObject = ObjectUtil.getBoxResult(keyClassName, "");
			if (ObjectUtil.checkSimple(keyClassName)) {
				keyObject = ObjectUtil.getBoxResult(keyClassName, this.m_elementValue);
			} else {
				this.setBindObject(keyObject);
			}
			mapObject.addKey(keyObject);
			break;
		case 6: // 哈希字典值
			mapObject = ((MapObject) parent);
			String valueClassName = mapObject.getValueGenericType();
			Object valueObject = ObjectUtil.getBoxResult(valueClassName, "");
			if (ObjectUtil.checkSimple(valueClassName)) {
				valueObject = ObjectUtil.getBoxResult(valueClassName, this.m_elementValue);
			} else {
				this.setBindObject(valueObject);
			}
			mapObject.addValue(valueObject);
			break;
		default:
			break;
		}// switch end
		if (verboseInfo != null && verbose) {
			StringBuilder sb = new StringBuilder();
			for (String msg : verboseInfo.keySet()) {
				if (sb.length() > 0) sb.append("\r\n");
				sb.append("\t").append(msg);
			}
			FileLogger.debug("反序列化\r\n%1$s\r\n过程输出：\r\n%2$s", this.getXmlSource(), sb.toString());
		}
	}

	/**
	 * 在obj对应的对象中，把propValue值转换为通过propName指定的getter属性的返回值类型，并设置这个转换后的值到通过propName指定的setter属性中。
	 * 
	 * @param obj Object
	 * @param propName String
	 * @param propValue String
	 */
	protected void setPropValueFromText(Object obj, String propName, String propValue) {
		if (obj == null || propName == null || propName.length() == 0) return; // || propValue == null
		String pascalFormat = StringUtil.toPascalCase(propName);
		String setPropMethodName = "set" + pascalFormat;
		String getPropMethodName = "get" + pascalFormat;
		Method m = null;
		Class<?> cls = obj.getClass();
		Class<?> paramType = null;
		try {
			Method getter = cls.getMethod(getPropMethodName, new Class<?>[] {});
			if (getter == null) return;
			paramType = getter.getReturnType();
			if (paramType == null) return;
			m = cls.getMethod(setPropMethodName, new Class<?>[] { paramType });
			if (m == null) return;
			Class<?>[] paramClasses = m.getParameterTypes();
			if (paramClasses == null || paramClasses.length != 1) return;
			Class<?> paramCls = paramClasses[0];
			if (!paramCls.getName().equalsIgnoreCase(paramType.getName())) return;
			Object x = ObjectUtil.getResult(paramCls, propValue);
			m.invoke(obj, x);
		} catch (SecurityException e) {
			addVerboseInfo(String.format("安全控制异常：%1$s.%2$s,原因：%3$s。", obj.getClass().getName(), propName, e.getMessage()), e);
		} catch (NoSuchMethodException e) {
			addVerboseInfo(String.format("类“%1$s”中找不到“%2$s”属性。", obj.getClass().getName(), propName), e);
		} catch (IllegalArgumentException e) {
			addVerboseInfo(String.format("设置属性错误结果时发生参数错误：对象：%1$s，属性名：%2$s,属性值：%3$s，原因：%4$s。", obj.getClass().getName(), propName, propValue, e.getMessage()), e);
		} catch (IllegalAccessException e) {
			addVerboseInfo(String.format("设置属性错误结果时发生访问权限错误：对象：%1$s，属性名：%2$s,属性值：%3$s，原因：%4$s。", obj.getClass().getName(), propName, propValue, e.getMessage()), e);
		} catch (InvocationTargetException e) {
			addVerboseInfo(String.format("设置属性错误结果时发生调用错误：对象：%1$s，属性名：%2$s,属性值：%3$s，原因：%4$s。", obj.getClass().getName(), propName, propValue, e.getMessage()), e);
		}
	}

	/**
	 * 在obj对应的对象中，把propValue值设置到通过propName指定的setter可读写属性中。
	 * 
	 * @param obj Object
	 * @param propName String
	 * @param propValue Object
	 */
	protected void setPropValue(Object obj, String propName, Object propValue) {
		if (obj == null || propName == null || propName.length() == 0 || propValue == null) return;
		String pascalFormat = StringUtil.toPascalCase(propName);
		String setPropMethodName = "set" + pascalFormat;
		String getPropMethodName = "get" + pascalFormat;
		Method m = null;
		Class<?> cls = obj.getClass();
		try {
			Method getter = cls.getMethod(getPropMethodName, new Class<?>[] {});
			if (getter == null) return;
			Class<?> paramType = getter.getReturnType();
			if (paramType == null) return;
			m = cls.getMethod(setPropMethodName, new Class<?>[] { paramType });
			if (m == null) return;
			Class<?>[] paramClasses = m.getParameterTypes();
			if (paramClasses == null || paramClasses.length != 1) return;
			m.invoke(obj, propValue);
		} catch (SecurityException e) {
			addVerboseInfo(String.format("安全控制异常：%1$s.%2$s,原因：%3$s。", obj.getClass().getName(), propName, e.getMessage()), e);
		} catch (NoSuchMethodException e) {
			addVerboseInfo(String.format("类“%1$s”中找不到“%2$s”属性。", obj.getClass().getName(), propName), e);
		} catch (IllegalArgumentException e) {
			addVerboseInfo(String.format("设置属性错误结果时发生参数错误：对象：%1$s，属性名：%2$s,属性值：%3$s，原因：%4$s。", obj.getClass().getName(), propName, propValue, e.getMessage()), e);
		} catch (IllegalAccessException e) {
			addVerboseInfo(String.format("设置属性错误结果时发生访问权限错误：对象：%1$s，属性名：%2$s,属性值：%3$s，原因：%4$s。", obj.getClass().getName(), propName, propValue, e.getMessage()), e);
		} catch (InvocationTargetException e) {
			addVerboseInfo(String.format("设置属性错误结果时发生调用错误：对象：%1$s，属性名：%2$s,属性值：%3$s，原因：%4$s。", obj.getClass().getName(), propName, propValue, e.getMessage()), e);
		}
	}

	/**
	 * 返回解析的xml源信息。
	 * 
	 * <p>
	 * 可能是xml文件路径或者xml内容文本。
	 * </p>
	 * 
	 * @return String
	 */
	public String getXmlSource() {
		return this.m_xmlSource;
	}

	protected static final String FIND_UNID = "<UNID>";

	/**
	 * 设置解析的xml源信息。
	 * 
	 * @param xmlSource String
	 */
	protected void setXmlSource(String xmlSource) {
		// 如果是xml内容，那么尝试获取其中的UNID值作为xml源信息以免输出异常信息时过长
		if (xmlSource != null && xmlSource.indexOf('<') >= 0) {
			int pos = xmlSource.indexOf(FIND_UNID);
			if (pos > 0) {
				m_xmlSource = xmlSource.substring(pos + FIND_UNID.length(), pos + FIND_UNID.length() + 32);
				return;
			}
		}
		this.m_xmlSource = xmlSource;
	}

	protected Map<String, Object> verboseInfo = null;
	private boolean verbose = false;

	/**
	 * 设置是否输出详细处理日志。
	 * 
	 * @param verbose
	 */
	protected void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	/**
	 * 添加详细日志消息文本
	 * 
	 * @param msg
	 * @param o 通常是异常对象，用于区分消息文本。
	 */
	protected void addVerboseInfo(String msg, Object o) {
		if (!verbose) return;
		if (msg == null || msg.isEmpty()) return;
		ParsedInfo root = getRoot();
		if (root == null) return;
		if (verboseInfo == null) verboseInfo = new HashMap<String, Object>();
		verboseInfo.put(msg, o);
	}

	/**
	 * 返回根节点。
	 * 
	 * @return ParsedInfo
	 */
	private ParsedInfo getRoot() {
		ParsedInfo current = this;
		ParsedInfo root = null;
		while (current != null) {
			ParsedInfo parent = current.getParent();
			if (parent == null) {
				root = current;
				break;
			} else {
				current = parent;
			}
		}
		return root;
	}

	/**
	 * 获取解析条目的类型。
	 * 
	 * @return ParsedInfoType
	 */
	public abstract ParsedInfoType getParsedInfoType();
}
