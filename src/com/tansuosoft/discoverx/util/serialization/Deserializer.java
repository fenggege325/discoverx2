/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

/**
 * 从目标文本数据源（一般是XML数据源）中反序列化信息为对象的类对应的接口。
 * 
 * @author coca@tansuosoft.cn
 */
public interface Deserializer {
	/**
	 * 将数据源信息反序列化到目标类指定类型的对象。
	 * 
	 * @param src 数据源文本，可能是路径或者直接数据源内容（系统应提供自动判断类型功能）。
	 * @param cls 反序列化的目标对象对应的类信息，可以为null。
	 * @return Object
	 */
	public Object deserialize(String src, Class<?> cls);
}

