/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

/**
 * 解析结果信息对象实例工厂。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class ParsedInfoFactory {

	/**
	 * 根据解析结果类型和必要初始化参数构造并返回解析结果。
	 * 
	 * @param parsedInfoType
	 * @param depth
	 * @param elementName
	 * @param bindObject
	 * @return ParsedInfo
	 */
	public static ParsedInfo getParsedInfo(ParsedInfoType parsedInfoType, int depth, String elementName, Object bindObject) {
		ParsedInfo pi = null;
		switch (parsedInfoType.getIntValue()) {
		case 0:
			pi = new NormalObject(depth, elementName, bindObject);
			break;
		case 1:
			pi = new Property(depth, elementName, bindObject);
			break;
		case 2:
			pi = new ListObject(depth, elementName, bindObject);
			break;
		case 3:
			pi = new ListItem(depth, elementName, bindObject);
			break;
		case 4:
			pi = new MapObject(depth, elementName, bindObject);
			break;
		case 5:
			pi = new MapKeyItem(depth, elementName, bindObject);
			break;
		case 6:
			pi = new MapValueItem(depth, elementName, bindObject);
			break;
		default:
			break;
		}
		return pi;
	}
}

