/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import com.tansuosoft.discoverx.util.ObjectUtil;
import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 列表集合类型的XML序列化类。
 * 
 * @author coca@tansuosoft.cn
 */
public class ListXmlSerializer implements Serializer {

	/**
	 * 重载serialize
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.Serializer#serialize(java.lang.Object, java.io.Writer)
	 */
	public void serialize(Object o, Writer writer) {
		StringWriter sw = null; // 输出文本的缓存对象
		Serializer ser = null; // 序列化对象
		Class<?> actualTypeCls1 = null; // 范型包含参数实际类型1对应java类
		List<?> valList = null; // 列表集合
		Object val1 = null; // 第一个值

		valList = (List<?>) o;
		if (valList == null || valList.isEmpty()) return;
		for (Object valx : valList) {
			val1 = valx;
			if (val1 != null) break;
		}
		StringBuffer sb = new StringBuffer();
		actualTypeCls1 = val1.getClass();
		for (Object valx : valList) {
			if (valx == null) continue;
			sb.append("<listItem>");
			if (ObjectUtil.checkPrimitive(actualTypeCls1.getName())) {
				sb.append(StringUtil.encode4Xml(StringUtil.convertToString(valx, "0")));
			} else if (actualTypeCls1.isPrimitive() || actualTypeCls1.isEnum()) {
				ser = (actualTypeCls1.isPrimitive() ? new PrimitiveSerializer() : new EnumSerializer());
				sw = new StringWriter();
				ser.serialize(valx, sw);
				sb.append(sw.toString());
			} else if (actualTypeCls1.getName() == String.class.getName()) {
				sb.append(StringUtil.encode4Xml(StringUtil.convertToString(valx, "")));
			} else {
				sw = new StringWriter();
				ser = new XmlSerializer();
				ser.serialize(valx, sw);
				sb.append("\r\n").append(sw.toString());
			}
			sb.append("</listItem>\r\n");
		}// for end
		try {
			writer.write(sb.toString());
		} catch (IOException e) {
			com.tansuosoft.discoverx.util.logger.FileLogger.error(e);
		}
	}
}

