/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 哈希字典集合对应的xml解析结果信息类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class MapObject extends ParsedInfo {

	/**
	 * 接收必要参数的构造器。
	 * 
	 * @param m_depth
	 * @param elementName
	 * @param bindObject
	 */
	public MapObject(int depth, String elementName, Object bindObject) {
		super(depth, elementName, bindObject);
	}

	/**
	 * 重载getParsedInfoType
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.ParsedInfo#getParsedInfoType()
	 */
	@Override
	public ParsedInfoType getParsedInfoType() {
		return ParsedInfoType.MapObject;
	}

	private String m_keyGenericType = null; // 哈希集合关键字（key）对应的范型类型名称。
	private String m_valueGenericType = null; // 哈希集合值（value）对应的范型类型名称。
	private List<Object> m_keys = null; // 按顺序保存哈希集合的所有键(key)
	private List<Object> m_values = null; // 按顺序保存哈希集合的所有值(value)

	/**
	 * 增加一个key到按顺序保存的哈希集合的所有key中。
	 * 
	 * @param key Object
	 */
	public void addKey(Object key) {
		if (this.m_keys == null) this.m_keys = new ArrayList<Object>();
		this.m_keys.add(key);
	}

	/**
	 * 增加一个value到按顺序保存的哈希集合的所有value中。
	 * 
	 * @param key Object
	 */
	@SuppressWarnings("unchecked")
	public void addValue(Object value) {
		if (this.m_values == null) this.m_values = new ArrayList<Object>();
		this.m_values.add(value);
		Map<Object, Object> map = (Map<Object, Object>) this.getBindObject();
		if (map != null) {
			Object key = this.m_keys.get(this.m_keys.size() - 1);
			Object val = this.m_values.get(this.m_values.size() - 1);
			map.put(key, val);
		}
	}

	/**
	 * 返回哈希集合关键字（key）对应的范型类型名称。
	 * 
	 * @return String 哈希集合关键字（key）对应的范型类型名称。
	 */
	public String getKeyGenericType() {
		return this.m_keyGenericType;
	}

	/**
	 * 设置哈希集合关键字（key）对应的范型类型名称。
	 * 
	 * @param keyGenericType String 哈希集合关键字（key）对应的范型类型名称。
	 */
	public void setKeyGenericType(String keyGenericType) {
		this.m_keyGenericType = keyGenericType;
	}

	/**
	 * 返回哈希集合值（value）对应的范型类型名称。
	 * 
	 * @return String 哈希集合值（value）对应的范型类型名称。
	 */
	public String getValueGenericType() {
		return this.m_valueGenericType;
	}

	/**
	 * 设置哈希集合值（value）对应的范型类型名称。
	 * 
	 * @param valueGenericType String 哈希集合值（value）对应的范型类型名称。
	 */
	public void setValueGenericType(String valueGenericType) {
		this.m_valueGenericType = valueGenericType;
	}
}

