/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

/**
 * 普通java bean对象对应的xml解析结果信息类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class NormalObject extends ParsedInfo {

	/**
	 * 接收必要参数的构造器。
	 * 
	 * @param m_depth
	 * @param elementName
	 * @param bindObject
	 */
	public NormalObject(int depth, String elementName, Object bindObject) {
		super(depth, elementName, bindObject);
	}

	/**
	 * 重载getParsedInfoType
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.ParsedInfo#getParsedInfoType()
	 */
	@Override
	public ParsedInfoType getParsedInfoType() {
		return ParsedInfoType.NormalObject;
	}

}

