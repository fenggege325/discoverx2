/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.serialization;

/**
 * 表示可xml序列化的接口。
 * 
 * <p>
 * 实现此接口的类可以通过其{@link XmlSerializable#toXml()}方法获取此类对应对象的xml序列化结果。
 * </p>
 * <p>
 * 自定义xml序列化输出的格式必须符合{@link XmlSerializer}的序列化规则。
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public interface XmlSerializable {
	/**
	 * 获取对象实例的XML序列化并获取序列化结果。
	 * 
	 * <p>
	 * 实现类中具体实现时，只要返回内部属性的XML序列化结果即可，不需要生成类自身对应的节点。
	 * </p>
	 * 
	 * @return String
	 */
	public String toXml();
}

