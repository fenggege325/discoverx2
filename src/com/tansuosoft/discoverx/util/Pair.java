/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

/**
 * 表示由关键字和对应值构成的键-值对象。
 * 
 * @author coca@tansuosoft.cn
 */
public class Pair {
	private Object m_first = null; // 第一个值。
	private Object m_second = null; // 第二个值。

	/**
	 * 缺省构造器。
	 */
	public Pair() {
	}

	/**
	 * 接收值参数的构造器。
	 */
	public Pair(Object first, Object second) {
		this.m_first = first;
		this.m_second = second;
	}

	/**
	 * 返回第一个值。
	 * 
	 * @return Object
	 */
	public Object getFirst() {
		return this.m_first;
	}

	/**
	 * 设置第一个值。
	 * 
	 * @param first Object
	 */
	public void setFirst(Object first) {
		this.m_first = first;
	}

	/**
	 * 返回第二个值。
	 * 
	 * @return Object
	 */
	public Object getSecond() {
		return this.m_second;
	}

	/**
	 * 设置第二个值。
	 * 
	 * @param second Object
	 */
	public void setSecond(Object second) {
		this.m_second = second;
	}
}

