/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.tree;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.TreeSet;

import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 从按一定层次分层的文本集合中构造树的类。
 * 
 * <p>
 * 比如有一个文本集合包含如下文本列表：
 * </p>
 * <ul>
 * <li>级别1_a\级别2_a</li>
 * <li>级别1_a\级别2_b\级别3_a</li>
 * <li>级别1_b</li>
 * <li>级别1_c\级别2_a</li>
 * <li>...</li>
 * </ul>
 * <p>
 * 那么此类可以根据这些文本构造出可供输出为树型数据结构的结果。其中文本集(如数组、列表等)合必须具有固定的树层次分隔符。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class TreeBuilder {
	private Comparator<String> comparator = null;
	private Map<Integer, Map<String, Tree>> allLevelAndTreesMap = null;

	/**
	 * 使用按中文拼音顺序排序文本的排序器的缺省构造器。
	 */
	public TreeBuilder() {
		this(new StringComparator());
	}

	/**
	 * 接收自定义文本排序器的构造器。
	 * 
	 * @param c
	 */
	public TreeBuilder(Comparator<String> c) {
		comparator = c;
		allLevelAndTreesMap = new HashMap<Integer, Map<String, Tree>>();
	}

	private Map<String, Boolean> query4ParentAddedMap = new HashMap<String, Boolean>(); // 用来检查是否String指定的值对应的树节点已经被加入到其父节点，这样可以防止重复加入。

	/**
	 * 加入并构造树节点。
	 * 
	 * @param t
	 * @return boolean 成功则返回true。
	 */
	public boolean add(Tree t) {
		boolean result = false;
		if (t == null || t.getValue() == null || t.getValue().isEmpty()) return result;
		Map<String, Tree> sameLevelTreesMap = null;
		Tree child = null;
		Tree p = t;
		while (p != null) {
			String value = p.getValue();
			int pLevel = p.getLevel();
			sameLevelTreesMap = allLevelAndTreesMap.get(pLevel);
			if (sameLevelTreesMap == null) { // 如果同级别不存在
				sameLevelTreesMap = new TreeMap<String, Tree>(comparator);
				allLevelAndTreesMap.put(pLevel, sameLevelTreesMap);
				if (child != null) {
					query4ParentAddedMap.put(child.getValue(), true);
					p.addChild(child);
				}
				sameLevelTreesMap.put(value, p);
			} else { // 如果同级别已经存在
				Tree existParent = sameLevelTreesMap.get(value);
				if (existParent == null) { // 如果同级别节点不存在
					if (child != null) {
						query4ParentAddedMap.put(child.getValue(), true);
						p.addChild(child);
					}
					sameLevelTreesMap.put(value, p);
				} else { // 如果同级别节点已经存在
					if (child != null && !query4ParentAddedMap.containsKey(child.getValue())) {
						query4ParentAddedMap.put(child.getValue(), true);
						existParent.addChild(child);
					}
				}
			}
			child = p;
			p = p.getParent();
		}
		result = true;
		return result;
	}

	/**
	 * 获取所有一级节点。
	 * 
	 * <p>
	 * 可以通过此方法递归输出树。
	 * </p>
	 * 
	 * @return Tree[]
	 */
	public Tree[] values() {
		query4ParentAddedMap.clear();
		query4ParentAddedMap = null;
		Map<String, Tree> map = allLevelAndTreesMap.get(1);
		if (map == null || map.size() == 0) return null;
		Tree[] arr = new Tree[map.size()];
		int idx = 0;
		for (String k : map.keySet()) {
			arr[idx++] = map.get(k);
		}
		return arr;
	}

	/**
	 * 测试用的main方法。
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// generateTestFile(); //需要输出测试数据文件时使用。

			String fn = "e:\\temp\\test_tree.txt";
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fn), "utf-8"));
			// String fn = "e:\\temp\\treebuilder_test.txt";
			// BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fn), "GBK"));

			String line = null;
			// 构造树
			int cnt = 0;
			long start = System.nanoTime();
			TreeBuilder treeBuilder = new TreeBuilder();
			while ((line = br.readLine()) != null) {
				if (StringUtil.isBlank(line.trim())) continue;
				treeBuilder.add(new Tree(line));
				cnt++;
			}
			long end = System.nanoTime();
			System.out.println("构造包含“" + cnt + "”个条目的树用时:" + (end - start) / 1000000);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("e:\\temp\\test_tree_result_" + treeBuilder.getClass().getSimpleName().toLowerCase() + ".txt"), "utf-8"));
			bw.write("构造包含“" + cnt + "”个条目的树共用时:" + ((end - start) / 1000000) + "\r\n");
			for (Tree t : treeBuilder.values()) {
				bw.write(t.toString());
			}
			bw.flush();
			bw.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 用于生成测试数据文件。
	 */
	protected static void generateTestFile() {
		final int MAX_LEVEL = 5;
		final int MAX_COUNT = 10000;
		int level = 0;
		try {
			Random r = new Random();
			TreeSet<String> fvs = new TreeSet<String>(new StringComparator());
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("e:\\temp\\test_tree.txt"), "utf-8"));
			int idx = 0;
			int cnt = 0;
			while (true) {
				if (fvs.size() > MAX_COUNT) break;
				cnt = r.nextInt(50);
				String l1 = String.format("值%05d", idx);
				for (int i = 0; i < cnt; i++) {
					StringBuilder sb = new StringBuilder();
					String fv = "";
					String cv = String.format("C%02d", i);
					level = 1 + r.nextInt(MAX_LEVEL);
					for (int l = 0; l < level; l++) {
						if (l == 0) sb.append(l1);
						else sb.append("\\").append(l1).append(cv).append("_L").append(l + 1);
					}
					fv = sb.toString();
					fvs.add(fv);
				}
				idx++;
			}
			for (String fv : fvs) {
				if (fv == null || fv.isEmpty()) continue;
				bw.write(fv + "\r\n");
			}
			bw.flush();
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 调试输出用的方法。
	 * 
	 * @param f
	 * @param objs
	 */
	protected static void wl(String f, Object... objs) {
		System.out.print(String.format(f, objs));
	}
}

