/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.tree;

import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.Comparator;

/**
 * ���ַ���������ƴ��˳�������{@link Comparator}ʵ���ࡣ
 * 
 * @author coca@tensosoft.com
 */
public final class StringComparator implements Comparator<String> {

	/**
	 * ������ƴ��˳���������������
	 */
	public static final RuleBasedCollator COLLATOR = (RuleBasedCollator) Collator.getInstance(java.util.Locale.CHINA);

	/**
	 * ���أ�ʵ������
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(String o1, String o2) {
		if (o1 == null && o2 != null) return -1;
		if (o1 == null && o2 == null) return 0;
		if (o1 != null && o2 == null) return 1;
		return COLLATOR.compare(o1, o2);
	}

}

