/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util.tree;

import java.util.ArrayList;
import java.util.List;

import com.tansuosoft.discoverx.util.StringUtil;

/**
 * 表示一个树节点对象的类。
 * 
 * @author coca@tensosoft.com
 */
public class Tree {
	/**
	 * 默认树层次分隔符字符串：反斜杠。
	 */
	public static final String DEFAULT_DELIMITER = "\\";
	/**
	 * 默认树层次分隔符字符：反斜杠。
	 */
	public static final char DEFAULT_DELIMITER_CHAR = DEFAULT_DELIMITER.charAt(0);

	private int m_level = 0; // 树的层次级别
	private String[] m_values = null; // 每个层次级别的值。
	private List<Tree> m_children = new ArrayList<Tree>(); // 包含的子节点。
	private String m_fullValue = null; // 完整节点值。
	private String m_delimiter = null; // 分隔符。
	private Object m_data = null; // 树包含的数据。

	/**
	 * 接收反斜杠分隔的完整节点值的构造器。
	 * 
	 * @param fullValue
	 */
	public Tree(String fullValue) {
		this(fullValue, null, DEFAULT_DELIMITER_CHAR);
	}

	/**
	 * 接收反斜杠分隔的完整节点值和数据值的构造器。
	 * 
	 * @param fullValue
	 */
	public Tree(String fullValue, Object data) {
		this(fullValue, data, DEFAULT_DELIMITER_CHAR);
	}

	/**
	 * 接收delimiter分隔的完整节点值的构造器。
	 * 
	 * @param fullValue
	 * @param delimiter
	 */
	public Tree(String fullValue, char delimiter) {
		this(fullValue, null, delimiter);
	}

	/**
	 * 接收delimiter分隔的完整节点值和数据值的构造器。
	 * 
	 * @param fullValue
	 * @param data
	 * @param delimiter
	 */
	public Tree(String fullValue, Object data, char delimiter) {
		if (fullValue == null || fullValue.isEmpty()) throw new IllegalArgumentException("无效的节点值。");
		m_fullValue = fullValue;
		m_values = StringUtil.splitString(fullValue, delimiter);
		m_delimiter = new String(new char[] { delimiter });
		m_level = m_values.length;
		m_data = data;
	}

	/**
	 * 返回树的层次级别。
	 * 
	 * <p>
	 * 如{@link #getValue()}的结果为“级别1\级别2”，则返回2。
	 * </p>
	 * 
	 * @return int
	 */
	public int getLevel() {
		return this.m_level;
	}

	/**
	 * 返回每个层次级别的值。
	 * 
	 * <p>
	 * 如{@link #getValue()}的结果为“级别1\级别2”，则返回包含2个值的数组，其包含值分别为“级别1”、“级别2”。
	 * </p>
	 * 
	 * @return String[]
	 */
	public String[] getValues() {
		return this.m_values;
	}

	/**
	 * 返回包含的子节点。
	 * 
	 * @return List&lt;Tree&gt;
	 */
	public List<Tree> getChildren() {
		return this.m_children;
	}

	/**
	 * 追加一个子节点。
	 * 
	 * @param child
	 * @return boolean 成功则返回true.
	 */
	protected boolean addChild(Tree child) {
		return m_children.add(child);
	}

	/**
	 * 返回完整节点值。
	 * 
	 * @return String
	 */
	public String getValue() {
		return this.m_fullValue;
	}

	/**
	 * 新建并返回一个当前树节点的上级树节点对象。
	 * 
	 * @return Tree
	 */
	protected Tree getParent() {
		int pl = m_level - 1;
		if (pl == 0) return null;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < pl; i++) {
			sb.append(i == 0 ? "" : m_delimiter).append(m_values[i]);
		}
		return new Tree(sb.toString());
	}

	/**
	 * 返回树包含的实际数据值。
	 * 
	 * <p>
	 * 使用者可传入各种类型的值，并在输出时一并输出或做其它自定义的用途。
	 * </p>
	 * 
	 * @return Object
	 */
	public Object getData() {
		return m_data;
	}

	/**
	 * 重载：输出方便调试的字符串文本。
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < m_level; i++) {
			sb.append("-");
		}
		sb.append(m_values[m_values.length - 1]);
		if (m_children.size() > 0) sb.append("{");
		int idx = 0;
		for (Tree t : m_children) {
			sb.append(idx == 0 ? "\r\n" : "").append(t.toString());
			idx++;
		}
		if (m_children.size() > 0) {
			for (int i = 0; i < m_level; i++) {
				sb.append("-");
			}
			sb.append("}");
		}
		sb.append("\r\n");
		return sb.toString();
	}

}

