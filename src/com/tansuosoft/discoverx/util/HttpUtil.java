/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Http相关的实用工具类。
 * 
 * @author coca@tansuosoft.cn
 */
public class HttpUtil {

	/**
	 * 无法获取客户端地址时返回的值。
	 */
	public static final String UNKNOWN_REMOTE_ADDRESS = "[未知地址]";

	/**
	 * 无法获取客户端名称时返回的值。
	 */
	public static final String UNKNOWN_CLIENT = "[未知客户端]";

	protected static final String CLIENTIP_PARAM_NAME = "clientip";
	protected static final String UNKNOWN = "unknown";
	protected static final String USER_AGENT_HEADER_NAME = "User-Agent";

	/**
	 * 获取指定http请求对应的请求客户端中通过名为“clientip”的参数人工提交的网络地址。
	 * 
	 * <p>
	 * 通常用于获取人为提交的小助手客户端实际地址。
	 * </p>
	 * 
	 * @param request
	 * @return String
	 */
	public static String getClientIP(HttpServletRequest request) {
		if (request == null) return null;
		final char c1 = ';';
		final char c2 = '-';
		try {
			String clientip = request.getParameter(CLIENTIP_PARAM_NAME);
			if (clientip != null && clientip.length() > 0) return clientip.replace(c1, c2);

			String clientipofheader = request.getHeader(CLIENTIP_PARAM_NAME);
			if (clientipofheader != null && clientipofheader.length() > 0) return clientipofheader.replace(c1, c2);
			return clientip;
		} catch (NullPointerException ex) {

		}
		return null;
	}

	/**
	 * 获取指定http请求对应的请求客户端地址。
	 * 
	 * <p>
	 * 本方法尝试从http请求中（用于获取请求地址）相关的CGI变量中获取。
	 * </p>
	 * 
	 * @param request HttpServletRequest
	 * @return String
	 */
	public static String getRemoteAddr(HttpServletRequest request) {
		if (request == null) return UNKNOWN_REMOTE_ADDRESS;
		try {
			String ip = request.getHeader("X-FORWARDED-FOR");
			if (ip != null && ip.length() > 0 && !UNKNOWN.equalsIgnoreCase(ip)) {
				ip = ip.toLowerCase().replace(UNKNOWN, UNKNOWN_REMOTE_ADDRESS);
			}
			if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
				ip = request.getHeader("Proxy-Client-IP");
			}
			if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
				ip = request.getHeader("WL-Proxy-Client-IP");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("HTTP_CLIENT_IP");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("HTTP_X_FORWARDED_FOR");
			}
			if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}

			return (ip == null || ip.length() == 0 ? UNKNOWN_REMOTE_ADDRESS : ip.trim());
		} catch (NullPointerException ex) {
		}
		return UNKNOWN_REMOTE_ADDRESS;
	}

	/**
	 * 获取请求客户端名称。
	 * 
	 * @param request
	 * @return String
	 */
	public static String getClient(HttpServletRequest request) {
		if (request == null) return UNKNOWN_CLIENT;
		return StringUtil.getValueString(request.getHeader(USER_AGENT_HEADER_NAME), UNKNOWN_CLIENT);
	}
}

