/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

import com.tansuosoft.discoverx.util.serialization.JsonSerializable;
import com.tansuosoft.discoverx.util.serialization.XmlSerializable;

/**
 * 表示把包含的属性值封装为json或xml格式以供客户端AJAX请求使用的类。
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public class AJAXResponse implements XmlSerializable, JsonSerializable {
	/**
	 * 缺省构造器。
	 */
	public AJAXResponse() {
	}

	private String m_message = null; // 处理结果消息。
	private int m_status = 0; // 状态。
	private Object m_tag = null; // 额外内容。
	private boolean m_outputXMLDeclaration = true; // 是否输出XML Declaration，默认为true。

	/**
	 * 返回处理结果消息。
	 * 
	 * @return String 处理结果消息。
	 */
	public String getMessage() {
		return this.m_message;
	}

	/**
	 * 设置处理结果消息。
	 * 
	 * @param message String 处理结果消息。
	 */
	public void setMessage(String message) {
		this.m_message = message;
	}

	/**
	 * 返回处理结果状态，默认为0。
	 * 
	 * <p>
	 * 返回的状态值为0表示正常处理，不为零表示异常状态。
	 * </p>
	 * 
	 * @return int 状态。
	 */
	public int getStatus() {
		return this.m_status;
	}

	/**
	 * 设置处理结果状态，默认为0。
	 * 
	 * @param status int 状态。
	 */
	public void setStatus(int status) {
		this.m_status = status;
	}

	/**
	 * 返回额外内容。
	 * 
	 * <p>
	 * 序列化为xml或json时，如果此属性有非null返回值则会一并输出。
	 * </p>
	 * 
	 * @return Object 额外内容。
	 */
	public Object getTag() {
		return this.m_tag;
	}

	/**
	 * 设置额外内容。
	 * 
	 * @param tag Object 额外内容。
	 */
	public void setTag(Object tag) {
		this.m_tag = tag;
	}

	/**
	 * 返回是否输出XML Declaration，默认为true。
	 * 
	 * @return boolean
	 */
	// public boolean getOutputXMLPI() {
	// return this.m_outputXMLDeclaration;
	// }
	/**
	 * 设置是否输出XML PI信息。
	 * 
	 * <p>
	 * 默认为true，它指示输出xml内容时是否同时输出“&lt;?xml version="1.0" encoding="gbk"?&gt;”信息。
	 * </p>
	 * 
	 * @param outputXMLDeclaration boolean。
	 */
	public void setOutputXMLPI(boolean outputXMLDeclaration) {
		this.m_outputXMLDeclaration = outputXMLDeclaration;
	}

	/**
	 * 重载toXml:输出XML文本。
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.XmlSerializable#toXml()
	 */
	public String toXml() {
		StringBuilder sb = new StringBuilder();
		if (this.m_outputXMLDeclaration) sb.append("<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n");
		sb.append("<response>\r\n");
		sb.append("<status>").append(this.getStatus()).append("</status>");
		sb.append("<message>").append(StringUtil.encode4Xml(this.getMessage())).append("</message>");
		if (this.m_tag != null) {
			sb.append("<tag>");
			if (m_tag instanceof XmlSerializable) {
				sb.append(StringUtil.encode4Xml(((XmlSerializable) m_tag).toXml()));
			} else {
				sb.append(StringUtil.encode4Xml(this.m_tag.toString()));
			}
			sb.append("</tag>");
		}
		sb.append("</response>\r\n");
		return sb.toString();
	}

	/**
	 * 重载toJson:输出JSON文本。
	 * 
	 * <p>
	 * 不包括最外层的大括号（即不包含最外层的“{”/“}”）。
	 * </p>
	 * 
	 * @see com.tansuosoft.discoverx.util.serialization.JsonSerializable#toJson()
	 */
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		sb.append("message:").append("'").append(StringUtil.encode4Json(this.m_message)).append("'");
		sb.append(",").append("status:").append(this.m_status);
		if (this.m_tag != null) {
			sb.append(",").append("tag:");
			if (m_tag instanceof JsonSerializable) {
				sb.append("{").append(((JsonSerializable) m_tag).toJson()).append("}");
			} else {
				sb.append("'").append(StringUtil.encode4Json(this.m_tag.toString())).append("'");
			}
		}
		return sb.toString();
	}

}

