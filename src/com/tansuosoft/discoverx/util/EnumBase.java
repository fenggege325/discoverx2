/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

/**
 * 系统所有枚举对象的通用实现接口定义。
 * 
 * <p>
 * <strong>系统所有枚举对象必须实现此接口，并重载toString方法以返回getIntValue结果的字符串表现形式。</strong>
 * </p>
 * 
 * @author coca@tansuosoft.cn
 * 
 */
public interface EnumBase {
	/**
	 * 返回枚举值对应的数字值（如果没有定义枚举数字值，那么建议默认可以返回枚举序号值作为数字值）。
	 * 
	 * @return int
	 */
	public int getIntValue();

	/**
	 * 根据枚举名称对应的字符串或枚举名称对应的数字值（字符串形式表示）返回枚举对象。
	 * 
	 * @param v
	 * @return Enum<?>
	 */
	public Enum<?> parse(String v);

}

