/*
 * Copyright 2009-2014 Tensosoft.
 *  
 * Licensed under the Tensosoft Opensource License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.tensosoft.com/eula.html
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tansuosoft.discoverx.util;

/**
 * 仅需输出日志信息的运行时异常类。
 * 
 * <p>
 * 记录错误日志时，此类型的异常不会被输出到错误日志而只会输出到普通日志。
 * </p>
 * 
 * @author coca@tensosoft.com
 */
public class InfoLogLevelException extends RuntimeException {

	/**
	 * 序列化序号。
	 */
	private static final long serialVersionUID = 5607171084013117302L;

	/**
	 * 缺省构造器。
	 */
	public InfoLogLevelException() {
		super();
	}

	/**
	 * 构造器。
	 * 
	 * @param message
	 * @param cause
	 */
	public InfoLogLevelException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 构造器。
	 * 
	 * @param message
	 */
	public InfoLogLevelException(String message) {
		super(message);
	}

	/**
	 * 构造器。
	 * 
	 * @param cause
	 */
	public InfoLogLevelException(Throwable cause) {
		super(cause);
	}

	/**
	 * 获取导致此异常的根{@link java.lang.Throwable}对象。
	 * 
	 * @return Throwable
	 */
	public Throwable getRootThrowable() {
		return ThrowableUtil.getRootThrowable(this);
	}
}

