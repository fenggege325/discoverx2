腾硕云办公平台(discoverx2)服务器端内核Java代码。
内核源码可以用以下方式编译
1.maven，执行maven package即可编译；
2.gradle，执行gradle build即可编译；
3.直接用eclipse打开，参考pom.xml中引用的依赖包并将对应jar文件下载下来并在eclipse中添加对应的jar引用即可编译。
注意:
1.gradle编译时，如果要自动拷贝到您的webapp下的WEB-INF/lib下，需要修改一下build.gradle中cp2Webapp任务的targetWebAppDir所指向的路径；
2.用eclipse打开时，可以用ant编译，注意修改builder.xml中的路径等信息即可；
3.javadoc会生成在工程根路径的javadocs目录下。

更多内容请访问：http://www.tensosoft.com